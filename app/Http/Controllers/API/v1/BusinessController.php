<?php
namespace App\Http\Controllers\API\v1;
use Hash;
use Mail;
use Input;
use Route;
use Session;
use Storage;
use Validator;
use App\model\Gig;
use Carbon\Carbon;
use App\model\Post;
use App\model\User;
use App\model\Admin;
use App\model\Rating;
use App\model\GigUser;
use App\model\Payment;
use App\model\GigMedia;
use App\model\GigPrice;
use App\model\PostUser;
use App\model\Timeline;
use App\model\GigDetail;
use App\model\PostImage;
use App\model\PostPrice;
use App\model\ShareData;
use Lcobucci\JWT\Parser;
use App\model\PostDetail;
use App\model\TempTokens;
use Safaricom\Mpesa\Mpesa;
use App\model\AdminPayment;
use App\model\Notification;
use App\model\TimelineLikes;
use Illuminate\Http\Request;
use App\model\BusinessDetail;
use App\model\GigMediaDetail;
use App\model\TimelineImages;
use App\model\InfluencerPrice;
use App\model\InfluencerDetail;
use App\model\InfluencerImages;
use App\model\InfulancerRating;
use App\model\MpesaPhoneNumber;
use App\model\PostDetailsMedia;
use App\model\CallBackTransaction;
use App\model\InfluencerInterests;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\API\v1\BaseTrait;
///peter live url http://3.17.207.36/admin/login
class BusinessController extends Controller
{
    use BaseTrait;
    
    public function paymentProcess(Request $request){
        
        $uid = uniqid();
        $BusinessShortCode ="4023745";
        $LipaNaMpesaPasskey = "c9775d74fa23cae71fbaa79642fa8d1856302e3e67f5b32684998618bce9e34f";
        $TransactionType = "CustomerPayBillOnline";
        $Amount = "1";
        //254722101512
        //254705837332
        
        //254742922085
        //254722101512
        //254706322847
        $PartyA = "254706322847";
        $PartyB = $BusinessShortCode;
        $PhoneNumber = $PartyA;
        // $CallBackURL = 'https://1.6.98.142/CallBackURL/'.$uid."";
        $CallBackURL = 'https://twiva.co.ke/mpesaCallbackUrl/'.$uid;
        $AccountReference = 'GHJ9e545869';
        $TransactionDesc = 'HK(&t(&545&*^^';
        $Remark = 'test';
        
        
        $c2bTransaction = Mpesa::STKPushSimulation($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remark);
        CallBackTransaction::insert(['u_id'=>$uid]);
        //return $c2bTransaction;
    }
    
    
    public function mpesaCallbackUrl(Request $request,$uid){
        
        $requested = $request->all();
        $save_data = json_encode($requested);
        CallBackTransaction::where('u_id',$uid)->update(["res" => $save_data]);
    }

    
    public function businessPaymentProcess(Request $request) {
        $InitiatorName = "peter";
                      $SecurityCredential = "OIlAtAWA7ync43OIwKb6V4KqWBzIzB79p76CmgVwBRtjK5vuQVK/6L2ny3ld/Q4KGp6PYlBxNNKLW55haibLw2dMbAu/ZrQcyGuyQpI8vWd3hOoNA2yyA0JsVFCYQT/+4vUjDi5wLBJ8/NpeBWV3Zfhr5Mw7rZEky6S/AkIwQ5m78UynAquJtQkV7JUMD1t4Yaw6m4iK1gUzW+FjqyP4lK33m98oomAnapSejaeYZfwM8A5ipSxSr+4ZjK292DMc4h9kSh8yHnVX8eHdG2+K5JNcq14+hB9SAVTrjYv9jx+GCTUznaH3vGejUh6+zFxuD12HSkal0MXMo4eXJIz0UA==";
                      $CommandID = "BusinessPayment";
                      $Amount = "1";
                      $PartyA = "4023745";//live
                      //$PartyB = "254722101512";//local
                      $PartyB = "254722101512";//phone number
                      $Remarks = "okay this better work";
                      $QueueTimeOutURL = "https://twiva.co.ke/m_pesa_callback.php";
                      $ResultURL = "https://twiva.co.ke/m_pesa_callback.php";
                      $Occasion = "testing";
                      $b2cTransaction = Mpesa::b2c($InitiatorName, $SecurityCredential, $CommandID, $Amount, $PartyA, $PartyB, $Remarks, $QueueTimeOutURL, $ResultURL, $Occasion);
        return $b2cTransaction;
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all() , ['email' => 'required|email|unique:users', 'password' => 'required|min:6', 'profile' => "image|mimes:jpeg,jpg,png,gif|max:5120", 'name' => "max:255", 'device_type' => 'required', 'device_id' => 'required', ], ["email.required" => "Please enter email address", "email.email" => "Please enter a valid email address", "password.required" => "Please enter password", ]);

        if ($validator->fails())
        {
            return response()
                ->json(["message" => $validator->errors()
                ->first() ], BAD_REQUEST);
        }

        $input = array();
        $input = $request->all();
        if ($request->password)
        {
            $input['password'] = Hash::make($request->password);
        }

        $input["profile"] = $this->uploadImage($request);
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwyz1234567890";
        $code =  substr(str_shuffle($str) , 0, 6);
        $input["refferal_code"] = $code;
        $input['user_type'] = 'B';
        $verify_link = Crypt::encryptString($request->email);
        $input['email_verify'] = $verify_link;
       
        $user = User::create($input);

        $input["user_id"] = $user->id;
        if ($request->get("business_detail"))
        {
            BusinessDetail::create($input);
        }

        $user->token = $user->createToken('*')->accessToken;

        if ($user)
        {
            $url = url('verify/'.$verify_link);
            try{
                Mail::send('email_verify',['url' => $url,'user_data' => $user], function ($m) use ($user) {
                        $m->from('info@twiva.co.ke', 'Twiva');
                        $m->to($user->email,'App User');
                        $m->subject('Email verification link');
                    });
                 return response()->json(["message" =>'Signup Successfully & email verification link has been sent to your registered email address.',"data" => $user]);
            }catch(\Exception $e){
                 return response()->json('Oops Something wrong! ' . $e->getMessage());
            }
           // return response()->json(["message" => "Registration successfully", "data" => $user], SUCCESS);
        }
        else
        {
            return response()->json(["message" => "Unable to proceed your request,please try later."], BAD_REQUEST);
        }
    }
    
      public function verify(Request $request,$verify_email_token){
         $user = User::where(['email_verify'=>$verify_email_token])->first();
        if($user) {
            $user->email_verify = null;
            $user->emailStatus = 1;
            $user->update();
            $title = "Email verified";
            $message = "Your email has been verified successfully. You may now login.";
            $type = "success";
            return view('email.feedback', compact('title', 'message', 'type'));
        }else {
            $title = "Invalid link";
            $message = "Your verification link is either expired or invalid.";
            $type = "danger";
            return view('email.feedback', compact('title', 'message', 'type'));
        }
    }
    public function checkRefferalCode(Request $request){
        $user = Auth::user();
        $refferal_code = $request->input('refferal_code');
        $check_refferal_code = User::where(['refferal_code'=>$refferal_code])
            ->first();
       if(!empty($check_refferal_code)){
        return response()->json(["message" => "Refferal code get successfully.", "data" => $user], SUCCESS);
       }else{
           return response()->json(["message" => "Refferal code are not valid."], BAD_REQUEST); 
       }
    }
    public function login(Request $request) {
        $validator = Validator::make($request->all() , ['email' => 'required', 'password' => 'required', 'device_type' => 'required', 'device_id' => 'required',

        ]);

        if($validator->fails()) {
            return response()
                ->json(["message" => $validator->errors()
                ->first() ]);
        }

        $request->user_type = 'B';

        $checkUserExist = User::where(['email' => $request->email, 'user_type' => "B", 'is_deleted' => "0"])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->status == "2") {
                http_response_code(403);
                echo json_encode(['result' => 'Failure', 'message' => 'Unlucky you, anyhow please contact us. your profile is...you know.']);
                exit;
            }
            
               if ($checkUserExist->emailStatus == 0) {
                http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Please verify your account.'],BAD_REQUEST);
                exit;
            }
            
           
            if(!Hash::check($request->password, $checkUserExist->password)) {
                return response()
                    ->json(["message" => "Please enter your valid email address or password."], BAD_REQUEST);
            }
        } else {
            return response()
                ->json(["message" => "Email address does not exist."], BAD_REQUEST);
        }

        if(Auth::attempt($request->only("email", "password", "user_type"))) {
            $User = User::find(Auth::id());
            $User->device_id = $request->device_id;
            $User->device_type = $request->device_type;
            $User->save();
            $User->token = $User->createToken('*')->accessToken;

            return response()
                ->json(["message" => "Login successfully.", "data" => $User]);
        } else {
            return response()->json(["message" => "Invalid credentials."], BAD_REQUEST);
        }
    }

    public function logout(Request $request) {
        $value = $request->bearerToken();
        $user_id = Auth::id();
         $checkUserExist = User::where(['id' => $user_id])->first();
        if ($checkUserExist->type == "I") {
            $logoutUser = User::where('id',$user_id)->update(['device_type'=>"",'device_id'=>""]);
        $id = (new Parser())->parse($value)->getHeader('jti');
        DB::table('oauth_access_tokens')
            ->where('id', $id)->update(['revoked' => true]);
        
        return response()
            ->json(['message' => "Successfully logout."], SUCCESS);
         }else{
            if ($checkUserExist->status == "2" && $checkUserExist->type == "B" ) {
                http_response_code(403);
                echo json_encode(['result' => 'Failure', 'message' => 'Unlucky you, anyhow please contact us. your profile is...you know.']);
                exit;
            }
            $logoutUser = User::where('id',$user_id)->update(['device_type'=>"",'device_id'=>""]);
        $id = (new Parser())->parse($value)->getHeader('jti');
        DB::table('oauth_access_tokens')
            ->where('id', $id)->update(['revoked' => true]);
        
        return response()
            ->json(['message' => "Successfully logout."], SUCCESS);
         }
       
    }

    public function changePassword(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
             if ($checkUserExist->status == "2" && $checkUserExist->user_type == "B" ) {
                 http_response_code(403);
                echo json_encode(['result' => 'Failure', 'message' => 'Unlucky you, anyhow please contact us. your profile is...you know.']);
                exit;
            }
        }
        $User = User::where('id', $user_id)->first();
        if ($User) {
            $Validator = Validator::make($request->all() , ["current_password" => "required", "new_password" => "required|min:6", "confirm_password" => "required|same:new_password"]);

            if ($Validator->fails()) {
                return response()
                    ->json(['message' => $Validator->errors()
                    ->first() ], BAD_REQUEST);
            }

            $curPass = $request->current_password;
            $newPass = $request->new_password;
            if (Hash::check($curPass, $User->password)){
                $User->password = Hash::make($newPass);
                $User->save();
                return response()
                    ->json(['message' => "Password updated successfully."], SUCCESS);
            } else {
                return response()
                    ->json(['message' => "current password doesn't match."], BAD_REQUEST);
            }
        } else {
            return response()
                ->json(['message' => "Social registered user can't change password."], BAD_REQUEST);
        }
    }

    public function updateProfile(Request $request)
    {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['profile' => "image|mimes:jpeg,jpg,png,gif|max:5120", 'name' => "max:255"], ['business_detail' => 'nullable|max:1500']);

        if ($Validator->fails())
        {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }

        /*if (($phone = $request->phone_number))
        {
            if (strlen($phone) < 8 || strlen($phone) > 11)
            {
                return response()->json(["message" => "Phone number should be between 8 to 11 digits."], BAD_REQUEST);
            }
        }*/

        $input = $request->all();
        if (array_key_exists("password", $input))
        {
            unset($input["password"]);
        }

        /*  $exists = User::where("email", $input["email"])->where("id", "!=", $user_id)->exists();
        
        if ($exists)
        {
            return response()->json(['message' => "Email address already taken"], BAD_REQUEST);
        }*/
        $ownerName = $request->input('owner_name');
        if (empty($ownerName))
        {
            $ownerName = "";
        }
        $businessDetail = $request->input('business_detail');
        if (empty($businessDetail))
        {
            $businessDetail = "";
        }
        $getBusinessDetail = BusinessDetail::where('user_id', $user_id)->first();
        if (!empty($getBusinessDetail))
        {

            $updateBusinessDetail = BusinessDetail::where('user_id', $user_id)->update(['owner_name' => $ownerName, 'business_detail' => $businessDetail, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') ]);
        }
        else
        {
            $insertBusinessDetail = BusinessDetail::insert(['owner_name' => $ownerName, 'business_detail' => $businessDetail, 'user_id' => $user_id, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') ]);
        }
        $input["profile"] = $this->uploadImage($request);

        $User = User::find($user_id)->fill($input)->save();

        if ($User)
        {
            return response()->json(['message' => "Profile details updated successfully."], SUCCESS);
        }
        else
        {
            return response()
                ->json(['message' => "Failed to update profile."], BAD_REQUEST);
        }
    }

    public function get_random_string($length = 10)
    {
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwyz1234567890";
        return substr(str_shuffle($str) , 0, $length);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all() , ['email' => 'required', ]);
        if ($validator->fails())
        {
            return $this->responseWithError($validator->errors()
                ->first());

        }
        $checkUserExist = User::where(['email' => $request->email,  'is_deleted' => "0"])
            ->first();
        if (!empty($checkUserExist))
        {
            if ($checkUserExist->status == "2")
            {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Unlucky you, anyhow please contact us. your profile is...you know.']);
                exit;
            }
            $resetPasswordToken = str_random(40);
            $url = url('user/reset-password/' . $resetPasswordToken);
            $updateData = User::where('id', $checkUserExist->id)
                ->update(['reset_password_token' => $resetPasswordToken, 'updated_at' => Date('Y-m-d H:i:s') ]);
            try
            {
                $user_data = User::where('id', $checkUserExist->id)
                    ->first();
                Mail::send('admin/email_forget', ['url' => $url, 'user_data' => $user_data], function ($m) use ($user_data)
                {
                    $m->from('info@twiva.co.ke', 'Twiva');
                    $m->to($user_data->email, 'App User');
                    $m->subject('Forgot Password link');
                });
                return response()
                    ->json(['message' => "A reset password link has been sent to your register email address, Please check!."], SUCCESS);
            }
            catch(\Exception $e)
            {
                return response()->json('Oops Something wrong! ' . $e->getMessage());
            }

        }
        else
        {
            return response()
                ->json(['message' => "Email address is not registered with us."], BAD_REQUEST);
        }
    }

    public function resetPassword(Request $request, $token)
    {
        $checkToken = user::where(['reset_password_token' => $token])->first();
        if (!empty($checkToken))
        {
            $current_time = Carbon::now();
            $startTime = Carbon::parse($current_time);
            $finishTime = Carbon::parse($checkToken->updated_at);
            $difference = $finishTime->diffInMinutes($startTime);
            if ($difference > 9)
            {
                return redirect(url("admin/reset-password-invalid"));
            }

            if ($request->isMethod('post'))
            {
                $token = $request->reset_token;

                $message = ['password.required' => 'Please enter password.', 'password.min' => 'Password must be at least 6 characters.', 'password_confirmation.required' => "Please enter confirm password.", 'password_confirmation.min' => "Confirm Password must be at least 6 characters.", 'password_confirmation.same' => "Password and confirm password does not match."];
                $this->validate($request, ['password' => 'required|min:6', 'password_confirmation' => 'required|same:password|min:6', ], $message);

                $updateData = User::where('id', $checkToken->id)
                    ->update(['password' => Hash::make($request->input('password')) , 'updated_at' => Date('Y-m-d H:i:s') , 'reset_password_token' => '', 'reset_password_token' => '']);

                return redirect(url("admin/password-reset-success"));

            }
            else
            {
                return view('admin.reset-password');
            }

        }
        else
        {
            return redirect(url("admin/reset-password-invalid"));
        }
    }
    public function userDetail()
    {
        $user = Auth::user();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $mpesaPhoneNumber = MpesaPhoneNumber::select('phone_number')->where('user_id', $user->id)
            ->first();
        $user->mpesaPhoneNumber = $mpesaPhoneNumber;
        return response()->json(["message" => "User detail get successfully.", "data" => $user], SUCCESS);
    }

    public function createPost(Request $request)
    {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['media.*' => 'present|file|mimetypes:video/quicktime,video/mp4,video/ogg,image/jpeg,image/jpg,image/png,image/gif|max:500000', 'post_name' => "required", 'description' => 'required|max:3000',
        // 'post_type' => 'required',
        'price_per_post' => 'required', 'what_to_include' => 'required|max:1500', 'what_not_to_include' => 'required|max:1500',

        ]);

        if ($Validator->fails())
        {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        //$checkUserExist = User::where(['id' => $user_id, 'user_type' => "B", 'is_deleted' => "0"])->first();
        //if ($checkUserExist->emailStatus == 0) {
              //   http_response_code(400);
               // echo json_encode(['result' => 'Failure', 'message' => 'Please verify your account'],BAD_REQUEST);
               // exit;
           // }
        $input = $request->all();
        $input['post_name'] = $request->post_name;
        $input['post_type'] = $request->post_type;
        $input['price_per_post'] = $request->price_per_post;
        $input['description'] = $request->description;
        $input['what_to_include'] = $request->what_to_include;
        $input['what_not_to_include'] = $request->what_not_to_include;
        $input['hashtag'] = $request->hashtag;
        $input['user_id'] = $user_id;
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        /*if (array_key_exists('type', $input))
        {
            unset($input["type"]);
        }
        */
        $insert_post = Post::create($input);
        $post_id = $insert_post->id;
        // $type = $request->input('type');
        //  type 1 == image// type = 2 video
        // $type = $request->type;
        self::addImagesPost($request, $post_id);
        return response()->json(["message" => "Post created successfully.", 'post_id' => $post_id]);

    }

    public function createGig(Request $request)
    {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , [
        // "media"=> "required",
        // 'media' => 'array|max_uploaded_file_size:3000',
        'media.*' => 'present|file|mimetypes:video/quicktime,video/mp4,video/ogg,image/jpeg,image/jpg,image/png,image/gif|max:500000', "gig_name" => "required|max:30", "description" => "required|max:3000", "gig_start_date_time" => "required", "gig_end_date_time" => "required", "what_to_do" => "required|max:1500", "price_per_gig" => "required", "location" => "required|max:200", "profile" => "image|mimes:jpeg,jpg,png,gif|max:5120", "phone_number" => "required|digits_between:8,15", "owner_name" => "required|max:30", "gig_complementry_item" => "nullable|max:1500",
        // "type"=> "required"
        ]);

        if ($Validator->fails())
        {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        //$checkUserExist = User::where(['id' => $user_id, 'user_type' => "B", 'is_deleted' => "0"])->first();
        //if ($checkUserExist->emailStatus == 0) {
               // http_response_code(400);
              //  echo json_encode(['result' => 'Failure', 'message' => 'Please verify your account'],BAD_REQUEST);
               // exit;
            //}
        $input = $request->all();
        $input['gig_name'] = $request->gig_name;
        $input['gig_start_date_time'] = $request->gig_start_date_time;
        $input['gig_end_date_time'] = $request->gig_end_date_time;
        $input['price_per_gig'] = $request->price_per_gig;
        $input['description'] = $request->description;
        $input['what_to_do'] = $request->what_to_do;
        $input['location'] = $request->location;
        $input['owner_name'] = $request->owner_name;
        $input['phone_number'] = $request->phone_number;
        $input['hashtag'] = $request->hashtag;
        $input['user_id'] = $user_id;
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        $gig_complementry_item = $request->gig_complementry_item;
        if (!empty($gig_complementry_item))
        {
            $input['gig_complementry_item'] = $gig_complementry_item;
        }
        else
        {
            $input['gig_complementry_item'] = "";
        }

        $input["profile"] = $this->uploadImage($request);

        $insert_gig = Gig::create($input);
        $gig_id = $insert_gig->id;
        self::addImagesGig($request, $gig_id);
        return response()->json(["message" => "Gig created successfully.", 'gig_id' => $gig_id]);
    }
     
     
     public function createTimeline(Request $request)
    {
        $user_id = Auth::id();
       // $checkUserExist = User::where(['id' => $user_id])->first();

//if (!empty($checkUserExist)) {
          //  if ($checkUserExist->is_deleted == "1") {
                //  http_response_code(400);
              //  echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
               // exit;
          //  }
      //  }
       // $Validator = Validator::make($request->all() , [
        // "media"=> "required",
        // 'media' => 'array|max_uploaded_file_size:3000',
       // 'media.*' => 'present|file|mimetypes:video/quicktime,video/mp4,video/ogg,image/jpeg,image/jpg,image/png,image/gif|max:500000',  "timeline_text" => "max:3000" 
       
      //  ]);

       // if ($Validator->fails())
        //{
           // return response()
              //  ->json(['message' => $Validator->errors()
               // ->first() ], BAD_REQUEST);
       // }
        //$input = $request->all();
       // $input['timeline_text'] = $request->timeline_text;
       // $input['user_id'] = $user_id;
       // $input['created_at'] = date('Y-m-d H:i:s');
        //$input['updated_at'] = date('Y-m-d H:i:s');
        // $insert_time_line = Timeline::create($input);
        // $timeline_id = $insert_time_line->id;
        //self::addTimelineImages($request, $timeline_id);
        return response()->json(["message" => "Unlucky you, anyhow please contact us. your profile is...you knowssss."]);
    }
    
        public function getTimeline(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
             if ($checkUserExist->status == "2")
            {
                $data = [];
                http_response_code(200);
                echo json_encode(['message' => 'sucess', 'data' =>$data ]);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , [

        ]);
        if ($Validator->fails())
        {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
    
        $getTimelineList = Timeline::with("timelineImages","likes")->orderby('id','DESC')->get();
         foreach($getTimelineList as $timeline){
          $user = Admin::select('name')->where('id',1)->first();
          $user->profile = url('public/admin/production/images/twiva.png');
          $timeline->user = $user;
         }
        return response()
            ->json(["message" => "Timeline list successfully.", 'data' => $getTimelineList]);

    }
    
    public function TimelineLike(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
             if ($checkUserExist->status == "2")
            {
                 $data = [];
                http_response_code(200);
                echo json_encode(['message' => 'sucess', 'data' =>$data ]);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , [

        ]);
        if ($Validator->fails())
        {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
        $getTimelineLikeList = TimelineLikes::where(['user_id'=>$user_id,'timeline_id'=>$request->timeline_id])->first();
        if(!empty($getTimelineLikeList)){
         if($getTimelineLikeList->like_status ==1){  
        $updateTimelineLikeList = TimelineLikes::where(['user_id'=>$user_id,'timeline_id'=>$request->timeline_id])->update(['like_status'=> 0]);
        $getTimelist =TimelineLikes::where(['timeline_id'=>$request->timeline_id,'user_id'=>$user_id])->first(); 
         }
         if($getTimelineLikeList->like_status ==0){
          $updateTimelineLikeList = TimelineLikes::where(['user_id'=>$user_id,'timeline_id'=>$request->timeline_id])->update(['like_status'=> 1]);
         $getTimelist =TimelineLikes::where(['timeline_id'=>$request->timeline_id,'user_id'=>$user_id])->first(); 
         }           
        }else{
        
        $insertTimelineLikeList = TimelineLikes::insertGetId(['user_id'=>$user_id,'timeline_id'=>$request->timeline_id,'like_status'=>1]);
          $getTimelist =TimelineLikes::where(['id'=>$insertTimelineLikeList,'user_id'=>$user_id])->first();         
        }
        
        return response()
            ->json(["message" => "Timeline like list successfully.", 'data' => $getTimelist]);

    }
    public static function uploadImage_control($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false)
    {
        /* user can send single or multiple image in first argument can accept request and image path also*/

        /* all images are saving in storage folder which is root */

        $return_data = "";
        $profile = null;
        if ($multiple)
        {
            $profile = $request_profile;
        }
        else if ($request_profile->hasFile("media"))
        {
            $profile = $request_profile->file("media");
        }

        if ($profile)
        {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalName();
            $file_name = uniqid() . "_" . $file_name;
            $file_name = str_replace(array(
                " ",
                "(",
                ")"
            ) , "", $file_name);
            if (!file_exists($path))
            {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            $return_data = $file_name;
        }

        return $return_data;
    }
   
     private static function addTimelineImages(Request $request, $timeline_id)
    {

        $is_inserted = 0;
        if ($request->hasFile("media"))
        {
            $images = $request->file("media");
            // $thumbnail = $request->file("thumbnail");
            $count = count($images);
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/timeline_images", 1);

                    $create_array = ["timeline_id" => $timeline_id, "media" => $image_name];
                    
                    TimelineImages::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0)
        {
            return response()->json(["message" => "Gig image saved successfully."]);
        }
    }

    private static function addImagesGig(Request $request, $gig_id)
    {

        $is_inserted = 0;
        if ($request->hasFile("media"))
        {
            $images = $request->file("media");
            // $thumbnail = $request->file("thumbnail");
            $count = count($images);
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/gig_images", 1);

                    $create_array = ["gig_id" => $gig_id, "media" => $image_name];
                    /* if ($type == 2 && $thumbnail)
                    {
                    /* write move file code
                    
                        $create_array["thumbnail"] = self::uploadImage_control($thumbnail[$i], "/app/public/uploads/images/business_images/gig_images", 1);
                    }*/
                    GigMedia::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0)
        {
            return response()->json(["message" => "Gig image saved successfully."]);
        }
    }

    private static function addImagesPost(Request $request, $post_id)
    {

        $is_inserted = 0;
        if ($request->hasFile("media"))
        {
            $images = $request->file("media");
            //$thumbnail = $request->file("thumbnail");
            $count = count($images);
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/post_images", 1);

                    $create_array = ["post_id" => $post_id, "media" => $image_name];
                    /* if ($type == 2 && $thumbnail)
                    {
                        $create_array["thumbnail"] = self::uploadImage_control($thumbnail[$i], "/app/public/uploads/images/business_images/post_images", 1);
                    }*/
                    PostImage::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0)
        {
            return response()->json(["message" => "Post image saved successfully."]);
        }
    }

    private static function createPostImages(Request $request, $post_id)
    {
        $is_inserted = 0;
        if ($request->hasFile("media"))
        {
            $images = $request->file("media");
            $thumbnail = $request->file("thumbnail");
            $count = count($images);
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/post_detail_image", 1);

                    $create_array = ["post_detail_id" => $post_id, "media" => $image_name];
                    if ($thumbnail)
                    {
                        $create_array["thumbnail"] = self::uploadImage_control($thumbnail[$i], "/app/public/uploads/images/business_images/post_detail_image", 1);
                    }
                    PostDetailsMedia::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0)
        {
            return response()->json(["message" => "Post image saved successfully."]);
        }
    }

    public function setGigPrice(Request $request)
    {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['price' => 'required', 'gig_id' => 'required', 'infulancer_id' => 'required', 'complementry_item' => 'max:200', 'total_amount' => 'nullable',

        ]);

        if ($Validator->fails())
        {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        //new comment 27-2-2020
        //if (!empty($request->input('total_amount')))
        //{
           // $getMpesaPhoneNumber = MpesaPhoneNumber::where(['user_id' => $user_id])->first();
           /// if (empty($getMpesaPhoneNumber))
           // {
           ///     return response()->json(["message" => "Please register your Mpesa phone number first."], BAD_REQUEST);
           // }
       // }

        $input = $request->all();
        $price = $request->price;
        $complementry_item = $request->complementry_item;
        $gig_id = $request->gig_id;
        $infulancer_id = $request->infulancer_id;
        //1 pending 2=>accept,3 reject
        $status = 1;
        $prices = explode(",", $price);
        $complementryItem = explode(",", $complementry_item);
        $infulancerId = explode(",", $infulancer_id);
        $infulancer_count = count($infulancerId);
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name;
        for ($i = 0;$i < $infulancer_count;$i++)
        {
            $set_gig_price = ['user_id' => $user_id, 'complementry_item' => @$complementryItem[$i], 'gig_id' => $gig_id, 'infulancer_id' => @$infulancerId[$i], 'price' => @$prices[$i], 'status' => @1, 'updated_at' => date('Y-m-d H:i:s') , 'created_at' => date('Y-m-d H:i:s') , ];
            GigPrice::insert($set_gig_price);
            GigUser::insert(['user_id' => @$infulancerId[$i], 'gig_id' => $gig_id, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') ]);

            $CommandID = "CustomerPayBillOnline";
            $Amount = @$prices[$i];
            $MSISDN = "0711650653";
            $BillRefNumber = "test";
            $ShortCode = "600762";
           $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
            $getAdminId = Admin::first();
            $insertAdminAmount = AdminPayment::insert(['user_id' => $user_id, 'infulencer_id' => @$infulancerId[$i], 'amount' => @$prices[$i], 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $gig_id, 'type' => 'gig']);

            $gig_price_id = $gig_id;
            //notifiaction code is pending
            $offer_price_data = GigPrice::where(['id' => $gig_price_id])->first();
            // $message = $name,. .,"invited you to a new Post";
            $message = $name . " invited you to a new Gig";
            $notificationMessage = "invited you to a new Gig";
            $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId[$i], 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$gig_id ]);
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $gig_id
            );
            
            $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'gig invitation','id'=>$gig_id);
                            
            $getInfulancerUser = User::where(['id' => $infulancerId[$i], 'status' => '1', 'is_deleted' => '0'])->first();
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
            {
                if ($getInfulancerUser->device_type == '2')
                {
                    $this->send_android_notification($getInfulancerUser->device_id,"invited you to a new Gig","gig invited",$message_new);
                }
                else
                {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "invited you to a new Gig", $notifyMessage);
                }
            }

        }

        return response()->json(["message" => "Gig offer price set successfully."]);

    }

    public function setPostPrice(Request $request)
    {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }

        $Validator = Validator::make($request->all() , ['price' => 'required', 'post_id' => 'required', 'infulancer_id' => 'required', 'complementry_item' => 'max:200', 'total_amount' => 'nullable',

        ]);

        if ($Validator->fails())
        {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        //new comment 27-2-2020
        //if (!empty($request->input('total_amount')))
        //{
          //  $getMpesaPhoneNumber = MpesaPhoneNumber::where(['user_id' => $user_id])->first();
           // if (empty($getMpesaPhoneNumber))
           // {
             //   return response()->json(["message" => "Please register your Mpesa phone number first."], BAD_REQUEST);
           // }
       // }
        $input = $request->all();
        $price = $request->price;
        $complementry_item = $request->complementry_item;
        $post_id = $request->post_id;
        $infulancer_id = $request->infulancer_id;
        $date = date('Y-m-d H:i:s');
        //1 pending 2=>accept,3 reject
        $status = 1;
        $prices = explode(",", $price);
        $complementryItem = explode(",", $complementry_item);
        $infulancerId = explode(",", $infulancer_id);
        $infulancer_count = count($infulancerId);
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name;
        for ($i = 0;$i < $infulancer_count;$i++)
        {
            $set_post_price = ['user_id' => $user_id, 'complementry_item' => @$complementryItem[$i], 'post_id' => $post_id, 'infulancer_id' => @$infulancerId[$i], 'price' => @$prices[$i], 'status' => @1, 'updated_at' => date('Y-m-d H:i:s') , 'created_at' => date('Y-m-d H:i:s') ];
            PostPrice::insert($set_post_price);
            PostUser::insert(['user_id' => @$infulancerId[$i], 'post_id' => $post_id, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') ]);

           $CommandID = "CustomerPayBillOnline";
            $Amount = @$prices[$i];
            $MSISDN = "0711650653";
            $BillRefNumber = "test";
            $ShortCode = "600762";
           $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
            $getAdminId = Admin::first();
            $insertAdminAmount = AdminPayment::insert(['user_id' => $user_id, 'infulencer_id' => @$infulancerId[$i], 'amount' => @$prices[$i], 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $post_id, 'type' => 'post']);

            $post_price_id = $post_id;

            ///notification code is pending
            $offer_price_data = PostPrice::where(['post_id' => $post_price_id, 'infulancer_id' => $infulancerId[$i]])->first();
            $message = $name . " invited you to a new Post";
            $notificationMessage =  "invited you to a new Post";
            $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId[$i], 'message' => $message, 'created_at' => $date, 'updated_at' => $date,'type'=>'post','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$post_id]);
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $post_id
            );
            $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'invited','id'=>$post_id);
            $getInfulancerUser = User::where(['id' => $infulancerId[$i], 'status' => '1', 'is_deleted' => '0'])->first();
               
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
            {
                if ($getInfulancerUser->device_type == '2')
                {
                    $this->send_android_notification($getInfulancerUser->device_id, "invited you to a new Post","invited", $message_new);
                }
                else
                {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "invited you to a new Post", $notifyMessage);
                }
            }

        }

        // return  $b2cTransaction;
        return response()->json(["message" => "Post offer price set successfully."]);

    }
    public function viewMessageResetPassword()
    {
        $title = "Password Reset Success";
        $message = "Password has been reset successfully.";
        $type = "success";
        return view('admin.feedback', compact('title', 'message', 'type', 'link'));
    }

    public function resetPasswordInvalid()
    {
        $title = "Invalid link";
        $message = "The forgot password link you clicked is invalid or expired.";
        $type = "danger";
        return view('admin.feedback', compact('title', 'message', 'type'));
    }
    
    

  public function acceptPostList(Request $request)
    {
        
        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
        ->where("user_id",$user)
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }

        $post_data = Post::with(["user", "Images", "postUserComplete" => function($query){
        return $query->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")->groupBy("post_users.id");
        }])
        ->select("posts.*", DB::raw("(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where (( post_users.status = 5) and post_prices.status = 1) and post_users.post_id = posts.id) as d"))
            ->orderBy("posts.id", "desc")
            ->whereIn("posts.id",$j)
            ->where('posts.user_id',$user)
            //->havingRaw("d > 0")
            ->get();

        return response()
            ->json(["message" => "Accept post details get successfully.", "data" => $post_data]);
    }
   

    
 
   public function acceptGigList(Request $request)
    {


        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , []);

          $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
        ->where("user_id",$user)
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }
         $gigData = Gig::with(["user", "Images", "gigUserComplete" => function($query){
            return $query->select(DB::raw(" distinct gig_users.id "),"gig_users.*","gig_prices.price","gig_prices.complementry_item")->groupBy("gig_users.id");
              }])
        ->select("gigs.*", DB::raw("(select group_concat(distinct gig_users.id) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id join gigs on gigs.id = gig_users.gig_id where (( gig_users.status = 4) and gig_prices.status = 1) and gig_users.gig_id = gigs.id) as d"))
            ->orderBy("gigs.id", "desc")
            ->whereIn("gigs.id",$j)
            ->where('gigs.user_id',$user)
            ->get();

        
    return response()
            ->json(["message" => "Accept gig details get successfully.", "data" => $gigData]);
     }



    public function completedPostList(Request $request)
    {
        $current_route = Route::getFacadeRoot()->current()
            ->uri();

        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        /*$Validator = Validator::make($request->all(),[
         ]);*/

        if ($current_route == "api/v1/business-completed-post-list")
        {
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $message = "complete post details get successfully.";
        }
        else
        {
            $where = "((post_users.status = 0 or post_users.status = 2) and (post_prices.status = 0 or post_prices.status = 2))";
            $message = "Reject post details get successfully.";
        }

       $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
        ->where("user_id",$user)
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return str_replace(",0","",$e);
               
            },$get_ids);
        }

        /*
            select * from `post_users` inner join `post_prices` on `post_users`.`post_id` = `post_prices`.`post_id` where `post_users`.`post_id` in (20, 48) and (post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id) and `dd` is null
            

        */


        $post_data = Post::with(["user", "Images", "postUserCompleted" => function ($query) use ($where,$user)
        {
            return $query->whereRaw($where);
        }
       ])
        ->select("posts.*", DB::raw("(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where (( post_users.status = 5) and post_prices.status = 1) and post_users.post_id = posts.id) as d"))
            ->orderBy("posts.id", "desc")
            ->whereIn("posts.id",$j)
            ->where('posts.user_id', $user)
            ->havingRaw("d > 0")
            ->get();


        return response()
            ->json(["message" => $message, "data" => $post_data]);
    }
       
 
 public function completedGigList(Request $request)
    {
        $current_route = Route::getFacadeRoot()->current()
            ->uri();

        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        if ($current_route == "api/v1/business-completed-gig-list")
        {
            $where = "(gig_users.status = 4 and gig_prices.infulancer_id = gig_users.user_id)";
            $message = "complete gig details get successfully.";
        }
        else
        {
            $where = "((gig_users.status = 0 or gig_users.status = 2) and (gig_prices.status = 0 or gig_prices.status = 2))";
            $message = "Reject post details get successfully.";
        }

        $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
        ->where("user_id",$user)
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return str_replace(",0","",$e);
               
            },$get_ids);
        }

        $gigData = Gig::with(["user", "Images", "gigUserCompleted" => function ($query) use ($where)
        {
            return $query->whereRaw($where);
        }
       ])
        ->select("gigs.*", DB::raw("(select group_concat(distinct gig_users.id) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id join gigs on gigs.id = gig_users.gig_id where (( gig_users.status = 4) and gig_prices.status = 1) and gig_users.gig_id = gigs.id) as d"))
            ->orderBy("gigs.id", "desc")
            ->whereIn("gigs.id",$j)
            ->where('gigs.user_id', $user)
            ->havingRaw("d > 0")
            ->get();
      
        return response()
            ->json(["message" => $message, "data" => $gigData]);
    }
    public function RejectPostList(Request $request, $id)
    {

        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , [
        // "infulancer_id"    => "required|numeric",
        ]);

        $rejectPostDetails = PostUser::where(['post_users.post_id' => $id])->whereIn('post_users.status', [6, 2])
            ->with("user")
            ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
            ->get();
        return response()
            ->json(["message" => "Reject post Details get successfully.", "data" => $rejectPostDetails]);
    }

    public function RejectGigList(Request $request, $id)
    {

        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , [
        // "infulancer_id"    => "required|numeric",
        ]);

        $rejectGigDetails = GigUser::where(['gig_users.gig_id' => $id])->whereIn('gig_users.status', [5, 2])
            ->with("user")
        /* ->select("gig_users.*","gig_prices.price")
                    ->join("gig_prices","gig_users.gig_id","=","gig_prices.gig_id")
                    ->get();*/
            ->select("gig_users.*", DB::raw("(select price from gig_prices where gig_id = gig_users.gig_id and infulancer_id  = gig_users.user_id) as price"))
            ->get();
        return response()
            ->json(["message" => "Rejected gig Details get successfully.", "data" => $rejectGigDetails]);
    }

    public function resendPostPrice(Request $request)
    {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['price' => 'required', 'post_id' => 'required', 'infulancer_id' => 'required',

        ]);

        if ($Validator->fails())
        {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        $input = $request->all();
        $price = $request->price;
        $post_id = $request->post_id;
        $infulancerId = $request->infulancer_id;
        $status = 1;
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name;
        PostPrice::where(['infulancer_id' => $infulancerId, 'post_id' => $post_id, 'user_id' => $user_id])->update(['price' => $price, 'status' => $status, 'is_resend' => 1]);
        PostUser::where(['user_id' => $infulancerId, 'post_id' => $post_id])->update(['status' => 6, 'is_resend' => 1]);
        $CommandID = "CustomerPayBillOnline";
            $Amount = $price;
            $MSISDN = "0711650653";
            $BillRefNumber = "test";
            $ShortCode = "600762";
           $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
        $getAdminId = Admin::first();
        $insertAdminAmount = AdminPayment::insert(['user_id' => $user_id, 'infulencer_id' => $infulancerId, 'amount' => $price, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $post_id, 'type' => 'post']);

        //notifiaction code is pending
        $offerPriceData = PostPrice::where(['post_id' => $post_id, 'infulancer_id' => $infulancerId])->first();

        $message = $name . " sent a better post offer";
        $notificationMessage = "sent a better post offer";
        $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$post_id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $post_id
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'better post offer','id'=>$post_id);
        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
        {
            if ($getInfulancerUser->device_type == '2')
            {
                $this->send_android_notification($getInfulancerUser->device_id, "sent a better post offer","better post offer", $message_new);
            }
            else
            {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "sent a better post offer", $notifyMessage);
            }
        }
        return response()->json(["message" => "Post Price resent successfully."]);
    }

    public function resendGigPrice(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['price' => 'required', 'gig_id' => 'required', 'infulancer_id' => 'required',

        ]);

        if ($Validator->fails()) {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        $input = $request->all();
        $price = $request->price;
        $gig_id = $request->gig_id;
        $infulancerId = $request->infulancer_id;
        $status = 1;
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name;
        GigPrice::where(['infulancer_id' => $infulancerId, 'gig_id' => $gig_id, 'user_id' => $user_id])->update(['price' => $price, 'status' => $status, 'is_resend' => 1]);
        GigUser::where(['user_id' => $infulancerId, 'gig_id' => $gig_id])->update(['status' => 5, 'is_resend' => 1]);

           $CommandID = "CustomerPayBillOnline";
            $Amount = $price;
            $MSISDN = "0711650653";
            $BillRefNumber = "test";
            $ShortCode = "600762";
           $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
        $getAdminId = Admin::first();
        $insertAdminAmount = AdminPayment::insert(['user_id' => $user_id, 'infulencer_id' => $infulancerId, 'amount' => $price, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $gig_id, 'type' => 'gig']);
        //notifiaction code is pending
        $offerPriceData = GigPrice::where(['gig_id' => $gig_id, 'infulancer_id' => $infulancerId])->first();
        $message = $name . " sent a better gig offer";
        $notificationMessage = "sent a better gig offer";
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $gig_id
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'better gig offer','id'=>$gig_id);
        $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$gig_id ]);
        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
            if ($getInfulancerUser->device_type == '2') {
                 $this->send_android_notification($getInfulancerUser->device_id, "sent a better post offer","better gig offer", $message_new);
            } else {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "sent a better gig offer", $notifyMessage);
            }
        }
        return response()->json(["message" => "Gig Price resent successfully."]);

    }
    public function notifiactionList(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , []);
        if ($Validator->fails()) {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }

        $getNotificationList = Notification::where('user_id', $user_id)->with("otherUser")
            ->orderBy("id", "DESC")
            ->get();

        foreach ($getNotificationList as $notification) {
            if($notification->type == "post") {
                $postUserStatus = PostUser::where(['post_id' => $notification->post_id, 'user_id' => $notification->other_user_id])->first();
                if($postUserStatus) {
                    $notification->new_status = $postUserStatus->status;
                } else {
                    $notification->new_status = 0;
                }
            } else {
                $postUserStatus = GigUser::where(['gig_id' => $notification->post_id, 'user_id' => $notification->other_user_id])->first();
                if($postUserStatus) {
                    $notification->new_status = $postUserStatus->status;
                } else {
                    $notification->new_status = 0;
                }
            }
        }

        return response()
            ->json(["message" => "Notifiaction list get successfully.", "data" => $getNotificationList]);
    }

    public function getBusinessRatingList(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , [

        ]);
        if ($Validator->fails())
        {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
        $getBusinessRatingList = Rating::where('business_id', $user_id)->get();
        return response()
            ->json(["message" => "Busienss rating list successfully.", 'data' => $getBusinessRatingList]);

    }

    public function payment(Request $request) {

        /*for testing security credentials get this url
         https://developer.safaricom.co.ke/test_credentials*/

        $InitiatorName = "peter";
        $SecurityCredential = "AH/nebiHylfMZq7VwoR99FOaeNU4X8IlgY1bWl955vAqC1U56oOhGWFGNLAuY4eVigCTc47vBizT5Ch3PzkDQCtOK+zynshVOC6/2VWoYNOHJuSzPbMZD4jyFmZesbsrlvLfd8xGMyAtrOuk/gZqOg0T+QN8jakl8Cbucz1NXfyZ0n0UP1mY8ywZAcq4GIMheNS5QcF8/OSTALH7JzTekgNNzj80r4WmfMEDMzTXgYskQQxt/Oa1xze/2YH4yJSD1P0DVfxuhvrqheIABkAEuXe+/5qMEAKB1ciMReWY0vVknHGl6EwW4Z2YEesEUVgYim/kdge8Oi06hn3/5skIkQ==";
        $CommandID = "BusinessPayment";
        $Amount = "200";
        // $PartyA = $request->input('partyA'); //,"174379");
        $PartyA = "600762";
        $PartyB = "254722000000";
        $Remarks = "okay this better work";
        $QueueTimeOutURL = "http://www.google.com";
        $ResultURL = "http://www.google.com";
        $Occasion = "testing";

        $b2cTransaction = Mpesa::b2c($InitiatorName, $SecurityCredential, $CommandID, $Amount, $PartyA, $PartyB, $Remarks, $QueueTimeOutURL, $ResultURL, $Occasion);
        return $b2cTransaction;


    }

    public function AcceptPostDetail(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['post_id' => 'required',
         'infulancer_id' => 'required',
          //'image'=>'image|mimes:jpeg,jpg,png,gif|max:5120',
         'image'=>'required',
          'text'=>'required|max:3000']);

        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
        $postId = $request->input('post_id');
        $text = $request->input('text');
        $infulancerId = $request->input('infulancer_id');
        $imageName = $request->input('image');
       // $images = $request->file('image');
       // $imageName = $this->uploadImage($images, "/app/public/uploads/images/share_images", 1);
        $insertShareData = ShareData::insert(['infulancer_id'=>$infulancerId,'post_id'=>$postId,'user_id'=>$user_id,'image'=>$imageName,'text'=>$text,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);

        // $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->update(['status' => '3', 'updated_at' => date('Y-m-d H:i:s') ]);


        $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();


        $getBusienssName = User::where(['id' => $user_id])->first();
        if (!empty($getBusienssName->name)) {
            $name = $getBusienssName->name;
        } else {
            $name = "dummy name";
        }

        $message = $name . " approved your Post";
        $notificationMessage =  "Your post has been approved";

        $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$postId ]);

        $updatePostUserStatus->status = 3;
        $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
        $updatePostUserStatus->update();
        
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $postId
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'approved','id'=>$postId);
        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
            if ($getInfulancerUser->device_type == '2') {
                $this->send_android_notification($getInfulancerUser->device_id,'Your post has been approved', "approved",$message_new);
            } else {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'Your post has been approved', $notifyMessage);
            }
        }

        return response()->json(["message" => "Post Detail Accepted successfully."]);

    }

    public function RejectPostDetail(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['post_id' => 'required', 'infulancer_id' => 'required', 'text' => 'required',
        // 'post_detail_id'   =>'required',
        ]);
        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }

        $postId = $request->input('post_id');
        $infulancerId = $request->input('infulancer_id');
        //$postDetailId = $request->input('post_detail_id');
        $text = $request->input('text');
        // $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->update(['status' => 4, 'reject_post_reason' => $text, 'is_submit' => 0]);

        $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();


        $getBusienssName = User::where(['id' => $user_id])->first();
        if (!empty($getBusienssName->name)) {
            $name = $getBusienssName->name;
        } else {
            $name = "dummy name";
        }
        $message = $name . "  needs edits of your Post";
        $notificationMessage =  "requested edits of your post";
        $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$postId ]);

        $updatePostUserStatus->status = 4;
        $updatePostUserStatus->reject_post_reason = $text;
        $updatePostUserStatus->is_submit = 0;
        $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
        $updatePostUserStatus->update();

        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $postId
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'requested edits','id'=>$postId);
        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
            if ($getInfulancerUser->device_type == '2') {
                $this->send_android_notification($getInfulancerUser->device_id,'requested edits of your post',"requested edits", $message_new);
            } else {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'requested edits of your post', $notifyMessage);
            }
        }
        return response()->json(["message" => "Post Detail Rejected successfully."]);

    }

    public function confirmCheckInRequest(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['gig_id' => 'required', 'infulancer_id' => 'required', ]);
        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
        $gigId = $request->input('gig_id');
        $infulancerId = $request->input('infulancer_id');

        $updatePostUserStatus = GigUser::where(['gig_id' => $gigId, 'user_id' => $infulancerId])->first();

        $getBusienssName = User::where(['id' => $user_id])->first();

        if (!empty($getBusienssName->name)) {
            $name = $getBusienssName->name;
        } else {
            $name = "dummy name";
        }

        $message = $name . "  checked you in";
        $notificationMessage =  "checked you in";
        $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$gigId]);

        $updatePostUserStatus->status = 3;
        $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
        $updatePostUserStatus->update();


        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $gigId
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'checked','id'=>$gigId);
        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
            if ($getInfulancerUser->device_type == '2') {
                $this->send_android_notification($getInfulancerUser->device_id,'checked you in', "checked" ,$message_new);
            } else {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'checked you in', $notifyMessage);
            }
        }
        return response()->json(["message" => "Gig checkin request  Accepted successfully."]);

    }

    public function updateMpesaPhoneNumber(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['phone_number' => 'required',

        ]);
        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
        $date = date('Y-m-d H:i:s');
        $phoneNumber = $request->input('phone_number');
        $getMpesaPhoneNumber = MpesaPhoneNumber::where(['user_id' => $user_id])->first();
        if (!empty($getMpesaPhoneNumber)){
            $UpdateMpesaPhoneNumber = MpesaPhoneNumber::where(['user_id' => $user_id])->update(['phone_number' => $phoneNumber, 'created_at' => $date, 'updated_at' => $date]);
        } else {
            $UpdateMpesaPhoneNumber = MpesaPhoneNumber::insert(['user_id' => $user_id, 'phone_number' => $phoneNumber, 'created_at' => $date, 'updated_at' => $date]);
        }
        return response()->json(["message" => "Mpesa phone number updated successfully."]);

    }

    public function gigReminder(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['gig_id' => 'required', 'infulancer_id' => 'required',

        ]);

        if ($Validator->fails()) {
            return response()
                ->json(['message' => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        $input = $request->all();
        $gig_id = $request->gig_id;
        $infulancerId = $request->infulancer_id;

        $getBusienssName = User::where('id', $user_id)->first();
        $name = $getBusienssName->name;
        //notifiaction code is pending
        $message = "Please respond to " . $name;
        $notificationMessage = "Gig reminder";
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $gig_id
        );
       $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'Gig reminder','id'=>$gig_id);
        $updatePostUserStatus = GigUser::where(['gig_id' => $gig_id, 'user_id' => $infulancerId])->first();

        $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$gig_id ]);

        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if(!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)){
            if ($getInfulancerUser->device_type == '2') {
                $this->send_android_notification($getInfulancerUser->device_id,'Gig reminder','Gig reminder', $message_new);
            } else {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'Gig reminder', $notifyMessage);
            }
        }
        return response()->json(["message" => "Gig reminder sent successfully."]);

    }

    public function postReminder(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['post_id' => 'required', 'infulancer_id' => 'required',

        ]);

        if ($Validator->fails()) {
            return response()->json(['message' => $Validator->errors()->first() ], BAD_REQUEST);
        }

        $input = $request->all();
        $post_id = $request->post_id;
        $infulancerId = $request->infulancer_id;
        $getBusienssName = User::where('id', $user_id)->first();
        $name = $getBusienssName->name;

        $updatePostUserStatus = PostUser::where(['post_id' => $post_id, 'user_id' => $infulancerId])->first();

        //notifiaction code is pending
        $message = "Please respond to " . $name;
        $notificationMessage = "post reminder";
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $post_id
        );
         $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'post reminder','id'=>$post_id);
        $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=> $updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$post_id]);
        $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        if(!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
            if ($getInfulancerUser->device_type == '2') {
                $this->send_android_notification($getInfulancerUser->device_id,'post reminder', 'post reminder',$message_new);
            } else {
                $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'post reminder', $notifyMessage);
            }
        }
        return response()->json(["message" => "Post reminder sent successfully."]);

    }

    public function addInfulancerRating(Request $request) {
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                  http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ["rating" => "required|numeric", "comment" => "max:1500", "infulencer_id" => "required",'type'=>'required','post_id'=>'required'

        ]);

        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }

        $getRatingData = InfulancerRating::where(['infulencer_id'=>$request->input('infulencer_id'),'type'=>$request->input('type'),'post_id'=>$request->input('post_id'),'user_id'=>$user_id])->first();

        if(!empty($getRatingData)){
             //$rating_status = 2; 
             return response()
           // ->json(["message" => "You have already rate this influencer."]);
            ->json(["message" => "Rating already submitted."]);
        }
        $insert_rating = new InfulancerRating();
        $insert_rating->user_id = $user_id;
        $insert_rating->infulencer_id = $request->input('infulencer_id');
        $insert_rating->rating = $request->input('rating');
        $insert_rating->type = $request->input('type');
        $insert_rating->post_id = $request->input('post_id');
        $insert_rating->comment = $request->input('comment');
        $insert_rating->save();

        return response()
            ->json(["message" => "Rating submitted successfully."]);
    }


    public function busienssGigDetail(Request $request){
        $user_id = Auth::id();
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                 http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['gig_id' => 'required']);
        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }

        $gigId = $request->input('gig_id');
        /*$gigData = Gig::with(["user", "Images", "gigUser" ]) 
       ->select("gigs.*", DB::raw("(select group_concat(gig_users.id) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id join gigs on gigs.id = gig_users.gig_id and gig_users.gig_id = gigs.id) as d"))
            ->orderBy("gigs.id", "desc")
            ->where('gigs.user_id', $user_id)->havingRaw("d > 0")
            ->where('gigs.id', $gigId)
            ->first();*/



        $gigData = Gig::with(["user", "Images", "gigUser" ])
                            ->orderBy("id", "desc")
                            ->whereId($gigId)
                            ->first();

        $final_data = Gig::with(["user", "Images"])->orderBy("id", "desc")->whereId($gigId)->first();
        $final_data->gig_user_completed = $gigData->gigUser;
        return response()->json(["message" => " Gig Details get successfully.", "data" => $final_data]);
    }


    public function busienssPostDetail(Request $request){
        $user = Auth::id();
        $checkUserExist = User::where(['id' => $user])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['post_id' => 'required']);
        if ($Validator->fails()) {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);

        }
        $postId = $request->input('post_id');


        $post_data = Post::with(["user", "Images", "updatePrice", "postUserCompleted"])
        ->select("posts.*", DB::raw("(select group_concat(post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id  and post_users.post_id = posts.id) as d"))
            ->orderBy("posts.id", "desc")
            ->where('posts.id',$postId)
            ->where('posts.user_id', $user)->havingRaw("d > 0")
            ->first();

        return response()->json(["message" => " Post Details get successfully.", "data" => $post_data]);
    }
    
    public function test()
    {
        
        $data = "0";
        return response()->json(["message" => "User detail get successfully.", "data" => $data], SUCCESS);
    }

    public function navSwitch(Request $request)
    {
        // Currently DB is different but we need to check by access token
        // $user_id = DB::table('oauth_access_tokens')->where('id', $request->user_token)->value('user_id');
        
        $user = User::find($request->user_id);
        if(!$user) {
            return response()->json(["message" => "User Not Found."], BAD_REQUEST);
        } 
        $temp = Auth::loginUsingId($user->id);
        TempTokens::create([
            'user_id' => $user->id,
            'data' => 'nav-switch'
        ]);
        $user['url'] = config('app.testing_url').'/business'.$request->nav_url.'?id='.$user->id;
        $user->token = $user->createToken('*')->accessToken;

        return response()->json(["message" => "User .", "data" => $user], SUCCESS);
    }

}

