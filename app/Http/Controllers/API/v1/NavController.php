<?php

namespace App\Http\Controllers\API\v1;

use App\model\User;
use App\model\TempTokens;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NavController extends Controller
{
    public function navSwitch(Request $request)
    {
        // Currently DB is different but we need to check by access token
        // $user_id = DB::table('oauth_access_tokens')->where('id', $request->user_token)->value('user_id');

        $user = User::find($request->user_id);
        if(!$user) {
            return response()->json(["message" => "User Not Found."], 400);
        }
        $temp = Auth::loginUsingId($user->id);
        TempTokens::create([
            'user_id' => $user->id,
            'data' => 'nav-switch'
        ]);
        //$user['url'] = config('app.testing_url').'/business/navChange?id='.$user->id.'&url='.$request->nav_url;
        if($request->type==2){
            $user['url'] = config('app.testing_url').'/influencers/navChange?id='.$user->id.'&url='.$request->nav_url;
            //$user['url'] = 'http://127.0.0.1:8000/influencers/navChange?id='.$user->id.'&url='.$request->nav_url;
        }else{
            $user['url'] = config('app.testing_url').'/business/navChange?id='.$user->id.'&url='.$request->nav_url;
            //$user['url'] = 'http://127.0.0.1:8000/business/navChange?id='.$user->id.'&url='.$request->nav_url;

        }
        
        $user->token = $user->createToken('*')->accessToken;
        return response()->json(["message" => "User .", "data" => $user], 200);
    }
}