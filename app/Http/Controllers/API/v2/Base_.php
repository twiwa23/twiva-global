<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use DB;
use Hash;
use Auth;
use Mail;
use Validator;
use Lcobucci\JWT\Parser;
use Illuminate\Http\Request;
use Input;

define("SUCCESS",200);
define("NO_CONTENT",204);
define("BAD_REQUEST",400);
define("UNAUTHORISED",401);
define("NOT_FOUND",404);

abstract class Base extends Controller
{

    public function uploadImage($request,$dir="/app/public/uploads/images/business"){
        $return_data = "";
        if($request->hasFile("profile")){
            $profile = $request->file("profile");
            if($profile){
                $path = storage_path().$dir;
                $file_name = $profile->getClientOriginalName();
                
                $file_name = uniqid()."_".$file_name;
                $file_name = str_replace(array(" ","(",")"),"",$file_name);
                if(!file_exists($path)){
                    mkdir(url($dir));
                }

                $profile->move($path,$file_name);
                $return_data = $file_name;
            }
         }
         return $return_data;
    }
}
