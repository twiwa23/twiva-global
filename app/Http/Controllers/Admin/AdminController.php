<?php
namespace App\Http\Controllers\Admin;

header('Cache-Control: no-store, private, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);

use Session;
use Hash;
use App\model\User;
use App\model\Admin;
use App\model\InfluencerImages;
use App\model\InterestUser;
use App\model\Interest;
use App\model\InfluencerDetail;
use App\model\InfluencerSocialDetails;
use App\model\Price;
use Auth;
use App\model\Gig;
use App\model\GigMedia;
use App\model\GigPrice;
use App\model\GigUser;
use App\model\PostUser;
use App\model\Post;
use App\model\PostImage;
use App\model\PostPrice;
use App\model\InfluencerInterests;
use App\model\InfluencerPrice;
use App\model\Payment;
use App\model\AdminPayment;
use App\model\Blog;
use App\model\Featured;
use App\model\RefferalList;
use App\model\TimelineImages;
use App\model\Timeline;
use App\model\TimelineLikes;
use App\model\SubadminPermission;
use App\model\Notification;
use App\model\Sidebar;
use App\model\UserPost;
use App\model\Category;
use App\model\CategoryUser;
use App\model\Portfolio;
use App\model\Services;
use App\model\Blogger;
use App\model\Rate;
use DB;
use Validator;
use Carbon\Carbon;
use App\model\BusinessDetail;
use Safaricom\Mpesa\Mpesa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function influencerManagement(Request $request) {
       $user = Auth::guard('admin')->user();
       $subadmin_user_id = $user->id;
        $infulancer = User::where(['user_type' => 'I','is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view('admin.influencer-Management', compact('infulancer','subadmin_user_id'));
    }

    public function editInfluencer(Request $request, $id)
    {
        $selected_interests = InfluencerInterests::selectRaw("group_concat(interest_id) as interest_id ")->where('user_id', $id)->first()->interest_id;

        if ($request->isMethod('get'))
        {
            $user = User::with(["SocialDetail", "price"])->find($id);
            $InfulancerPrice = InfluencerPrice::where('user_id', $id)->first();
            $InfulancerDetail = InfluencerDetail::where('user_id', $id)->first();

            $infulancerImageData = InfluencerImages::where('user_id', $id)->get();
            $interests = Interest::get();

            return view('admin.edit-influencer', compact('user', 'infulancerImageData', 'interests', 'selected_interests', 'InfulancerDetail', 'InfulancerPrice'));
        }

        if ($request->isMethod('post'))
        {
            $message = ['name.max' => 'name should not be greater than 20 characters', 'facebook_name.max' => 'facebook Username should not be greater than 20 characters', 'facebook_friends.max' => 'facebook friend should not be greater than 10 digits', 'twitter_name.max' => 'Twitter name should not be greater than 20 characters', 'twitter_friends.max' => 'Twitter followers should not be greater than 10 digits', 'instagram_name.max' => 'Instagram name should not be greater than 20 characters', 'instagram_friends.max' => 'Instagram followers cannot be greater than 10 digits','gig_price.max' => 'Gig price should not be  greater than 10 digits', 'plug_price.max' => 'Post price should not be greater than 10 digits','instagram_name.without_spaces'=>'instagram name should without space.','twitter_name.without_spaces'=>'twitter name should without space.','facebook_name.without_spaces'=>'facebook name should without space.','phone_number.digits_between'=>'Phone number should be 7 to 15 digits.',
            // 'gender.max'=> 'Please select gender',
            //'date.max'   => 'Please select date of birth'
            ];
             /* Validator::extend('without_spaces', function($attr, $value){
                return preg_match('/^\S*$/u', $value);
               });*/
            $this->validate($request, ['name' => 'max:30', 'facebook_name' => 'max:20', 'facebook_friends' => 'max:10', 'twitter_name' => 'max:20', 'twitter_friends' => 'max:10', 'instagram_name' => 'max:20', 'instagram_friends' => 'max:10', 'gig_price' => 'max:10', 'plug_price' => 'max:10','phone_number' =>  'nullable|digits_between:7,15',

            ], $message);
            $updateInfulancerPrice = InfluencerPrice::where('user_id', $id)->update(['gig_price' => $request->gig_price, 'plug_price' => $request->plug_price]);

            $updateInfulancer = User::where('id', $id)->update(['name' => $request->name,'phone_number'=>$request->phone_number]);
            //update social details//
            $updateInfulancerSocialDetail = InfluencerSocialDetails::where('user_id', $id)->update(['facebook_name' => $request->facebook_name, 'facebook_friends' => $request->facebook_friends, 'twitter_name' => $request->twitter_name, 'twitter_friends' => $request->twitter_friends, 'instagram_name' => $request->instagram_name, 'instagram_friends' => $request->instagram_friends]);
            if (!empty($request->gender))
            {
                $gender = $request->gender;
            }
            else
            {
                $gender = "";
            }

            if (!empty($request->date))
            {
                $dob = $request->date;
            }
            else
            {
                $dob = "";
            }
            $getInfulancerDetail = InfluencerDetail::where('user_id', $id)->first();
            if (!empty($getInfulancerDetail))
            {
                $updateInfulancerDetail = InfluencerDetail::where('user_id', $id)->update(['dob' => $dob, 'user_id' => $id, 'gender' => $gender]);
            }
            else
            {
                $updateInfulancerDetail = InfluencerDetail::insert(['dob' => $dob, 'user_id' => $id, 'gender' => $gender]);
            }
            if ($request->hasFile("image"))
            {
                $image = $request->file('image');
                $imageId = $request->imageid;
                // $count_image = count($image);
                foreach ($image as $i => $images)
                {
                    if (isset($images) && !empty($images))
                    {
                        $imageName = $this->uploadImage($images, "/app/public/uploads/images/influencer_images", 1);
                        $imageData = new InfluencerImages();
                        if (isset($imageId[$i]) && !empty($imageId[$i]))
                        {
                            $imageData = InfluencerImages::find($imageId[$i]);
                        }
                        $imageData->user_id = $id;
                        $imageData->image = $imageName;
                        $imageData->save();
                    }
                }
            }
            $selected = explode(",", $selected_interests);
            $interestsId = $request->interest_id;
            if ($interestsId)
            {
                $deletable = array_diff($selected, $interestsId);
                $insertable = array_diff($interestsId, $selected);
            }
            else
            {
                $deletable = $selected;
                $insertable = array();
            }
            if ($deletable && !empty($deletable) && count($deletable) > 0)
            {

                InfluencerInterests::where('user_id', $id)->whereIn("interest_id", $deletable)->delete();
            }
            if ($insertable && !empty($insertable) && count($insertable) > 0)
            {
                foreach ($insertable as $interestsIds)
                {
                    if (isset($interestsIds) && !empty($interestsIds))
                    {
                        $interestData = new InfluencerInterests();
                        $interestData->user_id = $id;
                        $interestData->interest_id = $interestsIds;
                        $interestData->save();
                    }
                }
            }

            Session::flash('message', 'Influencer has been updated successfully.');
            return redirect('admin/influencer-Management');

        }
    }

    public function uploadImage($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false)
    {
       
        $return_data = "";
        $profile = null;
        if ($multiple)
        {
            $profile = $request_profile;
        }
        else if ($request_profile->hasFile("image"))
        {
            $profile = $request_profile->file("image");
        }

        if ($profile)
        {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalExtension();
            $file_name = uniqid() . "." . $file_name;
            $file_name = str_replace(array(
                " ",
                "(",
                ")"
            ) , "", $file_name);
            if (!file_exists($path))
            {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            //chmod($path,0775);
            $return_data = $file_name;
        }

        return $return_data;
    }
    /*funtion */
    
    public function deleteInfluencer(Request $request)
    {
        $request->infulancer_id;
        $user = User::where('id', '=', $request->customer_id)
            ->update(['is_deleted' => '1']);
        Session::flash('message', 'Influencer has been deleted successfully.');
        return redirect('admin/influencer-Management')
            ->withInput();
    }
    
      public function viewInfluencer(Request $request, $id)
        {
             $user = User::with(["Interests", "SocialDetail", "price", "InfluencerDetail"])->find($id);
            $infulancerImageData = InfluencerImages::where('user_id', $id)->get();
            $InfulancerPrice = InfluencerPrice::where('user_id', $id)->first();
    
            $averagePostPrice = PostPrice::selectRaw('AVG(price)/count(*) as averagePrice')->where(['user_id' => $id, 'status' => 1])->first();
    
            $averageGigPrice = DB::table('gig_prices')->selectRaw('AVG(price)/count(*) as averagePrice')
                ->join('gig_users', 'gig_users.user_id', '=', 'gig_prices.infulancer_id')
                ->whereRaw('(gig_users.gig_id = gig_prices.gig_id)')
                ->where(['gig_prices.infulancer_id' => $id])->where(['gig_prices.status' => 1])
                ->where(['gig_users.user_id' => $id])->where(['gig_users.status' => 4])
                ->first();
            $averagePostPrice = DB::table('post_prices')->selectRaw('AVG(price)/count(*) as averagePrice')
                ->join('post_users', 'post_users.user_id', '=', 'post_prices.infulancer_id')
                ->whereRaw('(post_users.post_id = post_prices.post_id)')
                ->where(['post_prices.infulancer_id' => $id])->where(['post_prices.status' => 1])
                ->where(['post_users.user_id' => $id])->where(['post_users.status' => 3])
                ->first();
            return view('admin.view-influencer', compact('user', 'infulancerImageData', 'InfulancerPrice', 'averagePostPrice', 'averageGigPrice'));
        }

//    public function viewInfluencer(Request $request, $id)
//    {
//        $user = User::with(["Interests", "SocialDetail", "price", "InfluencerDetail"])->find($id);
//        $infulancerImageData = InfluencerImages::where('user_id', $id)->get();
//        $InfulancerPrice = InfluencerPrice::where('user_id', $id)->first();
//
//        $interestData = InfluencerInterests::where(['user_id' => $id])->get();
//
//
//        if(!empty($interestData)){
//          foreach ($interestData as $interests)
//        {
//            $intersetsName = Interest::where(['id' => $interests
//                ->interest_id])
//                ->first();
//            $interests->interests_name = $intersetsName->interest_name;
//
//        }
//        }
//
//
////        foreach ($interestData as $interests)
////        {
////            $intersetsName = Interest::where(['id' => $interests
////                ->interest_id])
////                ->first();
////            $interests->interestsName = $intersetsName->interest_name;
////        }
//        //return  $interestData;
//        $averagePostPrice = PostPrice::selectRaw('AVG(price)/count(*) as averagePrice')->where(['user_id' => $id, 'status' => 1])->first();
//
//        $averageGigPrice = DB::table('gig_prices')->selectRaw('AVG(price)/count(*) as averagePrice')
//            ->join('gig_users', 'gig_users.user_id', '=', 'gig_prices.infulancer_id')
//            ->whereRaw('(gig_users.gig_id = gig_prices.gig_id)')
//            ->where(['gig_prices.infulancer_id' => $id])->where(['gig_prices.status' => 1])
//            ->where(['gig_users.user_id' => $id])->where(['gig_users.status' => 4])
//            ->first();
//        $averagePostPrice = DB::table('post_prices')->selectRaw('AVG(price)/count(*) as averagePrice')
//            ->join('post_users', 'post_users.user_id', '=', 'post_prices.infulancer_id')
//            ->whereRaw('(post_users.post_id = post_prices.post_id)')
//            ->where(['post_prices.infulancer_id' => $id])->where(['post_prices.status' => 1])
//            ->where(['post_users.user_id' => $id])->where(['post_users.status' => 3])
//            ->first();
//        return view('admin.view-influencer', compact('user', 'infulancerImageData', 'interestData', 'InfulancerPrice', 'averagePostPrice', 'averageGigPrice'));
//    }
    
    public function subadminManagement(Request $request) {
         $subadmin= Admin::where(["type" =>"subadmin",'is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view('admin.subadmin-management', compact('subadmin'));
    }

    public function addsubAdmin(Request $request)
    {
       
        if ($request->isMethod('get'))
        {
            $sidebar = Sidebar::get();

            return view('admin.add-subadmin', compact('sidebar'));
        }

        if ($request->isMethod('post'))
        {
            //return $request->all();
            $message = ['name.max' => 'name should not be greater than 20 characters', 'name.required' => 'Please enter name','email.required' => 'please enter email address','email.max' => 'Email address should not be greater than 200 characters','visible_pwd.required' => 'please enter password', 'visible_pwd.max' => 'password should not be greater than  200 characters', 'phone_number.required' => 'Please enter phone number', 'phone_number.max' => 'Phone number should not be greater than 15 digits',
            
            ];
             
            $this->validate($request, ['name' => 'required|max:20', 'email' => 'required|email|unique:admins|max:200', 'visible_pwd' => 'required|max:200', 'phone_number' => 'required|max:15', 

            ], $message);
            
             $addsubadmin = Admin::insertGetId(['name' => $request->name,'phone_number'=>$request->phone_number,'email'=>$request->email,'password' => Hash::make($request->input('visible_pwd')) , 'visible_pwd' => $request->input('visible_pwd'),'type'=>"subadmin"]);
            
                 $insertable = $request->sidebar_id;
                 /*$add = $request->add;
                 if(!empty($add)){
                   $add = 1; 
                 }else{
                   $add = 0; 
                 }
                 $edit = $request->edit;
                   if(!empty($edit)){
                   $edit = 1; 
                 }else{
                   $edit = 0; 
                 }
                  $view = $request->view;
                   if(!empty($view)){
                   $edit = 1; 
                 }else{
                   $edit = 0; 
                 }
                 $delete = $request->delete;
                 if(!empty($delete)){
                   $delete = 1; 
                 }else{
                   $delete = 0; 
                 }
            if ($insertable && !empty($insertable) && count($insertable) > 0)
            {
                foreach ($insertable as $sidebarIds)
                {
                    if (isset($sidebarIds) && !empty($sidebarIds))
                    {
                        $interestData = new SubadminPermission();
                        $interestData->user_id = $addsubadmin;
                        $interestData->sidebar_id = $sidebarIds;
                        $interestData->add = $add;
                        $interestData->edit = $edit;
                        $interestData->view = $view;
                        $interestData->delete = $delete;
                        $interestData->save();
                    }
                }
            }
*/
              $all_add = $request->get("add");
                $all_edit = $request->get("edit");
                $all_view = $request->get("view");
                $all_delete = $request->get("delete");
                foreach ($insertable as $sidebarIds)
                {
                    //return $sidebarIds;
                    if (isset($sidebarIds) && !empty($sidebarIds))
                    {
                        $insert_data = [];
                        $insert_data = [
                            "user_id" => $addsubadmin,
                            "sidebar_id"   => $sidebarIds,
                            "add" => 0,
                            "edit" => 0,
                            "view" => 0,
                            "delete" => 0
                        ];

                        if(isset($all_add[$sidebarIds])){
                            $insert_data["add"] = @$all_add[$sidebarIds][0] == '1' ? 1: 0;
                        }
                        if(isset($all_edit[$sidebarIds])){

                            $insert_data["edit"] = @$all_edit[$sidebarIds][0] == "1" ? 1: 0;
                        }
                        if(isset($all_view[$sidebarIds])){
                            $insert_data["view"] = @$all_view[$sidebarIds][0] == '1' ? 1: 0;
                        }
                        if(isset($all_delete[$sidebarIds])){
                            $insert_data["delete"] = @$all_delete[$sidebarIds][0] == '1' ? 1: 0;
                        }

                            SubadminPermission::create($insert_data);
                        
                    }
                }

            Session::flash('message', 'SubAdmin has added updated successfully.');
            return redirect('admin/subadmin-management');

        }
    }
     public function editsubAdmin(Request $request, $id)
    {
         $selected_sidebar = SubadminPermission::where('user_id', $id)->get();

        if ($request->isMethod('get'))
        {
            $user = Admin::find($id);
            $sidebar = Sidebar::get();

            return view('admin.edit-subadmin', compact('user', 'sidebar', 'selected_sidebar'));
        }

        if ($request->isMethod('post'))
        {
            $message = ['name.max' => 'name should not be greater than 20 characters', 'name.required' => 'Please enter name','email.required' => 'please enter email address','email.max' => 'Email address should not be greater than 200 characters','visible_pwd.required' => 'please enter password', 'visible_pwd.max' => 'password should not be greater than  200 characters', 'phone_number.required' => 'Please enter phone number', 'phone_number.max' => 'Phone number should not be greater than 15 digits',
            
            ];
             
            $this->validate($request, ['name' => 'required|max:20',  'visible_pwd' => 'required|max:200', 'phone_number' => 'required|max:15', 'email' => 'unique:admins,email,'.$id

            ], $message);
            /// return $request->all();
            $updateInfulancer = Admin::where('id', $id)->update(['name' => $request->name,'phone_number'=>$request->phone_number,'email'=>$request->email,'password' => Hash::make($request->input('visible_pwd')) , 'visible_pwd' => $request->input('visible_pwd')]);
          
            $selected = json_decode($request->get("old_sidebar"));
            $sidebarId = $request->sidebar_id;
            if ($sidebarId)
            {
                 $deletable = array_diff($selected, $sidebarId);
                  $insertable = array_diff($sidebarId, $selected);
            }
            else
            {
                $deletable = $selected;
                $insertable = array();
            }
            
            if ($deletable && !empty($deletable) && count($deletable) > 0)
            {

                SubadminPermission::where('user_id', $id)->whereIn("sidebar_id", $deletable)->delete();
            }
            if ($insertable && !empty($insertable) && count($insertable) > 0)
            {

                $all_add = $request->get("add");
                $all_edit = $request->get("edit");
                $all_view = $request->get("view");
                $all_delete = $request->get("delete");
                foreach ($insertable as $sidebarIds)
                {
                    if (isset($sidebarIds) && !empty($sidebarIds))
                    {
                        $insert_update_data = [];
                        $insert_update_data = [
                            "user_id" => $id,
                            "sidebar_id"   => $sidebarIds,
                            "add" => 0,
                            "edit" => 0,
                            "view" => 0,
                            "delete" => 0
                        ];

                        if(isset($all_add[$sidebarIds])){
                            $insert_update_data["add"] = @$all_add[$sidebarIds][0] == '1' ? 1: 0;
                        }
                        if(isset($all_edit[$sidebarIds])){

                            $insert_update_data["edit"] = @$all_edit[$sidebarIds][0] == "1" ? 1: 0;
                        }
                        if(isset($all_view[$sidebarIds])){
                            $insert_update_data["view"] = @$all_view[$sidebarIds][0] == '1' ? 1: 0;
                        }
                        if(isset($all_delete[$sidebarIds])){
                            $insert_update_data["delete"] = @$all_delete[$sidebarIds][0] == '1' ? 1: 0;
                        }
                       // return $insert_update_data;
                        
                        $is_exist = SubadminPermission::where(["user_id" => $id,"sidebar_id" => $insert_update_data["sidebar_id"]])->first();
                        if($is_exist){
                            SubadminPermission::where("id",$is_exist->id)->update($insert_update_data);
                        }else{
                            SubadminPermission::create($insert_update_data);
                        }
                        // $SubAdminRight = new SubadminPermission();
                        // if($is_exist){
                        //     $SubAdminRight = SubadminPermission::find($is_exist->id);
                        // }
                        // $SubAdminRight->fill($insert_update_data);
                        // $SubAdminRight->save();

                         //return $insert_update_data;
                        // $interestData = new SubadminPermission();
                        // $interestData->user_id = $id;
                        // $interestData->sidebar_id = $sidebarIds;
                        // $interestData->save();
                    }
                }
            }
            Session::flash('message', 'SubAdmin has been updated successfully.');
            return redirect('admin/subadmin-management');

        }
    }
    public function viewsubAdmin(Request $request, $id)
    {
        $user = Admin::find($id);
        
        $sideBarData = SubadminPermission::where(['user_id' => $id])->get();
        foreach ($sideBarData as $sidebarIds)
        {
            $sideBarName = Sidebar::where(['id' => $sidebarIds
                ->sidebar_id])
                ->first();
                $sidebarIds->name = $sideBarName->name; 
        }
        
     
        return view('admin.view-subadmin', compact('user', 'sideBarData'));
    }
    public function deletesubAdmin(Request $request)
    {
        $user = Admin::where('id', '=', $request->customer_id)->delete();
        $user = SubadminPermission::where('user_id', '=', $request->customer_id)->delete();
        Session::flash('message', 'SubAdmin has been deleted successfully.');
        return redirect('admin/subadmin-management')
        ->withInput();
    }
    public function business(Request $request)
    { 
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $businessListing = User::where(['user_type' => 'B', 'is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view('admin.business', compact('businessListing','subadmin_user_id'));
    }

    public function editBusiness(Request $request, $id)
    {
        if ($request->isMethod('get'))
        {
           $user = User::find($id);
            $BusinessDetail = BusinessDetail::where('user_id', $id)->first();
            $gigData = Gig::where('user_id', $id)->get();

            return view('admin.edit-business', compact('user', 'gigData', 'BusinessDetail'));
        }

        if ($request->isMethod('post'))
        {
           //return  $request->all();
            $message = ['name.required' => 'Please enter business name', 'phone_number.required' => 'Please enter business number', 'description.required' => 'Please enter business description', 'image.image:jpg,png' => 'Please select only JPG or PNG image',
                'phone_number.digits_between'=>'Phone number should be 7 to 15 digits.' ];

            $this->validate($request, ['name' => 'required|max:30', 'phone_number' =>  'required|digits_between:7,15', 'description' => 'required|max:3000', 'image' => 'mimes:jpeg,png,jpg'

            ], $message);

            $getBusinessDetail = BusinessDetail::where('user_id', $id)->first();
            if (!empty($getBusinessDetail))
            {
                $updateBusinessDetail = BusinessDetail::where('user_id', $id)->update(['business_detail' => $request->description, 'owner_name' => '']);
            }
            else
            {
                $insertBusinessDetail = BusinessDetail::insert(['business_detail' => $request->description, 'user_id' => $id, 'owner_name' => '']);
            }

            if ($request->hasFile("image"))
            {
                $image = $request->file('image');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = User::where('id', $id)->update(['name' => $request->name, 'phone_number' => $request->phone_number, 'profile' => $imageName]);

            }
            $updateBusiness = User::where('id', $id)->update(['name' => $request->name, 'phone_number' => $request->phone_number]);
            Session::flash('message', 'Business Detail has been updated successfully.');
            return redirect('admin/business');

        }
    }

    public function viewBusiness(Request $request, $id)
    {
        $businessDetails = User::find($id);
        $contactDetails = Gig::where('user_id', $id)->get();
        return view('admin.view-business', compact('businessDetails', 'contactDetails'));
    }
    
     public function refferalList(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $businessListing = User::where(['is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view('admin.refferal-list', compact('businessListing','subadmin_user_id'));
    }
    
     public function viewRefferal(Request $request, $id)
    {
        $businessDetails = User::find($id);
        $contactDetails = RefferalList::where('user_id', $id)->get();
         foreach($contactDetails as $details){
         $details->userDetail = User::where(['id'=>$details->other_user_id])->first();
        }
       
        return view('admin.view-refferal', compact('businessDetails', 'contactDetails'));
    }
    public function blockBusiness(Request $request, $id)
    {
        $users = User::whereId($id)->first();
        if ($users->status == '1')
        {
             $users = User::where(['id'=>$id])->update(['status'=>'2']);
            /*$users->status = 2;
            $users->update();*/
            Session::flash('message', 'User Blocked successfully.');
        }
        else
        {
             $users = User::where(['id'=>$id])->update(['status'=>'1']);
            Session::flash('message', 'User Unblocked successfully.');
        }
        return redirect('admin/business');

    }

    public function blockInfluencer(Request $request, $id)
    {
        $users = User::whereId($id)->first();
        if ($users->status == '1')
        {
             $users = User::where(['id'=>$id])->update(['status'=>'2']);
            /*$users->status = 2;
            $users->update();*/
            Session::flash('message', 'User Blocked successfully.');
        }
        else
        {
             $users = User::where(['id'=>$id])->update(['status'=>'1']);
            Session::flash('message', 'User Unblocked successfully.');
        }
        return redirect('admin/influencer-Management');

    }
    public function editBusinessContact(Request $request, $id, $user_id)
    {
        if ($request->isMethod('get'))
        {
            $user = User::find($user_id);
            $gigData = Gig::where('id', $id)->first();

            return view('admin.edit-bussiness-contact', ['gigData' => $gigData, 'user' => $user]);
        }

        if ($request->isMethod('post'))
        {
            $message = ['owner_name.required' => 'Please enter contact person name', 'phone_number.required' => 'Please enter contact person number', 'image.image:jpg,png' => 'Please select only JPG or PNG image', ];

            $this->validate($request, ['owner_name' => 'required|max:30', 'phone_number' => 'required|max:20', 'image' => 'mimes:jpeg,png,jpg'

            ], $message);
            if ($request->hasFile("image"))
            {
                $image = $request->file('image');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = Gig::where('id', $id)->update(['owner_name' => $request->owner_name, 'phone_number' => $request->phone_number, 'profile' => $imageName]);

            }
            Session::flash('message', 'Business Detail has been updated successfully.');
            return redirect('admin/edit-business/' . $user_id);

        }
    }

    public function deleteBusiness(Request $request)
    {

        $user = User::where('id', '=', $request->customer_id)
            ->update(['is_deleted' => '1']);
        Session::flash('message', 'Business has been deleted successfully.');
        return redirect('admin/business')
            ->withInput();
    }

    public function Gigs(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
          $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

          $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }
          $gigDetail = Gig::orderBy("id", "desc")
            ->whereIn("id",$j)
           
            ->get();
        // $gigDetail = Gig::orderby('id',"DESC")->get();
        return view('admin.gigs', compact('gigDetail','subadmin_user_id'));
    }
    
      public function acceptedGigList(Request $request)
       {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
         $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }
          $gigDetail = Gig::orderBy("id", "desc")
            ->whereIn("id",$j)
           
            ->get();

        return view('admin.accepted-gig-list', compact('gigDetail','subadmin_user_id'));
       }
      public function completedGigList(Request $request)
    { 
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
         $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }
         $gigDetail = Gig::orderBy("gigs.id", "desc")
            ->whereIn("id",$j)
           
            ->get();
        return view('admin.completed-gig-list', compact('gigDetail','subadmin_user_id'));
    }

    public function viewGigs(Request $request, $id)
    {  
    if ($request->isMethod('get'))
    {
    $gigDetail = Gig::where(['id' => $id])->first();
    $start = Carbon::parse($gigDetail->gig_start_date_time);
    $end = Carbon::parse($gigDetail->gig_end_date_time);
    $diff = $end->diffInHours($start);
    //echo "<pre>";print_r($diff);die;
    $hour = $diff . " Hours";
    $busienssDetail = User::where(['id' => $gigDetail
        ->user_id])
        ->first();
            
    if($busienssDetail->user_type == "B"){

        $businessData = BusinessDetail::where(['user_id' => $gigDetail
        ->user_id])
        ->first();
    }else{
        $businessData = InfluencerDetail::where(['user_id' => $gigDetail
        ->user_id])
        ->first();
    }
    $gigImages = GigMedia::where(['gig_id' => $id])->get();
     $gigPrice = GigPrice::where(['gig_prices.gig_id' => $id])
                ->leftJoin("users","users.id","=","gig_prices.infulancer_id")
                ->leftJoin("gig_users","gig_users.user_id","=","gig_prices.infulancer_id")
                ->select("gig_prices.*","users.name as username","users.email as useremail","users.phone_number as phoneNumber","users.id as user_id","gig_users.status as gig_status")
                ->where("users.user_type",'I')
                ->groupby('gig_prices.infulancer_id')
                ->get();
    $gigUser = GigUser::where('gig_id',$id)->get(); 
    return view('admin.view-gigs', compact('gigDetail', 'gigPrice', 'gigImages', 'busienssDetail', 'hour','businessData','gigUser'));
    }
    if($request->isMethod('POST')){ 
    $price =  $request->get("price");
    if(isset($price) && !empty($price)){
    // $price[0] = number_format($price[0],2);
    $post_id = $id;
    $infulancerId =  $request->get("user_id");
    $gigDetail = Gig::where(['id' => $id])->first();
    $getBusinessName = User::where('id', $gigDetail->user_id)->first();
    $name = $getBusinessName->name;
    GigPrice::where(['infulancer_id' => $infulancerId, 'gig_id' => $id, 'user_id' => $gigDetail->user_id])->update(['price' => $price, 'status' =>1, 'is_resend' => 1]);
    GigUser::where(['user_id' => $infulancerId, 'gig_id' => $id])->update(['status' =>5, 'is_resend' => 1]);

       $CommandID = "CustomerPayBillOnline";
        $Amount = $price;
        $MSISDN = "0711650653";
        $BillRefNumber = "test";
        $ShortCode = "600762";
    $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
    $getAdminId = Admin::first();
    $insertAdminAmount = AdminPayment::insert(['user_id' => $gigDetail->user_id, 'infulencer_id' => $infulancerId, 'amount' => $price, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $id, 'type' => 'gig']);
    //notifiaction code is pending
    $offerPriceData = GigPrice::where(['gig_id' => $id, 'infulancer_id' => $infulancerId])->first();
    $message = $name . " sent a better gig offer";
    $notificationMessage = "sent a better gig offer";
    $notifyMessage = array(
        'sound' => 1,
        'message' => $message,
        'id' => $id
    );
    $message_new = array('sound' =>1,'message'=>$message,
                        'notifykey'=>'better gig offer','id'=>$id);
    $insertNotification = Notification::insert(['other_user_id' => $gigDetail->user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$id ]);
    $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
    if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
        if ($getInfulancerUser->device_type == '2') {
             $this->send_android_notification($getInfulancerUser->device_id, "sent a better post offer","better gig offer", $message_new);
        } else {
            $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "sent a better gig offer", $notifyMessage);
        }
    }
    }if($update_p > 0){
         Session::flash('message', 'Gig Price resent successfully.');
        return redirect('admin/view-gigs/'.$id);
     }else{
         Session::flash('danger', 'Gig Price not submitted.');
         return redirect('admin/view-gigs/'.$id);
     }
    } 
   
    
    }
     public function addGigPrice(Request $request,$id)
    {  
    
        if ($request->isMethod('get'))
        {
       
        $gigDetail = Gig::where('id', $id)->first();
         $user = User::whereNotIn("id",function($query) use($id){
                    return $query->select("user_id")
                            ->from("gig_users")
                            ->where("gig_id",$id);
       })->get(); 
       
        return view('admin.add-gig-price', compact('gigDetail','user'));
    }
     if($request->isMethod('POST')){ 
       // return $request->all();
      $price =  $request->get("price");
      $infulancerId = $request->get("user_id");
    if(isset($price) && !empty($price)){
       // $price[0] = number_format($price[0],2);
    $gigDetail = Gig::where(['id' => $id])->first();
    $getBusinessName = User::where('id', $gigDetail->user_id)->first();
    $name = $getBusinessName->name;
    GigPrice::insert(['infulancer_id' => $infulancerId, 'gig_id' => $id, 'user_id' => $gigDetail->user_id,'price' => $price, 'status' =>'1']);
    GigUser::insert(['user_id' => $infulancerId, 'gig_id' => $id,'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);

      $CommandID = "CustomerPayBillOnline";
            $Amount = $price;
            $MSISDN = "0711650653";
            $BillRefNumber = "test";
            $ShortCode = "600762";
           $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
            $getAdminId = Admin::first();
            $insertAdminAmount = AdminPayment::insert(['user_id' => $gigDetail->user_id, 'infulencer_id' =>$infulancerId, 'amount' => $price, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $id, 'type' => 'gig']);

            ///notification code is pending
            $offer_price_data = GigPrice::where(['gig_id' => $id, 'infulancer_id' => $infulancerId])->first();
            $message = $name . " invited you to a new Gig";
            $notificationMessage =  "invited you to a new Gig";
            $insertNotification = Notification::insert(['other_user_id' => $gigDetail->user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' =>date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$id]);
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $id
            );
            $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'invited','id'=>$id);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
               
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
            {
                if ($getInfulancerUser->device_type == '2')
                {
                    $this->send_android_notification($getInfulancerUser->device_id, "invited you to a new Gig","invited", $message_new);
                }
                else
                {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "invited you to a new Gig", $notifyMessage);
                }
            }
    }
     if($price > 0){
         Session::flash('message', 'Gig price submitted successfully.');
        return redirect('admin/add-gig-price/'.$id);
     }else{
         Session::flash('danger', 'Gig price not submitted.');
         return redirect('admin/add-gig-price/'.$id);
     }
    } 
   
    
    }

    public function Plug(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0") 
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }

          $postListing = Post:: orderBy("id", "desc")
            ->whereIn("id",$j)
            
            ->get();
        //$postListing = Post::orderby('id',"DESC")->get();
         
        return view('admin.plug', compact('postListing','subadmin_user_id'));
    }
   
    public function acceptedPostList(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0") 
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }

            $postListing = Post:: orderBy("id", "desc")
            ->whereIn("id",$j)
            
            ->get();
        return view('admin.accepted-post-list', compact('postListing','subadmin_user_id'));
    }
     public function completedPostList(Request $request)
    {   
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
        ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
        ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }

            $postListing = Post:: orderBy("id", "desc")
            ->whereIn("id",$j)
            
            ->get();
        return view('admin.completed-post-list', compact('postListing','subadmin_user_id'));
    }
    public function viewPlug(Request $request, $id)
    {  
        if ($request->isMethod('get'))
        {
        $postDetail = Post::where('id', $id)->first();
        $busienss_Detail = User::where('id', $postDetail->user_id)
            ->first();
        $businessData = BusinessDetail::where('user_id', $postDetail->user_id)
            ->first();
        $postImages = PostImage::where('post_id', $id)->get();
         $postPrice = PostPrice::where(['post_prices.post_id' => $id])
                    ->leftJoin("users","users.id","=","post_prices.infulancer_id")
                    ->leftJoin("post_users","post_users.user_id","=","post_prices.infulancer_id")
                    ->select("post_prices.*","users.name as username","users.email as useremail","users.phone_number as phoneNumber","post_users.status as post_status")
                    ->where("users.user_type",'I')
                    ->groupby('post_prices.infulancer_id')
                    ->get();
  
        return view('admin.view-plug', compact('postPrice', 'postDetail', 'postImages', 'busienss_Detail','businessData'));
    }
     if($request->isMethod('POST')){   
    $price =  $request->get("price");
    if(isset($price) && !empty($price)){
       // $price[0] = number_format($price[0],2);
    $post_id = $id;
     $infulancerId = $request->get("user_id");
    $gigDetail = Post::where(['id' => $id])->first();
    $getBusinessName = User::where('id', $gigDetail->user_id)->first();
    $name = $getBusinessName->name;
    PostPrice::where(['infulancer_id' => $infulancerId, 'post_id' => $id, 'user_id' => $gigDetail->user_id])->update(['price' => $price, 'status' =>'1', 'is_resend' => '1']);
    PostUser::where(['user_id' => $infulancerId, 'post_id' => $id])->update(['status' =>'6', 'is_resend' => '1']);

       $CommandID = "CustomerPayBillOnline";
        $Amount = $price;
        $MSISDN = "0711650653";
        $BillRefNumber = "test";
        $ShortCode = "600762";
       $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
    $getAdminId = Admin::first();
    $insertAdminAmount = AdminPayment::insert(['user_id' => $gigDetail->user_id, 'infulencer_id' => $infulancerId, 'amount' => $price, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $id, 'type' => 'post']);
    //notifiaction code is pending
    $offerPriceData = PostPrice::where(['post_id' => $id, 'infulancer_id' => $infulancerId])->first();
    $message = $name . " sent a better post offer";
    $notificationMessage = "sent a better post offer";
    $notifyMessage = array(
        'sound' => 1,
        'message' => $message,
        'id' => $id
    );
    $message_new = array('sound' =>1,'message'=>$message,
                        'notifykey'=>'better post offer','id'=>$id);
    $insertNotification = Notification::insert(['other_user_id' => $gigDetail->user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$id ]);
    $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
    if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
        if ($getInfulancerUser->device_type == '2') {
             $this->send_android_notification($getInfulancerUser->device_id, "sent a better post offer","better gig offer", $message_new);
        } else {
            $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "sent a better gig offer", $notifyMessage);
        }
    }
    }
     if($price > 0){
         Session::flash('message', 'Post Price resent successfully.');
        return redirect('admin/view-plug/'.$id);
     }else{
         Session::flash('danger', 'Post Price not submitted.');
         return redirect('admin/view-plug/'.$id);
     }
    } 
   
    
    }
    
     public function addPostPrice(Request $request,$id)
    {  
    
        if ($request->isMethod('get'))
        {
        
        $postDetail = Post::where('id', $id)->first();
        
       $postDetails =  Post::select(DB::raw("( select group_concat(user_id) from post_users where post_id = posts.id ) as users"),"id")->where("id",$id)->first();
       //echo json_encode(["data" => $postDetails->]); die;

         $user = User::whereNotIn("id",function($query) use($id){
                    return $query->select("user_id")
                            ->from("post_users")
                            ->where("post_id",$id);
       })->get(); 
       
        return view('admin.add-post-price', compact('postDetail','user'));
    }
     if($request->isMethod('POST')){ 
       // return $request->all();
      $price =  $request->get("price");
      $infulancerId = $request->get("user_id");
    if(isset($price) && !empty($price)){
       // $price[0] = number_format($price[0],2);
    
    $gigDetail = Post::where(['id' => $id])->first();
    $getBusinessName = User::where('id', $gigDetail->user_id)->first();
    $name = $getBusinessName->name;
    PostPrice::insert(['infulancer_id' => $infulancerId, 'post_id' => $id, 'user_id' => $gigDetail->user_id,'price' => $price, 'status' =>'1']);
    PostUser::insert(['user_id' => $infulancerId, 'post_id' => $id,'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);

      $CommandID = "CustomerPayBillOnline";
            $Amount = $price;
            $MSISDN = "0711650653";
            $BillRefNumber = "test";
            $ShortCode = "600762";
           $c2bTransaction = Mpesa::c2b($ShortCode,$CommandID,$Amount, $MSISDN, $BillRefNumber);
            $getAdminId = Admin::first();
            $insertAdminAmount = AdminPayment::insert(['user_id' => $gigDetail->user_id, 'infulencer_id' =>$infulancerId, 'amount' => $price, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') , 'post_id' => $id, 'type' => 'post']);

            ///notification code is pending
            $offer_price_data = PostPrice::where(['post_id' => $id, 'infulancer_id' => $infulancerId])->first();
            $message = $name . " invited you to a new Post";
            $notificationMessage =  "invited you to a new Post";
            $insertNotification = Notification::insert(['other_user_id' => $gigDetail->user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' =>date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>0,'notify_message'=>$notificationMessage,'post_id'=>$id]);
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $id
            );
            $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'invited','id'=>$id);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
               
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
            {
                if ($getInfulancerUser->device_type == '2')
                {
                    $this->send_android_notification($getInfulancerUser->device_id, "invited you to a new Post","invited", $message_new);
                }
                else
                {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = "invited you to a new Post", $notifyMessage);
                }
            }
    }
     if($price > 0){
         Session::flash('message', 'Post price submitted successfully.');
        return redirect('admin/add-post-price/'.$id);
     }else{
         Session::flash('danger', 'Post price not submitted.');
         return redirect('admin/add-post-price/'.$id);
     }
    } 
   
    
    }
    public function payments(Request $request)
    {   
        $payments = Payment::with('user')->orderby('id',"DESC")->get();
        return view('admin.payments', compact('payments'));
        //return view('admin.payments');
        
    }

    public function blog (Request $request){
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $blog = Blog::orderby('id',"DESC")->get();

        return view ('admin.blog',compact('blog','subadmin_user_id'));

    }

    public function addBlog(Request $request)
    {
        if ($request->isMethod('get'))
        {
            
            return view('admin.add-blog');
        }

        if ($request->isMethod('post'))
        {
            $message = ['title.required' => 'Please enter blog title', 'text.required' => 'Please enter blog text', 'image.required'=>'Please select blog image','image.image:jpg,png' => 'Please select only JPG or PNG image', ];

            $this->validate($request, ['title' => 'required|max:100',  'text' => 'required|max:10000', 'image' => 'required|mimes:jpeg,png,jpg'

            ], $message);

        

            if ($request->hasFile("image"))
            {
                $image = $request->file('image');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = Blog::insert(['title' => $request->title, 'text' => $request->text, 'image' => $imageName,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);

            }
            Session::flash('message', 'Blog has been added successfully.');
            return redirect('admin/blog');

        }
    }

    public function viewBlog (Request $request,$id){
        $blog = Blog::where('id',$id)->first();

        return view ('admin.view-blog',compact('blog'));

    }
    public function deleteBlog(Request $request)
    {
        $request->blog_id;
        $user = Blog::where('id', '=', $request->customer_id)->delete();
            
        Session::flash('message', 'Blog has been deleted successfully.');
        return redirect('admin/blog')
            ->withInput();
    }

    public function userPost (Request $request){
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $post = UserPost::orderby('id',"DESC")->get();

        return view ('admin.user-post',compact('post','subadmin_user_id'));

    }
     public function viewUserPost (Request $request,$id){
        $post = UserPost::where('id',$id)->first();

        return view ('admin.view-user-post',compact('post'));

    }
    public function deleteUserPost(Request $request)
    {
        $request->blog_id;
        $user = UserPost::where('id', '=', $request->customer_id)->delete();
            
        Session::flash('message', 'User Post has been deleted successfully.');
        return redirect('admin/user-post')
            ->withInput();
    }
   public function featured (Request $request){
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
         $featured = Featured::orderby('id',"DESC")->get();
        return view ('admin.featured',compact('featured','subadmin_user_id'));

    }
   public function addFeatured(Request $request)
    {
        if ($request->isMethod('get'))
        {
            
            return view('admin.add-featured');
        }

        if ($request->isMethod('post'))
        {
            $message = [ 'image.required'=>'Please select featured image','image.image:jpg,png' => 'Please select only JPG or PNG image','link.required'=>'Please enter link' ];

            $this->validate($request, ['image' => 'required|mimes:jpeg,png,jpg','link'=>'required'

            ], $message);

           // $getFeaturedData = Featured::count();
            //if($getFeaturedData >=5){
            // Session::flash('message', 'You have already ten featured image before add new image previous image deleted first .');
            //  return back()->withInput();  
            // }
               
            if ($request->hasFile('image'))
            {
                $image = $request->file('image');
                $link = $request->input('link');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = Featured::insert(['image' => $imageName,'link'=>$link]);

            }
            Session::flash('message', 'Featured has been added successfully.');
            return redirect('admin/featured');

        }
    }
     public function editFeatured(Request $request,$id)
    {
        if ($request->isMethod('get'))
        {
          $featured = Featured::where('id',$id)->first();
           return view ('admin.edit-featured',compact('featured'));
        }

        if ($request->isMethod('post'))
        {
            $message = [ 'image.image:jpg,png' => 'Please select only JPG or PNG image','link.required'=>'please enter link' ];

            $this->validate($request, ['image' => 'mimes:jpeg,png,jpg','link'=>'required'

            ], $message);
            //return $request->all();
            if ($request->hasFile("image"))
            {   
                $image = $request->file('image');  
                $link = $request->input('link');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = Featured::where('id', $id)->update(['image' =>$imageName,'link'=>$link]);

            }else{
                $updateBusiness = Featured::where('id', $id)->update(['link'=>$request->link]);
            }
            Session::flash('message', 'Featured has been updated successfully.');
            return redirect('admin/featured');

        }
    }


    public function viewFeatured (Request $request,$id){
        $featured = Featured::where('id',$id)->first();
        return view ('admin.view-featured',compact('featured'));

    }

    public function deleteFeatured(Request $request)
    {
        $request->featured_id;
        $user = Featured::where('id', '=', $request->customer_id)->delete();
           
        Session::flash('message', 'Featured has been deleted successfully.');
        return redirect('admin/featured')
            ->withInput();
    }

  public function interests (Request $request){
         $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
         $interests = Interest::orderby('id',"DESC")->get();
        return view ('admin.interests',compact('interests','subadmin_user_id'));

    } 
 public function addInterests(Request $request)
    {
        if ($request->isMethod('get'))
        {
            
            return view('admin.add-interests');
        }

        if ($request->isMethod('post'))
        {
            $message = [ 'image.required'=>'Please select interests image','image.image:jpg,png' => 'Please select only JPG or PNG image','interest_name.required'=>'Please enter interests' ];

            $this->validate($request, ['image' => 'required|mimes:jpeg,png,jpg','interest_name'=>'required'

            ], $message);

          
            if ($request->hasFile('image'))
            {
                $image = $request->file('image');
                $interest_name = $request->input('interest_name');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = Interest::insert(['image' => $imageName,'interest_name'=>$interest_name]);

            }
            Session::flash('message', 'Interests has been added successfully.');
            return redirect('admin/interests');

        }
    }
     public function editInterests(Request $request,$id)
    {
        if ($request->isMethod('get'))
        {
          $interests = Interest::where('id',$id)->first();
           return view ('admin.edit-interests',compact('interests'));
        }

        if ($request->isMethod('post'))
        {
            $message = [ 'image.image:jpg,png' => 'Please select only JPG or PNG image','interest_name.required'=>'please enter interests' ];

            $this->validate($request, ['image' => 'mimes:jpeg,png,jpg','interest_name'=>'required'

            ], $message);
            //return $request->all();
            if ($request->hasFile("image"))
            {   
                $image = $request->file('image');  
                $interest_name = $request->input('interest_name');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateBusiness = Interest::where('id', $id)->update(['image' =>$imageName,'interest_name'=>$interest_name]);

            }else{
                $updateBusiness = Interest::where('id', $id)->update(['interest_name'=>$request->interest_name]);
            }
            Session::flash('message', 'Interests has been updated successfully.');
            return redirect('admin/interests');

        }
    }


    public function viewInterests (Request $request,$id){
        $interests = Interest::where('id',$id)->first();
        return view ('admin.view-interests',compact('interests'));

    }

    public function deleteInterests(Request $request)
    {
        $request->featured_id;
        $user = Interest::where('id', '=', $request->customer_id)->delete();
           
        Session::flash('message', 'Interests has been deleted successfully.');
        return redirect('admin/interests')
            ->withInput();
    }

    public function influencerPayment(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $influencer = User::where(['user_type' => 'I', 'status' => '1', 'is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view ('admin/influencer-payment',compact('influencer','subadmin_user_id'));
    }
     
     public function viewInfluencerPayment(Request $request,$id){
        if($request->isMethod('GET')){

            $pluck_business_pay = AdminPayment::where(['infulencer_id' => $id,'status'=>1])->pluck('id');

            $payment_get = Payment::whereIn('admin_payment_id',$pluck_business_pay)->pluck('admin_payment_id');

            $busienssDetail = AdminPayment::where(['infulencer_id' => $id,'status'=>1])->whereNotIn('id',$payment_get)->get();


             foreach($busienssDetail as $busienssDetails){
            if($busienssDetails->type == "post"){

            $businessData = Post::select('post_name')->where(['id'=>$busienssDetails->post_id])->first();
          $busienssDetails->name = $businessData->post_name; 
        }elseif($busienssDetails->type == "gig"){
          $businessData = Gig::select('gig_name')->where(['id'=>$busienssDetails->post_id])->first();
            $busienssDetails->name = $businessData->gig_name;    
         } 
     }
          //return $busienssDetail; 
            return view ('admin/view-influencer-payment',compact('busienssDetail'));
        }

        if($request->isMethod('POST')){
            
            if(empty($request->put_value_select)){
                Session::flash('danger', 'Please select checkbox for a payment.');
                return back()->withInput();
            }

            $selected_for_payment = explode(',', $request->put_value_select);

             $get_influencer_id_of_selected = AdminPayment::whereIn('id',$selected_for_payment)->select('infulencer_id','amount','user_id','post_id','type','id')->with('user')->get();

            foreach ($get_influencer_id_of_selected as $influencer) {
            $amount = $influencer->amount;
             $InitiatorName = "peter";
            $SecurityCredential = "Fbjnx3KAPGYo/z/Zvg5pBMnnfAg9Kb6FBf8G37KdbWV6cfz66DYmaa/ohCqgYiVeJIIB6IoozLQgSSTTZ3z2yHchpBzecjfFeJ01nkF29J7Ap3G3t76bAb6pyuh3Ckk2QX3lH5f7MxmOO3bKY7qltmv2dtmCqNytEWVmwDD0Z2zmioH87/5nEJrtK/uMpV3XNVnK0vOIAa4xJk+ZGeGuxDYIsoZ2K3MOPadvNS2JxWapzyEqnKr39M3qgmerd4FIyfNoSIh6GnFQItxlytvLYyt/HRdJro0oNWGEpwmS+GerouirfXm9SqQmKYw1KOCLc+o2kcxw27FRL3SrAjvfFg==";
            $CommandID = "BusinessPayment";
            $percentage = 25;
            $adminAmount = ($percentage / 100) * $amount;
            $Amount = $amount-$adminAmount;
            $PartyA = "600762";
            $PartyB = "254722000000";
            $Remarks = "okay this better work";
            $QueueTimeOutURL = "http://www.google.com";
            $ResultURL = "http://www.google.com";
            $Occasion = "testing";
             $b2cTransaction = Mpesa::b2c($InitiatorName, $SecurityCredential, $CommandID, $Amount, $PartyA, $PartyB, $Remarks, $QueueTimeOutURL, $ResultURL, $Occasion);

            $insertPayment = Payment::insert([
                'user_id' =>$influencer->user_id,
                'infulencer_id'=> $influencer->infulencer_id,
                'amount' => $Amount,
                'created_at' => date('Y-m-d H:i:s') ,
                'updated_at' => date('Y-m-d H:i:s'),
                'post_id'    => $influencer->post_id,
                'type' =>$influencer->type,
                'admin_payment_id'  => $influencer->id,
                'status'=>2
                ]);
        
            }

            Session::flash('message', 'Payment has been sent successfully.');
            return redirect('admin/influencer-payment');
        }
     }
     public function businessPayment(Request $request)
    {   
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
         $business = User::where(['user_type' => 'B', 'status' => '1', 'is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view ('admin/business-payment',compact('business','subadmin_user_id'));
    }

    public function viewBusienssPayment(Request $request,$id)
     {

      if($request->isMethod('GET')){

        $pluck_business_id = AdminPayment::where(['user_id' => $id,'status'=>2])->pluck('id');

        $payment_get = Payment::whereIn('admin_payment_id',$pluck_business_id)->pluck('admin_payment_id');



        $busienssDetail = AdminPayment::where(['user_id' => $id,'status'=>2])->whereNotIn('id',$payment_get)->get();


         foreach($busienssDetail as $busienssDetails){
            if($busienssDetails->type == "post"){

            $businessData = Post::select('post_name')->where(['id'=>$busienssDetails->post_id])->first();
           $busienssDetails->name = $businessData->post_name; 
        }elseif($busienssDetails->type == "gig"){
          $businessData = Gig::select('gig_name')->where(['id'=>$busienssDetails->post_id])->first();
           $busienssDetails->name = $businessData->gig_name;  
         } 
     }
        //return $busienssDetail;    
        return view ('admin/view-business-payment',compact('busienssDetail'));
        }

        if($request->isMethod('POST')){
            
            if(empty($request->put_value_select)){
                Session::flash('danger', 'Please select checkbox for a payment.');
                return back()->withInput();
            }

            $selected_for_payment = explode(',', $request->put_value_select);

            $get_busienss_id_of_selected = AdminPayment::whereIn('id',$selected_for_payment)->select('infulencer_id','amount','user_id','type','post_id','id')->with('user')->get();

             foreach ($get_busienss_id_of_selected as $business) {
            $amount = $business->amount;    
             $InitiatorName = "peter";
            $SecurityCredential = "Fbjnx3KAPGYo/z/Zvg5pBMnnfAg9Kb6FBf8G37KdbWV6cfz66DYmaa/ohCqgYiVeJIIB6IoozLQgSSTTZ3z2yHchpBzecjfFeJ01nkF29J7Ap3G3t76bAb6pyuh3Ckk2QX3lH5f7MxmOO3bKY7qltmv2dtmCqNytEWVmwDD0Z2zmioH87/5nEJrtK/uMpV3XNVnK0vOIAa4xJk+ZGeGuxDYIsoZ2K3MOPadvNS2JxWapzyEqnKr39M3qgmerd4FIyfNoSIh6GnFQItxlytvLYyt/HRdJro0oNWGEpwmS+GerouirfXm9SqQmKYw1KOCLc+o2kcxw27FRL3SrAjvfFg==";
            $CommandID = "BusinessPayment";
            $percentage = 25;
            $adminAmount = ($percentage / 100) * $amount;
            $Amount = $amount-$adminAmount;
            $PartyA = "600762";
            $PartyB = "254722000000";
            $Remarks = "okay this better work";
            $QueueTimeOutURL = "http://www.google.com";
            $ResultURL = "http://www.google.com";
            $Occasion = "testing";
             $b2cTransaction = Mpesa::b2c($InitiatorName, $SecurityCredential, $CommandID, $Amount, $PartyA, $PartyB, $Remarks, $QueueTimeOutURL, $ResultURL, $Occasion);

            $insertPayment = Payment::insert([
                'user_id' =>$business->user_id,
                'infulencer_id'=> $business->infulencer_id,
                'amount' => $Amount,
                'created_at' => date('Y-m-d H:i:s') ,
                'updated_at' => date('Y-m-d H:i:s'),
                'post_id'    => $business->post_id,
                'type' =>$business->type,
                'admin_payment_id'  => $business->id
                ]);
        
            }


            Session::flash('message', 'Payment has been sent successfully.');
            return redirect('admin/business-payment');
        }
     }

public function createTimeline(Request $request){
        if($request->isMethod("GET")){  
            return view("admin.add-twiva-updates");
        }
        if($request->isMethod("POST")){
            $video_image_types = ["video/x-flv",
                           "video/mp4",
                           "application/x-mpegURL",
                           "video/MP2T",
                           "video/3gp",
                           "video/quicktime",
                           "video/x-msvideo",
                           "video/x-ms-wmv",
                            "image/jpg",
                            "image/jpeg",
                            "image/png",
                        ];

            $message = ['timeline_text.required'=>'Please enter timeline text.',
                        'timeline_text.max'=>"timeline text is not greater than 10000 characters."];
            $this->validate($request, [ "timeline_text" =>  "required|max:10000"
            ], $message);
            
            if($request->hasFile("image_video")){
                foreach ($request->file("image_video") as $value) {
                    if(!in_array($value->getMimeType(),$video_image_types)){
                        return back()->withInput()->with("danger","Please upload jpg, jpeg, png, mp4, 3gp, mov, avi file only");
                    }else if($value->getSize() > 20971520){ /*define size of file*/
                        return back()->withInput()->with("danger","File should not be greater than 20 MB");
                    }
                }
            }
            $insertData["timeline_text"] = $request->get("timeline_text");
            $insertData["user_id"] = 1;
            $is_inserted = Timeline::create($insertData);          
            $video_images = $request->file("image_video");

            if($is_inserted){
                if($request->hasFile("image_video")){
                $cnt_img_vid = count($video_images);
                for($k = 0; $k < $cnt_img_vid;$k++){
                    if(isset($video_images[$k]) && !empty($video_images[$k])){
                        $file = $this->uploadImage($video_images[$k],"/app/public/uploads/images/business_images/timeline_images", 1);
                        $insert_data = array();
                        $insert_data["media"] = $file;
                        $insert_data["timeline_id"] = $is_inserted->id;

                        TimelineImages::create($insert_data);
                    }
                }
            }     

            }
                 Session::flash('message', 'Timeline has been added successfully.');
                return redirect("admin/twiva-updates-list");
            }

            
        
    }

     public function getTimeline(Request $request) {  
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
         $getTimelineList = Timeline::with("timelineImages")->orderby('id','DESC')->get();
        return view('admin/twiva-updates-list',compact('getTimelineList','subadmin_user_id'));
       
    }
   
     public function viewTimeline(Request $request,$id) {
       
        $viewTimeline = Timeline::with("timelineImages")->where('id',$id)->first();
       // echo "<pre>";print_r($viewTimeline->timeline_text);die;
       return view('admin/view-twiva-updates-list',compact('viewTimeline'));
       
    }

   public function send_android_notification($device_token,$message,$notmessage='',$msgsender_id=''){
    if (!defined('API_ACCESS_KEY'))
    {
      define('API_ACCESS_KEY','AAAAw_qk_lI:APA91bHb0bLFByFcj3gd8umTYG5QzOFLUVWkzXudN9xHAXl232plDra86kXHohP-vAq27SM1mYT4q-FuI4OQj135k1D6_XVjF6w3uHZaFJxvR4tQO-Zi725juBHnBMB_jDWOm4eC5VMy');
    }
    $registrationIds = array($device_token);
    $fields = array(
      'registration_ids' => $registrationIds,
      'alert' => $message,
      'sound' => 'default',
      'Notifykey' => $notmessage, 
      'data'=>$msgsender_id
        
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($fields) );
    $result = curl_exec($ch);

    if($result == FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }

    curl_close( $ch );
    return  $result;
  }

public  function send_iphone_notification($recivertok_id,$message,$notmessage='',$notfy_message){
    //$PATH = dirname(dirname(dirname(dirname(__FILE__))))."/pemfile/lens_development_push.pem";
    $PATH = dirname(dirname(dirname(dirname(__FILE__))))."/pemfile/apns-prod.pem";
    $deviceToken = $recivertok_id;  
    $passphrase = "";
    $message = $message;  
    $ctx = stream_context_create();
         stream_context_set_option($ctx, 'ssl', 'local_cert', $PATH);
         stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    
      $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err,
    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
       
      /*$fp = stream_socket_client(
                 'ssl://gateway.push.apple.com:2195', $err,
     $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
      */
    $body['message'] = $message;
    $body['post_id'] = $notfy_message;
    $body['Notifykey'] = $notmessage;
            if (!$fp)
                 exit("Failed to connect: $err $errstr" . PHP_EOL);

        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'details'=>$body
        );
           
    $payload = json_encode($body);
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
    $result = fwrite($fp, $msg, strlen($msg));

    //echo "<pre>";
    // print_r($result);
    // if (!$result)
      // echo 'Message not delivered' . PHP_EOL;
    // else
      // echo 'Message successfully delivered' . PHP_EOL;
    // exit;
    fclose($fp);
    return $result;
    die;
  }


     public function creatorsManagement(Request $request) {
       $user = Auth::guard('admin')->user();
       $subadmin_user_id = $user->id;
        $creator = User::where(['user_type' => 'W','is_deleted' => '0'])->orderby('id',"DESC")->get();
        return view('admin.creators-Management', compact('creator','subadmin_user_id'));
    }

     public function viewCreators (Request $request,$id){
         $creator = User::with('categoryUser')->where('id',$id)->first();
         $CategoryUser = CategoryUser::where('user_id',$id)->first();
         $category = Category::where('id',$CategoryUser->category_id)->first();
        return view ('admin.view-creators',compact('creator','category'));

    }

    public function deleteCreators(Request $request)
    {
        $user = User::where('id', '=', $request->creators_id)->delete();
        $service = Services::where('user_id', '=', $request->creators_id)->delete();
        $portfolio = Portfolio::where('user_id', '=', $request->creators_id)->delete();
        $blogger = Blogger::where('user_id', '=', $request->creators_id)->delete();
        $rate = Rate::where('user_id', '=', $request->creators_id)->delete();
        Session::flash('message', 'creators has been deleted successfully.');
        return redirect('admin/creators-Management')
            ->withInput();
    }

    public function portfolioManagement(Request $request,$id) {
       
        $portfolio = Portfolio::where(['user_id' => $id])->orderby('id',"DESC")->get();
        return view('admin.portfolio-management', compact('portfolio','id'));
    }

     public function viewPortfolio (Request $request,$id){
         $portfolio = Portfolio::where('id',$id)->first();   
        return view ('admin.view-portfolio',compact('portfolio'));

    }

    public function deletePortfolio(Request $request)
    {
        $user = Portfolio::where('id', '=', $request->portfolio_id)->delete();
        Session::flash('message', 'Portfolio has been deleted successfully.');
        return back()->withInput();
    }
    public function servicesManagement(Request $request,$id) {
      
        $service = Services::where(['user_id' =>$id])->orderby('id',"DESC")->get();
        return view('admin.services-management', compact('service','id'));
    }

     public function viewServices (Request $request,$id){
         $service = Services::where('id',$id)->first();
        return view ('admin.view-services',compact('service'));

    }

    public function deleteServices(Request $request)
    {
        $user = Services::where('id', '=', $request->services_id)->delete();
           
        Session::flash('message', 'Services has been deleted successfully.');
         return back()->withInput();
    }
    public function rateManagement(Request $request,$id) {
        $rate = Rate::where(['user_id' =>$id])->orderby('id',"DESC")->get();
        return view('admin.rate-management', compact('rate','id'));
    }

     public function viewRate (Request $request,$id){
         $rate = Rate::where('id',$id)->first();
        return view ('admin.view-rate',compact('rate'));

    }

    public function deleteRate(Request $request)
    {
        $rate = Rate::where('id', '=', $request->rates_id)->delete();
           
        Session::flash('message', 'Rate has been deleted successfully.');
        return back()->withInput();
    }
    public function bloggerManagement(Request $request,$id) {
        $user = Auth::guard('admin')->user();
        $blogger = Blogger::where(['user_id'=>$id])->orderby('id',"DESC")->get();
        return view('admin.blogger-management', compact('blogger','id'));
    }

     public function viewBlogger (Request $request,$id){
         $blogger = Blogger::where('id',$id)->first();
        return view ('admin.view-bloggers',compact('blogger'));

    }

    public function deleteBlogger(Request $request)
    {
        $user = Blogger::where('id', '=', $request->bloggers_id)->delete();
           
        Session::flash('message', 'Blogger has been deleted successfully.');
        return back()->withInput();
    }
    
    public function hashTag(Request $request)
    {
         return view ('admin.hashtag');
    }

          
}

