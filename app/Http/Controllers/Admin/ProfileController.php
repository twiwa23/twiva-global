<?php
namespace App\Http\Controllers\Admin;
use DB;
use Auth;
use Hash;
use Session;
use Validator;
use App\model\Gig;
use Carbon\Carbon;
use App\model\Blog;
use App\model\Post;
use App\model\Rate;
use App\model\User;
use VideoThumbnail;
use App\model\Admin;
use App\model\Blogger;
use App\model\Country;
use App\model\Payment;
use App\model\WebUser;
use App\model\Category;
use App\model\Featured;
use App\model\Services;
use App\model\UserPost;
use App\model\Portfolio;
use App\model\AdminPayment;
use App\model\CategoryUser;
use Illuminate\Http\Request;
use App\Mail\AdminForgotPassword;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

header('Cache-Control: no-store, private, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);
class ProfileController extends Controller
{


    public function landingPage(Request $request)
    {
        $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
         $featured = Featured::get();
        return view('landing/landing-page',compact('featured','auth_id'));
    }

      public function userLogin(Request $request){

          $login_check = auth()->guard('web')->user();
            if($login_check){

                $auth_id = auth()->guard('web')->user()->id;
            }else{
                $auth_id = "";
            }
            if($request->isMethod("GET")){
                return view("landing/user-login",compact('auth_id'));
            }

           if($request->isMethod("POST")){

                  $message = [ 'email.required' => 'Please enter email', 'password.required' => 'Please enter password'

                ];

                $this->validate($request, ['email' => 'required', 'password' =>'required|max:30'

                ], $message);

               $email = $request->email;
                $password = $request->password;
                 $checkUserExist = User::where(['email' => $request->email, 'is_deleted' => "0"])->first();

                 if(!empty($checkUserExist)){
                   if ($checkUserExist->user_type == "B") {
                    Session::flash('danger', 'Please enter a valid email address');
                      return redirect("user/login");
                }
                if ($checkUserExist->emailStatus == 0) {
                    Session::flash('danger', 'Please verify your account');
                      return redirect("user/login");
                }
                }else{
                Session::flash('danger', 'Email address and password does not match');
                    return back()->withInput();
                }
                if(auth()->guard('web')->attempt(['email' => $email, 'password' => $password])) {
                   $login_token = str_random(32);
                    User::where(['email'=>$request->email,'password'=>$password])->update(['remember_token'=>$login_token]);
                    $request->session()->put('remember_token', $login_token);
                    $user_id = $checkUserExist->id;
                    $checInfulancerUser = User::where(['email' => $request->email, 'is_deleted' => "0","user_type"=>"I"])->first();

                    if(!empty($checInfulancerUser)){
                      $checkCategoryUser = CategoryUser::where(['user_id' => $checInfulancerUser->id])->first();
                      if(!empty($checkCategoryUser)){
                       return redirect('user/portfolio/'.$user_id);
                      }else{
                      return redirect('user/edit-profile/');
                      }
                     }
                       return redirect('user/portfolio/'.$user_id);

                }
                else
                {
                    Session::flash('danger', 'Email address and password does not match');
                    return back()->withInput();
                }

            }
        }

//     public function userLogin(Request $request){
//
//      $login_check = auth()->guard('web')->user();
//        if($login_check){
//
//            $auth_id = auth()->guard('web')->user()->id;
//        }else{
//            $auth_id = "";
//        }
//        if($request->isMethod("GET")){
//            return view("landing/user-login",compact('auth_id'));
//        }
//
//       if($request->isMethod("POST")){
//
//              $message = [ 'email.required' => 'Please enter email', 'password.required' => 'Please enter password'
//
//            ];
//
//            $this->validate($request, ['email' => 'required', 'password' =>'required|max:30'
//
//            ], $message);
//
//           $email = $request->email;
//            $password = $request->password;
//             $checkUserExist = User::where(['email' => $request->email, 'is_deleted' => "0"])->first();
//             if(!empty($checkUserExist)){
//
//            if ($checkUserExist->emailStatus == 0) {
//                Session::flash('danger', 'Please verify your account.');
//                  return redirect("user/login");
//            }
//            }else{
//            Session::flash('danger', 'Email address and password does not match.');
//                return back()->withInput();
//            }
//            if(auth()->guard('web')->attempt(['email' => $email, 'password' => $password])) {
//               $login_token = str_random(32);
//                User::where(['email'=>$request->email,'password'=>$password])->update(['remember_token'=>$login_token]);
//                $request->session()->put('remember_token', $login_token);
//                $user_id = $checkUserExist->id;
//                $checInfulancerUser = User::where(['email' => $request->email, 'is_deleted' => "0","user_type"=>"I"])->first();
//
//                if(!empty($checInfulancerUser)){
//                  $checkCategoryUser = CategoryUser::where(['user_id' => $checInfulancerUser->id])->first();
//                  if(!empty($checkCategoryUser)){
//                   return redirect('user/portfolio/'.$user_id);
//                  }else{
//                  return redirect('user/edit-profile/');
//                  }
//                 }
//                   return redirect('user/portfolio/'.$user_id);
//
//            }
//            else
//            {
//                Session::flash('danger', 'Email address and password does not match.');
//                return back()->withInput();
//            }
//
//        }
//    }

        // For signup
    public function userSignup(Request $request){
         $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        if($request->isMethod("GET")){
            $category = Category::where('id','<=',8)->get();
            $countries = Country::get();
            return view("landing/user-signup",compact('auth_id','category', 'countries'));
        }

        if($request->isMethod("POST")){
            // return $request->all();
              $message = ['name.required'=>'Please enter name','name.max' => 'name should not be greater than 50 characters', 'email.required' => 'Please enter email', 'password.required' => 'Please enter password', 'password.max' => 'Password should not be greater than 30 characters','confirm_password.required'=>"Please enter confirm password",'confirm_password.max' => 'Confirm password should not be greater than 30 characters','description.required'=>"Please enter description",'description.max' => 'Description should not be greater than 1000 characters','phone_number.digits_between'=>'Phone number should be 8 to 15 digits.','category_name.required'=>'Please select category name',
              'phone_code.required'=>'Please select Phone Code',

            ];

            $this->validate($request, ['name' => 'required|unique:users|min:3|max:50', 'email' => 'required|email|unique:users|max:200', 'password' =>'required|min:6|max:30',
               'description'=>'required|max:1000','confirm_password'=>'required|same:password|max:30','phone_number' =>  'required|digits_between:8,15','category_name'=>'required',
               'phone_code'=>'required',

            ], $message);

            $imageName = "";
            if ($request->hasFile("profile")){
                $image = $request->file('profile');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
           }
            $verify_link = str_random(20);
            $login_token = str_random(32);
            $phone_number = $request->phone_code . '-' . $request->phone_number;
            $updateUser = User::insertGetId(['name' => $request->name, 'email' => $request->email, 'profile' => $imageName,'password' => Hash::make($request->password),'user_type'=>'W', 'remember_token' => $login_token,'description'=>$request->description,'phone_number'=>$phone_number,'email_verify'=>$verify_link]);
            if(!empty($updateUser)){

            $insertCategory = CategoryUser::insert(['user_id'=>$updateUser,'category_id'=>$request->category_name]);
            }

             $url = url('user/verify/'.$verify_link);
             $user_data = User::where('id',$updateUser)->first();
            try{
                Mail::send('mail-verify',['url' => $url,'user_data' => $user_data], function ($m) use ($user_data) {
                        // $m->from('info@twiva.co.ke', 'Twiva');
                        $m->from(config('mail.from.address'), 'Twiva');
                        $m->to($user_data->email,'App User');
                        $m->subject('Email verification link');
                    });
                 Session::flash('message', 'Signup Successfully & email verification link has been sent to your registered email address..');
                  $request->session()->put('remember_token', $login_token);

              // Auth()->guard('web')->loginUsingId($updateUser, true);
            return redirect("user/login");
            }catch(\Exception $e){
                 //return response()->json('Oops Something wrong! ' . $e->getMessage());
                 Session::flash('danger', 'Oops Something wrong!.'.$e->getMessage());
                return back()->withInput();
            }

        }

    }

    public function verify(Request $request,$verify_email_token){
         $user = User::where(['email_verify'=>$verify_email_token])->first();
         //return $user; die;
        if($user) {
            $user->email_verify = null;
            $user->emailStatus = 1;
            $user->update();
            $title = "Email verified";
            $message = "Your email has been verified successfully. You may now login.";
            $type = "success";
            return view('email.feedback', compact('title', 'message', 'type'));
        }else {
            $title = "Invalid link";
            $message = "Your verification link is either expired or invalid.";
            $type = "danger";
            return view('email.feedback', compact('title', 'message', 'type'));
        }
    }
     public function checkEmail(Request $request){

        $email = $request->email;
        $user_find = User::whereEmail($email)->first();

        if(!empty($user_find)){
            return "1";
        }else{
            return "0";
        }
    }

    public function checkName(Request $request){
        $user = auth()->guard('web')->user();
        $name = $request->name;
        if(!empty($user)){
            $user_find = User::whereName($name)->where('id','!=', $user->id)->first();
        }else{
            $user_find = User::whereName($name)->first();
        }

        if(!empty($user_find)){
            return "1";
        }else{
            return "0";
        }
    }
//     public function checkName(Request $request){
//
//        $name = $request->name;
//        $user_find = User::whereName($name)->first();
//
//        if(!empty($user_find)){
//            return "1";
//        }else{
//            return "0";
//        }
//    }
   public function creators(Request $request)
    {
       $login_check = auth()->guard('web')->user();

        if($login_check){

           $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        $category_id = $request->get("category_id");
        $category = Category::get();
        $categoryUser = CategoryUser::get()->pluck("category_id");
        $users = User::select("users.name as user_name","users.id as user_id","users.profile as user_profile","users.background_image as background_image","users.description as user_description","categories.name as category_name","category_users.category_id as category_id","category_users.user_id as category_user_id")
            ->leftJoin('category_users','category_users.user_id', '=', 'users.id')
            ->leftJoin('categories','categories.id', '=', 'category_users.category_id')
            ->whereIn('category_users.category_id',$categoryUser);
          /* ->where("users.user_type","W")
            ->where("users.user_type","I");*/
         // ->orderby('users.id',"DESC");
          if($category_id){
            $uses = $users->where("category_users.category_id",$category_id);
          }

            $users = $users->paginate(6)->onEachSide(1);

        return view('landing/creators',compact('users','category','auth_id'));
    }


      public function portfolio(Request $request,$id = null)
     {
      $auth = auth()->guard('web')->user();
       if(!$id){
          $user_id = $auth->id;
        }else {
          $user_id = $id;
        }
        if($request->ajax()){
           if($request->hasFile("image")){
                 $image = $request->file('image');
                 $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                 $updateUser = User::where('id',$auth->id)->update(['background_image'=>$imageName]);
                }
        }
        if($request->isMethod("GET")){
        $media_type = $request->get("media-type");
       //return $auth;die;
        $portfolio = Portfolio::where('user_id',$user_id);
        // if($media_type){
        //   if($media_type == '1'){
        //     $portfolio = $portfolio->where("type","image");
        //   }else if($media_type == '2'){
        //     $portfolio = $portfolio->where("type","video");
        //   }
        // }
        $portfolio = $portfolio->get();
        $portfolio_images = Portfolio::where('user_id',$user_id)->where('type', 'image')->get();
        $portfolio_videos = Portfolio::where('user_id',$user_id)->where('type', 'video')->get();
        $service = Services::where('user_id',$user_id)->get();
        $rate = Rate::where('user_id',$user_id)->get();
        $blogger = Blogger::where('user_id',$user_id)->get();
        $categoryUser = CategoryUser::where('user_id',$user_id)->first();
        if($categoryUser) {
            $category = Category::where('id',$categoryUser->category_id)->first();
        } else {
            $category['name'] = "Other";
        }

        $user = User::where(['id'=>$user_id])->first();
        return view('landing/portfolio',compact('portfolio','service','rate','category','user','user_id','blogger', 'portfolio_images', 'portfolio_videos'));
    }
  }


      public function hireMe(Request $request,$id = null)
    {
        if ($request->isMethod('get'))
        {
            $user_id = $id;
             $user = User::where('id',$user_id)->first();
             return view('landing.hire-me',compact('user'));

        }
        if ($request->isMethod('post'))
        {
        $message = array(
            'email.required' => 'Please enter registered email address',
            'email.email' => 'Please enter a valid email address',
            'email.exists' => 'Enter registered email address',
            'email.max' => 'Email address should be maximum 200 characters',
            'text.required'=>"Please write message",
            'text.max'=>'Message should maximum 2000 characters',
            'subject.required'=>"Please enter subject",
            'subject.max'=>'Subject should be maximum 100 characters'
        );
        $validator = Validator::make($request->all() ,
         ['email' => 'required|email|max:200',
          'text'=>'required|max:2000',
          'subject'=>'required|max:100',


         ], $message);

        if ($validator->fails())
        {
            return back()
                ->withErrors($validator)->withInput();
        }

        $email   = $request->input('email');
        $data = $request->input('text');
         $to  = $request->get('to');
        $subject = $request->input('subject');
        try{
        Mail::send('hire-me-mail',['from' => $email,'subject'=>$subject,'data'=>$data,'to'=>$to], function ($m) use ($email) {
            // $m->from($email, 'Twiva');
            // $m->to('info@twiva.co.ke','App User');
            // $m->subject('contact us ');
            $m->from(config('mail.from.address'), 'Twiva');
            $m->to(config('mail.from.to'))->subject('contact us ');
        });
         Session::flash("message","Message Sent Successfully.");
          return redirect('user/hire-me/'.$id);
            }catch(\Exception $ex){
                return $ex->getMessage();
            return back()->with("error",$ex->getMessage());
            }
        return redirect('user/hire-me');
    }

    }

     public function viewPortfolio(Request $request,$id = null)
     {
       $auth = auth()->guard('web')->user();
       $user_id = $id;
       //return $auth;die;
          $media_type = $request->get("media-type");
       //return $auth;die;
        if($auth){
          $user_id = $auth->id;
        }
        $portfolio = Portfolio::where('user_id',$user_id);
        if($media_type){
          if($media_type == '1'){
            $portfolio = $portfolio->where("type","image");
          }else if($media_type == '2'){
            $portfolio = $portfolio->where("type","video");
          }
        }
        $portfolio = $portfolio->get();
        $service = Services::where('user_id',$user_id)->get();
        $rate = Rate::where('user_id',$user_id)->get();
        $categoryUser = CategoryUser::where('user_id',$user_id)->first();
        $category = Category::where('id',$categoryUser->category_id)->first();
          $user = User::where(['id'=>$user_id])->first();

         $user = User::where(['id'=>$user_id])->first();
        return view('landing/view-protfolio-detail',compact('portfolio','category','user','user_id'));
    }

     public function viewBlogger(Request $request,$id = null)
     {
       $auth = auth()->guard('web')->user();
        $user_id = $id;
       //return $auth;die;

        if($auth){
          $user_id = $auth->id;
        }
        $blogger = Blogger::where('user_id',$user_id);
        $categoryUser = CategoryUser::where('user_id',$user_id)->first();
        $category = Category::where('id',$categoryUser->category_id)->first();
          $user = User::where(['id'=>$user_id])->first();

         $user = User::where(['id'=>$user_id])->first();
         $blogger = $blogger->paginate(9);
        return view('landing/view-blogger',compact('blogger','category','user','user_id'));
    }


     public function viewBloggerDetail(Request $request,$id = null)
     {
       $auth = auth()->guard('web')->user();

        $blogger = Blogger::where('id',$id)->first();
        $categoryUser = CategoryUser::where('user_id',$blogger->user_id)->first();
        $category = Category::where('id',$categoryUser->category_id)->first();
        $user = User::where(['id'=>$blogger->user_id])->first();

         $user = User::where(['id'=>$blogger->user_id])->first();
        return view('landing/view-blogger-detail',compact('blogger','category','user'));
    }
    public function addServices(Request $request)
    {
        $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }

     if ($request->isMethod('get'))
        {

         return view('landing/add-services',compact('auth_id'));
        }

        if ($request->isMethod('post'))
        {
           //return $request->all();
            $message = ['title.required' => 'Please enter title', 'description.required' => 'Please enter description'];

            $this->validate($request, ['title' => 'required|max:30',  'description' => 'required|max:400',
            ], $message);

               $imageName = "";
            if ($request->hasFile("profile")){
                 $image = $request->file('profile');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
           }
                $updateBusiness = Services::insert(['title' => $request->title, 'text' => $request->description, 'profile' => $imageName,'user_id'=>$auth_id]);


            Session::flash('message', 'Service has been added successfully.');
            return redirect('user/portfolio/'.$auth_id);

        }

    }

    public function addBlogger(Request $request)
    {
        $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }

     if ($request->isMethod('get'))
        {

         return view('landing/add-blogger',compact('auth_id'));
        }

        if ($request->isMethod('post'))
        {
           //return $request->all();
            $message = [ 'description.required' => 'Please enter description'];

            $this->validate($request, ['description' => 'required|max:2000',
            ], $message);

                $updateBusiness = Blogger::insert(['description' => $request->description, 'user_id'=>$auth_id]);


            Session::flash('message', 'Blog has been added successfully.');
            return redirect('user/portfolio/'.$auth_id);

        }

    }
     public function addRate(Request $request)
    {
        $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }

     if ($request->isMethod('get'))
        {

         return view('landing/add-rate',compact('auth_id'));
        }

        if ($request->isMethod('post'))
        {

            $message = ['title.required' => 'Please enter  title', 'description.required' => 'Please enter description','amount.required'=>'Please enter amount'  ];

            $this->validate($request, ['title' => 'required|max:30',  'amount' => 'required|digits_between:1,5','description'=>'required|max:400'
            ], $message);

                $updateBusiness = Rate::insert(['title' => $request->title, 'rate' => $request->amount, 'text' => $request->description,'user_id'=>$auth_id]);


            Session::flash('message', 'Rate has been added successfully.');
            return redirect('user/portfolio/'.$auth_id);

        }

    }

         public function addPortfolio(Request $request)
      {
          $login_check = auth()->guard('web')->user();

          if($login_check){

              $auth_id = auth()->guard('web')->user()->id;
          }else{
              $auth_id = "";
          }

       if ($request->isMethod('get'))
          {

           return view('landing/add-portfolio',compact('auth_id'));
          }

          if ($request->isMethod('post'))
          {
            // return $request->all();
             $video_image_types = ["video/x-flv","video/mp4","application/x-mpegURL","video/MP2T","video/3gp","video/quicktime","video/x-msvideo","video/x-ms-wmv","image/jpg","image/jpeg","image/png",
                          ];
                //return $request->all();
              if($request->hasFile("image_video")){
                   $value = $request->file("image_video");
                      if(!in_array($value->getMimeType(),$video_image_types)){
                          return back()->withInput()->with("danger","Please upload jpg, jpeg, png, mp4, 3gp, mov, avi file only");
                      }else if($value->getSize() > 20971520){ /*define size of file*/
                          return back()->withInput()->with("danger","File should not be greater than 20 MB");
                      }



              }
              $message = ['image_video.required'=>'Please select portfolio image or video'];



                 $video_images = $request->file("image_video");
                  $type = $value->getMimeType();

                  $file = $this->uploadImage($video_images,"/app/public/uploads/images/business", 1);
                  if($type == "image/jpeg" || $type == "image/jpg" || $type == "image/png"){
                    $type = "image";
                     $thumbnail_name = "";
                  }else{
                  $type = "video";

                  $video_path = url("/public/storage/uploads/images/business").'/'.$file;
                  $save_path = storage_path("/app/public/uploads/images/business");
                  $thumbnail_name  = uniqid()."_thumbnail.png";
                  $thumbnail = VideoThumbnail::createThumbnail($video_path, $save_path,$thumbnail_name, 3, 320, 240);
                 }

                 // $file = $this->uploadImage($video_images,"/app/public/uploads/images/business", 1);
                  $updateBusiness = Portfolio::insert(['thumbnail_name' => $thumbnail_name,'type'=>$type,'profile' => $file,'user_id'=>$auth_id]);


              Session::flash('message', 'Portfolio has been added successfully.');
              return redirect('user/portfolio/'.$auth_id);

          }

      }

     public function updateProfile(Request $request){

         $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        if($request->isMethod("GET")){
            $user_category_id = CategoryUser::where('user_id',$auth_id)->pluck('category_id')->first();
            $category = Category::get();
            $user = User::where(['id'=>$auth_id])->first();
            return view("landing/edit-profile",compact('auth_id','user_category_id','user','category'));
        }


        if($request->isMethod("POST")){
            //return  $request->all();
            //return $_FILES;
              $message = ['name.required'=>'Please enter name','name.max' => 'name should not be greater than 50 characters', 'description.required'=>"Please enter description",'description.max' => 'Description should not be greater than 5000 characters','category_name.required'=>'Please select category name',

            ];

            $this->validate($request, ['name' => 'required|max:50|unique:users,name,'.$auth_id,
               'description'=>'required|max:1000','category_name'=>'required'

            ], $message);



            if ($request->hasFile("profile")){
                 $image = $request->file('profile');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                 $updateUser = User::where('id',$auth_id)->update(['name' => $request->name, 'description'=>$request->description,'profile'=>$imageName]);
                  $checkUser = CategoryUser::where(['user_id'=>$auth_id])->first();
                  if(!empty($checkUser)){

                 $UpdateCategory = CategoryUser::where('user_id',$auth_id)->update(['category_id'=>$request->category_name]);
               }else{
                $insertCategory = CategoryUser::insert(['user_id'=>$auth_id,'category_id'=>$request->category_name]);
               }

               }

             if($request->hasFile("background_image")){
                 $image = $request->file('background_image');
                 $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                 $updateUser = User::where('id',$auth_id)->update(['background_image'=>$imageName]);
                }

               $updateUser = User::where('id',$auth_id)->update(['name' => $request->name, 'description'=>$request->description,]);
               $checkUser = CategoryUser::where(['user_id'=>$auth_id])->first();
                if(!empty($checkUser)){

                 $UpdateCategory = CategoryUser::where('user_id',$auth_id)->update(['category_id'=>$request->category_name]);
               }else{
                $insertCategory = CategoryUser::insert(['user_id'=>$auth_id,'category_id'=>$request->category_name]);
               }

            }

             Session::flash('message', 'Profile updated has been successfully. ');
            return redirect("user/portfolio/".$auth_id);




    }

     public function userlogout(Request $request) {
        $user = Auth::guard('web')->user();
        Auth()->guard('web')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        Session::flash('success', 'Logout successfully.');
        return redirect(url('/user/creators'))->withInput();
    }



    public function contactUs(Request $request)
    {
       // $auth_id = auth()->guard('web')->User()->id;
        if ($request->isMethod('get'))
        {
             return view('landing/contact-us');
        }
        if ($request->isMethod('post'))
        {
        $message = array(
            'email.required' => 'Please enter registered email address',
            'email.email' => 'Please enter a valid email address',
            'email.exists' => 'Enter registered email address'

        );
        $validator = Validator::make($request->all() ,
         ['email' => 'required|email',
          'message'=>'nullable|max:500',
          'subject'=>'required|max:100',
          'first_name'=>'required|max:50',
          'last_name'=>'required|max:50',

         ], $message);

        if ($validator->fails())
        {
            return back()
                ->withErrors($validator)->withInput();
        }

        $email   = $request->input('email');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $data = $request->input('message');
        $subject = $request->input('subject');
        try{
        Mail::send('contact-us',['first_name'=>$first_name,'last_name'=>$last_name,'email' => $email,'subject'=>$subject,'data'=>$data], function ($m) use ($email) {
            $m->from(config('mail.from.address'), 'Twiva');
            $m->to(config('mail.from.to'),'App User');
            $m->subject('contact us ');
        });
         Session::flash("success","Contact request sent successfully.");
          return redirect('contact-us');
            }catch(\Exception $ex){
                return $ex->getMessage();
            return back()->with("error",$ex->getMessage());
            }
        return redirect('contact-us');
    }
}

     public function blog(Request $request)
    {
       $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        $blog = blog::get();
        return view('landing/blog',compact('blog','auth_id'));
    }

    public function eShop()
    {
        return view('landing.eshop');
    }

     public function viewBlog(Request $request,$id)
    {
        $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        $id = base64_decode($id);
        $blog = blog::where('id',$id)->first();
        return view('landing/view-blog',compact('blog','auth_id'));
    }

    public function faq(Request $request)
    {
       $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        return view('landing/faq',compact('auth_id'));
    }
    public function influencer(Request $request)
    {
     $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        $featured = Featured::get();
        $featuredWithSocialType = [];
        foreach($featured as $feature){
            $twitter = Featured::where('id' , $feature->id)->where('link' , 'like' , '%'.'twitter'.'%')->first();
            $instagram = Featured::where('id' , $feature->id)->where('link' , 'like' , '%'.'instagram'.'%')->first();
            $facebook = Featured::where('id' , $feature->id)->where('link' , 'like' , '%'.'facebook'.'%')->first();
            if(!is_null($twitter)){
                $feature['type'] = 't';
                $featuredWithSocialType[] = $feature;
            }
            if(!is_null($instagram)){
                $feature['type'] = 'i';
                $featuredWithSocialType[] = $feature;
            }
            if(!is_null($facebook)){
                $feature['type'] = 'f';
                $featuredWithSocialType[] = $feature;
            }
        }
        $featured = $featuredWithSocialType;
        return view('landing/influencer',compact('featured', 'auth_id'));
    }
    public function policy(Request $request)
    {
       $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        return view('landing/policy',compact('auth_id'));
    }
    public function termsConditions(Request $request)
    {
     $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        return view('landing/terms-conditions',compact('auth_id'));
    }
    public function aboutUs(Request $request)
    {
        $login_check = auth()->guard('web')->user();

        if($login_check){

            $auth_id = auth()->guard('web')->user()->id;
        }else{
            $auth_id = "";
        }
        return view('landing/about-us',compact('auth_id'));
    }
    public function login(Request $request)
    {
        if ($request->isMethod('get'))
        {

            if (!empty(auth()->guard('admin')->user()->id))
            {
                return redirect('admin/index');
            }

            return view('admin.login');
        }

        if ($request->isMethod('post'))
        {
            $messages = ['email.email' => 'Please enter valid email address', 'email.exists' => 'Please enter registered email address', 'email.required' => 'Please enter email address', 'password.required' => 'Please enter password'];
            $data = $this->validate($request, ['email' => 'required|email', 'password' => 'required|min:6', ], $messages);

            $email = $request->email;
            $password = $request->password;

            if (auth()->guard('admin')->attempt(['email' => $email, 'password' => $password]))
            {
                $loginToken = str_random(32);
                Admin::where(['email' => $request->email])->update(['login_token' => $loginToken]);
                $request->session()->put('login_token_admin', $loginToken);
                return redirect('admin/index');
            }
            else
            {
                Session::flash('danger', 'Email address and password does not match.');
                return back()->withInput();
            }

        }

    }

    public function forgotPassword(Request $request)
    {

        if ($request->isMethod('post'))
        {
            $message = array(
                'email.required' => 'Please enter registered email address',
                'email.email' => 'Please enter a valid email address',
                'email.exists' => 'Enter registered email address'
            );
            $validator = Validator::make($request->all() , ['email' => 'required|email', 'email' => 'required|email|exists:admins,email', ], $message);

            if ($validator->fails())
            {
                return back()
                    ->withErrors($validator)->withInput();
            }
            $checkUserExist = Admin::where(['email' => $request->input('email') ,'type'=>"admin" ])
                ->first();
            if (!empty($checkUserExist))
            {

                $resetPasswordToken = str_random(40);
                $url = url('admin/reset-password/' . $resetPasswordToken);

                $updateData = Admin::where('id', $checkUserExist->id)
                              ->update(['reset_password_token' => $resetPasswordToken,
                               'updated_at' => Date('Y-m-d H:i:s') ]);

                try
                {
                    $userData = Admin::where('id', $checkUserExist->id)
                        ->first();
                    Mail::send('admin/email_forget', ['url' => $url, 'user_data' => $userData], function ($m) use ($userData)
                    {
                        $m->from(config('mail.from.address'), 'Twiva');
                        $m->to($userData->email, 'App User');
                        $m->subject('Forgot Password link');
                    });
                    Session::flash('message', 'Forgot password email has been sent to your registered email address.');
                    return back()->withErrors($message)->withInput();
                }
                catch(\Exception $e)
                {
                    $message = array(
                        'Oops Something wrong! ' . $e->getMessage()
                    );
                    return back()->withErrors($message)->withInput();
                }
            }
            else
            {
                Session::flash('danger', 'This mail address does not register with us.');
                return back()->withErrors($message)->withInput();
            }
        }
        else
        {
            return view('admin/forgot-password');
        }
    }

    public function resetPassword(Request $request, $token)
    {
        $checkToken = Admin::where(['reset_password_token' => $token])->first();
        if (!empty($checkToken))
        {
            $current_time = Carbon::now();
            $startTime = Carbon::parse($current_time);
            $finishTime = Carbon::parse($checkToken->updated_at);
            $difference = $finishTime->diffInMinutes($startTime);
            if ($difference > 9)
            {
                return redirect(url("admin/reset-password-invalid"));
            }

            if ($request->isMethod('post'))
            {
                $token = $request->reset_token;
                $message = ['password.required' => 'Please enter password', 'password.min' => 'Password must be at least 6 characters', 'password_confirmation.required' => "Please enter confirm password", 'password_confirmation.min' => "Confirm Password must be at least 6 characters", 'password_confirmation.same' => "Password and confirm password does not match"];
                $this->validate($request, ['password' => 'required|min:6|max:30', 'password_confirmation' => 'required|same:password|min:6|max:30', ], $message);

                $updateData = Admin::where('id', $checkToken->id)
                    ->update(['password' => Hash::make($request->input('password')) , 'visible_pwd' => $request->input('password') , 'updated_at' => Date('Y-m-d H:i:s') , 'reset_password_token' => '', 'reset_password_token' => '']);

                return redirect(url("admin/password-reset-success"));

            }
            else
            {
                return view('admin.reset-password');
            }

        }
        else
        {
            return redirect(url("admin/reset-password-invalid"));
        }
    }

    public function landingForgotPassword(Request $request)
    {

        if ($request->isMethod('post'))
        {
            $message = array(
                'email.required' => 'Please enter email address',
                'email.email' => 'Please enter valid email address',
                'email.exists' => 'Enter registered email address'
            );
            $validator = Validator::make($request->all() , ['email' => 'required|email|exists:users,email', ], $message);

            if ($validator->fails())
            {
                return back()
                    ->withErrors($validator)->withInput();
            }
            $checkUserExist = User::where(['email' => $request->input('email') ])
                ->first();
            if (!empty($checkUserExist))
            {

                $resetPasswordToken = str_random(40);
                 $url = url('user/reset-password/'.$resetPasswordToken);

                $updateData = User::where('id', $checkUserExist->id)
                              ->update(['reset_password_token' => $resetPasswordToken,
                               'updated_at' => Date('Y-m-d H:i:s') ]);

                try
                {
                    $userData = User::where('id', $checkUserExist->id)
                        ->first();
                    Mail::send('mail-forgot', ['url' => $url, 'user_data' => $userData], function ($m) use ($userData)
                    {
                        $m->from(config('mail.from.address'), 'Twiva');
                        // $m->from('info@twiva.co.ke', 'TWIVA');
                        $m->to($userData->email, 'App User');
                        $m->subject('Forgot Password link');
                    });
                    Session::flash('message', 'Forgot password email has been sent to your registered email address.');
                    return redirect('/user/login');
                   //return back()->withErrors($message)->withInput();
                }
                catch(\Exception $e)
                {
                    $message = array(
                        'Oops Something wrong! ' . $e->getMessage()
                    );
                    return back()->withErrors($message)->withInput();
                }
            }
            else
            {
                $message = array(
                    'This mail address does not register with us .'
                );
                return back()->withErrors($message)->withInput();
            }
        }
        else
        {
            return view('landing/forgot-password');
        }
    }

    public function landingResetPassword(Request $request, $token)
    {
        $checkToken = User::where(['reset_password_token' => $token])->first();
       // return $checkToken;
        if (!empty($checkToken))
        {
            $current_time = Carbon::now();
            $startTime = Carbon::parse($current_time);
            $finishTime = Carbon::parse($checkToken->updated_at);
            $difference = $finishTime->diffInMinutes($startTime);
            if ($difference > 9)
            {
                return redirect(url("user/reset-password-invalid"));
            }

            if ($request->isMethod('post'))
            {
                $token = $request->reset_token;
                $message = ['password.required' => 'Please enter password', 'password.min' => 'Password must be at least 6 characters','password.max' => 'Password maximum 30 characters', 'confirm_password.required' => "Please enter confirm password", 'confirm_password.min' => "Confirm Password must be at least 6 characters", 'confirm_password.same' => "Password and confirm password does not match"];
                $this->validate($request, ['password' => 'required|min:6|max:30', 'confirm_password' => 'required|same:password|min:6', ], $message);

                $updateData = User::where('id', $checkToken->id)
                    ->update(['password' => Hash::make($request->input('password')) , 'reset_password_token' => '']);

                return redirect(url("user/password-reset-success"));

            }
            else
            {
                return view('landing.reset-password');
            }

        }
        else
        {
            return redirect(url("user/reset-password-invalid"));
        }
    }

    public function updateEmail(Request $request)
    {
        if ($request->isMethod('get'))
        {
            $users = Auth::guard('admin')->user();
            return view('admin.manage-email', compact('users'));
        }

        if ($request->isMethod('post'))
        {
            $users = Auth::guard('admin')->user();
            $messages = ['email' => 'Please enter valid email address', 'email.required' => 'Please enter email address','phone_number'=>'Please enter phone number' ];

            $this->validate($request, ['email' => "required|email",

            'phone_number'=>'nullable|digits_between:7,15',
        ], $messages);

            $users->email = $request->email;
            $users->phone_number = $request->phone_number;
            if ($users->update())
            {
                Session::flash('message', 'Profile has been updated successfully.');
                return redirect(url('admin/manage-email'))->withInput();
            }
        }

    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('get'))
        {

            return view('admin.change-password');
        }

        if ($request->isMethod('post'))
        {
            $message = ['old_password.required' => 'Please enter old password', 'new_password.required' => 'Please enter new password', 'confirm_password.required' => "Please enter confirm password", 'confirm_password.same' => "Password and confirm password does not match"];
            $this->validate($request, ['old_password' => 'required', 'new_password' => 'required|min:6,max:20', 'confirm_password' => 'required|required_with:new_password|same:new_password'], $message);

            $old_password = $request->old_password;
            $new_password = $request->new_password;
            $admin = Auth::guard('admin')->user();

            if (Hash::check($old_password, $admin->password))
            {
                $update_data = Admin::where('id', $admin->id)
                               ->update(['password' => Hash::make($request->new_password),
                               'visible_pwd' => $request->new_password]);
                Auth()->guard()->logout();
                $request->session()->flush();
                $request->session()->regenerate();

                Session::flash('message', 'Password has been changed successfully.');
                return redirect(url('admin/login'))->withInput();
            }
            else
            {
                Session::flash('danger', "Failed to change password because old password is wrong.");
                return redirect(url('admin/change-password'))->withInput();
            }

        }
    }


    public function viewEmail(Request $request)
    {
        $user = Auth::guard('admin')->user();
        return view('admin.edit-email', compact('user'));

    }

    public function index(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $subadmin_user_id = $user->id;
        $currentMonth = date('m');
        $monthly_influencer_user = User::whereRaw('MONTH(created_at) = ?', [$currentMonth])->where(['is_deleted'=>"0",'user_type'=>"I"])->count();
        $influencer_user = User::where(['is_deleted'=>"0","user_type"=>"I"])->count();
        $monthly_brand_user = User::whereRaw('MONTH(created_at) = ?', [$currentMonth])->where(['is_deleted'=>"0",'user_type'=>"B"])->count();
        $brand_user = User::where(['is_deleted'=>"0","user_type"=>"B"])->count();
        //$monthly_post = Post::whereRaw('MONTH(created_at) = ?', [$currentMonth])->count();
       $monthly_post = Post::select("posts.id as post_id","post_users.post_id as post_user_id","posts.created_at as date","post_users.status as posts_status")
       ->leftJoin('post_users','post_users.post_id', '=', 'posts.id')
       ->where('post_users.status',5)
       ->whereMonth('posts.created_at',$currentMonth)
       ->distinct('post_users.post_id')->count('post_users.post_id');
       $total_post = Post::count();
       $total_gig = Gig::count();
       //$monthly_gig = Gig::whereRaw(['MONTH(created_at) = ?'=>$currentMonth)->count();
        $monthly_gig = Gig::select("gigs.id as gig_id","gig_users.gig_id as gigs_user_id","gigs.created_at as date","gig_users.status as gig_status")
       ->leftJoin('gig_users','gig_users.gig_id', '=', 'gigs.id')
       ->where('gig_users.status',4)
       ->whereMonth('gigs.created_at',$currentMonth)
       ->distinct('gig_users.gig_id')->count('gig_users.gig_id');

        $adminPayment = AdminPayment::where('status',1)->sum('amount');

        return view('admin.index', compact('monthly_influencer_user','influencer_user','monthly_brand_user','brand_user', 'monthly_post','total_post','monthly_gig', 'total_gig', 'adminPayment','subadmin_user_id'));
    }



//    public function index(Request $request)
//    {
//        $user = Auth::guard('admin')->user();
//         $subadmin_user_id = $user->id;
//        $currentMonth = date('m');
//        $monthly_influencer_user = User::whereRaw('MONTH(created_at) = ?', [$currentMonth])->where(['is_deleted'=>"0",'user_type'=>"I"])->count();
//        $influencer_user = User::where(['is_deleted'=>"0","user_type"=>"I"])->count();
//        $monthly_brand_user = User::whereRaw('MONTH(created_at) = ?', [$currentMonth])->where(['is_deleted'=>"0",'user_type'=>"B"])->count();
//        $brand_user = User::where(['is_deleted'=>"0","user_type"=>"B"])->count();
//        $monthly_post = Post::whereRaw('MONTH(created_at) = ?', [$currentMonth])->count();
//        $total_post = Post::count();
//        $monthly_gig = Gig::whereRaw('MONTH(created_at) = ?', [$currentMonth])->count();
//        $total_gig = Gig::count();
//        //$payment = Payment::whereRaw('MONTH(created_at) = ?', [$currentMonth])->sum('amount');
//        //  $payment = Payment::sum('amount');
//        //$adminPayment = AdminPayment::whereRaw('MONTH(created_at) = ?', [$currentMonth])->where('status',1)->sum('amount');
//          $adminPayment = AdminPayment::where('status',1)->sum('amount');
//        /*$adminAmount = AdminPayment::whereRaw('MONTH(created_at) = ?', [$currentMonth])->where('status',1)->sum('amount');*/
//       // $adminAmount = AdminPayment::where('status',1)->sum('amount');
//       // $percentage = 15;
//       // $adminProfit = ($adminPayment / 100) * $percentage;
//       // $profit = $adminProfit;
//
//
//        return view('admin.index', compact('monthly_influencer_user','influencer_user','monthly_brand_user','brand_user', 'monthly_post','total_post','monthly_gig', 'total_gig', 'adminPayment','subadmin_user_id'));
//    }

    public function logout(Request $request)
    {
        $user = Auth::guard('admin')->user();

        $request->session()->flush();

        $request->session()->regenerate();
        Session::flash('message', 'Logout successfully.');
        return redirect(url("admin/login"))->withInput();
    }
    public function viewMessageResetPassword()
    {
        $title = "Password Reset Success";
        $message = "Password has been reset successfully.";
        $type = "success";
        return view('email.feedback', compact('title', 'message', 'type'));
    }

    public function resetPasswordInvalid()
    {
        $title = "Invalid link";
        $message = "The forgot password link you clicked is invalid or expired.";
        $type = "danger";
        return view('email.feedback', compact('title', 'message', 'type'));
    }


public function uploadImage($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false)
    {
        /* user can send single or multiple image in first argument can accept request and image path also*/

        /* all images are saving in storage folder which is root*/
        $return_data = "";
        $profile = null;
        if ($multiple)
        {
            $profile = $request_profile;
        }
        else if ($request_profile->hasFile("image"))
        {
            $profile = $request_profile->file("image");
        }

        if ($profile)
        {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalExtension();
            $file_name = uniqid() . "." . $file_name;
            $file_name = str_replace(array(
                " ",
                "(",
                ")"
            ) , "", $file_name);
            if (!file_exists($path))
            {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            //chmod($path,0775);
            $return_data = $file_name;
        }

        return $return_data;
    }
}
