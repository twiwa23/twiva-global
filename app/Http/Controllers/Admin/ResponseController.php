<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;
use Storage;
use Auth;
use DateTime;

use App\model\User;
use Mail;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;
use App\Http\Controllers\Controller;


date_default_timezone_set('UTC');
class ResponseController extends Controller
{	
	public function is_require($data,$field)
	{
	   if(empty($data)){
		   $message = $field." field is required";
		   http_response_code(400);
		   // echo json_encode(['result'=>'Failure','message'=>$message]);exit;
		   echo json_encode(['return'=>0,'result'=>'Failure','message'=>$message]);exit;
	   }else{
		   return $data;
	   } 
	}
   
	public function responseOk($message,$data)
	{
		if(!empty($data)){
		   http_response_code(200);
		   // echo json_encode(['result'=>'Success','message'=>$message,'data'=>$data]);exit;
			echo json_encode(['return'=>1,'result'=>'Success','message'=>$message,'data'=>$data]);exit;
		}else{
			http_response_code(200);
		   // echo json_encode(['return'=>1,'result'=>'Success','message'=>$message]);exit;
			echo json_encode(['return'=>1,'result'=>'Success','message'=>$message]);exit;
		}
	}


   public function uploadImage($request_profile,String $dir="/app/public/uploads/images/business",$multiple = false){
        /* user can send single or multiple image in first argument can accept request and image path also*/

        /* all images are saving in storage folder which is root*/
      $return_data = " ";
        $profile = null;
        if($multiple){
            $profile = $request_profile;
        }else if($request_profile->hasFile("profile")){
            $profile = $request_profile->file("profile");
        }

        if($profile){
            $path = storage_path().$dir;
            $file_name = $profile->getClientOriginalName();
            $file_name = uniqid()."_".$file_name;
            $file_name = str_replace(array(" ","(",")"),"",$file_name);
            if(!file_exists($path)){
              mkdir(url($dir));
            }

            $profile->move($path,$file_name);
           //chmod($path,0775);
            $return_data = $file_name;
        }

       return $return_data;
    }

      public function addImages(Request $request,$user_id){
        $is_inserted = 0;
        if($request->hasFile("images")){
            $images = $request->file("images");
            $count = count($images);
            if($count > 0){
                for($i = 0; $i < $count;$i++){
                   $image_name = $this->uploadImage($images[$i],"/app/public/uploads/images/influencer_images",1);
                    InfluencerImages::create(["user_id" => $user_id,"image" => $image_name]);
                    $is_inserted++;
                }
            }
        }
        if($is_inserted > 0){
            return response()->json(["message" => "Interest image saved successfully"]);
        }
    }
	public function responseOkWithoutData($message)
	{
		http_response_code(204);
		return response()->json(['return'=>1,'result'=>'Success','message'=>$message]);
		exit;
	}
   
	public function responseWithError($message)
	{
		http_response_code(400);
		// echo json_encode(['result'=>'Failure','message'=>$message]);exit;
	    echo json_encode(['return'=>0,'result'=>'Failure','message'=>$message]);
	    exit;
    }
   
		
    public function checkUserExist()
	{
	   $userId = Auth::user()->id;
	 //  print $userId;die;
	   if(!empty($userId)){
		   $get_data = User::where('id',$userId)->first();
		   if(!empty($get_data)){
			   return $get_data;
		   }else{
			   http_response_code(401);
			   echo json_encode(['result'=>'Failure','message'=>'user does not exist']);exit;
		   }
		}else{
			http_response_code(401);
		   echo json_encode(['result'=>'Failure','message'=>'user does not exist']);exit;
		}
	}
								
   
	
	public static function randomNumber($length)
	{
		$result = '';
		for($i = 0; $i < $length; $i++) {
			$result .= mt_rand(0, 9);
		}
		return $result;
	}


	public function uploadImage($user_id,$image,$image_folder_name)
    {
        $image_name = str_random(20)."_$user_id.png";
        $path = Storage::putFileAs("public/$image_folder_name", $image, $image_name);
        $img_url =  "/storage/$image_folder_name/".$image_name;
        return $img_url;
    }

    public function copyImageFromLink($user_id,$image,$image_folder_name){
		$contents = file_get_contents($image);
		$image_name = str_random(20)."_new.png";
		Storage::put("public/user_image/$image_name", $contents);
		$image_url = "/storage/user_image/".$image_name;
		return $image_url;
    }

    public function sendMail($email_to,$mail_template,$subject,$data = array()){
		
    	try{
			Mail::send($mail_template,$data, function ($m) use ($data,$email_to,$subject) {
				$m->from(env('MAIL_FROM'), 'UWIN');
				$m->to($email_to,'App User');
				$m->cc('deftsofttesting786@gmail.com','App User');  
				$m->subject($subject);
			});
			return array('status' => 1, 'msg' => 'done');
		}catch(\Exception $e){
			return array('status' => 2,'msg' => 'Oops Something wrong! '.$e->getMessage());
		}
    }

    public function refreshAccessToken($email,$password,$user_id){
    	try{
			$http = new GuzzleHttp\Client;
			$url = Url('oauth/token');
			$response = $http->post($url, [
				'form_params' => [
					'grant_type' => 'password',
					'client_id' => 2,
					'client_secret' => env('TOKEN_CLIENT_SECRET'),
					'username' => $email,
					'password' => $password,
					'role' => 2,
					'scope' => '*',
				],
			]);
			$return_data = json_decode($response->getBody(), true);
			if(!empty($return_data)){
				User::where(['id' => $user_id])->update(['refresh_token' =>$return_data['refresh_token']]);
				return array('status' => 1,'return_data'=>$return_data);
			}else{
				return array('status' => 2,'msg' => 'Oops Something wrong!');
			}
		}catch(\Exception $e){
			return array('status' => 2,'msg' => 'Oops Something wrong! '.$e->getMessage());
		}
	}

	function refferal_num($insert_user){
			$size=3;
			$alpha_key = $insert_user;
			$keys = range('A', 'Z');
			for ($i = 0; $i < 2; $i++) {
				$alpha_key .= $keys[array_rand($keys)];
			}
			$length = $size - 2;
			$key = '';
			$keys = range(0, 9);
			for ($i = 0; $i < $length; $i++) {
				$key .= $keys[array_rand($keys)];
			}
			return $alpha_key . $key;
	}


	public function callApi($url)
	{
		try{
			$http = new GuzzleHttp\Client;
			$response = $http->get($url);
			$return_data = json_decode($response->getBody(), true);
			if(!empty($return_data)){
				return $return_data;
			}else{
				return 0;
			}
		}catch(\Exception $e){
			return 0;
		}

	}
		
}
