<?php

namespace App\Http\Controllers\Business;

header('Cache-Control: no-store, private, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);

use DB;
use Hash;
use Session;
use App\model\User;
use App\model\BusinessDetail;
use App\model\MpesaPhoneNumber;
use Validator;
use Auth;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
class BusinessProfileController extends Controller
{
    public function login(Request $request)
    {
        if ($request->isMethod("Get")) {

         // return auth()->guard('business')->user();
            if (auth()->guard('business')->check()) {
                return redirect(url('business/create-post-field'));
            }
            //return view('business.pages.profile.login');
            return Redirect::to('https://business.twiva.co.ke/');
        }

        if ($request->isMethod("POST")) {
            $message = [ 'email.required' => 'Please enter email', 'password.required' => 'Please enter password'

              ];

            $this->validate($request, ['email' => 'required', 'password' =>'required'

              ], $message);

            $email = $request->email;
            $password = $request->password;
            $checkUserExist = User::where(['email' => $request->email, 'is_deleted' => "0",'user_type'=>"B"])->first();

            if (!empty($checkUserExist)) {
                if ($checkUserExist->emailStatus == 0) {
                    Session::flash('danger', 'Please verify your account first.');
                    return redirect("/business/login");
                }
                if ($checkUserExist->status == "2") {
                    Session::flash('danger', 'Your account has been block by admin.');
                    return redirect("/business/login");
                }
            } else {
                Session::flash('danger', 'Email address and password does not match.');
                return back()->withInput();
            }
            if (auth()->guard('business')->attempt(['email' => $email, 'password' => $password])) {
                $login_token = str_random(32);
                $user = User::where(['email'=>$request->email,'password'=>$password])->update(['remember_token'=>$login_token,'device_id'=>'','device_type'=>'']);
                $data = User::where(['email'=>$request->email])->first();
                $user_data = $data->createToken('*')->accessToken;

                if ($data->login_status == 0) {
                    return redirect('business/create-gig-desc');
                } else {
                    $update_login_data = User::where(['email'=>$request->email,'password'=>$password])->update(['login_status'=>1]);
                    return redirect(url('business/create-post-field'));
                }
                //$request->session()->put('remember_token', $login_token);
                   // return redirect('business/post-list');
            } else {
                Session::flash('danger', 'Email address and password does not match.');
                return back()->withInput();
            }
        }
    }


    public function checkEmail(Request $request)
    {
        $email = $request->email;
        if ($email) {
            $user_find = User::whereEmail($email)->first();
            if ($user_find) {
                echo(json_encode("Email already exists"));
            } else {
                echo(json_encode(true));
            }
        }
    }




    public function forgotPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $message = array(
                   'email.required' => 'Please enter email address.',
                  'email.email' => 'Please enter valid email address.',
                  'email.exists' => 'Please enter registered email address.'
              );
            $validator = Validator::make($request->all(), ['email' => 'required|email|exists:users,email', ], $message);

            if ($validator->fails()) {
                return back()
                      ->withErrors($validator)->withInput();
            }
            $checkUserExist = User::where(['email' => $request->input('email'),'user_type'=>"B"])
                  ->first();
            if (!empty($checkUserExist)) {
                if ($checkUserExist->status == "2") {
                    Session::flash('danger', 'Your account has been block by admin.');
                    return redirect("/business/forgot-password");
                }
                $resetPasswordToken = str_random(40);
                $url = url('business/reset-password/'.$resetPasswordToken);

                $updateData = User::where('id', $checkUserExist->id)
                                ->update(['reset_password_token' => $resetPasswordToken,
                                 'updated_at' => Date('Y-m-d H:i:s') ]);

                try {
                    $userData = User::where('id', $checkUserExist->id)
                          ->first();
                    Mail::send('mail-forgot', ['url' => $url, 'user_data' => $userData], function ($m) use ($userData) {
                        $m->from('info@twiva.co.ke', 'TWIVA');
                        $m->to($userData->email, 'App User');
                        $m->subject('Forgot Password link');
                    });
                    Session::flash('success', 'A forgot password email has been sent to your registered email address.');
                    return redirect('business/login');
                    //return back()->withErrors($message)->withInput();
                } catch (\Exception $e) {
                    $message = array(
                          'Oops Something wrong! ' . $e->getMessage()
                      );
                    return back()->withErrors($message)->withInput();
                }
            } else {
                Session::flash('danger', 'Email address is not registered with us.');

                return back()->withInput();
            }
        } else {
            //return view('business.pages.profile.forgot');
            return Redirect::to('https://business.twiva.co.ke/business/forgot-password.php');
        }
    }

    public function resetPassword(Request $request, $token)
    {
        $checkToken = User::where(['reset_password_token' => $token])->first();

        if (!empty($checkToken)) {
            $current_time = Carbon::now();
            $startTime = Carbon::parse($current_time);
            $finishTime = Carbon::parse($checkToken->updated_at);
            $difference = $finishTime->diffInMinutes($startTime);
            if ($difference > 9) {
                return redirect(url("business/reset-password-invalid"));
            }

            if ($request->isMethod('post')) {
                $token = $request->reset_token;
                $message = ['password.required' => 'Please enter password', 'password.min' => 'Password must be at least 6 characters','password.max' => 'Password maximum 30 characters', 'confirm_password.required' => "Please enter confirm password", 'confirm_password.min' => "Confirm Password must be at least 6 characters", 'confirm_password.same' => "Password and confirm password does not match"];
                $this->validate($request, ['password' => 'required|min:6|max:30', 'confirm_password' => 'required|same:password|min:6', ], $message);

                $updateData = User::where('id', $checkToken->id)
                      ->update(['password' => Hash::make($request->input('password')) , 'reset_password_token' => '']);

                return redirect(url("business/password-reset-success"));
            } else {
                return view('business.pages.profile.reset-password');
            }
        } else {
            return redirect(url("business/reset-password-invalid"));
        }
    }
    public function viewMessageResetPassword()
    {
        $title = "Password Reset Success";
        $message = "Password has been reset successfully.";
        $type = "success";
        return view('business.pages.feedback', compact('title', 'message', 'type', 'link'));
    }

    public function resetPasswordInvalid()
    {
        $title = "Invalid link";
        $message = "The forgot password link you clicked is invalid or expired.";
        $type = "danger";
        return view('business.pages.feedback', compact('title', 'message', 'type'));
    }


    /*public function signUp(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.profile.signup');
        }
    }*/

    public function signUp(Request $request)
    {
        if ($request->isMethod("GET")) {
            //return view("business.pages.profile.signup");
            return Redirect::to('https://business.twiva.co.ke/influencer/auth/signup.php');
        }

        if ($request->isMethod("POST")) {
            // return $request->all();
            $message = ['company_name.required'=>'Please enter company name','company_name.max' => 'Company name should not be greater than 100 characters','country_name.required'=>'Please enter country name','country_name.max' => 'Country name should not be greater than 100 characters','contact_person_name.required'=>'Please enter contact person name','contact_person_name.max' => 'Contact Person name should not be greater than 100 characters', 'email.required' => 'Please enter email', 'password.required' => 'Please enter password', 'password.max' => 'Password should not be greater than 30 characters','business_address.required'=>"Please enter business address",'business_address.max' => 'Business address should not be greater than 1000 characters','contact_phone_number.digits_between'=>'Contact phone number should be 8 to 15 digits.','contact_phone_number.required'=>'Please enter contact phone number','register_phone_number.digits_between'=>'Register phone number should be 8 to 15 digits.','register_phone_number.required'=>'Please enter Register phone number','profile.image:jpg,png' => 'Please select only JPG or PNG image','business_details.required'=>'Please enter business details','business_address.max' => 'Business address should not be greater than 1000 characters'
              ];

            $this->validate($request, ['company_name' => 'required|min:3|max:100','contact_person_name' => 'required|min:3|max:100', 'email' => 'required|email|unique:users|max:200', 'password' =>'required|min:6|max:30',
                 'business_detail'=>'required|max:1000','contact_phone_number' =>  'required|digits_between:8,15','business_address'=>'required|max:1000','country_name' => 'required|min:3|max:100'

              ], $message);

            $imageName = "";
            if ($request->hasFile("profile")) {
                $image = $request->file('profile');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
            }
            $verify_link = str_random(20);
            $login_token = str_random(32);
            $updateUser = User::insertGetId(['name' => $request->company_name, 'email' => $request->email, 'profile' => $imageName,'password' => Hash::make($request->password),'user_type'=>'B', 'remember_token' => $login_token,'business_address'=>$request->business_address,'phone_number'=>$request->contact_phone_number,'email_verify'=>$verify_link,'contact_person'=>$request->contact_person_name,'country_name'=>$request->country_name]);
            if (!empty($updateUser)) {
                $insertCategory = BusinessDetail::insert(['user_id'=>$updateUser,'business_detail'=>$request->business_detail]);
                $insertCategory = MpesaPhoneNumber::insert(['user_id'=>$updateUser,'phone_number'=>$request->contact_phone_number]);
            }

            $url = url('user/verify/'.$verify_link);
            $user_data = User::where('id', $updateUser)->first();
            try {
                Mail::send('mail-verify', ['url' => $url,'user_data' => $user_data], function ($m) use ($user_data) {
                    $m->from('info@twiva.co.ke', 'Twiva');
                    $m->to($user_data->email, 'App User');
                    $m->subject('Email verification link');
                });
                Session::flash('success', 'Signup Successfully & email verification link has been sent to your registered email address.');
                // $request->session()->put('remember_token', $login_token);
                return back();
            } catch (\Exception $e) {
                //return response()->json('Oops Something wrong! ' . $e->getMessage());
                Session::flash('danger', 'Oops Something wrong!.'.$e->getMessage());
                return back()->withInput();
            }
        }
    }

    public function verify(Request $request, $verify_email_token)
    {
        $user = User::where(['email_verify'=>$verify_email_token])->first();
        //return $user; die;
        if ($user) {
            $user->email_verify = null;
            $user->emailStatus = 1;
            $user->update();
            $title = "Email verified";
            $message = "Your email has been verified successfully. You may now login.";
            $type = "success";
            return view('email.feedback', compact('title', 'message', 'type'));
        } else {
            $title = "Invalid link";
            $message = "Your verification link is either expired or invalid.";
            $type = "danger";
            return view('email.feedback', compact('title', 'message', 'type'));
        }
    }
    //  public function checkEmail(Request $request){

    //     $email = $request->email;
    //     $user_find = User::whereEmail($email)->first();

    //     if(!empty($user_find)){
    //         return "1";
    //     }else{
    //         return "0";
    //     }
    // }


    public function changePassword(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.profile.change_pass');
        }
        $id = Auth::guard('business')->user()->id;
        if ($request->isMethod('POST')) {
            $message = [
                'old_password.required'     => 'Please enter old password.',
                'new_password.required'     => 'Please enter new password.',
                'confirm_password.required' => "Please enter confirm password.",
                'new_password.min'          => 'New password must be at least 6 characters.',
                'confirm_password.min'      => "Confirm password must be at least 6 characters.",
                'confirm_password.same'     => "Password and confirm password does not match."
            ];
            $this->validate($request, [
                'old_password'      => 'required',
                'new_password'      => 'required|min:6,max:20',
                'confirm_password'  => 'required|required_with:new_password|same:new_password||min:6,max:20'
            ], $message);
            $get_user = User::where('id', $id)->first();
            $old_password = $request->old_password;
            if (Hash::check($old_password, $get_user->password)) {
                $admin = User::where(['id'=>$id])->update(['password' => Hash::make($request->new_password)]);
                Auth()->guard()->logout();
                $request->session()->flush();
                $request->session()->regenerate();
                Session::flash('success', 'Password has been changed successfully.');
                return redirect(url('business/login'))->withInput();
            } else {
                return back()->with('danger', "Please enter valid old password.");
            }
        }
    }

    public function updatePersonalDetails(Request $request)
    {
        $id = Auth::guard('business')->user()->id;
        if ($request->isMethod('get')) {
            $business_list = User::where(['user_type' => 'B','id' =>$id])->first();
            $business_details = BusinessDetail::where('user_id', $id)->first();
            return view('business.pages.profile.update-personal-details', compact('business_list', 'business_details'));
        }
        if ($request->isMethod("POST")) {
            // return $request->country_code;
            //return $request->all();
            $message = ['company_name.required'=>'Please enter company name','company_name.max' => 'Company name should not be greater than 100 characters','contact_person_name.required'=>'Please enter contact person name','contact_person_name.max' => 'Contact Person name should not be greater than 100 characters','phone_number.digits_between'=>' Phone number should be 8 to 15 digits.','phone_number.required'=>'Please enter phone number','profile.image:jpg,png' => 'Please select only JPG or PNG image','description.required'=>'Please enter description','description.max' => 'Description  should not be greater than 1000 characters'
              ];

            $this->validate($request, ['company_name' => 'required|min:3|max:100','contact_person_name' => 'required|min:3|max:100', /*'email' => 'required|email|unique:users|max:200'*/
                 'phone_number' =>  'required|digits_between:8,15','description'=>'required|max:1000'
              ], $message);

            $data = ['name' => $request->company_name, 'phone_number'=>$request->phone_number,'contact_person'=>$request->contact_person_name,'country_code'=>$request->country_code];

            if ($request->hasFile("profile")) {
                $image = $request->file('profile');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $data['profile'] = $imageName;
            }

            $updateUser = User::where('id', $id)->update($data);

            $updateBusinessDetail = BusinessDetail::where('user_id', $id)->update(['business_detail'=>$request->description]);

            Session::flash('success', 'Personal details has been updated successfully.');
            return redirect('business/settings');
        }
    }

    public function updateMpsaDetails(Request $request)
    {
        $id = Auth::guard('business')->user()->id;

        if ($request->isMethod('get')) {
            $data =  MpesaPhoneNumber::where(['user_id'=>$id])->first();
            return view('business.pages.profile.update-mpsa', compact('data'));
        }
        $id = Auth::guard('business')->user()->id;

        if ($request->isMethod('POST')) {
            $message = [
                'register_mpesa_phone_number.required'   => 'Please enter register mpesa phone number.',
                'register_mpesa_phone_number.digits_between'=>'Register mpesa phone number should be 8 to 15 digits.'

            ];
            $this->validate($request, [
                'register_mpesa_phone_number'      => 'required|digits_between:8,15',

            ], $message);


            $update = MpesaPhoneNumber::where('user_id', $id)->update(['phone_number'=>$request->register_mpesa_phone_number]);
            Session::flash('success', 'Registered mpesa phone number has been updated successfully.');
            return redirect('business/settings');
            //return back()->with('success', "Register mpesa phone number has been updated successfully.");
        }
    }

    public function businesslogout(Request $request)
    {
        $user = Auth::guard('business')->user();
        Auth()->guard('business')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        Session::flash('success', 'Logged out successfully.');
        return redirect(url('/business/login'))->withInput();
    }

    public function uploadImage($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false)
    {
        $return_data = "";
        $profile = null;
        if ($multiple) {
            $profile = $request_profile;
        } elseif ($request_profile->hasFile("image")) {
            $profile = $request_profile->file("image");
        }

        if ($profile) {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalExtension();
            $file_name = uniqid() . "." . $file_name;
            $file_name = str_replace(array(
                  " ",
                  "(",
                  ")"
              ), "", $file_name);
            if (!file_exists($path)) {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            //chmod($path,0775);
            $return_data = $file_name;
        }

        return $return_data;
    }

    public function hireMe(Request $request, $id = null)
    {
        if ($request->isMethod('get')) {
            $user_id = $id;
            $user = User::where('id', $user_id)->first();
            return view('business.pages.hire-me', compact('user'));
        }
        if ($request->isMethod('post')) {
            $message = array(
            'email.required' => 'Please enter registered email address',
            'email.email' => 'Please enter a valid email address',
            'email.exists' => 'Enter registered email address',
            'email.max' => 'Email address maximum 200 characters',
            'text.required'=>"plase enter text",
            'text.max'=>'text maximum 2000 characters',
            'subject.required'=>"plase enter subject",
            'subject.max'=>'subject maximum 100 characters'
        );
            $validator = Validator::make(
                $request->all(),
                ['email' => 'required|email|max:200',
          'text'=>'required|max:2000',
          'subject'=>'required|max:100',


         ],
                $message
            );

            if ($validator->fails()) {
                return back()
                ->withErrors($validator)->withInput();
            }

            $email   = $request->input('email');
            $data = $request->input('text');
            $to  = $request->get('to');
            $subject = $request->input('subject');
            try {
                Mail::send('hire-me-mail', ['from' => $email,'subject'=>$subject,'data'=>$data,'to'=>$to], function ($m) use ($email) {
                    $m->from($email, 'Twiva');
                    $m->to('info@twiva.co.ke', 'App User');
                    $m->subject('contact us ');
                });
                Session::flash("danger", "Message successfully sent.");
                return back();
            } catch (\Exception $ex) {
                return $ex->getMessage();
                return back()->with("error", $ex->getMessage());
            }
            return back();
        }
    }
}
