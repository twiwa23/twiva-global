<?php

namespace App\Http\Controllers\Business;

use Carbon\Carbon;
use App\model\Post;
use App\model\Rate;
use App\model\User;
use App\model\Blogger;
use App\model\Category;
use App\model\PostUser;
use App\model\Services;
use App\model\Portfolio;
use App\model\PostImage;
use App\model\ShareData;
use App\model\TempTokens;
use App\model\CategoryUser;
use App\model\Notification;
use Illuminate\Http\Request;
use App\model\InfulancerRating;
use App\model\MpesaPhoneNumber;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Exceptions\UserNotFoundException;
use Illuminate\Support\Facades\Validator;

class NewBusinessController extends Controller
{
    public function creators(Request $request)
    {
        $category_id = $request->get("category_id");
        $category = Category::where('id','<=','8')->get();
        $categoryUser = CategoryUser::get()->pluck("category_id");
        $users = User::select("users.name as user_name", "users.id as user_id", "users.profile as user_profile", "users.background_image as background_image", "users.description as user_description", "categories.name as category_name", "category_users.category_id as category_id", "category_users.user_id as category_user_id")
      ->leftJoin('category_users', 'category_users.user_id', '=', 'users.id')
      ->leftJoin('categories', 'categories.id', '=', 'category_users.category_id')
      ->whereIn('category_users.category_id', $categoryUser);

        if ($category_id) {
            $uses = $users->where("category_users.category_id", $category_id);
        }

        $users = $users->inRandomOrder()->paginate(12);

        return view('business/pages/creators', compact('users', 'category'));
    }

    public function portfolio(Request $request)
    {
        $user_id = $request->id;
        if ($request->ajax()) {
            if ($request->hasFile("image")) {
                $image = $request->file('image');
                $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                $updateUser = User::where('id', $auth->id)->update(['background_image' => $imageName]);
            }
        }
        if ($request->isMethod("GET")) {
            $media_type = $request->get("media-type");
            //return $auth;die;
            $portfolio = Portfolio::where('user_id', $user_id);
            if ($media_type) {
                if ($media_type == '1') {
                    $portfolio = $portfolio->where("type", "image");
                } elseif ($media_type == '2') {
                    $portfolio = $portfolio->where("type", "video");
                }
            }
            $portfolio = $portfolio->get();
            $service = Services::where('user_id', $user_id)->get();
            $rate = Rate::where('user_id', $user_id)->get();
            $blogger = Blogger::where('user_id', $user_id)->get();
            $categoryUser = CategoryUser::where('user_id', $user_id)->first();
            $category = Category::where('id', $categoryUser->category_id)->first();
            $user = User::where(['id' => $user_id])->first();
            return view('business/pages/portfolio', compact('portfolio', 'service', 'rate', 'category', 'user', 'user_id', 'blogger'));
        }
    }

    public function storePost(Request $request)
    {
        // dd($request);
        $todayDate = explode(' ',\Carbon\Carbon::now());
        // dd($todayDate[0], $request->input('expiration_date'));
        $id = Auth::guard('business')->user()->id;

        $message = [
            'post_name.required' =>'Please enter title.',
            'description.required' => 'Please enter description.',
            'what_to_include.required' =>"Please enter what to do.",
            'what_not_to_include.required' =>"Please enter what_not_to_include.",
            'expiration_date.required' =>'Please select expiration date',
            'hashtag.required'=>'Please enter hashtag.',
            'price_per_post.numeric' => 'Please Enter Number'
        ];

        $this->validate($request, [
            'post_name' => 'required',
            'what_to_include' =>'required',
            'description'=>'required',
            'what_not_to_include'=>'required',
            'expiration_date'=>'required|after_or_equal:'.$todayDate[0],
            'hashtag'=>'required',
            'price_per_post' => 'required|numeric'
        ], $message);

        $data = [
            "post_name" => $request->input('post_name'),
            "expiration_date" =>date("Y-m-d",strtotime($request->input('expiration_date'))),
            "description"=> $request->input('description'),
            "what_to_include"=> $request->input('what_to_include'),
            "what_not_to_include"=>$request->input('what_not_to_include'),
            "expiration_date"=>$request->input('expiration_date'),
            "other_requirement"=>$request->input('other_requirement'),
            "price_per_post"=>$request->input('price_per_post'),
            "user_id"=>$id,
            'hashtag' => $request->input('hashtag')
        ];

        $hashtag = $request->get("hashtag");
        if($hashtag && !empty($hashtag)){
            $hashtag = trim($hashtag);
            if($hashtag[0] == "#"){
                $hashtag = substr($hashtag,1,strlen($hashtag));
            }
            $data["hashtag"] = $hashtag;
        }

        $is_inserted = Post::create($data);


        // $video_images = $request->file("image_video");


        // if($is_inserted){
        //     if($request->hasFile("image_video")){
        //         $cnt_img_vid = count($video_images);
        //         for($k = 0; $k < $cnt_img_vid;$k++){
        //             if(isset($video_images[$k]) && !empty($video_images[$k])){
        //                 $file = $this->uploadImage_control($video_images[$k], "/app/public/uploads/images/business_images/post_images", 1);
        //                 $insert_data = array();
        //                 $insert_data["media"] = $file;
        //                 $insert_data["post_id"] = $is_inserted->id;
        //                 PostImage::create($insert_data);
        //             }
        //         }
        //     }
        // }

        $images = $this->parseFiles($request,"images_videos");
            if($images){
                $all_media = array();
                $video_image = $images;
                $len = count($images);
                $j = 1;
                if($len > 0){
                $j = 0;
                for($i = 0;$i < $len;$i++){
                    if(isset($video_image[$i])){
                    $cnt_img_vid = count($video_image[$i]);
                    for($k = 0; $k < $cnt_img_vid;$k++){
                        $file = $this->uploadImage_control($video_image[$i][$k], "/app/public/uploads/images/business_images/post_images", 1);

                        $insert_data = array();
                        $insert_data["media"] = $file;
                        $insert_data["post_id"] = $is_inserted->id;
                        PostImage::create($insert_data);

                    }
                    }
                }
                }
            }


            $update_login_data = User::where(['id'=>$id])->update(['login_status'=>'1']);
            Session::flash('success', 'Your post has been created successfull, please select influencers to work with.');
            return redirect(url('business/invite-influencers-post/'.base64_encode($is_inserted->id)));

    }

    private function parseFiles($request,$inputFiles)
    {
        $images = $request->file($inputFiles);
          /* remove not acceptable images/videos*/
          if($images && !empty($images)){
            $not_accept = array();
            $non_acceptable = $request->get("non_acceptable_files");

            if($non_acceptable && !empty($non_acceptable)){
              $exp = explode(",",$non_acceptable);
              $exp = array_filter($exp);
              $not_accept = array_values($exp);
            }

            for($w = 0;$w < count($not_accept);$w++){
                $val = $not_accept[$w];
                if(isset($images[$val[0]][$val[2]])){
                unset($images[$val[0]][$val[2]]);
                }
            }

            $images = array_filter($images);
            $images = array_values($images);
            $images = array_map(function($x){ return array_values($x); }, $images);
        }
        return $images;
    }

    public static function uploadImage_control($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false) {

        $return_data = "";
        $profile = null;
        if ($multiple)
        {
            $profile = $request_profile;
        }
        else if ($request_profile->hasFile("media"))
        {
            $profile = $request_profile->file("media");
        }

        if ($profile)
        {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalName();
            $file_name = uniqid() . "_" . $file_name;
            $file_name = str_replace(array(
                " ",
                "(",
                ")"
            ) , "", $file_name);
            if (!file_exists($path))
            {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            $return_data = $file_name;
        }

        return $return_data;
    }

    public function posts(Request $request)
    {
        ini_set('max_execution_time', 300);
        $user = Auth::guard('business')->id();
        $get_ids1 = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
            ->where("user_id",$user)
            ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
            ->get()->pluck("d");
            
            $j1 = [];

            if($get_ids1 && !empty($get_ids1) && count($get_ids1) > 0){
                $get_ids1 = json_encode($get_ids1);
                $get_ids1 = json_decode($get_ids1,true);

                $j1 = array_map(function($e){
                    $e = str_replace(",0","",$e);
                    $e = number_format($e);
                    return $e;
                },$get_ids1);
            }

        $get_ids2 = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d2")
        ->where("user_id",$user)
        ->havingRaw("SUBSTRING_INDEX(d2, ',', -1) != 0")
        ->get()->pluck("d2");
        $j2 = [];

        if($get_ids2 && !empty($get_ids2) && count($get_ids2) > 0){
            $get_ids2 = json_encode($get_ids2);
            $get_ids2 = json_decode($get_ids2,true);

            $j2 = array_map(function($e){
                return explode(",",$e)[0];

            },$get_ids2);
        }


        $post_data = Post::with(["user", "Images", "postUserCompleted" => function ($query) use ($user) {
            // return $query->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")->groupBy("post_users.id");
            return $query->whereRaw("(post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)");
        }])
              ->select("posts.*", DB::raw("(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where post_users.post_id = posts.id) as d"))
                ->orderBy("posts.id", "desc")
                ->whereIn("posts.id",$j1)
                ->orWhereIn("posts.id",$j2)
                ->where('posts.user_id', $user)
                // ->havingRaw("d > 0")
                ->paginate(10);
        // return $post_data;
        // dd($post_data);
        return view('business.pages.posts',compact('post_data'));

    }

    public function postDetails(Request $request) {
        if($request->isMethod('Get')) {
            $post_id = base64_decode($request->post_id);
            $post_details = Post::whereId($post_id)->with(["user", "Images"])->first();
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->post_id);

            $details = PostUser::where(['post_users.post_id' => $id])
                ->with(['postDetail','user'])
                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))
                ->groupBy("post_users.id")
                ->join("post_prices","post_users.post_id","=","post_prices.post_id")
                ->paginate(10);
            return view('business.pages.post_details',compact('post_details', 'details', 'id'));
        }

    }

    public function getPostDetails(Request $request , $post_id){
        if($request->isMethod('Get')) {
            $query = $request->get('query');
            $post_details = Post::whereId($post_id)->with(["Images"])->first();
            $user = Auth::guard('business')->id();
            $id = $post_id;
            $details = PostUser::where(['post_users.post_id' => $id])
                ->whereHas('user', function($q) use($query){
                $q->where('name','like','%'.$query.'%');})
                ->with(['postDetail','user'])
                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))
                ->groupBy("post_users.id")
                ->join("post_prices","post_users.post_id","=","post_prices.post_id")
                ->paginate(10);
            return view('business.pages.postdetails-data',compact('details'));
        }        
    }



    public function acceptPost(Request $request) {
        if($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->post_id);
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $acceptPostDetails = PostUser::where(['post_users.post_id' => $id])
                                ->whereNotIn('post_users.status',[6,2,5])
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")
                                ->groupBy("post_users.id")
                                ->with(['postDetail','user'])
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))->groupBy("post_users.id")
                                ->join("post_prices","post_users.post_id","=","post_prices.post_id")->paginate(9);
            $name = Post::where('id', $id)->pluck('post_name')->first();
            return view('business.pages.accpet_post',compact('acceptPostDetails','id', 'name'));
        }
    }

    public function acceptPostDetails(Request $request , $id) {
        $user_id = Auth::guard('business')->id();
        if($request->isMethod('get')) {
            $id = base64_decode($id);
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $acceptPostDetails = PostUser::where(['post_users.id' => $id])
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")
                                ->groupBy("post_users.id")
                                ->with(['postDetail','user'])
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user_id." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))->groupBy("post_users.id")
                                ->join("post_prices","post_users.post_id","=","post_prices.post_id")->first();
            return view('business.pages.accept_post_details',compact('acceptPostDetails'));
        }

        if($request->isMethod('post')) {

            $message = [
                'image.required' =>'Please Select Image.',
            ];

            $this->validate($request, [
                'image'=>'required'
            ], $message);

            $postId = $request->input('post_id');
            $text = $request->input('text');

            $infulancerId = $request->input('infulancer_id');
            $imageName = $request->input('image');

            $insertShareData = ShareData::insert(['infulancer_id'=>$infulancerId,'post_id'=>$postId,'user_id'=>$user_id,'image'=>$imageName,'text'=>$text,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);

            $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();
            $getBusienssName = User::where(['id' => $user_id])->first();

            if (!empty($getBusienssName->name)) {
                $name = $getBusienssName->name;
            } else {
                $name = "dummy name";
            }

            $message = $name . " approved your Post";
            $notificationMessage =  "Your post has been approved";

            $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$postId ]);

            $updatePostUserStatus->status = 3;
            $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
            $updatePostUserStatus->update();

            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $postId
            );
            $message_new = array('sound' =>1,'message'=>$message,
                                'notifykey'=>'approved','id'=>$postId);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            // if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
            //     if ($getInfulancerUser->device_type == '2') {
            //         $this->send_android_notification($getInfulancerUser->device_id,'Your post has been approved', "approved",$message_new);
            //     } else {
            //         $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'Your post has been approved', $notifyMessage);
            //     }
            // }

            Session::flash('success', 'Post has been approved successfully.');
            return redirect(url('business/accept-post',base64_encode($postId)));
        }
    }

    public function editPost(Request $request, $id)
    {
        $user_id = Auth::guard('business')->id();
        if ($request->isMethod('get')) {
            $id = base64_decode($id);
            $postDetail = Post::with('Images')->where('id', $id)->first();
            return view('business.pages.post_edit', compact('postDetail', 'id'));
        }
    }

    public function updateMpsaDetails(Request $request)
    {
        $id = Auth::guard('business')->user()->id;

        if($request->isMethod('get')) {
            $data =  MpesaPhoneNumber::where(['user_id'=>$id])->first();
            return view('business.pages.profile.update-mpsa',compact('data'));
        }
        $id = Auth::guard('business')->user()->id;

        if($request->isMethod('POST')){
            $message = [
                'register_mpesa_phone_number.required'   => 'Please enter register mpesa phone number.',
                'register_mpesa_phone_number.digits_between'=>'Register mpesa phone number should be 8 to 15 digits.'
            ];

            $this->validate($request, [
                'register_mpesa_phone_number'      => 'required|digits_between:8,15',

            ],$message);


            $update = MpesaPhoneNumber::where('user_id',$id)->update(['phone_number'=>$request->register_mpesa_phone_number]);
            Session::flash('success', 'Registered mpesa phone number has been updated successfully.');
            // return redirect('business/settings');
            return back()->with('success', "Register mpesa phone number has been updated successfully.");

        }
    }

    public function postRatingReview(Request $request) {
        if($request->isMethod('get')) {
            $id = base64_decode($request->segment(4, 'post_id'));
            $postDetail = Post::with('Images')->where('id', $id)->first();
            $user_id = base64_decode($request->segment(3, 'user_id'));
            $user = User::where('id', $user_id)->first();
            return view('business/pages/rating&review', compact('postDetail', 'id', 'user'));
        }

        if($request->isMethod('post')) {
          $user_id = Auth::guard('business')->id();
          $id = base64_decode($request->id);
          $post_id = base64_decode($request->post_id);
          $getRatingData = InfulancerRating::where(['infulencer_id'=>$id,'type'=> 'post','post_id'=>$post_id,'user_id'=>$user_id])->first();

                if(!empty($getRatingData)){
                  Session::flash('success', 'Rating already submitted.');
                //   return redirect(url('business/complete-accept-post',base64_encode($post_id)));
                  return redirect(url('business/posts'));
                }

                $insert_rating = new InfulancerRating();
                $insert_rating->user_id = $user_id;
                $insert_rating->infulencer_id = $id;
                $insert_rating->rating = $request->input('rating');
                $insert_rating->type = 'post';
                $insert_rating->post_id = $post_id;
                $insert_rating->comment = $request->input('review');
                $insert_rating->save();

                Session::flash('success', 'Rating submitted successfully.');
                return redirect(url('business/posts'));
                //   return redirect(url('business/complete-accept-post',base64_encode($post_id)));
        }
    }


    public function navChange(Request $request)
    {
        $user = User::find($request->id);
        $temp = TempTokens::where('user_id', $request->id)->where('data', 'nav-switch')->first();
        if(!$user || !$temp) {
            return response()->json(["message" => "User Not Found."], 400);
        }
        try {
            $loggedInUser = auth()->guard('business')->loginUsingId($user->id);

            if (!$loggedInUser) {
                // If User not logged in, then Throw exception
                throw new UserNotFoundException();
            }
        }catch(UserNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 400);
        }

        $temp->delete();
        $redirectTo = '/business'. $request->url;
        return redirect($redirectTo);
    }

    public function navChangeToNew(Request $request)
    {
        $user = User::find($request->user_id);
        if(!$user) {
            return response()->json(["message" => "User Not Found."], 400);
        }
        $temp = TempTokens::create([
            'user_id' => $user->id,
            'data' => 'nav-change'
        ]);
        $temp_id = base64_encode(base64_encode($temp->id));
        $temp_user_id = base64_encode(base64_encode($temp->user_id));
        $nav_url =  config('app.business_subdomain_url'). "business/". $request->nav_url;

        $url = config('app.business_subdomain_url'). "business.php?id=". $temp_id."&params=". $temp_user_id."&url=". $nav_url;
        return response([
            'url' => $url,
        ], 200);

    }




  public function hireMe(Request $request,$id = null)
  {
        if ($request->isMethod('get')) {
            $user_id = $id;
            $user = User::where('id',$user_id)->first();
            return view('business.pages.hire-me',compact('user'));

        }
        if ($request->isMethod('post')) {

            $message = array(
                'email.required' => 'Please enter registered email address',
                'email.email' => 'Please enter a valid email address',
                'email.exists' => 'Enter registered email address',
                'email.max' => 'Email address maximum 200 characters',
                'text.required'=>"plase enter text",
                'text.max'=>'text maximum 2000 characters',
                'subject.required'=>"plase enter subject",
                'subject.max'=>'subject maximum 100 characters'
            );
            $validator = Validator::make($request->all() ,
            ['email' => 'required|email|max:200',
            'text'=>'required|max:2000',
            'subject'=>'required|max:100',


            ], $message);

            if ($validator->fails())
            {
                return back()
                    ->withErrors($validator)->withInput();
            }

            $email   = $request->input('email');
            $data = $request->input('text');
            $to  = $request->get('to');
            $subject = $request->input('subject');
            try{
                Mail::send('hire-me-mail', ['from' => $email,'subject'=>$subject,'data'=>$data,'to'=>$to], function ($m){
                    $m->from(config('mail.from.address'), 'Twiva');
                    $m->to(config('mail.from.to'))->subject('contact us ');
                });

                Session::flash("success","Message successfully sent.");
                return back();
            }catch(\Exception $ex){
                // return $ex->getMessage();
                return back()->with("error",$ex->getMessage());
            }
            return back();
        }

    }

    public function businesslogout(Request $request) {
        $user = Auth::guard('business')->user();
        Auth()->guard('business')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        Session::flash('success', 'Logged out successfully.');
        // return redirect(url('/business/login'))->withInput();
        return redirect(config('app.business_subdomain_url'))->withInput();
    }

}
