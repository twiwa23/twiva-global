<?php

namespace App\Http\Controllers\Business;

header('Cache-Control: no-store, private, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);
use App\model\User;
use App\model\Post;
use App\model\Gig;
use App\model\Interest;
use App\model\InfluencerImages;
use App\model\InfluencerPrice;
use App\model\InfluencerInterests;
use App\model\InfluencerDetail;
use App\model\GigUser;
use App\model\PostUser;
use App\model\GigMedia;
use App\model\PostImage;
use App\model\GigPrice;
use App\model\PostPrice;
use App\model\BusinessDetail;
use App\model\Notification;
use App\model\Rating;
use App\model\PostDetail;
use App\model\GigDetail;
use App\model\MpesaPhoneNumber;
use App\model\Payment;
use App\model\AdminPayment;
use App\model\Admin;
use App\model\GigMediaDetail;
use App\model\PostDetailsMedia;
use App\model\InfulancerRating;
use App\model\ShareData;
use App\model\TimelineImages;
use App\model\Timeline;
use App\model\TimelineLikes;
use App\model\CallBackTransaction;
use App\model\Portfolio;
use App\model\Services;
use App\model\Blogger;
use App\model\Rate;
use App\model\Category;
use App\model\CategoryUser;
use Carbon\Carbon;
use Safaricom\Mpesa\Mpesa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\v2\BaseTrait;
use App\model\InfluencerWaitList;
use DB, Hash, Session, Storage, Route, Auth, Validator;

class TwitterTrendController extends Controller
{
    use BaseTrait;
    public function setMpesaPhone(Request $request)
    {
        $user_id = Auth::guard("business")->id();
        if ($request->isMethod('Get')) {
            $phoneNumber = MpesaPhoneNumber::where('user_id', $user_id)->first();
            if ($phoneNumber) {
                $phone = $phoneNumber->phone_number;
            } else {
                $phone = '';
            }
            return response()->json(["phone_number" => $phone], 200);
        }
        $phone = $request->get("phone");
        $isCreated = MpesaPhoneNumber::updateOrCreate(['user_id' => $user_id], ['phone_number' => $phone]);
        if ($isCreated) {
            $gig_id = $request->gig_id;
            $selectedInfluencers = $request->selectedInfluencers;
            // $selectedInfluencers format
            /**
            *    [{ id: 2142", image: "http://127.0.0.1:8000/public/images/dummy_user.jpg", name: "abf", price: "10", text: "Foo" }]
            */
            $this->saveInfluencerWaitList($gig_id, $user_id, $selectedInfluencers);
            $price = array_sum(array_column($selectedInfluencers, 'price'));

            // $gigSummary = $this->saveGigSummary($gig_id, $user_id, $selectedInfluencers);
            // $gigSummary = json_decode($gigSummary);
            // $res = array("message" => $gigSummary->message,"status"=>1);
            // return (json_encode($res));

            //By Vikash
            $payment = $this->payment($price , $gig_id , 'gig');
            $payment = json_decode($payment->content(), true);
            if($payment['status']==false){
                return json_encode($payment);
            }else{
                if($payment['payment_status']==1){
                $gigSummary = $this->saveGigSummary($gig_id, $user_id, $selectedInfluencers);
                $gigSummary = json_decode($gigSummary);
                $res = array("message" => $gigSummary->message,"status"=>1);
                return (json_encode($res));
                }

                elseif($payment['payment_status']==0){
                    $transaction = CallBackTransaction::where(['merchant_request_id'=>$payment['merchant_request_id']])->first();
                    $i = $transaction->status;
                    do {
                        $updatedStatus = $this->checkTransactionStatus($payment['merchant_request_id']);
                        $updatedStatus = json_decode($updatedStatus->content(), true);
                        if($updatedStatus['transaction_status']==1){
                            $gigSummary = $this->saveGigSummary($gig_id, $user_id, $selectedInfluencers);
                            $gigSummary = json_decode($gigSummary);
                            $res = array("message" => $gigSummary->message,"status"=>1);
                            return (json_encode($res));
                        }elseif($updatedStatus['transaction_status']!=0 && $updatedStatus['transaction_status']!=1){
                            $error_message = array('status'=>false,'message'=>'Payment failed');
                            return response()->json($error_message,200);
                        }
                    } while ($i == 0);                   
                }
                
                else{
                    return json_encode($payment);  
                }
            }

            //$paymentData = $this->businessPayment($phone, 'gig', $gig_id, $price);
            // $paymentData = json_decode($paymentData);
            // if($paymentData->status=='1'){
            //     $this->getBusinessPaymentStatus($user_id,$paymentData->uid);
            //     $gigSummary = $this->saveGigSummary($gig_id, $user_id, $selectedInfluencers);
            //     $gigSummary = json_decode($gigSummary);
            //     if($gigSummary->status=='1'){
            //         $res = array("message" => $gigSummary->message,"status"=>1);
            //         return (json_encode($res));
            //     }else{
            //         $res = array("message" => $gigSummary->message,"status"=>0);
            //         return (json_encode($res));
            //     }
            // }

            

            /*$gigSummary = $this->saveGigSummary($gig_id, $user_id, $selectedInfluencers);
            $gigSummary = json_decode($gigSummary);
            if($gigSummary->status=='1'){
                $paymentData = $this->businessPayment($phone, 'gig', $gig_id, $price);
                $paymentData = json_decode($paymentData);
                if($paymentData->status=='1'){
                    echo "<pre>"; print_r($paymentData); exit;
                }
            }else{
                $res = array("message" => $gigSummary->message,"status"=>0);
                return (json_encode($res));
            } */
        } else {
            $res = array("message" => "Bad request","status"=>0);
            return (json_encode($res));
        }
    }

    public function mpesaCallbackUrl(Request $request,$uid){
        $requested = $request->all();
        $save_data = json_encode($requested);
        CallBackTransaction::where('u_id',$uid)->update(["res" => $save_data,'status'=>2]);
    }

   
   public function getCallBackTransaction(Request $request){
        $user_id = Auth::guard("business")->id();
        $id = $request->id;  
        $get_data =  CallBackTransaction::where(['id'=>$id])->first();
        if($get_data){
            return response()->json(["message" => "Callback transaction get successfully.", "data" => $get_data], SUCCESS);
        }else{
            return response()->json(["message" => "Data not found."], BAD_REQUEST);
        }
    }
    public function lipaNaMpesaPassword()
    {
        $lipa_time = Carbon::rawParse('now')->format('YmdHms');
        $passkey = env('MPESA_PASS_KEY');
        $BusinessShortCode = env('MPESA_SHORT_CODE');
        $timestamp =$lipa_time;
        $lipa_na_mpesa_password = base64_encode($BusinessShortCode.$passkey.$timestamp);
        return $lipa_na_mpesa_password;
    }
       public function payment($price,$post_id,$type) {          
            $user_id = Auth::guard("business")->id();
            $getMpesaPhoneNumber = MpesaPhoneNumber::where(['user_id' => $user_id])->first();
            if(empty($getMpesaPhoneNumber->phone_number)){
                return response()->json(["message" => "Please update your mPesa phone number"], BAD_REQUEST);
            }
             $phone_number =$getMpesaPhoneNumber->phone_number;
             if($phone_number[0] == 0){
                 $phone_number = substr_replace($getMpesaPhoneNumber->phone_number,"254",0,1);
             }else{
                $phone_number = '254'.$phone_number;  
             }
             $CommandID = "CustomerPayBillOnline";
             $Amount = $price;
             $uid = uniqid();
             $BusinessShortCode ="4023745";
             $LipaNaMpesaPasskey = "c9775d74fa23cae71fbaa79642fa8d1856302e3e67f5b32684998618bce9e34f";
             $TransactionType = "CustomerPayBillOnline";
             $PartyA = $phone_number;
             $PartyB = $BusinessShortCode;
             $PhoneNumber = $PartyA;
             $CallBackURL = 'https://twiva.co.ke/mpesaCallbackUrl';
             $AccountReference = 'twiva';
             $TransactionDesc = 'twivaBusiness';
             $Remark = 'twivaPayment';
            $c2bTransaction = $this->customerMpesaSTKPush($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remark , $uid);
            $transactionResponse = json_decode($c2bTransaction);
            if(isset($transactionResponse->errorMessage)){
                $error_message = array('status'=>false,'type'=>'transaction','message'=>$transactionResponse->errorMessage);
                return response()->json($error_message,400);
            }
            // print'<pre>';print_R($c2bTransaction);die;
            $callBackData =  CallBackTransaction::insertGetId(['u_id'=>$uid,'user_id'=>$user_id,'post_id'=>$post_id,'type'=>$type,'status'=>0,'res'=>$c2bTransaction, 'merchant_request_id'=>$transactionResponse->MerchantRequestID, 'amount'=>$Amount]);
            $getCallBackData = CallBackTransaction::where('id',$callBackData)->first();
             return response()->json(["status"=>true,"message" => "Payment  has been inserted  successfully.",'merchant_request_id'=>$getCallBackData->merchant_request_id , 'payment_status'=>$getCallBackData->status]);
        }
        public function customerMpesaSTKPush($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remark , $uid)
        {
            $url = env('MPESA_STKPUSH_URL');
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->generateAccessToken()));
            $curl_post_data = [
                //Fill in the request parameters with valid values
                'BusinessShortCode' => env('MPESA_SHORT_CODE'),
                'Password' => $this->lipaNaMpesaPassword(),
                'Timestamp' => Carbon::rawParse('now')->format('YmdHms'),
                'TransactionType' => 'CustomerPayBillOnline',
                'Amount' => $Amount,
                'PartyA' => $PhoneNumber, //254728851199, // replace this with your phone number
                'PartyB' => env('MPESA_SHORT_CODE'),
                'PhoneNumber' => $PhoneNumber, // replace this with your phone number
                //'CallBackURL' => 'http://twiva-api.apparrant.com/api/transaction/confirmation',
                'CallBackURL' => $CallBackURL,
                'AccountReference' => "Twiva",
                'TransactionDesc' => "Twiva M-Pesa"
            ];            
            $data_string = json_encode($curl_post_data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            $curl_response = curl_exec($curl);
            return $curl_response;
        }

        public function generateAccessToken()
        {
            $consumer_key=env('MPESA_CONSUMER_KEY');
            $consumer_secret=env('MPESA_CONSUMER_SECRET');
            $credentials = base64_encode($consumer_key.":".$consumer_secret);
            $url = env('MPESA_O_AUTH_URL');
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$credentials));
            curl_setopt($curl, CURLOPT_HEADER,false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            $access_token=json_decode($curl_response);
            return $access_token->access_token;
        }

        function paymentCallback(Request $request){
            try{
                Log::info("Here i m in call back");
                $data = $request->all();
                Log::info(json_encode($data));
                $result_code = $data['Body']['stkCallback']['ResultCode'];
                $merchant_request_id = $data['Body']['stkCallback']['MerchantRequestID'];
                $user_transaction_detail = CallBackTransaction::where('merchant_request_id',$merchant_request_id)->first();
                if($user_transaction_detail){
                if($result_code == 1037){
                    $user_transaction_detail->status = 3;
                    $user_transaction_detail->res = $data;
                    $user_transaction_detail->save();
                    return response()->json(['status' => false, 'message'=>'Request timeout'],401);
                }elseif($result_code == 0){
                    $user_transaction_detail->status = 1;
                    $user_transaction_detail->res = $data;
                    $user_transaction_detail->save();
                    $success_message = array('status'=>true,'message'=>"Payment has been done successfully","data" => $return);
                    return response()->json($success_message,200);

                }elseif($result_code == 1032){
                    $user_transaction_detail->status = 2;
                    $user_transaction_detail->res = $data;
                    $user_transaction_detail->save();
                    return response()->json(['status' => false, 'message'=>'Request cancelled by user'],401);
                }elseif($result_code == 1){
                    $user_transaction_detail->status = 4;
                    $user_transaction_detail->transaction_details = $data;
                    $user_transaction_detail->save();
                    return response()->json(['status' => false, 'message'=>'The balance is insufficient for the transaction'],401);
                }else{
                    $user_transaction_detail->status = 5;
                    $user_transaction_detail->transaction_details = $data;
                    $user_transaction_detail->save();
                    return response()->json(['status' => false, 'message'=>'Case Not Detected'],401);
                }
                }else{
                    $error_message = array('message'=>"Transaction not found",'status'=>false);
                    return response()->json($error_message,400);
                }

            }catch( \Exception $e){
                $error_message = array('error'=>$e->getMessage(),'status'=>false);
                return response()->json($error_message,500);
            }
        }

        function checkTransactionStatus($merchant_request_id){
            $user_id = Auth::guard("business")->id();
            if( $user_id ){
                try{
                    $user_transaction_detail = CallBackTransaction::where('user_id',$user_id)->where('merchant_request_id',$merchant_request_id)->first();
                    if($user_transaction_detail){
                        $transaction_status = $user_transaction_detail->status;
                        $success_message = array('status'=>true,'message'=>"Transaction status fetched","transaction_status" => $transaction_status);
                        return response()->json($success_message,200);
                    }else{
                        $error_message = array('status'=>false,'message'=>'No transaction found');
                        return response()->json($error_message,401);
                    }
                }
                catch( \Exception $e)
                {
                    $error_message = array('error'=>$e->getMessage(),'status'=>false);
                    return response()->json($error_message,500);
                }
            }
            else{
                $error_message = array('status'=>false,'message'=>"Unauthorized");
                return response()->json($error_message,401);
            }            
        }

    public function saveGigSummary($gig_id,$user_id,$selectedInfluencers)
    {
            $allData = $selectedInfluencers;
            if (!empty($allData)) {
                $status = 1;
                $getBusinessName = User::where('id', $user_id)->first();
                $name = $getBusinessName->name;
                $date = date('Y-m-d H:i:s');
                //$influencerWaitList = array();
                foreach ($allData as $key => $value) {

                   // $influencerWaitList[$key]['id'] = $value['id'];
                   // $influencerWaitList[$key]['price'] = $value['price'];

                    $price = $value['price'];
                    $complementry_item = isset($value['text']) ? $value['text'] : "";
                    $infulancer_id = $value['id'];

                    $set_gig_price = [
                        'user_id' => $user_id,
                        'complementry_item' => $complementry_item,
                        'gig_id' => $gig_id,
                        'infulancer_id' => $infulancer_id,
                        'price' => $price,
                        'status' => 1,
                        'updated_at' => $date,
                        'created_at' => $date,
                    ];

                    GigPrice::insert($set_gig_price);

                    GigUser::insert([
                        'user_id' => $infulancer_id,
                        'gig_id' => $gig_id,
                        'created_at' => $date,
                        'updated_at' => $date,
                    ]);

                    AdminPayment::insert([
                        'user_id' => $user_id,
                        'infulencer_id' => $infulancer_id,
                        'amount' => $price,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'post_id' => $gig_id,
                        'type' => 'gig',
                    ]);

                    $message = $name . " invited you to a new Gig";
                    $notificationMessage = "invited you to a new Gig";

                    $insertNotification = Notification::insert([
                        'other_user_id' => $user_id,
                        'user_id' => $infulancer_id,
                        'message' => $message,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'type' => 'gig',
                        'old_status' => 0,
                        'notify_message' => $notificationMessage,
                        'post_id' => $gig_id,
                    ]);
                }
                /*if(count($influencerWaitList)>0){
                    $insert = [
                        "creator_id" => $user_id,
                        "post_gig_id" => $gig_id,
                        "is_accepted"   => '0',
                        "user_data" => json_encode($influencerWaitList),
                        "type"   => 'gig'
                    ];
                }

                $isCreated = InfluencerWaitList::create($insert); */
                $success_message = array('message'=>'Twitter Trends successfully created.','status'=>1);
			    return (json_encode($success_message));
            }else{
                $error_message = array('message'=>'Influencers should not be empty.','status'=>0);
			    return (json_encode($error_message));
            }      
    }


    public function saveInfluencerWaitList($gig_id, $user_id, $selectedInfluencers)
    {
        $allData = $selectedInfluencers;
        if (!empty($allData)) {
            $status = 1;
            $influencerWaitList = $selectedInfluencers;

            if(count(array($influencerWaitList))>0){
                $insert = [
                    "creator_id" => $user_id,
                    "post_gig_id" => $gig_id,
                    "is_accepted"   => '0',
                    "user_data" => json_encode($influencerWaitList),
                    "type"   => 'gig',
                ];
            }

            $isCreated = InfluencerWaitList::create($insert);
            $success_message = array('status'=>1);
            return (json_encode($success_message));
        }else{
            $error_message = array('message'=>'Influencers should not be empty.','status'=>0);
            return (json_encode($error_message));
        }  
            
    }

    public function businessPayment(Request $request){
        $user_id = Auth::guard("business")->id(); 
        $rules = [
            "price" => "required",
            "post_id" => "required|exists:posts,id",
            "infulancer_id" => "required",
        ];
        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails()){
            $keys = array_keys($validator->getMessageBag()->toArray());
            $response['errors'] = $validator->getMessageBag()->toArray();
            $response['errors_keys'] = $keys;
            return Response()->json($response, 400);
        }
        $input = $request->all();
        $prices = json_decode($request->price, TRUE);
        $infulancerId = json_decode($request->infulancer_id, TRUE);

        //$complementry_item = $request->complementry_item;
        $post_id = $request->post_id;
        $date = date('Y-m-d H:i:s');
        $status = 1;
        //$complementryItem = explode(",", $complementry_item);
        $infulancer_count = count($infulancerId);
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name;
        for ($i = 0;$i < $infulancer_count;$i++)
        {
            $set_post_price = [
                            'user_id' => $user_id,
                            //'complementry_item' => @$complementryItem[$i],
                            'post_id' => $post_id,
                            'infulancer_id' => $infulancerId[$i]['id'],
                            'price' => $prices[$i]['price'],
                            'status' => 1,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'created_at' => date('Y-m-d H:i:s')
                        ];
            PostPrice::insert($set_post_price);
            PostUser::insert([
                                'user_id' => $infulancerId[$i]['id'],
                                'post_id' => $post_id,
                                'created_at' => date('Y-m-d H:i:s'), 
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
            $getAdminId = Admin::first();
            $insertAdminAmount = AdminPayment::insert([
                                                    'user_id' => $user_id,
                                                    'infulencer_id' =>$infulancerId[$i]['id'],
                                                     'amount' => $prices[$i]['price'],
                                                     'created_at' => date('Y-m-d H:i:s'),
                                                     'updated_at' => date('Y-m-d H:i:s'),
                                                     'post_id' => $post_id,
                                                     'type' => 'post'
                                                 ]);

            $post_price_id = $post_id;

            ///notification code is pending
            $offer_price_data = PostPrice::where(['post_id' => $post_price_id, 'infulancer_id' => $infulancerId[$i]['id']])->first();
            $message = $name . " invited you to a new Post";
            $notificationMessage =  "invited you to a new Post";
            $insertNotification = Notification::insert([
                                                'other_user_id' => $user_id,
                                                'user_id' => $infulancerId[$i]['id'],
                                                'message' => $message,
                                                'created_at' => $date,
                                                'updated_at' => $date,
                                                'type'=>'post',
                                                'old_status'=>0,
                                                'notify_message'=>$notificationMessage,
                                                'post_id'=>$post_id]);
                                                $notifyMessage = array(
                                                    'sound' => 1,
                                                    'message' => $message,
                                                    'id' => $post_id
                                                );
                                                $message_new = array('sound' =>1,'message'=>$message,
                                                                'notifykey'=>'invited','id'=>$post_id);
                                                $getInfulancerUser = User::where(['id' => $infulancerId[$i], 'status' => '1', 'is_deleted' => '0'])->first();
                                                   
                                                if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
                                                {
                                                    if ($getInfulancerUser->device_type == '2')
                                                    {
                     $device_token = $getInfulancerUser->device_id;
                    $message = 'invited you to a new Post';
                    $notmessage = "invited";
                    $msgsender_id = $message_new;

                    if (!defined('API_ACCESS_KEY'))
                    {
                      define('API_ACCESS_KEY','AAAAw_qk_lI:APA91bHb0bLFByFcj3gd8umTYG5QzOFLUVWkzXudN9xHAXl232plDra86kXHohP-vAq27SM1mYT4q-FuI4OQj135k1D6_XVjF6w3uHZaFJxvR4tQO-Zi725juBHnBMB_jDWOm4eC5VMy');
                    }
                    $registrationIds = array($device_token);
                    $fields = array(
                      'registration_ids' => $registrationIds,
                      'alert' => $message,
                      'sound' => 'default',
                      'Notifykey' => $notmessage, 
                      'data'=>$msgsender_id
                        
                    );
                    $headers = array(
                      'Authorization: key=' . API_ACCESS_KEY,
                      'Content-Type: application/json'
                    );
                
                    $ch = curl_init();
                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    curl_setopt( $ch,CURLOPT_POST, true );
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($fields) );
                    $result = curl_exec($ch);
                
                    if($result == FALSE) {
                      die('Curl failed: ' . curl_error($ch));
                    }
                
                    curl_close( $ch );                                                       
                                                    }
                                                }
               
        }  
        $response['return'] = true;
        $response['message'] = 'Post created successfully';
        return response()->json($response,200);	      
    }   
    // public function businessPayment($phone, $type, $post_id,$price){
    //     $user_id = Auth::guard("business")->id();
    //     $getMpesaPhoneNumber = $phone;
    //     if (empty($getMpesaPhoneNumber)) {
    //         $res = array('message'=>'Please update your mPesa phone number.','status'=>0);
    //         return (json_encode($res));
    //     }
    //     $number = $getMpesaPhoneNumber;
    //     $phone_number = substr_replace($number, "254", '0', 1);
    //     $getPrice = InfluencerWaitList::where([
    //         'post_gig_id' => $post_id,
    //         'type' => $type,
    //     ])
    //         ->orderBy('id', 'desc')
    //         ->first();
        
    //     if (!$getPrice || empty($getPrice)) {
    //         return response()->json(["message" => "Payment failed"], 205);
    //     }
    //     $CommandID = "CustomerPayBillOnline";
    //     $Amount = $price;
    //     $uid = uniqid();

    //     $BusinessShortCode = "4023745"; /* admin short code*/

    //     $LipaNaMpesaPasskey = "c9775d74fa23cae71fbaa79642fa8d1856302e3e67f5b32684998618bce9e34f"; /* admin mpesa key*/
    //     $TransactionType = "CustomerPayBillOnline";
    //     $Amount = $price; /* deducted amount */
    //     $PartyA = $phone_number;
    //     $PartyB = $BusinessShortCode;
    //     $PhoneNumber = $PartyA;

    //     $CallBackURL = 'https://twiva.co.ke/mpesaCallbackUrl/' . $uid;
    //     //$CallBackURL = 'http://192.168.3.177/mpesaCallbackUrlBusiness/'.$uid;
    //     $AccountReference = '765';
    //     $TransactionDesc = 'Twiva Payment for Influencer Invitation';
    //     $Remark = 'Twiva Payment for Influencer Invitation';
    //     try {
    //         /* bypass payment for single user and insert the dummy transection data */
    //         if ($user_id != 7) {
    //             //info@twiva.co.ke
    //             $c2bTransaction = Mpesa::STKPushSimulation($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remark);
    //             $paymentRes = json_decode($c2bTransaction, true);
    //             $callBackData = CallBackTransaction::insertGetId(['u_id' => $uid, 'user_id' => $user_id, 'post_id' => $post_id, 'type' => $type, 'status' => 1]);
    //         } else {
    //             $callBackData = CallBackTransaction::insertGetId([
    //                 'u_id' => $uid,
    //                 'user_id' => $user_id,
    //                 'post_id' => $post_id,
    //                 'type' => $type,
    //                 'status' => 1,
    //                 "res" =>
    //                     '{"Body":{"stkCallback":{"MerchantRequestID":"12230-10115693-1","CheckoutRequestID":"ws_CO_210720201537398543","ResultCode":0,"ResultDesc":"The service request is processed successfully.","CallbackMetadata":{"Item":[{"Name":"Amount","Value":5},{"Name":"MpesaReceiptNumber","Value":"OGL1X9S407"},{"Name":"Balance"},{"Name":"TransactionDate","Value":20200721153747},{"Name":"PhoneNumber","Value":254706322847}]}}}}',
    //             ]);
    //         }

    //         $getCallBackData = CallBackTransaction::where('id', $callBackData)->first();
    //     } catch (Exception $ex) {
    //         $res = array('message'=>'Payment failed','status'=>0,"error" => $ex->getMessage());
    //         return (json_encode($res));
    //     }
    //     $res = array('message'=>'Payment  has been inserted  successfully.','status'=>1,'data' => $getCallBackData, "payment" => @$c2bTransaction, "call back" => $callBackData, "uid" => $uid);
    //     return (json_encode($res));
    // }


    public function getBusinessPaymentStatus($user_id,$uid){
        
        $get_data =  CallBackTransaction::where(['u_id' => $uid])->first();

        if($get_data && !empty($get_data)){
          if(!empty($get_data->res)){
  
            $res = json_decode($get_data->res,true);
            if(isset($res['Body']["stkCallback"]["ResultCode"])){
              if($res["Body"]["stkCallback"]['ResultCode'] == '0'){
                
                $type = $get_data->type;
                $user_id = $get_data->user_id;
                $post_id = $get_data->post_id;
                $filter = [
                    "user_id" => $user_id,
                    "post_id" => $post_id
                ];
  
                $getData = InfluencerWaitList::where([
                              "creator_id" => $user_id,
                              "post_gig_id" => $post_id,
                              'type' => $type,
                              'is_accepted' => '0'
                            ])->orderBy("id","desc")->first();
  
                InfluencerWaitList::where(["id" => $getData->id])->update(['is_accepted' => '1']);
                  
                /*if($type == 'gig'){
                  $filter["user_data"] = $getData->user_data;
                  $this->setGigInfluencers($filter);
                  return response()->json([],200);
                }else{
                  $filter["user_data"] = $getData->user_data;
                  $this->setPostInfluencers($filter);
                  /* post set influencer*/
                  //return response()->json([],200);
                  
                //}
                /* payment made successfully*/
                $res = array('message'=>'Payment Successfull','status'=>1);
                return (json_encode($res));
              }else {
                $res = array('message'=>'Payment failed','status'=>0);
                return (json_encode($res));
              }
            }else{
                $res = array('message'=>'Payment failed','status'=>0);
                return (json_encode($res));
            }
          }else{
            $res = array('message'=>'Payment failed','status'=>0);
            return (json_encode($res));
          }
        }else{
            $res = array('message'=>'Payment failed','status'=>0);
            return (json_encode($res));  
        }
    }

    public function setGigInfluencers(array $filter){
        $user_id = $filter["user_id"];
        $gig_id = $filter["post_id"];
        $allData = json_decode($filter["user_data"]);

        $status = 1;
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name;
        $date = date('Y-m-d H:i:s');
        
        foreach ($allData as $value) {
            $price = $value->price;
            $complementry_item = isset($value->compliment) ? $value->compliment : "";
            $infulancer_id = $value->id;
        
            $set_gig_price = [
                'user_id' => $user_id,
                'complementry_item' => $complementry_item,
                'gig_id' => $gig_id,
                'infulancer_id' => $infulancer_id,
                'price' => $price,
                'status' => 1,
                'updated_at' => $date,
                'created_at' => $date
             ];

            GigPrice::insert($set_gig_price);

            GigUser::insert([
                        'user_id' => $infulancer_id,
                        'gig_id' => $gig_id,
                        'created_at' => $date,
                        'updated_at' => $date
                      ]);

            AdminPayment::insert([
                        'user_id' => $user_id,
                        'infulencer_id' => $infulancer_id,
                        'amount' => $price,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'post_id' => $gig_id,
                        'type' => 'gig'
                    ]);
            
            $message = $name . " invited you to a new Gig";
            $notificationMessage = "invited you to a new Gig";

            $insertNotification = Notification::insert([
                        'other_user_id' => $user_id,
                        'user_id' => $infulancer_id,
                        'message' => $message,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'type' => 'gig',
                        'old_status' => 0,
                        'notify_message' => $notificationMessage,
                        'post_id' => $gig_id
                    ]);         
        }

        $res = array('message'=>'Payment failed','status'=>0);
            return (json_encode($res));  
}

    // public function businessPayment($request){
    //     $user_id = Auth::guard("business")->id();
    //     $getMpesaPhoneNumber = MpesaPhoneNumber::where(['user_id' => $user_id])->first();
    //     if (empty($getMpesaPhoneNumber->phone_number)) {
    //         return response()->json(["message" => "Please update your mPesa phone number."], 205);
    //     }
    //     $number = $getMpesaPhoneNumber->phone_number;
    //     $post_id = $request->gig_id;
    //     $selectedInfluencers = $request->selectedInfluencers;
    //     $type = 'gig';
    //     $price = 1;
    //     $price = array_sum(array_column($selectedInfluencers, 'price'));
    //     $phone_number = substr_replace($number, "254", '0', 1);
    //     $getPrice = InfluencerWaitList::where([
    //         'post_gig_id' => $post_id,
    //         'type' => $type,
    //     ])
    //         ->orderBy('id', 'desc')
    //         ->first();

    //     if (!$getPrice || empty($getPrice)) {
    //         return response()->json(["message" => "Payment failed"], 205);
    //     }
    //     $CommandID = "CustomerPayBillOnline";
    //     $Amount = $price;
    //     $uid = uniqid();

    //     $BusinessShortCode = "4023745"; /* admin short code*/

    //     $LipaNaMpesaPasskey = "c9775d74fa23cae71fbaa79642fa8d1856302e3e67f5b32684998618bce9e34f"; /* admin mpesa key*/
    //     $TransactionType = "CustomerPayBillOnline";
    //     $Amount = $price; /* deducted amount */
    //     $PartyA = $phone_number;
    //     $PartyB = $BusinessShortCode;
    //     $PhoneNumber = $PartyA;

    //     $CallBackURL = 'https://twiva.co.ke/mpesaCallbackUrl/' . $uid;
    //     //$CallBackURL = 'http://192.168.3.177/mpesaCallbackUrlBusiness/'.$uid;
    //     $AccountReference = '765';
    //     $TransactionDesc = 'test';
    //     $Remark = 'test';
    //     try {
    //         /* bypass payment for single user and insert the dummy transection data */
    //         if ($user_id != 7) {
    //             //info@twiva.co.ke
    //             $c2bTransaction = Mpesa::STKPushSimulation($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remark);
    //             $paymentRes = json_decode($c2bTransaction, true);

    //             // if(isset($paymentRes["errorMessage"])){
    //             //   return response()
    //             //      ->json(["message" => "failed"],204);
    //             // }

    //             $callBackData = CallBackTransaction::insertGetId(['u_id' => $uid, 'user_id' => $user_id, 'post_id' => $post_id, 'type' => $type, 'status' => 1]);
    //         } else {
    //             $callBackData = CallBackTransaction::insertGetId([
    //                 'u_id' => $uid,
    //                 'user_id' => $user_id,
    //                 'post_id' => $post_id,
    //                 'type' => $type,
    //                 'status' => 1,
    //                 "res" =>
    //                     '{"Body":{"stkCallback":{"MerchantRequestID":"12230-10115693-1","CheckoutRequestID":"ws_CO_210720201537398543","ResultCode":0,"ResultDesc":"The service request is processed successfully.","CallbackMetadata":{"Item":[{"Name":"Amount","Value":5},{"Name":"MpesaReceiptNumber","Value":"OGL1X9S407"},{"Name":"Balance"},{"Name":"TransactionDate","Value":20200721153747},{"Name":"PhoneNumber","Value":254706322847}]}}}}',
    //             ]);
    //         }

    //         $getCallBackData = CallBackTransaction::where('id', $callBackData)->first();
    //     } catch (Exception $ex) {
    //         return response()->json(["message" => "Payment failed", "error" => $ex->getMessage()], 203);
    //     }
    //     return $request;
    // }

    public function influencersList(Request $request)
    {
        if ($request->isMethod('Get')) {
            $search = $request->get('query');
            $id = Auth::guard('business')->user();

            $infulancer_list = User::with(["Images", "Interests", "SocialDetail", "price", "InfluencerDetail", "infulancerRating"])
                ->select(
                    "users.id",
                    "users.id as uid",
                    "users.name",
                    "users.email",
                    "users.profile",
                    "users.phone_number",
                    "users.country_code",
                    "users.description",
                    "D.facebook_friends",
                    "D.twitter_friends",
                    "D.instagram_friends",
                    "influencer_price.plug_price",
                    "influencer_price.gig_price",
                    "influencer_details.dob",
                    "influencer_details.gender"
                )
                ->join("influencer_social_details as D", "D.user_id", "=", "users.id")
                ->leftJoin("influencer_price", "influencer_price.user_id", "=", "users.id")
                ->leftJoin("influencer_details", "influencer_details.user_id", "=", "users.id")
                ->where([
                    "user_type" => 'I',
                    "status" => '1',
                    "emailStatus" => '1',
                    'is_deleted' => '0',
                ]);

            if ($search) {
                $infulancer_list = $infulancer_list->where(function ($query) use ($search) {
                    return $query
                        ->where("D.twitter_name", "LIKE", '%' . $search . "%")
                        ->orWhere("D.facebook_name", "LIKE", '%' . $search . "%")
                        ->orWhere("D.instagram_name", "LIKE", '%' . $search . "%")
                        ->orWhere("users.name", "LIKE", '%' . $search . "%");
                });
            }

            // return $infulancer_list;

            $infulancer_list = $infulancer_list->orderBy('users.id', 'desc')->paginate(9);

            return view('business.pages.influ_list', compact('infulancer_list'));
        }
    }

    public function influencersDetails(Request $request)
    {
        if ($request->isMethod('Get')) {
            /* $id = Auth::guard('business')->user();*/
            $infulancerDetail = User::with(["Images", "Interests", "SocialDetail", "price", "InfluencerDetail", "infulancerRating"])
                ->where(["id" => base64_decode($request->id)])
                ->first();

            return view('business.pages.influ_details', compact('infulancerDetail'));
        }
    }

    public function notification(Request $request)
    {
        if ($request->isMethod('Get')) {
            $id = Auth::guard('business')->user()->id;
            $getNotificationList = Notification::where('user_id', $id)
                ->with("otherUser")
                ->orderBy("id", "DESC")
                ->paginate(10);

            foreach ($getNotificationList as $notification) {
                if ($notification->type == "post") {
                    $postUserStatus = PostUser::where(['post_id' => $notification->post_id, 'user_id' => $notification->other_user_id])->first();
                    if ($postUserStatus) {
                        $notification->new_status = $postUserStatus->status;
                    } else {
                        $notification->new_status = 0;
                    }
                } else {
                    $postUserStatus = GigUser::where(['gig_id' => $notification->post_id, 'user_id' => $notification->other_user_id])->first();
                    if ($postUserStatus) {
                        $notification->new_status = $postUserStatus->status;
                    } else {
                        $notification->new_status = 0;
                    }
                }
            }
            //return $getNotificationList;
            return view('business.pages.notification', compact('getNotificationList'));
        }
    }
    public function settings(Request $request)
    {
        if ($request->isMethod('Get')) {
            $id = Auth::guard('business')->user()->id;
            $user = User::where('id', $id)->first();
            return view('business.pages.settings', compact('user'));
        }
    }

    public function inviteInfluencersList(Request $request)
    {
        if ($request->isMethod('Get')) {
            $id = Auth::guard('business')->user();
            return view('business.pages.invite_influ');
        }
    }

    //---------------gig start-----------------------//
    public function gigList(Request $request)
    {
        if ($request->isMethod('Get')) {
            $id = Auth::guard('business')->user()->id;
            $gig_list = Gig::with(["user", "Images"])
                ->withCount([
                    'gigInfluencerUser as gig_completed_user' => function ($query) {
                        $query->where('status', 4);
                    },
                    'gigInfluencerUser',
                    'gigInfluencerUser as gig_accepted_user' => function ($query) {
                        $query->whereIn('status', [1, 3 , 4]);
                    },
                    'gigInfluencerUser as gig_pending_user' => function ($query) {
                        $query->where('status', 5);
                    },
                    'gigInfluencerUser as gig_declined_user' => function ($query) {
                        $query->where('status', 2);
                    },
                ])
                ->where('user_id', $id)
                ->orderby("id", "DESC")
                ->paginate(10);
            // echo "<pre>"; print_r($gig_list->toArray()); exit;
            return view('business.pages.gigs.gig_list', compact('gig_list', 'id'));
        }
    }

    public function getGigCompleteList(Request $request)
    {
        if ($request->isMethod("GET")) {
            $user = Auth::guard("business")->id();

            $where = "(gig_users.status = 4 and gig_prices.infulancer_id = gig_users.user_id)";

            // $where = "((gig_users.status = 0 or gig_users.status = 2) and (gig_prices.status = 0 or gig_prices.status = 2))";
            $get_ids = Gig::selectRaw(
                "(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d"
            )
                ->where("user_id", $user)
                ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
                ->get()
                ->pluck("d");
            $j = [];

            if ($get_ids && !empty($get_ids) && count($get_ids) > 0) {
                $get_ids = json_encode($get_ids);
                $get_ids = json_decode($get_ids, true);

                $j = array_map(function ($e) {
                    return number_format(str_replace(",0", "", $e));
                }, $get_ids);
            }

            $gigData = Gig::with(["user", "Images"])
                ->select("gigs.*")
                ->orderBy("gigs.id", "desc")
                ->whereIn("gigs.id", $j)
                ->where('gigs.user_id', $user)
                ->paginate(9);

            return view('business.pages.gig_complete_list', compact('gigData'));
        }
    }
    public function gigReminder(Request $request) {

        $user_id = Auth::guard('business')->id();
        $gig_id = base64_decode($request->gig_id);
        $infulancerId = base64_decode($request->influencer_id);
        if($gig_id && $infulancerId) {
            $getBusienssName = User::where('id', $user_id)->first();
            $name = $getBusienssName->name;
            $updatePostUserStatus = GigUser::where(['gig_id' => $gig_id, 'user_id' => $infulancerId])->first();
            //notifiaction code is pending
            $message = "Please respond to " . $name;
            $notificationMessage = "gig reminder";
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $gig_id
            );
            
            $message_new = array('sound' =>1,'message'=>$message,
                                'notifykey'=>'gig reminder','id'=>$gig_id);
            $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=> $updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$gig_id]);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if(!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id,'gig reminder', 'gig reminder',$message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'gig reminder', $notifyMessage);
                }
            }
            $res = array('message'=>'Gig reminder sent successfully.','status'=>1);
            return (json_encode($res));
        } else {
            $res = array('message'=>'Unable to proceed your request,please try later.','status'=>0);
            return (json_encode($res));
        }

    }
    public function gigDetails(Request $request)
    {
        if ($request->isMethod('Get')) {
            $gig_detail = Gig::with("Images")
                ->withCount([
                    'gigInfluencerUser as gig_completed_user' => function ($query) {
                        $query->where('status', 4);
                    },
                    'gigInfluencerUser'
                ])
                ->where(['id' => base64_decode($request->id)])
                ->first();
            $id = base64_decode($request->id);
            $gigDetailsList = GigUser::where(['gig_users.gig_id' => $id])
                ->with([
                    'user',
                    'influencer_social_details',
                    "InfluencerDetailNew" => function ($query) {
                        return $query->select("name", "influencer_id");
                    }]
                )
                ->select(
                    DB::raw(" distinct gig_users.id "),
                    "gig_users.*",
                    "gig_prices.price",
                    "gig_prices.infulancer_id as influence",
                    "gig_prices.complementry_item"
                )
                ->join("gig_prices", "gig_users.gig_id", "=", "gig_prices.gig_id")
                ->groupBy("gig_users.id")
                ->paginate(10);
            // echo "<pre>"; print_r($gig_detail->toArray()); exit;
            return view('business.pages.gigs.gig_detail', compact('id','gig_detail','gigDetailsList'));
        }
    }
    public function createGig(Request $request)
    {
        if ($request->isMethod('GET')) {
            return view('business.pages.gigs.create-gig');
        }
        $params = $request->all();
        // echo "<pre>"; print_r($params); exit;
        $id = Auth::guard('business')->user()->id;
        $message = [
            'title.required' => 'Please enter title',
            'title.max' => 'Title should not be greater than 30 characters',
            'description.required' => 'Please enter description',
            'description.max' => 'Description should not be greater than 1000 characters',
            'what_to_do.required' => "Please enter what to do",
            'what_to_do.max' => 'What to do should not be greater than 1000 characters',
            'contact_person_name.required' => "Please enter contact person name",
            'contact_person_name.max' => 'Contact person name should not be greater than 50 characters',
            'contact_person_number.digits_between' => 'Contact person number should be 8 to 15 digits.',
            'gig_start_date.required' => 'Please select gig start date',
            'contact_person_name.required' => 'Please enter contact person number',
            'gig_end_date.required' => 'Please select gig end date',
            // 'gig_start_time.required' => 'Please select gig start time',
            // 'gig_end_time.required' => 'Please select gig end time',
            'gig_price_offering.required' => 'Please enter gig price offering',
            'address.required' => 'Please enter address',
            'venue.required' => 'Please enter venue',
        ];

        $this->validate(
            $request,
            [
                'title' => 'required|max:30',
                'what_to_do' => 'required|max:1000',
                'description' => 'required|max:1000',
                'contact_person_name' => 'required|max:50',
                'contact_person_number' => 'required|digits_between:8,15',
                'gig_start_date' => 'required',
                'gig_end_date' => 'required',
                // 'gig_start_time' => 'required',
                // 'gig_end_time' => 'required',
                'gig_price_offering' => 'required|digits_between:1,8',
                'address' => 'required|max:1000',
                'venue' => 'required|max:100',
            ],
            $message
        );
        $startDate = str_replace('/', '-', $request->input('gig_start_date'));
        $endDate = str_replace('/', '-', $request->input('gig_end_date'));
        $data = [
            "gig_name" => $request->input('title'),
            "gig_start_date_time" => date("Y-m-d H:i:s", strtotime($startDate)),
            "gig_end_date_time" => date("Y-m-d H:i:s", strtotime($endDate)),
            // "gig_end_date_time" => date("Y-m-d H:i:s", strtotime($request->input('gig_end_date') . " " . $request->input('gig_end_time'))),
            // "gig_start_date_time" => date("Y-m-d H:i:s", strtotime($request->input('gig_start_date') . " " . $request->input('gig_start_time'))),
            "description" => $request->input('description'),
            "what_to_do" => $request->input('what_to_do'),
            "phone_number" => $request->input('contact_person_number'),
            "owner_name" => $request->input('contact_person_name'),
            "user_id" => $id,
            'price_per_gig' => $request->input('gig_price_offering'),
            'location' => $request->input('address'),
            'venue' => $request->input('venue'),
        ];
        $hashtag = $request->get("hashtag");
        if ($hashtag && !empty($hashtag)) {
            $hashtag = trim($hashtag);
            if ($hashtag[0] == "#") {
                $hashtag = substr($hashtag, 1, strlen($hashtag));
            }
            $data["hashtag"] = $hashtag;
        }
        $is_inserted = Gig::create($data);

        $video_images = $request->file("image_video");
        if ($is_inserted) {
            if ($request->hasFile("image_video")) {
                $cnt_img_vid = count($video_images);
                for ($k = 0; $k < $cnt_img_vid; $k++) {
                    if (isset($video_images[$k]) && !empty($video_images[$k])) {
                        $file = $this->uploadImage_control($video_images[$k], "/app/public/uploads/images/business_images/gig_images", 1);
                        $insert_data = [];
                        $insert_data["media"] = $file;
                        $insert_data["gig_id"] = $is_inserted->id;
                        GigMedia::create($insert_data);
                    }
                }
            }
        }
        $update_login_data = User::where(['id' => $id])->update(['login_status' => '1']);
        return redirect('business/invite-influencers-gig/' . $is_inserted->id);
    }
    public function createGigDesc(Request $request)
    {
        if ($request->isMethod('GET')) {
            return view('business.pages.create-gigdesc');
        }
    }

    public function createGigFiled2(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.create_gig_filed2');
        }
        $id = Auth::guard('business')->user()->id;
        if ($request->isMethod("POST")) {
            $message = [
                'title.required' => 'Please enter title',
                'title.max' => 'Title should not be greater than 30 characters',
                'description.required' => 'Please enter description',
                'description.max' => 'Description should not be greater than 1000 characters',
                'what_to_do.required' => "Please enter what to do",
                'what_to_do.max' => 'What to do should not be greater than 1000 characters',
                'contact_person_name.required' => "Please enter contact person name",
                'contact_person_name.max' => 'Contact person name should not be greater than 50 characters',
                'contact_person_number.digits_between' => 'Contact person number should be 8 to 15 digits.',
                'gig_start_date.required' => 'Please select gig start date',
                'contact_person_name.required' => 'Please enter contact person number',
                'gig_end_date.required' => 'Please select gig end date',
                'gig_start_time.required' => 'Please select gig start time',
                'gig_end_time.required' => 'Please select gig end time',
            ];

            $this->validate(
                $request,
                [
                    'title' => 'required|max:30',
                    'what_to_do' => 'required|max:1000',
                    'description' => 'required|max:1000',
                    'contact_person_name' => 'required|max:50',
                    'contact_person_number' => 'required|digits_between:8,15',
                    'gig_start_date' => 'required',
                    'gig_end_date' => 'required',
                    'gig_start_time' => 'required',
                    'gig_end_time' => 'required',
                ],
                $message
            );

            $request->session()->put('gig', [
                "gig_name" => $request->input('title'),
                "gig_end_date_time" => date("Y-m-d H:i:s", strtotime($request->input('gig_end_date') . " " . $request->input('gig_end_time'))),
                "gig_start_date_time" => date("Y-m-d H:i:s", strtotime($request->input('gig_start_date') . " " . $request->input('gig_start_time'))),
                "description" => $request->input('description'),
                "what_to_do" => $request->input('what_to_do'),
                "phone_number" => $request->input('contact_person_number'),
                "owner_name" => $request->input('contact_person_name'),
                "user_id" => $id,
            ]);

            return redirect('business/create-gig-field');
        }
    }

    public function createGigField(Request $request)
    {
        $id = Auth::guard('business')->user()->id;
        if ($request->isMethod('get')) {
            return view('business.pages.create_gig_filed');
        }
        if ($request->isMethod("POST")) {
            $prevData = $request->session()->get("gig");
            $message = ['gig_price_offering.required' => 'Please enter gig price offering', 'address.required' => 'Please enter address', 'venue.required' => 'Please enter venue'];

            $this->validate($request, ['gig_price_offering' => 'required|digits_between:1,8', 'address' => 'required|max:1000', 'venue' => 'required|max:100'], $message);

            $second_page_data = ['price_per_gig' => $request->input('gig_price_offering'), 'location' => $request->input('address'), 'venue' => $request->input('venue'), ''];

            $data = array_merge($prevData, $second_page_data);
            $hashtag = $request->get("hashtag");
            if ($hashtag && !empty($hashtag)) {
                $hashtag = trim($hashtag);
                if ($hashtag[0] == "#") {
                    $hashtag = substr($hashtag, 1, strlen($hashtag));
                }
                $data["hashtag"] = $hashtag;
            }
            $is_inserted = Gig::create($data);

            $video_images = $request->file("image_video");
            if ($is_inserted) {
                if ($request->hasFile("image_video")) {
                    $cnt_img_vid = count($video_images);
                    for ($k = 0; $k < $cnt_img_vid; $k++) {
                        if (isset($video_images[$k]) && !empty($video_images[$k])) {
                            $file = $this->uploadImage_control($video_images[$k], "/app/public/uploads/images/business_images/gig_images", 1);
                            $insert_data = [];
                            $insert_data["media"] = $file;
                            $insert_data["gig_id"] = $is_inserted->id;
                            GigMedia::create($insert_data);
                        }
                    }
                }
            }
            $update_login_data = User::where(['id' => $id])->update(['login_status' => '1']);

            return redirect('business/invite-influencers-gig/' . $is_inserted->id);
        }
    }

    public function inviteinfluencersGig(Request $request, $id = null, $type =null)
    {
        if (!Auth::guard('business')->check()) {
            return redirect('business/login');
        }
        if ($type && $type != 1) {
            return redirect('business/gig-list');
        }
        $getGigs = User::with('infulancerRating')->with([
            "Images" => function ($query) {
                return $query->select("image", "id", "user_id");
            },
            "InfluencerDetailNew" => function ($query) {
                return $query->select("name", "influencer_id");
            },
        ])
            ->select(
                "users.id",
                "users.id as uid",
                "users.name",
                "users.email",
                "users.profile",
                "users.phone_number",
                "users.country_code",
                "users.description",
                "D.facebook_friends",
                "D.twitter_friends",
                "D.instagram_friends",
                "influencer_price.plug_price",
                "influencer_price.gig_price",
                "influencer_details.dob",
                "influencer_details.gender"
            )
            ->join("influencer_social_details as D", "D.user_id", "=", "users.id")
            ->leftJoin("influencer_price", "influencer_price.user_id", "=", "users.id")
            ->leftJoin("influencer_details", "influencer_details.user_id", "=", "users.id")
            ->where([
                "user_type" => 'I',
                "status" => '1',
                "emailStatus" => '1',
                'is_deleted' => '0',
            ]);
            if($type == 1){
                $exportUser = GigUser::where(['gig_id' => $id])
                ->groupBy("id")
                ->pluck('user_id');
                $getGigs = $getGigs->whereNotIn('users.id',$exportUser);
            }
        $getGigs = $getGigs->orderBy('users.id', 'desc')->paginate(10);
        $gigPrice = Gig::where("id", $id)
            ->select("price_per_gig")
            ->first()->price_per_gig;
        //echo "<pre>"; print_r($getGigs->toArray()); exit;
        return view('business.pages.gigs.invite_influ_gig', ["id" => $id, "getGigs" => $getGigs, "gigPrice" => $gigPrice]);
        //return view('business.pages.gigs.invite_influ_gig', ["id" => $id, "type" => $type , "gigPrice" => $gigPrice]);
    }



    public function getInfluencersGig(Request $request , $id=null , $type=null){
        $getGigs = User::with('infulancerRating')->with([
            "Images" => function ($query) {
                return $query->select("image", "id", "user_id");
            },
            "InfluencerDetailNew" => function ($query) {
                return $query->select("name", "influencer_id");
            },
        ])
            ->select(
                "users.id",
                "users.id as uid",
                "users.name",
                "users.email",
                "users.profile",
                "users.phone_number",
                "users.country_code",
                "users.description",
                "D.facebook_friends",
                "D.twitter_friends",
                "D.instagram_friends",
                "influencer_price.plug_price",
                "influencer_price.gig_price",
                "influencer_details.dob",
                "influencer_details.gender"
            )
            ->join("influencer_social_details as D", "D.user_id", "=", "users.id")
            ->leftJoin("influencer_price", "influencer_price.user_id", "=", "users.id")
            ->leftJoin("influencer_details", "influencer_details.user_id", "=", "users.id")
            ->where([
                "user_type" => 'I',
                "status" => '1',
                "emailStatus" => '1',
                'is_deleted' => '0',
            ]);
            if($type == 1){
                $exportUser = GigUser::where(['gig_id' => $id])
                ->groupBy("id")
                ->pluck('user_id');
                $getGigs = $getGigs->whereNotIn('users.id',$exportUser);
            }
        if(!empty($request->query)){
            $getGigs = $getGigs->where('name' , 'like' , '%'.$request->get('query').'%');
        }
        $getGigs = $getGigs->orderBy('users.id', 'desc')->paginate(10);
        $gigPrice = Gig::where("id", $id)
            ->select("price_per_gig")
            ->first()->price_per_gig;
        //return response()->json(["id" => $id, "getGigs" => $getGigs, "gigPrice" => $gigPrice]);
        return view('business.pages.gigs.influencers-data', compact('id','getGigs','gigPrice'));
    }

    public function gigfilter(Request $request)
    {
        if ($request->isMethod('get')) {
            $data = Interest::select("id", "image", "interest_name")
                ->orderby('id', 'desc')
                ->get();
            return view('business.pages.filter_gig', ["data" => $data]);
        }
    }

    public function gigSummaryPage(Request $request)
    {
        if ($request->ajax()) {
            $ids = $request->get("users_id");

            $getGigs = User::with("Images")
                ->select(
                    "users.id",
                    "users.id as uid",
                    "users.name",
                    "users.email",
                    "users.profile",
                    "users.phone_number",
                    "users.country_code",
                    "users.description",
                    "D.facebook_friends",
                    "D.twitter_friends",
                    "D.instagram_friends",
                    "influencer_price.plug_price",
                    "influencer_price.gig_price",
                    "influencer_details.dob",
                    "influencer_details.gender"
                )
                ->join("influencer_social_details as D", "D.user_id", "=", "users.id")
                ->leftJoin("influencer_price", "influencer_price.user_id", "=", "users.id")
                ->leftJoin("influencer_details", "influencer_details.user_id", "=", "users.id")
                ->where([
                    "user_type" => 'I',
                    "status" => '1',
                    "emailStatus" => '1',
                    'is_deleted' => '0',
                ])
                ->whereRaw("(users.id in (" . $ids . "))")
                ->get();

            return response()->json(["gigs" => $getGigs]);
        }

        if ($request->isMethod('get')) {
            return view('business.pages.summary_gig_page');
        }

        if ($request->isMethod('POST')) {
            $gig_id = $request->segment(3);
            $user_id = Auth::guard('business')->id();

            $allData = $request->get("gig_prices");

            if (!empty($allData)) {
                $allData = json_decode($allData);
                $input = $request->all();
                //1 pending 2=>accept,3 reject
                $status = 1;
                $getBusinessName = User::where('id', $user_id)->first();
                $name = $getBusinessName->name;
                $date = date('Y-m-d H:i:s');

                foreach ($allData as $value) {
                    $price = $value->price;
                    $complementry_item = isset($value->compliment) ? $value->compliment : "";
                    $infulancer_id = $value->id;

                    $set_gig_price = [
                        'user_id' => $user_id,
                        'complementry_item' => $complementry_item,
                        'gig_id' => $gig_id,
                        'infulancer_id' => $infulancer_id,
                        'price' => $price,
                        'status' => 1,
                        'updated_at' => $date,
                        'created_at' => $date,
                    ];

                    GigPrice::insert($set_gig_price);

                    GigUser::insert([
                        'user_id' => $infulancer_id,
                        'gig_id' => $gig_id,
                        'created_at' => $date,
                        'updated_at' => $date,
                    ]);

                    AdminPayment::insert([
                        'user_id' => $user_id,
                        'infulencer_id' => $infulancer_id,
                        'amount' => $price,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'post_id' => $gig_id,
                        'type' => 'gig',
                    ]);

                    $message = $name . " invited you to a new Gig";
                    $notificationMessage = "invited you to a new Gig";

                    $insertNotification = Notification::insert([
                        'other_user_id' => $user_id,
                        'user_id' => $infulancer_id,
                        'message' => $message,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'type' => 'gig',
                        'old_status' => 0,
                        'notify_message' => $notificationMessage,
                        'post_id' => $gig_id,
                    ]);
                }
            }

            Session::flash('success', 'Gig has been created successfully.');
            return redirect("/business/gig-list");
        }
    }

    public function acceptGig(Request $request)
    {
        if ($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->gig_id);

            //$where = "((gig_users.status = 1 or gig_users.status = 3 ) and gig_users.infulancer_id = gig_users.user_id)";
            $where = "(gig_users.status = 1 or gig_users.status = 3 )";
            $acceptGigDetails = GigUser::where(['gig_users.gig_id' => $id])
                ->select(DB::raw(" distinct gig_users.id "), "gig_users.*", "gig_prices.price", "gig_users.complementry_item")
                ->groupBy("gig_users.id")
                ->with(['user'])
                ->select(
                    DB::raw(" distinct gig_users.id "),
                    "gig_users.*",
                    "gig_prices.price",
                    "gig_prices.infulancer_id as influence",
                    "gig_prices.complementry_item" /*,DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'gig' and user_id = ".$user." and gig_id = gig_users.gig_id and infulencer_id = gig_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated")*/
                )
                ->whereRaw($where)
                ->join("gig_prices", "gig_users.gig_id", "=", "gig_prices.gig_id")
                ->groupBy("gig_users.id")
                ->paginate(9);

            return view('business.pages.accept_gig', compact('acceptGigDetails', 'id'));
        }
    }

    public function gigCheckInConfirm(Request $request, $gig_user_id)
    {
        $gig_user_id = base64_decode($gig_user_id);
        $isUpdated = GigUser::where([
            "id" => $gig_user_id,
            "status" => '1',
            "is_checkin" => '1',
        ])->update(["status" => '3']);
        if ($isUpdated) {
            return back()->with("success", "Check In Confirm successfully");
        } else {
            return back()->with("danger", "Unable to Check In Confirm");
        }
    }

    public function acceptGigDetails(Request $request)
    {
        $user_id = Auth::guard('business')->id();
        if ($request->isMethod('get')) {
            $id = base64_decode($request->gig_id);
            $acceptGigDetails = Gig::where("post_id");

            return view('business.pages.accept_gig_details', ["acceptGigDetails" => $acceptGigDetails]);
        }
    }

    public function pendingGigList(Request $request)
    {
        if ($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->gig_id);
            $pendingGigDetails = GigUser::where(['gig_users.gig_id' => $id])
                ->where('gig_users.status', 5)
                ->with(["user", "Images"])
                ->select("gig_users.*", DB::raw("(select price from gig_prices where gig_id = gig_users.gig_id and infulancer_id  = gig_users.user_id limit 1) as price"))
                ->paginate(9);

            return view('business.pages.pending_gig_list', compact('pendingGigDetails', 'id'));
        }
    }

    public function declinedGig(Request $request)
    {
        if ($request->isMethod('get')) {
            $user = Auth::id();
            $id = base64_decode($request->gig_id);

            $declinedGigDetails = GigUser::where(['gig_users.gig_id' => $id])
                ->where('gig_users.status', 2)
                ->with("user")
                ->select("gig_users.*", DB::raw("(select price from gig_prices where gig_id = gig_users.gig_id and infulancer_id  = gig_users.user_id limit 1) as price"))
                ->paginate(9);

            return view('business.pages.declined_gig', ["declinedGigDetails" => $declinedGigDetails, 'id' => $id]);
        }
    }

    public function pendingGig(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.pending_gig');
        }
    }

    public function resubmitGig(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.re-submit-gig');
        }
    }
    ///-----------post start----------------------------//

    // public function postList(Request $request) {
    //     if($request->isMethod('Get')) {
    //      $id = Auth::guard('business')->user();

    //      return view('business.pages.post-list');
    //     }

    // }

    public function postList(Request $request)
    {
        $user = Auth::guard('business')->id();
        $get_ids = Post::selectRaw(
            "(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d"
        )
            ->where("user_id", $user)
            ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0")
            ->get()
            ->pluck("d");
        $j = [];

        if ($get_ids && !empty($get_ids) && count($get_ids) > 0) {
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids, true);

            $j = array_map(function ($e) {
                return explode(",", $e)[0];
            }, $get_ids);
        }

        $post_data = Post::with([
            "user",
            "Images",
            "postUserComplete" => function ($query) {
                return $query->select(DB::raw(" distinct post_users.id "), "post_users.*", "post_prices.price", "post_prices.complementry_item")->groupBy("post_users.id");
            },
        ])
            ->select(
                "posts.*",
                DB::raw(
                    "(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where (( post_users.status = 5) and post_prices.status = 1) and post_users.post_id = posts.id) as d"
                )
            )
            ->orderBy("posts.id", "desc")
            ->whereIn("posts.id", $j)
            ->where('posts.user_id', $user)
            ->paginate(9);

        // return $post_data;

        return view('business.pages.post-list', compact('post_data'));
    }

    public function completedPostList(Request $request)
    {
        $user = Auth::guard('business')->id();
        $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";

        $get_ids = Post::selectRaw(
            "(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d"
        )
            ->where("user_id", $user)
            ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
            ->get()
            ->pluck("d");
        $j = [];

        if ($get_ids && !empty($get_ids) && count($get_ids) > 0) {
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids, true);

            $j = array_map(function ($e) {
                $e = str_replace(",0", "", $e);
                $e = number_format($e);
                return $e;
            }, $get_ids);
        }

        $post_data = Post::with([
            "user",
            "Images",
            "postUserCompleted" => function ($query) use ($where, $user) {
                return $query->whereRaw("(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)");
            },
        ])
            ->select(
                "posts.*",
                DB::raw(
                    "(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where (( post_users.status = 5) and post_prices.status = 1) and post_users.post_id = posts.id) as d"
                )
            )
            ->orderBy("posts.id", "desc")
            ->whereIn("posts.id", $j)
            ->where('posts.user_id', $user)
            ->havingRaw("d > 0")
            ->simplePaginate(9);

        return view('business.pages.post-list', compact('post_data'));
    }

    public function postDetails(Request $request)
    {
        if ($request->isMethod('Get')) {
            $post_id = base64_decode($request->post_id);
            $post_details = Post::whereId($post_id)
                ->with(["user", "Images"])
                ->first();
            return view('business.pages.post_details', compact('post_details'));
        }
    }
    public function createPostDesc(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.create-postdesc');
        }
    }

    public function acceptPost(Request $request)
    {
        if ($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->post_id);
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $acceptPostDetails = PostUser::where(['post_users.post_id' => $id])
                ->whereNotIn('post_users.status', [6, 2, 5])
                ->select(DB::raw(" distinct post_users.id "), "post_users.*", "post_prices.price", "post_prices.complementry_item")
                ->groupBy("post_users.id")
                ->with(['postDetail', 'user'])
                ->select(
                    DB::raw(" distinct post_users.id "),
                    "post_users.*",
                    "post_prices.price",
                    "post_prices.infulancer_id as influence",
                    "post_prices.complementry_item",
                    DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = " . $user . " and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated")
                )
                ->groupBy("post_users.id")
                ->join("post_prices", "post_users.post_id", "=", "post_prices.post_id")
                ->paginate(9);

            return view('business.pages.accpet_post', compact('acceptPostDetails', 'id'));
        }
    }

    public function pendingPostList(Request $request)
    {
        if ($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->post_id);

            $pendingPostDetails = PostUser::where(['post_users.post_id' => $id])
                ->where('post_users.status', 6)
                ->with(["user", "Images"])
                ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
                ->paginate(9);

            return view('business.pages.pending_post_list', compact('pendingPostDetails', 'id'));
        }
    }

    public function pendingPost(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.pending_post');
        }
    }

    public function declinedPost(Request $request)
    {
        if ($request->isMethod('get')) {
            $user = Auth::id();
            $id = base64_decode($request->post_id);

            $declinedPostDetails = PostUser::where(['post_users.post_id' => $id])
                ->where('post_users.status', 2)
                ->with(["user", "Images"])
                ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
                ->paginate(9);
            return view('business.pages.declined_post', compact('declinedPostDetails', 'id'));
        }
    }

    public function acceptPostDetails(Request $request)
    {
        $user_id = Auth::guard('business')->id();
        if ($request->isMethod('get')) {
            $id = base64_decode($request->id);
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $acceptPostDetails = PostUser::where(['post_users.id' => $id])
                ->select(DB::raw(" distinct post_users.id "), "post_users.*", "post_prices.price", "post_prices.complementry_item")
                ->groupBy("post_users.id")
                ->with(['postDetail', 'user'])
                ->select(
                    DB::raw(" distinct post_users.id "),
                    "post_users.*",
                    "post_prices.price",
                    "post_prices.infulancer_id as influence",
                    "post_prices.complementry_item",
                    DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = " . $user_id . " and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated")
                )
                ->groupBy("post_users.id")
                ->join("post_prices", "post_users.post_id", "=", "post_prices.post_id")
                ->first();
            // return $acceptPostDetails;
            return view('business.pages.accept_post_details', compact('acceptPostDetails'));
        }

        if ($request->isMethod('post')) {
            // return $request->all();
            $postId = $request->input('post_id');
            $text = $request->input('text');

            $infulancerId = $request->input('infulancer_id');
            $imageName = $request->input('image');

            $insertShareData = ShareData::insert([
                'infulancer_id' => $infulancerId,
                'post_id' => $postId,
                'user_id' => $user_id,
                'image' => $imageName,
                'text' => $text,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();
            $getBusienssName = User::where(['id' => $user_id])->first();

            if (!empty($getBusienssName->name)) {
                $name = $getBusienssName->name;
            } else {
                $name = "dummy name";
            }

            $message = $name . " approved your Post";
            $notificationMessage = "Your post has been approved";

            $insertNotification = Notification::insert([
                'user_id' => $infulancerId,
                'other_user_id' => $user_id,
                'message' => $message,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type' => 'post',
                'old_status' => $updatePostUserStatus->status,
                'notify_message' => $notificationMessage,
                'post_id' => $postId,
            ]);

            $updatePostUserStatus->status = 3;
            $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
            $updatePostUserStatus->update();

            $notifyMessage = [
                'sound' => 1,
                'message' => $message,
                'id' => $postId,
            ];
            $message_new = ['sound' => 1, 'message' => $message, 'notifykey' => 'approved', 'id' => $postId];
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id, 'Your post has been approved', "approved", $message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'Your post has been approved', $notifyMessage);
                }
            }

            Session::flash('success', 'Post has been approved successfully.');
            return redirect(url('business/accept-post', base64_encode($postId)));
        }
    }

    public function uploadData(Request $request)
    {
        $user_id = Auth::guard('business')->id();
        $postId = base64_decode($request->post_id);
        $infulancerId = base64_decode($request->influencer_id);
        if ($request->isMethod('get')) {
            return view('business.pages.Upload-data');
        }

        if ($request->isMethod('post')) {
            $text = $request->input('text');
            $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();
            $getBusienssName = User::where(['id' => $user_id])->first();
            if (!empty($getBusienssName->name)) {
                $name = $getBusienssName->name;
            } else {
                $name = "dummy name";
            }
            $message = $name . "  needs edits of your Post";
            $notificationMessage = "requested edits of your post";
            $insertNotification = Notification::insert([
                'user_id' => $infulancerId,
                'other_user_id' => $user_id,
                'message' => $message,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type' => 'post',
                'old_status' => $updatePostUserStatus->status,
                'notify_message' => $notificationMessage,
                'post_id' => $postId,
            ]);

            $updatePostUserStatus->status = 4;
            $updatePostUserStatus->reject_post_reason = $text;
            $updatePostUserStatus->is_submit = 0;
            $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
            $updatePostUserStatus->update();

            $notifyMessage = [
                'sound' => 1,
                'message' => $message,
                'id' => $postId,
            ];
            $message_new = ['sound' => 1, 'message' => $message, 'notifykey' => 'requested edits', 'id' => $postId];
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id, 'requested edits of your post', "requested edits", $message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'requested edits of your post', $notifyMessage);
                }
            }
            Session::flash('success', 'Post has been rejected successfully.');
            return redirect(url('business/accept-post', base64_encode($postId)))->withInput();
        }
    }

    public function postReminder(Request $request)
    {
        $user_id = Auth::guard('business')->id();
        $post_id = base64_decode($request->post_id);
        $infulancerId = base64_decode($request->influencer_id);
        if ($post_id && $infulancerId) {
            $getBusienssName = User::where('id', $user_id)->first();
            $name = $getBusienssName->name;
            $updatePostUserStatus = PostUser::where(['post_id' => $post_id, 'user_id' => $infulancerId])->first();
            //notifiaction code is pending
            $message = "Please respond to " . $name;
            $notificationMessage = "post reminder";
            $notifyMessage = [
                'sound' => 1,
                'message' => $message,
                'id' => $post_id,
            ];

            $message_new = ['sound' => 1, 'message' => $message, 'notifykey' => 'post reminder', 'id' => $post_id];
            $insertNotification = Notification::insert([
                'other_user_id' => $user_id,
                'user_id' => $infulancerId,
                'message' => $message,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type' => 'post',
                'old_status' => $updatePostUserStatus->status,
                'notify_message' => $notificationMessage,
                'post_id' => $post_id,
            ]);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id, 'post reminder', 'post reminder', $message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'post reminder', $notifyMessage);
                }
            }
            Session::flash('success', 'Post reminder sent successfully.');
            return back()->withInput();
        } else {
            Session::flash('danger', 'Unable to proceed your request,please try later.');
            return back()->withInput();
        }
    }

    public function createPostField(Request $request)
    {
        $id = Auth::guard('business')->user()->id;
        if ($request->isMethod('get')) {
            return view('business.pages.create_post_filed');
        }

        if ($request->isMethod("POST")) {
            $message = [
                'post_name.required' => 'Please enter title.',
                'description.required' => 'Please enter description.',
                'what_to_include.required' => "Please enter what to do.",
                'what_not_to_include.required' => "Please enter what_not_to_include.",
                'expiration_date.required' => 'Please select expiration date',
            ];

            $this->validate(
                $request,
                [
                    'post_name' => 'required',
                    'what_to_include' => 'required',
                    'description' => 'required',
                    'what_not_to_include' => 'required',
                    'expiration_date' => 'required',
                ],
                $message
            );

            $data = [
                "post_name" => $request->input('post_name'),
                "expiration_date" => date("Y-m-d", strtotime($request->input('expiration_date'))),
                "description" => $request->input('description'),
                "what_to_include" => $request->input('what_to_include'),
                "what_not_to_include" => $request->input('what_not_to_include'),
                "expiration_date" => $request->input('expiration_date'),
                "other_requirement" => $request->input('other_requirement'),
                "price_per_post" => $request->input('price_per_post'),
                "user_id" => $id,
            ];

            $request->session()->put('post', $data);
            return redirect('business/create-post2');
        }
    }
    public function createPost2(Request $request)
    {
        $id = Auth::guard('business')->user()->id;
        if ($request->isMethod('get')) {
            return view('business.pages.create-post-2');
        }
        if ($request->isMethod("POST")) {
            $prevData = $request->session()->get("post");
            $message = [
                'hashtag.required' => 'Please enter hashtag.',
            ];

            $this->validate(
                $request,
                [
                    'hashtag' => 'required',
                ],
                $message
            );

            $data = [
                'hashtag' => $request->input('hashtag'),
            ];

            $data = array_merge($prevData, $data);

            $hashtag = $request->get("hashtag");
            if ($hashtag && !empty($hashtag)) {
                $hashtag = trim($hashtag);
                if ($hashtag[0] == "#") {
                    $hashtag = substr($hashtag, 1, strlen($hashtag));
                }
                $data["hashtag"] = $hashtag;
            }

            $is_inserted = Post::create($data);

            $video_images = $request->file("image_video");

            if ($is_inserted) {
                if ($request->hasFile("image_video")) {
                    $cnt_img_vid = count($video_images);
                    for ($k = 0; $k < $cnt_img_vid; $k++) {
                        if (isset($video_images[$k]) && !empty($video_images[$k])) {
                            $file = $this->uploadImage_control($video_images[$k], "/app/public/uploads/images/business_images/post_images", 1);
                            $insert_data = [];
                            $insert_data["media"] = $file;
                            $insert_data["post_id"] = $is_inserted->id;
                            PostImage::create($insert_data);
                        }
                    }
                }
            }

            $update_login_data = User::where(['id' => $id])->update(['login_status' => '1']);
            Session::flash('success', 'Post has been created successfully.');
            return redirect(url('business/invite-influencers-post', base64_encode($is_inserted->id)));
        }
    }

    public function uploadData2(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('business.pages.upload-data');
        }
    }

    public function inviteinfluencersPost(Request $request, $id = null)
    {
        if ($request->ajax()) {
            $facebook = $request->get("facebook");
            $twitter = $request->get("twitter");
            $instagram = $request->get("instagram");
            $price = $request->get("price");
            $gender = $request->get("gender");
            $interests = $request->get("interests");

            $search = $request->get("search");

            $isLimit = $request->get("hasLimit");

            $minReach = '20000';

            $where = [];

            // if($facebook){
            //     $where[] = " ( D.facebook_friends between 1 and ".$facebook." ) ";
            // }

            // if($twitter){
            //     $where[] = " ( D.twitter_friends between 1 and ".$twitter." ) ";
            // }

            // if($instagram){
            //     $where[] = " ( D.instagram_friends between 1 and ".$instagram." ) ";
            // }

            // if($price){
            //     $where[] = " ( influencer_price.gig_price between 1 and ".$price." ) ";
            // }

            // if($gender){
            //     $where[] = " ( influencer_details.gender = ".($gender == "male" ? 1 : 2)." ) ";
            // }

            if ($facebook && $facebook != "0-0") {
                $where[] = " ( D.facebook_friends between " . ($facebook[0] == '0' ? 1 : $facebook[0]) . " and " . explode("-", $facebook)[1] . " ) ";
            }

            if ($twitter && $twitter != "0-0") {
                $where[] = " ( D.twitter_friends between " . ($twitter[0] == '0' ? 1 : $twitter[0]) . " and " . explode("-", $twitter)[1] . " ) ";
            }

            if ($instagram && $instagram != "0-0") {
                $where[] = " ( D.instagram_friends between " . ($instagram[0] == '0' ? 1 : $instagram[0]) . " and " . explode("-", $instagram)[1] . " ) ";
            }

            if ($price && $price != "0-0") {
                $where[] = " ( influencer_price.gig_price between " . ($price[0] == '0' ? 1 : $price[0]) . " and " . explode("-", $price)[1] . " ) ";
            }

            if ($gender) {
                $wh = [];
                if ($gender["male"] != 0) {
                    $wh[] = " influencer_details.gender = '1' ";
                }

                if ($gender["female"] != 0) {
                    $wh[] = " influencer_details.gender = '2' ";
                }

                if (count($wh) > 0) {
                    $where[] = "( " . implode(" or ", $wh) . " ) ";
                }
            }

            if ($interests) {
                $emlInterest = explode(",", $interests);
                if (count($emlInterest) > 0) {
                    $emlInterest = array_map(function ($e) {
                        return " interest_id = " . $e . " ";
                    }, $emlInterest);

                    $bindInterest = implode(" and ", $emlInterest);
                    $where[] = " users.id in ( select user_id from influencer_interests where ( " . $bindInterest . "))";
                }
            }

            $getPost = User::with("Images")
                ->select(
                    "users.id",
                    "users.id as uid",
                    "users.name",
                    "users.email",
                    "users.profile",
                    "users.phone_number",
                    "users.country_code",
                    "users.description",
                    "D.facebook_friends",
                    "D.twitter_friends",
                    "D.instagram_friends",
                    "influencer_price.plug_price",
                    "influencer_price.gig_price",
                    "influencer_details.dob",
                    "influencer_details.gender"
                )
                ->join("influencer_social_details as D", "D.user_id", "=", "users.id")
                ->leftJoin("influencer_price", "influencer_price.user_id", "=", "users.id")
                ->leftJoin("influencer_details", "influencer_details.user_id", "=", "users.id")
                ->where([
                    "user_type" => 'I',
                    "status" => '1',
                    "emailStatus" => '1',
                    'is_deleted' => '0',
                ]);

            if ($search) {
                $getPost = $getPost->where(function ($query) use ($search) {
                    return $query
                        ->where("D.twitter_name", "LIKE", '%' . $search . "%")
                        ->orWhere("D.facebook_name", "LIKE", '%' . $search . "%")
                        ->orWhere("D.instagram_name", "LIKE", '%' . $search . "%")
                        ->orWhere("users.name", "LIKE", '%' . $search . "%");
                });
            }

            if (count($where) > 0) {
                $setWhere = implode(" and ", $where);
                $setWhere = " ( " . $setWhere . " ) ";
                $getPost = $getPost->orderBy('users.id', 'Desc')->whereRaw($setWhere);
            }

            if ($isLimit && !empty($isLimit)) {
                $getPost = $getPost->where(function ($query) use ($minReach) {
                    return $query
                        ->where("D.facebook_friends", ">=", $minReach)
                        ->orWhere("D.twitter_friends", ">=", $minReach)
                        ->orWhere("D.instagram_friends", ">=", $minReach);
                });

                $getPost = $getPost->orderBy("id", "desc")->paginate(20);
            } else {
                $getPost = $getPost->orderBy("id", "desc")->paginate(9);
            }

            return response()->json(["post" => $getPost]);
        }

        if ($request->isMethod('get')) {
            // return base64_decode($id);
            $postPrice = Post::where("id", base64_decode($id))
                ->select("price_per_post")
                ->first()->price_per_post;
            return view('business.pages.invite_influ_post', ["id" => $id, "postprice" => $postPrice]);
        }
    }

    public function postfilter(Request $request)
    {
        if ($request->isMethod('get')) {
            $data = Interest::select("id", "image", "interest_name")
                ->orderby('id', 'desc')
                ->get();
            return view('business.pages.filter_post', compact('data'));
        }
    }

    public function resubmitPost(Request $request)
    {
        $user_id = Auth::guard('business')->user()->id;
        if ($request->isMethod('get')) {
            $user = Auth::id();
            $id = base64_decode($request->post_id);
            $declinedPostDetails = PostUser::where(['post_users.post_id' => $id])
                ->where('post_users.status', 2)
                ->with("user", "post")
                ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
                ->first();
            return view('business.pages.re-submit-post', compact('declinedPostDetails'));
        }

        if ($request->isMethod('post')) {
            $price = $request->price;
            $post_id = base64_decode($request->post_id);
            $infulancerId = $request->infulancerId;
            $status = 1;
            $getBusinessName = User::where('id', $user_id)->first();
            $name = $getBusinessName->name;
            PostPrice::where(['infulancer_id' => $infulancerId, 'post_id' => $post_id, 'user_id' => $user_id])->update(['price' => $price, 'status' => $status, 'is_resend' => 1]);
            PostUser::where(['user_id' => $infulancerId, 'post_id' => $post_id])->update(['status' => 6, 'is_resend' => 1]);

            Session::flash('success', 'Post has been re-submit successfully.');
            return redirect(url('business/post-details', base64_encode($post_id)))->withInput();
        }
    }

    public function postSummaryPage(Request $request)
    {
        if ($request->ajax()) {
            $ids = $request->get("users_id");
            $getPost = User::select(
                "users.id as uid",
                "users.name",
                "users.email",
                "users.profile",
                "users.phone_number",
                "users.country_code",
                "users.description",
                "D.facebook_friends",
                "D.twitter_friends",
                "D.instagram_friends",
                "influencer_price.plug_price",
                "influencer_price.post_price",
                "influencer_details.dob",
                "influencer_details.gender"
            )
                ->join("influencer_social_details as D", "D.user_id", "=", "users.id")
                ->leftJoin("influencer_price", "influencer_price.user_id", "=", "users.id")
                ->leftJoin("influencer_details", "influencer_details.user_id", "=", "users.id")
                ->where([
                    "user_type" => 'I',
                    "status" => '1',
                    "emailStatus" => '1',
                    'is_deleted' => '0',
                ])
                ->whereRaw("(users.id in (" . $ids . "))")
                ->get();

            return response()->json(["posts" => $getPost]);
        }

        if ($request->isMethod('get')) {
            return view('business.pages.summary_page');
        }

        if ($request->isMethod('POST')) {
            // return $request;
            $post_id = base64_decode($request->segment(3));
            $user_id = Auth::guard('business')->id();

            $allData = $request->get("post_prices");

            if (!empty($allData)) {
                $allData = json_decode($allData);
                $input = $request->all();
                //1 pending 2=>accept,3 reject
                $status = 1;
                $getBusinessName = User::where('id', $user_id)->first();
                $name = $getBusinessName->name;
                $date = date('Y-m-d H:i:s');

                foreach ($allData as $value) {
                    $price = $value->price;
                    $complementry_item = isset($value->compliment) ? $value->compliment : "";
                    $infulancer_id = $value->id;

                    $set_post_price = [
                        'user_id' => $user_id,
                        'complementry_item' => $complementry_item,
                        'post_id' => $post_id,
                        'infulancer_id' => $infulancer_id,
                        'price' => $price,
                        'status' => 1,
                        'updated_at' => $date,
                        'created_at' => $date,
                    ];

                    PostPrice::insert($set_post_price);

                    PostUser::insert([
                        'user_id' => $infulancer_id,
                        'post_id' => $post_id,
                        'created_at' => $date,
                        'updated_at' => $date,
                    ]);

                    AdminPayment::insert([
                        'user_id' => $user_id,
                        'infulencer_id' => $infulancer_id,
                        'amount' => $price,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'post_id' => $post_id,
                        'type' => 'post',
                    ]);

                    $message = $name . " invited you to a new Post";
                    $notificationMessage = "invited you to a new Post";

                    $insertNotification = Notification::insert([
                        'other_user_id' => $user_id,
                        'user_id' => $infulancer_id,
                        'message' => $message,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'type' => 'post',
                        'old_status' => 0,
                        'notify_message' => $notificationMessage,
                        'post_id' => $post_id,
                    ]);
                }
            }

            Session::flash('success', 'Post has been created successfully.');
            return redirect("/business/post-list");
        }
    }

    private static function addImagesGig(Request $request, $gig_id)
    {
        $is_inserted = 0;
        if ($request->hasFile("media")) {
            $images = $request->file("media");
            $count = count($images);
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/gig_images", 1);

                    $create_array = ["gig_id" => $gig_id, "media" => $image_name];
                    GigMedia::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0) {
            return response()->json(["message" => "Gig image saved successfully."]);
        }
    }

    private static function addImagesPost(Request $request, $post_id)
    {
        $is_inserted = 0;
        if ($request->hasFile("media")) {
            $images = $request->file("media");
            $count = count($images);
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/post_images", 1);

                    $create_array = ["post_id" => $post_id, "media" => $image_name];
                    PostImage::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0) {
            return response()->json(["message" => "Post image saved successfully."]);
        }
    }

    public static function uploadImage_control($request_profile, string $dir = "/app/public/uploads/images/business", $multiple = false)
    {
        $return_data = "";
        $profile = null;
        if ($multiple) {
            $profile = $request_profile;
        } elseif ($request_profile->hasFile("media")) {
            $profile = $request_profile->file("media");
        }

        if ($profile) {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalName();
            $file_name = uniqid() . "_" . $file_name;
            $file_name = str_replace([" ", "(", ")"], "", $file_name);
            if (!file_exists($path)) {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            $return_data = $file_name;
        }

        return $return_data;
    }

    function confirmCheckInRequest($gigId , $infulancerId){
        $gigId = base64_decode($gigId);
        $infulancerId = base64_decode($infulancerId);
        $user_id = Auth::guard('business')->id();
        $updatePostUserStatus = GigUser::where(['gig_id' => $gigId, 'user_id' => $infulancerId])->first();
        $getBusienssName = User::where(['id' => $user_id])->first();
        if (!empty($getBusienssName->name)) {
            $name = $getBusienssName->name;
        } else {
            $name = "dummy name";
        }

        $message = $name . "  checked you in";
        $notificationMessage =  "checked you in";
        $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$gigId]);

        $updatePostUserStatus->status = 3;
        $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
        $updatePostUserStatus->update();


        // $notifyMessage = array(
        //     'sound' => 1,
        //     'message' => $message,
        //     'id' => $gigId
        // );
        // $message_new = array('sound' =>1,'message'=>$message,
        //                     'notifykey'=>'checked','id'=>$gigId);
        // $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
        // if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
        //     if ($getInfulancerUser->device_type == '2') {
        //         $this->send_android_notification($getInfulancerUser->device_id,'checked you in', "checked" ,$message_new);
        //     } else {
        //         $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'checked you in', $notifyMessage);
        //     }
        // }
        return response()->json(["message" => "Gig checkin request  Accepted successfully."]);
    }
}
