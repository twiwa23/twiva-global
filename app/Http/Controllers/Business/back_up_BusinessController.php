<?php

namespace App\Http\Controllers\Businessssssss;

header('Cache-Control: no-store, private, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);

use DB;
use Hash;
use Session;
use App\model\User;
use App\model\Post;
use App\model\Gig;
use App\model\Interest;
use App\model\InfluencerImages;
use App\model\InfluencerPrice;
use App\model\InfluencerInterests;
use App\model\InfluencerDetail;
use App\model\GigUser;
use App\model\PostUser;
use App\model\GigMedia;
use App\model\PostImage;
use App\model\GigPrice;
use App\model\PostPrice;
use App\model\BusinessDetail;
use App\model\Notification;
use App\model\Rating;
use App\model\PostDetail;
use App\model\GigDetail;
use App\model\MpesaPhoneNumber;
use App\model\Payment;
use App\model\AdminPayment;
use App\model\Admin;
use App\model\GigMediaDetail;
use App\model\PostDetailsMedia;
use App\model\InfulancerRating;
use App\model\ShareData;
use App\model\TimelineImages;
use App\model\Timeline;
use App\model\TimelineLikes;
use App\model\CallBackTransaction;
use App\model\Portfolio;
use App\model\Services;
use App\model\Blogger;
use App\model\Rate;
use App\model\Category;
use App\model\CategoryUser;
use Storage;
use Carbon\Carbon;
use Route;
use Safaricom\Mpesa\Mpesa;
use Validator;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\v2\BaseTrait;

class BusinessControllersssss extends Controller
{
    use BaseTrait;

      public function influencersList(Request $request) {
        if($request->isMethod('Get')) {
          $search = $request->get('search');
          $id = Auth::guard('business')->user();
           
          $infulancer_list = User::with(["Images", "Interests", "SocialDetail", "price", "InfluencerDetail","infulancerRating"])
                              ->select("users.id","users.id as uid","users.name","users.email","users.profile","users.phone_number","users.country_code","users.description","D.facebook_friends","D.twitter_friends","D.instagram_friends","influencer_price.plug_price","influencer_price.gig_price","influencer_details.dob","influencer_details.gender")
                                ->join("influencer_social_details as D","D.user_id","=","users.id")
                                ->leftJoin("influencer_price","influencer_price.user_id","=","users.id")
                                ->leftJoin("influencer_details","influencer_details.user_id","=","users.id")
                                ->inRandomOrder()
                                ->where([
                                        "user_type" => 'I',
                                        "status" => '1',
                                        "emailStatus" => '1',
                                        'is_deleted' => '0',

                                ]);

                if($search){
                    $infulancer_list = $infulancer_list->where(function($query) use($search){
                        return $query->where("D.twitter_name","LIKE",'%'.$search."%")
                                     ->orWhere("D.facebook_name","LIKE",'%'.$search."%")
                                     ->orWhere("D.instagram_name","LIKE",'%'.$search."%")
                                     ->orWhere("users.name","LIKE",'%'.$search."%");
                    });
                }

                // return $infulancer_list;

            $infulancer_list = $infulancer_list->orderBy('users.id','desc')->paginate(9);

           return view('business.pages.influ_list',compact('infulancer_list'));
       }

    }

    public function influencersDetails(Request $request) {
        if($request->isMethod('Get')) {
            /* $id = Auth::guard('business')->user();*/
            $infulancerDetail = User::with(["Images", "Interests", "SocialDetail", "price", "InfluencerDetail","infulancerRating"])
                                      ->where(["id"=>base64_decode($request->id)])->first();

            return view('business.pages.influ_details',compact('infulancerDetail'));
        }

    }



    public function notification(Request $request) {
        if($request->isMethod('Get')) {
           $id = Auth::guard('business')->user()->id;
           $getNotificationList = Notification::where('user_id', $id)->with("otherUser")
                ->orderBy("id", "DESC")
                ->paginate(10);

            foreach ($getNotificationList as $notification) {
                if($notification->type == "post") {
                    $postUserStatus = PostUser::where(['post_id' => $notification->post_id, 'user_id' => $notification->other_user_id])->first();
                    if($postUserStatus) {
                        $notification->new_status = $postUserStatus->status;
                    } else {
                        $notification->new_status = 0;
                    }
                } else {
                    $postUserStatus = GigUser::where(['gig_id' => $notification->post_id, 'user_id' => $notification->other_user_id])->first();
                    if($postUserStatus) {
                        $notification->new_status = $postUserStatus->status;
                    } else {
                        $notification->new_status = 0;
                    }
                }
            }
           //return $getNotificationList;
            return view('business.pages.notification',compact('getNotificationList'));
        }

    }
    public function settings(Request $request) {
        if($request->isMethod('Get')) {
           $id = Auth::guard('business')->user()->id;
           $user = User::where('id',$id)->first();
           return view('business.pages.settings',compact('user'));
       }

    }

    public function inviteInfluencersList(Request $request) {
        if($request->isMethod('Get')) {
           $id = Auth::guard('business')->user();
           return view('business.pages.invite_influ');
       }

    }


        //---------------gig start-----------------------//
    public function gigList(Request $request) {
        if($request->isMethod('Get')) {
           $id = Auth::guard('business')->user()->id;

          $where = "(gig_users.status = 4 and gig_prices.infulancer_id = gig_users.user_id)";
         $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
            ->where("user_id",$id)
            ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
            ->get()->pluck("d");
            $j = [];

            if($get_ids && !empty($get_ids) && count($get_ids) > 0){
                $get_ids = json_encode($get_ids);
                $get_ids = json_decode($get_ids,true);

                $j = array_map(function($e){
                    return number_format(str_replace(",0","",$e));
                   
                },$get_ids);
            }

           $gig_list = Gig::with("Images")
                        ->where(['user_id' => $id])
                        ->whereNotIn('id',$j)
                        ->orderby("id", "DESC");

           $gig_list = $gig_list->paginate(6);
           return view('business.pages.gig_list',compact('gig_list','id'));
       }

    }

    public function getGigCompleteList(Request $request){
        if($request->isMethod("GET")){
            $user = Auth::guard("business")->id();

           
                $where = "(gig_users.status = 4 and gig_prices.infulancer_id = gig_users.user_id)";

                // $where = "((gig_users.status = 0 or gig_users.status = 2) and (gig_prices.status = 0 or gig_prices.status = 2))";
         $get_ids = Gig::selectRaw("(select concat(GROUP_CONCAT(DISTINCT gig_users.gig_id),',',(SUM(gig_users.status)/count(gig_users.gig_id)%4)) from gig_users join gig_prices on gig_prices.infulancer_id = gig_users.user_id  where gig_prices.status = 1 and gig_users.gig_id = gigs.id ) as d")
            ->where("user_id",$user)
            ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
            ->get()->pluck("d");
            $j = [];

            if($get_ids && !empty($get_ids) && count($get_ids) > 0){
                $get_ids = json_encode($get_ids);
                $get_ids = json_decode($get_ids,true);

                $j = array_map(function($e){
                    return number_format(str_replace(",0","",$e));
                   
                },$get_ids);
            }

           $gigData = Gig::with(["user", "Images"])
                ->select("gigs.*")
                ->orderBy("gigs.id", "desc")
                ->whereIn("gigs.id",$j)
                ->where('gigs.user_id', $user)
                ->paginate(9);

         return view('business.pages.gig_complete_list',compact('gigData'));
        }
    }

    public function gigDetails(Request $request) {
        if($request->isMethod('Get')) {
         $gig_detail = Gig::with("Images")
         ->where(['id' =>base64_decode($request->id)])->first();

         return view('business.pages.gig_detail',compact('gig_detail'));
     }

    }

    public function createGigDesc(Request $request) {
        if($request->isMethod('GET')) {
            return view('business.pages.create-gigdesc');
        } 

    }


    public function createGigFiled2(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.create_gig_filed2');
        }
        $id = Auth::guard('business')->user()->id;
        if($request->isMethod("POST")){
            $message = ['title.required'=>'Please enter title','title.max' => 'Title should not be greater than 30 characters','description.required' => 'Please enter description', 'description.max' => 'Description should not be greater than 1000 characters','what_to_do.required'=>"Please enter what to do",'what_to_do.max' => 'What to do should not be greater than 1000 characters','contact_person_name.required'=>"Please enter contact person name",'contact_person_name.max' => 'Contact person name should not be greater than 50 characters','contact_person_number.digits_between'=>'Contact person number should be 8 to 15 digits.','gig_start_date.required'=>'Please select gig start date','contact_person_name.required'=>'Please enter contact person number','gig_end_date.required'=>'Please select gig end date','gig_start_time.required'=>'Please select gig start time','gig_end_time.required'=>'Please select gig end time',

        ];

        $this->validate($request, ['title' => 'required|max:30','what_to_do' =>'required|max:1000', 
           'description'=>'required|max:1000','contact_person_name'=>'required|max:50','contact_person_number' =>  'required|digits_between:8,15','gig_start_date'=>'required','gig_end_date'=>'required','gig_start_time'=>'required','gig_end_time'=>'required',
       ], $message);

        $request->session()->put('gig', [
            "gig_name" => $request->input('title'),
            "gig_end_date_time" =>date("Y-m-d H:i:s",strtotime($request->input('gig_end_date')." ". $request->input('gig_end_time'))),
            "gig_start_date_time"=> date("Y-m-d H:i:s",strtotime($request->input('gig_start_date')." ". $request->input('gig_start_time'))),
            "description"=> $request->input('description'),
            "what_to_do"=> $request->input('what_to_do'),
            "phone_number"=>$request->input('contact_person_number'),
            "owner_name"=>$request->input('contact_person_name'),
            "user_id"=>$id
        ]); 

        return redirect('business/create-gig-field');


    }


    }

     public function createGigField(Request $request) {
         $id = Auth::guard('business')->user()->id;
        if($request->isMethod('get')) {
            return view('business.pages.create_gig_filed');
        }
         if($request->isMethod("POST")){
            $prevData =  $request->session()->get("gig");
          $message = ['gig_price_offering.required'=>'Please enter gig price offering',
                      'address.required'=>'Please enter address',
                      'venue.required'=>'Please enter venue'
              ];
              
              $this->validate($request, [
                        'gig_price_offering'=>'required|digits_between:1,8',
                          'address'=>'required|max:1000',
                          'venue'=>'required|max:100',

              ], $message);


               $second_page_data = [
                                  'price_per_gig'=>$request->input('gig_price_offering'),
                                  'location'=>$request->input('address'),
                                  'venue'=>$request->input('venue'),''
                                ];

                  $data = array_merge($prevData,$second_page_data);
                  $hashtag = $request->get("hashtag");
                  if($hashtag && !empty($hashtag)){
                      $hashtag = trim($hashtag);
                      if($hashtag[0] == "#"){
                          $hashtag = substr($hashtag,1,strlen($hashtag));
                      }
                      $data["hashtag"] = $hashtag;
                  }

                   $is_inserted = Gig::create($data);
                  //   if($is_inserted){
                  //         if($request->hasFile("image_video")){
                  //         $cnt_img_vid = count($video_images);
                  //         for($k = 0; $k < $cnt_img_vid;$k++){
                  //             if(isset($video_images[$k]) && !empty($video_images[$k])){
                  //                 $file = $this->uploadImage_control($video_images[$k], "/app/public/uploads/images/business_images/gig_images", 1);
                  //                 $insert_data = array();
                  //                 $insert_data["media"] = $file;
                  //                 $insert_data["gig_id"] = $is_inserted->id;
                  //                 GigMedia::create($insert_data);
                  //             }
                  //         }
                  //     }     
                  // }
                    $images = $this->parseFiles($request,"images_videos");

                      if($images){
                        $all_media = array();
                        $video_image = $images;
                        $len = count($images);
                        $j = 1; 
                        if($len > 0){
                          $j = 0;
                          for($i = 0;$i < $len;$i++){
                            if(isset($video_image[$i])){
                              $cnt_img_vid = count($video_image[$i]);
                              for($k = 0; $k < $cnt_img_vid;$k++){
                                $file = $this->uploadImage_control($video_image[$i][$k], "/app/public/uploads/images/business_images/gig_images", 1);

                                  $insert_data = array();
                                  $insert_data["media"] = $file;
                                  $insert_data["gig_id"] = $is_inserted->id;
                                  GigMedia::create($insert_data);

                              }             
                            }
                          } 
                        }
                      }

            $update_login_data = User::where(['id'=>$id])->update(['login_status'=>'1']);
          
            Session::flash('success', 'Your gig has been created successfully, please select influencers to work with.');
            return redirect('business/invite-influencers-gig/'.$is_inserted->id);
         }
    }

    private function parseFiles($request,$inputFiles){
      $images = $request->file($inputFiles);
  
        /* remove not acceptable images/videos*/
        if($images && !empty($images)){
          $not_accept = array();
          $non_acceptable = $request->get("non_acceptable_files");

          if($non_acceptable && !empty($non_acceptable)){
            $exp = explode(",",$non_acceptable);
            $exp = array_filter($exp);
            $not_accept = array_values($exp);
          }
          
          for($w = 0;$w < count($not_accept);$w++){
            $val = $not_accept[$w];
            if(isset($images[$val[0]][$val[2]])){
              unset($images[$val[0]][$val[2]]);
            }
          }
        
          $images = array_filter($images);
          $images = array_values($images);
          $images = array_map(function($x){ return array_values($x); }, $images);
        }
        return $images;
    }

   public function inviteinfluencersGig(Request $request,$id = null) {
        if($request->ajax()){
            $facebook = $request->get("facebook");
            $twitter = $request->get("twitter");
            $instagram = $request->get("instagram");
            $price = $request->get("price");
            $gender = $request->get("gender");
            $interests = $request->get("interests");

            $search = $request->get("search");

            $isLimit = $request->get("hasLimit");

            $minReach = '20000';

            $where = [];

            if($facebook && $facebook != "0-0"){
                $where[] = " ( D.facebook_friends between ".($facebook[0] == '0' ? 1 : $facebook[0])." and ".$facebook[2]." ) ";
            }

            if($twitter  && $twitter != "0-0"){
                $where[] = " ( D.twitter_friends between ".($twitter[0] == '0' ? 1 : $twitter[0])." and ".explode("-",$twitter)[1]." ) ";
            }

            if($instagram  && $instagram != "0-0"){
                $where[] = " ( D.instagram_friends between ".($instagram[0] == '0' ? 1 : $instagram[0])." and ".explode("-",$instagram)[1]." ) ";
            }

            if($price  && $price != "0-0"){
                $where[] = " ( influencer_price.gig_price between ".($price[0] == '0' ? 1 : $price[0])." and ".explode("-",$price)[1]." ) ";
            }

            if($gender){
                $wh = [];
                if($gender["male"] != 0){
                  $wh[] = " influencer_details.gender = '1' ";
                }

                if($gender["female"] != 0){
                  $wh[] = " influencer_details.gender = '2' ";
                }

                if(count($wh) > 0){
                  $where[] = "( ". implode(" or ", $wh) . " ) ";
                }

            }


            if($interests){
              $emlInterest = explode(",", $interests);
              if(count($emlInterest) > 0){
                $emlInterest = array_map(function($e){
                    return " interest_id = ".$e." ";
                },$emlInterest);

                $bindInterest = implode(" and ",$emlInterest);
                $where[] = " users.id in ( select user_id from influencer_interests where ( ".$bindInterest."))";
              }
            }

             $getGigs = User::with(["Images" => function($query){
                return $query->select("image","id","user_id");
             }])->select("users.id","users.id as uid","users.name","users.email","users.profile","users.phone_number","users.country_code","users.description","D.facebook_friends","D.twitter_friends","D.instagram_friends","influencer_price.plug_price","influencer_price.gig_price","influencer_details.dob","influencer_details.gender")
                        ->join("influencer_social_details as D","D.user_id","=","users.id")
                        ->leftJoin("influencer_price","influencer_price.user_id","=","users.id")
                        ->leftJoin("influencer_details","influencer_details.user_id","=","users.id")
                        ->inRandomOrder()
                        ->where([
                                "user_type" => 'I',
                                "status" => '1',
                                "emailStatus" => '1',
                                'is_deleted' => '0',

                            ]);

                if($search){
                    $getGigs = $getGigs->where(function($query) use($search){
                        return $query->where("D.twitter_name","LIKE",'%'.$search."%")
                                     ->orWhere("D.facebook_name","LIKE",'%'.$search."%")
                                     ->orWhere("D.instagram_name","LIKE",'%'.$search."%")
                                     ->orWhere("users.name","LIKE",'%'.$search."%");
                    });
                }

             if(count($where) > 0){
                $setWhere = implode(" and ", $where);
                $setWhere = " ( ".$setWhere." ) ";
                $getGigs = $getGigs->whereRaw($setWhere);  
             }
             if($isLimit && !empty($isLimit)){
                $getGigs = $getGigs->where(function($query) use($minReach){
                    return $query->where("D.facebook_friends",">=",$minReach)
                                  ->orWhere("D.twitter_friends",">=",$minReach)
                                  ->orWhere("D.instagram_friends",">=",$minReach);
                });

                $getGigs = $getGigs->orderBy('users.id','desc')->paginate(20);
             }else {
                $getGigs = $getGigs->orderBy('users.id','desc')->paginate(9);
             }


            return response()->json(["gigs" => $getGigs]);
        }

        if($request->isMethod('GET')) {
            $gigPrice = Gig::where("id",$id)->select("price_per_gig")->first()->price_per_gig;
            return view('business.pages.invite_influ_gig',["id" => $id,"gigprice" => $gigPrice]);
        }
    }   


    public function gigfilter(Request $request) {
        if($request->isMethod('get')) {
            $data = Interest::select("id","image","interest_name")->orderby('id','desc')->get();
            return view('business.pages.filter_gig',["data" => $data]);
        }
    }  


    public function gigSummaryPage(Request $request) {
        if($request->ajax()){

            $ids = $request->get("users_id");

            $getGigs = User::with("Images")->select("users.id","users.id as uid","users.name","users.email","users.profile","users.phone_number","users.country_code","users.description","D.facebook_friends","D.twitter_friends","D.instagram_friends","influencer_price.plug_price","influencer_price.gig_price","influencer_details.dob","influencer_details.gender")
                        ->join("influencer_social_details as D","D.user_id","=","users.id")
                        ->leftJoin("influencer_price","influencer_price.user_id","=","users.id")
                        ->leftJoin("influencer_details","influencer_details.user_id","=","users.id")
                        ->where([
                                "user_type" => 'I',
                                "status" => '1',
                                "emailStatus" => '1',
                                'is_deleted' => '0',

                            ])
                        ->whereRaw("(users.id in (".$ids."))")
                        ->get();

            return response()->json(["gigs" => $getGigs]);
        }

        if($request->isMethod('get')) {
            return view('business.pages.summary_gig_page');
        }

        if($request->isMethod('POST')){
            $gig_id = $request->segment(3);
            $user_id = Auth::guard('business')->id();

            $allData = $request->get("gig_prices");

            if(!empty($allData)){

                $allData = json_decode($allData);
                $input = $request->all();
                //1 pending 2=>accept,3 reject
                $status = 1;
                $getBusinessName = User::where('id', $user_id)->first();
                $name = $getBusinessName->name;
                $date = date('Y-m-d H:i:s');

                foreach ($allData as $value) {
                    $price = $value->price;
                    $complementry_item = isset($value->compliment) ? $value->compliment : "";
                    $infulancer_id = $value->id;
                
                    $set_gig_price = [
                        'user_id' => $user_id,
                        'complementry_item' => $complementry_item,
                        'gig_id' => $gig_id,
                        'infulancer_id' => $infulancer_id,
                        'price' => $price,
                        'status' => 1,
                        'updated_at' => $date,
                        'created_at' => $date
                     ];

                    GigPrice::insert($set_gig_price);

                    GigUser::insert([
                                'user_id' => $infulancer_id,
                                'gig_id' => $gig_id,
                                'created_at' => $date,
                                'updated_at' => $date
                              ]);

                    AdminPayment::insert([
                                'user_id' => $user_id,
                                'infulencer_id' => $infulancer_id,
                                'amount' => $price,
                                'created_at' => $date,
                                'updated_at' => $date,
                                'post_id' => $gig_id,
                                'type' => 'gig'
                            ]);
                    
                    $message = $name . " invited you to a new Gig";
                    $notificationMessage = "invited you to a new Gig";

                    $insertNotification = Notification::insert([
                                'other_user_id' => $user_id,
                                'user_id' => $infulancer_id,
                                'message' => $message,
                                'created_at' => $date,
                                'updated_at' => $date,
                                'type' => 'gig',
                                'old_status' => 0,
                                'notify_message' => $notificationMessage,
                                'post_id' => $gig_id
                            ]);         
                }
            }

            Session::flash('success', 'Gig created successfully.');
            return redirect("/business/gig-list");
        }
    }

    public function acceptGig(Request $request) {
		if($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->gig_id);
			
            //$where = "((gig_users.status = 1 or gig_users.status = 3 ) and gig_users.infulancer_id = gig_users.user_id)";
			$where = "(gig_users.status = 1 or gig_users.status = 3 )";
            $acceptGigDetails = GigUser::where(['gig_users.gig_id' => $id])
                                ->select(DB::raw(" distinct gig_users.id "),"gig_users.*","gig_prices.price","gig_users.complementry_item")
                                ->groupBy("gig_users.id")
                                ->with(['user']) 	
                                ->select(DB::raw(" distinct gig_users.id "),"gig_users.*","gig_prices.price","gig_prices.infulancer_id as influence","gig_prices.complementry_item"/*,DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'gig' and user_id = ".$user." and gig_id = gig_users.gig_id and infulencer_id = gig_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated")*/)
								->whereRaw($where)
                                ->join("gig_prices","gig_users.gig_id","=","gig_prices.gig_id")
								->groupBy("gig_users.id")
								->paginate(9);
								
            return view('business.pages.accept_gig',compact('acceptGigDetails','id'));
        }
		
    } 
	
	public function gigCheckInConfirm(Request $request,$gig_user_id){
		$gig_user_id = base64_decode($gig_user_id);
		$isUpdated = GigUser::where([
									"id" => $gig_user_id,
									"status" => '1',
									"is_checkin" => '1'
									])
								->update(["status" => '3']);
		if($isUpdated){
			return back()->with("success","Check In Confirm successfully");
		}else {
			return back()->with("danger","Unable to Check In Confirm");
		}
		
	}
	
	public function acceptGigDetails(Request $request) {
        $user_id = Auth::guard('business')->id();
        if($request->isMethod('get')) {
            $id = base64_decode($request->gig_id);
			$acceptGigDetails = Gig::where("post_id");
								
            return view('business.pages.accept_gig_details',["acceptGigDetails" => $acceptGigDetails]);
        }
    }


    public function pendingGigList(Request $request) {
        if($request->isMethod('get')) {
			$user = Auth::guard('business')->id();
            $id = base64_decode($request->gig_id);
            $pendingGigDetails = GigUser::where(['gig_users.gig_id' => $id])->where('gig_users.status',5)
                ->with(["user","Images"])
                ->select("gig_users.*", DB::raw("(select price from gig_prices where gig_id = gig_users.gig_id and infulancer_id  = gig_users.user_id limit 1) as price"))
                ->paginate(9);
				
            return view('business.pages.pending_gig_list',compact('pendingGigDetails','id'));
        }
    } 

    public function declinedGig(Request $request) {
        if($request->isMethod('get')) {
			$user = Auth::id();
            $id = base64_decode($request->gig_id);

            $declinedGigDetails = GigUser::where(['gig_users.gig_id' => $id])->where('gig_users.status',2)
                ->with("user")
                ->select("gig_users.*", DB::raw("(select price from gig_prices where gig_id = gig_users.gig_id and infulancer_id  = gig_users.user_id limit 1) as price"))
                ->paginate(9);
				
            return view('business.pages.declined_gig',["declinedGigDetails" => $declinedGigDetails,'id'=>$id]);
        }
    }


    public function pendingGig(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.pending_gig');
        }
    } 


    public function resubmitGig(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.re-submit-gig');
        }
    } 
        ///-----------post start----------------------------//


    // public function postList(Request $request) {
    //     if($request->isMethod('Get')) {
    //      $id = Auth::guard('business')->user();

    //      return view('business.pages.post-list');
    //     }

    // }

    public function postList(Request $request) {
        $user = Auth::guard('business')->id();
        $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
                ->where("user_id",$user)
                ->havingRaw("SUBSTRING_INDEX(d, ',', -1) != 0")
                ->get()->pluck("d");
        $j = [];

        if($get_ids && !empty($get_ids) && count($get_ids) > 0){
            $get_ids = json_encode($get_ids);
            $get_ids = json_decode($get_ids,true);

            $j = array_map(function($e){
                return explode(",",$e)[0];
               
            },$get_ids);
        }


        $post_data = Post::with(["user", "Images", "postUserComplete" => function($query){
            return $query->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")->groupBy("post_users.id");
        }])
        ->select("posts.*", DB::raw("(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where (( post_users.status = 5) and post_prices.status = 1) and post_users.post_id = posts.id) as d"))
            ->orderBy("posts.id", "desc")
            ->whereIn("posts.id",$j)
            ->where('posts.user_id',$user)
            ->paginate(9);

        // return $post_data;

        return view('business.pages.post-list',compact('post_data'));

    }

    public function completedPostList(Request $request) {
        $user = Auth::guard('business')->id();
        $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";

        $get_ids = Post::selectRaw("(select concat(GROUP_CONCAT(DISTINCT post_users.post_id),',',(SUM(post_users.status)/count(post_users.post_id)%5)) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id  where post_prices.status = 1 and post_users.post_id = posts.id ) as d")
            ->where("user_id",$user)
            ->havingRaw("SUBSTRING_INDEX(d, ',', -1) = 0")
            ->get()->pluck("d");
            $j = [];

            if($get_ids && !empty($get_ids) && count($get_ids) > 0){
                $get_ids = json_encode($get_ids);
                $get_ids = json_decode($get_ids,true);

                $j = array_map(function($e){
                    $e = str_replace(",0","",$e);
                    $e = number_format($e);
                    return $e;
                },$get_ids);
            }


        $post_data = Post::with(["user", "Images", "postUserCompleted" => function ($query) use ($where,$user) {
                return $query->whereRaw("(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)");
            }])
              ->select("posts.*", DB::raw("(select group_concat(distinct post_users.id) from post_users join post_prices on post_prices.infulancer_id = post_users.user_id join posts on posts.id = post_users.post_id where (( post_users.status = 5) and post_prices.status = 1) and post_users.post_id = posts.id) as d"))
                ->orderBy("posts.id", "desc")
                ->whereIn("posts.id",$j)
                ->where('posts.user_id', $user)
                ->havingRaw("d > 0")
                ->simplePaginate(9);

				
        return view('business.pages.post-list',compact('post_data'));
    }

    public function postDetails(Request $request) {
        if($request->isMethod('Get')) {
            $post_id = base64_decode($request->post_id);
            $post_details = Post::whereId($post_id)->with(["user", "Images"])->first();
            return view('business.pages.post_details',compact('post_details'));
        }

    }
    public function createPostDesc(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.create-postdesc');
        }
    }

    public function acceptPost(Request $request) {
        if($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->post_id);
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $acceptPostDetails = PostUser::where(['post_users.post_id' => $id])
                                ->whereNotIn('post_users.status',[6,2,5])
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")
                                ->groupBy("post_users.id")
                                ->with(['postDetail','user']) 
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))->groupBy("post_users.id")
                                ->join("post_prices","post_users.post_id","=","post_prices.post_id")->paginate(9);
								
            return view('business.pages.accpet_post',compact('acceptPostDetails','id'));
        }
    }
	



    public function pendingPostList(Request $request) {
        if($request->isMethod('get')) {
            $user = Auth::guard('business')->id();
            $id = base64_decode($request->post_id);

            $pendingPostDetails = PostUser::where(['post_users.post_id' => $id])->where('post_users.status',6)
                ->with(["user","Images"])
                ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
                ->paginate(9);

            return view('business.pages.pending_post_list',compact('pendingPostDetails','id'));
        }
    }


    public function pendingPost(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.pending_post');
        }
    }

    public function declinedPost(Request $request) {
        if($request->isMethod('get')) {
            $user = Auth::id();
            $id = base64_decode($request->post_id);

            $declinedPostDetails = PostUser::where(['post_users.post_id' => $id])->where('post_users.status',2)
                ->with(["user","Images"])
                ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
                ->paginate(9);
            return view('business.pages.declined_post',compact('declinedPostDetails','id'));
        }
    }

    public function acceptPostDetails(Request $request) {
        $user_id = Auth::guard('business')->id();
        if($request->isMethod('get')) {
            $id = base64_decode($request->id);
            $where = "(post_users.status = 5 and post_prices.status = 1 and post_prices.infulancer_id = post_users.user_id)";
            $acceptPostDetails = PostUser::where(['post_users.id' => $id])
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.complementry_item")
                                ->groupBy("post_users.id")
                                ->with(['postDetail','user']) 
                                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user_id." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))->groupBy("post_users.id")
                                ->join("post_prices","post_users.post_id","=","post_prices.post_id")->first();
                                // return $acceptPostDetails;
            return view('business.pages.accept_post_details',compact('acceptPostDetails'));
        }

        if($request->isMethod('post')) {
            // return $request->all();
            $postId = $request->input('post_id');
            $text = $request->input('text');

            $infulancerId = $request->input('infulancer_id');
            $imageName = $request->input('image');

            $insertShareData = ShareData::insert(['infulancer_id'=>$infulancerId,'post_id'=>$postId,'user_id'=>$user_id,'image'=>$imageName,'text'=>$text,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);

            $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();
            $getBusienssName = User::where(['id' => $user_id])->first();

            if (!empty($getBusienssName->name)) {
                $name = $getBusienssName->name;
            } else {
                $name = "dummy name";
            }

            $message = $name . " approved your Post";
            $notificationMessage =  "Your post has been approved";

            $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$postId ]);

            $updatePostUserStatus->status = 3;
            $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
            $updatePostUserStatus->update();
            
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $postId
            );
            $message_new = array('sound' =>1,'message'=>$message,
                                'notifykey'=>'approved','id'=>$postId);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id,'Your post has been approved', "approved",$message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'Your post has been approved', $notifyMessage);
                }
            }

            Session::flash('success', 'Post has been approved successfully.');
            return redirect(url('business/accept-post',base64_encode($postId)));
        }
    }

    public function uploadData(Request $request) {
        $user_id = Auth::guard('business')->id();
        $postId = base64_decode($request->post_id);
        $infulancerId = base64_decode($request->influencer_id);
        if($request->isMethod('get')) {
            return view('business.pages.Upload-data');
        }

        if($request->isMethod('post')) {
            $text = $request->input('text');
            $updatePostUserStatus = PostUser::where(['post_id' => $postId, 'user_id' => $infulancerId])->first();
            $getBusienssName = User::where(['id' => $user_id])->first();
            if (!empty($getBusienssName->name)) {
                $name = $getBusienssName->name;
            } else {
                $name = "dummy name";
            }
            $message = $name . "  needs edits of your Post";
            $notificationMessage =  "requested edits of your post";
            $insertNotification = Notification::insert(['user_id' => $infulancerId, 'other_user_id' => $user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$postId ]);

            $updatePostUserStatus->status = 4;
            $updatePostUserStatus->reject_post_reason = $text;
            $updatePostUserStatus->is_submit = 0;
            $updatePostUserStatus->updated_at = date('Y-m-d H:i:s');
            $updatePostUserStatus->update();

            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $postId
            );
            $message_new = array('sound' =>1,'message'=>$message,
                                'notifykey'=>'requested edits','id'=>$postId);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id,'requested edits of your post',"requested edits", $message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'requested edits of your post', $notifyMessage);
                }
            }
            Session::flash('success', 'Post has been rejected successfully.');
            return redirect(url('business/accept-post',base64_encode($postId)))->withInput();
        }
    }

    public function postReminder(Request $request) {
        $user_id = Auth::guard('business')->id();
        $post_id = base64_decode($request->post_id);
        $infulancerId = base64_decode($request->influencer_id);
        if($post_id && $infulancerId) {
            $getBusienssName = User::where('id', $user_id)->first();
            $name = $getBusienssName->name;
            $updatePostUserStatus = PostUser::where(['post_id' => $post_id, 'user_id' => $infulancerId])->first();
            //notifiaction code is pending
            $message = "Please respond to " . $name;
            $notificationMessage = "post reminder";
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $post_id
            );
            
            $message_new = array('sound' =>1,'message'=>$message,
                                'notifykey'=>'post reminder','id'=>$post_id);
            $insertNotification = Notification::insert(['other_user_id' => $user_id, 'user_id' => $infulancerId, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=> $updatePostUserStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$post_id]);
            $getInfulancerUser = User::where(['id' => $infulancerId, 'status' => '1', 'is_deleted' => '0'])->first();
            if(!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id)) {
                if ($getInfulancerUser->device_type == '2') {
                    $this->send_android_notification($getInfulancerUser->device_id,'post reminder', 'post reminder',$message_new);
                } else {
                    $this->send_iphone_notification($getInfulancerUser->device_id, $message, $notmessage = 'post reminder', $notifyMessage);
                }
            }
            Session::flash('success', 'Post reminder sent successfully.');
            return back()->withInput();
        } else {
            Session::flash('danger', 'Unable to proceed your request,please try later.');
            return back()->withInput();
        }

    }

    public function createPostField(Request $request) {
        $id = Auth::guard('business')->user()->id;
        if($request->isMethod('get')) {
            return view('business.pages.create_post_filed');
        }

        if($request->isMethod("POST")){
            $message = [
                'post_name.required' =>'Please enter title.',
                'description.required' => 'Please enter description.',
                'what_to_include.required' =>"Please enter what to do.",
                'what_not_to_include.required' =>"Please enter what_not_to_include.",
                'expiration_date.required' =>'Please select expiration date',
            ];

            $this->validate($request, [
                'post_name' => 'required',
                'what_to_include' =>'required', 
                'description'=>'required',
                'what_not_to_include'=>'required',
                'expiration_date'=>'required',
            ], $message);

            $data = [
                "post_name" => $request->input('post_name'),
                "expiration_date" =>date("Y-m-d",strtotime($request->input('expiration_date'))),
                "description"=> $request->input('description'),
                "what_to_include"=> $request->input('what_to_include'),
                "what_not_to_include"=>$request->input('what_not_to_include'),
                "expiration_date"=>$request->input('expiration_date'),
                "other_requirement"=>$request->input('other_requirement'),
                "price_per_post"=>$request->input('price_per_post'),
                "user_id"=>$id
            ];

            $request->session()->put('post',$data); 
            return redirect('business/create-post2');
        }

    }
    public function createPost2(Request $request) {
        $id = Auth::guard('business')->user()->id;
        if($request->isMethod('get')) {
            return view('business.pages.create-post-2');
        }
        if($request->isMethod("POST")){
            $prevData =  $request->session()->get("post");
            $message = [
                'hashtag.required'=>'Please enter hashtag.'
            ];

            $this->validate($request, [
                'hashtag'=>'required'
            ], $message);

            $data = [
                'hashtag' => $request->input('hashtag')
            ];

            $data = array_merge($prevData,$data);

            $hashtag = $request->get("hashtag");
            if($hashtag && !empty($hashtag)){
                $hashtag = trim($hashtag);
                if($hashtag[0] == "#"){
                    $hashtag = substr($hashtag,1,strlen($hashtag));
                }
                $data["hashtag"] = $hashtag;
            }
            
            $is_inserted = Post::create($data);


            $video_images = $request->file("image_video");
            
          
            // if($is_inserted){
            //     if($request->hasFile("image_video")){
            //         $cnt_img_vid = count($video_images);
            //         for($k = 0; $k < $cnt_img_vid;$k++){
            //             if(isset($video_images[$k]) && !empty($video_images[$k])){
            //                 $file = $this->uploadImage_control($video_images[$k], "/app/public/uploads/images/business_images/post_images", 1);
            //                 $insert_data = array();
            //                 $insert_data["media"] = $file;
            //                 $insert_data["post_id"] = $is_inserted->id;
            //                 PostImage::create($insert_data);
            //             }
            //         }
            //     }     
            // }

            $images = $this->parseFiles($request,"images_videos");
                if($images){
                  $all_media = array();
                  $video_image = $images;
                  $len = count($images);
                  $j = 1; 
                  if($len > 0){
                    $j = 0;
                    for($i = 0;$i < $len;$i++){
                      if(isset($video_image[$i])){
                        $cnt_img_vid = count($video_image[$i]);
                        for($k = 0; $k < $cnt_img_vid;$k++){
                          $file = $this->uploadImage_control($video_image[$i][$k], "/app/public/uploads/images/business_images/post_images", 1);

                            $insert_data = array();
                            $insert_data["media"] = $file;
                            $insert_data["post_id"] = $is_inserted->id;
                            PostImage::create($insert_data);

                        }             
                      }
                    } 
                  }
                }


            $update_login_data = User::where(['id'=>$id])->update(['login_status'=>'1']);
            Session::flash('success', 'Your post has been created successfully, please select influencers to work with.');
            return redirect(url('business/invite-influencers-post',base64_encode($is_inserted->id)));
        }
    }



    public function uploadData2(Request $request) {
        if($request->isMethod('get')) {
            return view('business.pages.upload-data');
        }
    }


    public function inviteinfluencersPost(Request $request,$id = null) {
        if($request->ajax()){
            $facebook = $request->get("facebook");
            $twitter = $request->get("twitter");
            $instagram = $request->get("instagram");
            $price = $request->get("price");
            $gender = $request->get("gender");
            $interests = $request->get("interests");

            $search = $request->get("search");

            $isLimit = $request->get("hasLimit");

            $minReach = '20000';

            $where = [];

            // if($facebook){
            //     $where[] = " ( D.facebook_friends between 1 and ".$facebook." ) ";
            // }

            // if($twitter){
            //     $where[] = " ( D.twitter_friends between 1 and ".$twitter." ) ";
            // }

            // if($instagram){
            //     $where[] = " ( D.instagram_friends between 1 and ".$instagram." ) ";
            // }

            // if($price){
            //     $where[] = " ( influencer_price.gig_price between 1 and ".$price." ) ";
            // }

            // if($gender){
            //     $where[] = " ( influencer_details.gender = ".($gender == "male" ? 1 : 2)." ) ";
            // }

            if($facebook && $facebook != "0-0"){
                $where[] = " ( D.facebook_friends between ".($facebook[0] == '0' ? 1 : $facebook[0])." and ".explode("-",$facebook)[1]." ) ";
            }

            if($twitter  && $twitter != "0-0"){
                $where[] = " ( D.twitter_friends between ".($twitter[0] == '0' ? 1 : $twitter[0])." and ".explode("-",$twitter)[1]." ) ";
            }

            if($instagram  && $instagram != "0-0"){
                $where[] = " ( D.instagram_friends between ".($instagram[0] == '0' ? 1 : $instagram[0])." and ".explode("-",$instagram)[1]." ) ";
            }

            if($price  && $price != "0-0"){
                $where[] = " ( influencer_price.gig_price between ".($price[0] == '0' ? 1 : $price[0])." and ".explode("-",$price)[1]." ) ";
            }

            if($gender){
                $wh = [];
                if($gender["male"] != 0){
                  $wh[] = " influencer_details.gender = '1' ";
                }

                if($gender["female"] != 0){
                  $wh[] = " influencer_details.gender = '2' ";
                }

                if(count($wh) > 0){
                  $where[] = "( ". implode(" or ", $wh) . " ) ";
                }

            }
            

            if($interests){
              $emlInterest = explode(",", $interests);
              if(count($emlInterest) > 0){
                $emlInterest = array_map(function($e){
                    return " interest_id = ".$e." ";
                },$emlInterest);

                $bindInterest = implode(" and ",$emlInterest);
                $where[] = " users.id in ( select user_id from influencer_interests where ( ".$bindInterest."))";
              }
            }

             $getPost = User::with("Images")->select("users.id","users.id as uid","users.name","users.email","users.profile","users.phone_number","users.country_code","users.description","D.facebook_friends","D.twitter_friends","D.instagram_friends","influencer_price.plug_price","influencer_price.gig_price","influencer_details.dob","influencer_details.gender")
                        ->join("influencer_social_details as D","D.user_id","=","users.id")
                        ->leftJoin("influencer_price","influencer_price.user_id","=","users.id")
                        ->leftJoin("influencer_details","influencer_details.user_id","=","users.id")
                        ->inRandomOrder()
                        ->where([
                                "user_type" => 'I',
                                "status" => '1',
                                "emailStatus" => '1',
                                'is_deleted' => '0',

                            ]);

                if($search){
                    $getPost = $getPost->where(function($query) use($search){
                        return $query->where("D.twitter_name","LIKE",'%'.$search."%")
                                     ->orWhere("D.facebook_name","LIKE",'%'.$search."%")
                                     ->orWhere("D.instagram_name","LIKE",'%'.$search."%")
                                     ->orWhere("users.name","LIKE",'%'.$search."%");
                    });
                }

             if(count($where) > 0){
                $setWhere = implode(" and ", $where);
                $setWhere = " ( ".$setWhere." ) ";
                $getPost = $getPost->orderBy('users.id','Desc')->whereRaw($setWhere);  
             }

             if($isLimit && !empty($isLimit)){
                $getPost = $getPost->where(function($query) use($minReach){
                    return $query->where("D.facebook_friends",">=",$minReach)
                                  ->orWhere("D.twitter_friends",">=",$minReach)
                                  ->orWhere("D.instagram_friends",">=",$minReach);
                });

                $getPost = $getPost->orderBy("id","desc")->paginate(20);
             }else {
                $getPost = $getPost->orderBy("id","desc")->paginate(9);
             }

            return response()->json(["post" => $getPost]);
        }

        if($request->isMethod('get')) {
            // return base64_decode($id);
            $postPrice = Post::where("id",base64_decode($id))
              ->select("price_per_post")
              ->first()
              ->price_per_post;
            return view('business.pages.invite_influ_post',["id" => $id,"postprice" => $postPrice]);
        }

    }

    public function postfilter(Request $request) {
        if($request->isMethod('get')) {
            $data = Interest::select("id","image","interest_name")->orderby('id','desc')->get();
            return view('business.pages.filter_post',compact('data'));
        }
    }

    public function resubmitPost(Request $request) {
        $user_id = Auth::guard('business')->user()->id;
        if($request->isMethod('get')) {
            $user = Auth::id();
            $id = base64_decode($request->post_id);
            $declinedPostDetails = PostUser::where(['post_users.post_id' => $id])->where('post_users.status',2)
                ->with("user","post")
                ->select("post_users.*", DB::raw("(select price from post_prices where post_id = post_users.post_id and infulancer_id  = post_users.user_id limit 1) as price"))
                ->first();
            return view('business.pages.re-submit-post',compact('declinedPostDetails'));
        }

        if($request->isMethod('post')) {
            $price = $request->price;
            $post_id = base64_decode($request->post_id);
            $infulancerId = $request->infulancerId;
            $status = 1;
            $getBusinessName = User::where('id', $user_id)->first();
            $name = $getBusinessName->name;
            PostPrice::where(['infulancer_id' => $infulancerId, 'post_id' => $post_id, 'user_id' => $user_id])->update(['price' => $price, 'status' => $status, 'is_resend' => 1]);
            PostUser::where(['user_id' => $infulancerId, 'post_id' => $post_id])->update(['status' => 6, 'is_resend' => 1]);

            Session::flash('success', 'Post has been re-submit successfully.');
            return redirect(url('business/post-details',base64_encode($post_id)))->withInput();
            
        }
    } 

    public function postSummaryPage(Request $request) {
        if($request->ajax()){
            $ids = $request->get("users_id");
            $getPost = User::select("users.id as uid","users.name","users.email","users.profile","users.phone_number","users.country_code","users.description","D.facebook_friends","D.twitter_friends","D.instagram_friends","influencer_price.plug_price","influencer_price.post_price","influencer_details.dob","influencer_details.gender")
                        ->join("influencer_social_details as D","D.user_id","=","users.id")
                        ->leftJoin("influencer_price","influencer_price.user_id","=","users.id")
                        ->leftJoin("influencer_details","influencer_details.user_id","=","users.id")
                        ->where([
                                "user_type" => 'I',
                                "status" => '1',
                                "emailStatus" => '1',
                                'is_deleted' => '0',

                            ])
                        ->whereRaw("(users.id in (".$ids."))")
                        ->get();

            return response()->json(["posts" => $getPost]);
        }

        if($request->isMethod('get')) {
            return view('business.pages.summary_page');
        }

        if($request->isMethod('POST')){
            // return $request;
            $post_id = base64_decode($request->segment(3));
            $user_id = Auth::guard('business')->id();

            $allData = $request->get("post_prices");

            if(!empty($allData)){

                $allData = json_decode($allData);
                $input = $request->all();
                //1 pending 2=>accept,3 reject
                $status = 1;
                $getBusinessName = User::where('id', $user_id)->first();
                $name = $getBusinessName->name;
                $date = date('Y-m-d H:i:s');

                foreach ($allData as $value) {
                    $price = $value->price;
                    $complementry_item = isset($value->compliment) ? $value->compliment : "";
                    $infulancer_id = $value->id;
                
                    $set_post_price = [
                        'user_id' => $user_id,
                        'complementry_item' => $complementry_item,
                        'post_id' => $post_id,
                        'infulancer_id' => $infulancer_id,
                        'price' => $price,
                        'status' => 1,
                        'updated_at' => $date,
                        'created_at' => $date
                     ];

                    PostPrice::insert($set_post_price);

                    PostUser::insert([
                                'user_id' => $infulancer_id,
                                'post_id' => $post_id,
                                'created_at' => $date,
                                'updated_at' => $date
                              ]);

                    AdminPayment::insert([
                                'user_id' => $user_id,
                                'infulencer_id' => $infulancer_id,
                                'amount' => $price,
                                'created_at' => $date,
                                'updated_at' => $date,
                                'post_id' => $post_id,
                                'type' => 'post'
                            ]);
                    
                    $message = $name . " invited you to a new Post";
                    $notificationMessage = "invited you to a new Post";

                    $insertNotification = Notification::insert([
                                'other_user_id' => $user_id,
                                'user_id' => $infulancer_id,
                                'message' => $message,
                                'created_at' => $date,
                                'updated_at' => $date,
                                'type' => 'post',
                                'old_status' => 0,
                                'notify_message' => $notificationMessage,
                                'post_id' => $post_id
                            ]);         
                }
            }

            Session::flash('success', 'Post created successfully.');
            return redirect("/business/post-list");
        }
    }


    private static function addImagesGig(Request $request, $gig_id)
    {

        $is_inserted = 0;
        if ($request->hasFile("media"))
        {
            $images = $request->file("media");
            $count = count($images);
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/gig_images", 1);

                    $create_array = ["gig_id" => $gig_id, "media" => $image_name];
                    GigMedia::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0)
        {
            return response()->json(["message" => "Gig image saved successfully."]);
        }
    }

    private static function addImagesPost(Request $request, $post_id)
    {

        $is_inserted = 0;
        if ($request->hasFile("media"))
        {
            $images = $request->file("media");
            $count = count($images);
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    $image_name = self::uploadImage_control($images[$i], "/app/public/uploads/images/business_images/post_images", 1);

                    $create_array = ["post_id" => $post_id, "media" => $image_name];
                    PostImage::create($create_array);
                    $is_inserted++;
                }
            }
        }
        if ($is_inserted > 0)
        {
            return response()->json(["message" => "Post image saved successfully."]);
        }
    }

    public static function uploadImage_control($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false) {

        $return_data = "";
        $profile = null;
        if ($multiple)
        {
            $profile = $request_profile;
        }
        else if ($request_profile->hasFile("media"))
        {
            $profile = $request_profile->file("media");
        }

        if ($profile)
        {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalName();
            $file_name = uniqid() . "_" . $file_name;
            $file_name = str_replace(array(
                " ",
                "(",
                ")"
            ) , "", $file_name);
            if (!file_exists($path))
            {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            $return_data = $file_name;
        }

        return $return_data;
    }


   public function creators(Request $request) {  
          $category_id = $request->get("category_id");   
          $category = Category::get();
          $categoryUser = CategoryUser::get()->pluck("category_id");
          $users = User::select("users.name as user_name","users.id as user_id","users.profile as user_profile","users.background_image as background_image","users.description as user_description","categories.name as category_name","category_users.category_id as category_id","category_users.user_id as category_user_id")
              ->leftJoin('category_users','category_users.user_id', '=', 'users.id')
              ->leftJoin('categories','categories.id', '=', 'category_users.category_id')
              ->whereIn('category_users.category_id',$categoryUser);
          
            if($category_id){
              $uses = $users->where("category_users.category_id",$category_id);
            }

              $users = $users->paginate(12);
       
          return view('business/pages/creators',compact('users','category'));
      }

      public function portfolio(Request $request) {  
         $user_id = $request->id;
          if($request->ajax()){
             if($request->hasFile("image")){
                   $image = $request->file('image');
                   $imageName = $this->uploadImage($image, "/app/public/uploads/images/business", 1);
                   $updateUser = User::where('id',$auth->id)->update(['background_image'=>$imageName]);
                  }
          }
          if($request->isMethod("GET")){
          $media_type = $request->get("media-type");
         //return $auth;die;
          $portfolio = Portfolio::where('user_id',$user_id);
          if($media_type){
            if($media_type == '1'){
              $portfolio = $portfolio->where("type","image");
            }else if($media_type == '2'){
              $portfolio = $portfolio->where("type","video");
            }
          }
          $portfolio = $portfolio->get();
          $service = Services::where('user_id',$user_id)->get();
          $rate = Rate::where('user_id',$user_id)->get();
          $blogger = Blogger::where('user_id',$user_id)->get();
          $categoryUser = CategoryUser::where('user_id',$user_id)->first();
          $category = Category::where('id',$categoryUser->category_id)->first();
          $user = User::where(['id'=>$user_id])->first();
          return view('business/pages/portfolio',compact('portfolio','service','rate','category','user','user_id','blogger'));
      }
    }

}
