<?php

namespace App\Http\Controllers\Influencers;
use App\Http\Controllers\API\v2\BaseTrait;
use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\{User,TempTokens,Post,PostUser,PostPrice,Notification,AdminPayment,Gig,GigUser,GigPrice,ShareData,Payment,Rating,PostDetail,PostDetailsMedia,GigDetail};
class InfluencersController extends Controller
{
    use BaseTrait;
    public function navChange(Request $request)
    {
        $user = User::find($request->id);
        $temp = TempTokens::where('user_id', $request->id)->where('data', 'nav-switch')->first();
        if(!$user || !$temp) {
            return response()->json(["message" => "User Not Found."], 400);
        }
        try {
            $loggedInUser = auth()->guard('business')->loginUsingId($user->id);

            if (!$loggedInUser) {
                // If User not logged in, then Throw exception
                throw new UserNotFoundException();
            }
        }catch(UserNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 400);
        }

        $temp->delete();
        $redirectTo = '/influencers'. $request->url;
        return redirect($redirectTo);
    }

    public function navChangeToNew(Request $request)
    {
        $user = User::find($request->user_id);
        if(!$user) {
            return response()->json(["message" => "User Not Found."], 400);
        }
        $temp = TempTokens::create([
            'user_id' => $user->id,
            'data' => 'nav-change'
        ]);
        $temp_id = base64_encode(base64_encode($temp->id));
        $temp_user_id = base64_encode(base64_encode($temp->user_id));
        $nav_url =  config('app.business_subdomain_url'). "influencer/dashboard/". $request->nav_url;
        $url = config('app.business_subdomain_url'). "influencer/dashboard/influencer-dashboard.php?id=". $temp_id."&params=". $temp_user_id."&url=". $nav_url;
        return response([
            'url' => $url,
        ], 200);

    }

    function myInvitesPosts(){
        return view('influencers/my-invites/posts/posts');
    }

    function myInvitesPostsData(Request $request){
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $posts = PostUser::orderBy('id','desc')->with('Post')->where('user_id',$user_id)->where('status' , 6);
        if(!empty($request->name)){
        $name = $request->name;
        $posts = PostUser::orderBy('id','desc')->with('Post')->whereHas('Post', function ($query) use($name) {
            $query->where('post_name','like','%'.$name.'%');
        })->where('user_id',$user_id)->where('status' , 6);
        }
        if(!empty($request->from_date) && !empty($request->to_date)){
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            $posts = PostUser::orderBy('id','desc')->with('Post')->whereHas('Post', function ($query) use($from_date,$to_date) {
                $query->where('expiration_date', '>=' , $from_date)->where('expiration_date', '<=' , $to_date);
            })->where('user_id',$user_id)->where('status' , 6);
            }
        if(!empty($request->price) && $request->price!=0){
            if($request->price == 1){
                $price_from = 1000;
                $price_to = 5000;
            }
            if($request->price== 2){
                $price_from = 6000;
                $price_to = 9000;
            }else{
                $price_from = 10000;
                $price_to = 5000;
            }

        }
        $posts = $posts->paginate(10);
        foreach($posts as $post){
            $post->price = PostPrice::select('price')->where('post_id' , $post->post_id)->where('infulancer_id' , $user_id)->first();

        }
        return view('influencers/my-invites/posts/data' , ['posts'=>$posts]);        
    }

    function myInvitesPostDetails($id){
        $post = Post::with('Images')->where('id' , $id)->first();       
        return view('influencers/my-invites/posts/details' , ['post'=>$post]);
    }

    function acceptPostInvites($post_id){
        $user = Auth::guard('business')->user();
        PostPrice::where('post_id' , $post_id)->where('infulancer_id' , $user->id)->update([
            "status"=>1,
        ]);
        $acceptPostUser = PostUser::where(['post_id' => $post_id, 'user_id' => $user->id])->first();
        $priceData = PostPrice::where('post_id',$post_id)->where('infulancer_id',$user->id)->first();
        $message = $user->name . "Accepted your post";
        $notificationmessage = "Accepted Your Post";
        $insertNotification = Notification::insert(['user_id' =>$priceData->user_id, 'other_user_id' => $user->id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$acceptPostUser->status,'notify_message'=>$notificationmessage,'post_id'=>$post_id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $post_id
        );
        $message_new = array('sound' =>1,'message'=>$message,'notifykey'=>'accepted','id'=>$post_id);
        $acceptPostUser->status = 1;
        $acceptPostUser->update();
        $getBusinessUser = User::where(['id' => $priceData->user_id, 'status' => '1', 'is_deleted' => '0'])->first();
        // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
        //     {
        //         if ($getBusinessUser->device_type == '2')
        //             {
        //                 $this->send_android_notification($getBusinessUser->device_id,"accepted your post",'accepted your post',$message_new);
        //             }
        //             else
        //             {
        //                 $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage ="accepted your post", $notifyMessage);
        //             }
        //      }
        return redirect('influencers/my-invites/posts/posts')->with('message','Invite accepted successfully');
        //return redirect()->route('myInvitesPosts')->with('message','Invite accepted successfully');

    }

    function rejectMyInvitesPost(Request $request){
        $userdata = Auth::guard('business')->user();
        $user = $userdata->id;

        $post_id = $request->post_id;
        $reason = $request->reason;
        $rejectAmount = $request->reject_amount;
        $rejectPostUser = PostUser::where(['post_id' => $post_id, 'user_id' => $user])->first();
        $offerPriceData = PostPrice::where(['post_id' => $post_id])->first();
        $rejectPostPayment = AdminPayment::where(['post_id' => $post_id, 'infulencer_id' => $user,'type'=>'post','user_id'=>$offerPriceData->user_id])->update(['status' => 2]);
        $offerPriceData = PostPrice::where(['post_id' => $post_id])->first();
        $getInfulancerUserName = User::where(['id' => $user, 'status' => '1', 'is_deleted' => '0'])
            ->first();
        if(!empty($getInfulancerUserName->name)){
            $name = $getInfulancerUserName->name;
        }else{
            $name= "-----";
        }

        if($reason == "Low Pay"){
            $message = $name. " negotiated a higher pay";   
        }
        $message = $name. " declined your post";
        $notificationMessage = "Post offer price Reject";
        $insertNotification = Notification::insert(['user_id' => $offerPriceData->user_id, 'other_user_id' => $user, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$rejectPostUser->status,'notify_message'=>$notificationMessage,'post_id'=>$post_id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $post_id
        );
         $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'declined','id'=>$post_id);
        $rejectPostUser->status = 2;
        $rejectPostUser->reason = $reason;
        $rejectPostUser->reject_amount = $rejectAmount;
        $rejectPostUser->update();



        $getBusinessUser = User::where(['id' => $offerPriceData->user_id, 'status' => '1', 'is_deleted' => '0'])
            ->first();
        // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
        // {
        //     if ($getBusinessUser->device_type == '2')
        //     {
        //         $this->send_android_notification($getBusinessUser->device_id,'Post offer price Reject','declined' ,$message_new);
        //     }
        //     else
        //     {
        //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage = 'Post offer price Reject', $notifyMessage);
        //     }
        // }
        return redirect('influencers/my-invites/posts/posts')->with('message','Invite rejected successfully');
        //return redirect()->route('myInvitesPosts')->with('message','Invite rejected successfully');    
    }

    function myInvitesTwitterTrends(){
        return view('influencers/my-invites/twitter-trends/twitter-trends');   
    }

    function myInvitesTwitterTrendsData(Request $request){
        $userdata = Auth::guard('business')->user();
        $user = $userdata->id;  
        $tts = GigUser::with("Gig")->where(['status' => 5, 'user_id' => $user])->orderby("id", "DESC");
        if(!empty($request->name)){
            $name = $request->name;
            $tts = GigUser::with("Gig")->whereHas('Gig', function ($query) use($name) {
                $query->where('gig_name','like','%'.$name.'%');
            })->where(['status' => 5, 'user_id' => $user])->orderby("id", "DESC");    
        }
        if(!empty($request->from_date) && !empty($request->to_date)){
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            $tts = GigUser::with("Gig")->whereHas('Gig', function ($query) use($from_date , $to_date) {
                $query->where('gig_start_date_time','>=',$from_date)->where('gig_end_date_time','<=',$to_date);
            })->where(['status' => 5, 'user_id' => $user])->orderby("id", "DESC");             
        }
        $tts = $tts->paginate(10);
        foreach($tts as $tt){
            $price_comp = GigPrice::select('price','complementry_item')->where(['gig_id'=>$tt->gig_id,'infulancer_id' => $user])->first(); 
            $tt->price = $price_comp->price; 
            $tt->complementry = $price_comp->complementry_item; 
        }
        return view('influencers/my-invites/twitter-trends/data' , ['tts'=>$tts]);       
    }

    function myInvitesTwitterTrendDetails($id){
        $tt = Gig::with('Images')->where('id' , $id)->first();
        return view('influencers/my-invites/twitter-trends/details' , ['tt'=>$tt]);
    }

    function acceptTwitterTrendInvites($gig_id){
        $userdata = Auth::guard('business')->user();
        $user = $userdata->id;  
        $acceptGigUser = GigUser::where(['gig_id' => $gig_id, 'user_id' => $user])->first();
        $getInfulancerUserName = User::where(['id' => $user, 'status' => '1', 'is_deleted' => '0'])
        ->first();
        if(!empty($getInfulancerUserName->name)){

        $name = $getInfulancerUserName->name;
        }else{
        $name= "dummy name";
        }
        $offerPriceData = GigPrice::where(['gig_id' => $gig_id])->first();
        $message = $name. " accepted your gig";
        $notificationMessage  = "accepted your gig";
        $insertNotification = Notification::insert(['user_id' => $offerPriceData->user_id, 'other_user_id' => $user, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$acceptGigUser->status,'notify_message'=>$notificationMessage,'post_id'=>$gig_id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $gig_id
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'gig accepted','id'=>$gig_id);
        $acceptGigUser->status = 1;
        $acceptGigUser->update();

        $getBusinessUser = User::where(['id' => $offerPriceData->user_id, 'status' => '1', 'is_deleted' => '0'])->first();
        // if(!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
        // {
        //     if ($getBusinessUser->device_type == '2')
        //     {
        //         $this->send_android_notification($getBusinessUser->device_id,  "accepted your gig",'gig accepted', $message_new);
        //     }
        //     else
        //     {
        //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage =  "accepted your gig", $notifyMessage);
        //     }
        // }
        return redirect('/influencers/my-invites/twitter-trends/twitter-trends')->with('message','Invite accepeted successfully');         
    }

    function rejectMyInvitesTwitterTrend(Request $request){
        $userdata = Auth::guard('business')->user();
        $user = $userdata->id; 
        $reason = $request->input('reason');
        $gig_id = $request->input('gig_id');
        $rejectAmount = $request->input('reject_amount');
        $rejectGigUser = GigUser::where(['gig_id' => $gig_id, 'user_id' => $user])->first();

        $offerPriceData = GigPrice::where(['gig_id' => $gig_id])->first();
        $rejectGigPayment = AdminPayment::where(['post_id' => $gig_id, 'infulencer_id' => $user,'type'=>'gig','user_id'=>$offerPriceData->user_id])->update(['status' => 2]);
       
        $getInfulancerUserName = User::where(['id' => $user, 'status' => '1', 'is_deleted' => '0'])
            ->first();
        if(!empty($getInfulancerUserName->name)){

        $name = $getInfulancerUserName->name;
        }else{
         $name= "dummy name";
        }
        if($reason == "Low Pay"){
         $message = $name. " negotiated a higher pay";   
        }
        $message = $name. " declined your gig";
        $notificationMessage = "Gig offer price Reject";
        $insertNotification = Notification::insert(['user_id' => $offerPriceData->user_id, 'other_user_id' => $user, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$rejectGigUser->status,'notify_message'=>$notificationMessage,'post_id'=>$gig_id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $gig_id
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'gig declined','id'=>$gig_id);
        $rejectGigUser->status = 2;
        $rejectGigUser->reason = $reason;
        $rejectGigUser->reject_amount = $rejectAmount;
        $rejectGigUser->update();

        $getBusinessUser = User::where(['id' => $offerPriceData->user_id, 'status' => '1', 'is_deleted' => '0'])
            ->first();
        // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
        // {
        //     if ($getBusinessUser->device_type == '2')
        //     {
        //         $this->send_android_notification($getBusinessUser->device_id,'Gig offer price Reject','gig declined',$message_new);
        //     }
        //     else
        //     {
        //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage = 'Gig offer price Reject', $notifyMessage);
        //     }
        // }   
        return redirect('/influencers/my-invites/twitter-trends/twitter-trends')->with('message','Invite rejected successfully');         
        
    }

    function acceptedPosts(){
        return view('influencers/accepted/posts/posts');     
    }

    function acceptedPostsData(Request $request){
        $userdata = Auth::guard('business')->user();
        $user = $userdata->id; 
        $posts = PostUser::orderBy('id','desc')->with("Post")->whereIN('status', [1,4,3,5])
                ->where(['user_id' => $user])
                ->paginate(10);
        if(!empty($request->name)){
            $name = $request->name;
        $posts = PostUser::orderBy('id','desc')->with('Post')->whereHas('Post', function ($query) use($name) {
            $query->where('post_name','like','%'.$name.'%');
        })->whereIN('status', [1,4,3,5])
        ->where(['user_id' => $user])->orderby("id", "DESC")
        ->paginate(10);
        }
        if(!empty($request->from_date) && !empty($request->to_date)){
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            $posts = PostUser::orderBy('id','desc')->with('Post')->whereHas('Post', function ($query) use($from_date,$to_date) {
                $query->where('expiration_date', '>=' , $from_date)->where('expiration_date', '<=' , $to_date);
            })->whereIN('status', [1,4,3,5])
            ->where(['user_id' => $user])
            ->paginate(10);;
            }
        foreach($posts as $post){
            $post->price = PostPrice::select('price','complementry_item')->where(['post_id' => $post->post_id,'infulancer_id' => $user])->first(); 
            $post->getShareData = ShareData::where(['post_id'=>$post->post_id,'infulancer_id'=>$user])->first();
            $getPayment = Payment::where(['infulencer_id'=>$user,'type'=>'post','post_id'=>$post->post_id])->first(); 
            if($getPayment){
                $post->getPayment = 2; 
            }else{
                $post->getPayment = 3;
            }
        } 
        // dd($posts);
        return view('influencers/accepted/posts/data' , ['posts'=>$posts]);  
    }

    function acceptedPostDetails($id){
        $post = Post::with('Images')->where('id' , $id)->first();       
        return view('influencers/accepted/posts/details' , ['post'=>$post]);
    }

    function startPost($id){
        $post = Post::with('Images')->where('id' , $id)->first();  
        return view('influencers/accepted/posts/start-post' , ['post'=>$post]);
    }

    function ressubmitPost(){
        $post = Post::with('Images')->where('id' , $id)->first();  
        return view('influencers/accepted/posts/start-post' , ['post'=>$post]);        
    }

    function sendForApproval(Request $request){
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $checkUserExist = User::where(['id' => $user_id])->first();

        if (!empty($checkUserExist)) {
            if ($checkUserExist->is_deleted == "1") {
                http_response_code(400);
                echo json_encode(['result' => 'Failure', 'message' => 'Your account has been deleted by admin.']);
                exit;
            }
             if ($checkUserExist->status == "2")
            {
                 $data = [];
                http_response_code(200);
                echo json_encode(['message' => 'sucess', 'data' =>$data ]);
                exit;
            }
        }
        $Validator = Validator::make($request->all() , ['post_id' => 'required',  'post_user_id' => "required",'text_one'=>'nullable|max:1500','text_two'=>'nullable|max:1500','text_three'=>'nullable|max:1500'

        ]);

        if ($Validator->fails())
        {
            return response()
                ->json(["message" => $Validator->errors()
                ->first() ], BAD_REQUEST);
        }
        $getPostDetail = PostDetail::where(['user_id' => $user_id, 'post_id' => $request->post_id])->first();
        $resend_post_user = PostUser::where(['post_id'=>$request->post_id,'user_id'=>$user_id])->first();
        if(!empty($resend_post_user) && $resend_post_user->status == 4){
            $deletePostMediaDetail = PostDetailsMedia::where(['post_detail_id' => $getPostDetail->id, 'user_id' => $user_id])->delete();
            $deletePostDetail = PostDetail::where(['user_id' => $user_id, 'post_id' => $request
                ->post_id])
                ->delete();
            $update_post_user = PostUser::where(['post_id'=>$request
            ->post_id,'user_id'=>$user_id])->update(['status'=> 1]);
       
        }
        $getPostDetail = PostDetail::where(['user_id' => $user_id, 'post_id' => $request
            ->post_id])
            ->first();
        
        if (!empty($getPostDetail))
        {
            return redirect()->back()->with("message" , "Your post is already pending for business approval.");
        }
        $input = $request->all();
        $input['text_one'] = $request->text_one;
        $input['text_two'] = $request->text_two;
        $input['text_three'] = $request->text_three;
        $input['post_id'] = $request->post_id;
        $input['post_user_id'] = $resend_post_user->id;
        $input['user_id'] = $userdata->id;
        $insert_post = PostDetail::create($input);
        $post_id = $insert_post->id;
        $this->createPostImages($request, $post_id);
        $update_post_user = PostUser::where(['post_id'=>$request
            ->post_id,'user_id'=>$user_id])->update(['is_submit'=>1]);
        $getBusinessId = Post::where(['id'=>$request->post_id])->first();
        $getInfulancerName = User::where(['id' => $user_id, 'status' => '1', 'is_deleted' => '0'])
            ->first();
        if(!empty( $getInfulancerName->name)){

        $name = $getInfulancerName->name;
        }else{
         $name = "dummy name";   
        }
           
        $message = "Review ". $name . " Post";
        $notificationMessage = "submitted a post for your approval";
        $insertNotification = Notification::insert(['user_id' => $getBusinessId->user_id, 'other_user_id' =>$user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'post','old_status'=>$resend_post_user->status,'notify_message'=>$notificationMessage,'post_id'=>$request
            ->post_id]);
        // $notifyMessage = array(
        //     'sound' => 1,
        //     'message' => $message,
        //     'id' => $request
        //     ->post_id
        // );
        // $message_new = array('sound' =>1,'message'=>$message,
        //                     'notifykey'=>'submitted a post for your approval','id'=>$request
        //     ->post_id);
        //  $getBusinessUser = User::where(['id' => $getBusinessId->user_id])
        //     ->first();
        // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
        // {
        //     if ($getBusinessUser->device_type == '2')
        //     {
        //         $this->send_android_notification($getBusinessUser->device_id,"submitted a post for your approval", 'approval',$message_new);
        //     }
        //     else
        //      {
        //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage ="submitted a post for your approval", $notifyMessage);
        //     }
        // }
        return redirect('/influencers/accepted/posts/posts')->with("message","Post details submited successfully");
    }

    function resubmitPostView($id){
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $getPostDetails = PostDetail::where(['user_id' => $user_id, 'post_id' => $id])->first();
        $postMediaDetails = PostDetailsMedia::where(['post_detail_id' => $getPostDetails->id, 'user_id' => $user_id])->get();
        $post = Post::with('Images')->where('id' , $id)->first();  
        return view('/influencers/accepted/posts/resubmit' , ['post'=>$post , 'getPostDetails'=>$getPostDetails , 'postMediaDetails'=>$postMediaDetails]);
    }

    function createPostImages(Request $request, $post_id)
    {
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $is_inserted = 0;
        if ($request->file("media"))
        {
            $images = $request->file("media");
            $thumbnail = $request->file("thumbnail");
            $count = count($images);
            $date = date("Y-m-d H:i:s");
            if ($count > 0)
            {
                for ($i = 0;$i < $count;$i++)
                {
                    // return $user_id;
                    $image_name = $this->uploadImage_control($images[$i], "/app/public/uploads/images/business_images/post_detail_image", 1);
                    $create_array = ["post_detail_id" => $post_id, "media" => $image_name, 'user_id' => $user_id, 'created_at' => $date, 'updated_at' => $date, ];
                    PostDetailsMedia::insert($create_array);
                    $is_inserted++;
                }
            }
        }
    }   

    function uploadImage_control($request_profile, String $dir = "/app/public/uploads/images/business", $multiple = false)
    {
        $return_data = "";
        $profile = null;
        if ($multiple)
        {
            $profile = $request_profile;
        }
        else if ($request_profile->hasFile("media"))
        {
            $profile = $request_profile->file("media");
        }

        if ($profile)
        {
            $path = storage_path() . $dir;
            $file_name = $profile->getClientOriginalName();
            $file_name = uniqid() . "_" . $file_name;
            $file_name = str_replace(array(
                " ",
                "(",
                ")"
            ) , "", $file_name);
            if (!file_exists($path))
            {
                mkdir(url($dir));
            }

            $profile->move($path, $file_name);
            $return_data = $file_name;
        }

        return $return_data;
    }

    function viewPost($id){
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $post = Post::with('Images')->where('id' , $id)->first();
        $post_user = PostUser::where('post_id' , $id)->where('user_id' , $user_id)->first();
        $postview = PostDetail::where(['user_id' => $user_id, 'post_id' => $id])->first();
        $postviewmedia = PostDetailsMedia::where(['post_detail_id' => $postview->id, 'user_id' => $user_id])->get();  
        return view('/influencers/accepted/posts/view-post' , ['post'=>$post , 'postview'=>$postview , 'postviewmedia'=>$postviewmedia , 'post_user'=>$post_user]);        
    }

    function acceptedTwitterTrends(){       
        return view('/influencers/accepted/twitter-trends/twitter-trends');        
    }

    function acceptedTwitterTrendsData(Request $request){
        $userdata = Auth::guard('business')->user();
        $user = $userdata->id; 
        $tts = GigUser::with("Gig")->whereIN('status', [1,3,4])
                ->where(['user_id' => $user])->orderby("id", "DESC")
                ->paginate(10);
        if(!empty($request->name)){
            $name = $request->name;
            $tts = GigUser::with("Gig")
            ->whereHas('Gig', function ($query) use($name) { $query->where('gig_name','like','%'.$name.'%');})
            ->whereIN('status', [1,3,4])
            ->where(['user_id' => $user])->orderby("id", "DESC")
            ->paginate(10);
        }
        if(!empty($request->from_date) && !empty($request->to_date)){
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            $tts = GigUser::with("Gig")
            ->whereHas('Gig', function ($query) use($from_date,$to_date) { $query->where('gig_start_date_time','>=',$from_date)->where('gig_end_date_time' , '<=' , $to_date);})
            ->whereIN('status', [1,3,4])
            ->where(['user_id' => $user])->orderby("id", "DESC")
            ->paginate(10);
        }
        foreach($tts as $tt){
            $tt->getPrice = GigPrice::select('price','complementry_item')->where(['gig_id'=>$tt->gig_id,'infulancer_id' => $user])->first(); 
            $getPayment = Payment::where(['type'=>'gig','infulencer_id'=>$user,'post_id'=>$tt->gig_id])->first();
            if(!empty($getPayment)){
                $tt->getPayment = 2; 
            }else{
                $tt->getPayment = 3;
            }  
            $getRatingData = Rating::where(['user_id'=>$user,'type'=>'gig','post_id'=>$tt->gig_id])->first();
            if(!empty($getRatingData)){
              $tt->is_rate = 1; 
            }else{
              $tt->is_rate = 0;
            }              
        }        
        // dd($tts);
        return view('/influencers/accepted/twitter-trends/data' , ['tts'=>$tts]); 
    }

    function acceptedTwitterTrendsDetails($id){
        $tt = Gig::with('Images')->where('id' , $id)->first();
        return view('influencers/accepted/twitter-trends/details' , ['tt'=>$tt]);
    }

    function checkIn($id){
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $date = date('Y-m-d H:i:s');
        $twitter_trend_details = GigDetail::where(['user_id' => $user_id, 'gig_id' => $id])->first();
        if (!empty($twitter_trend_details))
        {
        return redirect()->back()->with("error" , "Your check in request is already pending for business approval.");
        }  
        $tt = new GigDetail();
        $tt->user_id = $user_id;
        $tt->gig_id = $id;
        $tt->created_at = $date;
        $tt->updated_at = $date;
        $tt->save(); 
        $updateCheckInStatus = GigUser::where(['user_id' => $user_id, 'gig_id' => $id])->first();
        $getBusinessId = Gig::where(['id'=>$id])->first(); 
        $getInfulancerName = $userdata->name;
            if($getInfulancerName){
            $name = $getInfulancerName;
            }else{
             $name = "NAME";   
            }
               
            $message = "Please Check-in " . $name;
            $notificationMessage = "Please Check-in";
            $insertNotification = Notification::insert(['user_id' => $getBusinessId->user_id, 'other_user_id' =>$user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$updateCheckInStatus->status,'notify_message'=>$notificationMessage,'post_id'=>$id]);
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
               'id' => $id
            );
            $message_new = array('sound' =>1,'message'=>$message,
                                'notifykey'=>'Check-in','id'=>$id);
            $updateCheckInStatus->status = 1;
            $updateCheckInStatus->is_checkin = 1;
            $updateCheckInStatus->update();

            $getBusinessUser = User::where(['id' => $getBusinessId->user_id])->first();
            // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
            // {
            //     if ($getBusinessUser->device_type == '2')
            //     {
            //         $this->send_android_notification($getBusinessUser->device_id,"Please Check-in",'Check-in', $message_new);
            //     }
            //     else
            //     {
            //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage ="Please Check-in", $notifyMessage);
            //     }
            // } 
            return redirect()->back()->with('message' , 'Check In request sent successfully');
    }

    function checkOut($id){
        $userdata = Auth::guard('business')->user();
        $user_id = $userdata->id;
        $time = time();
        $updateGigDetail = GigDetail::where(['user_id' => $user_id, 'gig_id' => $id])
        ->update(['time' => $time]);
    $updateGigUser = GigUser::where(['user_id' => $user_id, 'gig_id' => $id])->first();
        
    $getBusinessId = Gig::where(['id'=>$id])->first();
    $getInfulancerName = $userdata;
    if(!empty( $getInfulancerName->name)) {
        $name = $getInfulancerName->name;
    }else{
        $name = "dummy name";   
    }
       
    $message = $name . " checked out ";
    $notificationMessage = "checked out";
    $insertNotification = Notification::insert(['user_id' => $getBusinessId->user_id, 'other_user_id' =>$user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$updateGigUser->status,'notify_message'=>$notificationMessage,'post_id'=>$id]);

    $notifyMessage = array(
        'sound' => 1,
        'message' => $message,
        'id' => $id
    );

    
    $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'gig checked out','id'=>$id);
        // ->update(['status' => 4, 'updated_at' => date('Y-m-d H:i:s') ]);

        $getBusinessUser = User::where(['id' => $getBusinessId->user_id])
            ->first();
        // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id))
        // {
        //     if ($getBusinessUser->device_type == '2')
        //     {
        //         $this->send_android_notification($getBusinessUser->device_id,"checked out", 'gig checked out',$message_new);
        //     }
        //     else
        //     {
        //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage ="checked out", $notifyMessage);
        //     }
        // }


        /// gig complete notification to busienss
        $message = "Your Gig has completed successfully";
        $notificationMessage = "Your Gig has completed successfully";
        $insertNotification = Notification::insert(['user_id' => $getBusinessId->user_id, 'other_user_id' =>$user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s'),'type'=>'gig','old_status'=>$updateGigUser->status,'notify_message'=>$notificationMessage,'post_id'=>$id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $id
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'gig completed','id'=>$id);

        $getBusinessUser = User::where(['id' => $getBusinessId->user_id])->first();
        // if (!empty($getBusinessUser->device_type) && !empty($getBusinessUser->device_id)) {
        //     if ($getBusinessUser->device_type == '2') {
        //         $this->send_android_notification($getBusinessUser->device_id,"Your gig has completed", 'gig completed',$message_new);
        //     } else {
        //         $this->send_iphone_notification($getBusinessUser->device_id, $message, $notmessage ="Your Gig has completed successfully", $notifyMessage);
        //     }
        // }
        /// gig complete notification to infulancer
        $message = "Your Gig has completed successfully";
        $insertNotification = Notification::insert(['other_user_id' => $getBusinessId->user_id, 'user_id' =>$user_id, 'message' => $message, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s') ,'type'=>'gig','old_status'=>$updateGigUser->status,'notify_message'=>$notificationMessage,'post_id'=>$id ]);
        $notifyMessage = array(
            'sound' => 1,
            'message' => $message,
            'id' => $id
        );
        $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'Gig has completed','id'=>$id);
        $updateGigUser->status = 4;
        $updateGigUser->updated_at = date('Y-m-d H:i:s');
        $updateGigUser->update();


        $getInfulencerUser = User::where(['id' => $user_id])
            ->first();
        // if (!empty($getInfulencerUser->device_type) && !empty($getInfulencerUser->device_id)) {
        //     if ($getInfulencerUser->device_type == '2') {
        //         $this->send_android_notification($getInfulencerUser->device_id, "Your Gig has completed successfully",'Gig has completed', $message_new);
        //     } else {
        //         $this->send_iphone_notification($getInfulencerUser->device_id, $message, $notmessage ="Your Gig has completed successfully", $notifyMessage);
        //     }
        // }

        return redirect()->back()->with('message' , 'Twitter trend  stop successfully');

    }

    function influencersLogout(Request $request) {
        $user = Auth::guard('business')->user();
        Auth()->guard('business')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        Session::flash('success', 'Logged out successfully.');
        // return redirect(url('/business/login'))->withInput();
        return redirect(config('app.business_subdomain_url'))->withInput();
    }
}
