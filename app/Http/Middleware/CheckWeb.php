<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard='web')
    {
        if(!Auth()->guard('web')->check()){
            return redirect(route('/user.login'));
        }

        if(Auth()->guard('web')->check()){
            $user = auth()->guard('web')->user();
            if($user->is_deleted == 1 && $user->status == 2){
                return redirect(route('/user.login'));
            }
        }

        if(Auth()->guard('web')->check()){
            $user = auth()->guard('web')->user();
            if(empty($user)){
                return redirect(route('/user.login'));
            }
        }
        return $next($request);
    }
}
