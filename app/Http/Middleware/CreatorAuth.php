<?php

namespace App\Http\Middleware;

use Closure;

class CreatorAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()) {
            return redirect('user/login')->with('danger','Permission Denied!!! You do not have access. Please Login to Continue');
        }
        return $next($request);
    }
}
