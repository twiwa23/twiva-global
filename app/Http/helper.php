<?php

if(!function_exists('conversionIntoKformat')){
    function conversionIntoKformat($input){
        if($input >= 1000000){
            return number_format(($input/1000),2) . "M";
        }else if($input >= 1000){
           return number_format(($input / 1000),2) . "K";
        }else {
            return $input;
        }
    }
}
