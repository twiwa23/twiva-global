<?php

namespace App\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\User;
use App\model\PostPrice;
use App\model\PostUser;
use App\model\Admin;
use App\model\AdminPayment;
use App\model\Notification;
class SendInvitation implements ShouldQueue
{  
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $price = '';
    public $post_id = '';
    public $user_id = '';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($post_id ,  $price , $user_id)
    {
        $this->price = $price;
        $this->post_id = $post_id;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $price = $this->price;
        $user_id = $this->user_id;
        //$complementry_item = $request->complementry_item;
        $post_id = $this->post_id;
        $infulancer_id = User::select('id')->where(['user_type'=>'I','status'=>'1' , 'is_deleted'=>'0'])->get()->toArray();
        $date = date('Y-m-d H:i:s');
        //1 pending 2=>accept,3 reject
        $status = 1;
        $prices = $this->price; //explode(",", $price);
        //$complementryItem = explode(",", $complementry_item);
        $infulancerId = $infulancer_id;
        $infulancer_count = count($infulancerId);
        $getBusinessName = User::where('id', $user_id)->first();
        $name = $getBusinessName->name; 
        // print_r($infulancer_count);die;
        for ($i = 0;$i < $infulancer_count;$i++)
        {
            $set_post_price = [
                            'user_id' => $user_id,
                            // 'complementry_item' => @$complementryItem[$i],
                            'post_id' => $post_id,
                            'infulancer_id' => @$infulancerId[$i]['id'],
                            'price' =>$prices,
                            'status' => 1,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'created_at' => date('Y-m-d H:i:s')
                        ];
            PostPrice::insert($set_post_price);
            PostUser::insert([
                                'user_id' => @$infulancerId[$i]['id'],
                                'post_id' => $post_id,
                                'created_at' => date('Y-m-d H:i:s'), 
                                'updated_at' => date('Y-m-d H:i:s'),
                                'is_bid'=>'1',
                            ]);
            $getAdminId = Admin::first();
            $insertAdminAmount = AdminPayment::insert([
                                                    'user_id' => $user_id,
                                                    'infulencer_id' => @$infulancerId[$i]['id'],
                                                     'amount' => $prices,
                                                     'created_at' => date('Y-m-d H:i:s'),
                                                     'updated_at' => date('Y-m-d H:i:s'),
                                                     'post_id' => $post_id,
                                                     'type' => 'post'
                                                 ]);

            $post_price_id = $post_id;

            ///notification code is pending
            $offer_price_data = PostPrice::where(['post_id' => $post_price_id, 'infulancer_id' => $infulancerId[$i]['id']])->first();
            $message = $name . " invited you to a new Post";
            $notificationMessage =  "invited you to a new Post";
            $insertNotification = Notification::insert([
                                                'other_user_id' => $user_id,
                                                'user_id' => $infulancerId[$i]['id'],
                                                'message' => $message,
                                                'created_at' => $date,
                                                'updated_at' => $date,
                                                'type'=>'post',
                                                'old_status'=>0,
                                                'notify_message'=>$notificationMessage,
                                                'post_id'=>$post_id]);
            $notifyMessage = array(
                'sound' => 1,
                'message' => $message,
                'id' => $post_id
            );
            $message_new = array('sound' =>1,'message'=>$message,
                            'notifykey'=>'invited','id'=>$post_id);
            $getInfulancerUser = User::where(['id' => $infulancerId[$i]['id'], 'status' => '1', 'is_deleted' => '0'])->first();
              
            // if (!empty($getInfulancerUser->device_type) && !empty($getInfulancerUser->device_id))
            // {
            //     if ($getInfulancerUser->device_type == '2')
            //     {
            //         $device_token = $getInfulancerUser->device_id;
            //         $message = 'invited you to a new Post';
            //         $notmessage = "invited";
            //         $msgsender_id = $message_new;

            //         if (!defined('API_ACCESS_KEY'))
            //         {
            //           define('API_ACCESS_KEY','AAAAw_qk_lI:APA91bHb0bLFByFcj3gd8umTYG5QzOFLUVWkzXudN9xHAXl232plDra86kXHohP-vAq27SM1mYT4q-FuI4OQj135k1D6_XVjF6w3uHZaFJxvR4tQO-Zi725juBHnBMB_jDWOm4eC5VMy');
            //         }
            //         $registrationIds = array($device_token);
            //         $fields = array(
            //           'registration_ids' => $registrationIds,
            //           'alert' => $message,
            //           'sound' => 'default',
            //           'Notifykey' => $notmessage, 
            //           'data'=>$msgsender_id
                        
            //         );
            //         $headers = array(
            //           'Authorization: key=' . API_ACCESS_KEY,
            //           'Content-Type: application/json'
            //         );
                
            //         $ch = curl_init();
            //         curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            //         curl_setopt( $ch,CURLOPT_POST, true );
            //         curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            //         curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            //         curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            //         curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($fields) );
            //         $result = curl_exec($ch);
                
            //         if($result == FALSE) {
            //           die('Curl failed: ' . curl_error($ch));
            //         }
                
            //         curl_close( $ch );
            //         //return  $result;
                    
            //     }
            // }

        }  
    }
    public $tries = 3;
}
