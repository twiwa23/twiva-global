<?php

namespace App\model;
use App\model\User;
use Illuminate\Database\Eloquent\Model;
class AdminPayment extends Model
{
   protected $table = "admin_payments";

    protected $fillable = ["user_id",'infulencer_id',"amount","post_id","type","status"];

    
     public function user(){
        return $this->belongsTo(User::class);
    }
}
