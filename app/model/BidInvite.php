<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class BidInvite extends Model
{
    protected $fillable = ['business_id' , 'post_id' , 'price' , 'influencers_id'];
}
