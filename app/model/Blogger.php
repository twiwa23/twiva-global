<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Blogger extends Model
{
   protected $table = "bloggers";

    protected $fillable = ['user_id',"description"];
}
