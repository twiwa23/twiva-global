<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class BusinessDetail extends Model
{
    protected $table = "business_details";

    protected $fillable = ["business_detail",'user_id',"owner_name"];
     
}
