<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class CategoryUser extends Model
{
    protected $table = "category_users";

    protected $fillable = ["user_id","category_id"];

     public function Category(){
        return $this->hasOne("App\model\Category","id");
    }
}
