<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Featured extends Model
{
    protected $table = "featureds";

    protected $fillable = ["text",'title',"image"];

     public function getImageAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

     
}