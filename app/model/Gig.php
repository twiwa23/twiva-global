<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Gig extends Model
{ 
    protected $primaryKey  =    'id';
    protected $table       =    'gigs';
    
    protected $fillable = [
        'gig_name', 'gig_start_date_time', 'gig_end_date_time','price_per_gig','description','what_to_do','user_id','created_at','updated_at','location','gig_complementry_item','profile','owner_name','phone_number','status','hashtag','venue'
    ];
  
      public function getprofileAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('public/storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

   public function Images(){
        return $this->hasMany("App\model\GigMedia");
    }
    public function user(){
        return $this->belongsTo("App\model\User");
    }

     public function price(){
        return $this->hasMany("App\model\GigPrice")->where('user_id',Auth::id());
    }
    public function gigInfluencerUser(){
      return $this->hasMany("App\model\GigUser","gig_id", 'id');
    }
    public function gigUser(){
        $user_id = Auth::id();
        if(!$user_id){
          $user_id = Auth::guard("business")->id();
        }
        return $this->hasMany("App\model\GigUser","gig_id")
                ->with('user')
                // ->select(DB::raw(" distinct gig_users.id "),"gig_users.*","gig_prices.price",DB::raw("IF((select created_at FROM `infulancer_ratings` where type = 'gig' and user_id = ".$user_id." and post_id = gig_users.gig_id and infulencer_id = gig_prices.infulancer_id),1,0) as is_rated"))
                ->select(DB::raw(" distinct gig_users.id "),"gig_users.*","gig_prices.price","gig_prices.infulancer_id as influence","gig_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'gig' and user_id = ".$user_id." and post_id = gig_users.gig_id and infulencer_id = gig_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))->groupBy("gig_users.id")
                ->join("gig_prices","gig_users.gig_id","=","gig_prices.gig_id")
                ->groupBy("gig_users.id");
    }

    public function gigDetail(){
      return $this->hasMany("App\model\GigDetail");
    }


    public function gigUserCompleted(){
      $user_id = Auth::id();
      return $this->hasMany("App\model\GigUser","gig_id")
                ->with(['user'])
                ->select("gig_users.*","gig_prices.price",DB::raw("IF((select created_at FROM `infulancer_ratings` where type = 'gig' and user_id = ".$user_id." and post_id = gig_users.gig_id and infulencer_id = gig_prices.infulancer_id),1,0) as is_rated"))
               ->join("gig_prices","gig_users.gig_id","=","gig_prices.gig_id");
    }

    public function gigUserComplete(){
      return $this->hasMany("App\model\GigUser","gig_id")
                ->with(['user'])
                ->select("gig_users.*","gig_prices.price")
                 ->whereIn('gig_users.status', [1, 3, 4])
               ->join("gig_prices","gig_users.gig_id","=","gig_prices.gig_id");
    }
}

