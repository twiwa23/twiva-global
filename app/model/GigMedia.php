<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class GigMedia extends Model
{
    protected $table = "gig_media";

    protected $fillable = ["gig_id","media"];
    
     public function getMediaAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business_images/gig_images');
        $read_path   = url('storage/uploads/images/business_images/gig_images');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

    public function getThumbnailAttribute($value)
    {
        $profile = $value;

        $check_path = public_path('storage/uploads/images/business_images/gig_images');
        $read_path   = url('public/storage/uploads/images/business_images/gig_images');
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }
    
}
