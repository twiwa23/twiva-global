<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
class GigUser extends Model
{
   protected $fillable = [
        'user_id', 'gig_id', 'status','is_resend','is_checkin','created_at','updated_at','reject_amount'
    ];
	public function Gig(){
        return $this->belongsTo("App\model\Gig")->with("Images","user");
    }
	 public function Images(){
        return $this->belongsTo("App\model\GigMedia");
    }
	
    public function user(){
        return $this->belongsTo("App\model\User")->with('Images');
    }
    public function influencer_social_details(){
        return $this->hasOne("App\model\InfluencerSocialDetails", "user_id", "user_id");
    }
    public function InfluencerDetailNew(){
        return $this->hasOne("App\model\InfluencerDetailNew", "influencer_id", 'user_id');
    }
}
