<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerDetail extends Model
{
    protected $table = "influencer_details";

    protected $fillable = ["dob","gender","user_id","about"];

}

