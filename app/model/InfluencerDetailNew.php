<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerDetailNew extends Model
{
    protected $table = "influencer_detail_new";

    protected $fillable = ['influencer_id', 'name', 'dob', 'gender', 'description', 'interests', 'profile_image', 'youtube_id', 'instagram_id', 'facebook_id', 'twitter_id', 'is_shop_listed'];

}

