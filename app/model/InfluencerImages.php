<?php 

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerImages extends Model
{
    protected $table = "influencer_images";
    protected $fillable = ["user_id","image"];    

    protected $hidden = ["user_id"];

    public function getImageAttribute($value){
        $profile = $value;
        $check_pathI = public_path('storage/uploads/images/influencer_images');
        $read_pathI   = url('storage/uploads/images/influencer_images');
        $dummy_profile   = url('public/images/dummy_user.jpg');

        if(file_exists($check_pathI.'/'.$profile) && !empty($profile)){
            return $read_pathI.'/'.$profile;
        }else {
            return $dummy_profile;
        }
    }
}

?>
