<?php 

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerPrice extends Model
{
    protected $table = "influencer_price";

    protected $fillable = ["user_id","plug_price","gig_price"];    
}

?>
