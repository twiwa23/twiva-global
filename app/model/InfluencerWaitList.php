<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerWaitList extends Model
{
    protected $fillable = ["creator_id","post_gig_id","user_data","type","is_accepted"];
}
