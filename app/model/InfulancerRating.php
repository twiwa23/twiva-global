<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfulancerRating extends Model
{
    //

     protected $fillable = [
        'user_id', 'rating', 'comment','infulencer_id'
    ];

    public function user(){
    	return $this->belongsTo("App\model\User")->with('Images');
    }
}
