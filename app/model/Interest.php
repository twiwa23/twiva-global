<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $table = "interests";

    protected $fillable = ["user_id","added_by","image","interest_name"];

    protected $hidden = ["user_id","added_by","laravel_through_key"];

  
   
     public function getImageAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }
       
}
