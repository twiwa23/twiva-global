<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class MpesaPhoneNumber extends Model
{
     protected $fillable = [
        'user_id', 'phone_number'
    ];

}
