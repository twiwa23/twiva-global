<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'user_id', 'other_user_id', 'status','message','post_id','type','notify_message'
    ];
     public function user(){
        return $this->belongsTo("App\model\User")->with("Images");
    }

    public function otherUser(){
        return $this->belongsTo("App\model\User",'other_user_id')->with("Images");
    }
}
