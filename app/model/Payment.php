<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ["user_id",'infulencer_id',"amount","post_id","type"];

    public function user(){
        return $this->belongsTo("App\model\User");
    }

}
