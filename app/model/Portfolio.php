<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = "porfolios";

    protected $fillable = ['user_id',"profile","type","thumbnail_name"];

     public function getProfileAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

    public function getThumbnailNameAttribute($value)
    {
        $profile = $value;
        $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

}
