<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Post extends Model
{
    protected $fillable = [
        'post_name', 'price_per_post','description','what_not_to_include','user_id','post_type','what_to_include','status','hashtag','other_requirement','expiration_date'
    ];
   
    public function Images(){
        return $this->hasMany("App\model\PostImage");
    }

    public function postDetail(){
        return $this->hasMany("App\model\PostDetail")->with('media');
    }

    public function user(){
    	return $this->belongsTo("App\model\User");
    }
    public function price(){
        return $this->hasMany("App\model\PostPrice")->where('user_id',Auth::id());
    } 

     public function updatePrice(){
        return $this->hasMany("App\model\PostPrice","post_id");
           
        } 

    public function postUserComplete(){
        return $this->hasMany("App\model\PostUser","post_id")
                ->with(['postDetail','user'])
                ->select("post_users.*","post_prices.price","post_prices.complementry_item")
                ->whereIn('post_users.status', [1, 3, 4, 5])
                ->join("post_prices","post_users.post_id","=","post_prices.post_id");
    }

    public function postUserCompleted(){
        $user_id = Auth::id();
        if(!$user_id) {
            $user_id = Auth::guard("business")->id();
        }
        return $this->hasMany("App\model\PostUser","post_id")
                ->with(['postDetail','user']) 
                ->select(DB::raw(" distinct post_users.id "),"post_users.*","post_prices.price","post_prices.infulancer_id as influence","post_prices.complementry_item",DB::raw("(CASE WHEN (select id FROM `infulancer_ratings` where type = 'post' and user_id = ".$user_id." and post_id = post_users.post_id and infulencer_id = post_users.user_id) > 0 THEN 1 ELSE 0 END) as is_rated"))->groupBy("post_users.id")
                ->join("post_prices","post_users.post_id","=","post_prices.post_id");      
    }
    
}
