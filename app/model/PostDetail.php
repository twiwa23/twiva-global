<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PostDetail extends Model
{
    protected $fillable = [
        'user_id', 'post_id','text_one','text_two','text_three','post_user_id'
    ];

    public function media(){
        return $this->hasMany("App\model\PostDetailsMedia",'post_detail_id');
    }

    public function user(){
    	return $this->belongsTo("App\model\User")->with(['Images']);
    }
}
