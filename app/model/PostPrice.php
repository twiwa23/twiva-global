<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
class PostPrice extends Model
{
   protected $fillable = [
        'user_id', 'post_id', 'price','status','is_resend'
    ];
}
