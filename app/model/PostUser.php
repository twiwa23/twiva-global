<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
class PostUser extends Model{

  protected $fillable = [
        'user_id', 'post_id', 'status','is_resend','reject_post_reason','reason','is_submit','reject_amount'
    ];
	
    public function Post(){
        return $this->belongsTo("App\model\Post")->with("Images","user","postDetail");
    }

	public function Images(){
        return $this->belongsTo("App\model\PostImage");
    }

    public function user(){
    	return $this->belongsTo("App\model\User")->with(['Images']);
    }

    public function postDetail(){
        return $this->hasOne("App\model\PostDetail",'post_user_id')
                 ->with("media");
    }

    public function Price(){
        return $this->hasMany("App\model\PostPrice","post_id");
           
        }
    

}
