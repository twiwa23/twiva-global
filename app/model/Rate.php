<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
   protected $table = "rates";

    protected $fillable = ['rate',"text",'title',"user_id"];

}
