<?php

namespace App\model;


use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'user_id', 'rating', 'comment','business_id','type','post_id'
    ];

    public function user(){
    	return $this->belongsTo("App\model\User")->with('Images');
    }

}
