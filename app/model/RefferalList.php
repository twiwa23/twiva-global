<?php

namespace App\model;
use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;

class RefferalList extends Model
{
    protected $table = "refferal_codes";
	 protected $fillable = [
        'user_id', 'other_user_id','refferal_code'
    ];
	
	public function User(){
        return $this->belongsTo("App\model\User",'user_id');
    }
	
}
