<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
   protected $table = "services";

    protected $fillable = ["text",'title',"profile","user_id"];

     public function getProfileAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }
}
