<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SubadminPermission extends Model
{
    protected $table = "subadmin_permissions";

    protected $fillable = ["user_id","","sidebar_id","status","add","edit","view","delete"];

    
}
