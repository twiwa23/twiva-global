<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class TempTokens extends Model
{
    protected $table = 'temp_tokens';

    protected $fillable = ['id', 'user_id', 'type', 'access_token', 'data' ];
}
