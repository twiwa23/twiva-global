<?php

namespace App\model;
use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
     protected $table = "timelines";
	  protected $fillable = [
        'user_id', 'timeline_text'
    ];
	 public function timelineImages(){
        return $this->hasMany("App\model\TimelineImages");
    }
	
	public function User(){
        return $this->belongsTo("App\model\User",'user_id')->with("Images");
    }
	public function likes(){
        return $this->hasOne("App\model\TimelineLikes","timeline_id")->where('user_id',Auth::id());
    }
}
