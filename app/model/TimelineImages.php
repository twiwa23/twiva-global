<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class TimelineImages extends Model
{
    protected $table = "timeline_images";
	 protected $fillable = [
        'timeline_id', 'media'
    ];
	public function getMediaAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business_images/timeline_images');
        $read_path   = url('public/storage/uploads/images/business_images/timeline_images');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

    

    public function timeline(){
        return $this->belongsTo("App\model\Timeline");
    }
}
