<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class TimelineLikes extends Model
{
    protected $table = "timeline_likes";
}
