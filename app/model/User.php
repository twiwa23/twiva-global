<?php

namespace App\model;

use Illuminate\Support\Str;
use App\model\BusinessDetail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type','profile','country_code','phone_number','contact_person','facebook_id','instagram_id','twitter_id','status','is_deleted','country_code',"device_id","device_type","refferal_code","email_verify","emailStatus","background_image","discover_twiva","other"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password','is_deleted','status','user_type'
    ];

    protected $appends = ['businessDetail', 'companyDetail'];

    protected $guarded = ['businessDetail', 'companyDetail'];

    public function getProfileAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
         $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

    public function getBackgroundImageAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
         $read_path   = url('storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

    public function getBusinessDetailAttribute(){
        if($this->user_type == 'B'){
            return BusinessDetail::whereUserId($this->id)->select("business_detail","owner_name")->first();
        }
    }

    public function getCompanyDetailAttribute(){
        if($this->user_type == 'B' || $this->type == 1){
            return Company::where('user_id', $this->id)->select("busi_name", "logo")->first();
        }
    }

    public function Images(){
        return $this->hasMany("App\model\InfluencerImages");
    }

    public function Interests(){
       return $this->hasManyThrough("App\model\Interest","App\model\InfluencerInterests","user_id","id","id","interest_id");
    }

    public function SocialDetail(){
        return $this->hasOne("App\model\InfluencerSocialDetails");
    }

    public function price(){
        return $this->hasOne("App\model\InfluencerPrice");
    }

    // public function InfluencerDetail(){
    //     return $this->hasOne("App\model\InfluencerDetail");
    // }
    public function InfluencerDetail(){
        return $this->hasOne("App\model\InfluencerDetailNew", "influencer_id", 'id');
    }

    public function InfluencerDetailNew(){
        return $this->hasOne("App\model\InfluencerDetailNew", "influencer_id", 'id');
    }

    /*Johny 21/11/2019 for post detail after influencer accept the post request*/
    public function postDetail(){
        return $this->hasOne("App\model\PostDetail",'user_id');
    }

    public function infulancerRating(){
        return $this->hasMany("App\model\InfulancerRating","infulencer_id");
    }

     public function CategoryUser(){
        return $this->hasOne("App\model\CategoryUser")->with("Category");
    }
    // public function avgRating(){
    //     return $this->infulancerRating->avg('rating');
    // }



    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */


    public function generateToken()
    {
        $this->api_token = Str::random(60);
        $this->save();

        return $this->api_token;
    }
}
