<?php

namespace App\model;
use Auth;
use DB;

use Illuminate\Database\Eloquent\Model;

class UserPost extends Model
{
     protected $table = "user_posts";
	  protected $fillable = ['user_id', 'text','profile','title'];
	public function getProfileAttribute($value)
    {
         $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('public/storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }
}
