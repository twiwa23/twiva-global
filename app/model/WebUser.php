<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use  Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class WebUser extends Authenticatable
{
    use HasApiTokens, Notifiable;

    //protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type','is_deleted','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
     public function getImageAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('public/storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }
}