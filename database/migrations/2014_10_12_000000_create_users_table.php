<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name",100)->nullable();
            $table->string("email",100)->nullable()->unique();
            $table->enum("user_type",['I','B'])->default('I')->comment("I => Influencer B => Business");
            
            $table->string("password")->nullable();

            $table->string("profile")->nullable();
            $table->string("country_code",20)->nullable();
            $table->bigInteger("phone_number")->nullable();
            $table->string("contact_person",100)->nullable();

            $table->string("facebook_id",100)->nullable();
            $table->string("instagram_id",100)->nullable();
            $table->string("twitter_id",100)->nullable();
            $table->string("device_id")->nullable();
            $table->string("device_type",30)->nullable();
             $table->string("reset_password_token")->nullable();
            $table->enum("status",['0','1','2'])->default('1')->comment("0 => Inactive 1 => active 2 => block");

            $table->enum("is_deleted",['0','1'])->default('0')->comment("0 => active 1 => deleted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
