<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencerSocialDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_social_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->index("user_id");

            $table->string("facebook_name",100)->nullable();
            $table->unsignedBigInteger("facebook_friends")->nullable();
            $table->string("twitter_name",100)->nullable();
            $table->unsignedBigInteger("twitter_friends")->nullable();
            $table->string("instagram_name",100)->nullable();
            $table->unsignedBigInteger("instagram_friends")->nullable();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_social_details');
    }
}
