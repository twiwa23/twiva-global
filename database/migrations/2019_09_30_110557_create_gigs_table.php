<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('gigs', function (Blueprint $table) {
            $table->bigIncrements('id');
			 $table->unsignedBigInteger('user_id');
            $table->index("user_id");
			$table->text("gig_name");
            $table->string("gig_start_date_time");  
            $table->string("gig_end_date_time");
            $table->string('price_per_gig');
            $table->longText('gig_complementry_item');
            $table->string('profile');
            $table->string('owner_name');
            $table->string('phone_number');
			$table->string('location');
            $table->longText('description');
            $table->longText('what_to_do');
            $table->longText('hashtag');
            $table->string('status');
            $table->timestamps();
			
			$table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gigs');
    }
}
