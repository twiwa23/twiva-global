<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gig_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('gig_id');
            $table->index("gig_id");
			$table->text("media");
            $table->string("thumbnail")->nullable();  
        
            $table->timestamps();
			
			$table->foreign('gig_id')
                    ->references('id')
                    ->on('gigs')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gig_media');
    }
}
