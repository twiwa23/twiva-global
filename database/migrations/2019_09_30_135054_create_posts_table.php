<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->index("user_id");

            /* insert new changes here*/
            $table->text("post_name");
            $table->string("post_type")->nullable();
            $table->string("price_per_post");
            $table->longText('description');
            $table->longText('what_to_include');
            $table->longText('what_not_to_include');
            $table->longText('hashtag');
            $table->string("status");
           
            
            /* end here*/

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
