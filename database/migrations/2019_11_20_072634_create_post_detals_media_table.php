<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostDetalsMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_detals_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_detail_id');
            $table->index("post_detail_id");
            $table->foreign('post_detail_id')
                    ->references('id')
                    ->on('post_details')
                    ->onDelete('cascade');
            $table->string("media")->nullable(); 
             $table->integer("user_id")->nullable(); 
            $table->string("thumbnail")->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_detals_media');
    }
}
