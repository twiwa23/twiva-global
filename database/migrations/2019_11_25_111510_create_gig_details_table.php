<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gig_details', function (Blueprint $table) {
                  $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->index("user_id");
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->unsignedBigInteger('gig_id');
            $table->index("gig_id");
            $table->foreign('gig_id')
                    ->references('id')
                    ->on('gigs')
                    ->onDelete('cascade');
            $table->string("time")->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gig_details');
    }
}
