<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigMediaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gig_media_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('gig_detail_id');
            $table->index("gig_detail_id");
            $table->foreign('gig_detail_id')
                    ->references('id')
                    ->on('gig_details')
                    ->onDelete('cascade');
            $table->string("media")->nullable(); 
               $table->integer("user_id")->nullable(); 
            $table->string("thumbnail")->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gig_media_details');
    }
}
