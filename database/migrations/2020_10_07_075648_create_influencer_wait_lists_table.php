<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencerWaitListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_wait_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("creator_id");
            $table->integer("post_gig_id");
            $table->text("user_data")->nullable();
            $table->enum("type",['post','gig'])->nullable();
            $table->enum("is_accepted",['0','1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_wait_lists');
    }
}
