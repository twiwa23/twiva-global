<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    	/* insert interests*/
    	DB::statement("INSERT INTO `interests` (`user_id`, `added_by`, `image`, `interest_name`, `created_at`, `updated_at`) VALUES
				(1, 'A', 'interest-pic1.png', 'fashion', '2019-09-23 18:30:00', '2019-09-23 18:30:00'),
				(1, 'A', 'interest-pic2.png', 'food', '2019-09-23 18:30:00', '2019-09-23 18:30:00'),
				(1, 'A', 'interest-pic3.png', 'health', '2019-09-23 18:30:00', '2019-09-23 18:30:00'),
				(1, 'A', 'interest-pic4.png', 'life style', '2019-09-23 18:30:00', '2019-09-23 18:30:00'),
				(1, 'A', 'interest-pic5.png', 'luxury', '2019-09-23 18:30:00', '2019-09-23 18:30:00'),
				(1, 'A', 'interest-pic6.png', 'night life', '2019-09-23 18:30:00', '2019-09-23 18:30:00')"
			);
    }
}
