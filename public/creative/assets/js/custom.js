$(document).ready(function(){
    $('.navbar-toggler').click(function(){
        $('#collapsibleNavbar').toggle();
    });

    
    $('.main-tab-content .nav-link').click(function(){
        $('.main-tab-content .nav-link').removeClass('active');
        $(this).addClass('active');
    });


    $('.accordion .card').click(function(){
        if(!$(this).find('.collapse').hasClass('show')){
            $('.accordion .card .card-header').removeClass('active');
            $(this).find('.card-header').addClass('active');
        }else{
            $(this).find('.card-header').removeClass('active');
        }
    });


    // Multiple Images Upload //
    if (window.File && window.FileList && window.FileReader) {
        $("#files").on("change", function(e) {
          var files = e.target.files,
            filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function(e) {
                    var file = e.target;
                    $("<div class=\"image-container\">" +
                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                        "<br/><span class=\"remove\">Delete</span>" +
                        "</div>").insertBefore(".upload-field");
                    $(".remove").click(function(){
                        $(this).parent(".image-container").remove();
                    });         
                });
                fileReader.readAsDataURL(f);
            }
        });
    } 
    else {
    alert("Your browser doesn't support to File API")
    }


    // Profile Image Upload //
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image')
                    .attr('src', e.target.result)
                    .width("100%")
                    .height("100%");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".uploader").change(function() {
        readURL(this);
    });

    // Select Images //
    $(".image-container img").each(function () {
        $(this).click(function () {
            $(".popup").show();
        });
    });

});

// Image Zoom Modal Start //

    // Open the Modal
    function openModal() {
        document.getElementById("imgModal").style.display = "block";
    }
    
    // Close the Modal
    function closeModal(){
        document.getElementById("imgModal").style.display = "none";
    }
    
    var slideIndex = 1;
    showSlides(slideIndex);
    
    // Next/previous controls
    function plusSlides(n) {
        showSlides((slideIndex += n));
    }
    
    // Thumbnail image controls
    function currentSlide(n) {
        showSlides((slideIndex = n));
    }
    
    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        if (n > slides.length) {
        slideIndex = 1;
        }
        if (n < 1) {
        slideIndex = slides.length;
        }
        for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
    }
    
// Image Zoom Modal End //






