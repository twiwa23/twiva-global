//// Left Menu ////
$(function () {
    var url = window.location;
    $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
    $('#sidebar-menu a').filter(function () {
        return this.href == url;
    }).parent('li').addClass('current-page').parent('ul').parent().addClass('active');

    if ($('.sidebar-submenu ').hasClass('active')){
        $(".sidebar-submenu.active").parent('li').addClass('current-page');
        $(".sidebar-submenu.active").parent('li').find("> a").addClass("current-page");
        $(".sidebar-submenu.active").parent('li').find("> .current-page .fa-chevron-down").addClass("rotate-active");
    }
});

$(document).ready(function(){
    // Sidebar Dropdown Menu //
    $(".sidebar-dropdown > a").on("click", function () {
        $(this).parent().toggleClass("current-page");
        $(this).siblings().toggleClass('active');
        $(this).find(".fa-chevron-down").toggleClass("rotate-active");
    });
});
//// Left Menu ////

$(document).ready(function () {
    $(".navbar-toggler").on('click',function () {
        $(".left_col").toggle();
        $("#sidebar-menu").toggleClass("show-sidebar");
        $(".backdrop").show();
    });

    $(".backdrop").on("click",function () {
        $(this).hide();
        $(".left_col").toggle();
        $("#sidebar-menu").toggleClass("show-sidebar");
    });

// Custom Select Button //    
 $(".drop-btn").on("click", function () {        
      if ($(this).find(".select-btn").hasClass("open")) {             
          $(this).find(".select-btn").removeClass("open");         
        } else {             
            $(this).find(".select-btn").addClass("open");        
         }     
        });     
        $(".btn-option").each(function () {        
             $(this).on("click", function () { 
                var parent = $(this).parent(".btn-options").parent(".select-btn");            
                var option_value = $(this).data("value");             
                var option_id = $(this).data("id");             
                parent.find(".btn__trigger").text(option_value);             
                // console.log(option_id);
                $("#price").val(option_id);         
            });     
        });    
  // Custom Select Button //

    // Sidebar Dropdown Menu //
    // $(".sidebar-dropdown").on("click", function () {
    //     $(this).find(".sidebar-submenu").toggleClass("active");
    //     $(this).find(".fa-chevron-down").toggleClass("rotate-active");
    //     if ($(this).find(".sidebar-submenu").hasClass("active")) {
    //     $(this).find(".fa-chevron-down").addClass("rotate-active");
    //     }
    // });

    // Select Images //
    $(".img-list-item .img-list").each(function () {
        $(this).on("click",function () {
        $(this).parent(".img-list-item").find(".selected").css("opacity", "1");
        $(this).addClass("red-border");
        $(this).parent(".img-list-item").find(".select").css("display", "none");
        });
    });

    // Post Steps //
    var index = 0;
    var total = $(".invite-list-item").length;
    var totalTabs = $(".invite-list-content").length;
    $("#next-step").on("click", function () {
        $(".invite-list-item").removeClass("active");
        $(".invite-list-content").removeClass("active");
        $(".invite-list-item").eq(index).addClass("active");
        $(".invite-list-content").eq(index).addClass("active");
        index = (index + 1) % total;

        if ($(".invite-list-content:nth-child(4)").hasClass("active")) {
        $("#next-step").hide();
        $("#create").show();
        }
    });

    // Image Upload //
    var abc = 0; // Declaring and defining global increment variable.
    $(document).ready(function () {
        //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
        $("#add_more").click(function () {
        $(this).before(
            $("<label/>", {
            id: "filediv",
            class: "file-upload",
            })
            .fadeIn("slow")
            .append(
                $("<input/>", {
                name: "file[]",
                type: "file",
                id: "file",
                }),
                $("<br/><br/>")
            )
        );
        });
        // Following function will executes on change event of file input to select different file.
        $("body").on("change", "#file", function () {
        if (this.files && this.files[0]) {
            abc += 1; // Incrementing global variable by 1.
            var z = abc - 1;
            var x = $(this)
            .parent()
            .find("#previewimg" + z)
            .remove();
            $(this).before(
            "<div id='abcd" +
                abc +
                "' class='abcd'><img id='previewimg" +
                abc +
                "' src=''/></div>"
            );
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();
            $("#abcd" + abc).append(
            $("<img/>", {
                id: "img",
                src: "./assets/images/icons/cross.png",
                alt: "delete",
            }).click(function () {
                $(this).parent().remove();
            })
            );
        }
        });
        // To Preview Image
        function imageIsLoaded(e) {
        $("#previewimg" + abc).attr("src", e.target.result);
        $("#previewimg" + abc).attr("class", "previewimg");
        }
        $("#upload").on("click",function (e) {
        var name = $(":file").val();
        if (!name) {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
        });
    });
    //   Copy;

    $(".quantity").each(function () {
        var spinner = $(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find(".quantity-up"),
        btnDown = spinner.find(".quantity-down"),
        min = input.attr("min"),
        max = input.attr("max");

        btnUp.on("click",function () {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
        });

        btnDown.on("click",function () {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
        });
    });

    $(".am").on("click",function () {
        $(this).addClass("active");
        $(".pm").removeClass("active");
    });

    $(".pm").on("click",function () {
        $(this).addClass("active");
        $(".am").removeClass("active");
    });

// Multiple Images Upload //
var storedFiles = [];
if (window.File && window.FileList && window.FileReader) {
    
    $(".files").on("change", function (e) {
        var files = e.target.files,
        filesLength = files.length;
        storedFiles.push(files);

        if(storedFiles.length == 3){
            $(".upload-field").hide();
        }
        for (var i = 0; i < filesLength; i++) {
            var f = files[i];
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
            var file = e.target;
            $(
                '<div class="image-container mb-3">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<input type="file" name="media[]"  class="files"/ value="">' +
                "<br/><span class=\"remove\"><img src='./assets/images/icons/x.svg'/></span>" +
                "</div>"
            ).insertBefore(".upload-field");
            
            $(".remove").on("click",function () {
                $(this).parent(".image-container").remove();
                storedFiles.pop(files);
                if(storedFiles.length < 3){
                    $(".upload-field").show();
                }
                console.log(storedFiles.length);
            });
            };
            fileReader.readAsDataURL(f);
        }
        console.log(storedFiles.length);
    });
    
} 
else {
    alert("Your browser doesn't support to File API");
}

    // Dropdown Arrow Button //
    $('.dropdown-arrow').on('click',function(){
        $(this).find('img').toggleClass('rotate-active');
    });




});

// Image Zoom Modal Start //

// Open the Modal
function openModal() {
    document.getElementById("imgModal").style.display = "block";
}

// Close the Modal
function closeModal() {
    document.getElementById("imgModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
    showSlides((slideIndex += n));
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides((slideIndex = n));
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    captionText.innerHTML = dots[slideIndex - 1].alt;
}



// Image Zoom Modal End //


function image_1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
        var size=(input.files[0].size);
        if(size < 2000000) {
            $("#image_error").hide();
            $('#img1').attr('src', e.target.result);
            $('.image-delete1').addClass('d-flex');
        }else{
            $("#image_error").show();
            $("#image_error").html('Image size should be less than 2MB');
        }
        };
        reader.readAsDataURL(input.files[0]);
        $("#user").addClass("hide");
        $("#img1").removeClass("hide");

    }
}

function image_2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
        var size=(input.files[0].size);
        if(size < 2000000) {
            $("#image_error").hide();
            $('#img2').attr('src', e.target.result);
            $('.image-delete2').addClass('d-flex');
        }else{
            $("#image_error").show();
            $("#image_error").html('Image size should be less than 2MB');
        }
        };
        reader.readAsDataURL(input.files[0]);
        $("#user").addClass("hide");
        $("#img2").removeClass("hide");

    }
}

function image_3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
        var size=(input.files[0].size);
        if(size < 2000000) {
            $("#image_error").hide();
            $('#img3').attr('src', e.target.result);
            $('.image-delete3').addClass('d-flex');
        }else{
            $("#image_error").show();
            $("#image_error").html('Image size should be less than 2MB');
        }
        };
        reader.readAsDataURL(input.files[0]);
        $("#user").addClass("hide");
        $("#img3").removeClass("hide");

    }
}

function image_4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
        var size=(input.files[0].size);
        if(size < 2000000) {
            $("#image_error").hide();
            $('#img4').attr('src', e.target.result);
            $('.image-delete4').addClass('d-flex');
        }else{
            $("#image_error").show();
            $("#image_error").html('Image size should be less than 2MB');
        }
        };
        reader.readAsDataURL(input.files[0]);
        $("#user").addClass("hide");
        $("#img4").removeClass("hide");

    }
}