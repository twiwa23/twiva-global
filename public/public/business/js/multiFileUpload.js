  

	/* course images/video*/
  var file_Types = {
                   "image" : ["jpeg","jpg","png"],
                   "video" : ["mp4","avi","3gp","flv","mov","video"]
                };

    let util = {
      maxVideo : 5,
      maxImages : 5,
      maxVideoLen : 20971520, // 20971520 /* 20MB */
      maxImagesLen : 4194304, // 4194304 /*4MB*/
      maxVideoDuration : 120,
      totalMedia : 5,
      maxVideoLenText : ((20971520 / 1024) / 1024) + "MB",
      maxImageLenText : ((4194304 / 1024) / 1024) + "MB"
    }

    $(document).on("click",".images_placehold",function(){

      $("#video_image_error").text("").hide();
      let recursion = $(this).attr("data-recursion");
      recursion++;
      $(this).attr("data-recursion",recursion)

      $(".media_inputs .img_video").append(`<input type="file" name="images_videos[${recursion}][]" class="upload_image" multiple accept="video/*,image/*" />`)

      $(this).parent().find("input[name='images_videos["+recursion+"][]']").click();

      $(document).on("change","input[name='images_videos["+recursion+"][]']",function(event){
      var _this = $(this);
      let _cls = $(this).attr("name");
      _cls = _cls.replace("images_videos",'');
      _cls = _cls.replace("[",'');
      _cls = _cls.replace("]",'');
      _cls = _cls.replace("[]",'');
      let files = event.target.files;
      var has_vid_error = 0;
      var has_img_error = 0;
      var has_error = 0;
      var valid_images = 0;
      var valid_video = 0;
      var video_len_error = image_len_error = 0;
      var zero_len = 0;
      var new_total_img = new_total_vid = 0;

      let valid_video_images = files.length;
      for(let i = 0; i < files.length;i++){
        let spl = files[i].name.split(".");

        let file_len = files[i].size;

        if(file_len == 0){
          zero_len++;
        }

          let get_type = spl[spl.length-1];
          get_type = get_type.toLowerCase();
          if(file_Types.image.indexOf(get_type) == -1 && file_Types.video.indexOf(get_type) == -1){
             has_error++;
          }

              if(file_Types.image.indexOf(get_type) != -1){

                  if(file_len > util.maxImagesLen){
                    image_len_error++;
                  }


                  let u_images = $(".ext_media_record").attr("total-media");

                  if(!localStorage.getItem("o_img")){
                    localStorage.setItem("o_img",u_images);
                  }

                  u_images = parseInt(u_images)
                  u_images = u_images + 1
                  valid_images++;
                  new_total_img++;

                  $(".ext_media_record").attr("total-media",u_images)
                }else if(file_Types.video.indexOf(get_type) != -1) {
                  if(file_len > util.maxVideoLen){
                    video_len_error++;
                  }

                  let u_video = $(".ext_media_record").attr("total-media");
                  if(!localStorage.getItem("o_vid")){
                    localStorage.setItem("o_vid",u_video);
                  }
                  u_video = parseInt(u_video)
                  u_video = u_video + 1
                  valid_video++;
                  new_total_vid++;
                  $(".ext_media_record").attr("total-media",u_video)
                }else {
                  has_error++;
                }
            }

            let o_img = localStorage.getItem("o_img");

            if(o_img){
              localStorage.removeItem("o_img")
            }

            let o_vid = localStorage.getItem("o_vid");
            if(o_vid){
              localStorage.removeItem("o_vid")
            }

            if(has_error > 0){
              if(o_img){
                o_img = parseInt(o_img);
                let uploaded_images_up = $(".ext_media_record").attr("total-media");
                uploaded_images_up = parseInt(uploaded_images_up)
                console.log("new ",uploaded_images_up,"old",o_img)
                if(uploaded_images_up >= o_img){

                  uploaded_images_up = uploaded_images_up - new_total_img;
                  $(".ext_media_record").attr("total-media",uploaded_images_up)
                }
              }
              if(o_vid){
                o_vid = parseInt(o_vid);
                let uploaded_video_up = $(".ext_media_record").attr("total-media");
                uploaded_video_up = parseInt(uploaded_video_up)
                if(uploaded_video_up > o_vid){

                  uploaded_video_up = o_vid > 0?uploaded_video_up - o_vid:0;

                  $(".ext_media_record").attr("total-media",uploaded_video_up)
                }
              }

              _this.val("")
              $("#video_image_error").text(`Please upload ${file_Types.image.join(", ") + ", "+ file_Types.video.join(", ") } file only`).show();
              return false;
            }


            /* video/image check */
            let upload_vid_img = parseInt($(".ext_media_record").attr("total-media"));

            // if(upload_img > 0 && upload_vid > 0){
            //   _this.val("")
            //   let uploaded_images_up = $(".ext_media_record").attr("total-media");
            //     uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
            //     $(".ext_media_record").attr("total-media",uploaded_images_up)

            //     let uploaded_video_up = $(".ext_media_record").attr("total-media");

            //       uploaded_video_up = parseInt(uploaded_video_up) - valid_video;
            //       $(".ext_media_record").attr("total-media",uploaded_video_up)

            //     $("#video_image_error").text(`Maximum ${util.totalMedia} images/video are allowed`).show();
            //   return false;
            // }

            if(upload_vid_img > util.totalMedia){
              _this.val("")
              let uploaded_images_video = $(".ext_media_record").attr("total-media");
                uploaded_images_video = parseInt(uploaded_images_video) - valid_video_images;
              $(".ext_media_record").attr("total-media",uploaded_images_video)
                $("#video_image_error").text(`Maximum ${util.totalMedia} images/video are allowed`).show();
              return false;
            }

            // if(upload_img > util.totalMedia){
            //   _this.val("")
            //     $(".ext_media_record").attr("total-media",upload_img - valid_images)
            //     $("#video_image_error").text(`Maximum ${util.totalMedia} images/video are allowed`).show();
            //   return false;
            // }

            if(zero_len > 0){
               _this.val("")
                let uploaded_images_video = $(".ext_media_record").attr("total-media");
                uploaded_images_video = parseInt(uploaded_images_video) - valid_video_images;
              $(".ext_media_record").attr("total-media",uploaded_images_video)

                $("#video_image_error").text("File size should be greater than zero").show();
              return false;
            }
            if(video_len_error > 0){
               _this.val("")
              let uploaded_images_video = $(".ext_media_record").attr("total-media");
                uploaded_images_video = parseInt(uploaded_images_video) - valid_video_images;
              $(".ext_media_record").attr("total-media",uploaded_images_video)
              
              $("#video_image_error").text("Video size should not greater than "+util.maxVideoLenText).show();
              return false;
            }

            if(image_len_error > 0){
               _this.val("")
              let uploaded_images_video = $(".ext_media_record").attr("total-media");
                uploaded_images_video = parseInt(uploaded_images_video) - valid_video_images;
              $(".ext_media_record").attr("total-media",uploaded_images_video)
              
              $("#video_image_error").text("Image size should not be greater than "+util.maxImageLenText).show();
              return false;
            }

              for(let i = 0; i < files.length;i++){
                let spl = files[i].name.split(".");
                let get_type = spl[spl.length-1];
                get_type = get_type.toLowerCase();

              let file_name = files[i].name;
                let trim_name = file_name.substr(0,((file_name.length-1) - get_type.length)).substr(0,30)+"."+get_type;

                if(file_Types.image.indexOf(get_type) != -1){

            var reader = new FileReader();
              reader.onload = function(e){
              $(".media_preview").prepend(`<div class="img_video text-center">
              <i class="remove-img" data-parent = "${_cls+"_"+i}" title="Remove image" type="total-media"></i> 
              <img src="${e.target.result}" class="preview_image" title="${file_name}">
              <div class="slide_name" style='display:none' >${trim_name}</div>
              </div>`);
                }

              reader.readAsDataURL(files[i]);
          }else if(file_Types.video.indexOf(get_type) != -1){
            $(".media_preview").prepend(`<div class="img_video col-5 text-center">
              <i class="remove-img" data-parent = "${_cls+"_"+i}" title="Remove video" type="total-media"></i> 
              <video controls loop src="${URL.createObjectURL(files[i])}" class="preview_image" title="${file_name}"></video>
              <div class="slide_name" style='display:none' media-name >${trim_name}</div>
              </div>`);
          }
         }

       if(parseInt($(".ext_media_record").attr("total-media")) >= util.totalMedia){
          $(".images_placehold").hide();
        }
      });
    });

    //not_accept
    $(document).on("click",".remove-img",function(){
      $("#video_image_error").text("").hide();
      let num = $(this).attr("data-parent");
      let index = num[0]
      let index1 = num[2]

      $(".non_acceptable_files").val($(".non_acceptable_files").val()+","+num)
      $(".ext_media_record").attr("total-media",parseInt($(".ext_media_record").attr("total-media")) - 1)

      $(this).parent().remove()
      setTimeout(function(){
        if($(".images_container").find(".img_video").length < util.totalMedia){
          $(".images_placehold").show();
        }
      },100)
    })