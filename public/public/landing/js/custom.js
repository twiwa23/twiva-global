
// sticky header js 

	
	// fixed header on scroll
$(window).scroll(function(){
    if ($(window).scrollTop() >= 77) {
        $('.navbar').addClass('fixed-header');
    }
    else {
        $('.navbar').removeClass('fixed-header');
    }

});

// active menu

jQuery(function () {

    jQuery(".navbar li a").click(function (e) {
        e.stopPropagation();
        jQuery(this).addClass("active");
//        jQuery(".scroll_control").removeClass('selected');
//        var str = jQuery(this).attr('href');
////        var res = str.replace("#", "");
//        jQuery(str).addClass('selected');
        

    });

        
    jQuery('body').click(function () {

        if (jQuery(".navbar li a").hasClass("active")) {
            jQuery(".navbar li a").removeClass("active");

        };
    });

});

// Owl Carousel
 $('.blog-carousel').owlCarousel({
            loop: false,
			center:true,
            pagination: true,
            autoplayTimeout: 3000,
			smartSpeed: 1000,
            autoplayHoverPause: false,
            nav: true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                768: {
                    items: 1
                },
				1025: {
                    items: 1
                }
            }
        })
		
		$('#client-speak').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            pagination: true,
            autoplayTimeout: 3000,
			smartSpeed: 1000,
            autoplayHoverPause: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                }
            }
        })


/* wow animation */
 new WOW().init();
