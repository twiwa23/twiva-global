@extends('layouts.app')
@section('title')
    404
@endsection
@section('content')
<!--Banner Section-->
<section id="error-page" class="d-flex align-items-center justify-content-center"  style="width: 100vw; height: 550px; padding-top: 100px;">
    <div>
        <img src="{{ asset('assets/images/404.png') }}" alt="">
    </div>
</section>
</div>
@endsection