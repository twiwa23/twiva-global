@extends('admin.layouts.app')
@section('title', ' Add Featured')
@section('content')
 <style type="text/css">
    a.btn.btn-primary.center.google {
    float: right;
      
}
ul.breadcrum {
    display: inline-block;
}
.btn.btn-primary.center {
    margin-left: 3.2%;
}
</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Add Featured Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
 
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/featured')}}">Featured</a></li>
                  <li>Add</li>
                </ul>
                @include('admin.layouts.notifications')
                  <div class="x_content">
                    <form method = "post" action = "" enctype=multipart/form-data>
                        {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                       <tr>
                          <td width="200">Featured Image</td>
                        <td class="add-menu" > 
                       @php($url =  url('public/admin/production/images/user.png'))
                         <img  title="Click to change image"
                           onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 100px;height: 100px; object-fit: scale-down;" />
                        <input style="display:none;" type="file" id="imgInp" name="image" data-role="magic-overlay"
                             data-target="#pictureBtn" value="">
                       <span style="display:none" class="text-danger" id="invalid_file">The image must be a file of type: jpeg, png, jpg. </span>
                       <span class="text-danger" style="display: inherit;">{{$errors->first('image')}}</span>
                       </td> 
                        </tr>
                          <tr>
                        	<td style="width:200px">Link</td>
                            <td><input type="text" name = "link" class="form-control" value="{{old('link')}}" placeholder="http://www.google.com"/>
                            <span class="text-danger">{{$errors->first('link')}}</span>
                            </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>  
                            <td><input type = "submit" name = "Add" value = "Add" class="btn btn-primary center"></td>
                        </tr>          
                    </table>
                  </form>
                  </div>
              
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        @endsection
        @section('js')
        <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
@endsection
        
