@extends('admin.layouts.app')
@section('title', ' Add Gig Price')
@section('content')
<style type="text/css">
  table#datatable-filter tr th:nth-child(1) {
    width: 70px!important;
}
table#datatable-filter tr th:nth-child(4) {
    width: 240px!important;
    white-space: nowrap;
}
</style>

        <!-- page content -->
        <div class="right_col" role="main">
        <div> 
            <div class="page-title">
              <div class="title_left">
                <h3>Add Gig Price</h3>
              </div>
      <div class="clearfix"></div>
            </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li><a href="{{url('admin/plug')}}">Gig Management</a></li>
                   <li><a href="{{url('admin/view-gigs/'.$gigDetail->id)}}">Gig Details</a></li>
                   <li>Add Gig Price</li>
                </ul>
                      @include('admin.layouts.notifications') 
                  <div class="x_content">
                  
                    <table id="datatable-filter1" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                            <th>User Name</th> 
                            <th>Phone Number</th> 
                             <th width="100">Add Price</th>  
                              <th class="no-sort sort">Action</th>                         
                        </tr>
                      </thead>


                       <tbody>
                         @php ($i=1)
                        @foreach($user as $users)
                        <tr>
                        <form method="POST" class="" style="position: relative; float: left;
                       width: 100%;">
                        {{@csrf_field()}}
                          <td>{{$i++}}</td>
                         <td>
                       @if(!empty($users->name)){{$users->name}} @else N/A @endif
                          </td>  
                           <td>
                       @if(!empty($users->phone_number)){{$users->phone_number}} @else N/A @endif
                          </td>  
                          <td><span> 
                            <input type = "text" class = "form-control" onkeypress="return isNumberKey(event)" name="price" maxlength="5">
                             <input type = "hidden" class = "form-control"  name="user_id" value = "{{$users->id}}">
                          </span>
                          </td>
                        <td><button type="submit" class="btn btn-primary">Add Price</button>
                        </td>
                         </form>
                        </tr>
                          @endforeach  
                      </tbody>
                    </table>
                  
                  </div>
                </div>
              </div>
                </div>
               </div>
              </div>
            </div>
          </div>

     
        <!-- /page content -->



    @endsection
    @section('js')
       <script>
      $(document).ready(function() {
      $('#datatable-filter1').dataTable({
        columnDefs: [
          { targets: 'no-sort', orderable: false }
        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search..."
        },
        "bLengthChange" : false, 
           "bInfo":false, 
         "pageLength": 10,   
       });
      });
    </script>

   <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       //-->
    </SCRIPT>
    <script>
      
     $(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });
         
   </script>

   <script>
      
     $(".alert-danger").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-danger").slideUp(500);
    });
         
   </script>
    @endsection