@extends('admin.layouts.app')
@section('title', ' Add Subadmin')
@section('content')
<style type="text/css">
  .cal.w100 {
    position: relative;
     position: relative;
    display: inline;
}
  
  input#datepicker-3 {
    display: inline-block;
    cursor: pointer;
        background-color: #fff;

}

</style>

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Add Subadmin</h3>
              </div>
			<div class="clearfix"></div>
            </div>
     <!--        @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
            @endif -->
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/subadmin-management')}}">Subadmin Management</a></li>
                  <li>Add</li>
                </ul>
                  <div class="x_content">
                    <form method ="post" action = "" enctype = multipart/form-data>
                               {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                    
                                                
                       <tr>
                          <td style="width:200px">Name</td>
                            <td><input type="text" name = "name" value= "{{old('name')}}" class="form-control" placeholder=""/>
                             <span class="text-danger">{{$errors->first('name')}}</span>
                            </td>
                        </tr>
                        <tr>
                          <td style="width:200px">Email Address</td>
                            <td><input type="text"  name = "email" value = "{{old('email')}}"class="form-control" />
							 <span class="text-danger">{{$errors->first('email')}}</span>
							</td>
                        </tr>
                           <tr>
                          <td>Phone Number</td>
                      <td><input type="text" name = "phone_number" onkeypress="return isNumberKey(event)" class="form-control" value="{{old('phone_number')}} "/>
                       <span class="text-danger">{{$errors->first('phone_number')}}</span>
                      </td>
                        </tr>
                          <tr>
                          <td>Password</td>
                      <td><input type="text" name = "visible_pwd" class="form-control" value="{{old('visible_pwd')}} "/>
                       <span class="text-danger">{{$errors->first('visible_pwd')}}</span>
                      </td>
                        </tr>
                          
                           <tr>
                        <td style="width:200px">
                          Permission
                        </td>
                       <td> 
                        <div class="new-homess">
                        
                      
                       <ul>
                        @foreach($sidebar as $value)
                         <li>
                          <div class="col-sm-4">
                        <input type="checkbox"  name="sidebar_id[{{$value->id}}]" value ='{{$value->id}}'>&ensp;{{$value->name}}   
                        </div>
                        <div class="col-sm-6">
                        @if($value->name == "Blog" || $value->name == "Interests" || $value->name == "Twiva Updates")
                        <label>Add</label> &nbsp;
                        <input type="checkbox" name="add[{{$value->id}}]" value = "1"
                       
                        > &nbsp; &nbsp;
                        @endif
                         @if($value->name == "Influencer Management" || $value->name == "Business Management" || $value->name == "Blog" || $value->name == "Interests" || $value->name == "Featured")
                        <label>Edit</label>
                        <input type="checkbox" name="edit[{{$value->id}}]" value = "1" > &nbsp; &nbsp;
                        @endif
                        @if($value->name == "Influencer Management" || $value->name == "Business Management" || $value->name == "Post Management" || $value->name == "Gigs Management" || $value->name == "Blog" || $value->name == "Featured" || $value->name == "Interests" || $value->name == "Influencer Payment" || $value->name == "Business Payment" || $value->name == "Refferal Management" || $value->name == "Twiva Updates" || $value->name == "User Post" || $value->name == "Creators Management")
                          <label>View</label>&nbsp;
                        <input type="checkbox" name="view[{{$value->id}}]" value = "1" > &nbsp; &nbsp; 
                        @endif
                        @if($value->name == "Influencer Management" || $value->name == "Business Management" || $value->name == "Blog" || $value->name == "Interests" || $value->name == "User Post" || $value->name == "Creators Management" )
                        <label>Delete</label> &nbsp;
                        <input type="checkbox" name="delete[{{$value->id}}]" value = "1" > &nbsp; &nbsp;
                         @endif
                      </div>
                         </li>

                                @endforeach
                                </ul>
                       
                              </div>
                      
                          </td>
                      </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	
                            <td><input type = "submit" name = "Add" value = "Add" class="btn btn-primary center"></td>
                        </tr>
                    </table>
                   </form>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

     
@endsection

 @section('js')

<script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".select_image").click(function(){
          $(this).next().click();
        });

        $(".image_choose").change(function(event){
          var choose_id = $(this).attr("id");
          var preview_id = choose_id.split("-")[1];
          var input = event.target;
           if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file-'+preview_id).css({'display': 'none'});
                } else {
                    $('#invalid_file-'+preview_id).css({'display': 'block'});
                    //$('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_image-'+preview_id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }

        })
    </script>


     <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       //-->
    </SCRIPT>
 @endsection