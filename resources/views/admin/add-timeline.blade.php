@extends('admin.layouts.app')
@section('title', ' Add Timeline')
@section('content')
 <style type="text/css">
    a.btn.btn-primary.center.google {
    float: right;
      
}
ul.breadcrum {
    display: inline-block;
}
.choose_images {
  width: 100px;
  height: 100px;
  object-fit: scale-down;
}

.text-danger {
    color: red;
    display: block;
}
.preview_image_cover img {
    margin-left: 0;
    height: 138px;
    object-fit: contain;
    /* background-color: #000; */
    width: 100%;
}
/*.img_video {
  max-height:160px;
  max-width: 200px;
  height:160px;
  width:200px;
  }*/
.preview_video {
    max-height: 136px;
    max-width: 185px;
    height: 160px;
    width: 200px;
    background-color: #000;
}

</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div> 
            <div class="page-title">
              <div class="title_left">
                <h3>Add Timeline Details</h3>
              </div>
      <div class="clearfix"></div>
            </div>
 
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/timeline-list')}}">Timeline Managment</a></li>
                  <li>Add</li>
                </ul>
                @include('admin.layouts.notifications')
                  <div class="x_content">
                    <form method = "post"  enctype="multipart/form-data">
                        {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                       <tr>
                          <td style = "width:230px;">Media</td>
                        <td class="add-menu"> 
                       @php($url =  url('public/admin/production/images/user.png'))
                       <div class="media_preview">
                           <img  title="Click to change image/video" src='{{$url}}' id="uploader" class="choose_images"  />
                       </div>
                        <input style="display:none;" type="file" name="image_video[]" class="get_input_files" multiple >
                       <span style="display:none" class="text-danger" id="invalid_file">The image must be a file of type: jpeg, png, jpg. </span>
                       <span class="text-danger" style="display: inherit;">{{$errors->first('image_video')}}</span>
                       </td> 
                        </tr>
                     
                         <tr>
                          <td>Timeline Text </td>
                      <td>
                        <div class = "text-area">
                       <textarea name="timeline_text">{{old('timeline_text')}}</textarea>
                       <span class="text-danger">{{$errors->first('timeline_text')}}</span>
                     </div>
                      </td>
                        </tr> 
                             
                        <tr>
                          <td>&nbsp;</td>  
                            <td><input type = "submit" name = "Add" value = "Add" class="btn btn-primary center"></td>
                        </tr>          
                    </table>
                  </form>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        @endsection
        @section('js')
        
    <script>
  var file_Types = {
                   "image" : ["jpeg","jpg","png"],
                   "video" : ["mp4","avi","3gp","flv","mov","video"]
                };

    $(document).on("click",".image_video_upload",function(){
      $("#all_error").hide().text("")
      let recursion = $(this).attr("data-recursion");
      recursion++;
      $(this).attr("data-recursion",recursion)
      $(this).parent().append(`<input type="file" name="image_video[${recursion}][]" class="upload_image" multiple accept="video/*,image/*" />`)

      $(this).parent().find("input[name='image_video["+recursion+"][]']").click();

      $(document).on("change","input[name='image_video["+recursion+"][]']",function(event){
      var _this = $(this);
      let _cls = $(this).attr("name");
      _cls = _cls.replace("image_video",'');
      _cls = _cls.replace("[",'');
      _cls = _cls.replace("]",'');
      _cls = _cls.replace("[]",'');
      let files = event.target.files;
      var has_error = 0;
      var valid_images = 0;
      var video_len_error = image_len_error = 0;
      var zero_len = 0;

      for(let i = 0; i < files.length;i++){
        let spl = files[i].name.split(".");
                let get_type = spl[spl.length-1];
                let file_len = files[i].size;
                if(file_len == 0){
                  zero_len++;
                }

                get_type = get_type.toLowerCase();
                if(file_Types.image.indexOf(get_type) == -1 && file_Types.video.indexOf(get_type) == -1){
                   has_error++;
                }else {
                  if(file_Types.image.indexOf(get_type) != -1){
                    if(file_len > 4194304){
                      image_len_error++;
                    }
                  }
                  if(file_Types.video.indexOf(get_type) != -1) {
                    if(file_len > 20971520){
                      video_len_error++;
                    }
                  }

                  let uploaded_images = $(".ext_record").attr("images");
                  uploaded_images = parseInt(uploaded_images)
                  uploaded_images = uploaded_images + 1
                  valid_images++;
                  $(".ext_record").attr("images",uploaded_images)
                }
              }

              if(has_error > 0){
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                _this.val("")
                $("#all_error").show().text("Please upload jpg, jpeg, png, mp4, 3gp, mov, avi file only")
                return false;
              }
              if(parseInt($(".ext_record").attr("images")) > 5){
                _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("Maximum 5 images/videos are allowed")
                  
                return false; 
              }

              if(zero_len > 0){
              _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("File size should be greater than zero")
                  
                return false; 
            }
            if(image_len_error > 0){
              _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("Image size should not greater than 4MB")
                  
                return false; 
            }
            if(video_len_error > 0){
              _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("Video size should not greater than 20MB")
                  
                return false; 
            }


                for(let i = 0; i < files.length;i++){
                  let spl = files[i].name.split(".");
                  let get_type = spl[spl.length-1];
                  get_type = get_type.toLowerCase();

                  let file_name = files[i].name;
                  let trim_name = file_name.substr(0,((file_name.length-1) - get_type.length)).substr(0,30)+"."+get_type;
                  let total_uploads = $(".img_container").find(".cus_img_video").length;
                    if(file_Types.image.indexOf(get_type) != -1){
                        var reader = new FileReader();
                      reader.onload = function(e){
                      $(".media_preview").append(`<div class="img_video col-sm-4 text-center"><div class="preview_image_cover">
                      <i class="glyphicon glyphicon-remove remove-img" data-parent = "${_cls+"_"+i}" title="Remove image"></i>  
                      <img src="${e.target.result}" class="preview_image" title="${file_name}"><br/><div style="word-break: break-all;">${trim_name}</div></div></div>`);
                        }
                      reader.readAsDataURL(files[i]);
                  }else if(file_Types.video.indexOf(get_type) != -1){
                    $(".media_preview").append(`<div class="img_video col-sm-4 text-center"><div class="preview_image_cover">
                      <i class="glyphicon glyphicon-remove remove-img" data-parent = "${_cls+"_"+i}" title="Remove video"></i>  
                      <video controls src="${URL.createObjectURL(files[i])}" class="preview_video" title="${file_name}"></video><br/><div style="word-break: break-all;">${trim_name}</div></div><div>`);
                  }
           }
           setTimeout(function(){
            if($(".media_preview").find(".img_video").length > 1){
                $(".media_chooser").addClass("col-sm-4")
              }else {
                $(".media_chooser").removeClass("col-sm-4")
              }
           },100)
           if(parseInt($(".ext_record").attr("images")) >= 5){
              $(".image_video_upload").hide();
            }
      });
    });

//not accept images/video
    $(document).on("click",".remove-img",function(){
      let num = $(this).attr("data-parent");
      let index = num[0]
      let index1 = num[2]
      //$(".choose_container input[name='images["+index+"]["+index1+"]']").val("")
      $(".not_accept").val($(".not_accept").val()+","+num)
      $(this).closest(".img_video").remove()
      $(".ext_record").attr("images",parseInt($(".ext_record").attr("images")) - 1)
      setTimeout(function(){
        if($(".media_preview").find(".col-sm-4").length < 5){
          $(".image_video_upload").show();
        }
            if($(".media_preview").find(".img_video").length > 1){
                $(".media_chooser").addClass("col-sm-4")
              }else {
                $(".media_chooser").removeClass("col-sm-4")
              }
           },100)
    })


/* upload multiple images/video */
    $(".choose_images").click(function(){
      $(".get_input_files").click();
    })
    var file_Types = {
                   "image" : ["jpeg","jpg","png"],
                   "video" : ["mp4","avi","3gp","flv","mov","video"]
                };

    $(".get_input_files").change(function
      (event){
        var _this = $(this);
        let files = event.target.files;
        var has_error = 0;
        var valid_images = 0;
        var video_len_error = image_len_error = 0;
        var zero_len = 0;
        if(files.length > 10){
          _this.val("")
          alert("Maximum 10 images/videos are allowed");
          return false;
        }
      for(let i = 0; i < files.length;i++){
        let spl = files[i].name.split(".");
                let get_type = spl[spl.length-1];
                let file_len = files[i].size;

                get_type = get_type.toLowerCase();
                if(file_Types.image.indexOf(get_type) == -1 && file_Types.video.indexOf(get_type) == -1){
                   has_error++;
                }else {
                  if(file_Types.image.indexOf(get_type) != -1){
                    if(file_len > 4194304){ /* 4 mb for image */
                      image_len_error++;
                    }
                  }
                  if(file_Types.video.indexOf(get_type) != -1) {
                    if(file_len > 20971520){ /*20mb for video */
                      video_len_error++;
                    }
                  }
                }
              }

              if(has_error > 0){
                _this.val("")
                alert("Please upload jpg, jpeg, png, mp4, 3gp, mov, avi file only")
                return false;
              }
            if(image_len_error > 0){
              _this.val("")
               alert("Image size should not greater than 4MB")
                return false; 
            }
            if(video_len_error > 0){
              _this.val("")
                  alert("Video size should not greater than 20MB")
                return false; 
            }

            for(let i = 0; i < files.length;i++){
                  let spl = files[i].name.split(".");
                  let get_type = spl[spl.length-1];
                  get_type = get_type.toLowerCase();

                  let file_name = files[i].name;
                  let trim_name = file_name.substr(0,((file_name.length-1) - get_type.length)).substr(0,30)+"."+get_type;
                    if(file_Types.image.indexOf(get_type) != -1){
                        var reader = new FileReader();
                      reader.onload = function(e){
                      $(".media_preview").append(`<div class="img_video col-sm-3 text-center"><div class="preview_image_cover">
                      <img src="${e.target.result}" class="preview_image"></div></div>`);
                        }
                      reader.readAsDataURL(files[i]);
                  }else if(file_Types.video.indexOf(get_type) != -1){
                    $(".media_preview").append(`<div class="img_video col-sm-3 text-center"><div class="preview_image_cover">
                      <video controls src="${URL.createObjectURL(files[i])}" class="preview_video" ></video></div><div>`);
                  }
           }

           $(".choose_images").hide()
      })
    </script>
@endsection