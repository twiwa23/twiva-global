@extends('admin.layouts.app')
@section('title', ' Blog')
@section('content')

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Blog</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li>Blog</li>
                </ul>
                 <?php
                $get_permission  = DB::table('subadmin_permissions')
                                  ->join("sidebars","sidebars.id","=","subadmin_permissions.sidebar_id")
                            ->where(['subadmin_permissions.user_id'=>$subadmin_user_id])
                            ->where(['sidebars.name' => "Blog"])
                            ->first();


                //echo "<pre>";print_r($get_permission);die;
                if(!empty($get_permission)) {
                $add =$get_permission->add;
                $edit = $get_permission->edit;
                $view = $get_permission->view;
                $delete = $get_permission->delete; 
                }else{
                $add = 1 ;
                $edit = 1;
                $view =1;
                $delete =1;        
                }
                
                
                ?>
                @if($add == 1)
                <a href="{{url('admin/add-blog')}}" class="btn btn-success">Add</a>
                @endif
                   @include('admin.layouts.notifications')
                  <div class="x_content">
                    <table id="datatable-filter" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                          
                            <th>Title</th>
                            
                            <th class="no-sort sort">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      
                       @php ($i=1)
                        @foreach($blog as $blogs)
                         <tr>
                         <td>{{$i++}}</td> 
                         
                          <td>@if(!empty($blogs->title)){{substr($blogs->title,0,15)}} @else N/A @endif</td>
                          
                           <td class = "text-center"> 
                            <?php $has = 0; ?>
                            @if($view == '1')
                            <?php $has++; ?>
                            <span><a href="{{url('admin/view-blog/'.$blogs->id)}}" class="btn btn-success">View</a>
                              @endif
                            @if($delete == '1')
                            <?php $has++; ?>
                           <a type="button" data-toggle="modal" id="{{$blogs->id}}" data-target="#myModal" class="btn btn-danger del">Delete</a>
                          </span>
                         @endif
                        
                        <?php if($has == 0) { ?>
                          N/A
                        <?php } ?>
                        </td>
                        </tr>  
                      @endforeach
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                </div>
               </div>
              </div>
            </div>
          </div>

     
        <!-- /page content -->

<div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
     <!-- Modal content-->
     <div class="modal-content">
        <div class="modal-header">
          <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
           <h4 class="modal-title" style="text-align: center;" ><b>Alert</b></h4>
        </div>
        <div class="modal-body" style="text-align: center; padding-bottom: 0px; margin: 0px 0 26px;">
          <p><strong>Are you sure you want to delete this Blog ?</strong></p>
          <form id="del" action="{{url('admin/delete-blog')}}" method="post">
          {{ csrf_field() }}
          <input id = "customer_id" name = "customer_id" type=  "hidden">
          </form>
        </div>
        <div class="modal-footer" style="text-align: center;">
        <button type="button" onclick="$('#del').submit()" class="btn btn-danger home dele" id="modal-btn-si">Delete</button>
           <!--  <a href="@if(!empty($user->id)) {{url('admin/delete-user',$user->id)}}@else 'N/A' @endif" class="btn btn-danger home dele"> Delete</a> -->
          <button type="button" class="btn btn-danger home dele" data-dismiss="modal">Cancel</button>
        </div>
     </div>                                       
   </div>
</div>
    @endsection

    @section('js')
  <script type="text/javascript">
    $('.del').bind('click', function() {
      var i = $(this).attr('id');
      $('#customer_id').val(i);
    });

  </script>
 <script type="text/javascript">
    $(function() {
      setTimeout(function(){
          $(".alertz").hide();
      }, 3000);
    });
    </script>
@endsection
