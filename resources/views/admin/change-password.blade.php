@extends('admin.layouts.app')
@section('title', ' Change Password')
@section('content')

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Change Password</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li><a href="javascript:void(0);">Manage Account</a></li>
                  <li>Change Password</li>
                </ul>
                 @include('admin.layouts.notifications')
                  <div class="x_content">
                     <form method = "post" action = "">
                       {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table">
                      <tr>
                          <td>Old Password</td>
                              <td><input type="Password"  name = "old_password" value = ""class="form-control" placeholder="**********"/>
                             <span class="text-danger error">{{$errors->first('old_password')}}</span></td>
                        </tr>
                        <tr>
                        	<td style="width:200px">New Password</td>
                           <td><input type="Password" name ="new_password" value = ""class="form-control" placeholder="**********"/>
                              <span class="text-danger error">{{$errors->first('new_password')}}</span></td>
                        </tr>
                         <tr>
                          <td style="width:200px">Confirm Password</td>
                             <td><input type="Password" name = "confirm_password" value = "" class="form-control" placeholder="**********"/>
                              <span class="text-danger error">{{$errors->first('confirm_password')}}</span>
                            </td>
                        </tr>         	 
                          <td></td>
                            <td><input type = "submit" name = "Update" value = "Update" class="btn btn-primary center"></td>
                        </tr>
                    </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->


@endsection
@section('js')
  
 <script type="text/javascript">
    $(function() {
      setTimeout(function(){
          $(".alertz").hide();
      }, 3000);
    });
    </script>
@endsection()