@extends('admin.layouts.app')
@section('title', ' Edit Business Details')
@section('content')
 <style type="text/css">
    a.btn.btn-primary.center.google {
    float: right;
      
}
ul.breadcrum {
    display: inline-block;
}
</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Business Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
 
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/business')}}">Business Management</a></li>
                  <li>Edit</li>
                </ul>
                @include('admin.layouts.notifications')
                  <div class="x_content">
                    <form method = "post" action = "" enctype=multipart/form-data>
                        {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                       <tr>
                          <td>Profile Picture</td>
                        <td class="add-menu"> 
                       @php($url =  $user->profile ? url($user->profile) : url('public/admin/production/images/user.png'))
                         <img  title="Click to change image"
                           onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 100px;height: 100px; object-fit: scale-down;" />
                        <input style="display:none;" type="file" id="imgInp" name="image" data-role="magic-overlay"
                             data-target="#pictureBtn" value="{{$user->profile}}">
                       <span class="text-danger">{{$errors->first('image')}}</span>
                       <span style="display:none" class="text-danger" id="invalid_file">The image must be a file of type: jpeg, png, jpg. </span>

                       </td> 
                        </tr>
                        <tr>
                        	<td style="width:200px">Business Name</td>
                            <td><input type="text" name = "name" class="form-control" value="@if(!empty($user->name)){{$user->name}} @else @endif"/>
                            <span class="text-danger">{{$errors->first('name')}}</span>
                            </td>
                        </tr>
                       
                         <tr>
                          <td>Email Address</td>
                      <td><input type="text" name = "email" class="form-control" value="@if(!empty($user->email)){{$user->email}} @else  @endif"/ readonly>
                       <span class="text-danger">{{$errors->first('email')}}</span>
                      </td>
                        </tr>
                        <tr>
                          <td>Business Number</td>
                      <td><input type="text" name = "phone_number" id="intTextBox" class="form-control" value="@if(!empty($user->phone_number)){{$user->phone_number}} @else  @endif "/>
                       <span class="text-danger">{{$errors->first('phone_number')}}</span>
                      </td>
                        </tr>
                         <tr>
                          <td>Business Description </td>
                      <td><textarea  class="form-control" name = "description">@if(!empty($BusinessDetail->business_detail)){{$BusinessDetail->business_detail}} @else 
                       @endif</textarea>
                       <span class="text-danger">{{$errors->first('description')}}</span>
                      </td>
                        </tr> 
                        <tr>
                          <td>&nbsp;</td>  
                            <td><input type = "submit" name = "Update" value = "Update" class="btn btn-primary center"></td>
                        </tr>          
                    </table>
                  </form>
                  </div>
                  <table id="datatable-filter" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                            <th class="no-sort">Contact Person Image</th> 
                            <th>Contact Person Name</th>
                            <th>Contact Person Number</th> 
                            <th class="no-sort" style="text-align: center;">Action</th>                        
                        </tr>
                      </thead>

                      <tbody>
                       @php ($i=1)
                        @foreach($gigData as $gig)
                        <tr>
                          <td>{{$i++}}</td>
                          <td>
                           @if($gig->profile)
                            <img src="{{$gig->profile}}" alt="User" style="width: 50px;height: 50px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 50px; height: 50px; object-fit: cover;">
                           @endif
                       </td>
                          <td>@if(!empty($gig->owner_name)){{$gig->owner_name}} @else N/A @endif</td>
                          <td>@if(!empty($gig->phone_number)){{$gig->phone_number}} @else N/A @endif</td>
                        <td><span><a href='{{url("admin/edit-bussiness-contact/$gig->id/$gig->user_id")}}' class="btn btn-success">Edit</a></span></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        @endsection
        @section('js')
        <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
   <script type="text/javascript">
        function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}


// Install input filters.
setInputFilter(document.getElementById("intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });

    </script>
@endsection
        
