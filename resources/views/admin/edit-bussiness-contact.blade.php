@extends('admin.layouts.app')
@section('title', ' Edit Contact Person Details')
@section('content')

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Contact Person Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/business')}}">Business Management</a></li>
                <li><a href="{{url('admin/edit-business/'.$user->id)}}"> Edit Business Details</a></li>
                <li>Edit Contact Person Details</li>
                </ul>
                  <!--   @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
           @endif -->
                    @include('admin.layouts.notifications')
                  <div class="x_content">
                     <form method = "post" action = "" enctype=multipart/form-data>
                        {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                       <tr>
                          <td style="width:200px">Contact Person Image</td>
                        <td class="add-menu"> @php($url =  $gigData->profile ? url($gigData->profile) : url('public/admin/production/images/user.png'))
                        <img  title="Click to change image"
                         onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 100px;height: 100px; object-fit: scale-down;" />
                    <input style="display:none;" type="file" id="imgInp" name="image" data-role="magic-overlay"
                           data-target="#pictureBtn" value="{{$user->profile}}">
                    <span class="text-danger">{{$errors->first('image')}}</span>
                    <span style="display:none" class="text-danger" id="invalid_file">The image must be a file of type: jpeg, png, jpg. </span>
                      </td> 
                        </tr>
                       
                         <tr>
                          <td>Contact Person Name</td>
                      <td><input type="text" name = "owner_name" value = "@if(!empty($gigData->owner_name)){{$gigData->owner_name}} @else N/A @endif "class="form-control" />
                      <span class="text-danger">{{$errors->first('owner_name')}}</span>
                      </td>
                        </tr>
                        <tr>
                          <td>Contact Person Number</td>
                      <td><input type="text" name = "phone_number" class="form-control" value="@if(!empty($gigData->phone_number)){{$gigData->phone_number}} @else N/A @endif "class="form-control" />
                     <span class="text-danger">{{$errors->first('phone_number')}}</span>
                      </td>
                        </tr>
                          
                            <tr>
                          <td>&nbsp;</td>  
                            <td><input type = "submit" name = "Update" value = "Update" class="btn btn-primary center"></td>
                        </tr>            
                    </table>
                  </form>
                  </div>
                 
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->



   @endsection
   @section('js')
        <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
@endsection