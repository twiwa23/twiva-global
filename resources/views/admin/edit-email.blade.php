@extends('admin.layouts.app')
@section('title', ' My Profile')
@section('content')
  <style type="text/css">
    a.btn.btn-primary.center {
    float: right;
       margin-top: 16px;

}
ul.breadcrum {
    display: inline-block;

}
input.form-control.for::placeholder {
    color: #000;
}
  </style>



        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>My Profile</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li><a href="javascript:void(0);">Manage Account</a></li>
                  <li>My Profile</li>

                </ul>
                   <a href="{{url('admin/manage-email')}}" class="btn btn-primary center">Edit</a>

                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table">
                      <tr>
                          <td>Email Address</td>
                            <td><input type="text" name= "email" value = "{{$user->email}}" class="form-control for" placeholder=""/ disabled=""></td>
                        </tr>         
                         <!-- <tr>
                          <td>Phone Number</td>
                            <td><input type="text" name= "phone_number" value = "{{$user->phone_number}}" class="form-control for" placeholder=""/ disabled=""></td>
                        </tr>    -->  
                    </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->


@endsection