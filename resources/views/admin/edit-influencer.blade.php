@extends('admin.layouts.app')
@section('title', ' Edit Influencer')
@section('content')
<style type="text/css">
  .cal.w100 {
    position: relative;
     position: relative;
    display: inline;
}
  
  input#datepicker-3 {
    display: inline-block;
    cursor: pointer;
        background-color: #fff;

}

</style>

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Influencer</h3>
              </div>
			<div class="clearfix"></div>
            </div>
     <!--        @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
            @endif -->
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/influencer-Management')}}">Influencer Management</a></li>
                  <li>Edit</li>
                </ul>
                  <div class="x_content">
                    <form method ="post" action = "" enctype = multipart/form-data>
                               {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                      <tr>
                          <tr>
                           <td style="width:200px">Image</td>
                            <td>
                              <?php
                                $i = 0;
                               ?>
                            @foreach($infulancerImageData as $ImageData)
                            <?php $i++; ?>
                             @php($url = $ImageData->image ? url($ImageData->image) : url('public/admin/production/images/user.png'))
                          
                              <img title="Click to change image" class="select_image" src='{{$url}}' id="preview_image-{{$i}}" style="width: 100px;height: 100px; object-fit: cover;" />

                              <input style="display:none;" type="file" id="imgInp-{{$i}}" class="image_choose" name="image[]" style="width: 100px;height: 100px; object-fit: cover;" value="{{$ImageData->image}}" >

                              <input type = "hidden" name = "imageid[]" value = "{{$ImageData->id}}">

                              <span class="text-danger">{{$errors->first('image')}}</span>

                              <span style="display:none" class="text-danger" id=" invalid_file-{{$i}}">The image must be a file of type: jpeg, png, jpg. </span>              
                              @endforeach
                           </td>
                            
                        </tr>
                                                
                       <tr>
                          <td style="width:200px">Full Name</td>
                            <td><input type="text" name = "name" value= "@if(!empty($user->name)){{$user->name}} @else  @endif" class="form-control" placeholder=""/>
                             <span class="text-danger">{{$errors->first('name')}}</span>
                            </td>
                        </tr>
                        <tr>
                          <td style="width:200px">Email Address</td>
                            <td><input type="text"  name = "email" value = "@if(!empty($user->email)){{$user->email}} @else @endif"class="form-control" placeholder="" readonly/></td>
                        </tr>
                           <tr>
                          <td>Phone Number</td>
                      <td><input type="text" name = "phone_number" onkeypress="return isNumberKey(event)" class="form-control" value="@if(!empty($user->phone_number)){{$user->phone_number}} @else  @endif "/>
                       <span class="text-danger">{{$errors->first('phone_number')}}</span>
                      </td>
                        </tr>
                        <tr>
                          <td style="width:200px">Desired Post Price</td>
                            <td><input type="text"  onkeypress="return isNumberKey(event)" name = "plug_price" value = "@if(!empty($InfulancerPrice->plug_price)){{$InfulancerPrice->plug_price}} @else @endif" class="form-control txtChar" placeholder=""/>
                              <span class="text-danger">{{$errors->first('plug_price')}}</span>
                            </td>
                            
                        </tr>
                         <!-- <tr>
                          <td style="width:200px">Average Plug Price</td>
                            <td><input type="text" class="form-control" placeholder=""/></td>
                        </tr> -->
                         <tr>
                          <td style="width:200px">Desired Gig Price</td>
                            <td><input type="text" onkeypress="return isNumberKey(event)" name = "gig_price" value = "@if(!empty($InfulancerPrice->gig_price)){{$InfulancerPrice->gig_price}} @else @endif" class="form-control txtChar" placeholder=""/>
                            <span class="text-danger">{{$errors->first('gig_price')}}</span>
                            </td>
                           
                        </tr>
                        <!--  <tr>
                          <td style="width:200px">Average Gig Price</td>
                            <td><input type="text" class="form-control" placeholder=""/></td>
                        </tr> -->
                         <tr>
                            <tr>
                          <td style="width:200px">Date Of Birth</td>
                            <td><div class="field">
                           <div class="cal w100">
                               <span> <input type="text" name = "date" value = "@if(!empty($InfulancerDetail->dob)){{$InfulancerDetail->dob}} @else @endif" id="datepicker-3" class="form-control datepicker" placeholder="Select Time" ><!--  <i class="fa fa-calendar" aria-hidden="true"></i> --></span>
                            </div>
                         </div>
                         <span class="text-danger">{{$errors->first('dob')}}</span>
                       </td>
                        </tr>
                        @if(!empty($InfulancerDetail->gender))
                      <tr>
                        <td style="width:200px">
                          Gender
                        </td>
                       <td> 
                        <select name = "gender"class="selectpicker form-control">
                        <option value="" selected>Select Gender </option> 
                        <option value="1" @if($InfulancerDetail->gender == "1") selected @endif>Male</option>
                        <option value="2" @if($InfulancerDetail->gender == "2") selected @endif>Female</option>
                        </select>
                        
                        <span class="text-danger">{{$errors->first('gender')}}</span>
                        </div>
                        </td>
                      </tr>
                      @else
                       <tr>
                        <td style="width:200px">
                          Gender
                        </td>
                       <td> 
                        <select name = "gender"class="selectpicker form-control">
                        <option value="" selected>Select Gender </option> 
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                        </select>
                        <span class="text-danger">{{$errors->first('gender')}}</span>
                        </div>
                        </td>
                      </tr>
                      @endif
                       <tr>
                          <td style="width:200px">Facebook Username</td>
                            <td><input type="text" name = "facebook_name" value = "@if(!empty($user->SocialDetail->facebook_name)){{$user->SocialDetail->facebook_name}} @else  @endif" class="form-control" placeholder=""/>
                               <span class="text-danger">{{$errors->first('facebook_name')}}</span>
                            </td>
                        </tr>
                          <tr>
                          <td style="width:200px">Twitter Username</td>
                            <td><input type="text" name = "twitter_name" value = "@if(!empty($user->SocialDetail->twitter_name)){{$user->SocialDetail->twitter_name}} @else @endif" class="form-control" placeholder=""/>
                           <span class="text-danger">{{$errors->first('twitter_name')}}</span>
                            </td>
                        </tr>
                         <tr>
                          <td style="width:200px">Instagram Username</td>
                            <td><input type="text"  name = "instagram_name" value = "@if(!empty($user->SocialDetail->instagram_name)){{$user->SocialDetail->instagram_name}} @else @endif" class="form-control" placeholder=""/>
                             <span class="text-danger">{{$errors->first('instagram_name')}}</span>
                            </td>
                        </tr>
                         <tr>
                          <td style="width:200px">Facebook Friends</td>
                            <td><input type="text" onkeypress="return isNumberKey(event)"  name = "facebook_friends"  value = "@if(!empty($user->SocialDetail->facebook_friends)){{$user->SocialDetail->facebook_friends}} @else  @endif"  class="form-control txtChar" placeholder=""/>
                             <span class="text-danger">{{$errors->first('facebook_friends')}}</span>
                            </td>
                        </tr>
                        <tr>
                          <td style="width:200px">Twitter Followers</td>
                            <td><input type="text" onkeypress="return isNumberKey(event)" name = "twitter_friends" value = "@if(!empty($user->SocialDetail->twitter_friends)){{$user->SocialDetail->twitter_friends}} @else  @endif" class="form-control txtChar" placeholder=""/>
                             <span class="text-danger">{{$errors->first('twitter_friends')}}</span>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:200px">Instagram Followers </td>
                            <td><input type="text" onkeypress="return isNumberKey(event)" name = "instagram_friends"  class="form-control txtChar" value = "@if(!empty($user->SocialDetail->instagram_friends)){{$user->SocialDetail->instagram_friends}} @else  @endif"   placeholder=""/>
                              <span class="text-danger">{{$errors->first('instagram_friends')}}</span>
                            </td>
                        </tr>
                           <tr>
                        <td style="width:200px">
                          Interests
                        </td>
                       <td> 
                        <div class="new-homess">
                        <?php 
                          $user_interests = [];
                          if($selected_interests && !empty($selected_interests)){
                            $user_interests = explode(",", $selected_interests);
                          }
                        ?>
                        @foreach($interests as $value)
                       
                        <input type="checkbox"  name="interest_id[]" value ='{{$value->id}}' {{ in_array($value->id, $user_interests) ? "checked" : ""}} />&ensp;{{$value->interest_name}}<br/>
                                @endforeach
                              </div>
                      
                          </td>
                      </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	
                            <td><input type = "submit" name = "Update" value = "Update" class="btn btn-primary center"></td>
                        </tr>
                    </table>
                   </form>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

     
@endsection

 @section('js')

<script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".select_image").click(function(){
          $(this).next().click();
        });

        $(".image_choose").change(function(event){
          var choose_id = $(this).attr("id");
          var preview_id = choose_id.split("-")[1];
          var input = event.target;
           if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file-'+preview_id).css({'display': 'none'});
                } else {
                    $('#invalid_file-'+preview_id).css({'display': 'block'});
                    //$('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_image-'+preview_id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }

        })
    </script>

<!-- <script type="text/javascript">
        function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}


// Install input filters.
setInputFilter(document.getElementById("intTextBox1"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("intTextBox2"), function(value) {
  return /^-?\d*$/.test(value); });

setInputFilter(document.getElementById("intTextBox3"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("intTextBox4"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("intTextBox5"), function(value) {
  return /^-?\d*$/.test(value); });
    </script> -->

     <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       //-->
    </SCRIPT>
 @endsection