@extends('admin.layouts.app')
@section('title', ' Edit Interests')
@section('content')
 <style type="text/css">
    a.btn.btn-primary.center.google {
    float: right;
      
}
ul.breadcrum {
    display: inline-block;
}
.btn.btn-primary.center {
    margin-left: 3.2%;
}
</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Interests Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
 
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/interests')}}">Interests</a></li>
                  <li>Edit</li>
                </ul>
                @include('admin.layouts.notifications')
                  <div class="x_content">
                    <form method = "post" action = "" enctype=multipart/form-data>
                        {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                       <tr>
                          <td>Interests Image</td>
                        <td class="add-menu"> 
                       @php($url =  $interests->image ? url($interests->image) : url('public/admin/production/images/user.png'))
                         <img  title="Click to change image"
                           onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 100px;height: 100px; object-fit: scale-down;" />
                        <input style="display:none;" type="file" id="imgInp" name="image" data-role="magic-overlay"
                             data-target="#pictureBtn" value="{{$interests->profile}}" placeholder="http://www.google.com">
                       <span class="text-danger">{{$errors->first('image')}}</span>
                       <span style="display:none" class="text-danger" id="invalid_file">The image must be a file of type: jpeg, png, jpg. </span>

                       </td> 
                        </tr>
                      <tr>
                        	<td style="width:200px">Interests</td>
                            <td><input type="text" name = "interest_name" class="form-control" value="{{$interests->interest_name}}"/>
                            <span class="text-danger">{{$errors->first('interest_name')}}</span>
                            </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>  
                            <td><input type = "submit" name = "Update" value = "Update" class="btn btn-primary center"></td>
                        </tr>          
                    </table>
                  </form>
                  </div>
              
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        @endsection
        @section('js')
        <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
@endsection
        
