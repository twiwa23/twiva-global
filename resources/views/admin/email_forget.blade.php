

<body style="font-family:'Times New Roman';font-size:15px">
<div style="margin-bottom: 20px;text-align: center;">
	<td align="center" valign="top" style="padding:20px 0;">
        	<a href="javascript:void(0);" style="border:0; outline:0;"><img src="{{url('public/admin/production/images/twiva.png')}}" alt="" width="100"/></a>
        </td>
	
</div>
	<div>
 		<p style="Margin-top:5px;Margin-bottom:10px">Hello <?php if(!empty($user_data->name)) echo $user_data->name; else echo'User'; ?>,</p>
		<p style="Margin-bottom: 15px;">
		<p>We have received your request to reset password. In order to proceed with this request, Please follow the link below: </p>
		<p style="Margin-bottom:10px;"><a href="{{ $url }}"> Click here for reset your password</a></p>
		<p style="Margin-bottom:10px;">If you don’t want to reset your password, you can ignore this email.</p>
		<p style="Margin-bottom:10px;">If you did not request this change, you may want to review your security settings or contact us for assistance.</p>
		{{ $url }}
		<p style="Margin-bottom:10px;">Best Regards, <br/>
		Your Friends at TWIVA
		</p>	
	</div>
</body>