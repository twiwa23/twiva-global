@extends('admin.layouts.app')
@section('title', ' Gigs Management')
@section('content')
<style type="text/css">
  table#datatable-filter tr th:nth-child(1) {
    width: 70px!important;
}
table#datatable-filter tr th:nth-child(4) {
    width: 240px!important;
    white-space: nowrap;
}
</style>

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Gigs Management</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li>Gigs  Management</li>
                </ul>
                 <?php
                 $get_permission  = DB::table('subadmin_permissions')
                                  ->join("sidebars","sidebars.id","=","subadmin_permissions.sidebar_id")
                            ->where(['subadmin_permissions.user_id'=>$subadmin_user_id])
                            ->where(['sidebars.name' => "Gigs Management"])
                            ->first();
                if(!empty($get_permission)) {
                $add =$get_permission->add;
                $edit = $get_permission->edit;
                $view = $get_permission->view;
                $delete = $get_permission->delete; 
                }else{
                $add = 1 ;
                $edit = 1;
                $view =1;
                $delete =1;        
                }
                
                
                ?>
                  <a href="{{url('admin/accepted-gig-list')}}" class="btn btn-success">Accepted</a>
                   <a href="{{url('admin/completed-gig-list')}}" class="btn btn-success">Completed</a>
                  <div class="x_content">
                    <table id="datatable-filter" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                            <th>Gigs Name</th>                     
                           <th class="no-sort sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>
                         @php ($i=1)
                        @foreach($gigDetail as $gig)
                         <tr>
                         <td>{{$i++}}</td> 
                          <td>@if(!empty($gig->gig_name)){{$gig->gig_name}} @else N/A @endif</td>
                           <td class = "text-center">  
                            <?php $has = 0; ?>
                            @if($view == '1')
                            <?php $has++; ?><span><a href="{{url('admin/view-gigs/'.$gig->id)}}" class="btn btn-success">View</a></span>
                         @endif
                        
                        <?php if($has == 0) { ?>
                          N/A
                        <?php } ?>
                      </td>
                        </tr>
                        @endforeach 
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                </div>
               </div>
              </div>
            </div>
          </div>

     
        <!-- /page content -->
@endsection