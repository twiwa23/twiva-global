@extends('admin.layouts.app')
@section('title', ' Dashboard')
@section('content')

        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3 class="text-center"><i class="fa fa-home" style="margin-right: 12px"></i></h3>
              </div>
			<div class="clearfix"></div>
            </div>
                <div class="x_panel" style="padding:30px  ">
        	<div class="dashboard-container">
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                                <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                          <i class="fa fa-user"></i>
                                            <h2>Total Brands</h2>
                                            <p>@if(!empty($brand_user)){{$brand_user}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                      <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                                <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                          <i class="fa fa-user"></i>
                                            <h2>Monthly Brands Signups</h2>
                                            <p>@if(!empty($monthly_brand_user)){{$monthly_brand_user}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                                <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                          <i class="fa fa-user"></i>
                                            <h2>Total Influencers</h2>
                                            <p>@if(!empty($influencer_user)){{$influencer_user}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                                <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                          <i class="fa fa-user"></i>
                                            <h2>Monthly Influencers Signups</h2>
                                            <p>@if(!empty($monthly_influencer_user)){{$monthly_influencer_user}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                                <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                       <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            <h2>Total Gigs</h2>
                                            <p>@if(!empty($total_gig)){{$total_gig}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                                <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                       <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            <h2>Monthly Gigs</h2>
                                            <p>@if(!empty($monthly_gig)){{$monthly_gig}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                               <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                         <i class="fa fa-shield" aria-hidden="true"></i>
                                           <h2>Total Posts</h2>
                                            <p>@if(!empty($total_post)){{$total_post}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                               <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                         <i class="fa fa-shield" aria-hidden="true"></i>
                                           <h2>Monthly Posts</h2>
                                            <p>@if(!empty($monthly_post)){{$monthly_post}} @else 0 @endif</p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
					<?php  $type = DB::table('admins')->where('id',$subadmin_user_id)->first();
                           $get_type = $type->type;

					?>
					@if($get_type == "admin")
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="content">
                               <a href="javascript:void(0);">
                                <div class="row">
                                    <div class="col-xs-12 footer text-center">
                                        <div class="numbers stats">
                                       <i class="fa fa-money" aria-hidden="true"></i>
                                           <h2>Brand Total Spending</h2>
                                            <p>
                                                <?php
                                                if(!empty($adminPayment)){
                                                    if($adminPayment >= 17200000){
                                                        $adminPayment = $adminPayment - 17200000;
                                                    }
                                                    if($adminPayment > 1000000){
                                                        echo number_format((float)($adminPayment / 1000000), 2, '.', '')." Million";
                                                    }else {
                                                        echo $adminPayment;
                                                    }
                                                }else {
                                                    echo "0";
                                                }
                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                  @endif
                    </div>
                    </div>
                   </div>
                  </div>
              </div>
            </div>
          </div>
          
     
      @endsection