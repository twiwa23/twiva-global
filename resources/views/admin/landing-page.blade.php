<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
       <title>Twiva</title>
        <link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
      <link rel="shortcut icon" type="image/png" href="{{url('public/landing/images/favicon.png')}}">
      <link rel="stylesheet" href="{{url('public/landing/css/influencer-landing.css')}}" />
      <link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
   </head>
   <style type="text/css">
      div#communityCounter {
      display: none;
      }
      .one-app__section {
      margin: -300px 0 -120px;
      width: 100%;
      }
   </style>
   <body id="influencer-landing" data-lang="en_GB">
      <div class="header__hamburger" id="headerHamburger">
      </div>
      <div class="heading toper">
         <div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
            <div class="header__current-language" id="languageSelector">
               <nav class="navbar">
                  <div class="container">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0);"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                           <li class="first-twiva"><a href="javascript:void(0);">Brands</a></li>
                           <li><a href="javascript:void(0);">Influencer</a></li>
                           <li><a href="javascript:void(0);"> About</a></li>
                           <li class="last"><a href="javascript:void(0);"> Blog</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>
      <section class="video" id="new">
         <div class="easyhtml5video" style="position:relative; max-width:100%;">
         <video playsinline="" muted="" autoplay="" preload="auto" loop="" poster="images/pov-time-lapse-journey-on-the-modern-driverless-dubai-el_002.jpg" style="width:100%; height:100%;" id="video">
            <source src="{{url('public/landing/images/pov-time-lapse-journey-on-the-modern-driverless-dubai-el_002.mp4')}}" type="video/mp4">
         </video>
         <div class="new-home "  data-aos="fade-up" data-aos-delay="300">
            <div class="container">
               <p>Tell Your Brand</p>
               <h2>Strong Better</h2>
               <h3>The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</h3>
               <a href="javascript:void(0)" class="new-buttom">Join to work with us</a>
            </div>
         </div>
      </section>
      <div class="skew">
         <a href="#nwes"><img src="{{url('public/landing/images/arrow-smoth.png')}}" alt=""></a>
      </div>
      <!-- how it works section -->
      <section class="tiwa-works" id="nwes">
         <div class="container">
         <div class="heading" data-aos="fade-down"  data-aos-delay="500">
            <p>How</p>
            <h2>Twiva Works</h2>
         </div>
         <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-6">
               <div class="new-box" data-aos="fade-right">
                  <h2>Offline <span> influence</span></h2>
                  <p>Twiva lets you invite influencers to events or venues - a feature not available on any other platform. Let influencers bring what they are best at to your door.</p>
               </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
               <div class="new-box jack" data-aos="fade-right">
                  <h2><span>Analytics</span></h2>
                  <p>Predict reach, cost, and engagement across influencer content all while measuring real-time influencer post-performance. Then generate reports to help align your marketing strategy with what is working for you.</p>
               </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
               <div class="new-box super"  data-aos="fade-right">
                  <figure><img src="{{url('public/landing/images/team.png')}}" alt=""></figure>
                  <h2><span>Streamlined Collaboration</span></h2>
                  <p class="john">Work with influencers to promote your business easily at scale using our streamlined collaboration tool to make sure every shilling you spend on influencers reaches an audience who matters Every step of your influencer marketing campaign:<br>
                     Twiva streamlines your collaboration with influencers from; discovering, evaluating, engaging, activating, measuring and compensating influencers
                  </p>
               </div>
            </div>
         </div>
      </section>
      <!-- how it ends section -->
      <div class="counter community__counter-container" id="communityCounter" data-counter-value="3813">
      </div>
      <section class="section-v2 one-app__section" id="new">
         <div class="col-md-6">
            <div class="section-v2__animation one-app__animation">
               <div class="one-app">
                  <div class="one-app__stripe" data-aos="fade-left" data-aos-anchor-placement="center-bottom"></div>
                  <div class="one-app__stripe-second" data-aos="fade-up" data-aos-anchor-placement="center-bottom"></div>
                  <div class="one-app__phone">
                     <div class="one-app__phone-header"></div>
                     <div class="one-app__screens">
                        <div class="one-app__screen one-app__screen-donuts one-app__screen-left one-app__screen-left"></div>
                        <div class="one-app__screen one-app__screen-blonde"></div>
                        <div class="one-app__screen one-app__screen-car one-app__screen-right"></div>
                        <div class="one-app__screen one-app__screen-donuts one-app__screen-right one-app__screen-right--more"></div>
                        <div class="one-app__screen one-app__screen-blonde one-app__screen-right one-app__screen-right--more"></div>
                        <div class="one-app__screen one-app__screen-car one-app__screen-right one-app__screen-right--more"></div>
                     </div>
                     <div class="one-app__phone-footer-container">
                        <p class="one-app__phone-footer">
                           YOUR RATE: <span class="one-app__rate">$ 199.00</span>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="section-v2__content one-app__content" data-aos="fade-left" data-aos-anchor-placement="bottom-bottom">
            <div class="section-v2__title">
               Beautiful interface
            </div>
            <p>
               Lorem ipsum dolor sit amet, consectetu  adipiscing elit. Curabitur blandit nibh in arcu  dapibus luctus. Lorem ipsum dolor sit amet,  consectetur adipiscing elit.
               <br>
               dapibus luctus. Lorem ipsum dolor sit amet,  consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetu  adipiscing elit. Curabitur blandit. Lorem ipsum dolor sit amet, consectetu  adipiscing elit. Curabitur blandit nibh in arcu  dapibus luctus. Lorem ipsum dolor sit amet,  consectetur adipiscing elit.
            </p>
            <!--        <div class="button button--long section-v2__button buttonCheckCampaigns">
               Download indaHash app! <img class="button__arrow" src="/assets/images/deal_landing/arrow.svg" />
               </div> -->
         </div>
         <!-- important div -->
         <div id="counter-million" class="counter influencer-counter__box hidden">
         <div id="odometerInfluencers" class="counter__bounty" data-counter-value="965639"></div>
      </section>
      <!-- important div -->
      <section class="about-us" id="new">
         <div class="container">
         <div class="heading" data-aos="fade-down">
            <h2>About Twiva</h2>
         </div>
         <div class="row">
         <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="text-line" data-aos="fade-right">
               <h1>Learn More About Twiva</h1>
               <p>Twiva was founded by a group of small business owners who found it difficult to identify and collaborate with local influencers.
                  <br>
                  <br>
                  Twiva allows brands to collaborate with influencers with ease. Through the use of a mobile application, Twiva allows you to invite influencers to your events or businesses to help you increase sales
               </p>
            </div>
         </div>
         <div class="col-md-6 new" data-aos="fade-left">
            <img src="{{url('public/landing/images/girls.png')}}" alt="">
         </div>
      </section>
      <!-- new-home -->
      <section class="Influencer" id="new">
         <div class="container">
         <div class="new-file-top" data-aos="fade-down" data-aos-delay="300">
            <h2>FEATURED INFLUENCERS</h2>
         </div>
         <div class="heading"  data-aos="fade-down" data-aos-delay="600">
            <h2>meet some of our influencers</h2>
         </div>
         <div class="row"  data-show="2" data-aos="zoom-in-up" data-aos-offset="50" data-aos-anchor-placement="top-bottom">
            <div class="col-md-4 col-xs-6 col-sm-6">
               <img src="{{url('public/landing/images/kite1.png')}}" alt="">
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 top-new">
               <img src="{{url('public/landing/images/photo2.png')}}" alt="">
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 images-defect">
               <img src="{{url('public/landing/images/photo1.png')}}" alt="">
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 new-might">
               <img src="{{url('public/landing/images/photo4.png')}}" alt="">
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 next-photo">
               <img src="{{url('public/landing/images/photo5.png')}}" alt="">
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 last-img">
               <img src="{{url('public/landing/images/photo6.png')}}" alt="">
            </div>
         </div>
      </section>
      <section class="Download" id="new">
         <div class="container">
         <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="images-hjj"  data-aos="fade-up">
               <img src="{{url('public/landing/images/hand.png')}}" alt="">
            </div>
         </div>
         <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="new-hight" data-aos="fade-down">
               <h2>Get THE APP ON YOUR PHONE</h2>
               <p>DOWNLOAD</p>
               <a href="javascript:void(0);" class="store-app">
                  <figure class="new-right"><i class="fa fa-apple"></i></figure>
                  <div class="new-file-center"><label>DOWNLOAD ON THE<br>App Store</label></div>
               </a>
               <a href="javascript:void(0);" class="store-app black">
                  <figure class="new-right"><i class="fa fa-android"></i></figure>
                  <div class="new-file-center"><label>DOWNLOAD ON THE<br>App Store</label></div>
               </a>
            </div>
         </div>
      </section>
      <section class="footer">
         <div class="container">
         <div class="col-md-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
            </div>
         </div>
         <div class="col-md-2">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="javascript:void(0);">Home</a></li>
                  <li><a href="javascript:void(0);">Contact US</a></li>
                  <li><a href="javascript:void(0);">Privacy Policy</a></li>
                  <li><a href="javascript:void(0);">Terms and Conditions</a></li>
                  <li><a href="javascript:void(0);">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-2">
            <div class="new-good gone">
               <h2>Social Media</h2>
               <ul>
                  <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                  <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                  <li><i class="fa fa-instagram" aria-hidden="true"></i></li>
               </ul>
            </div>
         </div>
         <div class="col-md-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                     <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>
                     <p>info@twiva.com</p>
                  </li>
                  <li>
                     <figure>  <i class="fa fa-map-marker" aria-hidden="true"></i></figure>
                     <p>1680 N Vine St Suite 1112 Hollywood, CA 90028 United States</p>
                  </li>
                  <li>
                     <figure><i class="fa fa-phone" aria-hidden="true"></i></figure>
                     <p>+01-989989898</p>
                  </li>
               </ul>
            </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  2019 Twiva. All Rights Reserved.</p>
         </div>
      </section>
      <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
         <i class="fa fa-chevron-up"></i>
      </div>
      <!-- new-home -->
      </main>
      <script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
      <script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
      <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
      <script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
      <script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
      <script type="text/javascript">
         $(window).scroll(function() {
           if ($(this).scrollTop() > 200){
               $('nav.navbar').addClass("sticky");
           }
           else{
               $('nav.navbar').removeClass("sticky");
           }
         });
      </script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
             $(window).scroll(function(){
                 if ($(this).scrollTop() > 50) {
                     $('#backToTop').fadeIn('slow');
                 } else {
                     $('#backToTop').fadeOut('slow');
                 }
             });
             $('#backToTop').click(function(){
                 $("html, body").animate({ scrollTop: 0 }, 500);
                 return false;
             });
         });
      </script>
      </script>
   </body>
</html>
