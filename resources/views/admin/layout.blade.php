<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="icon" href="{{url('public/admin/production/images/Fav.png')}}" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="{{url('public/admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('public/admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url('public/admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{url('public/admin/vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <!-- Bootstrap Date-Picker Plugin -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <!-- bootstrap-progressbar -->
    <link href="{{url('public/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{url('public/admin/vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{url('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{url('public/admin/build/css/custom.min.css')}}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{url('public/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
  </head>
  <style type="text/css">
    .datepicker-orient-bottom {
    top: 460px!important;
}
  </style>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{url('admin/index')}}" class="site_title logo"><img src="{{url('public/admin/production/images/logo.png')}}" alt="logo"></a>
              <a href="{{url('admin/index')}}" class="site_title sm"><img src="{{url('public/admin/production/images/logo.png')}}" alt="logo"></a>
            </div>

                       
 <div class="clearfix"></div>
            <div class="new-heading">
              <h2>ADMIN</h2>
            </div>
                        

            <!-- sidebar menu -->
              <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="{{url('public/admin/index')}}"><i class="fa fa-home"></i>Dashboard </a></li>
                  <li class="active"><a href="{{url('public/admin/influencer-Management')}}"><i class="fa fa-user" aria-hidden="true"></i>Influencer Management</a></li>
                  <li><a href="{{url('public/admin/bussiness')}}"><i class="fa fa-globe" aria-hidden="true"></i>Business Management</a></li>
                  <li><a href="{{url('public/admin/gigs')}}"><i class="fa fa-info-circle" aria-hidden="true"></i>Gigs Management</a></li>
                    <li><a href="{{url('public/admin/plug')}}"><i class="fa fa-shield" aria-hidden="true"></i>Plugs Management</a></li>
                   <li><a href="{{url('public/admin/payments')}}"><i class="fa fa-money" aria-hidden="true"></i>Payments
                   Management</a></li>
                    <li><a href="{{url('public/admin/blog')}}"><i class="fa fa-rss-square" aria-hidden="true"></i>Blog
                   </a></li>
                     <li><a href="{{url('public/admin/featured')}}"><i class="fa fa-picture-o" aria-hidden="true"></i>Featured
                   </a></li>
                     <li><a><i class="fa fa-cog" aria-hidden="true"></i>Manage Account<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('public/admin/edit-email')}}">My Profile</a></li>
                      <li><a href="{{url('public/admin/change-password')}}">Change Password</a></li>
                    </ul>
                  </li>
                    <li><a href="{{url('public/admin/login')}}"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>

              </div>
            </div>
            <!-- /sidebar menu -->

          
            
          
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
               <ul class="nav navbar-nav navbar-right">
            
              </ul>
            </nav>
          </div>
        </div>
    

          @yield('content')


  <!-- jQuery -->
    <script src="{{url('public/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{url('public/admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{url('public/admin/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{url('public/admin/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{url('public/admin/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{url('public/admin/vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{url('public/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{url('public/admin/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{url('public/admin/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{url('public/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{url('public/admin/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{url('public/admin/vendors/DateJS/build/date.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <!-- JQVMap -->
    <script src="{{url('public/admin/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{url('public/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{url('public/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{url('public/admin/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Datatables -->
    <script src="{{url('public/admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>

  <!-- Date Picker -->
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
  <script type="text/javascript" src="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{url('public/admin/build/js/custom.min.js')}}"></script>
    <script>
      $(document).ready(function() {
      $('#datatable-filter').dataTable({
        columnDefs: [
          { targets: 'no-sort', orderable: false }
        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search..."
        },
        "bLengthChange" : false, 
           "bInfo":false, 
         "pageLength": 5,   
       });
      });
    </script>
  <script>
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>

          <script>
      $('.navbar-right .user-profile.dropdown-toggle').click(function(){
        $("body").removeClass("nav-sm").addClass('nav-md');
      });
    </script>
<script>
     $(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });
   </script>	
 @yield('js')
  </body>
</html>




