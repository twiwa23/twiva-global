<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="icon" href="{{url('public/admin/production/images/Fav.png')}}" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="{{url('public/admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <!---- claender-->
       <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Font Awesome -->
    <link href="{{url('public/admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url('public/admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{url('public/admin/vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <!-- Bootstrap Date-Picker Plugin -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <!-- bootstrap-progressbar -->
    <link href="{{url('public/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{url('public/admin/vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{url('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{url('public/admin/build/css/custom.min.css')}}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{url('public/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
  </head>
  <style type="text/css">
    .datepicker-orient-bottom {
    top: 460px!important;
}
  </style>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title logo"><img src="images/logo.png" alt="logo"></a>
              <a href="index.html" class="site_title sm"><img src="images/logo.png" alt="logo"></a>
            </div>

                       
 <div class="clearfix"></div>
            <div class="new-heading">
              <h2>ADMIN</h2>
            </div>
                        

            <!-- sidebar menu -->
              <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="{{url('public/admin/index')}}"><i class="fa fa-home"></i>Dashboard </a></li>
                  <li class="active"><a href="{{url('public/admin/influencer-Management')}}"><i class="fa fa-user" aria-hidden="true"></i>Influencer Management</a></li>
                  <li><a href="{{url('public/admin/bussiness')}}"><i class="fa fa-globe" aria-hidden="true"></i>Business Management</a></li>
                  <li><a href="{{url('public/admin/gigs')}}"><i class="fa fa-info-circle" aria-hidden="true"></i>Gigs Management</a></li>
                    <li><a href="{{url('public/admin/plug')}}"><i class="fa fa-shield" aria-hidden="true"></i>Post Management</a></li>
                 <!--   <li><a href="{{url('public/admin/payments')}}"><i class="fa fa-money" aria-hidden="true"></i>Payments
                   Management</a></li> -->
                   <li><a href="{{url('public/admin/blog')}}"><i class="fa fa-rss-square" aria-hidden="true"></i>Blog
                   </a></li>
                   
                    <li><a href="{{url('public/admin/featured')}}"><i class="fa fa-picture-o" aria-hidden="true"></i>Featured
                   </a></li>
                     <li><a href="{{url('public/admin/influencer-payment')}}"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Influencer Payment
                   </a></li>
                    <li><a href="{{url('public/admin/business-payment')}}"><i class="fa fa-money" aria-hidden="true"></i>Business Payment
                   </a></li> 
				   <li><a href="{{url('public/admin/refferal-list')}}"><i class="fa fa-picture-o" aria-hidden="true"></i>Reffral Managment
                   </a></li>
                 <li><a href="{{url('public/admin/timeline-list')}}"><i class="fa fa-picture-o" aria-hidden="true"></i>Timeline Managment
                   </a></li>
                <li><a href="{{url('admin/hashtag')}}" target="_blank"><i class="fa fa-rss-square" aria-hidden="true"></i>HashTag Post
                    </a></li>
                     <li><a><i class="fa fa-cog" aria-hidden="true"></i>Manage Account<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('public/admin/edit-email')}}">My Profile</a></li>
                      <li><a href="{{url('public/admin/change-password')}}">Change Password</a></li>
                    </ul>
                  </li>
                    <li><a href="{{url('public/admin/login')}}"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>

              </div>
            </div>
            <!-- /sidebar menu -->

          
            
          
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
               <ul class="nav navbar-nav navbar-right">
            
              </ul>
            </nav>
          </div>
        </div>
    

          @yield('content')


  <!-- jQuery -->
    <script src="{{url('public/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{url('public/admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
   

    
   
    <!-- DateJS -->
    <script src="{{url('public/admin/vendors/DateJS/build/date.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <!-- JQVMap -->
   
    
    <!-- bootstrap-daterangepicker -->
    <script src="{{url('public/admin/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Datatables -->
    <script src="{{url('public/admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>

  <!-- Date Picker -->
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
  <script type="text/javascript" src="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{url('public/admin/build/js/custom.min.js')}}"></script>
   
     <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
         <script>
      $(document).ready(function() {
      $('#datatable-filter').dataTable({
        columnDefs: [
          { targets: 'no-sort', orderable: false }
        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search..."
        },
        "bLengthChange" : false, 
           "bInfo":false, 
         "pageLength": 5,   
       });
      });
    </script>  
  
<script> 
  //var today = new Date();
  $(document).ready(function () {
      $('#datepicker-3').datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: 0,
        yearRange: '1970:2050',
        // yearRange: '1950:' + new Date().getFullYear().toString(),
       // minDate: today,
        dateFormat: 'yy-mm-dd'
      });

  });
</script>

          <script>
      $('.navbar-right .user-profile.dropdown-toggle').click(function(){
        $("body").removeClass("nav-sm").addClass('nav-md');
      });
    </script>
<script>
     $(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });
   </script>	
 @yield('js')
  </body>
</html>




