	 <?php
	 
	   $user = Auth::guard('admin')->user();
	   $user_id = $user->id;
     
		   
	 ?>
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	 	<div class="menu_section">
	 		<ul class="nav side-menu">
				<li class= <?php if(Request::is("admin/index")) {echo 'active';} else {echo '';}?> ><a href="{{ url('admin/index') }}"><i class="fa fa-home"></i> Dashboard </a></li>
	 			@if($user->type == "admin")
	 			<li class=<?php if(Request::is("admin/influencer-Management") || Request::is("admin/view-influencer/*") || Request::is("admin/edit-influencer/*"))  {echo 'active';} else {echo '';}?>> 
	 				<a href="{{ url('admin/influencer-Management') }}"><i class="fa fa-user" aria-hidden="true"></i>Influencer Management</a>
	 				</li>
	 				<li class=<?php if(Request::is("admin/business") || Request::is("admin/view-business/*") || Request::is("admin/edit-business/*") || Request::is("admin/edit-bussiness-contact/*")) {echo 'active';} else {echo '';}?>> 
	 					<a href="{{ url('admin/business')}}"><i class="fa fa-globe" aria-hidden="true"></i>Business Management</a></li>
	 					<li class=<?php if(Request::is("admin/subadmin-management") || Request::is("admin/add-subadmin")  || Request::is("admin/view-subadmin/*") || Request::is("admin/edit-subadmin/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/subadmin-management') }}"><i class="fa fa-user" aria-hidden="true"></i>Create SubAdmin </a></li>
	 						<li class=<?php if(Request::is("admin/gigs") ||Request::is("admin/add-gig-price/*")|| Request::is("admin/accepted-gig-list")|| Request::is("admin/completed-gig-list") || Request::is("admin/view-gigs/*")) {echo 'active';} else {echo '';}?>> 
	 						<a href="{{ url('admin/gigs') }}"> 
	 							<i class="fa fa-info-circle" aria-hidden="true"></i>Gigs Management</a>
	 							</li>
							<li class=<?php if(Request::is("admin/plug") ||Request::is("admin/accepted-post-list") ||Request::is("admin/add-post-price/*")||Request::is("admin/completed-post-list")|| Request::is("admin/view-plug/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/plug') }}"><i class="fa fa-shield" aria-hidden="true"></i>Post Management</a>
	 							</li>
							
									<li class=<?php if(Request::is("admin/blog") || Request::is("admin/add-blog") || Request::is("admin/view-blog/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/blog') }}"><i class="fa fa-rss-square" aria-hidden="true"></i>Blog </a></li>
								<li class=<?php if(Request::is("admin/featured") || Request::is("admin/add-featured")  || Request::is("admin/view-featured/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/featured') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;Featured </a></li>
								<li class=<?php if(Request::is("admin/interests") || Request::is("admin/add-interests")  || Request::is("admin/view-interests/*") || Request::is("admin/edit-interests/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/interests') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;Interests </a></li>
	 							<li class=<?php if(Request::is("admin/influencer-payment") || Request::is("admin/view-influencer-payment/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/influencer-payment') }}"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Influencer Payment</a>
	 							</li>
	 							<li class=<?php if(Request::is("admin/business-payment") || Request::is("admin/view-business-payment/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/business-payment') }}"><i class="fa fa-money" aria-hidden="true"></i>  Business Payment</a>
	 							</li>
								<li class=<?php if(Request::is("admin/refferal-list") || Request::is("admin/view-refferal/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/refferal-list') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>  Refferal Management</a>
	 							</li>
	 								<li class=<?php if(Request::is("admin/twiva-updates-list") || Request::is("admin/view-twiva-updates-list/*") || Request::is("admin/add-twiva-updates")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/twiva-updates-list') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>  Twiva Updates</a>
	 							</li>
	 									<li class=<?php if(Request::is("admin/creators-Management") || Request::is("admin/view-creators/*") || Request::is("admin/portfolio-management/*")|| Request::is("admin/services-management/*") || Request::is("admin/rate-management/*") || Request::is("admin/blogger-management/*") || Request::is("admin/view-portfolio/*") || Request::is("admin/view-services/*") || Request::is("admin/view-blogger/*") || Request::is("admin/view-rate/*") ) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/creators-Management') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>  Creators Managment</a>
	 							</li>
                                <li><a href="{{url('admin/hashtag')}}" target="_blank"><i class="fa fa-rss-square" aria-hidden="true"></i>HashTag Post
                               </a></li>
								<li><a><i class="fa fa-cog" aria-hidden="true"></i>Manage Account <span class="fa fa-chevron-down"></span></a>
	 								<ul class="nav child_menu">
	 									 <li><a href="{{url('admin/edit-email')}}">My Profile</a></li>
                                       <li><a href="{{url('admin/change-password')}}">Change Password</a></li>
	 								</ul>
	 							</li>
								@else
									<?php
								 $sidebarData = DB::table('sidebars')
								   ->select('sidebars.name as sidebarName','sidebars.id as sidebarId')
									->join('subadmin_permissions', 'subadmin_permissions.sidebar_id', '=', 'sidebars.id')
									->where("subadmin_permissions.user_id",$user_id)
									 ->get();
									 foreach($sidebarData as $sidebarIteam){
										 
									 
								?>
								@if($sidebarIteam->sidebarName == "Influencer Management")
									<li class=<?php if(Request::is("admin/influencer-Management") || Request::is("admin/view-influencer/*") || Request::is("admin/edit-influencer/*"))  {echo 'active';} else {echo '';}?>> 
										<a href="{{ url('admin/influencer-Management') }}"><i class="fa fa-user" aria-hidden="true"></i>Influencer Management</a>
										</li>
										@endif
								@if($sidebarIteam->sidebarName == "Business Management")
									<li class=<?php if(Request::is("admin/business") || Request::is("admin/view-business/*") || Request::is("admin/edit-business/*") || Request::is("admin/edit-bussiness-contact/*")) {echo 'active';} else {echo '';}?>> 
	 					         <a href="{{ url('admin/business')}}"><i class="fa fa-globe" aria-hidden="true"></i>Business Management</a></li>
										@endif
								@if($sidebarIteam->sidebarName == "Gigs Management")
										<li class=<?php if(Request::is("admin/gigs") || Request::is("admin/accepted-gig-list")|| Request::is("admin/completed-gig-list") || Request::is("admin/view-gigs/*")) {echo 'active';} else {echo '';}?>> 
	 						         <a href="{{ url('admin/gigs') }}"> 
	 							    <i class="fa fa-info-circle" aria-hidden="true"></i>Gigs Management</a>
	 							</li>
										@endif
								@if($sidebarIteam->sidebarName == "Post Management")
									<li class=<?php if(Request::is("admin/plug") ||Request::is("admin/accepted-post-list")||Request::is("admin/completed-post-list")|| Request::is("admin/view-plug/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/plug') }}"><i class="fa fa-shield" aria-hidden="true"></i>Post Management</a>
	 							</li>
										@endif
							    @if($sidebarIteam->sidebarName == "Blog")
									<li class=<?php if(Request::is("admin/blog") || Request::is("admin/add-blog") || Request::is("admin/view-blog/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/blog') }}"><i class="fa fa-rss-square" aria-hidden="true"></i>Blog </a></li>
										@endif
							    @if($sidebarIteam->sidebarName == "Featured")
									<li class=<?php if(Request::is("admin/featured") || Request::is("admin/add-featured")  || Request::is("admin/view-featured/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/featured') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;Featured </a></li>
								<li class=<?php if(Request::is("admin/interests") || Request::is("admin/add-interests")  || Request::is("admin/view-interests/*") || Request::is("admin/edit-interests/*")) {echo 'active';} else {echo '';}?>> 
										@endif
								@if($sidebarIteam->sidebarName == "Interests")
									<li class=<?php if(Request::is("admin/interests") || Request::is("admin/add-interests")  || Request::is("admin/view-interests/*") || Request::is("admin/edit-interests/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/interests') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;Interests </a></li>
										@endif
								@if($sidebarIteam->sidebarName == "Influencer Payment")
									<li class=<?php if(Request::is("admin/influencer-payment") || Request::is("admin/view-influencer-payment/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/influencer-payment') }}"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Influencer Payment</a>
	 							</li>
										@endif
								@if($sidebarIteam->sidebarName == "Business Payment")
									<li class=<?php if(Request::is("admin/business-payment") || Request::is("admin/view-business-payment/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/business-payment') }}"><i class="fa fa-money" aria-hidden="true"></i> Business Payment</a>
	 							</li>
										@endif
								@if($sidebarIteam->sidebarName == "Refferal Management")
									<li class=<?php if(Request::is("admin/refferal-list") || Request::is("admin/view-refferal/*")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/refferal-list') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>  Refferal Management</a>
	 							</li>
										@endif
								@if($sidebarIteam->sidebarName == "Twiva Updates")
									<li class=<?php if(Request::is("admin/twiva-updates-list") || Request::is("admin/view-twiva-updates-list/*") || Request::is("admin/add-twiva-updates")) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/twiva-updates-list') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>  Twiva Updates</a>
	 							</li>
								     @endif
								     	@if($sidebarIteam->sidebarName == "Creators Management")
								     	<li class=<?php if(Request::is("admin/creators-Management") || Request::is("admin/view-creators/*") || Request::is("admin/portfolio-management/*")|| Request::is("admin/services-management/*") || Request::is("admin/rate-management/*") || Request::is("admin/blogger-management/*") || Request::is("admin/view-portfolio/*") || Request::is("admin/view-services/*") || Request::is("admin/view-blogger/*") || Request::is("admin/view-rate/*") ) {echo 'active';} else {echo '';}?>> 
	 							<a href="{{ url('admin/creators-Management') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>  Creators Managment</a>
	 							</li>
	 							 @endif
									 <?php } ?>
		                         @endif
	 							

	 							<li><a href="{{url('admin/logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>	
	 						</ul>

              </div>
            </div>
   	
