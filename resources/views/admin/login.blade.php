<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Login</title>
<link rel="icon" href="{{url('public/admin/production/images/Fav.png')}}" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="{{url('public/admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('public/admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url('public/admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{url('public/admin/vendors/animate.css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{url('public/admin/build/css/custom.min.css')}}" rel="stylesheet">
  </head>
<style type="text/css">
  a.btn.btn-success.submit {
    margin-top: 34px;
}
.login_content {
    margin: 38px auto;
    }
    .logo-wrapper img {
    max-width: 114px;
}
</style>
  <body class="login">
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
           <form method="post">
		    {{csrf_field()}}  
                <div class="logo-wrapper"><img src="{{url('public/admin/production/images/logo.png')}}" alt="Callz"/></div>
                <h1><span>Admin Login</span></h1>
				 @include('admin.layouts.notifications')
              <div>
                <input type="text" name = "email" value = "" class="form-control" placeholder="Email"  />
				 <span class="text-danger error">{{$errors->first('email')}}</span>
              </div>
              <div>
                <input type="password" name = "password" value = "" class="form-control" placeholder="Password" />
				 <span class="text-danger error">{{$errors->first('password')}}</span>
              </div>

              <div>
                  <p class="forget">
                     <a  href="{{url('admin/forgot-password')}}" class="reset_pass to_register">Forgot password?</a>
                  </p>                     
                  <!--<a href="{{url('admin/index')}}" class="btn btn-success submit">Login</a>-->
				   <button type="submit" class="btn btn-success submit">Login</button>
              </div>
            </div>

     
   

    <script src="{{url('public/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
      <!-- jQuery -->
    <script src="{{url('public/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{url('public/admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{url('public/admin/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{url('public/admin/vendors/nprogress/nprogress.js')}}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Chart.js -->
    <script src="{{url('public/admin/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{url('public/admin/vendors/gauge.js/dist/gauge.min.js')}}"></script>

    <!-- bootstrap-progressbar -->
    <script src="{{url('public/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{url('public/admin/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{url('public/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{url('public/admin/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{url('public/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{url('public/admin/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{url('public/admin/vendors/DateJS/build/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{url('public/admin/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{url('public/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{url('public/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{url('public/admin/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Datatables -->
    <script src="{{url('public/admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{url('public/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>

    <script>
      $('.navbar-right .user-profile.dropdown-toggle').click(function(){
        $("body").removeClass("nav-sm").addClass('nav-md');
      });
    </script>

	  <script type="text/javascript">
    $(function() {
      setTimeout(function(){
          $(".alertz").hide();
      }, 3000);
    });
    </script>
  </body>
</html>
