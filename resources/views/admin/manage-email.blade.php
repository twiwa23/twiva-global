@extends('admin.layouts.app')
@section('title', ' Edit Profile')
@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Edit  Profile</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                   <li><a href="javascript:void(0);">Manage Account</a></li>
                    <li><a href="{{url('admin/edit-email')}}">My Profile</a></li>
                  <li>Edit</li>
                </ul>
                 @include('admin.layouts.notifications')
                  <div class="x_content">
                    <form method = "post" action = "">
                       {{csrf_field()}}
                    <table class="table table-striped table-bordered custom-table new">
                        <tr>
                        	<td style="width:200px"> Email Address</td>
                            <td><input type="text" name = "email" value = "{{$users->email}}" class="form-control" placeholder=""/>
                            <span class="text-danger">{{$errors->first('email')}}</span>
                            </td>
                              
                        </tr>
                        <!--   <tr>
                          <td style="width:200px"> Phone Number</td>
                            <td><input type="text" name = "phone_number" value = "{{$users->phone_number}}" class="form-control" placeholder=""/>
                            <span class="text-danger">{{$errors->first('phone_number')}}</span>
                            </td>
                              
                        </tr> -->
                          <td></td>
                            <td><input type = "submit" name = "Update" value = "Update" class="btn btn-primary center"></td>
                      
                    </table>
                  </form>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->



   @endsection