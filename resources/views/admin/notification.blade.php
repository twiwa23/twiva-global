@extends('admin.layouts.app')
@section('title', ' Notifications')
@section('content')

  <style type="text/css">
    .custom-table td textarea {
 height: 191px;
    resize: none;
    width: 401px;

}
.btn.btn-primary.center {
    margin-left: 12.2%;
}
.btn.btn-primary.center {
    margin-left: 20.2%;
}
  </style>

 

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Notifications</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li>Notifications</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table new">
                        <tr>
                        	<td style="width:200px" class="first">Message</td>
                            <td><textarea placeholder="lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in five centuries, but also the leap into electronic  five centuries, but also the leap into electronic"></textarea></td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td><a href="javascript:void(0);" class="btn btn-primary center">Send</button></td>
                        </tr>
                    </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->



   @endsection