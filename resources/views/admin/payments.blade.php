@extends('admin.layouts.app')
@section('title', ' Payments Management')
@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">
                <h3>Payments  Management</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li>Payments  Management</li>
                </ul>

                   <div class="x_content">
                    <table id="datatable-filter" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                            <th class="">Business Name</th> 
                            <th>Email Address</th>
                            <th>Transaction Id</th>
                            <th>Amount</th>
                            <th>Date</th>                         
                        </tr>
                      </thead>
                      <tbody>
                           @php ($i=1)
                        @foreach($payments as $payment)
                        <tr>
                          <td>{{$i++}}</td> 
                          <td>@if(!empty($payment->user->name)){{$payment->user->name}} @else N/A @endif</td>
                          <td>@if(!empty($payment->user->email)){{$payment->user->email}} @else N/A @endif</td>
                           <td>N/A</td>
                          <td>@if(!empty($payment->amount))${{$payment->amount}} @else N/A @endif</td>
                          <td>@if(!empty($payment->created_at)){{date("Y-m-d", strtotime($payment->created_at))}} @else N/A @endif</td>
                        </tr>
                          @endforeach  
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                </div>
               </div>
            </div>
          </div>

     
        <!-- /page content -->



    @endsection