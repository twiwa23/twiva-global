@extends('admin.layouts.app')
@section('title', ' Portfolio Management')
@section('content')
<style>
td.sequence img {
    margin-right: 13px;
    height: 98px;
    width: 115px;
    object-fit: cover;
}
video {
    width: 116px;
    height: 94px;
    background-color: #000;
}
</style>
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Portfolio Management</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                   <li><a href="{{url('admin/view-creators/'.$id)}}">Creators Details</a></li>
                  <li>Portfolio Management</li>
                </ul>
              
                   @include('admin.layouts.notifications')
                  <div class="x_content">
                    <table id="datatable-filter" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                            <th class="no-sort">Image</th>                         
                         
                            <th class="no-sort sort">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      
                       @php ($i=1)
                        @foreach($portfolio as $portfolios)
                         <tr>
                         <td>{{$i++}}</td> 
                         @if(!empty($portfolios->profile))
                           <td class="sequence">
                            
                            <?php 
                           
                            $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                           
                            
                            <?php
                           
                            $get_file = $portfolios->profile;
              
                            $has_video = false;
                            $has_file = false;
                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                            $get_file_type = strtolower($get_file_type);

                            if(in_array($get_file_type,$video_ext)){
                              echo '<div class="new"><video src="'.$get_file.'" controls="" "></video></div>';
                            }else {
                              echo '<div class="new"><img src="'.$get_file.'" "/></div>';
                            }
                             ?>
                           </td>
                           @else
                            <td>
                             <img src="{{url('public/admin/production/images/user.png')}}" alt="blank_" style="width: 100px;height: 100px; object-fit: cover;">
                            </td>
                            @endif

                           <td class = "text-center"> 
                            <span><a href="{{url('admin/view-portfolio/'.$portfolios->id)}}" class="btn btn-success">View</a>
                                <a type="button" data-toggle="modal" id="{{$portfolios->id}}" data-target="#myModal" class="btn btn-danger del">Delete</a>
                               </span>
                         </td>
                        </tr>  
                      @endforeach
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                </div>
               </div>
              </div>
            </div>
          </div>

     
        <!-- /page content -->


<div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
     <!-- Modal content-->
     <div class="modal-content">
        <div class="modal-header">
         <!--   <button type="button" class="close" data-dismiss="modal">&times;</button> -->
           <h4 class="modal-title" style="text-align: center;" ><b>Alert</b></h4>
        </div>
        <div class="modal-body" style="text-align: center; padding-bottom: 0px; margin: 0px 0 26px;">
          <p><strong>Are you sure you want to delete this portfolio ?</strong></p>
          <form id="del" action="{{url('admin/delete-portfolio')}}" method="post">
          {{ csrf_field() }}
          <input id = "portfolio_id" name = "portfolio_id" type=  "hidden">
          </form>
        </div>
        <div class="modal-footer" style="text-align: center;">
        <button type="button" onclick="$('#del').submit()" class="btn btn-danger home dele" id="modal-btn-si">Delete</button>
           <!--  <a href="@if(!empty($user->id)) {{url('admin/delete-user',$user->id)}}@else 'N/A' @endif" class="btn btn-danger home dele"> Delete</a> -->
          <button type="button" class="btn btn-danger home dele" data-dismiss="modal">Cancel</button>
        </div>
     </div>                                       
   </div>
</div>
    @endsection

    @section('js')
  <script type="text/javascript">
    $('.del').bind('click', function() {
      var i = $(this).attr('id');
      $('#portfolio_id').val(i);
    });

  </script>
 <script type="text/javascript">
    $(function() {
      setTimeout(function(){
          $(".alertz").hide();
      }, 3000);
    });
    </script>
@endsection
