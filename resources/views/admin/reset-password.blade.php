<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Forgot Password </title>
<link rel="icon" href="{{url('public/admin/production/images/Fav.png')}}" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="{{url('public/admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('public/admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url('public/admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{url('public/admin/vendors/animate.css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{url('public/admin/build/css/custom.min.css')}}" rel="stylesheet">
  </head>
<style type="text/css">
      .logo-wrapper img {
    max-width: 114px;
}
</style>
  <body class="login">
	
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post">
		    {{csrf_field()}}  
              <div class="logo-wrapper"><img src="{{url('public/admin/production/images/logo.png')}}" alt="Callz"/></div>
              <h1><span>Forgot Password</span></h1>
			   @include('admin.layouts.notifications')
              <div>
                <input type="password" name = "password" value = "" class="form-control" placeholder="New Password"/>
                <span class="text-danger error">{{$errors->first('password')}}</span>
              </div>
              <div>
               <input type="password" class="form-control" name= "password_confirmation" placeholder="Confirm Password"/>
                <span class="text-danger error">{{$errors->first('password_confirmation')}}</span>
              </div>
              <div>
               <!-- <a class="btn btn-success submit" href="login.html">Send Forgot Password Email</a>-->
        <button type = "submit" class="btn btn-success submit">Submit</button>
              </div>

            </form>
          </section>
        </div>
      </div>

 <script src="{{url('public/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script>
      $('.navbar-right .user-profile.dropdown-toggle').click(function(){
        $("body").removeClass("nav-sm").addClass('nav-md');
      });
    </script>
  </body>
</html>
