@extends('admin.layouts.app')
@section('title', ' Influencer Details')
@section('content')
<style>
ul, li {
    list-style: inherit;
}
.new-new {
    margin-left: 14px;
}
</style>
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Blog Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/blog')}}">Blog</a></li>
                  <li>Blog Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                           <td style="width:200px">Blog Image</td>
                            <td>
                          
                            @if($blog->image)
                            <img src="{{$blog->image}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                           
                           </td>    
                        </tr>
                         <tr>
                          <td>Blog Title</td>
                          <td>@if(!empty($blog->title)){{$blog->title}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td>Blog text </td>
                      <td>
					  <div class = "new-new">
                          <span> 
                      {!!html_entity_decode($blog->text)!!}
                     </span>
					 </div>
                      </td>
                        </tr>
                        
                         
                         
                     
                        </tr>
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection