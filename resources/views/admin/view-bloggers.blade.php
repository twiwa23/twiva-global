@extends('admin.layouts.app')
@section('title', ' Bloggers Details')
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Bloggers Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/blogger-management/'.$blogger->user_id)}}">Bloggers Management</a></li>
                  <li>Bloggers Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                      
                         <tr>
                          <td width = "100">Text</td>
                          <td>@if(!empty($blogger->description)){{$blogger->description}} @else N/A @endif</td>    
                        </tr>
                          
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection