@extends('admin.layouts.app')
@section('title', 'View Business Payment')
@section('content')
<style type="text/css">
  input#select_all{
      display: inline-block;
    margin-top: 0;
    margin-bottom: 0;
    vertical-align: middle;
    width: 16px;
    height: 16px;
}
input.checkbox.single_check {
    width: 16px;
    height: 16px;
}
</style>

        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>View Business Payment</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                @include('admin.layouts.notifications')
                <div class="x_panel">

                  <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li>View Business Payment</li>
                </ul>
                    <form method="POST" class="pay_form">
                  {{@csrf_field()}}
                <input type="hidden" name="put_value_select" id="put_value_select">
                 <button type="submit" class="btn btn-primary">Pay</button>
               </form>
                  <div class="x_content">
                    <table id="datatable-filter1" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
                              <th class="">Name</th> 
                                 <th class="">Type</th>
                                   <th class="">Price</th>                         
                            <th class="no-sort sort"><input type="checkbox" class="checkbox" id="select_all"/> Pay All</th>
                        </tr>
                      </thead>

                      <tbody>
                         @php ($i=1)
                        @foreach($busienssDetail as $businessDetails)
                         <tr>
                         <td>{{$i++}}</td> 
                          <td>@if(!empty($businessDetails->name)){{$businessDetails->name}} @else N/A @endif</td> 
                           <td>@if(!empty($businessDetails->type)){{$businessDetails->type}} @else N/A @endif</td>
                            <td>@if(!empty($businessDetails->amount)){{$businessDetails->amount}} @else N/A @endif</td>
                        <td><span> <input class="checkbox single_check" value="{{$businessDetails->id}}" type="checkbox" name="check[]"></span></td> 
                        </tr>
                        </tr>
                        @endforeach 
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                </div>
               </div>
              </div>
            </div>
          </div>

     
        <!-- /page content -->
@endsection

@section('js')

<script>
      $(document).ready(function() {

        //DataTables_datatable-filter_/projects/patrick/admin/user-export-csv/MQ==

        var oTable = $('#datatable-filter1').dataTable({
          stateSave: true,
          columnDefs: [
          { targets: 'no-sort', orderable: false }
        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search..."
        },
        "bLengthChange" : false, 
           "bInfo":false, 
         "pageLength": 5
        });

        var allPages = oTable.fnGetNodes();

        $('input[type="checkbox"]', allPages).prop('checked', false);
        $('#put_value_select').val('');
        $('#select_all').prop('checked',false);

        $('body').on('click', '#select_all', function () {

            var check_select = [];
            if ($(this).hasClass('checkbox')) {
                $('input[type="checkbox"]', allPages).prop('checked', true);

            $.each($('input[type="checkbox"]:checked',allPages), function(){
              check_select.push($(this).val());
            });

            } else {
                $('input[type="checkbox"]', allPages).prop('checked', false);
            }
            $(this).toggleClass('checkbox');

            $('#put_value_select').val(check_select);
        })


        $('body').on('click', '.single_check', function () {

            var single_select = [];

            $.each($('input[type="checkbox"]:checked',allPages), function(){
              single_select.push($(this).val());
            });

            $('#put_value_select').val(single_select);

            
        })



      });
    </script> 
    <script>
      
     $(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });
         
   </script>

   <script>
      
     $(".alert-danger").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-danger").slideUp(500);
    });
         
   </script>

@endsection()