@extends('admin.layouts.app')
@section('title', ' Business Details')
@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Business Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/business')}}">Business Management</a></li>
                  <li>View</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                          <td style="width:200px">Profile Picture</td>
                          <td>
                            @if(!empty($businessDetails->profile))
                            
                              <img src="{{$businessDetails->profile}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                         </td>
                        </tr>
                         <tr>
                          <td>Business Name</td>
                          <td>@if(!empty($businessDetails->name)){{$businessDetails->name}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Email Address</td>
                          <td>@if(!empty($businessDetails->email)){{$businessDetails->email}} @else N/A @endif</td>
                           
                        </tr>
                         <tr>
                          <td>Phone Number</td>
                          <td>@if(!empty($businessDetails->phone_number)){{$businessDetails->phone_number}} @else N/A @endif</td>
						             </tr>
                            <tr>
                          <td>Business Description </td>
                          <td><textarea class="form-control" disabled="">@if(!empty($businessDetails->business_detail)){{$businessDetails->businessDetail->business_detail}} @else N/A @endif</textarea></td>  
                        </tr>
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->



    @endsection
