@extends('admin.layouts.app')
@section('title', ' Creators Details')
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Creators Details</h3>
                <a href = "{{url('admin/portfolio-management/'.$creator->id)}}" class = "btn btn-success">Portfolio Managmement</a>
                <a href = "{{url('admin/blogger-management/'.$creator->id)}}" class = "btn btn-success">Blogger Managmement</a>
                <a href = "{{url('admin/services-management/'.$creator->id)}}" class = "btn btn-success">Services Managmement</a>
                <a href = "{{url('admin/rate-management/'.$creator->id)}}" class = "btn btn-success">Rate Managmement</a>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/creators-Management')}}">Creators Management</a></li>
                  <li>Creators Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                           <td style="width:200px">Profile Image</td>
                            <td>
                            @if($creator->profile)
                            <img src="{{$creator->profile}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                            
                           </td>    
                        </tr>
                        <tr>
                           <td style="width:200px">Cover Image</td>
                            <td>
                            @if($creator->background_image)
                            <img src="{{$creator->background_image}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                            
                           </td>    
                        </tr>
                         <tr>
                          <td>Username</td>
                          <td>@if(!empty($creator->name)){{$creator->name}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td>Email Address </td>
                          <td>@if(!empty($creator->email)){{$creator->email}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Phone Number </td>
                          <td>@if(!empty($creator->phone_number)){{$creator->phone_number}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Category</td>
                          <td>@if(!empty($category->name)){{$category->name}} @else N/A @endif</td> 
                         
                        </tr>
                         <tr>
                          <td>Description</td> 
                          <td>   
                          @if(!empty($creator->description)){{$creator->description}} @else N/A @endif
                          
                           </td>
                        </tr>
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection