@extends('admin.layouts.app')
@section('title', ' Gigs Details')
@section('content')

        <!-- /top navigation -->
    <style type="text/css">
    a.btn.btn-primary.center.google {
    float: right;
    margin-top: 12px;
    }
    ul.breadcrum {
    display: inline-block;
    }

    .new {
      width: 22%;
      display: inline-block;
      margin-right: 17px;
      margin-bottom: 18px;
      position: relative;
      vertical-align: top;


      }
      .new video {
      width: 100%;
      height: 120px;
      background-color: #000;
      }
      td.sequence img {
    margin-right: 13px;
    width: 100%;
    height: 120px;
    object-fit: cover;
}
table#datatable-filter tr th:nth-child(4)
{
      width: 27% !important;
}
  
     </style>
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Gigs Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/gigs')}}">Gigs Management</a></li>
                  <li>Gigs Details</li>
                </ul>
                 @include('admin.layouts.notifications')
               @php $userDetail = $gigDetail; @endphp
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                          <td style="width:200px">Business Image/Logo</td>
                          <td>
                            @if(!empty($busienssDetail->profile))
                            
                              <img src="{{$busienssDetail->profile}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                         </td>
                        </tr>
                         <tr>
                          <td>Business Name</td>
                          <td>@if(!empty($busienssDetail->name)){{$busienssDetail->name}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Gig Name </td>
                          <td>@if(!empty($gigDetail->gig_name)){{$gigDetail->gig_name}} @else N/A @endif</td>  
                        </tr>
                          <tr>
                          <td>Gig  Location</td>
                          <td>@if(!empty($gigDetail->location)){{$gigDetail->location}} @else N/A @endif</td>
                           
                        </tr>
                         <tr>
                          <td>Gig Start Date</td>
                          <td>@if(!empty($gigDetail->gig_start_date_time)){{date("Y-m-d", strtotime($gigDetail->gig_start_date_time))}} @else N/A @endif</td>
                           
                        </tr>
                        <tr>
                          <td>Gig End Date</td>
                          <td>@if(!empty($gigDetail->gig_end_date_time)){{date("Y-m-d", strtotime($gigDetail->gig_end_date_time))}} @else N/A @endif</td>
                           
                        </tr>
                     
                         <tr>
                          <td>Gig Time</td>
                          <td>@if(!empty($gigDetail->gig_start_date_time)){{date("G:i", strtotime($gigDetail->gig_start_date_time))}} @else N/A @endif</td> 
                        </tr>
                       
                         <tr>
                          <td>HashTag </td>
                          <td>@if(!empty($gigDetail->hashtag)){{$gigDetail->hashtag}} @else N/A @endif</td>  
                        </tr>
                        <tr>
                          <td>What to do </td>
                          <td>@if(!empty($gigDetail->what_to_do)){{$gigDetail->what_to_do}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Gig Theme/Photo/Video </td>
                     
                           <td class="sequence">
                            <?php $videos = $All_files = "";
                            $no_doc = 0;
                            $total = 0;
                            $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                            @forelse($gigImages as $key => $files)
                            <?php
                            $total++;
                            $get_file = $files->media;
                            $has_video = false;
                            $has_file = false;
                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                            $get_file_type = strtolower($get_file_type);
                           if(in_array($get_file_type,$video_ext)){
                              echo '<div class="new"><video src="'.$get_file.'" controls="" "></video></div>';
                            }else {
                              echo '<div class="new"><img src="'.$get_file.'" "/></div>';
                            }
                             ?>
                            @empty
                             <img src="{{url('public/admin/production/images/user.png')}}" alt="blank_" style="width: 100px;height: 100px; object-fit: cover;">
                            @endforelse
                           </td>

                        </tr>
                         <tr>
                          <td>Gig Price</td>
                          <td>@if(!empty($gigDetail->price_per_gig)){{$gigDetail->price_per_gig}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Gig Complementary Items</td>
                          <td><textarea class="form-control" disabled="">@if(!empty($gigDetail->gig_complementry_item)){{$gigDetail->gig_complementry_item}} @else N/A @endif</textarea></td>  
                        </tr>
                       <tr>
                         <td>Contact Person Image</td>
                         <td> @if(!empty($gigDetail->profile))
                           <img src="{{$gigDetail->profile}}" alt="User" style="width: 50px;height: 50px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif</td>
                           </tr>
                            <tr>
                           <td>Contact Person Name</td>
                           <td>{{$gigDetail->owner_name ?: "N/A"}}</td>
                         </tr>
                         <tr>
                            <td>Contact Person Number</td>
                             <td>{{$gigDetail->phone_number ?: "N/A"}}</td>
                           </tr>
                         <tr>
                          <td>Gig Description</td>
                          <td><textarea class="form-control" disabled="">@if(!empty($gigDetail->description)){{$gigDetail->description}} @else N/A @endif</textarea></td>  
                        </tr>
                         <tr>
                          <td>Business Description</td>
                          <td><textarea class="form-control" disabled="">@if(!empty($businessData->business_detail)){{$businessData->business_detail}} @else N/A @endif </textarea></td>  
                        </tr>
                          
                         </table>
                         <a href = "{{url('admin/add-gig-price/'.$gigDetail->id)}}" class="btn btn-primary pull-right">Add Influencer</a>
                
                
                  <h2 style = "margin-bottom: 40px; position: relative; top: 37px;">Update Price User List</h2>
                  
                  <table id="datatable-filter1" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>User Name</th> 
                            <th>Phone Number</th>
                            <th>Status</th>  
                            <th>Current Price</th>
                             <th>Updated Price</th> 
                              <th class="no-sort sort">Action</th>                         
                        </tr>
                      </thead>

                       <tbody>
                         @php ($i=1)
                        @foreach($gigPrice as $prices)
                        <tr>
                          <form method="POST" class="" style="position: relative; float: left;
                       width: 100%;">
                         {{@csrf_field()}}
                          <td>{{$i++}}</td>
                         <td>
                       @if(!empty($prices->username)){{$prices->username}} @else N/A @endif
                          </td>  
                           <td>
                         @if(!empty($prices->phone_number)){{$prices->phone_number}} @else N/A @endif
                          </td>
                          @if(!empty($prices->gig_status))
                           <td>@if($prices->gig_status==5)Pending @elseif($prices->gig_status==1) Accepted 
                          @elseif($prices->gig_status==3) Accepted  @elseif($prices->gig_status==4) Completed @elseif($prices->gig_status==2) Rejected @else N/A @endif</td>
                           @else
                          <td>N/A</td>
                          @endif 
                          <td>@if(!empty($prices->price)){{$prices->price}} @else N/A @endif</td>
                          @if($prices->gig_status==2)
                          <td><span> 
                            <input type = "text" class = "form-control" onkeypress="return isNumberKey(event)" name="price" maxlength="5">
                             <input type = "hidden" class = "form-control"  name="user_id" value = "{{$prices->infulancer_id}}">
                          </span>
                          </td>

                        <td><button type="submit" class="btn btn-primary">Resend</button>
                        </td>
                         @else
                        <td>N/A</td>
                        <td>N/A</td>
                        @endif
                      </form>
                        </tr>
                          @endforeach  
                      </tbody>
                    </table>
                   
              </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
     
        <!-- /page content -->


@endsection

 @section('js')
   <script>
      $(document).ready(function() {
      $('#datatable-filter1').dataTable({
        columnDefs: [
          { targets: 'no-sort', orderable: false }
        ],
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search..."
        },
        "bLengthChange" : false, 
           "bInfo":false, 
         "pageLength": 10,   
       });
      });
    </script>

   <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       //-->
    </SCRIPT>
    <script>
      
     $(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });
         
   </script>

   <script>
      
     $(".alert-danger").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-danger").slideUp(500);
    });
         
   </script>

@endsection()