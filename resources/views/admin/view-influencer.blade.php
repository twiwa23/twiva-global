@extends('admin.layouts.app')
@section('title', ' Influencer Details')
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Influencer Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/influencer-Management')}}">Influencer Management</a></li>
                  <li>Influencer Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                           <td style="width:200px">Image</td>
                            <td>
                            @foreach($infulancerImageData as $ImageData)
                            @if($ImageData->image)
                            <img src="{{$ImageData->image}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                              @endforeach
                           </td>    
                        </tr>
                         <tr>
                          <td>Full Name</td>
                          <td>@if(!empty($user->name)){{$user->name}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td>Email Address </td>
                          <td>@if(!empty($user->email)){{$user->email}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Phone Number </td>
                          <td>@if(!empty($user->phone_number)){{$user->phone_number}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Desired Post Price</td>
                          <td>@if(!empty($InfulancerPrice->plug_price)){{$InfulancerPrice->plug_price}} @else N/A @endif</td>
                           
                        </tr>
                         <tr>
                          <td>Average Post Price</td>
                          <?php 
                           $averagePlugPrice = round($averagePostPrice->averagePrice, 2); ?>
                          <td>@if(!empty($averagePlugPrice)){{$averagePlugPrice}} @else N/A @endif</td> 
                        </tr>
                         <tr>
                          <td>Desired Gig Price</td>
                          <td>@if(!empty($InfulancerPrice->gig_price)){{$InfulancerPrice->gig_price}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Average Gig Price </td>
                            <?php 
                           $averageGigPrice = round($averageGigPrice->averagePrice, 2); ?>
                          <td>
                            @if(!empty($averageGigPrice)){{$averageGigPrice}} @else N/A @endif</td>  
                        </tr>
                        <tr>
                          <td>Date Of Birth</td>
                          <td>@if(!empty($user->InfluencerDetail->dob)){{$user->InfluencerDetail->dob}} @else N/A @endif</td>  
                        </tr>
                          <tr>
                          <td>Gender</td>
                          @if(!empty($user->InfluencerDetail))
                          <td>@if($user->InfluencerDetail->gender==1)Male @elseif($user->InfluencerDetail->gender==2) Female @else N/A @endif</td> 
                          @else
                          <td>N/A</td>
                          @endif 
                        </tr>
                         <tr>
                          <td>Facebook Username</td>
                          <td>@if(!empty($user->SocialDetail->facebook_name)){{$user->SocialDetail->facebook_name}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Twitter Username</td>
                          <td>@if(!empty($user->SocialDetail->twitter_name)){{$user->SocialDetail->twitter_name}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Instagram Username</td>
                          <td>@if(!empty($user->SocialDetail->instagram_name)){{$user->SocialDetail->instagram_name}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Facebook Friends</td>
                          <td>@if(!empty($user->SocialDetail->facebook_friends)){{$user->SocialDetail->facebook_friends}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                          <td>Twitter Followers</td>
                          <td>@if(!empty($user->SocialDetail->twitter_friends)){{$user->SocialDetail->twitter_friends}} @else N/A @endif</td>  
                        </tr>
                          <tr>
                          <td>Instagram Followers</td>
                          <td>@if(!empty($user->SocialDetail->instagram_friends)){{$user->SocialDetail->instagram_friends}} @else N/A @endif</td>  
                        </tr>
                         <tr>
                            <td>Interests</td>
                                <td>
                                    @forelse($user->interests as $interestDatas)
                                        {{$loop->first ? '' : ', ' }}
                                            @if(!empty($interestDatas->interest_name)){{$interestDatas->interest_name}}  @endif
                                                   @empty
                                                    N/A
                                            @endforelse
                                </td>
                        </tr>
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection
