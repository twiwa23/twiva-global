@extends('admin.layouts.app')
@section('title', ' Portfolio Details')
@section('content')
   <style>
    td.sequence img {
   margin-right: 13px;
    height: 500px;
    width: 69%;
    object-fit: contain;

  }
  video {
    height: 300px;
}
   </style>
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Portfolio Details</h3>
               
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/portfolio-management/'.$portfolio->user_id)}}">Portfolio Management</a></li>
                  <li>Portfolio Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                           <td style="width:200px"> Porfolio Image</td>
                             <td class="sequence" >
                            
                            <?php 
                           
                            $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                           
                            
                            <?php
                           
                            $get_file = $portfolio->profile;
              
                            $has_video = false;
                            $has_file = false;
                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                            $get_file_type = strtolower($get_file_type);

                            if(in_array($get_file_type,$video_ext)){
                              echo '<div class="new"><video src="'.$get_file.'" controls="" "></video></div>';
                            }else {
                              echo '<div class="new"><img src="'.$get_file.'" "/></div>';
                            }
                             ?>
                           </td>
                        </tr>
                        
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection