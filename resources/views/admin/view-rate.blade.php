@extends('admin.layouts.app')
@section('title', ' Rate Details')
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Rate Details</h3>
               
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/rate-management/'.$rate->user_id)}}">Rate Management</a></li>
                  <li>Rate Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        
                         <tr>
                          <td width ="100">Rate</td>
                          <td>@if(!empty($rate->rate)){{$rate->rate}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td>Title </td>
                          <td>@if(!empty($rate->title)){{$rate->title}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Description </td>
                          <td>@if(!empty($rate->text)){{$rate->text}} @else N/A @endif</td>
                           
                        </tr>
                          
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection