@extends('admin.layouts.app')
@section('title', ' Refferal Details')
@section('content')


        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Refferal Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/refferal-list')}}">Refferal Management</a></li>
                  <li>View</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                       
                         <tr>
                          <td>Name</td>
                          <td>@if(!empty($businessDetails->name)){{$businessDetails->name}} @else N/A @endif</td>
                           
                        </tr>
                          <tr>
                          <td>Email Address</td>
                          <td>@if(!empty($businessDetails->email)){{$businessDetails->email}} @else N/A @endif</td>
                           
                        </tr>
                         <tr>
                          <td>Phone Number</td>
                          <td>@if(!empty($businessDetails->phone_number)){{$businessDetails->phone_number}} @else N/A @endif</td>
                            <tr>
                          <td>Refferal Code </td>
                          <td>@if(!empty($businessDetails->refferal_code)){{$businessDetails->refferal_code}} @else N/A @endif</td>  
                        </tr>
                        </table>
                  </div>
                 &nbsp;
                  <h4 style = "font-style: bold; font-size: 20px;">Refferal code user List </h4>
                  <table id="datatable-filter" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                            <th width="100">Sr. No.</th>
       
                            <th>Name</th>
                            <th>Phone Number</th>
                             <th>Email Address</th>	
                              <th>Refferal Code</th>								 
                        </tr>
                      </thead>

                      @php ($i=1)
                        @foreach($contactDetails as $contactDetail)
                       
                         <tr>
                         <td>{{$i++}}</td>        
                          <td>@if(!empty($contactDetail->userDetail->name)){{$contactDetail->userDetail->name}} @else N/A @endif</td>
                          <td>@if(!empty($contactDetail->userDetail->phone_number)){{$contactDetail->userDetail->phone_number}} @else N/A @endif</td>
						    <td>@if(!empty($contactDetail->userDetail->email)){{$contactDetail->userDetail->email}} @else N/A @endif</td>
						    <td>@if(!empty($contactDetail->userDetail->refferal_code)){{$contactDetail->userDetail->refferal_code}} @else N/A @endif</td>
                        </tr>
                      @endforeach
                         
                      </tbody>
                    </table>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->



    @endsection