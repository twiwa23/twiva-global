@extends('admin.layouts.app')
@section('title', ' Services Details')
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Services Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/services-management/'.$service->user_id)}}">Services Management</a></li>
                  <li>Services Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                           <td style="width:200px">Profile Image</td>
                            <td>
                            @if($service->profile)
                            <img src="{{$service->profile}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                            
                           </td>    
                        </tr>
                    
                         <tr>
                          <td>Title</td>
                          <td>@if(!empty($service->title)){{$service->title}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td>Description</td>
                          <td>@if(!empty($service->text)){{$service->text}} @else N/A @endif</td>
                           
                        </tr>
                          
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection