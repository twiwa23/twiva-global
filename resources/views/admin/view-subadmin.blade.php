@extends('admin.layouts.app')
@section('title', ' Subadmin Details')
@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Subadmin Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/subadmin-management')}}">Subadmin Management</a></li>
                  <li>Subadmin Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                       
                         <tr>
                          <td>Name</td>
                          <td>@if(!empty($user->name)){{$user->name}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td style="width:200px;">Email Address </td>
                          <td>@if(!empty($user->email)){{$user->email}} @else N/A @endif</td>
                           
                        </tr>
                        
                          <tr>
                          <td>PhoneNumber</td>
                          <td>@if(!empty($user->phone_number)){{$user->phone_number}} @else N/A @endif</td>
                           
                        </tr>
                         <tr>
                          <td>Permission</td> 
                          <td>   
                          @foreach($sideBarData as $sidebarDatas)
                          {{ $loop->first ? '' : ', ' }}
                          @if(!empty($sidebarDatas->name)){{$sidebarDatas->name}} @else N/A @endif
                          @endforeach 
                           </td>
                        </tr>
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection
