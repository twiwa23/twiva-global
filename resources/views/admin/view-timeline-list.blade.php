@extends('admin.layouts.app')
@section('title', ' Timeline Details')
@section('content')

        <!-- /top navigation -->

        <!-- page content -->
      <style type="text/css">
      .new {
      width: 22%;
      display: inline-block;
      margin-right: 17px;
      margin-bottom: 18px;
      position: relative;
      vertical-align: top;


      }
      .new video {
      width: 100%;
      height: 120px;
      background-color: #000;
      }
      td.sequence img {
    margin-right: 13px;
    width: 100%;
    height: 120px;
    object-fit: cover;
}
      </style>
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>Timeline Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
             
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/timeline-list')}}">Timeline Management</a></li>
                  <li>Timeline Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                          <td>Timeline Text</td>
                          <td><textarea class="form-control" disabled="">@if(!empty($viewTimeline->timeline_text)){{$viewTimeline->timeline_text}} @else N/A @endif</textarea></td>  
                        </tr>
                       
                         <tr>
                           <td style = "width:230px;">Media</td>
                           <td class="sequence">
                            @forelse ($viewTimeline->timelineImages as $media_files)
                            <?php $viewTimeline = $All_files = "";
                            $no_doc = 0;
                           
                            $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                           
                            
                            <?php
                           
                            $get_file = $media_files->media;
							
                            $has_video = false;
                            $has_file = false;
                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                            $get_file_type = strtolower($get_file_type);

                            if(in_array($get_file_type,$video_ext)){
                              echo '<div class="new"><video src="'.$get_file.'" controls="" "></video></div>';
                            }else {
                              echo '<div class="new"><img src="'.$get_file.'" "/></div>';
                            }
                             ?>
                            @empty
                             <img src="{{url('public/admin/production/images/user.png')}}" alt="blank_" style="width: 100px;height: 100px; object-fit: cover;">
                            @endforelse
                           </td>
                        </tr>
                         
                           
                         
                        
                         
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->



   @endsection