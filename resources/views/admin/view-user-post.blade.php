@extends('admin.layouts.app')
@section('title', ' User Post Details')
@section('content')
<style>
ul, li {
    list-style: inherit;
}
.new-new {
    margin-left: 14px;
}
</style>
        <!-- page content -->
        <div class="right_col" role="main">
        <div>	
            <div class="page-title">
              <div class="title_left">
                <h3>User Post Details</h3>
              </div>
			<div class="clearfix"></div>
            </div>
        	<div class="row">
        		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                 <li><a href="{{url('admin/user-post')}}">User Post</a></li>
                  <li>User Post Details</li>
                </ul>
                  <div class="x_content">
                    <table class="table table-striped table-bordered custom-table spam">
                        <tr>
                           <td style="width:200px">Post Image</td>
                            <td>
                          
                            @if($post->profile)
                            <img src="{{$post->profile}}" alt="User" style="width: 100px;height: 100px; object-fit: cover;"/>
                            @else
                            <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="width: 100px;height: 100px; object-fit: cover;">
                           @endif
                           
                           </td>    
                        </tr>
                         <tr>
                          <td>Post Title</td>
                          <td>@if(!empty($post->title)){{$post->title}} @else N/A @endif</td>    
                        </tr>
                          <tr>
                          <td>Post text </td>
                      <td>
					  <div class = "new-new">
                          <span> 
                      {!!html_entity_decode($post->text)!!}
                     </span>
					 </div>
                      </td>
                        </tr>
                        
                         
                         
                     
                        </tr>
                        </table>
                  </div>
                </div>
              </div>
                </div>
        </div>
              </div>
            </div>
          </div>
     
        <!-- /page content -->

    @endsection