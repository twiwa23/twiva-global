<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('business/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="{{ asset('business/assets/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('business/assets/css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">

    @yield('styles')
</head>

<body>

    <!-- Top Navbar Start -->
    <header class="header-section">
        <!-- Navbar Logo -->
        <div class="logo logo-toggle">
            <div class="nav toggle">
                <a id="menu_toggle" href="#" class="d-flex">
                    <img src="{{ asset('business/assets/images/icons/toggle_icon.svg') }}" alt="Toggle_Menu">
                </a>
            </div>
            <a href="/" class="main-logo"><img src="{{ asset('business/assets/images/icons/twiva-logo.svg') }}"></a>
        </div>

        <!-- Navbar Menu -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="">
                    <!-- <div class="nav toggle">
                        <a id="menu_toggle"></a>
                    </div> -->

                    <ul class="nav navbar-nav">
                        <li class="d-none">
                            <a href="#"><img src="{{ asset('business/assets/images/icons/message-square.svg') }}" alt=""></a>
                        </li>
                        <li class="" id="nav-click-new"  data-url="notifications.php">
                            <a href="#">
                                <img src="{{ asset('business/assets/images/icons/bell-icon.svg') }}">
                            </a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile-img">
                                    @if(empty(auth()->guard('business')->user()->companyDetail->logo))
                                        <img src="{{ asset('business/extra/images/profile.jpeg') }}" alt="">
                                    @else
                                        <img src="{{ config('app.dev_storage_url'). auth()->guard('business')->user()->companyDetail->logo }}" alt="">
                                    @endif
                                </div>
                                {{ auth()->guard('business')->user()->companyDetail->busi_name ?? 'User'}}

                                <img src="{{ asset('business/assets/images/icons/chevron-down-white.svg') }}" alt="" class="down-arrow">
                            </a>

                            {{-- <li class="dropdown">
                                <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile-img">
                                        <img src="{{ asset('business/assets/images/icons/img.jpg') }}" alt="" />
                                    </div>
                                    {{ auth()->guard('business')->user()->name ?? 'Business'}}
                                    <img src="{{ asset('business/assets/images/icons/chevron-down-white.svg') }}" alt="" class="down-arrow" />
                                </a> --}}

                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li class="header-company-name-wrapper">
                                        <p class="company-name">{{ auth()->guard('business')->user()->name ?? 'Business'}}</p>
                                        <p class="email">{{ auth()->guard('business')->user()->email ?? 'Business Email' }}</p>
                                    </li>
                                    <li>
                                        <a href="{{url('business/logout')}}" id="logout-btn">Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <!-- Top Navbar End -->
        @yield('content')


    <!-- <script src="assets/js/jquery-3.5.1.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('business/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('business/assets/js/moment.min.js') }}"></script>
    <script src="{{ asset('business/assets/js/daterangepicker.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('business/assets/js/custom.js') }}"></script>
    <script>

$(document).on('click', '.nav-click-new', function(e) {
        console.log($(this).data('id'));
        var user_id = $(this).data('id');
        // var user_token = STORAGE.get(STORAGE.accesstoken);
        var nav_url = '/navChange';
        e.preventDefault();
        if(user_id) {
            $.ajax({
                url: "{{ url('business/navChangeToNew') }}",
                type: "post",
                data: { user_id: user_id, nav_url : nav_url},
                success: function(data){
                    console.log(data);
                    // console.log(data.data.url);
                    // window.open(data.data.url);
                    window.location.href = data.url;
                },
                error : function(data) {
                    console.log(data)
                }
            });
        }
    });
    </script>
    <script type="text/javascript">
        $(function() {
            setTimeout(function(){
                $(".alertz").hide();
            }, 5000);
        });
    </script>
    <script>
        $(document).on('click', '#nav-click-new', function(e) {
            var user_id = $("#user_id").val();
            var nav_url = $(this).data('url');
            console.log(nav_url);
            e.preventDefault();
            if(user_id) {
                $.ajax({
                    url: "{{ route('navChangeToNew') }}",
                    type: "post",
                    data: { user_id: user_id, nav_url : nav_url},
                    success: function(data){
                        console.log(data);
                        console.log(data.url);
                        // window.open(data.url);
                        // let url = data.data.url.split("?");
                        window.location.href = data.url;
                    },
                    error : function(data) {
                        console.log(data)
                    }
                });
            }
        });
    </script>
@yield('scripts')

</body>

</html>
