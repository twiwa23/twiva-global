
@if(Session::has('success'))
  <div style="padding:11px;font-weight: 400;" class="alert alert-success alert-dismissible text-center alertz">{{ Session::get('success') }}</div>
@endif
@if(Session::has('danger'))
  <div style="padding:11px;font-weight: 400;" class="alert alert-danger alert-dismissible text-center alertz">{{ Session::get('danger') }}</div>
@endif



@if(Session::has('notification'))
    <div class="row alertz" id="alert2">
        <div class="alert alert-{{ Session::get('notification')['status'] == 'success' ? 'success' : 'danger'}} alert-dismissible fade in text-center">
            <span>{!! Session::get('notification')['message'] !!}</span>
        </div>
        <div class="clearfix"></div>

    </div>


@endif

