<?php $id = Auth::guard('business')->user()->id; ?>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <button class="menu-close-button"><img src="{{ asset('business/assets/images/icons/x.svg') }}" alt="" /></button>
    <div class="menu_section">
        <h4>Menu</h4>
        <input type="hidden" id="user_id" value="{{ auth()->guard('business')->user()->id }}">
        <ul class="nav side-menu">
            <li id="nav-click-new" data-url="dashboard-2.php">
                <a href="#">
                    <img src="{{ asset('business/assets/images/icons/dashboard-2.svg') }}">Dashboard
                    <span class="fa fa-chevron-down"></span>
                </a>

            </li>

            <li class="sidebar-dropdown">
                <a @if(Request::segment(2) == 'creaters') class="current-page" @endif > <img src="{{ asset('business/assets/images/icons/hire.svg') }}">Hire<span class="fa fa-chevron-down"></span></a>
                <div class="sidebar-submenu">
                    <ul class="dropdown-lists">

                        <li class="drop-down-item">
                            <a href="{{url('business/influencers-list') }}">Influencers</a>
                        </li>
                        <li class="drop-down-item @if(Request::segment(2) == 'creaters') active @endif">
                            <a href="{{url('business/creaters')}}">Designers</a>
                        </li>

                    </ul>
                </div>
            </li>

            <li>
                <a href="{{ url('business/posts') }}" @if(Request::segment(2) == 'posts') class="current-page" @endif>
                    <img src="{{ asset('business/assets/images/image.svg') }}">Posts
                    <span class="fa fa-chevron-down"></span> 
                </a>
            </li>

            <li>
                <a href="{{ url('business/gig-list') }}"  @if(Request::segment(2) == 'gig-list') class="current-page" @endif>
                    <img src="{{ asset('business/assets/images/icons/twitch.svg') }}">Twitter Trends
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li>

            <li id="nav-click-new"  data-url="my-catalog-2.php">
                <a href="#">
                    <img src="{{ asset('business/assets/images/icons/layers-2.svg') }}">My Catalog
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li>

            <li id="nav-click-new"  data-url="order.php">
                <a href="#">
                    <img src="{{ asset('business/assets/images/icons/orders-2.svg') }}">Order Received
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li>

            <li id="nav-click-new"  data-url="best-performing-product.php">
                <a href="#">
                    <img src="{{ asset('business/assets/images/icons/reports-2.svg') }}">Reports
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li>

            {{-- <li id="nav-click-new"  data-url="">
                <a href="./admin-transaction.html">
                    <img src="{{ asset('business/assets/images/icons/transaction-2.svg') }}">Transactions
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li> --}}
            <li class="sidebar-dropdown">
                <a href="javascript:void(0);"><img src="{{ asset('business/assets/images/icons/settings.svg') }}">Account Settings<span class="fa fa-chevron-down"></span></a>
                <div class="sidebar-submenu">
                    <ul class="dropdown-lists">

                        <li class="drop-down-item" id="nav-click-new"  data-url="bussiness-profile-information.php">
                            <a href="#">
                                Profile Settings
                            </a>
                        </li>
                        <li class="drop-down-item" id="nav-click-new"  data-url="my-subscriptions.php">
                            <a href="#">
                                My Subscription
                               </a>
                        </li>
                        <li class="drop-down-item" id="nav-click-new"  data-url="bussiness-bank-setup.php">
                            <a href="#">
                                Bank Setup
                                </a>
                        </li>
                        <li class="drop-down-item" id="nav-click-new"  data-url="bussiness-change-password.php">
                            <a href="#">
                                Change Password
                                </a>
                        </li>
                        <li class="drop-down-item" id="nav-click-new"  data-url="earnings.php">
                            <a href="#">
                                My Earning
                            </a>
                        </li>
                        <li class="drop-down-item" id="nav-click-new"  data-url="">
                            <a href="#">
                            Payment History
                            </a>
                        </li>
                        <!-- <li class="drop-down-item @if(Request::segment(2) == 'update-mpsa-account') active @endif">
                            <a href="{{url('business/update-mpsa-account')}}">Update MPesa Account</a>
                        </li> -->
                    </ul>
                </div>
            </li>
            <li id="nav-click-new"  data-url="refer-earn.php">
                <a href="#">
                    <img src="{{ asset('business/assets/images/icons/refer.svg') }}">Refer & Earn
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li>

            <li>
                <a href="{{url('business/logout')}}">
                    <img src="{{ asset('business/extra/images/logout-sidebar.svg') }}">Log Out
                    <span class="fa fa-chevron-down"></span>
                </a>
            </li>
        </ul>

    </div>
</div>
<div class="backdrop"></div>
