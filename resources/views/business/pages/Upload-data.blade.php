@extends('business.layout.app')
@section('title')
    Uploaded Data
@endsection
@section('styles')
<style>
    label#text-error {
        color: red;
    }
</style>

@endsection
@section('content')
<div class="influencers-profile">
    <div class="dashboard_container">
        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->
        <!-- Page Content -->
        <div class="right_col " role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{ url('business/posts') }}">Stories/ Feedposts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#">Uploaded Data</a></li>
                </ul>
            </div>
            <!--********** Breadcrumb End ***********-->

            <div class="post-container hire-me">
                <div class="post-heading">
                    <h3>Uploaded Data</h3>
                </div>
            @include('business.layout.notifications')
            <form method="post" id="form_validate">
                 {{csrf_field()}}
                <div class="post-input-container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="input-section">
                                <div class="form-group">
                                    <label>Enter your reason of rejection so influencer can update the same again</label>
                                    <textarea name="text" maxlength="1000" placeholder="" required class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="btn-wrapper">
                    <button type="submit" class="red-btn">Submit</button>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
}, "Space not allowed");

$('#form_validate').validate({
    rules: {
        text: {
            required: true,
            noSpace:true
        },
    },

    messages:{

        text:{
        required:'Please enter reason.'
        },
    }
});
</script>
@endsection
