@extends('business.layout.app')
@section('title')
View Post Details
@endsection
@section('content')
<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main" id="image-popup">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="{{url('business/post-details',base64_encode($acceptPostDetails->post_id))}}" class="active">{{$acceptPostDetails->post->post_name}}<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#" class="active">Hired<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#">View Post</a></li>
                </ul>
            </div>
            @include('business.layout.notifications')
            <!--**********  Breadcrumb End ***********-->

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="post" id="form_validate" onsubmit="form_validate()">
                {{csrf_field()}}
                <input type="hidden" name="post_id" value="{{$acceptPostDetails->post_id}}">
                <input type="hidden" name="infulancer_id" value="{{$acceptPostDetails->user_id}}">
                <?php
                    $url = base64_encode($acceptPostDetails->post_id);
                    $url1 = base64_encode($acceptPostDetails->user_id);

                    $base64_encode_url = url('business/upload-data').'/'.$url.'/'.$url1;
                ?>
                    <div class="view-post">
                        <div class="post-heading">
                            <h1>{{$acceptPostDetails->user->name }}'s Post</h1>
                            <div class="btn-section">
                                <a href="{{$base64_encode_url}}" class="white-btn">Request Edit</a>
                                <a class="red-btn" data-toggle="modal" data-target="#approve-modal">Approve</a>
                                <!-- <a href="#" class="red-btn" data-toggle="modal" data-target="#approve-modal">Approve</a> -->
                            </div>
                        </div>
                    </div>


                    <div class="post-details">
                        <p>21th Dec, 2020 <img src="{{ asset('business/assets/images/icons/bullet.svg') }}"> 14:35</p>
                        <h4>Choose the post you like :</h4>

                        <ul class="img-lists">
                            @if($acceptPostDetails->postDetail && !empty($acceptPostDetails->postDetail))
                        <?php $videos = $All_files = "";
                           $no_doc = 0;
                           $total = 0;
                           $video_ext = ["mp4","avi","3gp","flv","mov","video"];
                           $i=1; $k=1;
                        ?>
                        @forelse($acceptPostDetails->postDetail->media as $postDetail)
                            <li class="img-list-item">
                                <?php
                                $total++;
                                $get_file = $postDetail->media;
                                $has_video = false;
                                $has_file = false;
                                $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                                $get_file_type = strtolower($get_file_type);
                                if(in_array($get_file_type,$video_ext)){
                                    echo '<label for="cb'.$k++.'"><video src="'.$get_file.'" controls="" class="img-responsive video-containt video" " ></video></label><input type="checkbox" id="cb'.$i++.'" name="image" value="'.$postDetail->media.'"  class="radio check_box_img" required/>';
                                }else {
                                    echo '<label for="cb'.$k++.'">
                                        <img src="'.$get_file.'" alt="">
                                        <div class="image-zoom" onclick="openModal();currentSlide(1)">
                                            <img src="'.  asset('business/assets/images/icons/maximize-2.svg') .'" alt="">
                                        </div></label>
                                        <input type="checkbox" id="cb'.$i++.'" name="image" value="'.$postDetail->media.'"  class="radio check_box_img" />';
                                }
                                ?>
                                {{-- <img src="{{ asset('business/assets/images/profile-images/Rectangle724.png') }}" class="img-list">
                                <div class="select">Select</div>
                                <div class="selected">Selected</div>
                                <div class="image-zoom" onclick="openModal();currentSlide(1)">
                                    <img src="{{ asset('business/assets/images/icons/maximize-2.svg') }}" alt="">
                                </div> --}}
                            </li>
                            @empty
                        N/A
                        @endforelse
                        @else
                        N/A
                        @endif
                        </ul>

                        <h4>Choose the caption you like :</h4>

                        <div class="caption-lists">
                            @if($acceptPostDetails->postDetail->text_one)
                            <div class="caption-list-item">
                                <div>
                                    <label class="checkbox-wrapper">
                                        <input type="checkbox" id="caption-1" name="text" value="{{$acceptPostDetails->postDetail->text_one ?? 'N/A'}}">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label for="caption-1" class="caption-label">Caption 1</label>
                                </div>
                                <p>{{$acceptPostDetails->postDetail->text_one ?? 'N/A'}}</p>
                            </div>
                            @endif

                            @if($acceptPostDetails->postDetail->text_two)
                            <div class="caption-list-item">
                                <div>
                                    <label class="checkbox-wrapper">
                                        <input type="checkbox" id="caption-2" name="text" value="{{$acceptPostDetails->postDetail->text_two ?? 'N/A'}}">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label for="caption-2" class="caption-label">Caption 2</label>
                                </div>
                                <p>{{$acceptPostDetails->postDetail->text_two ?? 'N/A'}}</p>
                            </div>
                            @endif

                            @if($acceptPostDetails->postDetail->text_three)
                            <div class="caption-list-item">
                                <div>
                                    <label class="checkbox-wrapper">
                                        <input type="checkbox" id="caption-3" name="text" value="{{$acceptPostDetails->postDetail->text_one ?? ''}}">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label for="caption-3" class="caption-label">Caption 3</label>
                                </div>
                                <p>{{$acceptPostDetails->postDetail->text_three ?? 'N/A'}}</p>
                            </div>
                            @endif
                        </div>
                        <div class="new-fine">
                            <label id="text-error" class="error" for="text" style="display: none;"></label>
                        </div>
                    </div>

<div class="modal fade" id="approve-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Are you sure you want to approve the post of {{$acceptPostDetails->user->name}}?</h4>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="white-btn" data-dismiss="modal">Not sure yet</button>
                <button class="red-btn" onclick="form_validate()">Yes, Approve</button>

                <!-- <a href="" class="white-btn" data-dismiss="modal">Not sure yet</a>
                <a href="" class="red-btn">Yes, Approve</a> -->
            </div>

        </div>
    </div>
</div>
            </form>

            <!-- The Modal/Lightbox -->
            <div id="imgModal" class="popup">
                <span class="close cursor" onclick="closeModal()"><img src="{{ asset('business/assets/images/icons/x-circle.svg') }}"></span>
                <div class="modal-content">

                    <div class="mySlides">
                        <img src="{{ asset('business/assets/images/profile-images/Rectangle724.png') }}">
                    </div>

                    <div class="mySlides">
                        <img src="{{ asset('business/assets/images/profile-images/Rectangle725.png') }}">
                    </div>

                    <div class="mySlides">
                        <img src="{{ asset('business/assets/images/profile-images/Rectangle726.png') }}">
                    </div>

                    <!-- Next/previous controls -->
                    <a class="prev" onclick="plusSlides(-1)">
                        <img src="{{ asset('business/assets/images/icons/arrow-left-circle.svg') }}" alt="">
                    </a>
                    <a class="next" onclick="plusSlides(1)">
                        <img src="{{ asset('business/assets/images/icons/arrow-right-circle.svg') }}" alt="">
                    </a>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>
</div>


@endsection
@section('scripts')
    <script type="text/javascript">

   $("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>

<script type="text/javascript">
   $('#form_validate').validate({
      rules: {

          text: {
              required: true
          },
          image: {
              required: true
          },
      },

      messages:{

         text:{
            required:'Please select caption option.'
         },

         image:{
            required:'Please select theme photo/video.'
         },
      },
   });

   function form_validate() {
    $('#form_validate').validate({
      rules: {

          text: {
              required: true
          },
          image: {
              required: true
          },
      },

      messages:{

         text:{
            required:'Please select caption option.'
         },

         image:{
            required:'Please select theme photo/video.'
         },
      },
   });
   }
</script>
@endsection
