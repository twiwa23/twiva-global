@extends('business.layout.app')
@section('title')
    Hired Influencers
@endsection

@section('content')
<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="{{ url('business/post-details/'.base64_encode($id)) }}" class="active">{{ $name ?? '' }}<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="">Hired</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->

            <div class="influencer-list">
                <h4>Hired Influencers</h4>
            </div>
            @include('business.layout.notifications')
            @if(!empty($acceptPostDetails) && $acceptPostDetails && count($acceptPostDetails) > 0)
            <div class="catalog-page">
                <div class="select-top select-search">
                    <div class="search-wrap">
                        <div class="search-panel">
                            <input type="text" placeholder="Search here...">
                        </div>

                        <div class="button-catalog">
                            <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">All</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="5-Star">5-Star</span>
                                        <span class="btn-option" data-value="4-Star">4-Star</span>
                                        <span class="btn-option" data-value="3-Star">3-Star</span>
                                        <span class="btn-option" data-value="2-Star">2-Star</span>
                                        <span class="btn-option" data-value="1-Star">1-Star</span>
                                    </div>
                                    <img src="/assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="table-responsive">
                    <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>IMAGE</th>
                                <th>NAME</th>
                                {{-- <th>STAR RATING</th> --}}
                                <th>RATE</th>
                                <th>FOLLOWERS</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>

                        <tbody>

                                @foreach($acceptPostDetails as $post)
                                <?php
                                    if($post->user->SocialDetail->facebook_friends) {
                                       if($post->user->SocialDetail->facebook_friends >= 1000000){
                                        $facebook_friends = number_format(($post->user->SocialDetail->facebook_friends / 1000000))."M";
                                       }else if($post->user->SocialDetail->facebook_friends >= 1000){
                                        $facebook_friends = number_format(($post->user->SocialDetail->facebook_friends / 1000))."K";
                                       }else {
                                        $facebook_friends = $post->user->SocialDetail->facebook_friends;
                                       }
                                    }


                                    if($post->user->SocialDetail->twitter_friends) {
                                       if($post->user->SocialDetail->twitter_friends >= 1000000){
                                        $twitter_friends = number_format(($post->user->SocialDetail->twitter_friends / 1000000))."M";
                                       }else if($post->user->SocialDetail->twitter_friends >= 1000){
                                        $twitter_friends = number_format(($post->user->SocialDetail->twitter_friends / 1000))."K";
                                       }else {
                                        $twitter_friends = $post->user->SocialDetail->twitter_friends;
                                       }
                                    }

                                    if($post->user->SocialDetail->instagram_friends) {
                                       if($post->user->SocialDetail->instagram_friends >= 1000000){
                                        $instagram_friends = number_format(($post->user->SocialDetail->instagram_friends / 1000000))."M";
                                       }else if($post->user->SocialDetail->instagram_friends >= 1000){
                                        $instagram_friends = number_format(($post->user->SocialDetail->instagram_friends / 1000))."K";
                                       }else {
                                        $instagram_friends = $post->user->SocialDetail->instagram_friends;
                                       }
                                    }

                                 ?>
                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                @if(!empty($post->user->Images[0]->image))
                                                <?php $image = $post->user->Images[0]->image;  ?>
                                                <img src="{{$image}}" alt="upload-one" class="img-responsive round"/>
                                                @else
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                                @endif
                                            </div>
                                        </td>
                                        <td class="user-name"><a href="{{url('business/influencer-details',base64_encode($post->user->id))}}">
                                            {{ $post->user->name ?? '' }}</a></td>
                                        {{-- <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td> --}}
                                        <td class="post-rate">KSh {{$post->price ?? '0'}}</td>
                                        <td>
                                                <div class="social-media">
                                                    <a href="#" class="followers">
                                                        <img src="{{ asset('business/assets/images/icons/insta.svg') }}" alt="">
                                                        <p>{{$instagram_friends}}</p>
                                                    </a>
                                                    <a href="#" class="followers">
                                                        <img src="{{ asset('business/assets/images/icons/fb.svg') }}" alt="">
                                                        <p>{{$facebook_friends}}</p>
                                                    </a>
                                                    <a href="#" class="followers">
                                                        <img src="{{ asset('business/assets/images/icons/twit.svg') }}" alt="">
                                                        <p>{{$twitter_friends}}</p>
                                                    </a>
                                                </div>
                                        </td>
                                        <?php
                                            if($post->status == '1') {
                                                $status_btn = 'blue-box';
                                            $status = "Data not submitted yet";
                                            }
                                            if($post->is_submit == '1' && $post->status == '1') {
                                                $status_btn = 'blue-box';
                                            $status = "Updated Data";
                                            }

                                            if($post->status == '3') {
                                                $status_btn = 'completed';
                                            $status = "Approved";
                                            }
                                            if($post->status == '4') {
                                                $status_btn = 'declined';
                                            $status = "Requested Edit";
                                            }
                                        ?>

                                        <td class="{{ $status_btn }}">
                                            <p>{{ $status ?? 'N/A'}}</p>
                                        </td>
                                        <td>
                                            <!-- <a href="#" class="hire-me-btn">View Post</a>
                                            <a href="#" class="hire-me-btn" style="margin-left: 48px;">Raise Dispute</a> -->
                                            @if($post->is_submit == '1' && $post->status == '1')
                                            <a href="{{url('business/accept-post-details',base64_encode($post->id))}}">
                                                <button class="hire-me-btn">View Post</button>
                                            </a>
                                            @endif
                                            {{-- <button class="hire-me-btn" style="margin-left: 48px;">Raise Dispute</button> --}}
                                        </td>
                                        {{-- <td>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                            <button class="hire-me-btn">Send Reminder</button>
                                        </td> --}}
                                    </tr>
                                @endforeach


                        </tbody>

                    </table>
                </div>

                <div class="pagination">
                    <div class="col-sm-5">
                        <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                        <div class="">
                            <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                {{-- <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example_previous">
                                        <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                            <img src="./assets/images/icons/button.svg" alt="">
                                        </a>
                                    </li>

                                    <li class="paginate_button active">
                                        <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                    </li>

                                    <li class="paginate_button next" id="example_next">
                                        <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                            <img src="./assets/images/icons/Right.svg" alt="">
                                        </a>
                                    </li>

                                </ul> --}}
                            {!! $acceptPostDetails->links() !!}

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @else
            <div class="data_found text-center accept">
                <img src="{{url('public/business/images/empty.svg')}}" style="width: 87px; height: 113px;">
                <h1 class="text-center" style="font-size: 20px; margin-top: 10px;">No Data Found!</h1>
            </div>
            @endif




        </div>
        <!-- /page content -->
    </div>
</div>
@endsection
