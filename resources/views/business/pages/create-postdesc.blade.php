@extends('business.layout.app')
@section('title')
Post Description
@endsection
@section('content')
<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main" id="approved-page">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="./assets/images/icons/chevron-right-gray.svg" alt=""></a></li>
                    <li><a href="#">What are Posts?</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->

            <div class="what-is-post">
                <h4>Posts are invites to specific influencers to create content for a suggested price. A Post outlines the description of the posted social media content and only influencers that are invited can participate. Post currently are newsfeed posts, video-blogs (i.e., Instagram stories) as well as blogs.</h4>

                <div class="invite-steps">
                    <ul class="invite-list nav nav-tabs">
                        <li class="invite-list-item nav-item active">
                            <div class="number nav-link">1</div>
                            <p>Fill Details</p>
                        </li>
                        <li class="invite-list-item nav-item">
                            <div class="number nav-link">2</div>
                            <p>Invite</p>
                        </li>
                        <li class="invite-list-item nav-item">
                            <div class="number nav-link">3</div>
                            <p>Payment</p>
                        </li>
                        <li class="invite-list-item nav-item">
                            <div class="number nav-link">4</div>
                            <p>Track</p>
                        </li>
                    </ul>


                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane invite-list-content active">
                            <h5>Give information regarding the post (i.e. what the post must be about, Do’s n Dont’s)Upload theme photos/videos to inspire influencers on the type of content you’d like to see.</h5>
                        </div>
                        <div class="tab-pane invite-list-content">
                            <h5>Invite Influencers that match to your needs and propose a compensation</h5>
                        </div>
                        <div class="tab-pane invite-list-content">
                            <h5>Twiva holds payment and releases to influencer once sponsored content is posted. If post is not completed, money will be refunded to your account.</h5>
                        </div>
                        <div class="tab-pane invite-list-content">
                            <h5>Track progress of the sponsored content on our Post dashboard</h5>
                        </div>
                    </div>

                    <button class="red-btn" id="next-step">Next</button>
                    <a href="{{ url('business/create-post-field') }}"><button class="red-btn" id="create">Create Posts</button></a>
                </div>
            </div>



        </div>
        <!-- /page content -->
    </div>
</div>
@endsection
@section('scripts')

@endsection
