@extends('business.layout.app')
@section('content')
<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-heading">Designers</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->

            <div class="catalog-page">
                <div class="select-top select-search">
                    <div class="search-wrap">
                        {{-- <div class="search-panel">
                            <input type="text" placeholder="Search here...">
                        </div> --}}

                        <div class="button-catalog" style="width: 200px;">
                            <select name = "category_name" id = "category_name" class="form-control m-0" style="max-width: 100%;" required>

                                {{-- <option value=""> Select Category</option> --}}
                                 <option value="{{url('business/creaters')}}"> All Categories</option>
                               @foreach($category as $value)
                               <option value="{{url('business/creaters').'?category_id='.$value->id}}" @if(app("request")->input('category_id') == $value->id) selected @endif >{{$value->name}}</option>
                               @endforeach
                             </select>
                            {{-- <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">All</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="Blogger">Blogger</span>
                                        <span class="btn-option" data-value="Photography">Photography</span>
                                        <span class="btn-option" data-value="Videography">Videography</span>
                                        <span class="btn-option" data-value="Graphic Designer">Graphic Designer</span>
                                        <span class="btn-option" data-value="Artist">Artist</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div> --}}

                            {{-- <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">All</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="5-Star">5-Star</span>
                                        <span class="btn-option" data-value="4-Star">4-Star</span>
                                        <span class="btn-option" data-value="3-Star">3-Star</span>
                                        <span class="btn-option" data-value="2-Star">2-Star</span>
                                        <span class="btn-option" data-value="1-Star">1-Star</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div>

                            <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">Price Range</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                        <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                        <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div> --}}
                        </div>

                    </div>
                </div>

                <div class="table-responsive">
                    <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>IMAGE</th>
                                <th>NAME</th>
                                <th>CATEGORY</th>
                                {{-- <th>TWITTER TREND RATE</th> --}}
                                <th style="width:35%;">ACTION</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($users as $user)
                            <tr>
                                <td>
                                    @if(!empty($user->user_profile))
                                        @php $image =  url('storage/uploads/images/business/'.$user->user_profile);@endphp
                                                @else
                                        @php $image = asset('business/assets/images/profile-images/profile-2.png'); @endphp
                                    @endif
                                    <div class="user-image">
                                        <img src="{{ $image }}" alt="" class="new-photo">
                                    </div>
                                </td>
                                {{-- <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td> --}}

                                <td class="user-name">@if(!empty($user->user_name)){{$user->user_name}} @else N/A @endif</td>
                                <td class="post-rate">@if(!empty($user->category_name)){{$user->category_name}} @else N/A @endif</td>
                                {{-- <td class="post-rate">Ksh3000</td> --}}
                                <td>
                                    <a href="{{url('business/portfolio/'.$user->user_id)}}" class="hire-me-btn">Contact Me</a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center p-5 border border-0" colspan="4">
                                    <img src="{{asset('business/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                    <h3 style="font-size: 20px;">No Data Found!</h3>
                                </td>
                            </tr>
                            @endforelse

                        </tbody>

                    </table>
                </div>

                <div class="pagination">
                    <div class="col-sm-5">
                        <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                        <div class="">
                            <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                {{-- <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example_previous">
                                        <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                            <img src="./assets/images/icons/button.svg" alt="">
                                        </a>
                                    </li>

                                    <li class="paginate_button active">
                                        <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                    </li>

                                    <li class="paginate_button next" id="example_next">
                                        <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                            <img src="./assets/images/icons/Right.svg" alt="">
                                        </a>
                                    </li>

                                </ul> --}}
                                {{ $users->appends(request()->except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
document.getElementById("category_name").addEventListener("change",function(){
    window.location.href = this.value;
})
</script>

<script>
    $(document).ready(function(){
        setTimeout(function(){ functionTobeLoaded() }, 200);

        function functionTobeLoaded(){
            $('.pagination li:first-child').addClass('white-btn');
            $(".pagination li:last-child").addClass("red-btn");
            $('.pagination li:first-child span').text('Previous');
            $('.pagination li:first-child a').text('Previous');
            $('.pagination li:last-child span').text('Next');
            $('.pagination li:last-child a').text('Next');
        }
    });
    
</script>
@endsection
