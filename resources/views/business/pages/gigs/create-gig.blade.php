@extends('business.layout.app') @section("title","Create Twitter Trend") @section('content') @section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.css" /> -->
@endsection
<div class="" id="business-dashboard">
    <div class="dashboard_container">
        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->
            </div>
        </div>
        <!-- Left Column End -->
        <!-- page content -->
        <div class="right_col dashboard-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ url('business/gig-list') }}" class="active">Twitter Trends<img src="./assets/images/icons/chevron-right-gray.svg" alt="" /></a>
                    </li>
                    <li><a href="#">Create Twitter Trend</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->

            <div class="post-container">
                <form method="POST" autocomplete="off" id="form_validate" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="post-heading">
                        <h3>Twitter Trend Information</h3>
                        <a href="{{url('business/what-is-twitter-trend')}}" class="post-info">
                            <img src="./assets/images/icons/info2.svg" alt="" />
                            <span>What is Twitter Trend?</span>
                        </a>
                    </div>

                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input id="title" name="title" value="{{old('title')}}" type="text" class="form-control" />
                                        <span id="title-error" class="error text-danger" for="title">{{$errors->first('title')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input id="description" maxlength="1000" name="description" type="text" value="{{old('description')}}" class="form-control" />
                                        <span id="description-error" class="error text-danger" for="description">{{$errors->first('description')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Person Name</label>
                                        <input id="contact_person_name" name="contact_person_name" value="{{old('contact_person_name')}}" type="text" class="form-control" />
                                        <span id="contact_person_name-error" class="error text-danger" for="contact_person_name">{{$errors->first('contact_person_name')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Person Number</label>
                                        <input id="contact_person_number" onkeypress="return isNumberKey(event)" name="contact_person_number" value="{{old('contact_person_number')}}" type="number" class="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group mb-5">
                                        <label>What To Do <span>(These are the things Influencers have to do in their content)</span></label>
                                        <textarea name="what_to_do" maxlength="1000" id="what_to_do" value="{{old('what_to_do')}}" class="form-control"></textarea>
                                        <span id="what_to_do-error" class="error text-danger" for="what_to_do">{{$errors->first('what_to_do')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Compensation <span>(How much you will pay to the Influencer you hire)</span></label>
                                        <input id="gig_price_offering" maxlength="6" onkeypress="return isNumberKey(event)" name="gig_price_offering" value="{{old('gig_price_offering')}}" type="text" class="form-control" />
                                        <span id="gig_price_offering-error" class="error text-danger" for="gig_price_offering">{{$errors->first('gig_price_offering')}}</span>
                                    </div>

                                    <div class="row">
                                        <div class="com-sm-12 col-lg-12">
                                            <div class="form-group">
                                                <!-- <label>Gig Start Date</label> -->
                                                <label>Twitter Trends Start Date</label>
                                                <input type="text" class="form-control" id="gig_start_date" name="gig_start_date" value="{{old('gig_start_date')}}" autocomplete="off" required />
                                                <span id="gig_start_date-error" class="error text-danger" for="gig_start_date" style="display: none;">{{$errors->first('gig_start_date')}}</span>
                                            </div>
                                        </div>

                                        <!-- <div class="com-sm-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Gig Start Time</label>
                                                <div class="form-control">
                                                    <div class="quantity">
                                                        <input type="number" min="1" max="12" step="1" class="time-input" />
                                                        <div class="quantity-nav">
                                                            <button type="button" class="quantity-button quantity-up">&#xf106;</button>
                                                            <button type="button" class="quantity-button quantity-down">&#xf107;</button>
                                                            <div class="time-text">Hrs</div>
                                                        </div>
                                                    </div>

                                                    <div class="quantity">
                                                        <input type="number" min="1" max="60" step="1" value="1" class="time-input" />
                                                        <div class="quantity-nav">
                                                            <button type="button" class="quantity-button quantity-up">&#xf106;</button>
                                                            <button type="button" class="quantity-button quantity-down">&#xf107;</button>
                                                            <div class="time-text">Min</div>
                                                        </div>
                                                    </div>

                                                    <ul class="meridian">
                                                        <li class="am">AM</li>
                                                        <li class="pm">PM</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>

                                    <div class="row">
                                        <div class="com-sm-12 col-lg-12">
                                            <div class="form-group">
                                                <!-- <label>Gig End Date</label> -->
                                                <label>Twitter Trends End Date</label>
                                                <input id="gig_end_date" name="gig_end_date" value="{{old('gig_end_date')}}" type="text" class="form-control" />
                                                <span id="gig_end_date-error" class="error text-danger" for="gig_end_date" style="display: none;">{{$errors->first('gig_end_date')}}</span>
                                            </div>
                                        </div>

                                        <!-- <div class="com-sm-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Gig End Time</label>
                                                <div class="form-control">
                                                    <div class="quantity">
                                                        <input type="number" min="1" max="12" step="1" value="1" class="time-input" />
                                                        <div class="quantity-nav">
                                                            <button type="button" class="quantity-button quantity-up">&#xf106;</button>
                                                            <button type="button" class="quantity-button quantity-down">&#xf107;</button>
                                                            <div class="time-text">Hrs</div>
                                                        </div>
                                                    </div>

                                                    <div class="quantity">
                                                        <input type="number" min="1" max="60" step="1" value="1" class="time-input" />
                                                        <div class="quantity-nav">
                                                            <button type="button" class="quantity-button quantity-up">&#xf106;</button>
                                                            <button type="button" class="quantity-button quantity-down">&#xf107;</button>
                                                            <div class="time-text">Min</div>
                                                        </div>
                                                    </div>

                                                    <ul class="meridian">
                                                        <li class="am">AM</li>
                                                        <li class="pm">PM</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="post-heading">
                        <h3>Content Details</h3>
                    </div>

                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Hashtag <span>(Include hashtags that will be attached to the created content e.g. #twiva #yourproductlaunch)</span></label>
                                        <input name="hashtag" id="hashtag" maxlength="30" type="text" class="form-control" required />
                                        <span id="hashtag-error" class="error text-danger" for="hashtag">{{$errors->first('hashtag')}}</span>
                                    </div>
                                    <div class="form-group" style="display:none;">
                                        <label>Address</label>
                                        <input name="address" id="address" value="NA {{old('address')}}" type="text" class="form-control" required />
                                        <span id="address-error" class="error" for="address">{{$errors->first('address')}}</span>
                                    </div>

                                    <!-- <div class="form-group">
                                        <label>Add link for your content <span>(Include links that will be help influencers for creating a Twitter Trend)</span></label>
                                        <input type="text" class="form-control">
                                    </div> -->
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group" style="display:none;">
                                        <label>Venue</label>
                                        <input id="venue" name="venue" value="NA {{old('venue')}}" type="text" class="form-control" required />
                                        <span id="venue-error" class="error text-danger" for="venue">{{$errors->first('venue')}}</span>
                                    </div>
                                    <div class="form-group">
                                        @php($placeHoldUrl = url('public/business/images/add.png'))
                                        <label>Add a Theme <span>(Include any photo/video to be used as an inspiration for the Influencer)</span></label>
                                        <div class="form-control custom-form p-0" style="height: auto;">
                                            <label id="filediv" class="file-upload">
                                                <input type="hidden" name="non_acceptable_files" class="non_acceptable_files" />
                                                <input type="hidden" class="ext_media_record" images="0" video="0" total-media="0" />
                                                <input name="image_video[]" type="file" id="file" />
                                            </label>

                                            <input type="button" id="add_more" class="upload" value="Add" style="border: 0;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="btn-wrapper">
                        <button type="submit" class="red-btn">Next</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
@endsection @section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript" src="{{url('public/business/js/multiFileUpload.js')}}"></script>
<script src="{{ asset('business/assets/js/moment.min.js') }}"></script>
<script src="{{ asset('business/assets/js/daterangepicker.min.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.js"></script> -->
<script>
    $.validator.addMethod(
        "is_zero",
        function (val, elm) {
            if (val && val.trim().length > 0) {
                let sum = val
                    .toString()
                    .split("")
                    .map(Number)
                    .reduce(function (a, b) {
                        return a + b;
                    }, 0);
                if (sum <= 0) {
                    return false;
                }
            }
            return true;
        },
        "Price should be equal or more than 1"
    );
    $("#form_validate").validate({
        rules: {
            venue: {
                required: true,
                maxlength: 100,
            },
            address: {
                required: true,
                maxlength: 1000,
            },

            gig_price_offering: {
                required: true,
                minlength: 1,
                maxlength: 8,
                is_zero: true,
            },
            hashtag: {
                required: true,
            },
        },

        messages: {
            venue: {
                required: "Please enter venue.",
                maxlength: "venue maximum 100 characters long.",
            },
            address: {
                required: "Please enter address.",
                maxlength: "Address maximum 1000 characters long.",
            },

            gig_price_offering: {
                required: "Please enter compensation.",
                minlength: "Compensation must be at least 1 digits.",
                maxlength: "Compensation maximum 8 digits.",
            },
            hashtag: {
                required: "Please enter hashtag.",
            },
        },
        submitHandler: function (form) {
            if ($("#email-error").text().length > 0) {
                return false;
            } else {
                form.submit();
            }
        },
        /*errorHandler:function(e){
            console.log(e)
            }*/
    });
    $(document).ready(function () {
        console.log("document ready!");

        var $sticky = $(".sticky");
        var $stickyrStopper = $(".col-md-3.left_col.menu_fixed");
        if (!!$sticky.offset()) {
            // make sure ".sticky" element exists
            var generalSidebarHeight = $sticky.innerHeight();
            var stickyTop = $sticky.offset().top;
            var stickOffset = 0;
            var stickyStopperPosition = $stickyrStopper.offset().top;
            var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
            var diff = stopPoint + stickOffset;

            $(window).scroll(function () {
                // scroll event
                var windowTop = $(window).scrollTop(); // returns number

                if (stopPoint < windowTop) {
                    $sticky.css({ position: "absolute", top: diff });
                } else if (stickyTop < windowTop + stickOffset) {
                    $sticky.css({ position: "fixed", top: stickOffset });
                } else {
                    $sticky.css({ position: "absolute", top: "initial" });
                }
            });
        }
    });
    function isNumberKey(evt) {
        var charCode = evt.which ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) return false;

        return true;
    }
    $("#name").on("keypress", function (event) {
        let key = event.keyCode;
        if (key == 32) {
            return false;
        }
    });
    $(document).ready(function () {
        setTimeout(() => {
            $(".alert").fadeOut("slow");
        }, 6000);
    });

    /* show hide # in hashtag on focus in and focus out */
    /* show hide # in hashtag on focus in and focus out */
    $("#hashtag").on("click paste keypress", function () {
        let val = $(this).val();
        if (val[0] != "#") {
            $(this).val("#" + val);
        }
    });

    $("#hashtag")
        .on("keydown", function (e) {
            if (e.keyCode === 8) {
                let val = $(this).val();
                if (val && val.trim().length == 1) {
                    return val == "#" ? false : true;
                }
            }
            return true;
        })
        .on("select", function () {
            $("#hashtag").on("keyup", function (e) {
                let val = $(this).val();
                if (val[0] != "#") {
                    $(this).val("#" + val);
                }
            });
        })
        .bind("cut", function () {
            setTimeout(function () {
                let val = $("#hashtag").val();
                if (val[0] != "#") {
                    $("#hashtag").val("#" + val);
                }
            }, 100);
        });

    // $("#hashtag").focusout(function () {
    //     let val = $(this).val();
    //     if (val[0] == "#") {
    //         $(this).val(val.substring(1));
    //     }
    // });

    $(document).ready(function () {
        $("#hashtag").on("keypress", function (event) {
            let key = event.keyCode;
            if (key == 32) {
                return true;
            }
        });
    });
    $(function () {
        $('input[name="gig_start_date"]').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            showDropdowns: false,
            minYear: 1901,
            locale: {
                format: "DD/MM/YYYY hh:mm A",
            },
        });
        $('input[name="gig_end_date"]').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            showDropdowns: false,
            minYear: 1901,
            locale: {
                format: "DD/MM/YYYY hh:mm A",
            },
        });
    });
</script>
<!--   <script>
     function validateForm(){
      $("#invalid_file").hide();
      if($("input[name='image_video[]']").val() == ""){
        $("#invalid_file").show().text("Please upload theme photo.");
        return false;
      }else{
         $("#invalid_file").hide();  
      }
    }    
   </script> -->
@endsection
