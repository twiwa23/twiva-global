@extends('business.layout.app')
@section("title","Gigs List")
@section('content')
<style>
    .influencer-list {
        justify-content: flex-end;
    }
    .text-theme-primary{
        color: #8A0E49;
    }
    div.completed p{
        width: fit-content;
        padding: 4px 8px;
        background: #E0FFE5;
        border-radius: 2px;
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 150%;
        color: #27AE60;
    }
</style>
<div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>
    
                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->
                </div>
            </div>
            <!-- Left Column End -->
            <!-- page content -->
            <div class="right_col dashboard-page" role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/business/gig-list')}}" class="active">Twitter Trends<img src="{{ asset('assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        <li><a href="#">{{$gig_detail->gig_name}}</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
                
                <div class="feedpost-bidding">
                    <div class="feedpost-heading">
                        <h1>{{$gig_detail->gig_name}}</h1>
                        <div class="btn-section">
                            @if($gig_detail->gig_influencer_user_count != 0 && ($gig_detail->gig_influencer_user_count == $gig_detail->gig_completed_user))
                                <div class="completed"><p>Completed</p></div>
                            @else
                                <p class="ongoing">Ongoing</p>
                            @endif
                            
                            <!-- <p class="ongoing">Ongoing</p> -->
                            <!-- <a href="" class="ongoing">Ongoing</a> -->
                            <!-- <a href="">
                                <img src="{{ asset('assets/images/icons/bxs-edit.svg')}}" alt="">
                            </a> -->
                        </div>
                    </div>

                    <h4>{{$gig_detail->description}}</h4>
                    <div class="date-wrapper">
                        <div class="date">
                            <p><span>Start Date</span> {{date('d/m/y', strtotime($gig_detail->gig_start_date_time))}}</p>
                            <p><img src="{{ asset('assets/images/icons/clock.svg')}}" alt="">{{date('h:m A', strtotime($gig_detail->gig_start_date_time))}}</p>
                        </div>
                    
                        <div class="date">
                            <p><span>End Date</span> {{date('d/m/y', strtotime($gig_detail->gig_end_date_time))}}</p>
                            <p><img src="{{ asset('assets/images/icons/clock.svg')}}" alt="">{{date('h:m A', strtotime($gig_detail->gig_end_date_time))}}</p>
                        </div>
                    </div>
                    
                </div>

                <!-- <div class="you-hired">
                    <h2>You hired</h2>
                    <div class="img-list">
                        <div class="img-section">
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>
                            <div class="img-wrap">
                                <img src="{{ asset('assets/images/profile-images/profile-2.png')}}" alt="">
                            </div>

                            <a href="" class="more">+6 more</a>
                        </div>
                        <button class="red-btn">Check them out <img src="{{ asset('assets/images/icons/chevron-down.svg')}}" alt=""></button>
                    </div>
                </div> -->

                <div class="influencer-list">
                    <!-- <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#invitations">Invitations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#bids">Bids</a>
                        </li>
                    </ul> -->
                    <a href="{{url('business/invite-influencers-gig')}}/{{$id}}/1"><button class="red-btn">Invite More Influencers</button></a>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="invitations">
                        <div class="catalog-page">
                            <div class="select-top select-search">
                                <div class="search-wrap">
                                    <!-- <div class="search-panel">
                                        <input type="text" placeholder="Search here...">
                                    </div> -->
            
                                    <div class="button-catalog">
                                        <!-- <div class="drop-btn">
                                            <div class="select-btn">
                                                <div class="btn__trigger">All</div>
                                                <div class="btn-options">
                                                    <span class="btn-option" data-value="All">All</span>
                                                    <span class="btn-option" data-value="5-Star">5-Star</span>
                                                    <span class="btn-option" data-value="4-Star">4-Star</span>
                                                    <span class="btn-option" data-value="3-Star">3-Star</span>
                                                    <span class="btn-option" data-value="2-Star">2-Star</span>
                                                    <span class="btn-option" data-value="1-Star">1-Star</span>
                                                </div>
                                                <img src="{{ asset('assets/images/icons/down-arrow.svg')}}" alt="">
                                            </div>
                                        </div>
        
                                        <div class="drop-btn">
                                            <div class="select-btn">
                                                <div class="btn__trigger">Price Range</div>
                                                <div class="btn-options">
                                                    <span class="btn-option" data-value="All">All</span>
                                                    <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                                    <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                                    <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                                </div>
                                                <img src="{{ asset('assets/images/icons/down-arrow.svg')}}" alt="">
                                            </div>
                                        </div>
        
                                        <div class="drop-btn">
                                            <div class="select-btn">
                                                <div class="btn__trigger">All Status</div>
                                                <div class="btn-options">
                                                    <span class="btn-option" data-value="All Status">All Status</span>
                                                    <span class="btn-option" data-value="Invite Sent">Invite Sent</span>
                                                    <span class="btn-option" data-value="Pending">Pending</span>
                                                    <span class="btn-option" data-value="Low Pay">Low Pay</span>
                                                    <span class="btn-option" data-value="Declined">Declined</span>
                                                </div>
                                                <img src="{{ asset('assets/images/icons/down-arrow.svg')}}" alt="">
                                            </div>
                                        </div>

                                        <div class="drop-btn">
                                            <div class="select-btn">
                                                <div class="btn__trigger">Invitations</div>
                                                <div class="btn-options">
                                                    <span class="btn-option" data-value="Invitations">Invitations</span>
                                                    <span class="btn-option" data-value="Invite Sent">Invite Sent</span>
                                                    <span class="btn-option" data-value="Pending">Pending</span>
                                                    <span class="btn-option" data-value="Low Pay">Low Pay</span>
                                                    <span class="btn-option" data-value="Declined">Declined</span>
                                                </div>
                                                <img src="{{ asset('assets/images/icons/down-arrow.svg')}}" alt="">
                                            </div>
                                        </div> -->
        
                                    </div>
        
                                </div>
                            </div>
        
                            <div class="table-responsive">
                                <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>IMAGE</th>
                                            <th>NAME</th>
                                            <th>STAR RATING</th>
                                            <th>RATE</th>
                                            <th>FOLLOWERS</th>
                                            <th>STATUS</th>
                                            <th>ACTIONS</th>
                                        </tr>
                                    </thead>
        
                                    <tbody>
                                        @if($gigDetailsList)
                                            @foreach($gigDetailsList as $gig)
                                                <tr>
                                                    <td>
                                                        <div class="user-image">
                                                            @php $userImage = url('public/business/images/upload-one.png'); 
                                                                foreach($gig->user->images as $images){
                                                                    if($images->image && !empty($images->image)){ 
                                                                            $userImage = $images->image; break; 
                                                                    }
                                                                }
                                                            @endphp
                                                            <img src="{{$userImage}}" alt="">
                                                        </div>
                                                    </td>
                                                    <td class="user-name">
                                                    {{$gig->user->name != "" ? $gig->user->name : ($gig->InfluencerDetailNew ? $gig->InfluencerDetailNew['name'] : 'N/A')}}</td>
                                                    <td>
                                                        <div class="user-ratings">
                                                        @if(count($gig->user->infulancerRating)>0)
                                                            <?php $ratings = round($gig->user->infulancerRating->avg('rating'));?>
                                                            @for($i=0;$i<$ratings;$i++)
                                                            <img src="./assets/images/icons/star1.svg" alt="">
                                                            @endfor
                                                        @else
                                                            Not Rated
                                                        @endif
                                                        </div>
                                                    </td>
                                                    <td class="post-rate">Ksh {{$gig->price ?? '0'}}</td>
                                                    <td>
                                                        <div class="social-media">
                                                            <a href="#" class="followers">
                                                                <img src="{{ asset('assets/images/icons/insta.svg')}}" alt="">
                                                                <p>{{conversionIntoKformat($gig->influencer_social_details->facebook_friends ?? 0)}}</p>
                                                            </a>
                                                            <a href="#" class="followers">
                                                                <img src="{{ asset('assets/images/icons/fb.svg')}}" alt="">
                                                                <p>{{conversionIntoKformat($gig->influencer_social_details->instagram_friends ?? 0)}}</p>
                                                            </a>
                                                            <a href="#" class="followers">
                                                                <img src="{{ asset('assets/images/icons/twit.svg')}}" alt="">
                                                                <p>{{conversionIntoKformat($gig->influencer_social_details->twitter_friends ?? 0)}}</p>
                                                            </a>
                                                        </div>
                                                    </td>
                                                    @php
                                                        if($gig->status == '1' && $gig->is_checkin == '0') {
                                                            $status = "Accepted"; 
                                                            $class = "completed";
                                                        } else if($gig->status == '1' && $gig->is_checkin == '1'){
                                                            $status = "Confirm Check In";
                                                            $class = "completed";
                                                        }else if($gig->status == '3'){
                                                            $status = "Check In Approved";
                                                            $class = "completed";
                                                        }else if($gig->status == '5'){
                                                            $status = "Pending";
                                                            $class = "ongoing";
                                                        }else if($gig->status == '2'){
                                                            $status = "Declined";
                                                            $class = "declined";
                                                        }else if($gig->status == '4'){
                                                            $status = "Completed";
                                                            $class = "completed";
                                                        }
                                                    @endphp
                                                    <td class="{{$class}}">
                                                        <p>{{$status}}</p>    
                                                    </td>
                                                    
                                                    <td>
                                                        @if($gig->status == '5')
                                                            <button class="hire-me-btn send-reminder-btn" data-gig_id="{{base64_encode($id)}}" data-influencer_id="{{base64_encode($gig->user_id)}}"><i class="fa fa-spinner fa-spin d-none text-theme-primary"></i> Send Reminder</button>
                                                        @endif
                                                        @if($gig->status == '1' && $gig->is_checkin == '1')
                                                            <button class="hire-me-btn confirm-checkin-btn" data-gig_id="{{base64_encode($id)}}" data-influencer_id="{{base64_encode($gig->user_id)}}"><i class="fa fa-spinner fa-spin d-none text-theme-primary"></i> Confirm Checkin</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
        
                                </table>
                            </div>
        
                            <div class="pagination">
                                <div class="col-sm-5">
                                    <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                                    <div class="">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                        {{$gigDetailsList->appends(request()->except('page'))->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>


                
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('.send-reminder-btn').on('click', function(){
                let _this = $(this);
                _this.find('i').removeClass('d-none');
                _this.attr('disabled', true);
                let gig_id = _this.attr('data-gig_id')
                let influencer_id = _this.attr('data-influencer_id')
                $.ajax({
                    url: `{{url("/business/gig-reminder")}}/${gig_id}/${influencer_id}`,
                    type: 'POST',
                    dataType: 'json',
                    success: function (result) {   // success callback function
                        _this.find('i').addClass('d-none');
                        _this.attr('disabled', false);
                    }
                })
            });

            $(".confirm-checkin-btn").on('click' , function(){
                let _this = $(this);
                _this.find('i').removeClass('d-none');
                _this.attr('disabled', true);
                let gig_id = _this.attr('data-gig_id');
                let influencer_id = _this.attr('data-influencer_id');
                var base_url = window.location.origin;
                $.ajax({
                    url: `{{url("/business/confirm-check-in-request")}}/${gig_id}/${influencer_id}`,
                    type: 'POST',
                    dataType: 'json',
                    success: function (result) {                       
                        window.location = `{{url("/business/gig-details")}}/${gig_id}`;
                        // $(".confirm-checkin-btn").hide();
                        alert('Check In Approved');
                    }
                });
            });
        })
    </script>
@endsection
