@extends('business.layout.app')
@section("title","Gigs List")
@section('content')
<div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>
    
                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->
                </div>
            </div>
            <!-- Left Column End -->
            <!-- page content -->
            <div class="right_col dashboard-page" role="main">

                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="breadcrumb-heading">Twitter Trends</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->

                <div class="catalog-page">
                    <div class="select-top select-search" id="feedpost">
                        <div class="search-wrap">
                            <!-- <div class="search-panel">
                                <input type="text" placeholder="Search here...">
                            </div> -->
    
                            <div class="button-catalog">
                                <div class="drop-btn-wrapper">
                                    <!-- <div class="drop-btn" style="width: 85px;">
                                        <div class="select-btn" style="width: 100%;">
                                            <div class="btn__trigger">Date</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="Date1">Date1</span>
                                                <span class="btn-option" data-value="Date2">Date2</span>
                                                <span class="btn-option" data-value="Date3">Date3</span>
                                                <span class="btn-option" data-value="Date4">Date4</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div>
    
                                    <div class="drop-btn" style="width: 92px;">
                                        <div class="select-btn" style="width: 100%;">
                                            <div class="btn__trigger">Status</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="Status1">Status1</span>
                                                <span class="btn-option" data-value="Status2">Status2</span>
                                                <span class="btn-option" data-value="Status3">Status3</span>
                                                <span class="btn-option" data-value="Status4">Status4</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div> -->
                                </div>
                                <a href="{{ url('business/create-gig') }}" class="create-btn">Create Twitter Trends</a>
                                
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>IMAGE</th>
                                    <th>TITLE</th>
                                    <th>START DATE</th>
                                    <th>END DATE</th>
                                    <th>ACCEPTED</th>
                                    <th>PENDING</th>
                                    <th>DECLINED</th>
                                    <th>STATUS</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if(count($gig_list)>0)
                                @foreach($gig_list as $gig_lists)
                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                @if(!empty($gig_lists->Images[0]->media))
                                                    <img src="{{$gig_lists->Images[0]->media}}" alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>
                                                @else
                                                    <img src="{{url('public/business/images/default.svg')}}" alt="upload-one" class="img-responsive image" title="Click to expand image/video">
                                                @endif 
                                            </div>
                                        </td>
                                        <td class="title">{{$gig_lists->gig_name ? $gig_lists->gig_name : 'N/A'}}</td>
                                        <td>{{$gig_lists->gig_start_date_time ?? 'N/A'}}</td>
                                        <td>{{$gig_lists->gig_end_date_time ?? 'N/A'}}</td>
                                        <td>{{$gig_lists->gig_accepted_user}}</td>
                                        <td>{{$gig_lists->gig_pending_user}}</td>
                                        <td>{{$gig_lists->gig_declined_user}}</td>
                                        @if($gig_lists->gig_influencer_user_count != 0 && ($gig_lists->gig_influencer_user_count == $gig_lists->gig_completed_user))
                                            <td class="completed">
                                                <p>Completed</p>    
                                            </td>
                                        @else
                                            <td class="ongoing">
                                                <p>Ongoing</p>
                                            </td>
                                        @endif
                                        
                                        <td class="action text-center">
                                            <a href="{{url('business/gig-details',base64_encode($gig_lists->id))}}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                <td class="text-center p-5 border border-0" colspan="9">
                                    <img src="{{asset('influencers/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                    <h3 style="font-size: 20px;">You haven’t created any Twitter Trend yet</h3>
                                </td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="pagination">
                        <div class="col-sm-5">
                            <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                            <div class="">
                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                    {{$gig_list->appends(request()->except('page'))->links('vendor.pagination.custom') }}
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        setTimeout(function(){ functionTobeLoaded() }, 200);

        function functionTobeLoaded(){
            $('.pagination li:first-child').addClass('white-btn');
            $(".pagination li:last-child").addClass("red-btn");
            $('.pagination li:first-child span').text('Previous');
            $('.pagination li:first-child a').text('Previous');
            $('.pagination li:last-child span').text('Next');
            $('.pagination li:last-child a').text('Next');
        }
    });
</script>
@endsection