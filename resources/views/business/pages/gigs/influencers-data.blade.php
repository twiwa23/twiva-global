<div class="table-responsive">
                        <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>    
                                    <th style="width: 50px;">
                                        <label class="checkbox-container">
                                            <input type="checkbox" id="select-all">
                                            <span class="checkmark" style="background-color:transparent;border: 1px solid #556B86;"></span>
                                        </label>
                                    </th>
                                    <th>IMAGE</th>
                                    <th>NAME</th>
                                    <th>STAR RATING</th>
                                    <th>RATE</th>
                                    <th>FOLLOWERS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if(count($getGigs)>0)
                                @foreach($getGigs as $gig)
                                    <tr>
                                        <td>
                                            <label class="checkbox-container">
                                                <input id="item-{{$gig->id}}" class="influencer-select" type="checkbox" data-text="" data-name='{{$gig->name != "" ? $gig->name : ($gig->InfluencerDetailNew ? $gig->InfluencerDetailNew["name"] : "")}}' data-id="{{$gig->id}}" data-price="{{$gigPrice}}">
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="user-image">
                                                @if(count($gig->images) > 0)
                                                    <img id="image-{{$gig->id}}" src="{{ $gig->images[0]->image }}" alt="">
                                                @else
                                                    <img id="image-{{$gig->id}}" src="{{ url('public/business/images/upload-one.png') }}" alt="">
                                                @endif
                                            </div>
                                        </td>
                                        <td class="user-name"><a href="{{url('business/influencer-details',base64_encode($gig->id))}}"> {{$gig->name != "" ? $gig->name : ($gig->InfluencerDetailNew ? $gig->InfluencerDetailNew['name'] : '')}} </a></td>
                                        <td>
                                            <div class="user-ratings">
                                            @if(count($gig->infulancerRating)>0)
                                                <?php $ratings = round($gig->infulancerRating->avg('rating'));?>
                                                @for($i=0;$i<$ratings;$i++)
                                                <img src="{{asset('business/assets/images/icons/star1.svg')}}" alt="">
                                                @endfor
                                            @else
                                                Not Rated
                                            @endif
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh {{$gig->gig_price != '' ? $gig->gig_price : 0}}</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="{{asset('business/assets/images/icons/insta.svg')}}" alt="">
                                                    <p>{{conversionIntoKformat($gig->instagram_friends)}}</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="{{asset('business/assets/images/icons/fb.svg')}}" alt="">
                                                    <p>{{conversionIntoKformat($gig->facebook_friends)}}</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="{{asset('business/assets/images/icons/twit.svg')}}" alt="">
                                                    <p>{{conversionIntoKformat($gig->twitter_friends)}}</p>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="action text-center">
                                            <button data-toggle="modal" data-target="#adjust-price-modal" class="hire-me-btn adjust-price" data-name='{{$gig->name != "" ? $gig->name : ($gig->InfluencerDetailNew ? $gig->InfluencerDetailNew["name"] : "")}}' data-id="{{$gig->id}}" data-price="{{$gigPrice}}">
                                                Adjust Price
                                            </button>
                                        </td>
                                        <!-- <td class="completed">
                                            <p>Invite Sent</p>    
                                        </td> -->
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center p-5 border border-0" colspan="7">
                                        <img src="{{asset('business/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                        <h3 style="font-size: 20px;">No Data Found!</h3>
                                    </td>
                                </tr> 
                            @endif
                            </tbody>

                        </table>
                    </div>

                    <div class="pagination">
                        <div class="col-sm-5">
                            <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                            <div class="">
                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                    {{$getGigs->links()}}
                                    <script>
                                        $(document).ready(function(){
                                            setTimeout(function(){ functionTobeLoaded() }, 200);

                                            function functionTobeLoaded(){
                                                $('.pagination li:first-child').addClass('white-btn');
                                                $(".pagination li:last-child").addClass("red-btn");
                                                $('.pagination li:first-child span').text('Previous');
                                                $('.pagination li:first-child a').text('Previous');
                                                $('.pagination li:last-child span').text('Next');
                                                $('.pagination li:last-child a').text('Next');
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>