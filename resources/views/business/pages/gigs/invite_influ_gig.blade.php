@extends('business.layout.app')
@section("title","Gigs List")
@section('content')
<style>
    ul.inlfuencer-lists{
        overflow-y: auto;
        max-height: 75vh;
    }
    .bg-red-theme{
        background-color: #7A0D41;
        color: #fff;
        border-color: #7A0D41;
    }
    .phone-section .modal-footer{
        justify-content: space-around !important;
    }
    #adjust-price-modal .modal-dialog .modal-content .modal-header .modal-title {
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 20px;
        line-height: 25px;
        text-align: center;
        color: #333333;
    }
    #adjust-price-modal .modal-dialog {
        max-width: 479px;
    }
    #adjust-price-modal .modal-dialog .modal-content .modal-body .image img{
        width: 70px;
        object-fit: cover;
        border-radius: 50%;
    }
    #adjust-price-modal .modal-dialog .modal-content .modal-body .influ-name{
        font-size: 25px;
    }
</style>
<div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>
    
                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->
                </div>
            </div>
            <!-- Left Column End -->
            <!-- page content -->
            <div class="right_col dashboard-page" role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                    <li><a href="#" class="active">Twitter Trends<img src="{{asset('business/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                    <li><a href="">Invite Influencers</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->

                <div class="influencer-list">
                    <h4>Influencers List</h4>
                    <button disabled="" class="red-btn invite-influencer-btn" data-toggle="modal" data-target="#payment-modal">Invite Influencers</button>
                    
                    <!-- <a href="#" class="red-btn" style="background-color:rgba(122,13,65,0.6);" data-toggle="modal" data-target="#payment-modal">Invite Influencers</a> -->
                </div>
                
                <div class="catalog-page">
                    <div class="select-top select-search">
                        <div class="search-wrap">
                            <div class="search-panel">
                                <input type="text" placeholder="Search here..." id="search">
                            </div>
    
                            <!-- <div class="button-catalog">
                                <div class="drop-btn">
                                    <div class="select-btn">
                                        <div class="btn__trigger">All</div>
                                        <div class="btn-options">
                                            <span class="btn-option" data-value="All">All</span>
                                            <span class="btn-option" data-value="5-Star">5-Star</span>
                                            <span class="btn-option" data-value="4-Star">4-Star</span>
                                            <span class="btn-option" data-value="3-Star">3-Star</span>
                                            <span class="btn-option" data-value="2-Star">2-Star</span>
                                            <span class="btn-option" data-value="1-Star">1-Star</span>
                                        </div>
                                        <img src="{{asset('business/assets/images/icons/down-arrow.svg')}}" alt="">
                                    </div>
                                </div>

                                <div class="drop-btn">
                                    <div class="select-btn">
                                        <div class="btn__trigger">Price Range</div>
                                        <div class="btn-options">
                                            <span class="btn-option" data-value="All">All</span>
                                            <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                            <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                            <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                        </div>
                                        <img src="{{asset('business/assets/images/icons/down-arrow.svg')}}" alt="">
                                    </div>
                                </div>
                            </div> -->

                        </div>
                    </div>
                    <div id="table_data">
                        @include('business/pages/gigs/influencers-data')
                    </div>
                    
                    
                </div>

            </div>
            <div class="modal fade" id="payment-modal">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="inlfuencer-lists-section">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title text-capitalize">SUMMARY</h4>
                                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                            </div>
                    
                            <!-- Modal body -->
                            <div class="modal-body">
                                <ul class="inlfuencer-lists">
                                </ul>
                            </div>
                    
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="white-btn" data-dismiss="modal">Cancel</button>
                                <a href="javascript:void(0);" class="red-btn payment-complete"><i class="fa fa-spinner fa-spin d-none text-white"></i> Complete Payment</a>
                            </div>
                        </div>
                        <div class="phone-section d-none">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title text-capitalize">M-Pesa registerd number</h4>
                            </div>
                    
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="phone-from-inline">
                                    <form class="from-inline">
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-red-theme" id="basic-addon3">+254</span>
                                            </div>
                                            <input type="text" class="form-control" id="phone_number" name="phone_number" aria-describedby="basic-addon3">
                                        </div>
                                        <span class="text-danger error error-phone d-none">Phone number is required.</span>
                                    </form>
                                </div>
                            </div>
                    
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <a href="javascript:void(0);" class="red-btn make-payment-complete"><i class="fa fa-spinner fa-spin d-none text-white"></i> Make Payment</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="adjust-price-modal">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="inlfuencer-lists-section">
                            <!-- Modal Header -->
                            <div class="modal-header justify-content-center">
                                <h4 class="modal-title text-capitalize">Please enter the amount for selected Influencer</h4>
                            </div>
                    
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form class="text-center">
                                    <div class="image my-4">
                                        <img class="influ-image" src="http://127.0.0.1:8000/public/images/dummy_user.jpg" alt="">
                                    </div>
                                    <p class="influ-name my-3">Jon Doe</p>
                                    <div class="form-group text-left">
                                        <input placeholder="Price" type="number" class="form-control" id="influ-price"/>
                                        <span class="text-danger error-price d-none">Price is required</span>
                                    </div>
                                    <div class="form-group">
                                        <input placeholder="Complementary message" type="text" class="form-control" id="influ-text"/>
                                    </div>
                                </form>
                            </div>
                    
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="white-btn d-flex align-items-center" data-dismiss="modal" style="height: 40px;">Cancel</button>
                                <a href="javascript:void(0);" class="red-btn save-adjust-price"><i class="fa fa-spinner fa-spin d-none text-white"></i> Save Price</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="{{$id}}" id="gig_id">
            <!-- /page content -->
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            setTimeout(function(){ functionTobeLoaded() }, 200);

            function functionTobeLoaded(){
                $('.pagination li:first-child').addClass('white-btn');
                $(".pagination li:last-child").addClass("red-btn");
                $('.pagination li:first-child span').text('Previous');
                $('.pagination li:first-child a').text('Previous');
                $('.pagination li:last-child span').text('Next');
                $('.pagination li:last-child a').text('Next');
            }
        });
    </script>

    <script>
        $(document).ready(function(){
            var urlParams = new URLSearchParams(window.location.search);
            if(urlParams.get('page') == 1 || urlParams.get('page') == null){
                localStorage.removeItem('selectedInflu')
            }
            let selectedItems = localStorage.getItem('selectedInflu');
            selectedItems = selectedItems ? JSON.parse(selectedItems) : [];
            if(selectedItems.length == $('.influencer-select').length){
                $(`#select-all`).prop('checked', true);
            }
            if(selectedItems.length > 0){
                $('.invite-influencer-btn').removeAttr('disabled');
            }
            selectedItems.map((items)=>{
                $(`#item-${items.id}`).prop('checked', true);
            })
            var selectedInflu = selectedItems;
            $(document).on('change', '#select-all', function() {
                let _this = $(this)
                $('.influencer-select').each(function(){
                    $(this).prop("checked",_this.prop('checked'));
                    let items = {
                        id: $(this).attr('data-id'),
                        name: $(this).attr('data-name'),
                        price: $(this).attr('data-price'),
                        image: $(`#image-${$(this).attr('data-id')}`).attr('src'),
                        text: $(this).attr('data-text')
                    };
                    let isExist = selectedInflu.length > 0 ? selectedInflu.find(detail => detail.id == items.id) : null;
                    if(this.checked) {
                        if(!isExist){
                            selectedInflu.push(items);
                        }
                    }else{
                        let removeIndex = selectedInflu.map(item => item.id).indexOf(items.id);
                        selectedInflu.splice(removeIndex, 1);
                    }
                });
                if(selectedInflu.length > 0){
                    $('.invite-influencer-btn').removeAttr('disabled');
                }else{
                    $('.invite-influencer-btn').attr('disabled');
                }
                localStorage.setItem('selectedInflu', JSON.stringify(selectedInflu))
            })
            $(document).on('change', '.influencer-select', function() {
                let items = {
                    id: $(this).attr('data-id'),
                    name: $(this).attr('data-name'),
                    price: $(this).attr('data-price'),
                    image: $(`#image-${$(this).attr('data-id')}`).attr('src'),
                    text: $(this).attr('data-text')
                };
                let isExist = selectedInflu.length > 0 ? selectedInflu.find(detail => detail.id == items.id) : null;
                if(this.checked) {
                    if(!isExist){
                        selectedInflu.push(items);
                    }
                }else{
                    let removeIndex = selectedInflu.map(item => item.id).indexOf(items.id);
                    selectedInflu.splice(removeIndex, 1);
                }
                localStorage.setItem('selectedInflu', JSON.stringify(selectedInflu));

                if(selectedInflu.length > 0){
                    $('.invite-influencer-btn').removeAttr('disabled');
                }else{
                    $('.invite-influencer-btn').prop('disabled',true);
                }
            });
            $(document).on('click', '.invite-influencer-btn', function(){
                $('.inlfuencer-lists').html('');
                let totalprice = 0;
                let totalinfluencers = 0;
                for (let index = 0; index < selectedInflu.length; index++) {
                    const element = selectedInflu[index];
                    totalinfluencers++;
                    totalprice = totalprice + parseInt(element.price);
                    let listData = `
                        <li class="list-item" data-id="${element.id}">
                            <div class="image">
                                <img src="${element.image}" alt="">
                            </div>
                            <h5>${element.name}</h5>
                            <p class="inf-price" data-id="${element.id}">Ksh${element.price}<p>
                            <p class="inf-text-message" data-id="${element.id}">${element.text}<p>
                        </li>
                    `;
                    $('.inlfuencer-lists').append(listData)
                }
                let totalinfo = `<div class="font-weight-bold">Total Amount :  KSH ${totalprice}</div> <div class="font-weight-bold">Selected Influencers :  ${totalinfluencers}</div>`;
                    $('.inlfuencer-lists').append(totalinfo);
            });
            $(document).on('click', '.adjust-price', function(){
                $('.error-price').addClass('d-none');
                $('#influ-image').attr('scr', $(`#image-${$(`#item-${$(this).attr('data-id')}`).attr('data-id')}`).attr('src'))
                $('.influ-name').text($(`#item-${$(this).attr('data-id')}`).attr('data-name'))
                $('#influ-price').val($(`#item-${$(this).attr('data-id')}`).attr('data-price'))
                $('#influ-text').val($(`#item-${$(this).attr('data-id')}`).attr('data-text'))
                $('.save-adjust-price').attr('data-id',$(`#item-${$(this).attr('data-id')}`).attr('data-id'))
            });
            $(document).on('click', '.save-adjust-price', function(){
                if($('#influ-price').val() == "" || $('#influ-price').val() <= 0){
                    $('.error-price').removeClass('d-none');
                    return false;
                }else{
                    $('.error-price').addClass('d-none');
                }
                let selectedIndex = selectedInflu.map(item => item.id).indexOf($(this).attr('data-id'));
                if(selectedIndex != -1){
                    selectedInflu[selectedIndex].price = $('#influ-price').val();
                    selectedInflu[selectedIndex].text = $('#influ-text').val();
                }
                $(`#item-${$(this).attr('data-id')}`).attr('data-text',$('#influ-text').val()).attr('data-price',$('#influ-price').val());
                $('#adjust-price-modal').modal('hide');
            });
            $('body').on("keyup",'.inf-price', function(){
                let id = $(this).attr('data-id');
                let index = selectedInflu.map(item => item.id).indexOf(id);
                selectedInflu[index].price = $(this).val();
            });
            $('body').on("keyup",'.inf-text-message', function(){
                let id = $(this).attr('data-id');
                let index = selectedInflu.map(item => item.id).indexOf(id);
                selectedInflu[index].text = $(this).val();
            });
            $(document).on('click', '.payment-complete', function(){
                let _this = $(this);
                _this.find('i').removeClass('d-none');
                $.ajax({
                    url: '{{url("/business/set-mpesa-number")}}',
                    type: 'GET',
                    dataType: 'json',
                    success: function (result) {   // success callback function
                        $('.inlfuencer-lists-section').addClass('d-none');
                        $('.phone-section').removeClass('d-none');
                        $('#phone_number').val(result.phone_number);
                        _this.find('i').addClass('d-none');
                    }
                })
                console.log(selectedInflu,'selectedInflu');
            })
            $(document).on('click', '.make-payment-complete', function(){
                let _this = $(this);
                let phone = $('#phone_number').val();
                let gig_id = $('#gig_id').val();
                if(phone == ''){
                    $('#phone_number').addClass('border-danger');
                    $('.error-phone').removeClass('d-none');
                    return false;
                }else{
                    $('#phone_number').removeClass('border-danger');
                    $('.error-phone').addClass('d-none');
                }
                _this.attr('disabled', false);
                _this.find('i').removeClass('d-none');
                $.ajax({
                    url: '{{url("/business/set-mpesa-number")}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        gig_id,
                        phone,
                        selectedInfluencers :  selectedInflu
                    },
                    success: function (result) {   // success callback function
                        if(result.status==false){
                            swal({
                            title: "Payment failed",
                            text: result.message,
                            icon: "error",
                            button: "Ok",
                        })
                        .then((value) => {
                            //window.location.href = `{{'/business/invite-influencers-gig/${gig_id}'}}`

                            window.location.href = `{{'/business/gig-list'}}`
                        });
                        _this.find('i').addClass('d-none');
                        }else{
                            swal({
                            title: "Twitter Trends Created",
                            text: result.message,
                            icon: "success",
                            button: "Ok",
                        })
                        .then((value) => {
                            //window.location.href = `{{'/business/invite-influencers-gig/${gig_id}'}}`

                            window.location.href = `{{'/business/gig-list'}}`
                        });
                        _this.find('i').addClass('d-none');
                        }

                    }
                })
            })
        })

    </script>

<script>
$(document).ready(function(){
 $(document).on('click', '.pagination a', function(event){
  event.preventDefault(); 
  var page = $(this).attr('href').split('page=')[1];
  fetch_data(page);
 });

 function fetch_data(page)
 {
  var gig_id = $("#gig_id").val();
  var query=$("#search").val();
  var type = 1;
  $.ajax({
   url:"/business/get-influencers-gig/"+gig_id+"/"+type+"?page="+page,
   data:{'query': query},
   success:function(getGigs)
   {
    $('#table_data').html(getGigs);
   }
  });
 }
});
$('#search').on('keyup',function(){
var gig_id = $("#gig_id").val();
var query=$(this).val();
var type = 1;
$.ajax({
type : 'get',
url:"/business/get-influencers-gig/"+gig_id+"/"+type,
data:{'query': query},
success:function(getGigs){
    $('#table_data').html(getGigs);
}
});
});

</script>
@endsection