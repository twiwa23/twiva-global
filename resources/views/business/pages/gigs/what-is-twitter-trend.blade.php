@extends('business.layout.app') @section("title","What is twitter trend?") @section('content') @section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
<div class="" id="business-dashboard">
    <div class="dashboard_container">
        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->
            </div>
        </div>
        <!-- Left Column End -->
        <!-- page content -->
        <div class="right_col dashboard-page" role="main" id="approved-page">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('business/gig-list') }}" class="active">Twitter Trends<img src="./assets/images/icons/chevron-right-gray.svg" alt=""></a></li>
                        <li><a href="#">What is a Twitter Trend?</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
                
                <div class="what-is-post">
                    <h4>Gigs allows influencers and creatives (such as a comedian or photographer) to agree to attend an in-person event and exchange their social media value (i.e. Instagram stories) for possible payment or in-kind services (i.e., free admission, etc.). Gigs also allow you to run a campaign on Twitter to promote your business/products/services with the aim to trend and drive traffic to your business.</h4>

                    <div class="invite-steps">
                        <ul class="invite-list">
                            <li class="invite-list-item active">
                                <div class="number">1</div>
                                <p>Fill Details</p>
                            </li>
                            <li class="invite-list-item">
                                <div class="number">2</div>
                                <p>Invite</p>
                            </li>
                            <li class="invite-list-item">
                                <div class="number">3</div>
                                <p>Payment</p>
                            </li>
                            <li class="invite-list-item">
                                <div class="number">4</div>
                                <p>Track</p>
                            </li>
                        </ul>


                        <div class="tab-content">
                            <div class="tab-pane invite-list-content active">
                                <h5>We advise you to include all possible details (e.g., how many Instagram stories, what they should bring and do etc. )</h5>
                            </div>
                            <div class="tab-pane invite-list-content">
                                <h5>Invite specific influencers and set their compensation and/or in-kind products.</h5>
                            </div>
                            <div class="tab-pane invite-list-content">
                                <h5>Twiva holds payment to influencers and only releases once the influencer(s)/creative(s) have completed the ask successfully. If a Gig is not completed, money will be refunded to your account.You can release payment the next day, or it will automatically do so within 24 hours of the event/gig.</h5>
                            </div>
                            <div class="tab-pane invite-list-content">
                                <h5>Influencers will arrive/check-in at your event/campaign at the start time specified.You will be notified on your Inbox. They can then enjoy the event and check-out once they complete the Gig. This applies to those showing up virtually as well.</h5>
                            </div>
                        </div>

                        

                        <button class="red-btn" id="next-step">Next</button>
                        <a href="{{ url('business/create-gig') }}" class="red-btn text-center" id="create">Create Twitter Trend</a>
                    </div>
                </div>


                
            </div>
        <!-- /page content -->
    </div>
</div>
@endsection 
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript" src="{{url('public/business/js/multiFileUpload.js')}}"></script>
<script src="{{ asset('business/assets/js/moment.min.js') }}"></script>
<script src="{{ asset('business/assets/js/daterangepicker.min.js') }}"></script>
@endsection
