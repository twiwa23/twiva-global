@extends('business.layout.app')
@section('title')
    Hire
@endsection
@section('content')

<div class="influencers-profile">
        <div class="dashboard_container">
            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>
    
                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->

            <!-- Page Content -->
            <div class="right_col " role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('business/portfolio/'.$user->id) }}" class="active">{{ $user->name }}<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                        <li><a href="#">Hire Me</a></li>
                    </ul>
                </div>
                <!--********** Breadcrumb End ***********-->

                <div class="post-container hire-me">
                    <div class="post-heading">
                        <h3>Hire Me</h3>
                    </div>
                @include('business.layout.notifications')
                <form method = "post" action  = "" >
					 {{csrf_field()}}
                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>From</label>
                                        <input type="text" name ="email" value="{{old('email')}}"  class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>To</label>
                                        <input type="text" name="to" value="{{$user->name}}" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Subjects</label>
                                        <input type="text" name = "subject" value="{{old('subject')}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea name="text" value="{{old('text')}}" id="" rows="5" class="form-control c-text"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="btn-wrapper">
                        <button class="red-btn">Send</button>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('text');
    CKEDITOR.config.autoParagraph = false;

</script>
@endsection
