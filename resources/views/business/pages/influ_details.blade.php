@extends('business.layout.app')
@section('title')
Influencer Details
@endsection
@section('content')
<div class="backdrop"></div>

    <div class="influencers-profile">
        <div class="dashboard_container">
            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->

            <!-- Page Content -->
            <div class="right_col dashboard-page" role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{url('business/influencers-list') }}" class="active">Influencers<img src="{{ asset('business/assets/') }}/images/icons/chevron-right-gray.svg" alt=""></a></li>
                        <li><a href="#">@if(!empty($infulancerDetail->name)){{$infulancerDetail->name}}'s Profile @else N/A @endif</a></li>
                    </ul>
                </div>
                <!--********** Breadcrumb End ***********-->

                <div class="profile-container">
                    <div class="cover-image">
                        <img src="{{ asset('business/assets/images/profile-images/cover-image.png') }}" alt="">
                    </div>

                    <div class="profile-info">
                        <div class="profile-image">
                        @if($infulancerDetail->InfluencerDetail) <img src="{{ config('app.dev_storage_url') . $infulancerDetail->influencerDetail->profile_image }}" alt="" >
                        @else
                        <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}" alt="" >
                        @endif
                            <!-- <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}" alt=""> -->
                        </div>

                        <div class="profile-desc">
                            <div class="profile-content">
                                <div class="ratings">
                                    <h1 class="name">@if(!empty($infulancerDetail->name)){{$infulancerDetail->name}}'s Profile @else N/A @endif</h1>
                                    <div class="ratings-wrapper">
                                            @if(count($infulancerDetail->infulancerRating)>0)
                                                <?php $ratings = round($infulancerDetail->infulancerRating->avg('rating'));?>
                                                @for($i=0;$i<$ratings;$i++)
                                                <img src="{{asset('business/assets/images/icons/star1.svg')}}" alt="">
                                                @endfor
                                            @else
                                                Not Rated
                                            @endif
                                    </div>
                                </div>
                                <?php
                                if(isset($infulancerDetail->SocialDetail->facebook_friends)) {
                                   if($infulancerDetail->SocialDetail->facebook_friends >= 1000000){
                                    $facebook_friends = number_format(($infulancerDetail->SocialDetail->facebook_friends / 1000000))."M";
                                   }else if($infulancerDetail->SocialDetail->facebook_friends >= 1000){
                                    $facebook_friends = number_format(($infulancerDetail->SocialDetail->facebook_friends / 1000))."K";
                                   }else {
                                    $facebook_friends = $infulancerDetail->SocialDetail->facebook_friends;
                                   }
                                }else{$facebook_friends=0;}


                                if(isset($infulancerDetail->SocialDetail->twitter_friends)) {
                                   if($infulancerDetail->SocialDetail->twitter_friends >= 1000000){
                                    $twitter_friends = number_format(($infulancerDetail->SocialDetail->twitter_friends / 1000000))."M";
                                   }else if($infulancerDetail->SocialDetail->twitter_friends >= 1000){
                                    $twitter_friends = number_format(($infulancerDetail->SocialDetail->twitter_friends / 1000))."K";
                                   }else {
                                    $twitter_friends = $infulancerDetail->SocialDetail->twitter_friends;
                                   }
                                }else{$twitter_friends=0;}

                                if(isset($infulancerDetail->SocialDetail->instagram_friends)) {
                                   if($infulancerDetail->SocialDetail->instagram_friends >= 1000000){
                                    $instagram_friends = number_format(($infulancerDetail->SocialDetail->instagram_friends / 1000000))."M";
                                   }else if($infulancerDetail->SocialDetail->instagram_friends >= 1000){
                                    $instagram_friends = number_format(($infulancerDetail->SocialDetail->instagram_friends / 1000))."K";
                                   }else {
                                    $instagram_friends = $infulancerDetail->SocialDetail->instagram_friends;
                                   }
                                }else{$instagram_friends=0;}

                             ?>
                                <div class="social-media">
                                    <a href="#" class="followers">
                                        <img src="{{ asset('business/assets/images/icons/insta.svg') }}" alt="">
                                        <p>@if(!empty($instagram_friends)){{$instagram_friends}} @else 0 @endif <span>Followers</span></p>
                                    </a>
                                    <a href="#" class="followers">
                                        <img src="{{ asset('business/assets/images/icons/fb.svg') }}" alt="">
                                        <p>@if(!empty($facebook_friends)){{$facebook_friends}} @else 0 @endif <span>Followers</span></p>
                                    </a>
                                    <a href="#" class="followers">
                                        <img src="{{ asset('business/assets/images/icons/twit.svg') }}" alt="">
                                        <p>@if(!empty($twitter_friends)){{$twitter_friends}} @else 0 @endif <span>Followers</span></p>
                                    </a>
                                </div>
                            </div>

                            <div class="location">
                                {{-- <img src="{{ asset('business/assets/images/icons/location.svg') }}" alt=""><span>New Zealand</span> --}}
                            </div>

                            <div class="about">
                                <p> @if(!empty($infulancerDetail->influencerDetail->about)){{$infulancerDetail->influencerDetail->about}} @else N/A @endif</p>
                            </div>

                            <div class="hire-me">
                                <div class="cost">
                                    <div>
                                        <h4>KSh{{$infulancerDetail->price->plug_price ?? '0' }}</h4>
                                        <p><img src="{{ asset('business/assets/images/icons/tablet.svg') }}" alt="">Desired Cost per Post</p>
                                    </div>
                                    <div>
                                        <h4>@if(!empty($infulancerDetail->price->gig_price)){{'KSh'.$infulancerDetail->price->gig_price}} @else KSh0 @endif</h4>
                                        <p><img src="{{ asset('business/assets/images/icons/info1.svg') }}" alt="">Cost for Twitter Trend</p>
                                    </div>

                                </div>

                                {{-- <a href="#" class="hire-me-btn">Hire me</a> --}}
                            </div>

                            <div class="img-lists">
                            @foreach($interests as $interestDatas)
                                <div class="list-item">
                                    <div class="img-wrap">
                                        <!-- @if(!empty($interestDatas->image))
                                        <?php //$image = $interestDatas->image ?>
                                            <img src="{{$image}}" alt="upload-one" class="img-responsive"/>
                                        @else
                                            <img src="{{ asset('public/business/images/upload-one.png') }}" alt="upload-one" class="img-responsive">
                                        @endif
                                        {{-- <img src="{{ asset('business/assets/images/profile-images/Rectangle681.png') }}" alt=""> --}} -->
                                        <img src="{{ asset('public/business/images/upload-one.png') }}" alt="upload-one" class="img-responsive">
                                    </div>
                                    <p>@if(!empty($interestDatas->name)){!!html_entity_decode(substr($interestDatas->name,0,30))!!} @else  @endif</p>
                                </div>
                            @endforeach

                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
