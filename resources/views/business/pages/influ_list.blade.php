@extends('business.layout.app')
@section('title')
Influencers
@endsection
@section('content')

<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main" id="influencer-page">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{url('business/influencers-list') }}" class="breadcrumb-heading">Influencers</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->

            <div class="catalog-page">
                <div class="select-top select-search">
                    <div class="search-wrap">
                        @isset($_GET['search'])
                        <a href="{{url('business/influencers-list')}}" style="position: absolute;right: 64px;top: 10px;color: black;">
                            <i class="fa fa-times"></i>
                        </a>
                        @endif
                        <div class="search-panel">
                            <input type="text" placeholder="Search here..." name="search" class="form-control search" id="search" value="{{ isset($_GET['search']) ? $_GET['search'] : ''}}">
                        </div>

                        <div class="button-catalog">
                            <!-- <div class="drop-btn">
                                <button type="submit" id="search_infl" class="red-btn" style="cursor: pointer;">Search</button>
                            </div> -->
                            {{-- <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">All</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="5-Star">5-Star</span>
                                        <span class="btn-option" data-value="4-Star">4-Star</span>
                                        <span class="btn-option" data-value="3-Star">3-Star</span>
                                        <span class="btn-option" data-value="2-Star">2-Star</span>
                                        <span class="btn-option" data-value="1-Star">1-Star</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div> --}}

                            {{-- <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">Price Range</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                        <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                        <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div> --}}
                        </div>

                    </div>
                </div>

                <div id="influencers">
                   
                </div>

            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
@endsection
@section('scripts')



<script type="text/javascript">
    $( document ).ready(function() {


     $("#search_infl").click(function() {
       let search = $("#search").val();
       console.log(search);
       if(search) {
         window.location.href = "{{ url('business/influencers-list') }}"+"?search="+search;
       }
     })
    console.log( "document ready!" );
    });
 </script>

<script>
$(document).ready(function(){

//   var query=$("#search").val();
  $.ajax({
   url:"/business/fetch-influencers",
//    data:{'query': query},
   success:function(infulancer_list)
   {
    $('#influencers').html(infulancer_list);
   }
  });

 $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  fetch_data(page);
 });

 function fetch_data(page)
 {
  var query=$("#search").val();
  $.ajax({
   url:"/business/fetch-influencers?page="+page,
   data:{'query': query},
   success:function(infulancer_list)
   {
    $('#influencers').html(infulancer_list);
   }
  });
 }
});
$('#search').on('keyup',function(){
var query=$(this).val();
$.ajax({
type : 'get',
url:"/business/fetch-influencers",
data:{'query': query},
success:function(infulancer_list){
    $('#influencers').html(infulancer_list);
}
});
});



// setTimeout(function(){ functionTobeLoaded() }, 1000);

// function functionTobeLoaded(){
//     $(".pagination li:last-child").addClass("red-btn");
//     $('.pagination li:first-child').addClass('white-btn');
//     $('.pagination li:first-child span').text('Previous');
//     $('.pagination li:last-child a').text('Next');
// }



// jQuery(document.body).on('click','.pagination li:last-child', function(e) { 
//     alert("Hi");
//     // jQuery('html').addClass('menu-open');
// });

</script>
@endsection
