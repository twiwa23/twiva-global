<div class="table-responsive">
                    <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>IMAGE</th>
                                <th>NAME</th>
                                <th>STAR RATING</th>
                                <th>POST RATE</th>
                                <th>TWITTER TREND RATE</th>
                                <th>FOLLOWERS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>

                        <tbody>
                        @if(count($infulancer_list)>0)
                            @foreach ($infulancer_list as $influencer)
                            <tr>
                                <td>
                                    <div class="user-image">
                                    @if(isset($influencer->InfluencerDetail))
                                        @if($influencer->InfluencerDetail->profile_image) <img src="{{ config('app.dev_storage_url') . $influencer->influencerDetail->profile_image }}" alt="" >
                                        @else
                                        <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}" alt="" >
                                        @endif
                                    @else
                                    <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}" alt="" >
                                    @endif
                                    </div>
                                </td>
                                <td class="user-name"><a href="{{url('business/influencer-details',base64_encode($influencer->id))}}">@if(!empty($influencer->name)){{ substr($influencer->name,0,15)}} @else User @endif</a></td>
                                <td>
                                    <div class="user-ratings">
                                        @if(count($influencer->infulancerRating)>0)
                                            <?php $ratings = round($influencer->infulancerRating->avg('rating'));?>
                                            @for($i=0;$i<$ratings;$i++)
                                            <img src="./assets/images/icons/star1.svg" alt="">
                                            @endfor
                                        @else
                                            Not Rated
                                        @endif
                                    </div>
                                </td>
                                <td class="post-rate">{{ $influencer->plug_price ? 'KSh'.$influencer->plug_price : 'KSh0' }}</td>
                                <td class="post-rate">{{ $influencer->gig_price ? 'KSh'.$influencer->gig_price : 'KSh0' }}</td>
                                <?php
                                    if(isset($influencer->SocialDetail->facebook_friends)) {
                                       if($influencer->SocialDetail->facebook_friends >= 1000000){
                                        $facebook_friends = number_format(($influencer->SocialDetail->facebook_friends / 1000000))."M";
                                       }else if($influencer->SocialDetail->facebook_friends >= 1000){
                                        $facebook_friends = number_format(($influencer->SocialDetail->facebook_friends / 1000))."K";
                                       }else {
                                        $facebook_friends = $influencer->SocialDetail->facebook_friends;
                                       }
                                    }else{$facebook_friends=0;}


                                    if(isset($influencer->SocialDetail->twitter_friends)) {
                                       if($influencer->SocialDetail->twitter_friends >= 1000000){
                                        $twitter_friends = number_format(($influencer->SocialDetail->twitter_friends / 1000000))."M";
                                       }else if($influencer->SocialDetail->twitter_friends >= 1000){
                                        $twitter_friends = number_format(($influencer->SocialDetail->twitter_friends / 1000))."K";
                                       }else {
                                        $twitter_friends = $influencer->SocialDetail->twitter_friends;
                                       }
                                    }else{$twitter_friends = 0;}

                                    if(isset($influencer->SocialDetail->instagram_friends)) {
                                       if($influencer->SocialDetail->instagram_friends >= 1000000){
                                        $instagram_friends = number_format(($influencer->SocialDetail->instagram_friends / 1000000))."M";
                                       }else if($influencer->SocialDetail->instagram_friends >= 1000){
                                        $instagram_friends = number_format(($influencer->SocialDetail->instagram_friends / 1000))."K";
                                       }else {
                                        $instagram_friends = $influencer->SocialDetail->instagram_friends;
                                       }
                                    }else{$instagram_friends = 0;}

                                 ?>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="{{ asset('business/assets/images/icons/insta.svg') }}" alt="">
                                            <p>@if(!empty($instagram_friends)){{$instagram_friends}} @else 0 @endif</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="{{ asset('business/assets/images/icons/fb.svg') }}" alt="">
                                            <p>@if(!empty($facebook_friends)){{$facebook_friends}} @else 0 @endif</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="{{ asset('business/assets/images/icons/twit.svg') }}" alt="">
                                            <p>@if(!empty($twitter_friends)){{$twitter_friends}} @else 0 @endif</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <!-- <a href="#" class="hire-me-btn">Hire me</a> -->
                                    <a href="{{url('business/influencer-details',base64_encode($influencer->id))}}">
                                        <button class="hire-me-btn">View</button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center p-5 border border-0" colspan="7">
                                    <img src="{{asset('business/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                    <h3 style="font-size: 20px;">No Data Found!</h3>
                                </td>
                            </tr>   
                        @endif 


                        </tbody>

                    </table>
                </div>

                <div class="pagination">
                    <div class="col-sm-5">
                        <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                        <div class="">
                            <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                {{-- <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example_previous">
                                        <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                            <img src="./assets/images/icons/button.svg" alt="">
                                        </a>
                                    </li>

                                    <li class="paginate_button active">
                                        <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                    </li>

                                    <li class="paginate_button next" id="example_next">
                                        <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                            <img src="./assets/images/icons/Right.svg" alt="">
                                        </a>
                                    </li>

                                </ul> --}}
                                {{ $infulancer_list->links() }}
                                <script>
                                    $(document).ready(function(){
                                        setTimeout(function(){ functionTobeLoaded() }, 200);

                                        function functionTobeLoaded(){
                                            $('.pagination li:first-child').addClass('white-btn');
                                            $(".pagination li:last-child").addClass("red-btn");
                                            $('.pagination li:first-child span').text('Previous');
                                            $('.pagination li:first-child a').text('Previous');
                                            $('.pagination li:last-child span').text('Next');
                                            $('.pagination li:last-child a').text('Next');
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>