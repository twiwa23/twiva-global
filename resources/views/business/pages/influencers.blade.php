@extends('business.layout.app')
@section('content')
<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-heading">Influencers</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->
            
            <div class="catalog-page">
                <div class="select-top select-search">
                    <div class="search-wrap">
                        <div class="search-panel">
                            <input type="text" placeholder="Search here...">
                        </div>

                        <div class="button-catalog">
                            <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">All</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="5-Star">5-Star</span>
                                        <span class="btn-option" data-value="4-Star">4-Star</span>
                                        <span class="btn-option" data-value="3-Star">3-Star</span>
                                        <span class="btn-option" data-value="2-Star">2-Star</span>
                                        <span class="btn-option" data-value="1-Star">1-Star</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div>

                            <div class="drop-btn">
                                <div class="select-btn">
                                    <div class="btn__trigger">Price Range</div>
                                    <div class="btn-options">
                                        <span class="btn-option" data-value="All">All</span>
                                        <span class="btn-option" data-value="Ksh1000-Ksh5000">Ksh1000-Ksh5000</span>
                                        <span class="btn-option" data-value="Ksh6000-Ksh9000">Ksh6000-Ksh9000</span>
                                        <span class="btn-option" data-value="Above Ksh10000">Above Ksh10000</span>
                                    </div>
                                    <img src="./assets/images/icons/down-arrow.svg" alt="">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="table-responsive">
                    <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>IMAGE</th>
                                <th>NAME</th>
                                <th>STAR RATING</th>
                                <th>POST RATE</th>
                                <th>TWITTER TREND RATE</th>
                                <th>FOLLOWERS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="user-image">
                                        <img src="./assets/images/profile-images/profile-2.png" alt="">
                                    </div>
                                </td>
                                <td class="user-name">Ronald Richards</td>
                                <td>
                                    <div class="user-ratings">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                        <img src="./assets/images/icons/star1.svg" alt="">
                                    </div>
                                </td>
                                <td class="post-rate">Ksh2500</td>
                                <td class="post-rate">Ksh3000</td>
                                <td>
                                    <div class="social-media">
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/insta.svg" alt="">
                                            <p>2K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/fb.svg" alt="">
                                            <p>1K</p>
                                        </a>
                                        <a href="#" class="followers">
                                            <img src="./assets/images/icons/twit.svg" alt="">
                                            <p>50K</p>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="hire-me-btn">Hire me</a>
                                </td>
                            </tr>

                        </tbody>

                    </table>
                </div>

                <div class="pagination">
                    <div class="col-sm-5">
                        <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                        <div class="">
                            <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example_previous">
                                        <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                            <img src="./assets/images/icons/button.svg" alt="">
                                        </a>
                                    </li>

                                    <li class="paginate_button active">
                                        <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                    </li>

                                    <li class="paginate_button ">
                                        <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                    </li>

                                    <li class="paginate_button next" id="example_next">
                                        <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                            <img src="./assets/images/icons/Right.svg" alt="">
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
@endsection
