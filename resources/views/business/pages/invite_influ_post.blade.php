@extends('business.layout.app')
@section('title')
    Invite Influencers
@endsection
@section('content')
<style>
    ul.inlfuencer-lists{
        overflow-y: auto;
        max-height: 75vh;
    }
    .bg-red-theme{
        background-color: #7A0D41;
        color: #fff;
        border-color: #7A0D41;
    }
    .phone-section .modal-footer{
        justify-content: space-around !important;
    }
    #adjust-price-modal .modal-dialog .modal-content .modal-header .modal-title {
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 20px;
        line-height: 25px;
        text-align: center;
        color: #333333;
    }
    #adjust-price-modal .modal-dialog {
        max-width: 479px;
    }
    #adjust-price-modal .modal-dialog .modal-content .modal-body .image img{
        width: 70px;
        object-fit: cover;
        border-radius: 50%;
    }
    #adjust-price-modal .modal-dialog .modal-content .modal-body .influ-name{
        font-size: 25px;
    }
    #register-number .modal-dialog .modal-content .modal-header .modal-title {
    font-family: Source Sans Pro;
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
    line-height: 25px;
    text-align: center;
    color: #333333;
    margin-bottom: 15px;
}
.modal-dialog {
    max-width: 475px !important;
}
.modal .modal-dialog .modal-content .modal-body {
    margin-bottom: 15px !important;
}
</style>
<div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->

            <!-- page content -->
            <div class="right_col dashboard-page" role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        <li><a href="{{ url('business/create-post-field') }}" class="active">Create Post<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        {{-- <li><a href="#" class="active">Invite<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li> --}}
                        <li><a href="#">Invite Influencers</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->

                <div class="influencer-list">
                    <h4>Influencers List</h4>
                    <button class="red-btn" id="invite" data-toggle="modal" data-target="#payment-modal" disabled>Invite Influencers</button>

                    <!-- <a href="#" class="red-btn" style="background-color:rgba(122,13,65,0.6);" data-toggle="modal" data-target="#payment-modal">Invite Influencers</a> -->
                </div>
                @include('business.layout.notifications')
                <div class="catalog-page">
                    <div class="select-top select-search">
                        <div class="search-wrap">
                            <div class="search-panel">
                                <input type="text" placeholder="Search here..." id="search-input">
                            </div>

                            <div class="button-catalog">
                                {{-- <div class="drop-btn">
                                    <div class="select-btn">
                                        <div class="btn__trigger">All</div>
                                        <div class="btn-options">
                                            <span class="btn-option" data-value="All">All</span>
                                            <span class="btn-option" data-value="5-Star">5-Star</span>
                                            <span class="btn-option" data-value="4-Star">4-Star</span>
                                            <span class="btn-option" data-value="3-Star">3-Star</span>
                                            <span class="btn-option" data-value="2-Star">2-Star</span>
                                            <span class="btn-option" data-value="1-Star">1-Star</span>
                                        </div>
                                        <img src="{{ asset('business/assets/images/icons/down-arrow.svg')}}" alt="">
                                    </div>
                                </div> --}}

                                {{-- <div class="drop-btn">
                                    <div class="select-btn">
                                        <div class="btn__trigger">Price Range</div>
                                        <div class="btn-options">
                                            <span class="btn-option" data-value="All">All</span>
                                            <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                            <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                            <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                        </div>
                                        <img src="{{ asset('business/assets/images/icons/down-arrow.svg')}}" alt="">
                                    </div>
                                </div> --}}
                            </div>

                        </div>
                    </div>

                    <div class="table-responsive">
                        <input type="hidden" id="postPrice" value="{{ $postprice }}">
                        <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="checkbox-container">
                                            <input type="checkbox" id="select_all">
                                            <span class="checkmark" style="background-color:transparent;border: 1px solid #556B86;"></span>
                                        </label>
                                    </th>
                                    <th>IMAGE</th>
                                    <th>NAME</th>
                                    {{-- <th>STAR RATING</th> --}}
                                    <th>RATE</th>
                                    <th>FOLLOWERS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>

                            <tbody class="post_list">
                                <tr>
                                    <td>
                                        <label class="checkbox-container">
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="user-image">
                                            <img src="{{ asset('business/assets/images/profile-images/profile-2.png')}}" alt="">
                                        </div>
                                    </td>
                                    <td class="user-name">Ronald Richards</td>
                                    {{-- <td>
                                        <div class="user-ratings">
                                            <img src="{{ asset('business/assets/images/icons/star1.svg')}}" alt="">
                                            <img src="{{ asset('business/assets/images/icons/star1.svg')}}" alt="">
                                            <img src="{{ asset('business/assets/images/icons/star1.svg')}}" alt="">
                                            <img src="{{ asset('business/assets/images/icons/star1.svg')}}" alt="">
                                            <img src="{{ asset('business/assets/images/icons/star1.svg')}}" alt="">
                                        </div>
                                    </td> --}}
                                    <td class="post-rate">Ksh2500</td>
                                    <td>
                                        <div class="social-media">
                                            <a href="#" class="followers">
                                                <img src="{{ asset('business/assets/images/icons/insta.svg')}}" alt="">
                                                <p>2K</p>
                                            </a>
                                            <a href="#" class="followers">
                                                <img src="{{ asset('business/assets/images/icons/fb.svg')}}" alt="">
                                                <p>1K</p>
                                            </a>
                                            <a href="#" class="followers">
                                                <img src="{{ asset('business/assets/images/icons/twit.svg')}}" alt="">
                                                <p>50K</p>
                                            </a>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>

                        </table>
                    </div>

                    <div class="pagination">
                        <div class="col-sm-5">
                            <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                            <div class="">
                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                    {{-- <ul class="pagination">
                                        <li class="paginate_button previous disabled" id="example_previous">
                                            <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                                <img src="{{ asset('business/assets/images/icons/button.svg')}}" alt="">
                                            </a>
                                        </li>

                                        <li class="paginate_button active">
                                            <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                        </li>

                                        <li class="paginate_button next" id="example_next">
                                            <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                                <img src="{{ asset('business/assets/images/icons/Right.svg')}}" alt="">
                                            </a>
                                        </li>

                                    </ul> --}}
                                    <section class="login_FORM summary_deials infu_btns">
               <div class="form-group Submit medium_btn m_top">
                  <input type="hidden" name="selected_users" id="selected_users">
                  <input type="hidden" name="users_price" id="users_price">
                  {{-- <input type="button" name="Login" value="PROCEED" class="btn btn-primary" id="proceed-btn" ref = "{{url('business/post-summary-page').'/'.Request::segment(3) }}" /> --}}
                  <div class="show-more">
                     <a class="white-btn" href="" style="padding: 12px;" id="next-btn" />Show More</a>
                  </div>
               </div>
            </section>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /page content -->
        </div>
    </div>


    <!-- The Modal -->
    <div class="modal fade" id="adjust-price-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title text-capitalize">Please enter the amount for selected Influencers</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item d-flex align-items-center">
                            <div class="image">
                                {{-- <img src="{{ asset('business/assets/images/profile-images/Ellipse.png')}}" alt=""> --}}
                            </div>
                            {{-- <h5>Ronald Richards</h5> --}}
                            <p class="font-weight-bold">Ksh</p>
                            <input type="hidden" id="set_uid" />
                            <div class="w-50 ml-2">
                                <input type="tel" maxlength="6" id="set_price" class="form-control" required min="0"/>
                                <span class="text-danger"></span>
                            </div>
                        </li>
                    </ul>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="white-btn" data-dismiss="modal" style="height: 44px;">Cancel</button>
                    <button type="button" value="UPDATE" class="red-btn ml-2" id="update_price">Update</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="payment-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title text-capitalize">SUMMARY</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists final-list">
                        {{-- <li class="list-item">
                            <div class="image">
                                <img src="./assets/images/profile-images/Ellipse.png" alt="">
                            </div>
                            <h5>Ronald Richards</h5>
                            <p>Ksh</p>
                            <input type="number">
                        </li> --}}
                    </ul>
                </div>
               
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="white-btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="red-btn" id="confirm_booking" style="color:white;margin-left: 10px;">Complete Payment</button>
                </div>

            </div>
        </div>
    </div>

      <!-- Payment Model -->
<div id="paymentPop" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" id="close-pop">&times;</button>
          <h2 class="modal-title text-center" style="font-size: 24px;font-weight: bold;">
            Make Payment
          </h2>
        </div>
        <div class="modal-body">
          <div class="form-group" id="success-pop" style="display: none;">
            <div style="padding:11px;font-weight: 400;" class="alert alert-success text-center">
              Please proceed for the payment to create a Post.
            </div>
          </div>
          <span id="res-message"></span>
          <input type="hidden" id="created_id" />
     <!--      <div class="form-group" id="mPesa-container" style="display: none;">
            <input type="number" id="mphone" class="form-control" />
            <label class="error"></label>
          </div> -->
             <div class="form-group" id="mPesa-container" style="display: none;">
              <div class="form-group">
              <div class="email">
                 <input type="text" id="mphone" name="title" value="" class="form-control" placeholder="" maxlength="15" onkeypress="return onlyNumber(event)">
                 <span class="text-danger"></span>
              </div>
               <label class="error"></label>
           </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="save-mphone">Submit</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="register-number">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="phone-section">
                            <!-- Modal Header -->
                            <div class="modal-header justify-content-center">
                                <h4 class="modal-title">M-Pesa Registered Number</h4>
                            </div>
                    
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="phone-from-inline">
                                    <form class="from-inline">
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-red-theme" id="basic-addon3">+254</span>
                                            </div>
                                            <input type="text" class="form-control" id="phone_number" name="phone_number" aria-describedby="basic-addon3">
                                        </div>
                                        <span class="text-danger error error-phone d-none">Phone number is required.</span>
                                    </form>
                                </div>
                            </div>
                    
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <a href="javascript:void(0);" class="red-btn make-payment-complete"><i class="fa fa-spinner fa-spin d-none text-white"></i> Make Payment</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="post_id" value="{{base64_decode($id)}}">
            <input type="hidden" id="influencers">

@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        setInterval(() => {
            let users_selected = localStorage.getItem("_curSelUserPrs")
            if(!users_selected || users_selected == null) {
                $('#invite').attr('disabled', true);
            } else {
                if(users_selected.length >= 1) {
                    $('#invite').attr('disabled', false);
                } else {
                    $('#invite').attr('disabled', true);
                }
            }

        }, 3000);

    });
</script>

      <script type="text/javascript">

        /* set old values of selected users */
        $("#users_price").val(localStorage.getItem("_curSelUserPrs"))
        $("#selected_users").val(localStorage.getItem("_curSelUser"))
        /* make unselect checkbox*/
        $("#select_all").attr("checked",false)
        $("#select_all").prop("checked",false)
        
        function highLightContainer(element,type){
            // console.log($('.checkbox-container input').is(':checked'));
        // console.log("trigger")
        // console.log($(this))
        if(type == "add"){
            $(".checkbox-container input .perfomselectionchange").addClass('checkmark');
          element.addClass("active-selection").removeClass("lightBorderHighLight")
        }else if(type == "remove"){
            $(".checkbox-container input").attr('checked', false);
           element.removeClass("active-selection").addClass("lightBorderHighLight")
        }
      }


      function parseK(input){
        input = parseInt(input)
        if(input >= 1000000){
          return parseFloat((input / 1000000).toFixed(2)) + "M";
        }else if(input >= 1000){
          return parseFloat((input / 1000).toFixed(2)) + "K";
        }else {
          return input;
        }
      }


        // static price set by gig creator
        const  postPrice = $('#postPrice').val();
        function pushPopUsers(id){
          /* if user id exist then remove id else add user id*/
          let val = $("#selected_users").val();
          let ids = [];
          if(val != ""){
            ids = val.split(",");
          }

          let index = ids.indexOf(id);
          if(index == -1){
            ids.push(id)
          }else {
            ids.splice(index,1);
          }
          $("#selected_users").val(ids.join(","))

          localStorage.setItem("_curSelUser",$("#selected_users").val())
        }

        // select all users
        $("#select_all").click(function(){
          let users = [];
          if($(this).prop("checked")){
            $("#users_price").val("")
            $(".post_list").find(".gig_container").each(function(){
              users.push($(this).attr("uid"))
              setPrice($(this).attr("uid"),postPrice,"add", "", $(this).attr("name"))
              pushPopUsers($(this).attr("uid"))
              $(".checkbox-container input").prop('checked', true);
            })
            // $(document).find(".perfomselectionchange").text("UNSELECT")
            // $(".checkbox-container input").removeAttr('checked');
            $("#selected_users").val(users.join(","));
          }else {
            $("#selected_users").val("");
            $("#users_price").val("")
            localStorage.removeItem("_curSelUserPrs")
            localStorage.removeItem("_curSelUser")
            // $(".perfomselectionchange").attr('checked', false);
            $(".checkbox-container input").prop('checked', false);

            // $(document).find(".perfomselectionchange").text("SELECT")
          }
        })
        /* Adjust price pop up*/
        $(document).on("click",'.adjust_popup',function(){
        //   if($(this).parent().parent().find(".perfomselectionchange").attr('checked') == false){
        //     alert("Please select user for adjust price.");
        //     return false;
        //   }
        // $(".checkbox-container input").attr('checked', true);

        if($(this).parent().parent().find(".checkbox-container input").is(':checked')){
            console.log("selected");
            console.log($(this).data("uid"));
            let uid = $(this).data("uid");
            $("#set_uid").val(uid);

            let getDetail = getPrice("get",uid)

            $("#set_price").val(getDetail.price);
            $("#set_compliment").val(getDetail.compliment != null && getDetail.compliment != undefined ? getDetail.compliment : '');

            $('#adjust-price-modal').modal({
                backdrop: 'static'
            });
        }
        else{
            console.log('Not Selected');
            let ans = alert("Please select user for adjust price");
            setTimeout(() => {
                $('#adjust-price-modal').hide();
                $('.modal-backdrop').hide();
                // $("#adjust-price-modal").modal('dispose')

            }, 200);
            return false;
        }

        });

        /* set updated price for user*/

        $(document).on("click","#update_price",function(){
          let price = $("#set_price").val();
          let compliment = $("#set_compliment").val();
          if(compliment == ""){
            compliment = $("#set_compliment").text();
          }
          let uid = $("#set_uid").val();
          if(price == "" || price.trim().length == 0){
            alert("Please enter price");
            return false;
          }else if(isNaN(price)){
              alert("Please enter number only");
              return false;
          } else if(price <= 0){
            alert("Price should be equal or higher then 1");
            return false;
          }

          setPrice(uid,price,"update",compliment);
          $("#adjust-price-modal").modal("hide")

        })

        /* text change and mark checked if all users are selected or vice-versa*/
        $(document).on('click','.perfomselectionchange',function(){
              let id = $(this).closest(".gig_container").attr("uid")
              let name = $(this).closest(".gig_container").attr("name")
            if($('.checkbox-container input').is(':checked')){
            //   $(this).text("UNSELECT");
              $(this).attr('checked', false)
              setPrice(id,postPrice,"add", "", name)
            }else {
            //   $(this).text("SELECTin");
              $(this).attr('checked', true)
              setPrice(id,null,"remove")
            }

            pushPopUsers(id)

            let getAllSelected = $("#selected_users").val().split(",").filter(item => item);
            if(getAllSelected.length == $(".post_list").find(".gig_container").length){
              $("#select_all").click();
              $("#select_all").attr("checked",true);
            }else {
              $("#select_all").attr("checked",false);
            }

        });

    $( document ).ready(function(){
        var $sticky = $('.sticky');
        var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
        if (!!$sticky.offset()) { // make sure ".sticky" element exists

          var generalSidebarHeight = $sticky.innerHeight();
          var stickyTop = $sticky.offset().top;
          var stickOffset = 0;
          var stickyStopperPosition = $stickyrStopper.offset().top;
          var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
          var diff = stopPoint + stickOffset;

          $(window).scroll(function(){ // scroll event
            var windowTop = $(window).scrollTop(); // returns number

            if (stopPoint < windowTop) {
                $sticky.css({ position: 'absolute', top: diff });
            } else if (stickyTop < windowTop+stickOffset) {
                $sticky.css({ position: 'fixed', top: stickOffset });
            } else {
                $sticky.css({position: 'absolute', top: 'initial'});
            }
          });
        }
  });


function fetchGigs(url,data = {}){
  localStorage.setItem("last_url",url)
  $.ajax({
    url : url,
    method : "POST",
    data : data,
    beforeSend : function(){
      $("#next-btn").attr("disabled",true)
    },
    success:function(res){
      // $("#select_all").parent().hide()
      var showRes = "";
       let isCurrentPage = false;
       let currentPage = -1;
       let lastPage = 0;
       let getPageNum = 1;

       if(url.indexOf("?") != -1){
        let a = url.split("?");
        if(a.length > 1){
          let b = a[1].split("=");
          if(b.length > 1){
            getPageNum = parseInt(b[1])
          }
        }
       }

      if(res.post != "" && res.post != null){
        let post = res.post.data;
        let i = 0;
        currentPage = res.post.current_page;
        lastPage = res.post.last_page;

        if(post != null && post != undefined && post.length  > 0){
          if(res.post.next_page_url != null && res.post.next_page_url != "" && res.post.next_page_url != undefined){
            $("#next-btn").attr("href",res.post.next_page_url).show();
          }else {
            $("#next-btn").hide();
          }

          isCurrentPage = res.post.current_page == 1 ? true : false;


          post.forEach(function(data,i){
            let profile = "{{ asset('business/assets/images/profile-images/profile-2.png') }}";

             if(data.images != "" && data.images != null && data.images.length > 0){
              for(let a = 0;a < data.images.length;a++){
                if(data.images[a].image != null || data.images[a].image != "" || data.images[a].image != undefined){
                  profile = data.images[a].image;
                  break;
                }
              }
            }

            i++;
            let isRow = i % 4 == 0;
            let isAllSelected = false;
            if($("#select_all").is(":checked") === true){
              pushPopUsers(data.uid)
              isAllSelected = true;
              setPrice(data.uid,postPrice,"add","",data.name)
            }

            let isExist = getPrice("get",data.uid);

            let influencer_url = "{{url('business/influencer-details/')}}/"+btoa(data.uid);
        //     showRes += (`<div class="col-md-4 gig_container" uid='${data.uid}' >
        //                           <div class="white_bg_sec selected_bg_color ${isAllSelected || isExist ? 'active-selection' : 'lightBorderHighLight'} gig_box">
        //                           <a href="${influencer_url}">
        //                             <div class="Update-img text-center">
        //                               <img src="${profile}" alt="upload-one" class="img-responsive image_preview round">
        //                             </div>
        //                             <h5>${data.name ? data.name.substr(0,20) : "User"}</h5>
        //                             <div class="border-top">
        //                             </div>
        //                             <div class="social_flex">
        //                               <div class="social_img">
        //                                 <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
        //                                 <p>${data.facebook_friends > 0 ?  parseK(data.facebook_friends) : 0}<br> Followers</p>
        //                               </div>
        //                               <div class="social_img">
        //                                 <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
        //                                 <p>${data.instagram_friends > 0 ? parseK(data.instagram_friends) : 0}<br> Followers</p>
        //                               </div>
        //                               <div class="social_img border_none">
        //                                 <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
        //                                 <p>${data.twitter_friends > 0 ? parseK(data.twitter_friends) : 0}<br> Followers</p>
        //                               </div>
        //                             </div>
        //                             <div class="border-bottom">
        //                             </div>
        //                             <div class="date_content">
        //                               <span class="equal_width">Desired Post Rate: </span>KSh${data.gig_price > 0 ? data.gig_price : 0}
        //                             </div>
        //                             </a>
        //                             <div class="filter_btns d-flex" style="margin-top: 24px;">
        //                               <button type="button" class="btn btn-primary green_btn perfomselectionchange">${isAllSelected || isExist ? 'UNSELECT' : 'SELECT'}</button>
        //                               <button type="button" class="btn btn-primary green_btn adjust_popup" data-uid = ${data.uid}>ADJUST PRICE</button>
        //                             </div>
        //                           </div>
        //                         </div>`+ (i % 3 == 0 ?  "<div style='clear:both;width:100%'></div>" : ''))
          showRes += (`
                <tr uid='${data.uid}' class="gig_container" name='${data.name ? data.name.substr(0,20) : "User"}'>
                                    <td>
                                        <label class="checkbox-container">
                                            <input type="checkbox" ${isAllSelected || isExist ? 'checked' : ''} class="perfomselectionchange">
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="user-image">
                                            <img src="${profile}" alt="">
                                        </div>
                                    </td>
                                    <td class="user-name"><a href="{{url('business/influencer-details')}}/${btoa(data.id)}" target="blank"> ${data.name ? data.name.substr(0,20) : "User"} </a></td>
                                    <td class="post-rate">KSh ${data.gig_price > 0 ? data.gig_price : 0}</td>
                                    <td>
                                        <div class="social-media">
                                            <a href="#" class="followers">
                                                <img src="{{ asset('business/assets/images/icons/insta.svg')}}" alt="">
                                                <p>${data.instagram_friends > 0 ? parseK(data.instagram_friends) : 0}</p>
                                            </a>
                                            <a href="#" class="followers">
                                                <img src="{{ asset('business/assets/images/icons/fb.svg')}}" alt="">
                                                <p>${data.facebook_friends > 0 ?  parseK(data.facebook_friends) : 0}</p>
                                            </a>
                                            <a href="#" class="followers">
                                                <img src="{{ asset('business/assets/images/icons/twit.svg')}}" alt="">
                                                <p>${data.twitter_friends > 0 ? parseK(data.twitter_friends) : 0}</p>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="hire-me-btn green_btn adjust_popup" data-uid = ${data.uid} data-name = ${data.name ? data.name.substr(0,20) : "User"} data-toggle="modal" data-target="#adjust-price-modal">Adjust Price</button>
                                    </td>
                                </tr>
          `)
          })

        }
      }

      $("#next-btn").removeAttr("disabled",true)


      if(showRes != "" || showRes.length > 0){
        if(isCurrentPage){
          $('.post_list').html(showRes)
        }else {
          $('.post_list').append(showRes)
        }

        // $("#select_all").parent().show();
      }else {
        // console.log("currentPage ",currentPage," last page ",lastPage)
        if(currentPage < lastPage){
          $("#select_all").parent().hide();
          $("#next-btn").hide();
          $('.post_list').html('<h4 class="text-center">No Data Available</h4>')
        }

      }

      if((showRes == "" || showRes.length == 0) && getPageNum == 1){
          $("#select_all").parent().hide();
          $("#next-btn").hide();
          $('.post_list').html('<h4 class="text-center">No Data Available</h4>')
      }else {
              $("#select_all").parent().show();
          if(showRes != "" || showRes.length > 0){
            if(isCurrentPage){
              $('.post_list').html(showRes)
            }else {
              $('.post_list').append(showRes)
            }

            // $("#select_all").parent().show();
          }else {
            // console.log("currentPage ",currentPage," last page ",lastPage)
            if(currentPage < lastPage){
              $("#select_all").parent().hide();
              $("#next-btn").hide();
              $('.post_list').html('<h4 class="text-center">No Data Available</h4>')
            }

          }
       }

    }
  })
}

/* pagination*/
let getFilter = localStorage.getItem("user_filter");

if(getFilter != "" && getFilter != null && getFilter != undefined){
  getFilter = JSON.parse(getFilter)
  if(getFilter.search != null && getFilter.search != undefined){
    $("#search-input").val(getFilter.search)
  }
}else{
  getFilter = {}
}

fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);

$("#next-btn").click(function(){
  event.preventDefault();
  if($(this).attr("disabled")){
    return false;
  }
  fetchGigs($(this).attr("href"),getFilter)
})

$("#search-input").on("keyup",function(){
  let search = $(this).val();
  // $("#selected_users,#users_price").val("")
  getFilter['search'] = search;

  localStorage.setItem("user_filter",JSON.stringify(getFilter));

  fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);
})

/* show-suggested */

$(".show-suggested").click(function(){
  if(!$(this).attr("is-showing")){
      $(this).attr("is-showing",true)
      getFilter["hasLimit"] = 20;
      fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);
  }else {
    $(this).removeAttr("is-showing")
    delete getFilter['hasLimit'];
    localStorage.setItem("user_filter",JSON.stringify(getFilter));
    fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);
  }
})

/* set static price which gig creator already assign
 * set new price for user's if  if gig created update the price
*/
function setPrice(id,price,type,compliment = null, name = null){
  let prices = $("#users_price");

  let getAllPrices = prices.val();
  let setPrices = [];

  if(getAllPrices != "" && getAllPrices != null){
    setPrices = JSON.parse(getAllPrices);
  }

  if(type == "remove"){
    let delIndex = -1;
     setPrices.map((e,i) => {
      if(e.id == id){
        delIndex = i;
      }
      return e;
    });
     setPrices.splice(delIndex,1);
  }else if(type == "add"){
    let delIndex = -1;
    setPrices.map((e,i) => {
      if(e.id == id){
        delIndex = i;
      }
      return e;
    });

    if(delIndex == -1){
      let setData = {
        id : id,
        name : name,
        price : price
      };
      setPrices.push(setData);
    }
  }else if(type == "update"){
    setPrices.map((e) => {
      if(e.id == id){
        if(compliment){
           e['compliment'] = compliment;
        }

        e.price = price;
      }
      return e;
    })
  }
  prices.val(JSON.stringify(setPrices))

  localStorage.setItem("_curSelUserPrs",JSON.stringify(setPrices))
}


function getPrice(type,id = null){
  let prices = $("#users_price").val();
  let getPrices = [];

  if(prices != "" && prices != null){
    getPrices = JSON.parse(prices);
  }

   if(type == "get"){
    let index = -1;
     getPrices.map((e,i) => {
      if(e.id == id){
        index = i;
      }
      return e;
    });
     return getPrices[index];

 }else if(type == "sum"){
    let allData = {
      "sum" : 0,
      "total" : 0
    }

    getPrices.map((e,i) => {
      allData["sum"] = + allData["sum"] +  + e.price
      allData["total"] = allData["total"] + 1
    });
    return allData;
 }
}


/* check if gig creator select any user or not,
if gig creator not select any user need
 to show alert for select atleast a user
*/

$("#proceed-btn").click(function(){
  if($("#selected_users").val() == ""){
    alert("Please select influencer before proceeding.");
  }else {
    localStorage.setItem("selected_users",$("#selected_users").val())
    localStorage.setItem("users_price",$("#users_price").val())
    $("#select_all").removeAttr("checked").prop("checked",false)
    window.location.href = $(this).attr("ref");
  }
})

/* change border of post user container when ever select/deselect user*/
$('body').on('DOMSubtreeModified', '.perfomselectionchange', function(){
  if($(this).text() == "UNSELECT"){
    highLightContainer($(this).closest(".gig_container"),"add")
  }else {
    highLightContainer($(this).closest(".gig_container"),"remove")
  }
});


$(document).ready(function() {
    $('.image_preview').click(function(){
      let img = $(this).attr('src')
      $(".target_img").attr('src',img)
       $('.bs-example-modal-lg').modal({
         backdrop: 'static'
       });
     });

  })

  $("#invite").on("click",function(){
    if($("#users_price").val() == "" || $("#users_price").val() == "[]"){
        alert("Please select atleast one influencer to confirm post");
        return false;
    } else {
        let user_storage = localStorage.getItem('_curSelUserPrs');
        let parsed_users = JSON.parse(user_storage);
        $("#influencers").val(user_storage);
        let lists = '';
        let count = 0;
        let total = 0;
        parsed_users.forEach(element => {
          count++;
            lists += `<li class="list-item">
                            <h5><b>${element.name}</b></h5>
                            <p>KSH</p>
                            <p>${element.price}</p>
                        </li>`
            total += parseInt(element.price)
        });
        lists += `<div class="font-weight-bold">Total Amount :  KSH ${total}</div>`;
        lists += `<div class="font-weight-bold">Selected Influencers :  ${count}</div>`;
        $('.final-list').html(lists);

    }
  });

  $("#confirm_booking").on("click",function(){
      if($("#users_price").val() == "" || $("#users_price").val() == "[]"){
        alert("Please select atleast one influencer to confirm post");
        return false;
      }
      $("#payment-modal").modal("hide");
      $("#register-number").modal("show");
      $.ajax({
                    url: '{{url("/business/set-mpesa-number")}}',
                    type: 'GET',
                    dataType: 'json',
                    success: function (result) {   // success callback function
                        $('#phone_number').val(result.phone_number);
                    }
                })
      // let data  = {
      //         "post_prices" : $("#users_price").val(),
      //         "_token" : "{{csrf_token()}}",
      //         "is_save" : true
      //     }

      // $("#paymentPop").modal("show");

      // let getCreatedId = localStorage.getItem("create_id");

      // if(!getCreatedId){

      //     localStorage.removeItem("users_price")
      //     localStorage.removeItem("user_filter")
      //     localStorage.removeItem("influencer_reach")
      //     localStorage.removeItem("selected_users")
      //     localStorage.removeItem("next_page")
      //     localStorage.removeItem("_curSelUserPrs")
      //     localStorage.removeItem("_curSelUser")

      //     $("input[name='post_prices']").val()
      //     $("#res-message,#mPesa-container,#success-pop").hide();

      //     $.ajax({
      //       url : "{{url('business/post-summary-page').'/'.base64_decode(Request::segment(3))}}",
      //       type : "POST",
      //       data : data,
      //       beforeSend : function(){
      //         $(document).find("#save-mphone").attr("disabled",true)
      //       },
      //       complete : function(){
      //         $(document).find("#save-mphone").removeAttr("disabled")
      //       },
      //       success : function(res,msg,xhr){
      //         if(xhr.status == 201){
      //           /*show pop up here*/
      //           setTimeout(function(){
      //             $("#success-pop").show()
      //           },1000)

      //           $(document).find("#save-mphone").text("Process Payment")

      //            $(document).find("#success-pop .alert")
      //           .addClass("alert-success")
      //           .removeClass("alert-danger")
      //           .text("Please proceed for the payment to create a Post.")
      //           $("#created_id").val(res.created);
      //           localStorage.setItem("create_id",res.created + "_"+res.data.phone_number)

      //         }else if(xhr.status == 202){
      //           $("#res-message").show().text("Please set your mPesa phone number to make payment");
      //           $("#created_id").val(res.created);
      //           $("#mPesa-container").show()
      //           $("#success-pop .alert").text("Please proceed for the payment to create a Post.")
      //           setTimeout(function(){
      //             $("#success-pop").show()
      //           },1000)
      //           localStorage.setItem("create_id",res.created + "_")
      //         }else {
      //           alert("Unable to proceed your request, please try later.");
      //           window.location.href = "{{url('business/posts')}}"
      //         }
      //       },error : function(err){

      //       }
      //     })
      //   } else {
      //     let createdId = getCreatedId.split("_")[0];
      //     let phone = getCreatedId.split("_")[1];

      //     setTimeout(function(){
      //           $("#success-pop").show()
      //     },1000)
      //     if(phone == "" && phone.length == 0){
      //       $("#res-message").show().text("Please set your mPesa phone number to make payment");
      //       $("#created_id").val(createdId);
      //       $("#mPesa-container").show()
      //       $("#success-pop .alert").text("Please proceed for the payment to create a Post.")
      //     }else {
      //       $("#created_id").val(createdId);
      //       $(document).find("#save-mphone").text("Process Payment")
      //     }
      //   }
      // return false;

      // $("input[name='post_prices']").val($("#users_price").val())
      // localStorage.removeItem("users_price")
      // localStorage.removeItem("user_filter")
      // localStorage.removeItem("influencer_reach")
      // localStorage.removeItem("selected_users")
      // localStorage.removeItem("next_page")
      // localStorage.removeItem("_curSelUserPrs")
      // localStorage.removeItem("_curSelUser")
      // return true;
    });
    $(document).on('click', '.make-payment-complete', function(){
                let _this = $(this);
                let phone = $('#phone_number').val();
                let price = $("#users_price").val();
                let post_id = $("#post_id").val();
                let infulancer_id = $("#influencers").val();
                let type = 'post';
                if(phone == ''){
                    $('#phone_number').addClass('border-danger');
                    $('.error-phone').removeClass('d-none');
                    return false;
                }else{
                    $('#phone_number').removeClass('border-danger');
                    $('.error-phone').addClass('d-none');
                }
                _this.attr('disabled', false);
                _this.find('i').removeClass('d-none');
                $.ajax({
                    url: '{{url("/business/make-mpesa-payment")}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        phone,
                        type,
                        price,
                        post_id,
                        infulancer_id,
                    },
                    success: function (result) {   // success callback function
                        swal({
                            title: "Post Created",
                            text: result.message,
                            icon: "success",
                            button: "Ok",
                        })
                        .then((value) => {
                            window.location.href = `{{'/business/posts'}}`
                        });
                        _this.find('i').addClass('d-none');
                    }
                })
            })

    localStorage.removeItem("uid")
  function checkPaymenStatus(uid){
    if(uid){
      $.ajax({
          url : "{{url('business/get-business-payment-status')}}/" + uid,
          type : "GET",
          timeout: 5000,
          success : function(res,msg,xhr){
            if(xhr.status == 200){
              window.location.href = "{{url('business/posts')}}"
            } else if(xhr.status == 203) {
             alert("Payment Failed");
             window.location.href = "{{url('business/posts')}}"
           }
          },error:function(error){
            console.log("error in ajax ",error)
          }
        })
      }
  }

/*  $(document).on("keyup paste keydown","#mphone",function(e){
        if($(this).val().length == 15){
          return false;
        }
      });*/

  $("#save-mphone").click(function(){

    let type = $(this).text();
    let _this = $(this)

    if(type == "Process Payment"){

      let getPayload = async function(){
            return {
                "_token" : "{{ csrf_token() }}",
                post_id : await $("#created_id").val(),
                type : "post"
            }
       }
      getPayload()
      .then((data)=>{
      $("#mPesa-container").show().html("<div class='loader'></div>")

      $.ajax({
            url : "{{url('business/make-mpesa-payment')}}",
            type : "POST",
            data :data ,
            beforeSend : function(){
              $(document).find("#save-mphone").attr("disabled",true)

            },
            complete : function(){
              $(document).find("#save-mphone").removeAttr("disabled")
            },
            success : function(res,msg,xhr){
              console.log("res",res)
              localStorage.setItem("payment_completed",1)
              if(xhr.status == 200){
                $("#paymentPop h2.modal-title").text("Payment Under Process");
                // $("#mPesa-container").hide()
                $("#success-pop .alert").removeClass("alert-danger")
                  .addClass("alert-success")
                  .html("A confirmation message has been sent to your mPesa phone number, Please confirm to continue.<br/><span class='text-center'>Please do not refresh or press back<span>");

                  $("#close-pop").hide()
                  localStorage.removeItem("created_id");

                $(document).find("#save-mphone").text("Go to List Page").hide();
                let uid = res.uid;

                setInterval(function(){
                  checkPaymenStatus(uid);
                },5000);

              }else if(xhr.status == 204) {
                $("#mPesa-container").hide()
                $("#mPesa-container").hide()
                $("#success-pop .alert").removeClass("alert-success").addClass("alert-danger").text("Unexpected error while payment process");
              } else if(xhr.status == 205) {
                $("#mPesa-container").hide()
                alert("Please Enter your Mobile Number");
              }else {
                $("#mPesa-container").hide()
                alert("Unable to proceed your request, please try later");
              }
            },
            error : function(res){
                console.log(res)
               $("#mPesa-container").hide()
                alert("Unable to proceed your request, please try later");
            }
        })
      })

    } else if(type == "Go to List Page"){
      window.location.href = "{{url('business/posts')}}";
    }else {
      let phone = $("#mphone").val();
      if(phone.length == 0){
        alert("Please enter phone number.");
        return false;
      }else if(isNaN(phone)){
        alert("Please enter number only.");
        return false;
      }else if(phone.length < 8){
        alert("Phone should be atleast 8 digits long.");
        return false;
      }else {
        let data = {
          phone : phone,
          "_token" : "{{ csrf_token() }}"
        }

        $.ajax({
            url : "{{url('business/set-mpesa-number')}}",
            type : "POST",
            data : data,
            beforeSend : function(){
              $(document).find("#save-mphone").attr("disabled",true)

            },
            complete : function(){
              $(document).find("#save-mphone").removeAttr("disabled")
              $("#mPesa-container").hide()
              $(document).find("#res-message").hide()
            },
            success : function(res,msg,xhr){
              if(xhr.status == 200){
                let createdData = localStorage.getItem("create_id");
                if(createdData){
                  let id = createdData.split("_")[0];
                  localStorage.setItem("create_id",id + "_" + $("#mphone").val())
                  $("#created_id").val(id);

                  $("#success-pop .alert").text("Phone number set successfully.")
                  setTimeout(function(){
                    $("#success-pop").show()
                  },1000)

                  $(document).find("#save-mphone").text("Process Payment")

                }
              }else {
                alert("Unable to proceed your request, please try later");
              }
            }
        })
      }
    }
  })

  $(document).on("click","#close-pop",function(){
    if($(this).attr("is-redirect")){

    }else {
      $("#paymentPop").modal("hide")
    }
  })

        function onlyNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        }
    return true;
}


</script>
@endsection
