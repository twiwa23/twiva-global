@extends('business.layout.app')
@section('title')
    Portfolio
@endsection

@section('content')
<div class="influencers-profile creatives-profile">
    <div class="dashboard_container">
        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- Page Content -->
        <div class="right_col dashboard-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{ url('business/creaters') }}" class="active">Creatives<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#">@if(!empty($user->name)) {{$user->name . '\'s'}} @else N/A @endif Profile</a></li>
                </ul>
            </div>
            <!--********** Breadcrumb End ***********-->

            <div class="profile-container">
                <?php
                    $bg_image = $user->background_image ?: asset('business/assets/images/profile-images/cover-image.png');
                ?>
                <div class="cover-image">
                    <img src="{{ $bg_image }}" alt="">
                </div>

                <div class="profile-info">
                    <div class="profile-image">
                        @if(!empty($user->profile))
                        <img src="{{$user->profile}}" alt="" />
                        @else
                        <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}" alt="">
                        @endif
                    </div>

                    <div class="profile-desc">
                        <div class="profile-content">
                            <div class="profile-name">
                                <h1>@if(!empty($user->name)) {{$user->name}} @else N/A @endif</h1>
                                <span>
                                    <img src="{{ asset('business/assets/images/icons/bullet.svg') }}" alt="">@if(!empty($category->name)) {{$category->name}} @else N/A @endif
                                </span>
                                <span>
                                    {{-- <img src="{{ asset('business/assets/images/icons/bullet.svg') }}" alt=""> --}}
                                    {{-- <img src="{{ asset('business/assets/images/icons/location.svg') }}" alt="">New Zealand --}}
                                </span>
                            </div>

                            <div class="social-media">
                                <a href="#" class="followers">
                                    <img src="{{ asset('business/assets/images/icons/insta-black.svg') }}" alt="">
                                </a>
                                <a href="#" class="followers">
                                    <img src="{{ asset('business/assets/images/icons/circle-black.svg') }}" alt="">
                                </a>
                                <a href="#" class="followers">
                                    <img src="{{ asset('business/assets/images/icons/twitter-black.svg') }}" alt="">
                                </a>
                                <a href="#" class="followers">
                                    <img src="{{ asset('business/assets/images/icons/youtube-black.svg') }}" alt="">
                                </a>
                            </div>
                        </div>


                        <div class="about">
                            <p>@if(!empty($user->description)) {{$user->description}} @else N/A @endif</p>
                        </div>

                        <div class="hire-me">
                            <a href="{{ url('/business/hire-me/'.$user->id) }}" class="hire-me-btn">Contact me</a>
                        </div>

                        <div class="services">
                            <h1>Services</h1>
                            <div class="service-list">
                                @forelse($service as $services)
                                <div class="list-item">
                                    @if(!empty($services->profile))
                                        <img src="{{$services->profile}}" alt="" />
                                        @else
                                        <img src="{{ asset('business/assets/images/icons/lipstick2.svg') }}" alt="">
                                        @endif
                                        {{-- <h3>@if(!empty($services->title)) {{$services->title}} @else N/A @endif</h3> --}}
                                    <p>@if(!empty($services->text)) {{$services->text}} @else N/A @endif</p>
                                </div>
                                @empty
                                <div class="list-item">
                                    <p>No Services .</p>
                                </div>
                                @endforelse
                            </div>
                        </div>

                    </div>
                </div>

            </div>


            <div class="creators-page">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="creators-details-wrapper">
                            <div class="my-profile">
                                <h1 class="title">My Profile</h1>

                                <div class="row">
                                    <?php $img = 0;?>
                                    @foreach($portfolio as $portfolios)
                                    @if(!empty($portfolios->profile))
                                    <?php $img++;
                                        if($img <= 6){

                                        ?>
                                    <div class="col-sm-4 col-lg-4">
                                        <?php
                                            $video_ext = ["mp4","avi","3gp","flv","mov","video"];
                                            $get_file = $portfolios->profile;
                                            $thumbnail_name = $portfolios->thumbnail_name;

                                            $has_video = false;
                                            $has_file = false;
                                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                                            $get_file_type = strtolower($get_file_type);

                                            if(in_array($get_file_type,$video_ext)){
                                                echo '<div class="image-container"><video src="'.$get_file.'"  controls="" " poster="'.$thumbnail_name.'"></video></div>';
                                            }else {
                                                echo '<div class="image-container"><img src="'.$get_file.'" "/></div>';
                                            }

                                            if($img == 6){
                                            ?>
                                        <div class="image-container">
                                            View All
                                            {{-- <img src="{{ asset('business/assets/images/creators-page/rectangle-671.png') }}" alt=""> --}}
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="rate">
                                <h1 class="title">Rate</h1>

                                <div class="row">
                                    @foreach($rate as $rates)
                                    <div class="col-sm-4 col-lg-4">
                                        <div class="product-rate">
                                            {{-- <div class="product-img">
                                                <img src="{{ asset('business/assets/images/creators-page/rectangle-678.png') }}" alt="">
                                            </div> --}}

                                            <h2 class="product-name">@if(!empty($rates->title)){{$rates->title}} @else N/A @endif</h2>

                                            <p class="product-price">@if(!empty($rates->rate)){{$rates->rate}} @else N/A @endif KSh</p>
                                            <?php
                                                $text = $rates->text;
                                                    $texts = array();
                                                if($text && !empty($text)){
                                                    $texts = explode("\r\n",$text);
                                                }
                                            ?>
                                            <ul class="product-details">
                                                <?php for($i = 0; $i < count($texts); $i++){
                                                    if(isset($texts[$i]) && !empty($texts[$i])){

                                                    ?>
                                                 <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>{{ $texts[$i] }}</p>
                                                </li>
                                                 <?php } } if(count($texts) == 0){ echo "N/A"; } ?>
                                                {{-- <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li> --}}
                                            </ul>

                                            <div class="profile-links">
                                                <a href="{{url('/business/hire-me/'.$user->id)}}" class="red-btn">Contact me</a>
                                            </div>

                                        </div>
                                    </div>
                                    @endforeach

                                    {{-- <div class="col-sm-4 col-lg-4">
                                        <div class="product-rate">
                                            <div class="product-img">
                                                <img src="{{ asset('business/assets/images/creators-page/rectangle-680.png') }}" alt="">
                                            </div>

                                            <h2 class="product-name">Company Profiles,Mid Level Video Editing</h2>

                                            <p class="product-price">500 Ksh</p>

                                            <ul class="product-details">
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                            </ul>

                                            <div class="profile-links">
                                                <a href="" class="red-btn">Contact me</a>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-4 col-lg-4">
                                        <div class="product-rate">
                                            <div class="product-img">
                                                <img src="{{ asset('business/assets/images/creators-page/rectangle-680.png') }}" alt="">
                                            </div>

                                            <h2 class="product-name">Company Profiles,Mid Level Video Editing</h2>

                                            <p class="product-price">500 Ksh</p>

                                            <ul class="product-details">
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                                <li>
                                                    <img src="{{ asset('business/assets/images/icons/polygon.svg') }}" alt="">
                                                    <p>Lorem ipsum, dolor sit amet</p>
                                                </li>
                                            </ul>

                                            <div class="profile-links">
                                                <a href="" class="red-btn">Contact me</a>
                                            </div>

                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </main>
            </div>

    </div>
</div>
@endsection
