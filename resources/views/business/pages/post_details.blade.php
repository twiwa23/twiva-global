@extends('business.layout.app')
@section('title')
    Post Details
@endsection
@section('content')
@php
    $url = '';
    $rated = 0;
@endphp
<div class="" id="business-dashboard">
    <div class="dashboard_container">

        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->

        <!-- page content -->
        <div class="right_col dashboard-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#">{{$post_details->post_name ?? 'N/A'}}</a></li>
                </ul>
            </div>
            <!--**********  Breadcrumb End ***********-->

            <div class="feedpost-bidding">
                <div class="feedpost-heading">
                    <h1>{{$post_details->post_name ?? 'N/A'}}</h1>
                    <div class="btn-section">
                        @foreach ($details as $post)
                            @if($post->status == 5 && $post->is_submit == 1)
                                @php $post_status = 'Completed';
                                        $post_status_btn = 'completed';
                                @endphp
                                @if($post->is_rated == '0')
                                <?php
                                    $url = url('business/post-rating').'/'.base64_encode($post->user_id).'/'.base64_encode($post->post_id);
                                ?>
                                @endif
                                @if($post->is_rated == '1')
                                <?php
                                    $rated = 1;
                                ?>
                                @endif
                            @else
                                @php $post_status = 'OnGoing';
                                    $post_status_btn = 'ongoing';
                                @endphp
                            @endif

                        @endforeach
                        <p class="{{ $post_status_btn }}">{{ $post_status }}</p>
                        @if($url != '')
                            <a href="{{$url}}">
                                <button class="red-btn">Rate</button>
                            </a>
                        @endif
                        @if($rated == 1)
                            <button class="red-btn">Rated</button>
                        @endif
                        <!-- <a href="" class="ongoing">Ongoing</a> -->
                        <a href="{{ url('business/edit-post/'.base64_encode($post_details->id)) }}">
                            <img src="{{ asset('business/assets/images/icons/info2.svg') }}" alt="">
                        </a>
                    </div>
                </div>

                <h4>{{ $post_details->description ?? 'N/A' }}</h4>
                <p><span>Expiration Date</span> {{ $post_details->expiration_date ?? 'N/A' }}</p>
            </div>

            <div class="you-hired">
                <h2>You hired</h2>
                <div class="img-list">
                    <div class="img-section">
                        @foreach ($details as $post)
                            @if($post->status == 1 || $post->status == 5 || $post->status ==3)

                            <div class="img-wrap">
                                <a href=""><img src="{{ $post->user->Images[0]->image ?? asset('public/images/dummy_user.jpg') }}" alt=""></a>
                            </div>
                            @endif

                        @endforeach


                        {{-- <a href="" class="more">+6 more</a> --}}
                    </div>

                    <!-- <a href="" class="red-btn">Check them out <img src="./assets/images/icons/chevron-down.svg" alt=""></a> -->
                    <a href="{{url('business/accept-post',base64_encode($id))}}"><button class="red-btn">Check them out <img src="{{ asset('business/assets/images/icons/chevron-down.svg') }}" alt=""></button></a>
                </div>
            </div>
            @if($post_status != 'Completed')
            <div class="influencer-list">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#invitations">Invitations</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#bids">Bids</a>
                    </li> --}}
                </ul>
                <!-- <a href="#" class="red-btn">Invite More Influencers</a> -->
                <a href="{{url('business/invite-influencers-post/'.base64_encode($id))}}" class="red-btn">Invite More Influencers</a>
            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="invitations">
                    <div class="catalog-page">
                        <div class="select-top select-search">
                            <div class="search-wrap">
                                <div class="search-panel">
                                    <input type="text" placeholder="Search here..." id="search">
                                </div>

                                <div class="button-catalog">
                                    <!-- <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">All</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All">All</span>
                                                <span class="btn-option" data-value="5-Star">5-Star</span>
                                                <span class="btn-option" data-value="4-Star">4-Star</span>
                                                <span class="btn-option" data-value="3-Star">3-Star</span>
                                                <span class="btn-option" data-value="2-Star">2-Star</span>
                                                <span class="btn-option" data-value="1-Star">1-Star</span>
                                            </div>
                                            <img src="/assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div>

                                    <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">Price Range</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All">All</span>
                                                <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                                <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                                <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                            </div>
                                            <img src="/assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div>

                                    <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">All Status</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All Status">All Status</span>
                                                <span class="btn-option" data-value="Invite Sent">Invite Sent</span>
                                                <span class="btn-option" data-value="Pending">Pending</span>
                                                <span class="btn-option" data-value="Accepted">Accepted</span>
                                                <span class="btn-option" data-value="Declined">Declined</span>
                                            </div>
                                            <img src="/assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div> -->

                                </div>

                            </div>
                        </div>

                        <div id="table_data">
                            @include('/business/pages/postdetails-data')
                        </div>

                    </div>
                </div>

                {{-- <div class="tab-pane fade" id="bids">
                    <div class="catalog-page">
                        <div class="select-top select-search">
                            <div class="search-wrap">
                                <div class="search-panel">
                                    <input type="text" placeholder="Search here...">
                                </div>

                                <div class="button-catalog">
                                    <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">All</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All">All</span>
                                                <span class="btn-option" data-value="5-Star">5-Star</span>
                                                <span class="btn-option" data-value="4-Star">4-Star</span>
                                                <span class="btn-option" data-value="3-Star">3-Star</span>
                                                <span class="btn-option" data-value="2-Star">2-Star</span>
                                                <span class="btn-option" data-value="1-Star">1-Star</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div>

                                    <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">Price Range</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All">All</span>
                                                <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                                <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                                <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div>

                                    <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">All Status</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All Status">All Status</span>
                                                <span class="btn-option" data-value="Invite Sent">Invite Sent</span>
                                                <span class="btn-option" data-value="Pending">Pending</span>
                                                <span class="btn-option" data-value="Low Pay">Low Pay</span>
                                                <span class="btn-option" data-value="Declined">Declined</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>IMAGE</th>
                                        <th>NAME</th>
                                        <th>STAR RATING</th>
                                        <th>RATE</th>
                                        <th>FOLLOWERS</th>
                                        <th>STATUS</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="declined">
                                            <p>Declined</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Send Reminder</button>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="completed">
                                            <p>Invite Sent</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Accept</button>
                                            <button class="hire-me-btn">Adjust Price</button>
                                            <!-- <a href="#" class="hire-me-btn">Accept</a>
                                            <a href="#" class="hire-me-btn" style="margin-left: 48px;">Adjust Price</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="completed">
                                            <p>Invite Sent</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Send Reminder</button>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="declined">
                                            <p>Declined</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Accept</button>
                                            <button class="hire-me-btn">Adjust Price</button>
                                            <!-- <a href="#" class="hire-me-btn">Accept</a>
                                            <a href="#" class="hire-me-btn" style="margin-left: 48px;">Adjust Price</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="ongoing">
                                            <p>Low Pay</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Send Reminder</button>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="ongoing">
                                            <p>Pending</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Accept</button>
                                            <button class="hire-me-btn">Adjust Price</button>
                                            <!-- <a href="#" class="hire-me-btn">Accept</a>
                                            <a href="#" class="hire-me-btn" style="margin-left: 48px;">Adjust Price</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="declined">
                                            <p>Declined</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Send Reminder</button>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="ongoing">
                                            <p>Pending</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Send Reminder</button>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="user-image">
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                            </div>
                                        </td>
                                        <td class="user-name">Ronald Richards</td>
                                        <td>
                                            <div class="user-ratings">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                                <img src="./assets/images/icons/star1.svg" alt="">
                                            </div>
                                        </td>
                                        <td class="post-rate">Ksh2500</td>
                                        <td>
                                            <div class="social-media">
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/insta.svg" alt="">
                                                    <p>2K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/fb.svg" alt="">
                                                    <p>1K</p>
                                                </a>
                                                <a href="#" class="followers">
                                                    <img src="./assets/images/icons/twit.svg" alt="">
                                                    <p>50K</p>
                                                </a>
                                            </div>
                                        </td>

                                        <td class="declined">
                                            <p>Declined</p>
                                        </td>

                                        <td>
                                            <button class="hire-me-btn">Send Reminder</button>
                                            <!-- <a href="#" class="hire-me-btn">Send Reminder</a> -->
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                        </div>

                        <div class="pagination">
                            <div class="col-sm-5">
                                <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                                <div class="">
                                    <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                        <ul class="pagination">
                                            <li class="paginate_button previous disabled" id="example_previous">
                                                <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                                    <img src="./assets/images/icons/button.svg" alt="">
                                                </a>
                                            </li>

                                            <li class="paginate_button active">
                                                <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                            </li>

                                            <li class="paginate_button next" id="example_next">
                                                <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                                    <img src="./assets/images/icons/Right.svg" alt="">
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> --}}

            </div>
            @endif



        </div>
        <!-- /page content -->
    </div>
</div>
<input type="hidden" id="postId" value="{{$id}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){

$(document).on('click', '.pagination a', function(event){
 event.preventDefault(); 
 var page = $(this).attr('href').split('page=')[1];
 fetch_data(page);
});

function fetch_data(page)
{
var query=$("#search").val();
var postId = $("#postId").val();
 $.ajax({
  url:"/business/get-post-details/"+postId+"?page="+page,
  data:{'query': query},
  success:function(details)
  {
   $('#table_data').html(details);
  }
 });
}
});
$('#search').on('keyup',function(){
var query=$(this).val();
var postId = $("#postId").val();
$.ajax({
type : 'get',
url:"/business/get-post-details/"+postId,
data:{'query': query},
success:function(details){
    $('#table_data').html(details);
}
});
});
</script>
@endsection
