@extends('business.layout.app')
@section('title')
    Edit Post
@endsection
@section('styles')
    <style>
.media_preview .img_video {
    width: 120px;
    height: 110px;
    max-height: 110px;
    float: left;
    max-width: 120px;
    position: relative;
    word-break: break-all;
    border: 1px solid lightgray;
    margin: 0 15px 15px 0;
    padding: 2px;
}
.media_preview .preview_image {
  width: 112px;
  height: 104px;
  max-height: 104px;
  max-width: 112px;
  margin:0 !important;
}
.remove-img {
   background-color: rgb(115,14,57);
    width: 20px;
    height: 20px;
    padding: 4px;
    border-radius: 50%;
    box-shadow: 0px 0px 54px 0px rgba(82, 82, 82, 0.35);
    position: absolute;
    right: -7px;
    top: -10px;
    background-image: url('{{url("public/business/images/white_cross_icon.png")}}');
    background-size: cover;
    cursor: pointer;
    z-index: 1;
}
.clear-fix {
  clear: both;
  width: 100%
}
input.upload_image {
    display: none;
}
.error {
    color: red;
}
</style>
@endsection

@section('content')
<div class="influencers-profile">
        <div class="dashboard_container">
            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->

            <!-- Page Content -->
            <div class="right_col " role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                        <li><a href="#">View/Edit Post</a></li>
                    </ul>
                </div>
                <!--********** Breadcrumb End ***********-->

                <div class="post-container">
                    <div class="post-heading">
                        <h3>Post Information</h3>
                        <a href="{{ url('business/create-post-desc') }}" class="post-info">
                            <img src="{{ asset('business/assets/images/icons/info2.svg') }}" alt="">
                            <span>What are Post ?</span>
                        </a>
                    </div>
                    <form method="POST" action="{{ url('business/edit-post/'. $postDetail->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="post-input-container">

                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" readonly name="post_name" maxlength="30" class="form-control"  value="{{ $postDetail->post_name ?? '' }}" required>
                                        <span id="post_name-error" class="error" for="post_name">{{$errors->first('post_name')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea type="text"  readonly name="description" rows="2" maxlength="1000" placeholder="" class="form-control custom-h"  required>{{ $postDetail->description ?? '' }}</textarea>
                                        <span id="description-error" class="error" for="description">{{$errors->first('description')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Compensation <span>(How much you will pay each influencer you hire)</span></label>
                                        <input type="text" readonly  id="price_per_post" value="{{ $postDetail->price_per_post ?? '' }}" maxlength="6" name="price_per_post" class="form-control" placeholder="" required>
                                        <span id="price_per_post-error" class="error" for="price_per_post">{{$errors->first('price_per_post')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Expiration Date</label>
                                        <input type="date"  readonly  name="expiration_date" class="form-control" value="{{ $postDetail->expiration_date }}" required/>
                                        <span id="expiration_date-error" class="error" for="expiration_date">{{$errors->first('expiration_date')}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>What To Do <span>(These are the things Influencers have to do in their content)</span></label>
                                        <textarea type="text"  readonly name="what_to_include" maxlength="1000"  rows="2" placeholder="" class="form-control custom-h" required>{{ $postDetail->what_to_include ?? '' }}</textarea>
                                        <span id="what_to_include-error" class="error" for="what_to_include">{{$errors->first('what_to_include')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>What Not To Do <span>(These are the things Influencers should NOT do in their content)</span></label>
                                        <textarea type="text"  readonly name="what_not_to_include" maxlength="1000"  rows="2" placeholder="" required class="form-control custom-h">{{ $postDetail->what_not_to_include ?? ''}}</textarea>
                                        <span id="what_not_to_include-error" class="error" for="what_not_to_include">{{$errors->first('what_not_to_include')}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Other Requirements</label>
                                        <textarea type="text"  readonly name="other_requirement" rows="2" class="form-control custom-h">{{ $postDetail->other_requirement ?? '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="post-heading">
                        <h3>Theme Details</h3>
                    </div>

                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Hashtag <span>(Include hashtags that will be attached to the created content e.g. #twiva #yourproductlaunch)</span></label>
                                        <input type="text" readonly  name="hashtag" id="hashtag" maxlength="30" class="form-control" placeholder="" value="{{ $postDetail->hashtag  ?? ''}}" required>
                                        <span id="hashtag-error" class="error" for="hashtag">{{$errors->first('hashtag')}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                    <label>Add a Theme <span>(Include any photo/video to be used as an inspiration for the Influencer)</span></label>
                                        <div class="form-control custom-form" style="height:auto; background: #F8F8F8;;">
                                       
                                            @foreach($postDetail->Images as $image)
                                            <div class="image-container">
                                                @if(!empty($image->media))
                                                <img src="{{$image->media}}" class="images_placehold" alt="">
                                                @else
                                                <img src="{{url('public/business/images/default.svg')}}" alt="">
                                                @endif
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-sm-6 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Add a Theme <span>(Include any photo/video to be used as an inspiration for the Influencer)</span></label>
                                        {{-- <div class="form-control custom-form" style="height: 99px;">
                                            <label class="file-upload">
                                                <img src="{{ asset('business/assets/images/icons/input-plus.svg') }}" alt="">
                                                <input type="file">
                                            </label>

                                            <label class="file-upload">

                                            </label>
                                        </div> --}}

                                        {{-- @php($placeHoldUrl =  url('public/business/images/add.png'))
                                        <div class="images_container">
                                            <div class="media_preview"></div>
                                         </div>
                                        <div class="form-control custom-form" style="height: 99px;">
                                            <label  id="filediv" class="file-upload">
                                                <input type="hidden" name="non_acceptable_files" class="non_acceptable_files">
                                                <input type="hidden" class="ext_media_record" images="0" video="0"  total-media = "0" />
                                                <img src="{{ $placeHoldUrl }}" class="images_placehold" title="Select image/video" data-recursion="-1" id="file"/>
                                            </label>
                                            <label class="custom_error" id="video_image_error"></label>

                                            {{-- <input type="button" id="add_more" class="upload" value="Add" style="border: 0;"> --}}
                                        {{-- </div>  --}}
                                        <div class="img-div add-menu img" style="clear: both">
                                            @php($placeHoldUrl =  url('public/business/images/add.png'))
                                            @if(!empty($post_details->images))
                                            <?php $videos = $All_files = "";
                                               $no_doc = 0;
                                               $total = 0;
                                               $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                                            @forelse($post_details->images as $images)
                                            <li>
                                               <?php
                                                  $total++;
                                                  $get_file = $images->media;
                                                  $has_video = false;
                                                  $has_file = false;
                                                  $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                                                  $get_file_type = strtolower($get_file_type);
                                                  if(in_array($get_file_type,$video_ext)){
                                                  echo '<video src="'.$get_file.'" alt="upload-one" controls="" class="img-responsive video-containt video" target="_blank" title="Click to expand image/video"></video>';
                                                }else {
                                                  echo '<img src="'.$get_file.'" " alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>';
                                                }
                                                ?>
                                            </li>
                                            @empty
                                            N/A
                                            @endforelse
                                            @else
                                            N/A
                                            @endif
                                              {{-- <div class="images_container">
                                                 <div class="media_preview"></div>
                                              </div>
                                              @foreach ($postDetail->Images as $image)
                                                <img src="{{ $image->media }}">
                                              @endforeach --}}

                                                 {{-- <div class="media_inputs">
                                                      <div class="img_video">
                                                         <input type="hidden" name="non_acceptable_files" class="non_acceptable_files">
                                                         <input type="hidden" class="ext_media_record" images="0" video="0"  total-media = "0" />
                                                         <img src="{{ $placeHoldUrl }}" class="images_placehold" title="Select image/video" data-recursion="-1" />
                                                     </div>
                                                  </div>
                                                 <div class="clear-fix"></div>
                                                 <span class="custom_error" id="video_image_error"></span> --}}
                                         </div>

                                    </div>
                                </div>
                            </div> -->
                        </div>

                    </div>

                    {{-- <div class="btn-wrapper">
                        <button class="red-btn">Submit</button>
                    </div> --}}
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('scripts')

<script type="text/javascript" src="{{ asset('business/extra/multiFileUpload.js') }}"></script>
<script type="text/javascript">






   /* show hide # in hashtag on focus in and focus out */
   $("#hashtag").on("click paste keypress",function(){
    let val = $(this).val();
    if(val[0] != "#"){
       $(this).val("#"+val)
    }
   })

   $("#hashtag").on("keydown",function(e){
      if(e.keyCode === 8){
      let val = $(this).val();
        if(val && val.trim().length == 1){
          return val == "#" ? false : true;
        }
      }
      return true;
   })
   .on("select",function(){
      $("#hashtag").on("keyup",function(e){
        let val = $(this).val();
        if(val[0] != "#"){
           $(this).val("#"+val)
        }
      })
   })
   .bind("cut",function(){
    setTimeout(function(){
      let val = $("#hashtag").val();
        if(val[0] != "#"){
           $("#hashtag").val("#"+val)
        }
    },100)
   })


   $("#hashtag").focusout(function(){
      let val = $(this).val();
      if(val[0] == "#"){
        $(this).val(val.substring(1))
      }
   })

   $(document).ready(function(){
       $("#hashtag").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
      })
    })

</script>
@endsection
