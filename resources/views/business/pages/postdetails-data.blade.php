<div class="table-responsive">
                            <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>IMAGE</th>
                                        <th>NAME</th>
                                        <th>STAR RATING</th>
                                        <th>RATE</th>
                                        <th>FOLLOWERS</th>
                                        <th>STATUS</th>
                                        <th>ACTIONS</th>
                                        <th>REASON</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(!empty($details) && $details && count($details) > 0)
                                    @foreach ($details as $post)
                                    @php
                                        $reason = '';
                                        $reinvite = 0;
                                        $reject_amount = '';
                                    @endphp
                                        @if($post->status != 1 )
                                     <?php
                                    if($post->user->SocialDetail->facebook_friends) {
                                       if($post->user->SocialDetail->facebook_friends >= 1000000){
                                        $facebook_friends = number_format(($post->user->SocialDetail->facebook_friends / 1000000))."M";
                                       }else if($post->user->SocialDetail->facebook_friends >= 1000){
                                        $facebook_friends = number_format(($post->user->SocialDetail->facebook_friends / 1000))."K";
                                       }else {
                                        $facebook_friends = $post->user->SocialDetail->facebook_friends;
                                       }
                                    }


                                    if($post->user->SocialDetail->twitter_friends) {
                                       if($post->user->SocialDetail->twitter_friends >= 1000000){
                                        $twitter_friends = number_format(($post->user->SocialDetail->twitter_friends / 1000000))."M";
                                       }else if($post->user->SocialDetail->twitter_friends >= 1000){
                                        $twitter_friends = number_format(($post->user->SocialDetail->twitter_friends / 1000))."K";
                                       }else {
                                        $twitter_friends = $post->user->SocialDetail->twitter_friends;
                                       }
                                    }

                                    if($post->user->SocialDetail->instagram_friends) {
                                       if($post->user->SocialDetail->instagram_friends >= 1000000){
                                        $instagram_friends = number_format(($post->user->SocialDetail->instagram_friends / 1000000))."M";
                                       }else if($post->user->SocialDetail->instagram_friends >= 1000){
                                        $instagram_friends = number_format(($post->user->SocialDetail->instagram_friends / 1000))."K";
                                       }else {
                                        $instagram_friends = $post->user->SocialDetail->instagram_friends;
                                       }
                                    }

                                 ?>
                                        <tr>
                                            <td>
                                                <div class="user-image">
                                                @if(!empty($post->user->Images[0]->image))
                                                <?php $image = $post->user->Images[0]->image;  ?>
                                                <img src="{{$image}}" alt="upload-one" class="img-responsive round"/>
                                                @else
                                                <img src="{{ asset('business/assets/images/profile-images/profile-2.png') }}" alt="">
                                                @endif
                                                </div>
                                            </td>
                                            <td class="user-name">{{ $post->user->name ?? 'N/A'}}</td>
                                            <td>
                                            <div class="user-ratings">
                                                @if(count($post->user->infulancerRating)>0)
                                                    <?php $ratings = round($post->user->infulancerRating->avg('rating'));?>
                                                    @for($i=0;$i<$ratings;$i++)
                                                    <img src="{{asset('business/assets/images/icons/star1.svg')}}" alt="">
                                                    @endfor
                                                @else
                                                    Not Rated
                                                @endif
                                            </div>
                                            </td>                                            
                                            <td class="post-rate">KSH {{round(($post->price / 100) * 75)}}</td>
                                            <td>
                                                <div class="social-media">
                                                    <a href="#" class="followers">
                                                        <img src="{{ asset('business/assets/images/icons/insta.svg') }}" alt="">
                                                        <p>{{$instagram_friends}}</p>
                                                    </a>
                                                    <a href="#" class="followers">
                                                        <img src="{{ asset('business/assets/images/icons/fb.svg') }}" alt="">
                                                        <p>{{$facebook_friends}}</p>
                                                    </a>
                                                    <a href="#" class="followers">
                                                        <img src="{{ asset('business/assets/images/icons/twit.svg') }}" alt="">
                                                        <p>{{$twitter_friends}}</p>
                                                    </a>
                                                </div>
                                            </td>
                                            @php
                                                if($post->status == 6){
                                                    $status = 'Pending';
                                                    $status_btn = 'ongoing';
                                                }else if($post->status == 2){
                                                    $reason = $post->reason;
                                                    $status = 'Declined';
                                                    $status_btn = 'declined';
                                                    if($reason == 'Low Pay') {
                                                        $reinvite = 1;
                                                        $reject_amount = $post->reject_amount;
                                                    }
                                                } else {
                                                    $status = 'Accepted';
                                                    $status_btn = 'completed';
                                                }


                                            @endphp

                                            <td class="{{ $status_btn }}">
                                                <p>{{ $status }}</p>
                                            </td>

                                            <td>
                                                @if($post->is_submit == 1)
                                                <!-- <button class="hire-me-btn">Accept</button> -->
                                                @endif
                                                @if($reinvite == 1)
                                                <a href="{{url('business/re-submit-post',base64_encode($post->post_id))}}">
                                                    <button class="hire-me-btn" style="margin-left: 48px;">REINVITE</button>
                                                </a>
                                                @else
                                                <?php
                                                    $url = base64_encode($post->post_id);
                                                    $url1 = base64_encode($post->user_id);

                                                    $base64_encode_url = url('business/post-reminder').'/'.$url.'/'.$url1;
                                                ?>
                                                <a href="{{$base64_encode_url}}"><button class="hire-me-btn">Send Reminder</button></a>
                                                @endif
                                                {{-- <button class="hire-me-btn" style="margin-left: 48px;">Adjust Price</button> --}}
                                            </td>
                                            <td>
                                                <p>{{ $reason ?? '' }} @if($reject_amount) Reject Amount : {{ $reject_amount }} @endif</p>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    @else
                                        <tr class="data_found text-center">
                                        <td colspan="8" class="border border-0 p-5">
                                            <img src="{{ url('public/business/images/empty.svg') }}" style="width: 87px; height: 113px;">
                                            <h1 class="text-center" style="font-size: 20px; margin-top: 10px;">No Data Found!</h1>
                                        </td>
                                    </tr>
                                        @endif
                                    


                                </tbody>

                            </table>
                        </div>

                        <div class="pagination">
                            <div class="col-sm-5">
                                <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                                <div class="">
                                    <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                        {{-- <ul class="pagination">
                                            <li class="paginate_button previous disabled" id="example_previous">
                                                <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                                    <img src="./assets/images/icons/button.svg" alt="">
                                                </a>
                                            </li>

                                            <li class="paginate_button active">
                                                <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                            </li>

                                            <li class="paginate_button ">
                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                            </li>

                                            <li class="paginate_button next" id="example_next">
                                                <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                                    <img src="./assets/images/icons/Right.svg" alt="">
                                                </a>
                                            </li>

                                        </ul> --}}
                                        {{ $details->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>