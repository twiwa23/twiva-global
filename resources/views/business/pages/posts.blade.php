@extends('business.layout.app')
@section('title')
    Posts
@endsection

@section('content')
<div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->


            <!-- page content -->
            <div class="right_col dashboard-page" role="main">

                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('business/posts') }}" class="breadcrumb-heading">Posts</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
@include('business.layout.notifications')
                <div class="catalog-page">
                    <div class="select-top select-search" id="feedpost">
                        <div class="search-wrap">
                            {{-- <div class="search-panel">
                                <input type="text" placeholder="Search here...">
                            </div> --}}

                            <div class="button-catalog">
                                <div class="drop-btn-wrapper">
                                    {{-- <div class="drop-btn" style="width: 85px;">
                                        <div class="select-btn" style="width: 100%;">
                                            <div class="btn__trigger">Date</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="Date1">Date1</span>
                                                <span class="btn-option" data-value="Date2">Date2</span>
                                                <span class="btn-option" data-value="Date3">Date3</span>
                                                <span class="btn-option" data-value="Date4">Date4</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div> --}}

                                    {{-- <div class="drop-btn" style="width: 92px;">
                                        <div class="select-btn" style="width: 100%;">
                                            <div class="btn__trigger">Status</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="Status1">Status1</span>
                                                <span class="btn-option" data-value="Status2">Status2</span>
                                                <span class="btn-option" data-value="Status3">Status3</span>
                                                <span class="btn-option" data-value="Status4">Status4</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div> --}}
                                </div>


                                <a href="{{ url('business/create-post-field') }}" class="create-btn">
                                    Create Posts
                                </a>

                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>IMAGE</th>
                                    <th>TITLE</th>
                                    <th>EXPIRATION DATE</th>
                                    <th>ACCEPTED</th>
                                    <th>PENDING</th>
                                    <th>DECLINED</th>
                                    <th>STATUS</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if(!empty($post_data) && $post_data && count($post_data) > 0)
                                @foreach($post_data as $post)
                                    <tr>
                                        <td>
                                            <div class="user-image">
                                            @if(!empty($post->Images[0]->media))
                                            <img src="{{$post->images[0]->media}}" alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>
                                            @else
                                            <img src="{{url('public/business/images/default.svg')}}" alt="upload-one" class="img-responsive image" title="Click to expand image/video">
                                            @endif
                                            </div>
                                        </td>
                                        <td class="title"><a href="{{url('business/post-details',base64_encode($post->id))}}">{{ $post->post_name }} </a> </td>
                                        <td>{{ $post->expiration_date ?? '--'}}</td>
                                        @php
                                            $accepted = 0;
                                            $pending = 0;
                                            $decline = 0;
                                            $status = "OnGoing";
                                            $status_btn = "ongoing";
                                        @endphp

                                        @if(!empty($post->postUserCompleted))
                                        @foreach($post->postUserCompleted as $post_status)
                                            @if($post_status->status == 2)
                                                @php $decline++ @endphp
                                            @endif
                                            @if($post_status->status == 5)
                                                @php $accepted++ @endphp
                                                @php $status = "Completed" @endphp
                                                @php $status_btn = "completed" @endphp
                                            @endif
                                            @if($post_status->status == 6)
                                                @php $pending++ @endphp
                                            @endif
                                        @endforeach
                                        @endif
                                        <td>{{ $accepted }}</td>
                                        <td>{{ $pending }}</td>
                                        <td>{{ $decline }}</td>
                                        <td class="{{ $status_btn }}">
                                            <p>{{ $status }}</p>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                <td class="text-center p-5 border border-0" colspan="7">
                                    <img src="{{asset('influencers/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                    <h3 style="font-size: 20px;">You haven’t created any Posts yet</h3>
                                </td>
                            </tr> 
                            @endif

                            </tbody>
                        </table>
                    </div>
                    <!-- @if(count($post_data)==0)
                    <div class="card product-box-inner p-3" style="border:0;" id="nf">
                                <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                                    <img src="{{ asset('business/assets/images/icons/empty.svg') }}" alt="">
                                    <h3 style="font-size:20px;">
                                        You haven't created any Post
                                    </h3>
                                </div>
                    </div>
                    @endif -->
                    <div class="pagination">
                        <div class="col-sm-5">
                            <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                            <div class="">
                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                    {{-- <ul class="pagination">
                                        <li class="paginate_button previous disabled" id="example_previous">
                                            <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">
                                                <img src="./assets/images/icons/button.svg" alt="">
                                            </a>
                                        </li>

                                        <li class="paginate_button active">
                                            <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">7</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">8</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">9</a>
                                        </li>

                                        <li class="paginate_button ">
                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">10</a>
                                        </li>

                                        <li class="paginate_button next" id="example_next">
                                            <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">
                                                <img src="./assets/images/icons/Right.svg" alt="">
                                            </a>
                                        </li>

                                    </ul> --}}
                                    {!! $post_data->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        setTimeout(function(){ functionTobeLoaded() }, 200);

        function functionTobeLoaded(){
            $('.pagination li:first-child').addClass('white-btn');
            $(".pagination li:last-child").addClass("red-btn");
            $('.pagination li:first-child span').text('Previous');
            $('.pagination li:first-child a').text('Previous');
            $('.pagination li:last-child span').text('Next');
            $('.pagination li:last-child a').text('Next');
        }
    });
</script>
@endsection