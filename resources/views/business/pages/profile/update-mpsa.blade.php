@extends('business.layout.app')
@section('title')
    Update MPesa Account
@endsection
@section('styles')
<style>
    label#text-error {
        color: red;
    }
</style>

@endsection
@section('content')
<div class="influencers-profile">
    <div class="dashboard_container">
        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->
        <!-- Page Content -->
        <div class="right_col " role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="#">Account Settings<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#">Update Mpesa Account</a></li>
                </ul>
            </div>
            <!--********** Breadcrumb End ***********-->

            <div class="post-container hire-me">
                <div class="post-heading">
                    <h3>Update Mpesa Account</h3>
                </div>
            @include('business.layout.notifications')
            <form method="POST" id="form_validate" novalidate="novalidate">
                 {{csrf_field()}}
                <div class="post-input-container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="input-section">
                                <div class="form-group">
                                    <label>Registered Mpesa Phone Number</label>
                                    <input type="text" name="register_mpesa_phone_number" id="register_mpesa_phone_number" onkeypress="return isNumberKey(event)" value = "{{$data->phone_number}}" class="form-control"  placeholder="">
                                    <span class="text-danger"></span>
                                    <label id="register_mpesa_phone_number-error" class="error" for="register_mpesa_phone_number">{{$errors->first('register_mpesa_phone_number')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="btn-wrapper">
                    <button type="submit" class="red-btn">UPDATE</button>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
}, "Space not allowed");

$('#form_validate').validate({
    rules: {
        register_mpesa_phone_number: {
            required: true,
            minlength:8,
            maxlength:15
        },
    },

    messages:{
        register_mpesa_phone_number:{
        required:'Please enter register mpesa phone number.',
        minlength:'Register mpesa phone number must be at least 8 digits.',
        maxlength:'Register mpesa phone number  maximum 15 digits.',
        },
    },
});
</script>
@endsection
