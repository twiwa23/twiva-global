@extends('business.layout.app')
@section('title')
Review & Rating
@endsection
@section('content')
<div class="backdrop"></div>

    <div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- Sidebar menu -->
                    @include('business.layout.side_menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->

            <!-- page content -->
            <div class="right_col dashboard-page" role="main" id="approved-page">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('business/posts') }}" class="active">Posts<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                        <li><a href="{{ url('business/post-details/'.base64_encode($id)) }}" class="active">{{ $postDetail->post_name ?? 'Post' }}<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                        <li><a href="#">Rate</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->

                <div class="view-post">
                    <div class="post-heading">
                        <h1>{{ $user->name ?? 'User'}}</h1>
                        <div class="blue-box">Approved</div>
                    </div>
                </div>
                <form method="POST" id="form_validate" novalidate="novalidate" autocomplete="off">
                    {{csrf_field()}}
                <div class="influencer-ratings">
                    <h1>Please rate the influencer</h1>
                    <div class="review-wrapper starrating">
                        <input type="radio" id="rate5" class="rating-value" data-rating="5" name="stars" value="5"/>
                        <label for="rate5" title="5 star"></label>

                        <input type="radio" id="rate4" class="rating-value" data-rating="4" name="stars" value="4"/>
                        <label for="rate4" title="4 star"></label>

                        <input type="radio" id="rate3" class="rating-value" data-rating="3" name="stars" value="3"/>
                        <label for="rate3" title="3 star"></label>

                        <input type="radio" id="rate2" class="rating-value" data-rating="2" name="stars" value="2" />
                        <label for="rate2" title="2 star"></label>

                        <input type="radio" id="rate1" class="rating-value" data-rating="1" name="stars" value="1"/>
                        <label for="rate1" title="1 star"></label>
                    </div>
                    <span id="stars-error" class="error" for="stars"></span>
                    <h1>Add Comment (Optional)</h1>
                    <textarea name="review" maxlength="1000" id="review"  rows="5">{{old('review')}}</textarea>
                    <span id="review-error" class="error" for="review">{{$errors->first('review')}}</span>
                    <br>
                    <button class="red-btn">Submit</button>
                </div>
                </form>


                {{-- <div class="post-details">
                    <p>21th Dec, 2020 <img src="{{ asset('business/assets/images/icons/bullet.svg') }}"> 14:35</p>
                    <h4>Approved Post</h4>

                    <ul class="img-lists">
                        <li class="img-list-item">
                            <img src="{{ asset('business/assets/images/profile-images/Rectangle725.png') }}">
                        </li>
                    </ul>

                    <div class="caption-lists">
                        <div class="caption-list-item">
                            <div>
                                <label class="caption-label">Approved Caption</label>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                </div> --}}
            </div>
            <!-- /page content -->
        </div>
    </div>

@endsection
@section('scripts')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(':radio').change(function() {
    console.log('New star rating: ' + this.value);
});
 $('#form_validate').validate({
      rules: {
          stars: {
              required: true,
              maxlength:30,
          },

          review: {
              required: true
          }
      },

        messages:{
          stars:{
            required:'Please select rating.'
          },
          review:{
            required:'Please enter comment.'
          },

        },

        submitHandler : function(form){
         if(calculateDateTime() == 1){
          localStorage.setItem("post_data",JSON.stringify({
            "title" : $("#title").val(),
            "description" : $("textarea[name='description']").val(),
            "contact_person_name" : $("#contact_person_name").val(),
            "contact_person_number" : $("#contact_person_number").val(),
            "gig_start_date" : $("#gig_start_date").val(),
            "gig_start_time" : $("textarea[name='gig_start_time']").val(),
            "gig_end_date" : $("#gig_end_date").val(),
            "gig_end_time" : $("#gig_end_time").val(),
          }))
            form.submit();
         }
        }
  });
</script>
@endsection
