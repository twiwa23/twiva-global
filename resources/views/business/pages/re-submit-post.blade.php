@extends('business.layout.app')
@section('title')
    Re Submit Post
@endsection
@section('styles')
<style>
    label#price-error {
        color: red;
    }
</style>

@endsection
@section('content')
<div class="influencers-profile">
    <div class="dashboard_container">
        <!-- Left Column Start -->
        <div class="left_col">
            <div class="scroll-view">
                <div class="clearfix"></div>

                <!-- Sidebar menu -->
                @include('business.layout.side_menu')
                <!-- /Sidebar menu -->

            </div>
        </div>
        <!-- Left Column End -->
        <!-- Page Content -->
        <div class="right_col " role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="{{url('business/post-details',base64_encode($declinedPostDetails->post_id))}}" class="active">{{ $declinedPostDetails->post->post_name }}<img src="{{ asset('business/assets/images/icons/chevron-right-gray.svg') }}" alt=""></a></li>
                    <li><a href="#">ReSubmit Post Price</a></li>
                </ul>
            </div>
            <!--********** Breadcrumb End ***********-->

            <div class="post-container hire-me">
                <div class="post-heading">
                    <h3>Re Submit Post Price</h3>
                </div>
            @include('business.layout.notifications')
            <form method="post" id="form_validate">
                 {{csrf_field()}}
                <div class="post-input-container">
                    <div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <div class="input-section">
                                <div class="form-group">
                                    <label>POST NAME</label>
                                    <input type="text" value="{{$declinedPostDetails->post->post_name ?? 'N/A'}}" readonly class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <div class="input-section">
                                <div class="form-group">
                                    <label>New Request Price</label>
                                    <input type="tel" maxlength="15" name="price" class="form-control" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <input type="hidden" name="infulancerId" value="{{$declinedPostDetails->user_id}}" class="form-control">

                <div class="post-input-container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="input-section">
                                <div class="form-group">
                                    <label>Influencer Name</label>
                                    <input type="text" value="{{$declinedPostDetails->user->name ?? 'N/A'}}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="btn-wrapper">
                    <button type="submit" class="red-btn">Submit</button>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
    }, "Space not allowed");

    $.validator.addMethod("notOnlyZero", function (value, element, param) {
        return this.optional(element) || parseInt(value) > 0;
    });

    $('#form_validate').validate({
        rules: {
            price: {
                required: true,
                notOnlyZero: true
            },
        },

        messages:{

            price:{
                required:'Please enter new request price.',
                notOnlyZero:'Price should be greater than 0.'
            },
        }
    });

</script>
@endsection
