 <section class="footer">
         <div class="container">
         <div class="col-md-6 col-xs-4">
            <div class="new-file">
              <a href="{{url('business/influencers-list')}}">
                 <figure>
                    <img src="{{url('public/business/images/logo-footer.png')}}" alt="">
                 </figure>
                
              </a>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                   
                     <!-- <p>info@twiva.com</p> -->
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
               <br>
                  <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>
            </div>
             <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Facebook"><a href="https://www.facebook.com/twiva/" target="_blank"><img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon"/></a></li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Twitter"><a href="https://twitter.com/Twiva_influence" target="_blank"><img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon"/></a></li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Instagram"><a href="https://www.instagram.com/twiva_influence/" target="_blank"><img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon"/></a></li>
                  </ul>
               </div>
         </div>
      </div></section>
       



      <script src="{{url('public/business/js/jquery.min.js')}}"></script>
          <!--   <script src="{{url('public/business/js/sticky_footer.js')}}"></script> -->

            
      <script src="{{url('public/business/js/bootstrap.min.js')}}"></script>
      <script src="{{url('public/business/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
      <script src="{{url('public/business/js/custom.min.js')}}"></script>
      <script src="{{url('public/business/js/custom.js')}}"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

      <!-- Bootstrap -->
    </body>

    <script type="text/javascript">
    $(function() {
        setTimeout(function(){
            $(".alertz").hide();
        }, 5000);
    });

</script>

  </html>