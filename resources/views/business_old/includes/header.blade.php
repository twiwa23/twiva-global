<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('public/business/images/fav.png')}}" type="image/x-icon">
     <title>@yield('title')</title>
    <link rel="icon" href="{{url('public/business/images/fav.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{url('public/business/css/bootstrap-slider.min.css')}}">
    <link href="{{url('public/business/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('public/business/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{url('public/business/css/daterangepicker.css')}}" rel="stylesheet">
     <link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <link href="{{url('public/business/css/style.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <style type="text/css">
      textarea#set_compliment:focus {
      border-bottom: 2px solid #680d36;
      }
      .gig_box h5{
        height: 18px;
      }
      .tabs_list .tabs_bar {
        width: 100%;
      }
      .tabs_list ul.tabs {
        text-align: center;
      }
    </style>
  </head>
  <body class="nav-md footer_fixed">