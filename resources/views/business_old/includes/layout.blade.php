<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="scroll-view">
            <!-- sidebar menu -->
            
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="institute_logo">
                <a href="{{url('business/influencers-list')}}">
                  <img src="{{url('public/business/images/logo.png')}}">
                </a>
              </div>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="second">
                  <a href="{{url('business/update-personal-details')}}" class="user-profile">
                    <img src="{{url('public/business/images/user.png')}}" alt="">
                  </a>
                  <!-- <ul class="dropdown-menu dropdown-usermenu ">
                    <li><a href="edit-profile.html">Edit Profile</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> LogOut</a></li>
                  </ul> -->
                </li>
              </ul>
            </nav>
          </div>
        </div>