<style type="text/css">
    .alert-danger {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    width: 100%;
    margin: 0 auto;
}
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    width: 100%;
    margin: 0 auto;
}
</style>
@if(Session::has('success'))
  <div style="padding:11px;font-weight: 400;" class="alert alert-success alert-dismissible text-center alertz">{{ Session::get('success') }}</div>
@endif
@if(Session::has('danger'))
  <div style="padding:11px;font-weight: 400;" class="alert alert-danger alert-dismissible text-center alertz">{{ Session::get('danger') }}</div>
@endif

@if($errors->any())
    <!-- <div class="row alertz" id="alert1">
        <div class="alert alert-danger alert-dismissible fade in text-center">            
            <span>{!! $errors->first() !!}</span>
        </div>
        <div class="clearfix"></div>
    </div> -->
    <!-- <script type="text/javascript">
        $(function() {
            setTimeout(function(){
                $("#alert1").hide();
            }, 5000);
        });
    </script> -->
@endif


@if(Session::has('notification'))
    <div class="row alertz" id="alert2">
        <div class="alert alert-{{ Session::get('notification')['status'] == 'success' ? 'success' : 'danger'}} alert-dismissible fade in text-center">
            <span>{!! Session::get('notification')['message'] !!}</span>
        </div>
        <div class="clearfix"></div>
        
    </div>

    
@endif


<script type="text/javascript">
    $(function() {
        setTimeout(function(){
            $(".alertz").hide();
        }, 5000);
    });
</script>


