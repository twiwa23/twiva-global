<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
   <div class="menu_section">
      <?php 
         $id = Auth::guard('business')->user()->id;
         $data = DB::table('users')->where(['id'=>$id])->first();
         ?>
      <ul class="nav side-menu">
         <li class=<?php if(Request::is("business/creaters") || Request::is("business/portfolio/*") || Request::is("business/hire-me/*")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/creaters')}}">
            <img style="" src="{{url('public/business/images/creator1.png')}}" alt="">Creators
            </a>
         </li>

         <li class=<?php if(Request::is("business/influencers-list") || Request::is("business/influencer-details/*")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/influencers-list')}}">
            <img src="{{url('public/business/images/influencer.png')}}" alt="">Influencers
            </a>
         </li>
        
         @if($data->login_status == 0)
         <li class=<?php if(Request::is("business/post-list") || Request::is("business/create-post-desc")  || Request::is("business/create-post-field")  ||  Request::is("business/post-details/*")  || Request::is("business/create-post2") || Request::is("business/accept-post/*") || Request::is("business/accept-post-details/*") || Request::is("business/pending-post-list/*") || Request::is("business/pending-post-list/*") || Request::is("business/invite-influencers-post")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/create-post-desc')}}">
            <img src="{{url('public/business/images/user-one.png')}}" alt="">Posts
            </a>
         </li>
         @else
             <li class=<?php if(Request::is("business/post-list") || Request::is("business/create-post-desc")  || Request::is("business/create-post-field")  || Request::is("business/create-post2") || Request::is("business/post-details/*")  || Request::is("business/accept-post-details/*")  || Request::is("business/accept-post/*") || Request::is("business/pending-post-list/*") || Request::is("business/pending-post-list/*")  || Request::is("business/invite-influencers-post") || Request::is("business/post-summary-page/*") || Request::is("business/re-submit-post/*") || Request::is("business/invite-influencers-post/*") || Request::is("business/complete-post-list") || Request::is("business/post-filter/*") || Request::is("business/complete-accept-post/*") || Request::is("business/complete-post-details/*")  || Request::is("business/post-rating/*") || Request::is("business/declined-post/*")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/create-post-field')}}">
                <img src="{{url('public/business/images/user-one.png')}}" alt="">Posts
            </a>
            </li>
         @endif
         @if($data->login_status == 0)
         <li class=<?php if(Request::is("business/gig-list") || Request::is("business/gig-details/*")  || Request::is("business/create-gig-desc")  || Request::is("business/create-gig-filed-2")  || Request::is("business/create-gig-field")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/create-gig-desc')}}">
            <img src="{{url('public/business/images/lock.png')}}" alt="">Gigs
            </a>
         </li>
         @else
         <li class=<?php if( Request::is("business/gig-list") 
         || Request::is("business/gig-details/*")  
         ||  Request::is("business/create-gig-filed-2") 
         || Request::is("business/create-gig-field")
         || Request::is("business/accept-gig/*")
         || Request::is("business/accept-gig/{gig_id}")
         || Request::is("business/pending-gig-list/*")
         || Request::is("business/pending-gig-list/{gig_id}")
         || Request::is("business/declined-gig/*")
         || Request::is("business/declined-gig/{gig_id}")
         || Request::is("business/re-submit-gig/{gig_id}")
         || Request::is("business/re-submit-gig/*")
         || Request::is("business/gig-filter/*")
         || Request::is("business/invite-influencers-gig/*")
         || Request::is("business/gig-summary-page/*")
         || Request::is("business/gig-complete-list") 
         || Request::is("business/gig-rating/*") 
         || Request::is("business/complete-accept-gig/*")
         || Request::is("business/create-gig-desc"))
          {
            echo 'active';
         }
         ?>>
            <a href="{{url('business/create-gig-filed-2')}}">
            <img src="{{url('public/business/images/lock.png')}}" alt="">Gigs
            </a>
         </li>
         @endif
         <li class=<?php if(Request::is("business/notification")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/notification')}}">
            <img src="{{url('public/business/images/notification.png')}}" alt="">Notifications
            </a>
         </li>
         <li class=<?php if(Request::is("business/settings") || Request::is("business/update-personal-details") || Request::is("business/change-password") || Request::is("business/update-mpsa-account")) {echo 'active';} else {echo '';}?>>
            <a href="{{url('business/settings')}}">
            <img src="{{url('public/business/images/settings.png')}}" alt="">Settings
            </a>
         </li>
         
         <li>
            <a href="{{url('business/logout')}}">
            <img  src="{{url('public/business/images/login.png')}}" alt="">
            Logout
            </a>
         </li>
      </ul>
   </div>
   <div class="circular_img">
      <img src="{{url('public/business/images/circular_img.png')}}" alt="circular_img">
   </div>
</div>