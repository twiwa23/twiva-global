<div class="row">
    <div class="col-md-12">
        <div class="left-logo">
            <figure><a href="{{url('user/home')}}"><img src="{{url('public/website_assets/images/main-logo.png')}}" alt=""></a></figure>
        </div>
        <div class="middle">
            <h2 class="heading tprpy">@yield("title")</h2>
        </div>
        <div class="left-menu-icons lead">
            <ul>                
                <li class="last-toggle ">
                    <a href="javascript:void(0);"><img src="{{url('public/website_assets/images/toggle.png')}}" alt="" onclick="openNav()"></a>
                </li>
            </ul>
        </div>
    </div>
</div>