@include('business/includes.header')
<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="scroll-view">
            <!-- sidebar menu -->
            	@include('business/includes.sidebar')
            <!-- /sidebar menu -->
          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="institute_logo">
                <a href="{{url('business/influencers-list')}}">
                  <img src="{{url('public/business/images/logo.png')}}">
                </a>
              </div>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="second">
                <?php  $id = Auth::guard('business')->user()->id;
                    $user = App\Model\User::where('id',$id)->first();
                    if(!empty($user->profile)){
                      $img = $user->profile;
                   
                    }else{
                      $img = url('public/business/images/user.png');
                    }
                   ?>
                        <a href="{{url('business/update-personal-details')}}" class="user-profile">
                    <img src="{{$img}}" alt="">
                  </a>   
                              
    
                              
                 <!--  <a href="{{url('business/update-personal-details')}}" class="user-profile">
                    <img src="{{url('public/business/images/user.png')}}" alt="">
                  </a> -->
                </li>
              </ul>
            </nav>
          </div>
        </div>
@yield('content')
@include('business/includes.footer')
@yield('js')


