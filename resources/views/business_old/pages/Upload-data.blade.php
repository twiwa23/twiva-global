@extends("business/layout/web")
@section("title","Uploaded Data")
@section("content")
<style type="text/css">
  label#text-error {
    color: red;
    font-size: 16px;
  }
</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col bg_banner" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Uploaded Data</h2>
                      <form method="post" id="form_validate">
                        {{csrf_field()}}
                         <div class="uploaded-data" style="margin-top: 30px;">
                          <div class="label_addres">
                            <label for="">
                              Enter your reason of rejection so influencer can update the same again
                            </label>
                          </div>
                          <textarea type="text" name="text" maxlength="1000" placeholder=""></textarea>
                          <div class="space-top top">
                            <input type="submit" name="SEND" value="SEND" class="button">
                          </div>
                        </div>
                      </form>
                </div>
              </div>
            </div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
   $( document ).ready(function() {
     console.log( "document ready!" );
   
     var $sticky = $('.sticky');
     var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
     if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
       var generalSidebarHeight = $sticky.innerHeight();
       var stickyTop = $sticky.offset().top;
       var stickOffset = 0;
       var stickyStopperPosition = $stickyrStopper.offset().top;
       var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
       var diff = stopPoint + stickOffset;
   
       $(window).scroll(function(){ // scroll event
         var windowTop = $(window).scrollTop(); // returns number
   
         if (stopPoint < windowTop) {
             $sticky.css({ position: 'absolute', top: diff });
         } else if (stickyTop < windowTop+stickOffset) {
             $sticky.css({ position: 'fixed', top: stickOffset });
         } else {
             $sticky.css({position: 'absolute', top: 'initial'});
         }
       });
   
     }
   });
</script>

<script type="text/javascript">

   jQuery.validator.addMethod("noSpace", function(value, element) {
      return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
   }, "Space not allowed");

   $('#form_validate').validate({ 
      rules: {
          text: {
              required: true,
              noSpace:true
          },           
      },
   
      messages:{
          
         text:{  
            required:'Please enter reason.'
         },             
      }
   });
</script>
@endsection