@extends("business/layout/web")
@section("title","Accepted Post Submission Details")
@section("content")
<style type="text/css">
  .new-fine {
    display: flex;
}
input.form-control {
    width: 97%;
}
.new-fine input#vehicle1 {
     position: relative;
    top: 26px;
    width: 20px;
    height: 18px;

}

ul {
  list-style-type: none;
}

li {
  display: inline-block;
}


.uploaded-data .white_bg_sec.common_white_bg textarea.form-control{
    border: 2px solid #ccc!important;
    margin-bottom: 7px;
    margin-right: 6px;
    background-color: #fff !important: 
}

.error {
    color: red;
    font-size: 17px;
    padding-left: 0px;
    margin-left: 0px;
    margin-top: 0px;
    padding-top: 0px;
    border: 1px solid #fff;

}
.check_box_img {
  width: 20px;
  height: 18px;
  /*margin: auto !important;*/
  margin-left: 68px !important;
  margin-top: -4px !important;
  margin-bottom: 12px !important;
}
.size_chnage.theme_photos ul li img {
  width: 160px;
  height: 160px;
  object-fit: cover;
}
.size_chnage.theme_photos ul li video {
  width: 160px;
    height: 160px;
 object-fit: cover;
 margin-right: 5px;
 margin-bottom: 7px;
    border-radius: 5px;
}

section.accpet_post .theme_photos ul li img {
    margin-right: 33px;
    margin-top: 10px;
}

section.accpet_post .theme_photos ul li video {
    margin-right: 33px;
    margin-top: 10px;
}

video.target_vidoe {
    height: 400px;
}

</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Accepted Post Submission Details</h2>
   <section class="uploaded-data common-struct login_FORM login-input accpet_post">
      <div class="white_bg_sec common_white_bg">
        <form method="post" id="form_validate" onsubmit="form_validate1()"> 
            {{csrf_field()}}
            <input type="hidden" name="post_id" value="{{$acceptPostDetails->post_id}}">
            <input type="hidden" name="infulancer_id" value="{{$acceptPostDetails->user_id}}">
            <div class="labels">
              <h5 style="margin-bottom: 12px;">Caption</h5>
                <div class="new-fine">
                  <textarea class="form-control" disabled style="background-color: #fff;">{{$acceptPostDetails->postDetail->text_one ?? 'N/A'}}</textarea>
                  <input type="checkbox" id="vehicle1" name="text" value="{{$acceptPostDetails->postDetail->text_one ?? ''}}">
                </div>

                <div class="new-fine">
                  <textarea class="form-control" disabled style="background-color: #fff;">{{$acceptPostDetails->postDetail->text_two ?? 'N/A'}}</textarea>
                  <input type="checkbox" id="vehicle1" name="text" value="{{$acceptPostDetails->postDetail->text_one ?? ''}}">
                </div>

                <div class="new-fine">
                  <textarea class="form-control" disabled style="background-color: #fff;">{{$acceptPostDetails->postDetail->text_three ?? 'N/A'}}</textarea>
                  <input type="checkbox" id="vehicle1" name="text" value="{{$acceptPostDetails->postDetail->text_one ?? ''}}">
                </div>
                <div class="new-fine">
                  <label id="text-error" class="error" for="text" style="display: none;"></label>
                </div>
            </div>

             <div class="labels">
                <h5>Theme Photos</h5>
                <div class="theme_photos size_chnage" style="margin-bottom: 20px">
                   
                    <ul>
                        @if($acceptPostDetails->postDetail && !empty($acceptPostDetails->postDetail))                                  
                        <?php $videos = $All_files = "";
                           $no_doc = 0;
                           $total = 0;
                           $video_ext = ["mp4","avi","3gp","flv","mov","video"]; 
                           $i=1; $k=1;
                        ?>
                        @forelse($acceptPostDetails->postDetail->media as $postDetail)
                        <li>
                            <?php
                              $total++;
                              $get_file = $postDetail->media;
                              $has_video = false;
                              $has_file = false;
                              $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                              $get_file_type = strtolower($get_file_type);
                              if(in_array($get_file_type,$video_ext)){
                                echo '<label for="cb'.$k++.'"><video src="'.$get_file.'" controls="" class="img-responsive video-containt video" " ></video></label><input type="checkbox" id="cb'.$i++.'" name="image" value="'.$postDetail->media.'"  class="radio check_box_img" />';
                              }else {
                                echo '<label for="cb'.$k++.'"><img src="'.$get_file.'" " class="img-responsive image" style="cursor:pointer"/></label><input type="checkbox" id="cb'.$i++.'" name="image" value="'.$postDetail->media.'"  class="radio check_box_img" />';
                              }
                            ?>    
                        </li>
                        @empty
                        N/A
                        @endforelse
                        @else
                        N/A
                        @endif
                        
                     </ul>
                        <div class="">
                          <label id="image-error" class="error" for="image" style="margin-top: 8px;"></label>
                        </div>
                    <div class="new-fine">
                      <span id="image-error" class="error"></span>
                    </div>
                </div>
             </div>
             <div class="button_right_lrft">
                <?php 
                    $url = base64_encode($acceptPostDetails->post_id);
                    $url1 = base64_encode($acceptPostDetails->user_id);

                    $base64_encode_url = url('business/upload-data').'/'.$url.'/'.$url1;  
                ?>
                <div class="form-group Submit medium_btn m_top" style="text-align: left;     margin: 29px 0 27px !important;">
                   <a href="{{$base64_encode_url}}">
                   <input type="button" name="Login" value="REQUEST EDITS" class="btn btn-primary">
                   </a>
                </div>
                <div class="form-group Submit medium_btn m_top" style="text-align: right;     margin: 29px 0 27px !important; ">
                   <!-- <a href="{{url('business/accept-post')}}"> -->
                   <input type="Submit" name="Login" value="APPROVE" class="btn btn-primary">
                   <!-- </a> -->
                </div>
            </div>
        </form>
      </div>
   </section>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-lg view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <img src="" alt="woman"  class="target_img">
         </div>
         <div class="cross_icon" data-dismiss="modal">
            <a href="">
            <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
            </a>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-lg1 view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <video src="" alt="upload-one" class="target_vidoe" controls="" style = "width:100%;" title="Click to expand image"></video>
         </div>
         <div class="cross_icon" data-dismiss="modal">
            <a href="">
            <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
            </a>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script type="text/javascript">
   $('.image').click(function(){
    let img = $(this).attr('src')
    $(".target_img").attr('src',img)
     $('.bs-example-modal-lg').modal({
       backdrop: 'static'
     });
   }); 
    $('.video').click(function(){
    let video = $(this).attr('src')
    $(".target_vidoe").attr('src',video)
     $('.bs-example-modal-lg1').modal({
       backdrop: 'static'
     });
   }); 
</script>
<script type="text/javascript">
   $( document ).ready(function() {
     console.log( "document ready!" );
   
     var $sticky = $('.sticky');
     var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
     if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
       var generalSidebarHeight = $sticky.innerHeight();
       var stickyTop = $sticky.offset().top;
       var stickOffset = 0;
       var stickyStopperPosition = $stickyrStopper.offset().top;
       var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
       var diff = stopPoint + stickOffset;
   
       $(window).scroll(function(){ // scroll event
         var windowTop = $(window).scrollTop(); // returns number
   
         if (stopPoint < windowTop) {
             $sticky.css({ position: 'absolute', top: diff });
         } else if (stickyTop < windowTop+stickOffset) {
             $sticky.css({ position: 'fixed', top: stickOffset });
         } else {
             $sticky.css({position: 'absolute', top: 'initial'});
         }
       });
   
     }
   });

   $("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>

<script type="text/javascript">
   $('#form_validate').validate({ 
      rules: {
          
          text: {
              required: true
          },  
          image: {
              required: true
          },           
      },
   
      messages:{
          
         text:{  
            required:'Please select caption option.'
         }, 

         image:{  
            required:'Please select theme photo/video.'
         },             
      },   
   });
</script>
@endsection