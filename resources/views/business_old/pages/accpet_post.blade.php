@extends("business/layout/web")
@section("title","Accepted Post Influencers List")
@section("content")
<style>
      
.pagination {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
    float: right;
    color: red;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #410a2d;
    border-color: #410a2d;
}
.post_list .white_bg_sec{
  min-height: 328px;

}

.alert {
    width:700px !important;
}


img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
<h2 class="color-text">Accepted Post Influencers List</h2>
<div class="p_allside">
  @include('business.includes.notifications')
     <div class="tabs_list pending_button" style="margin-top: 12px;">
                          <div class="filter_btns button-size">
                            <a href = "javascript:void(0)" class="btn btn-primary mehron_btn ">Accepted</a>
                             <a href="{{url('business/pending-post-list',base64_encode($id))}}"
                             class="btn btn-primary green_btn ">Pending</a>
                             <a href="{{url('business/declined-post',base64_encode($id))}}"
                           class="btn btn-primary green_btn">Declined</a>
                           
                          </div>
                        </div>

   <section class="common-struct login-input equal_height equal_mb">
      <div class="post_list">
         <div class="row">
         @if(!empty($acceptPostDetails) && $acceptPostDetails && count($acceptPostDetails) > 0)
            @foreach($acceptPostDetails as $post)
               <div class="col-md-4">
                  <div class="white_bg_sec min_height_box">
                    <a href="{{url('business/influencer-details',base64_encode($post->user->id))}}">
                     <div class="Update-img text-center">
                        @if($post->user && $post->user->profile)
                           <img src="{{$post->user->profile}}" alt="upload-one" class="img-responsive round">
                        @else 
                           <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive round">
                        @endif
                     </div>
                     <h5 style="text-align: left;">{{$post->user->name ?? 'N/A'}}</h5>
                     <div class="date_content">
                        <span class="equal_width">Offered Price: </span>KSh{{$post->price ?? '0'}}
                     </div>
                     <div class="date_content">
                      <?php 
                        if($post->status == '1') {
                          $status = "Data not submitted yet"; 
                        } 
                        if($post->is_submit == '1' && $post->status == '1') {
                          $status = "Updated Data"; 
                        }

                        if($post->status == '3') {
                          $status = "Approved"; 
                        } 
                        if($post->status == '4') {
                          $status = "Requested Edit";
                        }
                      ?>
                        <span class="equal_width">Status:</span>{{$status ?? 'N/A'}} 
                     </div>
                    </a>
                     @if($post->is_submit == '1' && $post->status == '1')
                     <section class="login_FORM summary_deials m_t">
                        <div class="form-group Submit medium_btn " style="margin-bottom: 8px !important; width: 100% !important;">
                           <a href="{{url('business/accept-post-details',base64_encode($post->id))}}">
                           <input type="button" name="Login" value="VIEW" class="btn btn-primary" style="width: 200px !important;">
                           </a>
                        </div>
                     </section>
                     @endif
                  </div>
               </div>
            @endforeach
            {!! $acceptPostDetails->links() !!}
         @else
          <div class="data_found text-center accept">
             <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
             <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
          </div>
         @endif

         </div>
   </section>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
<script type="text/javascript">
    $(function() {
        setTimeout(function(){
            $(".alertz").hide();
        }, 5000);
    });
</script>

@endsection