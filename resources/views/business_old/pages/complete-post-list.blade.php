@extends("business/layout/web")
@section("title","Posts List")
@section("content")
<style>
   .common-struct .Update-img img {
     position: relative;
     border: 4px solid #fff;
     /*border-radius: 50%;*/
     box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
     width: 120px;
     height: 120px;
     margin: auto;
     cursor: pointer;
   }
   .common-struct .Update-img img{
        border-radius: unset;

}

   .common-struct .Update-img video {
     position: relative;
     border: 4px solid #fff;
     /*border-radius: 50%;*/
     box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
     width: 120px;
     height: 120px;
     margin: auto;
     cursor: pointer;
   }
  .pagination {
   
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
    color: red;
    display: flex;
    justify-content: center;
    align-items: center;
     width: 100%;
}
   .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
   z-index: 3;
   color: #fff;
   cursor: default;
   background-color: #410a2d;
   border-color: #410a2d;
   }
   .post_list .white_bg_sec{
   min-height: auto;
   }

 /* img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}
      video.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
    object-fit: cover;
}*/

.alert-success {
    width: 700px !important;
    margin: 0 auto;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
   <div class="col-sm-12">
      <div class="about-us">
        @include('business.includes.notifications')
         <h2 class="color-text">Posts List</h2>
         <div class="p_allside">
            <div class="tabs_list">
               <div class="tabs_bar">
                  <ul class="tabs">
                     <?php
                        $data = Auth::guard('business')->user();
                        
                         if($data->login_status == 0){
                           $url = url('business/create-post-desc');
                         }else{
                           $url =  url('business/create-post-field');
                         }
                         ?>
                     <a href="{{$url}}">
                        <li class="first">
                           Create
                        </li>
                     </a>
                     <a href="{{url('business/post-list')}}">
                        <li class="third">On Going</li>
                     </a>
                     <a href="{{url('business/complete-post-list')}}">
                        <li class="second">Completed</li>
                     </a>
                  </ul>
               </div>
               <!--  <div class="search_input">
                  <div>
                    <img src="{{url('public/business/images/search_icon.png')}}" alt="search_icon" alt="img-responsive">
                  </div>
                  <input type="text" placeholder="Search" class="form-control">
                  </div> -->
            </div>
            <section class="common-struct login-input heading_gap" style="margin-bottom: 40px;">
               <div class="post_list">
                  <div class="row">
                     @if(!empty($post_data) && $post_data && count($post_data) > 0)
                     @foreach($post_data as $post)
                     <div class="col-md-4">
                        <div class="white_bg_sec min_height_box">
                           <div class="Update-img text-center">
                               @if(!empty($post->user->profile))
                                    <img src="{{$post->user->profile}}" alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>'
                              @else
                              <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive image" title="Click to expand image/video">
                              @endif  
                           </div>
                           <a href="{{url('business/complete-post-details',base64_encode($post->id))}}" style="display: block; min-height: 175px !important;">
                              <h5 style="text-align: left;">{{$post->post_name ?? 'N/A'}}</h5>
                              <!-- <div class="date_content">
                                 <span>Date And Time:</span>&nbsp;&nbsp;&nbsp;{{$post->created_at}}
                              </div> -->
                              <p class="post_list_para">
                                 @if(strlen($post->description) > 150){{substr($post->description,0,150)}}... @else {{substr($post->description,0,150)}} @endif
                              </p>
                           </a>
                        </div>
                     </div>
                     @endforeach
                     @else 
                     <div class="data_found text-center">
                       <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                       <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Posts Completed Yet</h1>
                    </div>
                     @endif
                  </div>
                  {!! $post_data->links() !!}
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-lg view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <img src="" alt="woman"  class="target_img">
         </div>
         <div class="cross_icon" data-dismiss="modal">
            <a href="">
            <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
            </a>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-lg1 view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <video src="" alt="upload-one" class="target_vidoe" controls="" style = "width:100%;" title="Click to expand image"></video>
         </div>
         <div class="cross_icon" data-dismiss="modal">
            <a href="">
            <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
            </a>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $('.image').click(function(){
    let img = $(this).attr('src')
    $(".target_img").attr('src',img)
     $('.bs-example-modal-lg').modal({
       backdrop: 'static'
     });
   }); 
    $('.video').click(function(){
    let video = $(this).attr('src')
    $(".target_vidoe").attr('src',video)
     $('.bs-example-modal-lg1').modal({
       backdrop: 'static'
     });
   }); 
</script>
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
@endsection