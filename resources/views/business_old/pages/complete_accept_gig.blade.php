@extends("business/layout/web")
@section("title","Accepted Gig Influencers List")
@section("content")
<style type="text/css">
   .white_bg_sec {
      min-height: 200px;
   }
   .alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    width: 700px !important;
  }
  .pagination {
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
    color: red;
    display: flex;
    justify-content: center;
    align-items: center;
     width: 100%;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #410a2d;
    border-color: #410a2d;
}
  
img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Accepted Gig Influencers List</h2>

   <div class="p_allside">
    @include('business.includes.notifications')
      
	 
      <section class="common-struct login-input equal_height equal_mb">
         <div class="post_list">
            
            <!--<div class="row">
               <div class="col-md-4">
                  <a href="{{url('business/accept_gig_details')}}">
                     <div class="white_bg_sec">
                        <div class="Update-img text-center">
                           <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                        </div>
                        <h5 style="text-align: left;">Influencer Name</h5>
                        <div class="date_content">
                           <span class="equal_width">Offered Price: </span>$120.3
                        </div>
                     </div>
                  </a>
               </div>
            </div>-->
			<div class="row">
            <?php 
            $times = 0;
            ?>
         @if(!empty($acceptGigDetails) && $acceptGigDetails && count($acceptGigDetails) > 0)
            @foreach($acceptGigDetails as $gig)
            <?php 
            $times++;
            ?>
               <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12">
                  <div class="white_bg_sec">
                    <a href="{{url('business/influencer-details',base64_encode($gig->user->id))}}">
                     <div class="Update-img text-center">
                        <?php 
                             $userImage = url('public/business/images/upload-one.png');
                           ?>
                        @forelse($gig->user->images as $images)
                           @if($images->image && !empty($images->image))
                             <?php 
                               $userImage = $images->image;
                               break;
                             ?>
                           @endif
                         @empty
                         @endforelse
                          
                             <img src="{{$userImage}}" alt="upload-one" class="img-responsive img-responsive round" style="max-height: 160px;">
                          
                     </div>
                     <h5 style="text-align: left;">{{$gig->user->name ?? 'N/A'}}</h5>
                     <div class="date_content">
                        <span class="equal_width">Offered Price: </span>KSh{{$gig->price ?? '0'}}
                     </div>
                     <div class="date_content">
                      <?php 
                        if($gig->status == '1' && $gig->is_checkin == '0') {
                          $status = "Accepted"; 
                        } else if($gig->status == '1' && $gig->is_checkin == '1'){
							     $status = "Confirm Check In";
						      }else if($gig->status == '3'){
                          $status = "Check In Approved";
                        }
                      ?>
                        <span class="equal_width">Status:</span><span style="color:#5dc54e">Completed</span>  
                     </div>
                   </a>
                     @if($gig->is_rated == '0')
                     <section class="login_FORM summary_deials m_t">
                        <div class="form-group Submit medium_btn " style="margin-bottom: 8px !important; width: 100% !important;">
                          <?php 
                            $url = url('business/gig-rating').'/'.base64_encode($gig->user_id).'/'.base64_encode($gig->gig_id);
                          ?>
                           <a href="{{$url}}">
                           <input type="button" name="Login" value="Rate" class="btn btn-primary" style="width: 200px !important;">
                           </a>
                        </div>
                     </section>
                     @endif
                  </div>
               </div>
               @if($times % 3 == 0)
                  <div style="clear: both"></div>
               @endif
            @endforeach
            {!! $acceptGigDetails->links() !!}
         @else
         <div class="data_found text-center accept">
             <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
             <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
          </div>
         @endif

         </div>
			
			
         </div>
      </section>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
@endsection