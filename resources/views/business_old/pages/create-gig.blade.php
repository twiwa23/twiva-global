@extends("business/layout/web")
@section("title","Create Gig")
@section("content")
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Create Gig</h2>
   <ul class="tabs">
      <a href="{{url('business/create-gig-2')}}">
         <li class="first">Create</li>
      </a>
      <a href="gig_list.html">
         <li class="second">On Going</li>
      </a>
      <a href="gig_list.html">
         <li class="third">Completed</li>
      </a>
   </ul>
   <div class="hashtags">
      <p class="text"><span class="hash">Add a Theme</span>&nbsp;(Include any photos/videos to be used <br>as inspiration for the influencer)</p>
      <div class="img-div">
         <img src="{{url('public/business/images/upload-one.png')}}" alt="">
         <img src="{{url('public/business/images/upload-one.png')}}" alt="">
         <img src="{{url('public/business/images/upload-one.png')}}" alt="">
         <img src="{{url('public/business/images/upload-one.png')}}" alt="">
         <img src="{{url('public/business/images/upload-one.png')}}" alt="" class="add-button">
      </div>
      <div class="space-top top">
         <a href="{{url('business/invite-influ-gig')}}" class="button">CONTINUE</a>
      </div>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
@endsection