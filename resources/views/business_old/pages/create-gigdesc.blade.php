@extends("business/layout/web")
@section("title","Create Gig")
@section("content")
<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <div class="color-info">
    <span>
      <a  href="{{url('business/create-post-desc')}}">
    <i data-toggle="tooltip" data-placement="bottom" title="Info" class="fa fa-info-circle" aria-hidden="true"></i> 
   </a>
  </span>
  </div>
   <h2 class="color-text">Create Gig</h2>
   <div class="hashtags desc" style="max-width: 651px; margin-top: 15px;">
      <img src="{{url('public/business/images/women.jpg')}}" alt="" style="height: 100%; max-height: 290px; width: 100%;     border-top-left-radius: 10px;
    border-top-right-radius: 10px; object-fit: cover;">
      <div class="post-desc" style="margin-bottom: 173px;">
         <p class="posts">Gigs allows influencers and creatives (such as a comedian or photographer) to agree to attend an in-person event and exchange their social media value (i.e. Instagram stories) for possible payment or in-kind services (i.e., free admission, etc.). Gigs also allow you to run a campaign on Twitter to promote your business/products/services with the aim to trend and drive traffic to your business.</p>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow.png')}}" alt="">
            <p>To create a Gig, click the following button:</p>
         </div>
         <div class="small-wrapp">
            <a href="{{url('business/create-gig-filed-2')}}" class="button small">ADD A GIG</a>
         </div>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow1.png')}}" alt="">
            <p>Describe in detail the requirements of the Gig</p>
         </div>
         <ul class="details">
            <li> We advise you to include all possible details (e.g., how many Instagram stories, what they should bring and do etc. )</li>
         </ul>
         <div class="top-left" style="margin-bottom: 18px;">
            <img src="{{url('public/business/images/arrow2.png')}}" alt="">
            <p>Invite specific influencers and set their compensation and/or in-kind products</p>
         </div>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow3.png')}}" alt="">
            <p>Complete payment process.</p>
         </div>
         <ul class="details">
            <li>Twiva holds payment to influencers and only releases once the influencer(s)/creative(s) have completed the ask successfully. If a Gig is not completed, money will be refunded to your account</li>
         </ul>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow4.png')}}" alt="">
            <p>Check-in influencers on the day of your event/campaign</p>
         </div>
         <ul class="details">
            <li>Influencers will arrive/check-in at your event/campaign at the start time specified. On their app, they will indicate to you that they have arrived - you will be notified on your Inbox and asked to check them in. They can then enjoy the event/complete the Gig. This applies to those showing up virtually as well.</li>
         </ul>
         <div class="top-left" style="margin-bottom: 18px;">
            <img src="{{url('public/business/images/arrow5.png')}}" alt="">
            <p>Influencers will automatically ‘Check-out’ once the event/gig is done.</p>
         </div>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow6.png')}}" alt="">
            <p>You can release payment the next day, or it will automatically do so within 24 hours of the event/gig.</p>
         </div>
         <div class="space-top top continue">
            <a href="{{url('business/create-gig-filed-2')}}" class="button">CONTINUE</a>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
@endsection