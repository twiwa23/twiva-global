<!DOCTYPE html>
@extends("business/layout/web")
@section("title","Create Post")
@section("content")
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Create Post</h2>
                      <ul class="tabs">
                        <a href="{{url('business/create-post-field')}}">
                          <li class="first">
                              Create
                          </li>
                        </a>
                        <a href="{{url('business/post-list')}}">
                          <li class="second">On Going</li>
                        </a>
                        <a href="{{url('business/post-list')}}">
                          <li class="third">Completed</li>
                        </a>
                      </ul>
                       <div class="hashtags">
                      <p class="text"><span class="hash">Add Theme Photo</span>&nbsp;(Include any photos/videos to be used as inspiration for the influencer)</p>
                        <div class="img-div">
                          <img src="{{url('public/business/images/upload-one.png')}}" alt="">
                           <img src="{{url('public/business/images/upload-one.png')}}" alt="">
                            <img src="{{url('public/business/images/upload-one.png')}}" alt="">
                             <img src="{{url('public/business/images/upload-one.png')}}" alt="">
                             <span style="cursor: pointer;">
                             <img src="{{url('public/business/images/add.png')}}" alt="" class="add-button" class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Add More">
                               
                             </span>
                        </div>
                        <p class="text"><span class="hash">HashTag</span>&nbsp;(Include hashtags that will be attached to the created content<br> (e.g. #twiva #yourproductlaunch)) </p>
                        <div class="first-input">
                        <input type="text" class="form-control" value="#Facebook">
                        <img src="{{url('public/business/images/cross.png')}}" alt="">
                        
                      </div>
                       <div class="first-input">
                        <input type="text" class="form-control" value="#Instagram">
                        <img src="{{url('public/business/images/cross.png')}}" alt="">
                        
                      </div>
                       <div class="first-input">
                        <input type="text" class="form-control" value="#Twitter">
                        <img src="{{url('public/business/images/cross.png')}}" alt="">
                        
                      </div>
                      <div class="space-top top">
                      <a href="{{url('business/invite-influencers-post')}}" class="button">CREATE</a>
                    </div>
                    </div>
                </div>
            
              </div>
            </div>
       @endsection
       @section('js')
      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
   @endsection