@extends("business/layout/web")
@section("title","Create Post")
@section("content")
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Create Post</h2>
   <div class="hashtags desc" style="max-width: 651px; margin-top: 15px;">
      <img src="{{url('public/business/images/women.jpg')}}" alt="" style="height: 100%; max-height: 290px;     border-top-left-radius: 10px;
    border-top-right-radius: 10px; width: 100%; object-fit: cover;">
      <div class="post-desc" style="margin-bottom: 173px;">
         <p class="posts">Posts are invites to specific influencers to create content for a suggested price. A Post outlines the description of the posted social media content and only influencers that are invited can participate. Post currently are newsfeed posts, video-blogs (i.e., Instagram stories) as well as blogs.</p>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow.png')}}" alt="">
            <p>To create a Post, click the following button</p>
         </div>
         <div class="small-wrapp">
            <a href="{{url('business/create-post-field')}}" class="button small">ADD A POST</a>
         </div>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow1.png')}}" alt="">
            <p>Fill out the details:</p>
         </div>
         <ul class="details">
            <li>Give influencers information about the Post. (i.e., what the post should be about, things to do, things not to do).</li>
            <li>Upload theme photos/videos to inspire influencers on the type of content you would like to see.</li>
         </ul>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow2.png')}}" alt="">
            <p>Invite Influencers</p>
         </div>
          <ul class="details">
            <li>Invite influencers which match your needs and propose compensation level </li>
         </ul>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow3.png')}}" alt="">
            <p>Complete payment process.</p>
         </div>
          <ul class="details">
            <li>Twiva holds payment to influencer and only releases to influencer once sponsored content is posted. If Post is not completed, money will be refunded to your account</li>
         </ul>
         <div class="top-left">
            <img src="{{url('public/business/images/arrow4.png')}}" alt="">
            <p>Track progress of the sponsored content on our Post dashboard</p>
         </div>
         <div class="space-top top continue">
            <a href="{{url('business/create-post-field')}}" class="button">CONTINUE</a>
         </div>
      </div>
   </div>
</div>
@endsection

@section('js')
@endsection
