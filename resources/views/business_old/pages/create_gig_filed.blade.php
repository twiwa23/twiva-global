@extends("business/layout/web")
@section("title","Create Gig")
@section("content")
<style>
   
   .error {
    color: red;
   }
   .preview_image_cover .preview_video {
    max-height: 143px;
    max-width: 117px;
    height: 100px;
    width: 102px;
    background-color: #ffffff;
    border: 2px solid #000;
    border-radius: 3px;
}
.media_preview .img_video {
  padding-left: 0;
}
.media_preview .img_video .preview_image_cover {
  text-align: left;
}
.preview_image_cover.video {
    background-color: #fff;
    border: 2px solid #000;
    width: 89px;
    border-radius: 3px;
}
/*.preview_image_cover img {
    margin-left: 0;
    height: 138px;
    background-color: #000;
    }*/
.choose_images {
  width: 100px;
  height: 100px;
  object-fit: scale-down;
}

.text-danger {
    color: red;
    display: block;
}
.preview_image_cover img {
    margin-left: 0;
    height: 100px;
    object-fit: contain;
    background-color: #fff;
    border: 2px solid #000;
    width: 100%;
    margin-bottom: 8px;
    border-radius: 3px;
}

.media_preview .img_video {
    width: 120px;
    height: 110px;
    max-height: 110px;
    float: left;
    max-width: 120px;
    position: relative;
    word-break: break-all;
    border: 1px solid lightgray;
    margin: 0 15px 15px 0;
    padding: 2px;
}
.media_preview .preview_image {
  width: 112px;
  height: 104px;
  max-height: 104px;
  max-width: 112px;
  margin:0 !important;
}

.upload_image {
  display: none !important;
}
.images_placehold {
  cursor: pointer;
}

.remove-img {
   background-color: rgb(115,14,57);
    width: 20px;
    height: 20px;
    padding: 4px;
    border-radius: 50%;
    box-shadow: 0px 0px 54px 0px rgba(82, 82, 82, 0.35);
    position: absolute;
    right: -7px;
    top: -10px;
    background-image: url('{{url("public/business/images/white_cross_icon.png")}}');
    background-size: cover;
    cursor: pointer;
    z-index: 1;
}
.clear-fix {
  clear: both;
  width: 100%
}

#video_image_error {
  margin-top: 10px !important;
  font-weight: normal !important;
  color: red !important;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Create Gig</h2>
   <ul class="tabs">
      <a href="{{url('business/create-gig-filed-2')}}">
         <li class="first">Create</li>
      </a>
      <a href="{{url('business/gig-list')}}">
         <li class="second">On Going</li>
      </a>
     <a href="{{url('business/gig-complete-list')}}">
         <li class="third">Completed</li>
      </a>
   </ul>
   <div class="hashtags create_post_input">
      <form method="POST" id="form_validate" enctype="multipart/form-data" novalidate="novalidate" autocomplete="off">
         {{csrf_field()}}
         <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
         <div class="label_addres">
            <label for="">
          Compensation (How much you will pay each influencer you hire)
            </label>
         </div>
         <div class="form-group">
            <div class="email">
               <input type="text" id="gig_price_offering" maxlength="6" onkeypress="return isNumberKey(event)" name="gig_price_offering" value="{{old('gig_price_offering')}}" class="form-control" placeholder="">
               <span class="text-danger"></span>
            </div>
             <label id="gig_price_offering-error" class="error" for="gig_price_offering">{{$errors->first('gig_price_offering')}}</label>
         </div>
         <div class="label_addres">
            <label for="">
            Address
            </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="address" id="address"  rows="2" placeholder="">{{old('address')}}</textarea>
               <span class="text-danger"></span>
            </div>
             <label id="address-error" class="error" for="address">{{$errors->first('address')}}</label>
         </div>
         <div class="label_addres" style="margin-top: -5px;">
            <label for="">
            Venue
            </label>
         </div>
         <div class="form-group">
            <div class="email">
               <input type="text" id="venue" name="venue" value = "{{old('venue')}}" class="form-control" placeholder="">
               <span class="text-danger"></span>
            </div>
            <label id="venue-error" class="error" for="venue">{{$errors->first('venue')}}</label>
         </div>
         <div class="hashtags create_post_input">
            <p class="text"><span class="hash">Add Theme Photo</span>&nbsp;(Include any photos/videos to be used as inspiration for the influencer)</p>
            <div class="img-div add-menu img" style="clear: both">
               @php($placeHoldUrl =  url('public/business/images/add.png'))
                     
                 <div class="images_container">
                    <div class="media_preview"></div>
                 </div>

                    <div class="media_inputs">
                         <div class="img_video">
                            <input type="hidden" name="non_acceptable_files" class="non_acceptable_files">
                            <input type="hidden" class="ext_media_record" images="0" video="0"  total-media = "0" />
                            <img src="{{ $placeHoldUrl }}" class="images_placehold" title="Select image/video" data-recursion="-1" />
                        </div>
                     </div>
                    <div class="clear-fix"></div>
                    <label class="custom_error" id="video_image_error"></label>
            </div>
         </div>

         <div style="clear: both;width: 100%"></div>

         <p class="text"><span class="hash">HashTag</span>&nbsp;(Include hashtags that will be attached to the created content<br> (e.g. #twiva #yourproductlaunch)) </p>
            <div class="form-group">
         <div class="email">
            <input type="text" name="hashtag" id="hashtag" maxlength="30" class="form-control" placeholder="" required>
            <label id="hashtag-error" class="error" for="hashtag">{{$errors->first('hashtag')}}</label>
         </div>
       </div>

     
      <div class="space-top top">
         <button type="submit" name="submit"  class="button">CONTINUE</button>
      </div>
   </form>
   </div>
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

  $.validator.addMethod("is_zero",function(val,elm){
    if(val && val.trim().length > 0){
   let  sum = val
        .toString()
        .split('')
        .map(Number)
        .reduce(function (a, b) {
            return a + b;
        }, 0);
      if(sum <= 0){
        return false
      }
    }
    return true;
  },"Price should be equal or more than 1")
  $('#form_validate').validate({ 
      rules: {
          venue: {
              required: true,
              maxlength:100
          },
          address: {
              required: true,
              maxlength:1000,
            
          },
     
          gig_price_offering: {
              required: true,
              minlength:1,
              maxlength:8,
              is_zero : true
             
          },
          hashtag: {
              required: true
          },
      },

        messages:{
          venue:{  
            required:'Please enter venue.',
            maxlength:'venue maximum 100 characters long.'
          },
          address:{  
            required:'Please enter address.',
            maxlength:'Address maximum 1000 characters long.'
          },
          
          gig_price_offering:{  
            required:'Please enter compensation.',
            minlength:'Compensation must be at least 1 digits.',
            maxlength:'Compensation maximum 8 digits.'
          },  
          hashtag:{  
            required:'Please enter hashtag.'
         },          
        },
        submitHandler:function(form){

                  if($("#email-error").text().length > 0){
                    return false;
                  }else {
                    form.submit();
                  }
          }
        /*errorHandler:function(e){
          console.log(e)
        }*/
       
    
  });

    </script>
 <!--   <script>
     function validateForm(){
      $("#invalid_file").hide();
      if($("input[name='image_video[]']").val() == ""){
        $("#invalid_file").show().text("Please upload theme photo.");
        return false;
      }else{
         $("#invalid_file").hide();  
      }
    }    
   </script> -->
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
<script type="text/javascript">

       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
       //-->
    </SCRIPT>
 <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});

/* show hide # in hashtag on focus in and focus out */
    /* show hide # in hashtag on focus in and focus out */
   $("#hashtag").on("click paste keypress",function(){
    let val = $(this).val();
    if(val[0] != "#"){
       $(this).val("#"+val)
    }
   })

   $("#hashtag").on("keydown",function(e){
      if(e.keyCode === 8){
      let val = $(this).val();
        if(val && val.trim().length == 1){
          return val == "#" ? false : true;
        }
      }
      return true;
   })
   .on("select",function(){
      $("#hashtag").on("keyup",function(e){
        let val = $(this).val();
        if(val[0] != "#"){
           $(this).val("#"+val)
        }
      })
   })
   .bind("cut",function(){
    setTimeout(function(){
      let val = $("#hashtag").val();
        if(val[0] != "#"){
           $("#hashtag").val("#"+val)
        }
    },100)
   })


   $("#hashtag").focusout(function(){
      let val = $(this).val();
      if(val[0] == "#"){
        $(this).val(val.substring(1))
      }
   })

   $(document).ready(function(){
       $("#hashtag").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
      })
    })

</script>
<script type="text/javascript" src="{{url('public/business/js/multiFileUpload.js')}}"></script>
@endsection