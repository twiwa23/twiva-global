@extends("business/layout/web")
@section("title","Create Gig")
@section("content")
<style>
  .words {
    color: #686868 !important;
    text-align: right;
    font-size: 12px;
    margin-top: 3px;
  }
   .error {
    color: red;
    font-size: 16px;
   }
  .email.calender_icon img {
    position: absolute;
    width: 17px;
    top: 8px;
    right: 0;
    cursor: pointer;
}
.email.calender_icon{
   position: relative;
}
.color-info {
  text-align: right;
  margin: 11px 39px 0;
}
.color-info a {
  color: #5b0c33;
      font-size: 17px;
}
.color-info i {
  font-size: 28px;
      margin-left: 8px;
    vertical-align: middle;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
  <div class="color-info">
    <span>
      <a  href="{{url('business/create-gig-desc')}}">What is a gig
    <i data-toggle="tooltip" data-placement="bottom" title="Info" class="fa fa-info-circle" aria-hidden="true"></i> 
   </a>
  </span>
  </div>
   <h2 class="color-text">Create Gig</h2>
   <ul class="tabs">
      <a href="{{url('business/create-gig-filed-2')}}">
         <li class="first">Create</li>
      </a>
      <a href="{{url('business/gig-list')}}">
         <li class="second">On Going</li>
      </a>
      <a href="{{url('business/gig-complete-list')}}">
         <li class="third">Completed</li>
      </a>
   </ul>
   <div class="hashtags create_post_input">
      <form method="POST" id="form_validate" novalidate="novalidate" autocomplete="off">
          {{csrf_field()}}
      
         <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
         <div class="label_addres">
            <label for="">
            Title
            </label>
         </div>
         <div class="form-group">
            <div class="email">
               <input type="text" id="title" name="title" value="{{old('title')}}" class="form-control" placeholder="">
               <span class="text-danger"></span>
            </div>
             <label id="title-error" class="error" for="title">{{$errors->first('title')}}</label>
         </div>
         <div class="label_addres">
            <label for="">
            Description
            </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="description" maxlength="1000" id="description" rows="2" placeholder="">{{old('description')}}</textarea>
                 <!-- <div class="words">Maximum limit 1000 characters</div> -->
               <span class="text-danger"></span>
            </div>
             <label id="description-error" class="error" for="description">{{$errors->first('description')}}</label>
         </div>
         <div class="label_addres" style="margin-top: -5px;">
            <label for="">
            What To Do (These are the things Influencers have to do in 
            a gig)
            </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="what_to_do" maxlength="1000" id="what_to_do" rows="3" placeholder="">{{old('what_to_do')}}</textarea>
                 <!-- <div class="words">Maximum limit 1000 characters</div> -->
               <span class="text-danger"></span>
            </div>
            <label id="what_to_do-error" class="error" for="what_to_do">{{$errors->first('what_to_do')}}</label>
         </div>
         <div class="contact_flex" style="margin-top: -5px;">
            <div class="form_wd">
               <div class="label_addres">
                  <label for="">
                  Contact Person Name
                  </label>
               </div>
               <div class="form-group">
                  <div class="email">
                     <input type="text" id="contact_person_name" name="contact_person_name" value="{{old('contact_person_name')}}" class="form-control" placeholder="">
                     <span class="text-danger"></span>
                  </div>
                   <label id="contact_person_name-error" class="error" for="contact_person_name">{{$errors->first('contact_person_name')}}</label>
               </div>
            </div>
            <div class="form_wd">
               <div class="label_addres">
                  <label for="">
                  Contact Person Number
                  </label>
               </div>
               <div class="form-group">
                  <div class="email">
                     <input type="text" id="contact_person_number" onkeypress="return isNumberKey(event)" name="contact_person_number" value="{{old('contact_person_number')}}" class="form-control" placeholder="">
                     <span class="text-danger"></span>
                  </div>
               </div>
            </div>
         </div>
         <div class="contact_flex">
            <div class="form_wd">
               <div class="label_addres">
                  <label for="">
                  Gig Start Date
                  </label>
               </div>
                    <div class="form-group">
               <div class="email  calender_icon">
                                 <div class="bootstrap-iso">
                                  <input type="text" id="gig_start_date" name="gig_start_date" value = "{{old('gig_start_date')}}" class="form-control datepicker date_icon" placeholder=""  style="cursor: pointer; margin-top: 2px;" autocomplete="off" required>
                                </div>
                                
                                  <span class="text-danger"></span>
                             <!--  <img src="{{url('public/business/images/calendar.png')}}" alt="calender_icon">
                      -->
                           </div>
                            <label id="gig_start_date-error" class="error" for="gig_start_date" style="display: none;">{{$errors->first('gig_start_date')}}</label>
                        </div>
            </div>
            <div class="form_wd">
               <div class="label_addres">
                  <label for="">
                  Gig Start Time
                  </label>
               </div>
               <div class="form-group">
                  <div class="email calendar_icon">
                     <!-- <div class="calendar_icon1"> -->
                     <input type="time" id="email" name="gig_start_time" value="{{old('gig_start_time')}}" class="form-control date_icon" placeholder="" style="margin-top: 2px; cursor: pointer;">
                     <span class="text-danger"></span>
                     <!-- <img src="images/calender_icon.png" alt="calender_icon"> -->
                     <!-- </div> -->
                  </div>
                  <label id="gig_start_time-error" class="error" for="gig_start_time" style="display: none;">{{$errors->first('gig_start_time')}}</label>
               </div>
            </div>
         </div>
         <div class="contact_flex">
            <div class="form_wd">
               <div class="label_addres">
                  <label for="">
                  Gig End Date
                  </label>
               </div>
               <div class="form-group">
               <div class="email  calender_icon">
               <div class="bootstrap-iso"> <input type="text" id="gig_end_date" name="gig_end_date" class="form-control datepicker date_icon" value = "{{old('gig_end_date')}}" placeholder=""  style="margin-top: 2px; cursor: pointer; " autocomplete="off" required></div>
              
                <span class="text-danger"></span>
               <!-- <img src="{{url('public/business/images/calendar.png')}}" alt="calender_icon"> -->
                     </div>
                      <label id="gig_end_date-error" class="error" for="gig_end_date" style="display: none;">{{$errors->first('gig_end_date')}}</label>
                    </div>
                  </div>
            
            <div class="form_wd">
               <div class="label_addres">
                  <label for="">
                  Gig End Time
                  </label>
               </div>
               <div class="form-group">
                  <div class="email calendar_icon">
                   
                     <input type="time" id="gig_end_time" name="gig_end_time" value = "{{old('gig_end_time')}}" class="form-control date_icon" placeholder="" style="margin-top: 2px;">
                     <span class="text-danger"></span>
                    
                  </div>
                   <label id="gig_end_time-error" class="error" for="gig_end_time" style="display: none;">{{$errors->first('gig_end_time')}}</label>
               </div>
            </div>
         </div>
      <div class="space-top top">
        <button type = "submit" name = "submit"  class="button">NEXT</button>
      </div>
   </form>
   </div>
</div>
@endsection
@section('js')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
   function calculateDateTime(){
      let result = 1;
       let startDate = $("#gig_start_date").val();
       let startTime = $("input[name='gig_start_time']").val();
       let endDate = $("#gig_end_date").val();
       let endTime = $("#gig_end_time").val();

       if(startDate != "" && startTime != "" && endDate != "" || endTime != ""){
         let start = new Date(startDate + " " + startTime);
         let end = new Date(endDate + " " + endTime);
         if(start > end){
            result = 0;
            alert("Start time should be less than end time.");
         }
       }
       return result;
   }
  
  $('#form_validate').validate({ 
      rules: {
          title: {
              required: true,
              maxlength:30,
          },
           
          description: {
              required: true,
              maxlength:1000
          },
          what_to_do: {
              required: true,
              maxlength:1000,
            
          },
          contact_person_name: {
              required: true,
              maxlength:50,
             
          },
          contact_person_number: {
              required: true,
              minlength:8,
              maxlength:15,
             
          },
         
           gig_start_date: {
              required: true,
             
          },
           gig_end_date: {
              required: true,
   
          },
           gig_start_time: {
              required: true,
              
          },
          gig_end_time: {
              required: true,
              
          },
      },

        messages:{
          title:{  
            required:'Please enter title.',
            maxlength:'Title maximum 30 characters long.'
          },
          description:{  
            required:'Please enter description.',
            maxlength:'Description maximum 100 characters long.'
          },
          what_to_do:{  
            required:'Please enter what to do.',
            maxlength:'What to do maximum 1000 characters long.'
          },
          contact_person_name:{  
            required:'Please enter contact person name.',
            maxlength:'Contact person name maximum 100 characters long.'
          },

          contact_person_number:{  
            required:'Please enter contact person number.',
            minlength:'Contact person number must be at least 8 digits.',
            maxlength:'Contact person number maximum 15 digits.'
          },


          gig_start_date:{  
            required:'Please select gig start date.',
          },

         gig_end_date:{  
            required:'Please select gig end date.',
          },

         gig_start_time:{  
            required:'Please select gig start time.',
          },
          
         gig_end_time:{  
            required:'Please select gig end time.',
          },
                  
        },

        submitHandler : function(form){
         if(calculateDateTime() == 1){
          localStorage.setItem("post_data",JSON.stringify({
            "title" : $("#title").val(),
            "description" : $("textarea[name='description']").val(),
            "contact_person_name" : $("#contact_person_name").val(),
            "contact_person_number" : $("#contact_person_number").val(),
            "gig_start_date" : $("#gig_start_date").val(),
            "gig_start_time" : $("textarea[name='gig_start_time']").val(),
            "gig_end_date" : $("#gig_end_date").val(),
            "gig_end_time" : $("#gig_end_time").val(),
          }))
            form.submit();
         }
        }
  });
  let isPrevPage = localStorage.getItem("pageIs");
  if(isPrevPage){
      if(isPrevPage.indexOf("create-gig-field") != -1){
          console.log("--------- last ",isPrevPage)
      }
  }
  console.log(localStorage.getItem("post_data"))
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
   $(document).ready(function() {
    var date = new Date();
   var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      let datepicker = $('input[name="gig_start_date"],input[name="gig_end_date"]').datetimepicker({
         format: 'YYYY-MM-DD',
         minDate: today,
       });
   })
</script>
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
  <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
       //-->
    </SCRIPT>
    <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});

/* show hide # in hashtag on focus in and focus out */
   $("#hashtag").on("click paste keypress",function(){
    let val = $(this).val();
    if(val[0] != "#"){
       $(this).val("#"+val)
    }
   })

   $("#hashtag").focusout(function(){
      let val = $(this).val();
      if(val[0] == "#"){
        $(this).val(val.substring(1))
      }
   })

</script>
@endsection