@extends("business/layout/web")
@section("title","Create Post")
@section("content")
<style type="text/css">
   .error {
      color: red;
      font-size: 16px;
   }

   .email.calender_icon {
    background-image: none;
}

.email.calender_icon img {
    position: absolute;
    width: 17px;
    top: 8px;
    right: 0;
    cursor: pointer;
}
.email.calender_icon{
   position: relative;
}
.color-info {
  text-align: right;
  margin: 11px 39px 0;
}
.color-info a {
  color: #5b0c33;
      font-size: 17px;
}
.color-info i {
  font-size: 28px;
      margin-left: 8px;
    vertical-align: middle;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
  <div class="color-info">
    <span>
      <a  href="{{url('business/create-post-desc')}}">What is a post
    <i data-toggle="tooltip" data-placement="bottom" title="Info" class="fa fa-info-circle" aria-hidden="true"></i> 
   </a>
  </span>
  </div>
   <h2 class="color-text">Create Post </h2>
   <ul class="tabs">
      <a href="{{url('business/create-post-field')}}">
         <li class="first">
            Create
         </li>
      </a>
      <a href="{{url('business/post-list')}}">
         <li class="second">On Going</li>
      </a>
      <a href="{{url('business/complete-post-list')}}">
         <li class="third">Completed</li>
      </a>
   </ul>
   <div class="hashtags create_post_input">
      <form method="POST" id="form_validate" novalidate="novalidate" autocomplete="off">
         <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
         <div class="label_addres">
            <label for=""> Title </label>
         </div>
         <div class="form-group">
            <div class="email">
               <input type="text" name="post_name" maxlength="30" class="form-control" placeholder="" required>
               <label id="post_name-error" class="error" for="post_name">{{$errors->first('post_name')}}</label>
            </div>
         </div>
         <div class="label_addres">
            <label for=""> Description </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="description" rows="2" maxlength="1000" placeholder="" required></textarea>
               <label id="description-error" class="error" for="description">{{$errors->first('description')}}</label>
            </div>
         </div>
         <div class="label_addres" style="    margin-top: -5px;">
            <label for=""> Compensation (How much you will pay each influencer you hire) </label>
         </div>
         <div class="form-group">
            <div class="email">
               <input type="text" id="price_per_post" maxlength="6" name="price_per_post" class="form-control" placeholder="" required>
               <label id="price_per_post-error" class="error" for="price_per_post">{{$errors->first('price_per_post')}}</label>
            </div>
         </div>
         <div class="label_addres">
            <label for=""> What To Do (These are the things Influencers have to do in their content) </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="what_to_include" maxlength="1000" rows="2" placeholder="" required></textarea>
               <label id="what_to_include-error" class="error" for="what_to_include">{{$errors->first('what_to_include')}}</label>
            </div>
         </div>
         <div class="label_addres" style="    margin-top: -5px;">
            <label for="">What Not To Do (These are the things Influencers should NOT do in their content)</label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="what_not_to_include" maxlength="1000"  rows="2" placeholder="" required></textarea>
               <label id="what_not_to_include-error" class="error" for="what_not_to_include">{{$errors->first('what_not_to_include')}}</label>
            </div>
         </div>
         <div class="label_addres" style="    margin-top: -5px;">
            <label for="" > Expiration Date</label>
         </div>
         <div class="form-group">
       
            <div class="email calender_icon">
               <div class="bootstrap-iso"> <input type="text" id="expiration_date" name="expiration_date" class="form-control datepicker date_icon"  placeholder=""  style="margin-top: 2px; cursor: pointer;" autocomplete="off" required></div>
               <label id="expiration_date-error" class="error" for="expiration_date">{{$errors->first('expiration_date')}}</label>
              <img src="{{url('public/business/images/calendar.png')}}" alt="calender_icon">
               </div>
         </div>
         <div class="label_addres">
            <label for=""> Other Requirements </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="other_requirement" rows="2" placeholder=""></textarea>
               <span class="text-danger"></span>
            </div>
         </div>
         <div class="space-top top">
            <button type="submit" class="button">NEXT</button>
         </div>
      </form>
   </div>
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
   $(document).ready(function() {
    var date = new Date();
   var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      let datepicker = $('input[name="expiration_date"]').datetimepicker({
         format: 'YYYY-MM-DD',
         minDate: today,
       });
   })
</script>
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
<script type="text/javascript">

   $.validator.addMethod("notOnlyZero", function (value, element, param) {
          return this.optional(element) || parseInt(value) > 0;
   });

   $.validator.addMethod("greaterThan",function (value, element, param) {
          var $otherElement = $(param);
          return parseInt(value, 10) > parseInt($otherElement.val(), 10);
   });

   jQuery.validator.addMethod("noSpace", function(value, element) {
      return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
   }, "Space not allowed");

   $('#form_validate').validate({ 
      rules: {
          post_name: {
              required: true,
              maxlength:30,
              noSpace: true
          },
           
          description: {
              required: true,
              noSpace: true
          },
          what_to_include: {
              required: true,
              noSpace: true
          },
          price_per_post: {
              required: true,
              noSpace: true,
              notOnlyZero: true
          },
          what_not_to_include: {
              required: true,
              // minlength:8,
              noSpace: true,            
          },
         
           expiration_date: {
              required: true,
             
          }
           
      },

        messages:{
          post_name:{  
            required:'Please enter title.',
            maxlength:'Title maximum 30 characters long.'
          },
          description:{  
            required:'Please enter description.',
            maxlength:'Description maximum 100 characters long.'
          },
          what_to_include:{  
            required:'Please enter what to do.',
            maxlength:'What to do maximum 1000 characters long.'
          },
          what_not_to_include:{  
            required:'Please enter what not to do.',
            maxlength:'What not to do maximum 1000 characters long.'
          },

         price_per_post:{  
            required:'Please enter compensation.',
            notOnlyZero: "Compensation should be equal or greater than 1."
         },

         expiration_date:{  
            required:'Please select expiration date.',
          },
              
        },

        submitHandler : function(form){
         if(calculateDateTime() == 1){
            form.submit();
         }
        }
  });
</script>
@endsection