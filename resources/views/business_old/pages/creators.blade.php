@extends("business/layout/web")
@section("title","Creators")
@section("content")
<!-- <link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
 --><link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
<style>
.new-wrappwe img {
    max-width: 100%;
    height: 226px;
    width: 100%;
    object-fit: cover;
}
 img.new-photo {
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border:3px solid #8a0e49;
    object-fit: cover;
}
.pagination>li>a, .pagination>li>span {
    
    padding: 4px 16px;
    margin-left: -1px;
    text-decoration: none;
    font-size: 24px;
    font-size: 26px;
    text-align: center;
    line-height: 42px;
    color: #9ca8ae;
    border:0;
}
.new-button {
    width: 174px;
    height: 53px;
    background-color: #00b3a7;
    display: inline-block;
    font-size: 18px;
    font-weight: 600;
    line-height: 52px;
    color: #fff;
    border-radius: 7px;
    margin-top: 22px;
}
img.new-photo {
    position: absolute;
    bottom: -68px;
    left: 0;
    right: 0;
    margin: 0 auto;
}
.new-wrappwe {
    position: relative;
}
img.origin {
    margin-right: 24px;
    position: relative;
    top: -1px;
}
.creative-photo-img {
    padding: 12px 34px;
}
.text-gallery {
    margin-top: 83px;
    text-align: center;
    padding: 0 28px;
}
.new-button.creative {
    font-weight: 700;
    margin-top: 17px;
}
.text-gallery h2 {
    font-size: 26px;
    color: #27373f;
    font-weight: 700;
    border-bottom: 1px solid #e5ebef;
    padding-bottom: 21px;
}
img.new-photo {
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border: 3px solid #8a0e49;
    object-fit: cover;
}
.gallery-new {
    box-shadow: 0px 2px 30px 0px rgba(0, 45, 83, 0.1);
    border-radius: 12px;
    padding-bottom: 26px;
    margin-bottom: 34px;
    min-height: 500px;
}
.sub-heading p {
    background: -webkit-linear-gradient(#780c4f, #ad0e5a);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    margin: 0;
    font-size: 50px;
    line-height: 59px;
}
.creative-photo-img select.form-control {
    color: #84939b;
    font-size: 18px;
    /* padding: 12px 0; */
    height: 46px;
    font-weight: 400;
    border: 1px solid #ccd5d8;
    -webkit-appearance: none;
    background-image: url(../images/drop-down-icon.png);
    background-repeat: no-repeat;
    background-position: 94% center;
        margin-bottom: 24px;

}
.right_side a.new-buttom {
    background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%) !important;
    margin-top: 8px;
    margin-bottom: 5px;
}
.new-buttom {
    font-size: 18px;
    color: #fff;
    padding: 19px 52px;
    display: inline-block;
    margin-top: 14px;
    text-transform: uppercase;
}
.new-creative h2 {
    font-size: 77px;
    color: #27373f;
    position: relative;
}
.new-creative h2 span {
    color: #8a0e49;
    font-family: 'Damion', cursive;
    font-size: 115px;
}
.right_side a.new-buttom {
    background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%) !important;
    margin-top: 8px;
    margin-bottom: 5px;
}
.right_side h5 {
    color: #000;
}
.creative-photo-img .sub-heading {
    margin-bottom: 10px;
    }

.new-creative { 
       padding: 133px 0 7px 0px;

}
.pagination>li:first-child>a, .pagination>li:first-child>span {
    margin-left: 0;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    font-size: 68px!important;
    padding: 0px 23px;


}
.pagination>li:last-child>a, .pagination>li:last-child>span {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    font-size: 68px!important;
    padding: 0px 23px;

}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 2;
    color: #ccc;
    background-color: transparent;
    border-color: transparent;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #8a0e49;
    border-color: #8a0e49;
}

h3.text {
    margin-top: 20px;
    margin-bottom: 10px;
    font-size: 24px;
    color: #000;
}
.right_side h5 {
      color: #000;
    }
    .right_side a.new-buttom {
          background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%) !important;
              margin-top: 8px;
    margin-bottom: 5px;
    }
</style>
<body>
  <div class="right_col" role="main" style="min-height: 451px;">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">

 <!--  <div class="new-creative">
    <div class="container">
      <div class="col-md-7">

      <h2>Tell your brand <span>Story</span> better!       <img src="{{url('public/main-page/images/dots-small.png')}}" class="dots" alt="">
</h2>
<div class="row">
     
      </div>
    </div>
    <div class="col-md-5">
      <figure><img src="{{url('public/main-page/images/phone.png')}}" alt=""></figure>
  </div>
  
</div>
</div> -->
   <div class="creative-photo-img">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
      <div class="sub-heading">
      <p>Creators</p>
  </div>
</div>
</div>
  <div class="row">
    <div class="container">
      <div class="col-md-3">
             <select name = "category_name" id = "category_name" class="form-control" required>
            
               <option value=""> Select Category</option>
                <option value="{{url('business/creaters')}}"> All</option>
              @foreach($category as $value)
              <option value="{{url('business/creaters').'?category_id='.$value->id}}" @if(app("request")->input('category_id') == $value->id) selected @endif >{{$value->name}}</option>
              @endforeach
            </select>
          </div>
      </div>
    
  </div>
  <script type="text/javascript">
    document.getElementById("category_name").addEventListener("change",function(){
      window.location.href = this.value;
    })
  </script>


   </div>
 <div class="new-photo-gallerys">
  <div class="container">
   
  <div class="row">
    
     @forelse($users as $user)
  <div class="col-md-4 col-sm-6">
     <a href="{{url('business/portfolio/'.$user->user_id)}}">
    <div class="gallery-new">
      <div class="new-wrappwe">
        <?php 
         $bg_image = $user->background_image ?: url('public/main-page/images/brown-framed.jpg');  ?>
      <figure>

        
        <img src="{{url($bg_image)}}" alt=""></figure>
      @if(!empty($user->user_profile))
     @php $image =   url('public/storage/uploads/images/business/'.$user->user_profile);@endphp
                            <td><img src="{{$image}}" alt="" class="new-photo"/>
                            @else
                           <img src="{{url('public/main-page/images/creative-photo.png')}}" alt="" class="new-photo">
                           @endif</td> 
      
    </div>
    <div class="text-gallery">
      <h3 class="text">@if(!empty($user->user_name)){{$user->user_name}} @else N/A @endif</h3>
      <h2><img src="{{url('public/main-page/images/camera-creative.png')}}" alt="" class="origin">@if(!empty($user->category_name)){{$user->category_name}} @else N/A @endif</h2>
  </div>
  <div class="text-center">
  <a href="{{url('business/portfolio/'.$user->user_id)}}" class="new-button creative">PORTFOLIO</a>
</div>
</div>
</a>
 </div>
 @empty
<div class="text-center">
 <h2 style = "font-size: 30px;">No Data Found</h2>
</div>
@endforelse
<div class = "col-md-12">
<div class="text-center">
{{ $users->appends(request()->except('page'))->links() }}
</div>
</div>
</div>

<div class="pagination">
</div>
</div>

  </div>
</div>





</main>

@endsection
@section('js')

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
    document.getElementById("category_name").addEventListener("change",function(){
      window.location.href = this.value;
    })
  </script>
<!-- <script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script> -->
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>


<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

    
   </script>
@endsection



  