@extends("business/layout/web")
@section("title","Declined Gig Influencer Details")
@section("content")
<style>

img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}
</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Declined Gig Influencer Details</h2>
                      <div class="p_allside">
                        <div class="tabs_list pending_button" style="margin-top: 12px;">
                          <div class="filter_btns button-size">
                            <a href = "{{url('business/accept-gig',base64_encode($id))}}" class="btn btn-primary green_btn">Accepted</a>
                             <a href="{{url('business/pending-gig-list',base64_encode($id))}}"
                             class="btn btn-primary  green_btn">Pending</a>
                             <a href="javascript:void(0)"
                           class="btn btn-primary mehron_btn">Declined</a>
                           
                          </div>
                        </div>
                        <section class="uploaded-data common-struct login_FORM login-input single_list">
                          <!-- <div class="white_bg_sec"> 
                            <div class="Update-img text-center">
                              <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Influencer Name: </span>John Lewis
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Offered Price: </span>$120
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Status:  </span><a href="#" style="color: #f23d2f;">Rejected</a>
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Rejection Reason:  </span>Lorem iplusm ilopor vector
                            </div>
                            <div class="form-group Submit medium_btn m_top">
                              <a href="{{url('business/re-submit-gig')}}">
                                <input type="button" name="Login" value="RESUBMIT" class="btn btn-primary">
                                
                              </a>
                            </div>
                          </div>-->
						  @if(!empty($declinedGigDetails) && $declinedGigDetails && count($declinedGigDetails) > 0)
                            <div class="white_bg_sec"> 

                            @foreach($declinedGigDetails as $gig)
                            <a href="{{url('business/influencer-details',base64_encode($gig->user->id))}}">
                            <div class="Update-img text-center">
                              <?php 
                                    $userImage = url('public/business/images/upload-one.png');
                                  ?>
                                  @forelse($gig->user->images as $images)
                                    @if($images->image && !empty($images->image))
                                      <?php 
                                        $userImage = $images->image;
                                        break;
                                      ?>
                                    @endif
                                  @empty
                                  @endforelse
                                    
                                     
                                      <img src="{{$userImage}}" alt="upload-one" class="img-responsive round" style="max-height: 160px;">
                                  
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Influencer Name: </span>{{$gig->user->name ?? 'N/A'}}
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Offered Price: </span>{{$gig->price ? 'KSh'.round(($gig->price / 100) *75) :  KSh0}}
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Status:  </span><a href="#" style="color: #f23d2f;">Rejected</a>
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Rejection Reason:  </span>{{$gig->reason ?? 'N/A'}}
                            </div>
                          </a>
                          @if($gig->reason == "Low Pay")
                            <div class="date_content">
                              <span class="equal_width w2">Expected Price:  </span>KSh{{$gig->reject_amount ?? '0'}}
                            </div>

                          <div class="form-group Submit medium_btn m_top">
                              <a href="{{url('business/re-submit-gig',base64_encode($gig->gig_id))}}">
                                <input type="button" name="Login" value="REINVITE" class="btn btn-primary">
                              </a>
                            </div>
                          @endif
                            
                          @endforeach
                          {!! $declinedGigDetails->links() !!}
                            </div>
                         @else
                         <div class="data_found text-center">
                                 <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                                 <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
                              </div>
                         @endif
                        </section>
                      </div>
                </div>
             
              </div>
            </div>
         @endsection
         @section('js')
      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
   @endsection