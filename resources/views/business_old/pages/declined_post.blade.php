@extends("business/layout/web")
@section("title","Declined Post Influencer Details")
@section("content")
<style>
.pagination {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
    float: right;
    color: red;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #410a2d;
    border-color: #410a2d;
}
.post_list .white_bg_sec{
  min-height: 328px;

}
section.footer {
    margin-top: 69px;
}

img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}

.form-group.Submit.medium_btn.m_top .btn-primary {
    color: #fff;
    background-color: #00b3a7;
    border-color: #00b3a7;
    height: 50px;
    font-size: 20px;
    font-weight: 500;
    width: 100%;
    text-transform: uppercase;
}
.footer {
  height: 100px !important;
}
</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">

                      <h2 class="color-text">Declined Post Influencer Details</h2>
                      <div class="p_allside">
                         <div class="tabs_list pending_button" style="margin-top: 12px;">
                          <div class="filter_btns button-size">
                            <a href = "{{url('business/accept-post',base64_encode($id))}}" class="btn btn-primary green_btn">Accepted</a>
                             <a href="{{url('business/pending-post-list',base64_encode($id))}}"
                             class="btn btn-primary  green_btn ">Pending</a>
                             <a href="javascript:void(0)"
                           class="btn btn-primary mehron_btn ">Declined</a>
                           
                          </div>
                        </div>
                        <section class="common-struct login-input heading_gap">
                          <div class="post_list">
                            <div class="row">
                                  @if(!empty($declinedPostDetails) && $declinedPostDetails && count($declinedPostDetails) > 0)
                                <div class="col-md-4">
                                  <div class="white_bg_sec min_height_box"> 

                                  @foreach($declinedPostDetails as $post)
                                    <a href="{{url('business/influencer-details',base64_encode($post->user->id))}}">
                                  <div class="Update-img text-center">
                                    @if($post->user && $post->user->profile)
                                       <img src="{{$post->user->profile}}" alt="upload-one" class="img-responsive round">
                                    @else 
                                       <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive round">
                                    @endif
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width w2">Influencer Name: </span>{{$post->user->name ?? 'N/A'}}
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width w2">Offered Price: </span>KSh{{$post->price ?? '0'}}
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width w2">Status:  </span><a href="#" style="color: #f23d2f;">Rejected</a>
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width w2">Rejection Reason:  </span>{{$post->reason ?? 'N/A'}}
                                  </div>
                                </a>
                                  @if($post->reason == "Low Pay")
                                    <div class="date_content">
                                      <span class="equal_width w2">Expected Price:  </span>KSh{{$post->reject_amount ?? '0'}}
                                    </div>

                                  <div class="form-group Submit medium_btn m_top">
                                    <a href="{{url('business/re-submit-post',base64_encode($post->post_id))}}">
                                      <input type="button" name="Login" value="REINVITE" class="btn btn-primary">
                                    </a>
                                  </div>
                                  @endif
                                @endforeach
                                {!! $declinedPostDetails->links() !!}
                                  </div>


                               @else
                               <div class="data_found text-center">
                                 <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                                 <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
                              </div>
                               @endif
                                </div>
                            </div>
                              
                          </div>
                            
                        </section>
                      </div>
                </div>
           
              </div>
            </div>
            </div>
            </div>
          
@endsection
@section('js')

      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
      
   @endsection