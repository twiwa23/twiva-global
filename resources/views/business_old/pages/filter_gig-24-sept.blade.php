@extends("business/layout/web")
@section("title","Filter")
@section("content")

        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Filter</h2>
                        <section class="uploaded-data common-struct login_FORM login-input">
                          <div class="white_bg_sec common_white_bg filter_spacing">
                            <div class="labels">
                              <h5>Facebook Followers Between:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">3M</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="100" data-slider-step="1" data-slider-value="14"/>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Instagram Followers Between:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">3M</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="100" data-slider-step="1" data-slider-value="14"/>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Twitter Followers Between:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">3M</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="100" data-slider-step="1" data-slider-value="14"/>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Desired Price:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">50K</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="100" data-slider-step="1" data-slider-value="14"/>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Gender:</h5>
                              <div class="radio_flex">
                                <label class="radio_btn"> Male
                                  <input type="radio" checked="checked" name="radio">
                                  <span class="checkmark"></span>
                                </label>
                                <label class="radio_btn"> Female
                                  <input type="radio"  name="radio">
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                            </div>
                            <div class="labels">
                              <h5 style="margin-bottom: 8px;">Interests</h5>
                              <div class="theme_photos">
                                <ul>
                                  <li>
                                    <!-- <a href=""> -->
                                      <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                    <!-- </a> -->
                                    <p class="text-center name">
                                      Interest<br>
                                      Name
                                    </p>
                                  </li>
                                  <li>
                                    <!-- <a href=""> -->
                                      <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                    <!-- </a> -->
                                    <p class="text-center name">
                                      Interest<br>
                                      Name
                                    </p>
                                  </li>
                                  <li>
                                    <!-- <a href=""> -->
                                      <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                    <!-- </a> -->
                                    <p class="text-center name">
                                      Interest<br>
                                      Name
                                    </p>
                                  </li>
                                  <li>
                                    <!-- <a href=""> -->
                                      <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                    <!-- </a> -->
                                    <p class="text-center name">
                                      Interest<br>
                                      Name
                                    </p>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="button_right_lrft">
                              <div class="form-group Submit medium_btn m_top" style="text-align: left !important;">
                                <a href="{{url('business/invite-influencers-gig')}}">
                                  <input type="button" name="Login" value="FILTER" class="btn btn-primary" >
                                </a>
                              </div>
                              <div class="form-group Submit medium_btn m_top" style="text-align: right !important;">
                                <a href="{{url('business/invite-influencers-gig')}}">
                                  <input type="button" name="Login" value="RESET" class="btn btn-primary">
                                  
                                </a>
                              </div>
                            </div>
                          </div>
                        </section>
                        
                </div>
      
              </div>
            </div>
         @endsection
           @section('js')
           <script src="{{url('public/business/js/jquery1.min.js')}}"></script>
      <script src="{{url('public/business/js/bootstrap-slider.min.js')}}"></script>   
      <script type="text/javascript">
      (function( $ ){
$( document ).ready( function() {
  $( '.input-range').each(function(){
    var value = $(this).attr('data-slider-value');
    var separator = value.indexOf(',');
    if( separator !== -1 ){
      value = value.split(',');
      value.forEach(function(item, i, arr) {
        arr[ i ] = parseFloat( item );
      });
    } else {
      value = parseFloat( value );
    }
    $( this ).slider({
      formatter: function(value) {
        console.log(value);
        return ' ' + value;
      },
      min: parseFloat( $( this ).attr('data-slider-min') ),
      max: parseFloat( $( this ).attr('data-slider-max') ), 
      range: $( this ).attr('data-slider-range'),
      value: value,
      tooltip_split: $( this ).attr('data-slider-tooltip_split'),
      tooltip: $( this ).attr('data-slider-tooltip')
    });
  });
  
 } );
} )( jQuery );
      </script>
     
  @endsection