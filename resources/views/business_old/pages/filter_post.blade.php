@extends("business/layout/web")
@section("title","Filter")
@section("content")
<style type="text/css">
  .theme_photos ul li {
    margin-bottom: 15px !important;
    text-align: center;
  }

  .interest-img {
    max-width: 90px !important;
    width: 100% !important;
    max-height: 90px !important;
    height: 100% !important;
    margin:0 !important;
    margin:auto !important;
  }
  .interest-container {
    padding: 5px;
    cursor: pointer;
    border-radius: 5px;
    border: 1px solid lightgray;
    user-select: none;
  }
  .interest-container.active {
    border:2px solid rgb(115,14,57);
  }
  .theme_photos p.name {
    width: auto;
  }
  .theme_photos ul li img {
    width: 90px!important;
    height: 90px!important;
  }
.interest-container{
      min-height: 160px;

}
</style>

        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Filter</h2>
                        <section class="uploaded-data common-struct login_FORM login-input">
                          <div class="white_bg_sec common_white_bg filter_spacing">
                            <div class="labels">
                              <h5>Facebook Followers Between:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">1M</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <!-- <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="3000000" data-slider-step="1" data-slider-value="0" id="fb_selection" /> -->
                                  <input type="hidden" id="facebook-range-original" />
                                  <input type="text" id="facebook-range" readonly />
                                  <div id="facebook-slider"></div>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Instagram Followers Between:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">1M</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <!-- <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="3000000" data-slider-step="1" data-slider-value="0" id="instagram_selection"/> -->
                                  <input type="hidden" id="instagram-range-original" />
                                  <input type="text" id="instagram-range" readonly />
                                  <div id="instagram-slider"></div>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Twitter Followers Between:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number">1M</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <!-- <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="3000000" data-slider-step="1" data-slider-value="0" id="twitter_selection" /> -->
                                   <input type="hidden" id="twitter-range-original" />
                                  <input type="text" id="twitter-range" readonly />
                                  <div id="twitter-slider"></div>

                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Desired Price:</h5>
                              <div class="slider_row">
                                <div class="follwers_number">
                                  <div class="number">0</div>
                                  <div class="number" style="right: -40px;">50K</div>
                                </div>
                                <div class="slider-wrapper green">
                                  <!-- <input class="input-range" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-tooltip="always" data-slider-max="50000" data-slider-step="1" data-slider-value="0" id="price_selection" /> -->
                                   <input type="hidden" id="price-range-original" />
                                  <input type="text" id="price-range" readonly />
                                  <div id="price-slider"></div>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Gender:</h5>
                              <div class="radio_flex">
                                <label class="radio_btn"> Male
                                  <input type="checkbox" name="gender_male" class="gender_selection" value="male" />
                                  <span class="checkmark"></span>
                                </label>
                                <label class="radio_btn"> Female
                                  <input type="checkbox" name="gender_female" class="gender_selection" value="female" />
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                            </div>
                            <div class="labels">
                              <h5 style="margin-bottom: 8px;">Interests</h5>
                              <div class="theme_photos">
                                <ul>
                                  <?php $i = 0;?>
                                  @forelse($data as $interests)
                                  <?php $i++;

                                    $padding = 0;
                                    $isFourth = $i % 4;
                                    if($isFourth == 0){
                                      $padding = "0 0 0 7.5px";
                                    }else if(in_array($i,[1,5,9,13,17,21,25,29,33,37,41])){
                                      $padding = "0 7.5px 0 0";
                                    }else {
                                      $padding = " 0 7.5px 0 7.5px";
                                    }
                                   ?>
                                  <li class="col-xs-6 col-md-3" style="padding:{{$padding}}">
                                    <div class="interest-container" data-int-id = "{{$interests->id}}">
                                      <img src="{{$interests->image ? $interests->image :  url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive interest-img" >
                                    <p class="text-center name">{{$interests->interest_name}}</p>
                                    </div>
                                  </li>
                                  @empty
                                @endforelse
                                </ul>
                              </div>
                            </div>
                            <div class="button_right_lrft">
                              <input type="hidden" id="selected_interest" />
                              <div class="form-group Submit medium_btn m_top" style="text-align: left !important;">
                                <input type="submit" name="submit" class="btn btn-primary submit-filter" value="FILTER" id="filter" />
                              </div>
                              <div class="form-group Submit medium_btn m_top" style="text-align: right !important;">
                                <input type="submit" name="reset" class="btn btn-primary submit-filter" value="RESET"  id="reset" />
                              </div>
                            </div>
                          </div>
                        </section>
                        
                </div>
      
              </div>
            </div>
         @endsection
           @section('js')
           <!-- <script src="{{url('public/business/js/jquery1.min.js')}}"></script>
      <script src="{{url('public/business/js/bootstrap-slider.min.js')}}"></script>    -->

      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      <script type="text/javascript">
//       (function( $ ){
// $( document ).ready( function() {
//   $( '.input-range').each(function(){
//     var value = $(this).attr('data-slider-value');
//     var separator = value.indexOf(',');
//     if( separator !== -1 ){
//       value = value.split(',');
//       value.forEach(function(item, i, arr) {
//         arr[ i ] = parseFloat( item );
//       });
//     } else {
//       value = parseFloat( value );
//     }
//     $( this ).slider({
//       formatter: function(value) {
//         return ' ' + value;
//       },
//       min: parseFloat( $( this ).attr('data-slider-min') ),
//       max: parseFloat( $( this ).attr('data-slider-max') ), 
//       range: $( this ).attr('data-slider-range'),
//       value: value,
//       tooltip_split: $( this ).attr('data-slider-tooltip_split'),
//       tooltip: $( this ).attr('data-slider-tooltip')
//     });
//   });
  
//  } );
// } )( jQuery );

// /* set existing filter data*/
// let filterData = localStorage.getItem("user_filter");
// if(filterData && (filterData != null || filterData != "")){
//     filterData = JSON.parse(filterData);
//     $("#fb_selection").attr("data-slider-value",filterData.facebook);
//     $("#instagram_selection").attr("data-slider-value",filterData.instagram);
//     $("#twitter_selection").attr("data-slider-value",filterData.twitter);
//     $("#price_selection").attr("data-slider-value",filterData.price);

//     $("input[name='gender_female']").attr("checked",filterData.gender.female == 1 ?  true : false)
//     $("input[name='gender_male']").attr("checked",filterData.gender.male == 1 ?  true : false)

//     let ints = filterData.interests.split(",").map(e => parseInt(e));
//     $(".theme_photos").find(".interest-container").each(function(){
//       if(ints.indexOf($(this).data("int-id")) != -1){
//         $(this).addClass("active")
//       }
//     })
//     $("#selected_interest").val(filterData.interests)
// }

// /*get filter inputs */
// $(".submit-filter").click(function(){
//   let type = $(this).attr("name");
//   let facebook = $("#fb_selection").val();
//   let instagram = $("#instagram_selection").val();
//   let twitter = $("#twitter_selection").val();
//   let price = $("#price_selection").val();
//   let interests = $("#selected_interest").val();

//   let genderMale = $("input[name='gender_male']").is(":checked") ? 1 : 0;
//   let genderFemale = $("input[name='gender_female']").is(":checked") ? 1 : 0;


//   let filter = {
//     type : type,
//     facebook : facebook,
//     instagram : instagram,
//     twitter : twitter,
//     price : price,
//     gender : {"male" : genderMale,"female" : genderFemale},
//     interests : interests    
//   };

//   localStorage.setItem("user_filter",JSON.stringify(filter))
//   if(type == "reset"){
//     localStorage.removeItem("user_filter");
//   }
//   window.location.href = "{{url('business/invite-influencers-post').'/'.Request::segment(3)}}"
// })

// /* make active inactive interests */

// $(document).on("click",".interest-container",function(){
//     let interests = $("#selected_interest").val().split(",").filter(e => e);
//     let id = $(this).data("int-id");
//     let index = interests.indexOf(id);
//     if($(this).hasClass("active")){
//       $(this).removeClass("active")
//       interests.splice(index,1)
//     }else {
//       interests.push(id)
//       $(this).addClass("active")
//     }
//     $("#selected_interest").val(interests.join(","))
// })

/* set existing filter data*/
let filterData = localStorage.getItem("user_filter");
var init = {
  fb : {
    min : 0,
    max : 0
  },
  insta : {
    min : 0,
    max : 0
  },
  twitter : {
    min : 0,
    max : 0
  },
  price : {
    min : 0,
    max : 0
  }
}

function _parseInt(val){
  return val != null && val != "" && val != undefined ? parseInt(val) : 0;
}

if(filterData && (filterData != null || filterData != "")){
    filterData = JSON.parse(filterData);

    let fb = [0,0]
    if(filterData.facebook != null && filterData.facebook != undefined && filterData.facebook != ""){
      fb = filterData.facebook.split("-");
    }

    let insta = [0,0]
    if(filterData.instagram != null && filterData.instagram != undefined && filterData.instagram != ""){
      insta = filterData.instagram.split("-");
    }

    let twitter = [0,0]
    if(filterData.twitter != null && filterData.twitter != undefined && filterData.twitter != ""){
      twitter = filterData.twitter.split("-");
    }
    
    let _price = [0,0]
    if(filterData.price != null && filterData.price != undefined && filterData.price != ""){
      _price = filterData.price.split("-");
    }

    init = {
      fb : {
        min : _parseInt(fb[0]),
        max : _parseInt(fb[1])
      },
      insta : {
        min : _parseInt(insta[0]),
        max : _parseInt(insta[1])
      },
      twitter : {
        min : _parseInt(twitter[0]),
        max : _parseInt(twitter[1])
      },
      price : {
        min : _parseInt(_price[0]),
        max : _parseInt(_price[1])
      }
    }

    $("#facebook-range").val(parseK(init.fb.min) + "-" + parseK(init.fb.max));
    $("#instagram-range").val(parseK(init.insta.min) + "-" + parseK(init.insta.max));
    $("#twitter-range").val(parseK(init.twitter.min) + "-" + parseK(init.twitter.max));
    $("#price-range").val(parseK(init.price.min) + "-" + parseK(init.price.max));

    $("#facebook-range-original").val(init.fb.min + "-" + init.fb.max);
    $("#instagram-range-original").val(init.insta.min + "-" + init.insta.max);
    $("#twitter-range-original").val(init.twitter.min + "-" + init.twitter.max);
    $("#price-range-original").val(init.price.min + "-" + init.price.max);


    if(filterData.gender != null && filterData.gender != undefined){
       if(filterData.gender.female != "" && filterData.gender.female != null && filterData.gender.female != undefined){
        $("input[name='gender_female']").attr("checked",filterData.gender.female == 1 ?  true : false)
      }

      if(filterData.gender.male != "" && filterData.gender.male != null && filterData.gender.male != undefined){
        $("input[name='gender_male']").attr("checked",filterData.gender.male == 1 ?  true : false)
      }
    }

    if(filterData.interests != null && filterData.interests != undefined && filterData.interests != ""){
      let ints = filterData.interests.split(",").map(e => parseInt(e));

      $(".theme_photos").find(".interest-container").each(function(){
        if(ints.indexOf($(this).data("int-id")) != -1){
          $(this).addClass("active")
        }
      })
      $("#selected_interest").val(filterData.interests)
  }
}

/*get filter inputs */
$(".submit-filter").click(function(){
  let type = $(this).attr("name");
  let facebook = $("#facebook-range-original").val();
  let instagram = $("#instagram-range-original").val();
  let twitter = $("#twitter-range-original").val();
  let price = $("#price-range-original").val();
  let interests = $("#selected_interest").val();

  let genderMale = $("input[name='gender_male']").is(":checked") ? 1 : 0;
  let genderFemale = $("input[name='gender_female']").is(":checked") ? 1 : 0;

  let filter = {
    type : type,
    facebook : facebook,
    instagram : instagram,
    twitter : twitter,
    price : price,
    gender : {"male" : genderMale,"female" : genderFemale},
    interests : interests    
  };
  localStorage.setItem("user_filter",JSON.stringify(filter))

  if(type == "reset"){
    localStorage.removeItem("user_filter");
  }
  window.location.href = "{{url('business/invite-influencers-post').'/'.Request::segment(3)}}"
})

/* make active inactive interests */

$(document).on("click",".interest-container",function(){
    let interests = $("#selected_interest").val().split(",").filter(e => e);
    let id = $(this).data("int-id");
    let index = interests.indexOf(id);
    if($(this).hasClass("active")){
      $(this).removeClass("active")
      interests.splice(index,1)
    }else {
      interests.push(id)
      $(this).addClass("active")
    }
    $("#selected_interest").val(interests.join(","))
})

function parseK(input){
  input = parseInt(input)
  if(input >= 1000000){
    return parseFloat((input / 1000000).toFixed(2)) + "M";
  }else if(input >= 1000){
    return parseFloat((input / 1000).toFixed(2)) + "K";
  }else {
    return input;
  }
}

function getValue(selector,parse = true){
      let start = $(selector).slider("values",0);
      let end = $(selector).slider("values",1);
      if(start > 0 && end > 0){
        if(parse){
           return parseK(start) + "-" + parseK(end); 
        }
          return start + "-" + end; 
      }
    }


  $(function(){
    $("#facebook-slider").slider({
      range: true,
      min: 0,
      max: 1000000,
      values: [ init.fb.min, init.fb.max ],
      slide: function(event,ui){
        $("#facebook-range-original").val(ui.values[0]+ "-" + ui.values[1]);
        $("#facebook-range").val(parseK(ui.values[0]) + "-" + parseK(ui.values[1]));
      }
    });

    $("#instagram-slider").slider({
      range: true,
      min: 0,
      max: 1000000,
      values: [ init.insta.min, init.insta.max ],
      slide: function(event,ui){
        $("#instagram-range-original").val(ui.values[0] + "-" + ui.values[1]);
        $("#instagram-range").val(parseK(ui.values[0]) + "-" + parseK(ui.values[1]));
      }
    });

    $("#twitter-slider").slider({
      range: true,
      min: 0,
      max: 1000000,
      values: [ init.twitter.min, init.twitter.max],
      slide: function(event,ui){
        $("#twitter-range-original").val(ui.values[0] + "-" + ui.values[1]);
        $("#twitter-range").val(parseK(ui.values[0]) + "-" + parseK(ui.values[1]));
      }
    });

    $("#price-slider").slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ init.price.min, init.price.max ],
      slide: function(event,ui){
        $("#price-range-original").val(ui.values[0] + "-" + ui.values[1]);
        $("#price-range").val(parseK(ui.values[0]) + "-" + parseK(ui.values[1]));
      }
    });

    // $("#facebook-range").val(getValue("#facebook-slider"));
    // $("#instagram-range").val(getValue("#instagram-slider"));
    // $("#twitter-range").val(getValue("#twitter-slider"));
    // $("#price-range").val(getValue("#price-slider"));

    // $("#facebook-range-original").val(getValue("#facebook-slider",false));
    // $("#instagram-range-original").val(getValue("#instagram-slider",false));
    // $("#twitter-range-original").val(getValue("#twitter-slider",false));
    // $("#price-range-original").val(getValue("#price-slider",false));
  });
      </script>
     
  @endsection