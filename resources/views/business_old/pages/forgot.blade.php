<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://twiva.co.ke/public/landing/images/fav.png" type="image/x-icon">
    <title>Forgot Password</title>
    <link rel="icon" href="images/fav.png" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/daterangepicker.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
   <!--  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> -->
  </head>
  <body>
        <div class="main_container main_bg">

<div class="top_nav">
          <div class="nav_menu header_contain">
            <nav>
              <div class="institute_logo">
                <a href="login.html">
                  <img src="images/logo.png">
                </a>
              </div>
              <!-- <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div> -->
              <!-- <ul class="nav navbar-nav navbar-right">
                <li class="second">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt="">
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu ">
                    <li><a href="edit-profile.html">Edit Profile</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> LogOut</a></li>
                  </ul>
                </li>
              </ul> -->
            </nav>
          </div>
        </div>
<section class="login_FORM login-input contain_banner">
    <div class="container">
      <div class="col-sm-12">
        <h2 class="forgot_icon"> 
          <a href="login.html">
            <img src="images/back_icon.png" alt="back_icon">
            
          </a>
          Forgot Password
        </h2>
        <!-- <form method = "post" action = "" enctype=multipart/form-data>
                        <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx"> -->
                    <form method="POST" id="form_validate" novalidate="novalidate"> 
        <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
          <div class="label_addres">
              <label for="">
                Email Address
              </label>
            </div>
          <div class="form-group">
            <div class="email">
                <i class="fa">
              <img src="./images/inbox.png">
            </i>
          <input type="text" id="email" name="email" value="" class="form-control" placeholder="">
           
             <span class="text-danger"></span>
           </div>
<!--            <label id="email-error" class="error" for="email"></label>
 -->          </div>
              <!-- <div class="form-group">
            <div class="password">
              <i class="fa lock_icon">
              <img src="./images/lock1.png">
            </i>
          <input type="password" name="password" id="password" vlaue="" class="form-control" placeholder="Password" maxlength="20"> 
            
            <span class="text-danger"></span>
          </div> -->
          <label id="password-error" class="error" for="password"></label>
  <div class="form-group Submit">
    <a href="login.html">
            <input type="button" name="Login" value="SEND" class="btn btn-primary" style="margin-top: 28px; margin-bottom: 160px;">
      
    </a>
          </div>
        </div>
         <div class="form-group trouble_text">
           <!-- <div class="">
             <p class="" style="font-size: 15px;font-weight:400px; float: right; margin-top: -14px;"><a href="https://twiva.co.ke/user/forgot-password">Forgot Password?</a></p>
           </div> -->
          </div>

          
         
          <!-- <div class="form-group signup_a">
            <p class="">Don’t have an account? <a href="https://twiva.co.ke/user/signup"> Signup</a></p>
          </div> -->

        </form>
      </div>
    </div>
  </section>
  <section class="footer mb_footer">
         <div class="container footer_contain">
         <div class="col-md-6 col-xs-4">
            <div class="new-file">
              <a href="login.html">
                 <figure>
                    <img src="https://twiva.co.ke/public/landing/images/logo-footer.png" alt="">
                 </figure>
                
              </a>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="https://twiva.co.ke/contact-us">Contact Us</a></li>
                  <li><a href="https://twiva.co.ke/policy">Privacy Policy</a></li>
                  <li><a href="https://twiva.co.ke/terms-conditions">Terms and Conditions</a></li>
                  <li><a href="https://twiva.co.ke/faq">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                   
                     <!-- <p>info@twiva.com</p> -->
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
               <br>
                  <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>
            </div>
             <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Facebook"><a href="https://www.facebook.com/twiva/" target="_blank"><img src="images/fb_icon.png" alt="fb_icon"/></a></li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Twitter"><a href="https://twitter.com/Twiva_influence" target="_blank"><img src="images/twitter_icon.png" alt="twitter_icon"/></a></li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Instagram"><a href="https://www.instagram.com/twiva_influence/" target="_blank"><img src="images/insta_icon.png" alt="insta_icon"/></a></li>
                  </ul>
               </div>
         </div>
      </div></section>
</div>
<script src="js/jquery.min.js"></script>
      <!-- Bootstrap -->
      <script src="js/bootstrap.min.js"></script>
      <script src="bootstrap-daterangepicker/daterangepicker.js"></script>
      <script src="js/custom.min.js"></script>
      <script src="js/custom.js"></script>
    </body>
      </html>