@extends("business/layout/web")
@section("title","Gigs Complete List")
@section("content")
<style>
      img.round {
    position: relative;
    border: 4px solid #fff;
    /*border-radius: 50%;*/
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}
.common-struct .Update-img img {
     position: relative;
     border: 4px solid #fff;
     /*border-radius: 50%;*/
     box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
     width: 120px;
     height: 120px;
     margin: auto;
     cursor: pointer;
   }
.common-struct .Update-img img{
        border-radius: unset;

}
      video.round {

    position: relative;
    border: 4px solid #fff;
    /*border-radius: 50%;*/
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
    object-fit: cover;
}
.white_bg_sec{
  min-height:300px;
}
  .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
  background: #640C35; 
}

.tab-active {
  color: #fff !important;
    border-radius: 0px;
    background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%) !important;
}
.tabs li {
  background-color: #f1f1f1 !important;
    border-right: 3px solid #fff;
    color: #000;
    background-image: none;
}
.tabs .second {
  background-color: #f1f1f1 !important;
  color: black !important;
  background-image: none !important;
}

  </style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Gigs List</h2>
                      <div class="p_allside">
                        <div class="tabs_list">
                          <div class="tabs_bar">
                            <ul class="tabs">
                             
                              <a href="{{ url('business/create-gig-filed-2') }}">
                                <li class="first">Create</li>
                              </a>
                              <a href="{{url('business/gig-list')}}">
                                <li class="second">On Going</li>
                              </a>
                              <a href="{{url('business/gig-complete-list')}}">
                                <li class="third tab-active" >Completed</li>
                              </a>
                            </ul>
                          </div>
                   <!--        <div class="search_input">
                            <div>
                              <img src="{{url('public/business/images/search_icon.png')}}" alt="search_icon" alt="img-responsive">
                            </div>
                            <input type="text" placeholder="Search" class="form-control">
                          </div> -->
                        </div>
                        <section class="common-struct login-input gig_det heading_gap" style="margin-bottom: 40px;">
                          <div class="post_list">
                            <div class="row">
                              <?php 
                              $num_times = 0;
                              ?>
                              @forelse($gigData as $gig)
                              <?php $num_times++; ?>
                              <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12 ">
                                  <div class="white_bg_sec"> 
                                      <div class="Update-img text-center"> 
                             
                                     @if(!empty($gig->user->profile))
                                    <img src="{{$gig->user->profile}}" alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>'
                              @else
                              <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive image" title="Click to expand image/video">
                              @endif  
                                   </div>
                                  <a href="{{url('business/gig-details',base64_encode($gig->id)).'/'.base64_encode('completed') }}">
                                    <h5 style="text-align: left;">@if(!empty($gig->gig_name)){{$gig->gig_name}} @else N/A @endif</h5>
                                    <div class="gig_content">
                                      <?php
                                      if(!empty($gig->gig_start_date_time)) {
                                        $date_time = $gig->gig_start_date_time;
                                           $date = $date_time;
                                      }else{
                                        $date = "N/A";
                                      }

                                      ?>
                                      <div class="date_content">
                                        <span class="equal_width">Date And Time:</span>{{$date}}
                                      </div>
                                      <div class="date_content">
                                        <span class="equal_width">Location: </span><p style="word-break: break-all;">@if(!empty($gig->location)){{substr($gig->location,0,100)}} @else N/A @endif
                                        </p>
                                      </div>
                                      <div class="date_content">
                                        <?php
                                              $gig_user = DB::table('gig_users')->where('gig_id',$gig->id)->count();
                                              $gig_count = $gig_user; 
                                          
                                          ?>
                                        <span class="equal_width">Attending: </span>@if(!empty($gig_count)){{$gig_count}} @else 0 @endif
                                      </div>
                                    </div>
                                    <div class="date_content" style="margin-top: 9px;">
                                      <span class="equal_width" style="margin-bottom: 2px;">Description</span>
                                      <p class="post_list_para" style="word-break: break-all;">
                                      @if(!empty($gig->description)){!!html_entity_decode(substr($gig->description,0,100))!!} @else N/A @endif</p>
                                     <!--  <section class="login_FORM summary_deials two_btns">
                                        <div class="form-group Submit medium_btn m_bt">
                                          <input type="Submit" name="Login" value="VIEW INFLUENCERS" class="btn btn-primary">
                                        </div>
                                        <div class="form-group Submit medium_btn " style="margin-bottom: 0px !important;">
                                          <input type="Submit" name="Login" value="INVITE INFLUENCERS" class="btn btn-primary">
                                        </div>
                                      </section> -->
                                    </div>
                                  </a>
                                  </div> 
                              </div>
                              @if($num_times % 3 == 0)
                              <div style="clear: both;"></div>
                              @endif
                           @empty
                        <div class="data_found text-center">
                           <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                           <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Gig Completed Yet</h1>
                        </div>
                     @endforelse
                  </div>
                  <div class = "col-md-12">
                     <div class="text-center">
                        {{$gigData->appends(request()->except('page'))->links() }}
                     </div>
                  </div>
                            </div>
                       

                        </section>
                        <!-- Modal -->
                           <div class="modal fade bs-example-modal-lg view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <img src="" alt="woman"  class="target_img">
                              </div>
                              <div class="cross_icon" data-dismiss="modal">
                                <a href="">
                                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade bs-example-modal-lg1 view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <video src="" alt="upload-one" class="target_vidoe" controls="" style = "width:100%;" title="Click to expand image"></video>
                              </div>
                              <div class="cross_icon" data-dismiss="modal">
                                <a href="">
                                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>              
                      </div>
                </div>
         
              </div>
            </div>

         
@endsection
@section('js')
      <!-- <script src="js/jquery.min.js"></script>  -->
       <!-- Add jQuery library -->
  
  <!-- Add mousewheel plugin (this is optional) -->
  <script type="text/javascript" src="{{url('public/business/js/jquery.mousewheel.pack.js')}}"></script>

  <!-- Add fancyBox main JS and CSS files -->
  <script type="text/javascript" src="{{url('public/business/js/jquery.fancybox.pack.js')}}"></script>
      <!-- <script src="js/bootstrap-lightbox.min.js"></script> -->
     
      <script type="text/javascript">
        $('.image_video').click(function(){
          $('.bs-example-modal-lg').modal({
            backdrop: 'static'
          });
        }); 
        $('.image_video1').click(function(){
          $('.bs-example-modal-lg1').modal({
            backdrop: 'static'
          });
        }); 

        $( document ).ready(function() {
          $('.fancybox').fancybox();
  console.log( "document ready!" );
  // $('#myLightbox').lightbox();

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>


      <script type="text/javascript">
        
        $('.image').click(function(){
         let img = $(this).attr('src')
         $(".target_img").attr('src',img)
          $('.bs-example-modal-lg').modal({
            backdrop: 'static'
          });
        }); 
         $('.video').click(function(){
         let video = $(this).attr('src')
         $(".target_vidoe").attr('src',video)
          $('.bs-example-modal-lg1').modal({
            backdrop: 'static'
          });
        }); 
      </script>

 @endsection
