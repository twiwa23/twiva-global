@extends("business/layout/web")
@section("title","Gig Details")
@section("content")
<style>
  .video-containt{
    width: 90px;
    height: 90px;
    border-radius: 5px;
        margin-right: 20px;
  }
    section.login_FORM .btn-primary{
   font-size: 17px;
  }
    .theme_photos ul li img {
    cursor: pointer;
}

  </style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <section class="align_btns login_FORM">
                        @if(Request::segment(4))
                          <div class="form-group Submit medium_btn">
                            <a href="{{url('business/complete-accept-gig').'/'.base64_encode($gig_detail->id)}}">
                              <input type="button" name="Login" value="ACCEPTED INFLUENCERS" class="btn btn-primary">
                            </a>
                          </div>
                          @else 
                          <div class="form-group Submit medium_btn">
                            <a href="{{url('business/accept-gig').'/'.base64_encode($gig_detail->id)}}">
                              <input type="button" name="Login" value="ACCEPTED INFLUENCERS" class="btn btn-primary">
                            </a>
                          </div>
                          <div class="form-group Submit medium_btn">
                            <a href="{{url('business/pending-gig-list').'/'.base64_encode($gig_detail->id)}}">
                              <input type="button" name="Login" value="PENDING INFLUENCERS" class="btn btn-primary">
                            </a>
                          </div>
                          <div class="form-group Submit medium_btn">
                            <a href="{{url('business/declined-gig').'/'.base64_encode($gig_detail->id)}}">
                              <input type="button" name="Login" value="DECLINED INFLUENCERS" class="btn btn-primary">
                            </a>
                          </div>
                          @endif
                        </section>
                      <h2 class="color-text">Gig Details</h2>
                        <section class="uploaded-data common-struct login_FORM login-input">
                          <div class="white_bg_sec common_white_bg">
                            <div class="labels">
                              <h5>Title</h5>
                              <p style="word-break: break-all;">@if(!empty($gig_detail->gig_name)) {{$gig_detail->gig_name}} @else N/A @endif</p>
                            </div>
                            <div class="labels">
                              <h5>Description</h5>
                              <p style="word-break: break-all;"> @if(!empty($gig_detail->description)){{$gig_detail->description}} @else N/A @endif</p>
                            </div>
                            <div class="labels">
                              <h5>What To Do</h5>
                              <p style="word-break: break-all;"> @if(!empty($gig_detail->what_to_do)){{$gig_detail->what_to_do}} @else N/A @endif</p>
                            </div>
                            <div class="gig_contact_flex">
                              <div class="right_m">
                                <div class="labels">
                                  <h5>Contact Person Name</h5>
                                  <p>@if(!empty($gig_detail->owner_name)){{$gig_detail->owner_name}} @else N/A @endif</p>
                                </div>
                                <div class="">
                                <div class="labels">
                                  <h5>Contact Person Number</h5>
                                  <p>@if(!empty($gig_detail->phone_number)){{$gig_detail->phone_number}} @else N/A @endif</p>
                                </div>
                                 <?php
                                     if(!empty($gig_detail->gig_start_date_time)) {
                                        $date_time = $gig_detail->gig_start_date_time;
                                           $date = $date_time;
                                      }else{
                                        $date = "N/A";
                                      }

                                      ?>
                                      
                                <div class="labels">
                                  <h5>Gig Start Date And Time</h5>
                                  <p>{{$date}}</p>
                                </div>
                              </div>
                              
                                <?php
                                      if(!empty($gig_detail->gig_end_date_time)) {
                                        $date_time = $gig_detail->gig_end_date_time;
                                           $date = $date_time;
                                      }else{
                                        $date = "N/A";
                                      }

                                      ?>
                                <div class="labels">
                                  <h5>Gig End Date And Time</h5>
                                  <p>{{$date}}</p>
                                </div>
                              </div>
                            </div>
                            <div class="labels">
                              <h5>Offered Price</h5>
                              <p>
                                {{$gig_detail->price_per_gig ? 'KSh'.$gig_detail->price_per_gig : 'KSh0'}}
                              </p>
                            </div>
                            <div class="labels">
                              <h5>Address</h5>
                              <p style="word-break: break-all;">@if(!empty($gig_detail->location)){{$gig_detail->location}} @else N/A @endif</p>
                            </div>
                            <div class="labels">
                              <h5>Venue</h5>
                              <p style="word-break: break-all;">@if(!empty($gig_detail->location)){{$gig_detail->location}} @else N/A @endif</p>
                            </div>
                            <div class="labels">
                              <h5>HashTag</h5>
                              <p style="word-break: break-all;">@if(!empty($gig_detail->hashtag)) #{{$gig_detail->hashtag}} @else N/A @endif</p>
                            </div>
                             @if(!empty($gig_detail->images))
                            <div class="labels">  
                              <h5>Theme Photo</h5>
                              <div class="theme_photos" style="margin-bottom: 20px">
                                <ul>
                                   <?php $videos = $All_files = "";
                            $no_doc = 0;
                            $total = 0;
                            $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                             @forelse($gig_detail->images as $key => $files)
                            <li>
        
                            <?php
                            $total++;
                            $get_file = $files->media;
                            $has_video = false;
                            $has_file = false;
                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                            $get_file_type = strtolower($get_file_type);
                           /*if(in_array($get_file_type,$video_ext)){
                              echo '<a href="'.$get_file.'" target="_blank" ><video src="'.$get_file.'" controls="" class="img-responsive video-containt" " ></video></a>';
                            }else {
                              echo '<a href="'.$get_file.'" target="_blank" ><img src="'.$get_file.'" " class="img-responsive" />';
                            }*/
                              if(in_array($get_file_type,$video_ext)){
                              echo '<video src="'.$get_file.'" alt="upload-one" controls="" class="img-responsive video-containt video" target="_blank" title="Click to expand image/video"></video>';
                            }else {
                              echo '<img src="'.$get_file.'" " alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>';
                            }
                             ?>    
                                  </li>
                                @empty
                            <!--  <img src="{{url('public/business/images/upload-one.png')}}" alt="blank_" alt="upload-one" class="img-responsive"> -->
                            @endforelse  
                            
                                
                                </ul>
                              </div>
                            </div>
                            @else
                            
                            @endif
                          </div>
                        </section>
                           <div class="modal fade bs-example-modal-lg view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <img src="" alt="woman"  class="target_img">
                              </div>
                              <div class="cross_icon" data-dismiss="modal">
                                <a href="">
                                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade bs-example-modal-lg1 view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <video src="" alt="upload-one" class="target_vidoe" controls="" style = "width:100%;" title="Click to expand image"></video>
                              </div>
                              <div class="cross_icon" data-dismiss="modal">
                                <a href="">
                                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                </a>
                              </div>
                            </div>
                          </div>
                        </div> 
                </div>
          
              </div>
            </div>
        
@endsection
@section('js')
 <script type="text/javascript" src="{{url('public/business/js/jquery.mousewheel.pack.js')}}"></script>

  <!-- Add fancyBox main JS and CSS files -->
  <script type="text/javascript" src="{{url('public/business/js/jquery.fancybox.pack.js')}}"></script>
      <!-- <script src="js/bootstrap-lightbox.min.js"></script> -->
     
      <script type="text/javascript">
        $('.image_video').click(function(){
          $('.bs-example-modal-lg').modal({
            backdrop: 'static'
          });
        }); 
        $('.image_video1').click(function(){
          $('.bs-example-modal-lg1').modal({
            backdrop: 'static'
          });
        }); 

        $( document ).ready(function() {
          $('.fancybox').fancybox();
  console.log( "document ready!" );
  // $('#myLightbox').lightbox();

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>


      <script type="text/javascript">
        
        $('.image').click(function(){
         let img = $(this).attr('src')
         $(".target_img").attr('src',img)
          $('.bs-example-modal-lg').modal({
            backdrop: 'static'
          });
        }); 
         $('.video').click(function(){
         let video = $(this).attr('src')
         $(".target_vidoe").attr('src',video)
          $('.bs-example-modal-lg1').modal({
            backdrop: 'static'
          });
        }); 
      </script>
     <!--  <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script> -->
@endsection