@extends("business/layout/web")
@section("title","Hire Me")
@section("content")

<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">

<style>

 .error {
  color: red;
}

  .alert-danger {
       color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 545px;
    width: 100%;
    margin: 0 auto;
    margin-bottom: 12px;

}
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}
.hire_me_bg {
    padding: 32px 20px;
}
button.btn.btn-success {
    background-color: #8a0e49;
    border: 0;
    margin-top: 15px;
}
.sub-heading p {
    background: -webkit-linear-gradient(#780c4f, #ad0e5a);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    margin: 0;
    font-size: 50px;
    line-height: 59px;
}

.hire_form_fields label {
    color: #000;
    font-size: 14px;
    margin: 8px 0;
}
.hire_me_heading {
    text-align: center;
}
.hire_me_heading h3 {
    /* background: -webkit-linear-gradient(#780c4f, #ad0e5a); */
    /* -webkit-background-clip: text; */
    /* -webkit-text-fill-color: transparent; */
    /* margin: 0; */
    /* font-size: 50px; */
    /* line-height: 59px; */
    background: rgb(97,15,65);
    background: -webkit-linear-gradient(-90deg, rgba(97,15,65,1) 16%, rgba(193,32,108,1) 67%);
    background: -o-linear-gradient(-90deg, rgba(97,15,65,1) 16%, rgba(193,32,108,1) 67%);
    /* background: linear-gradient(-90deg, rgba(97,15,65,1) 16%, rgba(193,32,108,1) 67%); */
    /* background: linear-gradient(175deg, rgba(97,15,65,1) 16%, rgba(193,32,108,1) 71%); */
    -webkit-background-clip: text;
    text-align: center;
    -webkit-text-fill-color: transparent;
    font-size: 40px;
    text-align: center;
    font-weight: 600;
}

</style>


<body>
<div class="right_col" role="main" style="min-height: 438px;">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
   <div class="row">
      <div class="col-sm-12">
         <div class="about-us">
	
        
		<section class="sec_banner">
			 @include('admin.layouts.notifications')
			      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
            @endif
			<div class="hire_me_bg">
				
         <!-- <form method = "post" action = "" enctype=multipart/form-data>
          {{csrf_field()}} -->
         
				<form method = "post" action  = "" >
					 {{csrf_field()}}
					<div class="hire_me_heading">
						<h3>Hire Me</h3>
					</div>
					<div class="hire_form_fields">
	                    
						<label for="">From :</label>
						<input type="text"  name = "email" value = "{{old('email')}}" class="form-control"> 
						<label for="" class="to">To :</label>
						<input type="text" name = "to" value = "{{$user->name}}" class="form-control to_input" readonly>
						<label for="" class="subject">Subject :</label>
						<input type="text" name = "subject" value = "{{old('subject')}}" class="form-control sub_input">
						<textarea name="text" value = "{{old('text')}}"  class="form-control" placeholder="Write Something here.."></textarea>
						
					</div>
					<div class="send_info d-flex justify-content-between align-items-center">
						<div>
							<button type="submit" name = "submit"  class="btn btn-success">SEND</button>
							<!-- <input type="Submit" name="submit" value="submit" class="btn btn-primary"> -->
						</div>
						<div>
						</div>
					</div>
				</form>
			</div>
		</section>
  </div>
</div>
</div>
</div>

@endsection
@section('js')
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
 <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
       CKEDITOR.replace('text');
        CKEDITOR.config.autoParagraph = false;

      </script>

<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>


<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

    
   </script>
   <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

    
  
  $('#form_validate').validate({ 
      rules: {
          email: {
              required: true,
              email:true,
              maxlength:200
          },
          
          subject: {
              required: true,
              maxlength:100,
              minlength:3,
               
          },
          
          text: {
              required: true,
              minlength:10,
              maxlength:2000
          },
           
      },

        messages:{
          subject:{  
            required:'Please ensubjectter subject.',
            minlength:'subject must be at least 3 characters.',
            maxlength:'subject maximum 100 characters.'
          },
          email:{  
            required:'Please enter email address.',
            email:'Please enter a valid email address.',
            maxlength:'Email address maximum 200 characters.'
          },
         

          text:{  
            required:'Please enter tex.',
            minlength:'Text must be at least 10 characters.',
            maxlength:'Text maximum 2000 characters.'
          },

             
        },
       
        /*errorHandler:function(e){
          console.log(e)
        }*/
       
    
  });
</script>
@endsection
