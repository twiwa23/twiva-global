@extends("business/layout/web")
@section("title","Influencer Details")
@section("content")
<style>
  .star_align ul li{
   display: flex;
}
.carousel-inner img{
  width:100%;
  object-fit: cover;
}
/*section.infu_list .theme_photos ul li img {
    width: 65px;
    height: 65px;
    margin-right: 16px;
}*/
.theme_photos ul {
    display: flex;
    flex-wrap: nowrap;
    margin-top: 4px;
}
.theme_photos {
    overflow-y: hidden;
    overflow-x: auto;
}
.theme_photos li  {
   
    margin-right: 12px;
}
.theme_photos::-webkit-scrollbar {
  width: 10px;
  height: 8px;
}

/* Track */
.theme_photos::-webkit-scrollbar-track {
  background: #888;
}

/* Handle */
.theme_photos::-webkit-scrollbar-thumb {
  background: #640C35;
}

/* Handle on hover */
.theme_photos::-webkit-scrollbar-thumb:hover {
  background: #640C35;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
  background: #640C35; 
}
img.img-responsive.round {
    border-radius: 10px 10px 10px 10px;
}


img.img-responsive.round {
    border-radius: 10px 10px 10px 10px;
    object-fit: contain;
    height: 256px;
    background-image: -webkit-linear-gradient( -90deg, rgb(117 14 58) 0%, rgb(35,8,38) 100%);
  
}

</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Influencer Details</h2>
                        <section class="uploaded-data common-struct login_FORM login-input details_silder">
                          <div class="infu_details white_bg_sec common_white_bg selected_bg_color unslected_bg" style="border: 0;">
                            <div id="carousel-example-generic" class="carousel slide crousel_slider" data-ride="carousel">
                              <!-- Indicators -->
                              <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                              </ol>

                              <!-- Wrapper for slides -->
                             <div class="carousel-inner" role="listbox">
                                <?php
                                  $i = 0;
                                  $im_exist = 0;
                                 ?>
                                 @forelse($infulancerDetail->images as $images)
                                  @if(!empty($images->image))
                                  <?php $i++;
                                    $im_exist++; ?>
                                    <div class="item {{$i == 1 ? 'active' : ''}} ">
                                      <img src="{{$images->image}}" alt="upload-one" class="img-responsive round"/>
                                    </div>
                                    @endif
                                    @empty
                              @endforelse
                              @if($im_exist == 0)
                                  <div class="item active">
                                   <img src="{{url('public/business/images/img-post.png')}}" alt="upload-one"> 
                              </div>
                              @endif
                                <!-- Controls -->
                                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="font-size: 22px;"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="font-size: 22px;"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                              </div>
                               </div>
                            <div class="labels details_pd">
                              <h5>Influencer Name</h5>
                              <p>@if(!empty($infulancerDetail->name)){{$infulancerDetail->name}} @else N/A @endif</p>
                            </div>
                            <div class="social-followers">
                            <div class="border-top">
                            </div>
                            <?php 
                                if($infulancerDetail->SocialDetail->facebook_friends) {
                                   if($infulancerDetail->SocialDetail->facebook_friends >= 1000000){
                                    $facebook_friends = number_format(($infulancerDetail->SocialDetail->facebook_friends / 1000000))."M";
                                   }else if($infulancerDetail->SocialDetail->facebook_friends >= 1000){
                                    $facebook_friends = number_format(($infulancerDetail->SocialDetail->facebook_friends / 1000))."K";
                                   }else {
                                    $facebook_friends = $infulancerDetail->SocialDetail->facebook_friends;
                                   }
                                }


                                if($infulancerDetail->SocialDetail->twitter_friends) {
                                   if($infulancerDetail->SocialDetail->twitter_friends >= 1000000){
                                    $twitter_friends = number_format(($infulancerDetail->SocialDetail->twitter_friends / 1000000))."M";
                                   }else if($infulancerDetail->SocialDetail->twitter_friends >= 1000){
                                    $twitter_friends = number_format(($infulancerDetail->SocialDetail->twitter_friends / 1000))."K";
                                   }else {
                                    $twitter_friends = $infulancerDetail->SocialDetail->twitter_friends;
                                   }
                                }

                                if($infulancerDetail->SocialDetail->instagram_friends) {
                                   if($infulancerDetail->SocialDetail->instagram_friends >= 1000000){
                                    $instagram_friends = number_format(($infulancerDetail->SocialDetail->instagram_friends / 1000000))."M";
                                   }else if($infulancerDetail->SocialDetail->instagram_friends >= 1000){
                                    $instagram_friends = number_format(($infulancerDetail->SocialDetail->instagram_friends / 1000))."K";
                                   }else {
                                    $instagram_friends = $infulancerDetail->SocialDetail->instagram_friends;
                                   }
                                }

                             ?>
                              <div class="social_flex">
                                <div class="social_img">
                                  <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                  <p>@if(!empty($facebook_friends)){{$facebook_friends}} @else 0 @endif<br> Followers</p>
                                </div>
                                <div class="social_img">
                                  <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                  <p>@if(!empty($instagram_friends)){{$instagram_friends}} @else 0 @endif<br> Followers</p>
                                </div>
                                <div class="social_img border_none">
                                  <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                  <p>@if(!empty($twitter_friends)){{$twitter_friends}} @else 0 @endif<br> Followers</p>
                                </div>
                              </div>  
                              <div class="border-bottom">
                              </div>
                            </div>
                            <div class="labels rating_details">
                              <h5>Rating</h5>
                              <div class="star_align" style="margin-top: 3px;">
                                <ul>
                                  
                                   <?php 
                                    $viewStar = "";
                                    $countstar = 0;
                                if($infulancerDetail->infulancerRating) {
                                    $allStar = $infulancerDetail->infulancerRating->avg('rating');
                                    $fullStar = (int)$allStar;
                                    for($i=0; $i< $fullStar; $i++) {
                                        $countstar++;
                                        $path = url('public/business/images/star.png');
                                        $viewStar .= "<li><img class='img-responsive' alt='star'  src='".$path."'></li></li>";

                                    }
                                    $remain_star = $allStar - $fullStar;
                                    if($remain_star > 0) {
                                        $countstar++;
                                        if($remain_star <= 0.5) {
                                            $path = url('public/images/halfstar.png');
                                            $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";

                                        }elseif($remain_star > 0.5) {
                                            $path = url('public/images/fullstar.png');
                                            $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";

                                        }
                                    }
                                   $remainingstar =  5-$countstar;
                                   if($remainingstar > 0) {
                                       for($i=0; $i< $remainingstar; $i++) {
                                            $path = url('public/images/emptyStar.png');
                                            $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";

                                        }
                                    }
                                } else {
                                    for($i=0; $i< 5; $i++) {
                                            $path = url('public/images/emptyStar.png');
                                            $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";
                                        }
                                }
                                 
                                echo $viewStar;

                              ?>
                                  
                                </ul>
                              </div>
                            </div>
                            <div class="labels rating_details">
                              <h5>Desired Post Rate</h5>
                              <p>@if(!empty($infulancerDetail->price->gig_price)){{'KSh'.$infulancerDetail->price->gig_price}} @else KSh0 @endif</p>
                            </div>
                            <div class="labels rating_details">
                              <h5>About</h5>
                              <p>@if(!empty($infulancerDetail->influencerDetail->about)){{$infulancerDetail->influencerDetail->about}} @else N/A @endif</p>
                            </div>
                             <div class="labels rating_details">
                              <h5 style="margin-bottom: 8px;">Interests</h5>
                              <div class="theme_photos">
                               <ul>
                                      
                                        @foreach($infulancerDetail->interests as $interestDatas)
                                      
                                        <li>
                                       @if(!empty($interestDatas->image))
                                   
                              <?php $image = $interestDatas->image ?>
                              <img src="{{$image}}" alt="upload-one" class="img-responsive"/>
                              @else
                              <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                              @endif
                               <p class="text-center name" style="font-weight: 100; color: #4f504f">
                                            @if(!empty($interestDatas->interest_name)){!!html_entity_decode(substr($interestDatas->interest_name,0,30))!!} @else  @endif
                                 </p> 

                               </li> 
                            
                          @endforeach
                               </ul>        
                                 
                              </div>
                            </div>
                          </div>
                        </section>
                        
                </div>
       
              </div>
            </div>
        

@endsection
@section('js')
      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
   @endsection