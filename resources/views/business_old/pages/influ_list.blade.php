@extends("business/layout/web")
@section("title","Influencers List")
@section("content")
<style>
   img.round {
   position: relative;
   border: 4px solid #fff;
   border-radius: 50%;
   box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
   width: 120px;
   height: 120px;
   margin: auto;
   cursor: pointer;
   }
   .star_align ul li{
   display: flex;
   }
   /*section.infu_list .theme_photos ul li img {
   width: 65px;
   height: 65px;
   margin-right: 16px;
   }*/
   .theme_photos ul {
   display: flex;
   flex-wrap: nowrap;
   margin-top: 4px;
   }
   .theme_photos {
   overflow-y: auto;
   overflow-x: auto;
   }
   .theme_photos li  {
   margin-right: 12px;
   }
   .theme_photos::-webkit-scrollbar {
   width: 10px;
   height: 8px;
   }
   /* Track */
   .theme_photos::-webkit-scrollbar-track {
   background: #888;
   }
   /* Handle */
   .theme_photos::-webkit-scrollbar-thumb {
   background: #640C35;
   }
   /* Handle on hover */
   .theme_photos::-webkit-scrollbar-thumb:hover {
   background: #640C35;
   }
   .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
   background: #640C35; 
   }
   .star_align ul li {
   margin-right: 5px !important;
   }
   .search_input button {
   background-color: red;
   display: inline-block;
   padding: 5px 8px;
   color: #fff;
   }
   .search_input button {
   background-color: #560c31;
   display: inline-block;
   border: 1px solid #560c31;
   padding: 8px 8px;
   color: #fff;
   position: absolute;
   top: 0px;
   right: 0;
   border-top-right-radius: 12px;
   border-bottom-right-radius: 12px;
   }
   .tabs_list .search_input input{
   padding-right: 62px;
   }
  /* .common-struct .Update-img img {
    margin: auto;
    cursor: pointer;
    object-fit: contain;
    border-radius: 100%;
}
.white_bg_sec .Update-img{
      background-color: #fff;

}*/
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
   <div class="row">
      <div class="col-sm-12">
         <div class="about-us">
            <h2 class="color-text">Influencers List</h2>
            <div class="p_allside">
               <div class="tabs_list" style="margin-top: 12px;">
                  <div class="filter_btns filter_align" style="visibility: hidden;">
                     
                  </div>
                  <div class="search_input">
                     <form method="get">
                        <div>
                           <img src="{{url('public/business/images/search_icon.png')}}" alt="search_icon"> 
                        </div>
                           @isset($_GET['search'])
                           <a href="{{url('business/influencers-list')}}" style="position: absolute;right: 64px;top: 10px;color: black;">
                              <i class="fa fa-times"></i>
                           </a>
                           @endif
                        <input type="text" placeholder="Search" name="search" class="form-control search" value="{{isset($_GET['search']) ? $_GET['search'] : ''}}"> 
                        <button type="submit" id="search_infl" style="cursor: pointer;">Search</button>
                     </form>
                  </div>
               </div>
               <section class="common-struct login-input infu_list infu_interest" style="margin-bottom: 80px;">
                  <div class="post_list">
                     <div class="row">
                        @forelse($infulancer_list as $infulancers)
                        <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12">
                           <a href="{{url('business/influencer-details',base64_encode($infulancers->id))}}" class="click_event">
                              <div class="white_bg_sec selected_bg_color unslected_bg" style = "height: 539px;">
                                 <div class="Update-img text-center"> 
                                    @if(!empty($infulancers->images))
                                    <?php $image = @$infulancers->images[0]->image;  ?> 
                                    <img src="{{$image}}" alt="upload-one" class="img-responsive round"/>
                                    @else
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive round">
                                    @endif 
                                 </div>
                                 <div class="heading_star_align">
                                    <h5 style="text-align: left;">@if(!empty($infulancers->name)){{ substr($infulancers->name,0,15)}} @else N/A @endif</h5>
                                    <div class="star_align">
                                       <ul>
                                          <li>
                                             <?php 
                                                $viewStar = "";
                                                $countstar = 0;
                                                if($infulancers->infulancerRating) {
                                                $allStar = $infulancers->infulancerRating->avg('rating');
                                                $fullStar = (int)$allStar;
                                                for($i=0; $i< $fullStar; $i++) {
                                                $countstar++;
                                                $path = url('public/business/images/star.png');
                                                $viewStar .= "<li><img class='img-responsive' alt='star'  src='".$path."'></li>";
                                                
                                                }
                                                $remain_star = $allStar - $fullStar;
                                                if($remain_star > 0) {
                                                $countstar++;
                                                if($remain_star <= 0.5) {
                                                $path = url('public/images/halfstar.png');
                                                $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";
                                                
                                                }elseif($remain_star > 0.5) {
                                                $path = url('public/images/fullstar.png');
                                                $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";
                                                
                                                }
                                                }
                                                $remainingstar =  5-$countstar;
                                                if($remainingstar > 0) {
                                                for($i=0; $i< $remainingstar; $i++) {
                                                $path = url('public/images/emptyStar.png');
                                                $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";
                                                
                                                }
                                                }
                                                } else {
                                                for($i=0; $i< 5; $i++) {
                                                $path = url('public/images/emptyStar.png');
                                                $viewStar .= "<li><img class='img-responsive' alt='star' src='".$path."'></li>";
                                                }
                                                }
                                                
                                                echo $viewStar;
                                                
                                                ?>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="border-top">
                                 </div>
                                 <?php 
                                    if($infulancers->SocialDetail->facebook_friends) {
                                       if($infulancers->SocialDetail->facebook_friends >= 1000000){
                                        $facebook_friends = number_format(($infulancers->SocialDetail->facebook_friends / 1000000))."M";
                                       }else if($infulancers->SocialDetail->facebook_friends >= 1000){
                                        $facebook_friends = number_format(($infulancers->SocialDetail->facebook_friends / 1000))."K";
                                       }else {
                                        $facebook_friends = $infulancers->SocialDetail->facebook_friends;
                                       }
                                    }


                                    if($infulancers->SocialDetail->twitter_friends) {
                                       if($infulancers->SocialDetail->twitter_friends >= 1000000){
                                        $twitter_friends = number_format(($infulancers->SocialDetail->twitter_friends / 1000000))."M";
                                       }else if($infulancers->SocialDetail->twitter_friends >= 1000){
                                        $twitter_friends = number_format(($infulancers->SocialDetail->twitter_friends / 1000))."K";
                                       }else {
                                        $twitter_friends = $infulancers->SocialDetail->twitter_friends;
                                       }
                                    }

                                    if($infulancers->SocialDetail->instagram_friends) {
                                       if($infulancers->SocialDetail->instagram_friends >= 1000000){
                                        $instagram_friends = number_format(($infulancers->SocialDetail->instagram_friends / 1000000))."M";
                                       }else if($infulancers->SocialDetail->instagram_friends >= 1000){
                                        $instagram_friends = number_format(($infulancers->SocialDetail->instagram_friends / 1000))."K";
                                       }else {
                                        $instagram_friends = $infulancers->SocialDetail->instagram_friends;
                                       }
                                    }

                                 ?>
                                 <div class="social_flex">
                                    <div class="social_img">
                                       <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                       <p>@if(!empty($facebook_friends)){{$facebook_friends}} @else 0 @endif<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                       <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                       <p>@if(!empty($instagram_friends)){{$instagram_friends}} @else 0 @endif<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                       <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                       <p>@if(!empty($twitter_friends)){{$twitter_friends}} @else 0 @endif<br> Followers</p>
                                    </div>
                                 </div>
                                 <div class="border-bottom">
                                 </div>
                                 <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>
                                    {{ $infulancers->plug_price ? 'KSh'.$infulancers->plug_price : 'KSh0' }}
                                 </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Gig Rate: </span>
                                    {{ $infulancers->gig_price ? 'KSh'.$infulancers->gig_price : 'KSh0' }}
                                 </div>
                                 <div class="labels">
                                    <h5 style="margin-bottom: 8px; margin-top: 20px;">Interests</h5>
                                    <div class="theme_photos">
                                       <ul>
                                          @foreach($infulancers->interests as $interestDatas)
                                          <li>
                                             @if(!empty($interestDatas->image))
                                             @php $image = $interestDatas->image;  @endphp
                                             <img src="{{$image}}" alt="upload-one" class="img-responsive"/>
                                             @else
                                             <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                             @endif
                                             <p class="text-center name">
                                                @if(!empty($interestDatas->interest_name)){!!html_entity_decode(substr($interestDatas->interest_name,0,10))!!} @else  @endif
                                             </p>
                                          </li>
                                          @endforeach
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </div>
                        @empty
                        <div class="data_found text-center">
                           <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                           <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
                        </div>
                        @endforelse
                     </div>
                     <div class = "col-md-12">
                        <div class="text-center">
                           {{$infulancer_list->appends(request()->except('page'))->links() }}
                        </div>
                     </div>
                  </div>
            </div>
            </section>
         </div>
      </div>
      <!-- Modal -->
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $( document ).ready(function() {


    $("#search_infl").click(function() {
      let search = $(".search").val();
      if(search) {
        window.location.href = "{{ url('business/influencers-list') }}"+"?query="+search;
      }
    })
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
@endsection