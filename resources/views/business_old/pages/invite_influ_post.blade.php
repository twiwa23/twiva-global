@extends("business/layout/web")
@section("title","Invite Influencers")
@section("content")
<style type="text/css">
  .active-selection {
    border: 3px solid rgb(115,14,57);
  }
  .lightBorderHighLight {
    border:1px solid lightgray;
  }
  .gig_box:hover {
    border: 1px solid rgb(115,14,57);
  }
  img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
  }
  .alert-success {
    width: 700px !important;
    margin: 0 auto;
  }

  .common-btn input {
    height: 41px !important;
    width: 51% !important;
  }
  .form-group.Submit.medium_btn.m_top {
    display: flex;
    align-items: center;
    justify-content: space-around;
        flex-direction: row-reverse;

}
</style>
<script type="text/javascript">
  localStorage.removeItem("create_id")
</script>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
   <div class="col-sm-12">
      <div class="about-us">
        @include('business.includes.notifications')
         <h2 class="color-text">Invite Influencers</h2>
         <div class="p_allside">
            <div class="tabs_list sugget_btn" style="margin-top: 12px;">
               <div class="filter_btns filter_align">
                  <a href="{{url('business/post-filter').'/'.$id}}">
                  <button type="button" class="btn btn-primary mehron_btn"><img src="{{url('public/business/images/filter.png')}}" alt="filter" class="filter_icon">Filter</button>
                  </a>
                  <button type="button" class="btn btn-primary green_btn show-suggested">Suggested For You</button>
                  <label class="check_box">Select All
                  <input type="checkbox" id="select_all" />
                  <span class="checkmark1"></span>
                  </label>
               </div>
                 <div class="search_input">
                      <div>
                        <img src="{{url('public/business/images/search_icon.png')}}" alt="search_icon" alt="img-responsive">
                      </div>
                      <input type="text" placeholder="Search" class="form-control" id="search-input">
                    </div>
            </div>
            <section class="common-struct login-input">
               <div class="post_list"></div>
            </section>
            <div style="clear: both"></div>
            <section class="login_FORM summary_deials infu_btns">
               <div class="form-group Submit medium_btn m_top">
                  <input type="hidden" name="selected_users" id="selected_users">
                  <input type="hidden" name="users_price" id="users_price">
                  <input type="button" name="Login" value="PROCEED" class="btn btn-primary" id="proceed-btn" ref = "{{url('business/post-summary-page').'/'.Request::segment(3) }}" />
                  <div style="float: left;width: 200px;">
                     <a href="" style="padding: 12px;display: none;" class="btn btn-primary btn-small" id="next-btn" />Show More</a>
                  </div>
               </div>
            </section>

            <!-- Modal -->
          <div class="modal fade bs-example-modal-lg view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                   <div class="modal-body">
                      <img src="" alt="woman"  class="target_img">
                   </div>
                   <div class="cross_icon" data-dismiss="modal">
                      <a href="">
                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                      </a>
                   </div>
                </div>
             </div>
          </div>
          <!-- Modal -->
         
            <!-- Modal -->
            <div class="modal fade modal_heading" id="open-update-price-pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
               <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="modal-body">
                        <h6 class="modal-title" id="exampleModalLongTitle">Adjust Price</h6>
                        <section class="set_price_popup login_FORM">
                           <div class="label_addres" style="text-align: center;">
                              <label for="">
                              Set Price
                              </label>
                           </div>
                           <div class="form-group">
                              <input type="hidden" id="set_uid" />
                              <div style="width: 80%;margin:auto">
                                 <input type="text" maxlength="6" id="set_price" class="form-control" required />
                                 <span class="text-danger"></span>
                              </div>
                           </div>
                           <div class="label_addres" style="text-align: center;">
                              <label for="">
                              Complementary Items (Optional)
                              </label>
                           </div>
                           <div class="form-group text_area">
                              <div style="width: 80%;margin:auto;">
                                 <textarea id="set_compliment"></textarea>
                                 <span class="text-danger"></span>
                              </div>
                           </div>
                        </section>
                        <div class="common-btn">
                           <input type="submit" value="UPDATE" class="btn btn-primary" id="update_price">
                        </div>
                     </div>
                     <div class="cross_icon" data-dismiss="modal">
                        <a href="">
                        <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade modal_heading" id="exampleModalCenter23" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-body">
                  <h6 class="modal-title" id="exampleModalLongTitle">Adjust Price</h6>
                  <section class="set_price_popup login_FORM">
                     <div class="form-group">
                        <div class="email">
                           <input type="text" id="email" name="email" value="" class="form-control" placeholder="Set Price">
                           <span class="text-danger"></span>
                        </div>
                     </div>
                     <div class="form-group text_area">
                        <div class="email">
                           <!-- <input type="text" id="email" name="email" value="" class="form-control" placeholder="Complementary Items (Optional)"> -->
                           <textarea type="text" name="" id=""  rows="3" placeholder="Complementary Items (Optional)"></textarea>
                           <span class="text-danger"></span>
                        </div>
                     </div>
                  </section>
                  <div class="common-btn">
                     <input type="submit" name="Login" value="UPDATE" class="btn btn-primary" data-dismiss="modal">
                  </div>
               </div>
               <div class="cross_icon" data-dismiss="modal">
                  <a href="">
                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                  </a>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
   </div>
</div>
@endsection
@section('js')

      <script type="text/javascript">

        /* set old values of selected users */
        $("#users_price").val(localStorage.getItem("_curSelUserPrs"))
        $("#selected_users").val(localStorage.getItem("_curSelUser"))
        /* make unselect checkbox*/
        $("#select_all").attr("checked",false)
        $("#select_all").prop("checked",false)

        function highLightContainer(element,type){
        console.log("trigger")
        if(type == "add"){
          element.addClass("active-selection").removeClass("lightBorderHighLight")
        }else if(type == "remove"){
           element.removeClass("active-selection").addClass("lightBorderHighLight")
        }
      }


      function parseK(input){
        input = parseInt(input)
        if(input >= 1000000){
          return parseFloat((input / 1000000).toFixed(2)) + "M";
        }else if(input >= 1000){
          return parseFloat((input / 1000).toFixed(2)) + "K";
        }else {
          return input;
        }
      }


        // static price set by gig creator
        const  postPrice = "{{$postprice}}";
        function pushPopUsers(id){
          /* if user id exist then remove id else add user id*/
          let val = $("#selected_users").val();
          let ids = [];
          if(val != ""){
            ids = val.split(",");
          }

          let index = ids.indexOf(id);
          if(index == -1){
            ids.push(id)
          }else {
            ids.splice(index,1);
          }
          $("#selected_users").val(ids.join(","))

          localStorage.setItem("_curSelUser",$("#selected_users").val())
        }

        // select all users
        $("#select_all").click(function(){
          let users = [];
          if($(this).prop("checked")){
            $("#users_price").val("")
            $(".post_list").find(".gig_container").each(function(){
              users.push($(this).attr("uid"))
              setPrice($(this).attr("uid"),postPrice,"add")
              pushPopUsers($(this).attr("uid"))
            })
            $(document).find(".perfomselectionchange").text("UNSELECT")
            $("#selected_users").val(users.join(","));
          }else {
            $("#selected_users").val("");
            $("#users_price").val("")
            localStorage.removeItem("_curSelUserPrs")
            localStorage.removeItem("_curSelUser")
            $(document).find(".perfomselectionchange").text("SELECT")
          }
        })

        /* Adjust price pop up*/
        $(document).on("click",'.adjust_popup',function(){
          if($(this).parent().find(".perfomselectionchange").text() == "SELECT"){
            alert("Please select user for adjust price.");
            return false;
          }

         let uid = $(this).data("uid");
          $("#set_uid").val(uid);

          let getDetail = getPrice("get",uid)

          $("#set_price").val(getDetail.price);
          $("#set_compliment").val(getDetail.compliment != null && getDetail.compliment != undefined ? getDetail.compliment : '');

          $('#open-update-price-pop').modal({
            backdrop: 'static'
          });
        });

        /* set updated price for user*/

        $(document).on("click","#update_price",function(){
          let price = $("#set_price").val();
          let compliment = $("#set_compliment").val();
          if(compliment == ""){
            compliment = $("#set_compliment").text();
          }
          let uid = $("#set_uid").val();
          if(price == "" || price.trim().length == 0){
            alert("Please enter price");
            return false;
          }else if(isNaN(price)){
              alert("Please enter number only");
              return false;
          } else if(price <= 0){
            alert("Price should be equal or higher then 1");
            return false;
          }

          setPrice(uid,price,"update",compliment);
          $("#open-update-price-pop").modal("hide")

        })

        /* text change and mark checked if all users are selected or vice-versa*/
        $(document).on('click','.perfomselectionchange',function(){
              let id = $(this).closest(".gig_container").attr("uid")
            if($(this).text() == "SELECT"){
              $(this).text("UNSELECT");
              setPrice(id,postPrice,"add")
            }else {
              $(this).text("SELECT");
              setPrice(id,null,"remove")
            }

            pushPopUsers(id)

            let getAllSelected = $("#selected_users").val().split(",").filter(item => item);
            if(getAllSelected.length == $(".post_list").find(".gig_container").length){
              $("#select_all").click();
              $("#select_all").attr("checked",true);
            }else {
              $("#select_all").attr("checked",false);
            }
        
        });

    $( document ).ready(function(){
        var $sticky = $('.sticky');
        var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
        if (!!$sticky.offset()) { // make sure ".sticky" element exists

          var generalSidebarHeight = $sticky.innerHeight();
          var stickyTop = $sticky.offset().top;
          var stickOffset = 0;
          var stickyStopperPosition = $stickyrStopper.offset().top;
          var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
          var diff = stopPoint + stickOffset;

          $(window).scroll(function(){ // scroll event
            var windowTop = $(window).scrollTop(); // returns number

            if (stopPoint < windowTop) {
                $sticky.css({ position: 'absolute', top: diff });
            } else if (stickyTop < windowTop+stickOffset) {
                $sticky.css({ position: 'fixed', top: stickOffset });
            } else {
                $sticky.css({position: 'absolute', top: 'initial'});
            }
          });
        }
  });


function fetchGigs(url,data = {}){
  localStorage.setItem("last_url",url)
  $.ajax({
    url : url,
    method : "POST",
    data : data,
    beforeSend : function(){
      $("#next-btn").attr("disabled",true)
    },
    success:function(res){
      // $("#select_all").parent().hide()
      var showRes = "";
       let isCurrentPage = false;
       let currentPage = -1;
       let lastPage = 0; 
       let getPageNum = 1;
       
       if(url.indexOf("?") != -1){
        let a = url.split("?");
        if(a.length > 1){
          let b = a[1].split("=");
          if(b.length > 1){
            getPageNum = parseInt(b[1])
          }
        }
       }

      if(res.post != "" && res.post != null){
        let post = res.post.data;
        let i = 0;
        currentPage = res.post.current_page;
        lastPage = res.post.last_page;

        if(post != null && post != undefined && post.length  > 0){
          if(res.post.next_page_url != null && res.post.next_page_url != "" && res.post.next_page_url != undefined){
            $("#next-btn").attr("href",res.post.next_page_url).show();
          }else {
            $("#next-btn").hide();
          }

          isCurrentPage = res.post.current_page == 1 ? true : false;
          

          post.forEach(function(data,i){
            let profile = "{{ url('public/business/images/upload-one.png') }}";

             if(data.images != "" && data.images != null && data.images.length > 0){
              for(let a = 0;a < data.images.length;a++){
                if(data.images[a].image != null || data.images[a].image != "" || data.images[a].image != undefined){
                  profile = data.images[a].image;
                  break;
                }
              }
            }

            i++;
            let isRow = i % 4 == 0;
            let isAllSelected = false;
            if($("#select_all").is(":checked") === true){
              pushPopUsers(data.uid)
              isAllSelected = true;
              setPrice(data.uid,postPrice,"add")
            }

            let isExist = getPrice("get",data.uid);

            let influencer_url = "{{url('business/influencer-details/')}}/"+btoa(data.uid);
            showRes += (`<div class="col-md-4 gig_container" uid='${data.uid}' >
                                  <div class="white_bg_sec selected_bg_color ${isAllSelected || isExist ? 'active-selection' : 'lightBorderHighLight'} gig_box"> 
                                  <a href="${influencer_url}">
                                    <div class="Update-img text-center">
                                      <img src="${profile}" alt="upload-one" class="img-responsive image_preview round">
                                    </div>
                                    <h5>${data.name ? data.name.substr(0,20) : "User"}</h5>
                                    <div class="border-top">
                                    </div>
                                    <div class="social_flex">
                                      <div class="social_img">
                                        <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                        <p>${data.facebook_friends > 0 ?  parseK(data.facebook_friends) : 0}<br> Followers</p>
                                      </div>
                                      <div class="social_img">
                                        <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                        <p>${data.instagram_friends > 0 ? parseK(data.instagram_friends) : 0}<br> Followers</p>
                                      </div>
                                      <div class="social_img border_none">
                                        <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                        <p>${data.twitter_friends > 0 ? parseK(data.twitter_friends) : 0}<br> Followers</p>
                                      </div>
                                    </div>
                                    <div class="border-bottom">
                                    </div>
                                    <div class="date_content">
                                      <span class="equal_width">Desired Post Rate: </span>KSh${data.gig_price > 0 ? data.gig_price : 0}
                                    </div>
                                    </a>
                                    <div class="filter_btns d-flex" style="margin-top: 24px;">
                                      <button type="button" class="btn btn-primary green_btn perfomselectionchange">${isAllSelected || isExist ? 'UNSELECT' : 'SELECT'}</button>
                                      <button type="button" class="btn btn-primary green_btn adjust_popup" data-uid = ${data.uid}>ADJUST PRICE</button>
                                    </div>
                                  </div>
                                </div>`+ (i % 3 == 0 ?  "<div style='clear:both;width:100%'></div>" : ''))
          })
        }
      }

      $("#next-btn").removeAttr("disabled",true)


      // if(showRes != "" || showRes.length > 0){
      //   if(isCurrentPage){
      //     $('.post_list').html(showRes)
      //   }else {
      //     $('.post_list').append(showRes)
      //   }

      //   // $("#select_all").parent().show();
      // }else {
      //   // console.log("currentPage ",currentPage," last page ",lastPage)
      //   if(currentPage < lastPage){
      //     $("#select_all").parent().hide();
      //     $("#next-btn").hide();
      //     $('.post_list').html('<h4 class="text-center">No data available</h4>')
      //   }
        
      // }

      if((showRes == "" || showRes.length == 0) && getPageNum == 1){
          $("#select_all").parent().hide();
          $("#next-btn").hide();
          $('.post_list').html('<h4 class="text-center">No data available</h4>')
      }else {
              $("#select_all").parent().show();
          if(showRes != "" || showRes.length > 0){
            if(isCurrentPage){
              $('.post_list').html(showRes)
            }else {
              $('.post_list').append(showRes)
            }

            // $("#select_all").parent().show();
          }else {
            // console.log("currentPage ",currentPage," last page ",lastPage)
            if(currentPage < lastPage){
              $("#select_all").parent().hide();
              $("#next-btn").hide();
              $('.post_list').html('<h4 class="text-center">No data available</h4>')
            }
            
          }
       }
       
    }
  })
}

/* pagination*/
let getFilter = localStorage.getItem("user_filter");

if(getFilter != "" && getFilter != null && getFilter != undefined){
  getFilter = JSON.parse(getFilter)
  if(getFilter.search != null && getFilter.search != undefined){
    $("#search-input").val(getFilter.search)
  }
}else{
  getFilter = {}
}

fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);

$("#next-btn").click(function(){
  event.preventDefault();
  if($(this).attr("disabled")){
    return false;
  }
  fetchGigs($(this).attr("href"),getFilter)
})

$("#search-input").on("keyup",function(){
  let search = $(this).val();
  // $("#selected_users,#users_price").val("")
  getFilter['search'] = search;

  localStorage.setItem("user_filter",JSON.stringify(getFilter));

  fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);
})

/* show-suggested */

$(".show-suggested").click(function(){
  if(!$(this).attr("is-showing")){
      $(this).attr("is-showing",true)
      getFilter["hasLimit"] = 20;
      fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);
  }else {
    $(this).removeAttr("is-showing")
    delete getFilter['hasLimit'];
    localStorage.setItem("user_filter",JSON.stringify(getFilter));
    fetchGigs("{{url('business/invite-influencers-post').'/'.$id}}",getFilter);
  }
})

/* set static price which gig creator already assign
 * set new price for user's if  if gig created update the price
*/
function setPrice(id,price,type,compliment = null){
  let prices = $("#users_price");

  let getAllPrices = prices.val();
  let setPrices = [];

  if(getAllPrices != "" && getAllPrices != null){
    setPrices = JSON.parse(getAllPrices);
  }

  if(type == "remove"){
    let delIndex = -1;
     setPrices.map((e,i) => {
      if(e.id == id){
        delIndex = i;
      }
      return e;
    });
     setPrices.splice(delIndex,1);
  }else if(type == "add"){
    let delIndex = -1;
    setPrices.map((e,i) => {
      if(e.id == id){
        delIndex = i;
      }
      return e;
    });

    if(delIndex == -1){
      let setData = {
        id : id,
        price : price
      };
      setPrices.push(setData);
    }
  }else if(type == "update"){
    setPrices.map((e) => {
      if(e.id == id){
        if(compliment){
           e['compliment'] = compliment;
        }

        e.price = price;
      }
      return e;
    })
  }
  prices.val(JSON.stringify(setPrices))

  localStorage.setItem("_curSelUserPrs",JSON.stringify(setPrices))
}


function getPrice(type,id = null){
  let prices = $("#users_price").val();
  let getPrices = [];

  if(prices != "" && prices != null){
    getPrices = JSON.parse(prices);
  }

   if(type == "get"){
    let index = -1;
     getPrices.map((e,i) => {
      if(e.id == id){
        index = i;
      }
      return e;
    });
     return getPrices[index];

 }else if(type == "sum"){
    let allData = {
      "sum" : 0,
      "total" : 0
    }

    getPrices.map((e,i) => {
      allData["sum"] = + allData["sum"] +  + e.price 
      allData["total"] = allData["total"] + 1
    });
    return allData;
 }
}


/* check if gig creator select any user or not,
if gig creator not select any user need
 to show alert for select atleast a user
*/

$("#proceed-btn").click(function(){
  if($("#selected_users").val() == ""){
    alert("Please select influencer before proceeding.");
  }else {
    localStorage.setItem("selected_users",$("#selected_users").val())
    localStorage.setItem("users_price",$("#users_price").val())
    $("#select_all").removeAttr("checked").prop("checked",false)
    window.location.href = $(this).attr("ref");
  }
})

/* change border of post user container when ever select/deselect user*/
$('body').on('DOMSubtreeModified', '.perfomselectionchange', function(){
  if($(this).text() == "UNSELECT"){
    highLightContainer($(this).closest(".gig_box"),"add")
  }else {
    highLightContainer($(this).closest(".gig_box"),"remove")
  }
});


$(document).ready(function() {
    $('.image_preview').click(function(){
      let img = $(this).attr('src')
      $(".target_img").attr('src',img)
       $('.bs-example-modal-lg').modal({
         backdrop: 'static'
       });
     }); 
    
  })


</script>


   @endsection