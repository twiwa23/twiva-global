@extends("business/layout/web")
@section("title","Notifications")
@section("content")
<style>
 .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
  background: #640C35; 
}

</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
   <div class="col-sm-12">
      <div class="about-us">
         <h2 class="color-text">Notifications</h2>
         <div class="p_allside">
            <!-- <div class="tabs_list" style="margin-top: 12px;">
               <div class="filter_btns" style="visibility: hidden;">
                  <button type="button" class="btn btn-primary mehron_btn">Pending</button>
                  <button type="button" class="btn btn-primary green_btn">Accepted</button>
                  <button type="button" class="btn btn-primary mehron_btn">Declined</button>
                  <button type="button" class="btn btn-primary green_btn">Completed</button>
               </div>
               <div class="search_input">
                  <div>
                     <img src="{{url('public/business/images/search_icon.png')}}" alt="search_icon" alt="img-responsive">
                  </div>
                  <input type="text" placeholder="Search" class="form-control">
               </div>
            </div> -->
            <section class="common-struct login-input">
               <div class="post_list">
                  <div class="row">
                     <div class="col-md-12">
                        @forelse($getNotificationList as $notificationlist)
			              <?php
			             if($notificationlist->type == 'post'){
			                $url = url('business/post-details',base64_encode($notificationlist->post_id));
			              }else{
			                $url =  url('business/gig-details',base64_encode($notificationlist->post_id));
			              }
			              ?>
                        <a href = "{{$url}}">
                        <div class="notification_bg">
                           <div class="noti_para">
                              <p>@if(!empty($notificationlist->message)){{$notificationlist->message}} @else N/A @endif</p>
                              <span class="equal_width">@if(!empty($notificationlist->created_at)){{$notificationlist->created_at}} @else N/A @endif</span>
                           </div>
                        </div>
                        @empty
                        <div class="data_found text-center">
                           <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                           <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
                        </div>
                        @endforelse
                    </a>
                     </div>
                     <div class = "col-md-12">
                        <div class="text-center">
                           {{$getNotificationList->appends(request()->except('page'))->links() }}
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
@endsection