@extends("business/layout/web")
@section("title","Pending Gig Influencer Details")
@section("content")
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Pending Gig Influencer Details</h2>
                      <div class="p_allside">
                        <section class="uploaded-data common-struct login_FORM login-input single_list">
                          <div class="white_bg_sec"> 
                            <div class="Update-img text-center">
                              <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Influencer Name: </span>John Lewis
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Offered Price: </span>$120
                            </div>
                            <div class="date_content">
                              <span class="equal_width w2">Status:  </span>Pending
                            </div>
                            <div class="form-group Submit medium_btn m_top">
                              <a href="{{url('business/pending-gig-list')}}">
                                <input type="button" name="Login" value="SEND REMINDER" class="btn btn-primary">
                              </a>
                            </div>
                          </div>
                        </section>
                      </div>
                </div>
              
              </div>
            </div>
         @endsection
         @section('js')
      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
 @endsection