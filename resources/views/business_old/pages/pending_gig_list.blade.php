@extends("business/layout/web")
@section("title","Pending Gig Influencers List")
@section("content")
<style>
  .pagination {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
    float: right;
    color: red;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #410a2d;
    border-color: #410a2d;
}
img.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
}
      video.round {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
    background-color: #fff;
    object-fit: cover;
}
.alert-success {
    width: 700px !important;
    margin: 0 auto;
}
.gig_list .white_bg_sec{
  min-height: 328px;

}



</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                      @include('business.includes.notifications')
                    <div class="about-us">
                      <h2 class="color-text">Pending Gig Influencers List</h2>
                      <div class="p_allside">
                        <div class="tabs_list pending_button" style="margin-top: 12px;margin-bottom: 12px;">
                          <div class="filter_btns button-size">
                            <a href = "{{url('business/accept-gig',base64_encode($id))}}" class="btn btn-primary green_btn">Accepted</a>
                             <a href="javascript:void(0)"
                             class="btn btn-primary mehron_btn">Pending</a>
                             <a href="{{url('business/declined-gig',base64_encode($id))}}"
                           class="btn btn-primary green_btn">Declined</a>
                           
                          </div>
                        </div>
                        <section class="common-struct login-input equal_height equal_mb">
                          <div class="Gig_list">
                           							
							               <div class="row">
                              @if(!empty($pendingGigDetails) && $pendingGigDetails && count($pendingGigDetails) > 0)
                              @foreach($pendingGigDetails as $gig)
                              <div class="col-md-4">
                                  <div class="white_bg_sec"> 
                                    <a href="{{url('business/influencer-details',base64_encode($gig->user->id))}}">
                                    <div class="Update-img text-center">
                                        <?php 
                                          $userImage = url('public/business/images/upload-one.png');
                                        ?>
                                        @forelse($gig->user->images as $images)
                                          @if($images->image && !empty($images->image))
                                            <?php 
                                              $userImage = $images->image;
                                              break;
                                            ?>
                                          @endif
                                        @empty
                                        @endforelse
                                       
                                        <img src="{{$userImage}}" alt="upload-one" class="img-responsive round" style="max-height: 160px;">
                                            </div>
                                    <h5 style="text-align: left;">{{$gig->user->name ? substr($gig->user->name,0,20) : 'N/A'}}</h5>
                                    <div class="date_content">
                                      <span class="equal_width">Status: </span><span style="color:#ff7719">Pending </span>
                                    </div>
                                    <div class="date_content">
                                      <span class="equal_width">Offered Price: </span>KSh{{ $gig->price ? round(($gig->price / 100) * 75) : '0' }}
                                    </div>
                                  </a>
                                  <section class="login_FORM summary_deials m_t">
                                    <?php 
                                        $url = base64_encode($gig->gig_id);
                                        $url1 = base64_encode($gig->user_id);

                                        $base64_encode_url = url('business/gig-reminder').'/'.$url.'/'.$url1;  
                                    ?>
                                      <div class="form-group Submit medium_btn " style="margin-bottom: 8px !important; width: 100% !important;">
                                         <a href="{{$base64_encode_url}}">
                                         <input type="button" name="Login" value="Send Reminder" class="btn btn-primary" style="width: 200px !important;">
                                         </a>
                                      </div>
                                   </section>
                                  </div>
                              </div>
                              @endforeach
                                @else
                               <div class="data_found text-center">
                                   <img  width="53px" src="{{url('public/business/images/twiva.png')}}">
                                   <h1 class="text-center" style="font-size: 24px; margin-top: 10px;">No Data Found</h1>
                                </div>
                               @endif
                            </div>
                             {!! $pendingGigDetails->links() !!}
                          </div>
                        </section>
                      </div>
                </div>
    
              </div>
            </div>
       @endsection
       @section('js')


    
      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
   @endsection