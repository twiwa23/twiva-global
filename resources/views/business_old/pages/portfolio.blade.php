@extends("business/layout/web")
@section("title","Portfolio")
@section("content")


<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
<style>
.payments {
  min-height: 668px;
}
.new-file-service.add {
    background-color: #f1f1f1;
    text-align: center;
    display: flex;
    min-height: 414px;
    align-items: center;
    justify-content:center;
}
span.begin {
    margin-top: 23px;
    display: block;
    text-align: center;
}
.pay .col-md-3.col-sm-4 {
    min-height: 758px;
}
.new-file-service.add a {    
    min-height: 526px;
    color:black;
    
}
.text-color ul li{
      min-height: 358px;

}
.new-file-service p{
      min-height: 226px;

}
.alert-success{
  background-color: #ad0e5a;
  color:#fff;
  max-width: 500px;
  margin:0 auto;
  width: 100%;
}
.new-file-service.add figure {
    margin-bottom: 10px;
}
.pluse-add p {
    color: #000;
}
.new-file-service.add.image {
    min-height: 400px;
}
.payments.atrate {
    min-height: 623px;
}
.pluse-add.rate {
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 667px;
   background-color:#f1f1f1;
}.payments.atrate {
    padding-bottom: 0;
}
.pluse-rate figure {
    margin-bottom: 15px;
}

.pluse-add.rate a {
    color: black;
}
 img.new-photo {
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border:3px solid #8a0e49;
}
.over-view view img {
    height: 400px;
}
.over-view video {
    width: 100%;
    height: 400px;
    object-fit: cover;
}
.col-md-4.no-padding-right.col-sm-4 {
    min-height: 418px;
   
}
.login_HEADER.new .Logn-logo img {
    position: absolute;
    right: 0;
    left: 0;
    top: 5px;
    margin: 0 auto;

}
.New-describe-section figure img {
    margin-top: -108px;
    border-radius: 100%;
    border: 7px solid #780c4f;
    width: 193px;
    height: 193px;   
    object-fit: cover;

}

.new-file-service  {
    padding: 13px 11px 11px 12px; 
    text-align: left; 
}

.life-style p {
    min-height: 59px;
}
.new-file-service.file figure img {
    height: 100px;
    width: 100px;
    border-radius: 100%;
    margin: 0 auto;
    text-align: center;
    object-fit: cover;    
    border: 4px solid #931a71;
}
header.login_HEADER.new.creatives .new-button {
    width: 136px;
    height: 48px;
    font-size: 18px;
    font-weight: 600;
    margin-left: 16px;
    margin-top: 0px;
}
header.login_HEADER.new.creatives .new-button.first {
    background-color: #e8eff3;
    color: #63727a;
}
.login_HEADER.new.creatives .toggle {
    float: left;
    margin-top: 0;
}
h3.more {
    margin-top: 14px;
    font-size: 19px;
    color: #000;
}
.text-color ul li {
    min-height: auto;
}
.text-color ul {
    list-style-type: none;
    margin-left: 43px;
    min-height: 368px;
}

.gallary .over-view .new img {
    max-width: 100%;
    height: 400px;
    width: 100%;
    object-fit: cover;
}

.col-md-4.no-padding-right.col-sm-4.current:last-child .overlay {
    height: 99%; 
}

.images-gallery.blog .over-view {
    position: relative;
    min-height: 405px;
    box-shadow: 0px 2px 30px 0px rgba(0, 45, 83, 0.1);
    background-color: #fff
}
.images-gallery.blog .over-view .new {
    padding: 15px;
}
.col-md-4.no-padding-right.col-sm-4.current:last-child .text-center .new-button {
    display: none;
}
/*img.images-color {
    display: none;
}
.video-color {
    display: none;
}*/
.button-find {
    right: 15%;
    bottom: 14%;
    border: 1px solid #b6bfc5;
    padding: 9px 13px;
    color: #b1b3b5;
    font-size: 15px;
    position: absolute;
    z-index: 99999;
    cursor:pointer; 

}
.button-find button {
padding: 4px 16px;
    color: #a9a8a8;
    font-size: 14px;}
    .button-find i {
    margin-right: 12px;
}
.nav-md .container.body .right_col {
    margin-left: 242px;
    padding: 21px 0px 0px;
    background-image: url(../images/banner.png);
    /* padding: 120px 0 0 0; */
    background-repeat: no-repeat;
    background-size: contain;
    background-position: right;
}
.New-describe-section{
       z-index: 9;

}
.portfolio.new .container {
    padding: 0 25px;
}
.service .container {
    padding: 0 25px;
}
.pay .container{
    padding: 0px 38px;

}
.circle{
       z-index: 9;

}
</style>

<body>
     <div class="right_col" role="main" style="min-height: 451px;">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
   <?php 
      $bg_image = $user->background_image ?: url('public/main-page/images/brown-framed-background.jpg');  ?>
   <div class="background-image" id="back-image"  style = "
      background-image: url('{{$bg_image}}');
      padding: 98px 0 144px 0;
      background-repeat: no-repeat;
      background-position: center;
      background-size: cover;
      margin-top: 77px;
      position: relative;
      "
      >
      @include('admin.layouts.notifications')
      <div class="img-goood" style = "visibility: hidden;">
         <h2>@if(!empty($user->name)) {{$user->name}} @else N/A @endif</h2>
         <img src="{{url('public/main-page/images/dots-small.png')}}" class="dots" alt="" >
      </div>
      <input type="file" name="background_image" id="bg-image" style="display:none" />
   </div>
   </div>
   <div class="background-image-new">
      <div class="New-describe-section new-photo ">
         <figure>
            @if(!empty($user->profile))
            <img src="{{$user->profile}}" alt="" />
            @else
            <img src="{{url('public/main-page/images/photo-img.png')}}" alt="">
            @endif
         </figure>
         <h1>@if(!empty($user->name)) {{$user->name}} @else N/A @endif</h1>
         <h2>@if(!empty($category->name)) {{$category->name}} @else N/A @endif</h2>
         <p> @if(!empty($user->description)) {{$user->description}} @else N/A @endif</p>
      </div>
   </div>
   <?php
      ?>
   @if($category->name == "Blogger")
   <div class="portfolio new">
      <div class="container">
         <div class="sub-heading">
            <p>Portfolio</p>
            <h4>My Work</h4>
         </div>
      </div>
      <div class="gallary">
         <div class="container">
            <div class="wrapper">
               <div class="row">
                  <div class="images-gallery blog">
                     <?php $img = 0;?>
                     @foreach($blogger as $bloggers)
                     @if(!empty($bloggers->description))
                     <?php $img++;
                        if($img <= 6){ ?>
                     <div class="col-md-4 no-padding-right col-sm-4 current overlay-container">
                        <div class="over-view">
                           <div class="new"> {!!html_entity_decode(substr($bloggers->description,0,574))!!}</div>
                           <?php
                              if($img == 6){
                               ?>
                           <a href = "{{url('/user/view-blogger/'.$bloggers->user_id)}}">
                              <div class="overlay">
                                 <div class="new-top">
                                    <img src="{{url('public/main-page/images/eye.png')}}" alt="">
                                    <span>View All</span>
                                 </div>
                              </div>
                           </a>
                           <?php } ?>
                           <div class = "text-center">
                              <a href="{{url('/user/view-blogger-detail/'.$bloggers->id)}}" class="new-button">View More</a>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                     @endif
                     <!-- <img src="{{url('public/main-page/images/gallary2.png')}}" alt=""> -->
                     @endforeach
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
   @else
   <div class="portfolio new">
      <div class="container">
         <div class="sub-heading">
            <p>Portfolio</p>
            <h4>My Work</h4>
         </div>
         <div class="new-file">
            <?php $uri = implode("/",Request::segments());
               $img_uri = $uri."?media-type=1";
               $video_uri = $uri."?media-type=2";
               
               $media_type = isset($_GET["media-type"]) ? $_GET["media-type"] : "";
                 ?>
            <div class="camera-img">
               <a href="{{url($img_uri)}}">
               @if($media_type == '1')
               <img src="{{url('public/main-page/images/camera-potofolio.png')}}" alt="">
               @else
               <img src="{{url('public/main-page/images/cam.png')}}" alt="" >
               @endif
               </a>
               <a href="{{url($video_uri)}}">
               @if($media_type == '2')
               <img src="{{url('public/main-page/images/video.png')}}" alt="" >
               @else
               <img src="{{url('public/main-page/images/camera-grey.png')}}" alt="">
               @endif
               </a>
            </div>
         </div>
      </div>
      <div class="gallary">
         <div class="container">
            <div class="wrapper">
               <div class="row">
                  <div class="images-gallery">
                     <?php $img = 0;?>
                     @foreach($portfolio as $portfolios)
                     @if(!empty($portfolios->profile))
                     <?php $img++;
                        if($img <= 6){ 
                        
                         ?>
                     <div class="col-md-4 no-padding-right col-sm-4 current overlay-container">
                        <div class="over-view">
                           <?php 
                              $video_ext = ["mp4","avi","3gp","flv","mov","video"];
                              $get_file = $portfolios->profile;
                              $thumbnail_name = $portfolios->thumbnail_name;
                              
                              $has_video = false;
                              $has_file = false;
                              $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                              $get_file_type = strtolower($get_file_type);
                              
                              if(in_array($get_file_type,$video_ext)){
                                echo '<div class="new"><video src="'.$get_file.'"  controls="" " poster="'.$thumbnail_name.'"></video></div>';
                              }else {
                                echo '<div class="new"><img src="'.$get_file.'" "/></div>';
                              }
                              
                              if($img == 6){
                               ?>
                           <a href = "{{url('/user/view-portfolio/'.$portfolios->user_id)}}{{$media_type ? '?media-type='.$media_type.'' : ''}}">
                              <div class="overlay">
                                 <div class="new-top">
                                    <img src="{{url('public/main-page/images/eye.png')}}" alt="">
                                    <span>View All</span>
                                 </div>
                              </div>
                           </a>
                           <?php } ?>
                        </div>
                     </div>
                     <?php } ?>
                     @endif
                     <!-- <img src="{{url('public/main-page/images/gallary2.png')}}" alt=""> -->
                     @endforeach
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
   @endif
   <div class="service">
   <div class="container">
      <div class="sub-heading">
         <p>Services</p>
         <h4>What I do</h4>
      </div>
      <div class="row">
         @forelse($service as $services)
         <div class="col-md-4 col-sm-4"  style = "margin-bottom: 20px;">
            <div class="new-file-service file">
               <figure class="text-center">
                  @if(!empty($services->profile))      
                  <img src="{{$services->profile}}" alt="" />
                  @else
                  <img src="{{url('public/main-page/images/photo-img.png')}}" alt="">
                  @endif
               </figure>
               <h3 class="text"><span class="begin">@if(!empty($services->title)) {{$services->title}} @else N/A @endif</h3>
               <!-- <h3 class="text"><span class="begin">Lifestyle</span> Photography</h3> -->
               <p>@if(!empty($services->text)) {{$services->text}} @else N/A @endif</p>
               <!-- <div class = "text-center">
                  <a href="javascript:void(0);" class="new-button">View More</a>
                  </div>-->
            </div>
         </div>
         @empty
         @endforelse
         
         <div class="img-contaoner">
            <img src="{{url('public/main-page/images/service-dot.png')}}">
         </div>
      </div>
   </div>
   <div class="pay">
      <div class="container">
         <div class="row">
            <div class="sub-heading">
               <p>My Rate</p>
               <h4>Pricing</h4>
            </div>
            <div class="row">
               @foreach($rate as $rates)
               <div class="col-md-3 col-sm-4">
                  <div class="payments">
                     <div class="circle">
                        <p>KSh</p>
                        <h2>@if(!empty($rates->rate)){{$rates->rate}} @else N/A @endif</h2>
                     </div>
                     <div class="life-style">
                        <p>@if(!empty($rates->title)){{$rates->title}} @else N/A @endif</p>
                     </div>
                     <div class="text-color">
                        <?php 
                           $text = $rates->text;
                             $texts = array();
                           if($text && !empty($text)){
                               $texts = explode("\r\n",$text);
                           }
                           
                           /* echo "<pre>";
                           print_r($texts); die;*/
                           ?>
                        <ul>
                           <?php for($i = 0; $i < count($texts); $i++){
                              if(isset($texts[$i]) && !empty($texts[$i])){
                              
                              ?>
                           <li>{{$texts[$i]}}</li>
                           <?php } } if(count($texts) == 0){ echo "N/A"; } ?>
                        </ul>
                        <a href="{{url('/business/hire-me/'.$user->id)}}" class="new-button">Hire Me</a>
                     </div>
                  </div>
               </div>
               @endforeach
               
            </div>
         </div>
      </div>
   </div>

   @endsection
   @section('js')
   <script type="text/javascript">
      jQuery(document).ready(function($){
          $(window).scroll(function(){
              if ($(this).scrollTop() > 50) {
                  $('#backToTop').fadeIn('slow');
              } else {
                  $('#backToTop').fadeOut('slow');
              }
          });
          $('#backToTop').click(function(){
              $("html, body").animate({ scrollTop: 0 }, 500);
              return false;
          });
      });
   </script>
   @if(!empty($media_type))
   <script>
      $([document.documentElement, document.body]).animate({
          scrollTop: $(".portfolio").offset().top -100
      }, 10);
      
   </script>
   @endif
   <script type="text/javascript">
      $(document).ready(function(){
      // Add smooth scrolling to all links
      $("#nwes").on('click', function(event) {
      
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
      
          // Store hash
          var hash = this.hash;
      
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
      
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });
      });
   </script>
   </script>
   <script>
      setTimeout(function(){
          if($(".alert")){
              $(".alert").remove();
          }
      },5000);
      
      
   </script>
   <script type="text/javascript">
      $(document).ready(function(){
        $("#update-bg-image").click(function(){
            console.log(1)
          $("#bg-image").click();
        })
        $("#bg-image").change(function(event){
            let file = event.target.files[0];
      
            if (file) {
                    var type = file.type;
      
                    if (type == "image/png" || type == "image/jpeg" || type == "image/jpeg") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#back-image').css('background-image', "url('"+ e.target.result+"')");
                    }
                    reader.readAsDataURL(file);
      
                      var formData = new FormData();
                      formData.append("image",file)
                      formData.append("_token","{{csrf_token()}}")
      
                        $.ajax({
                          url: "",
                          type: "POST",
                          data: formData,
                          success: function (msg) {
                           console.log(msg)
                          },
                          cache: false,
                          contentType: false,
                          processData: false
                        });
      
                  }else {
                        $('#back-image').css('background-image', "url('{{$bg_image}}')");
      
                    alert("Please upload jpg, jpeg, png format image only.");
                    return false;
                  }
              }
        })
      })
   </script>
   @endsection