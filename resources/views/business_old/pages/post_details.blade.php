@extends("business/layout/web")
@section("title","Post Details")
@section("content")
<style type="text/css">
.video-containt{
   width: 90px;
   height: 90px;
   border-radius: 5px;
   margin-right: 20px;
  }
  section.login_FORM .btn-primary{
   font-size: 17px;
  }
  .theme_photos ul li img {
    cursor: pointer;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
   <div class="col-sm-12">
      <div class="about-us">
         <section class="align_btns login_FORM">
            <div class="form-group Submit medium_btn">
               <a href="{{url('business/accept-post',base64_encode($post_details->id))}}">
               <input type="Submit" name="Login" value="ACCEPTED INVITES" class="btn btn-primary">
               </a>
            </div>
            <div class="form-group Submit medium_btn">
               <a href="{{url('business/pending-post-list',base64_encode($post_details->id))}}">
               <input type="Submit" name="Login" value="PENDING INVITES" class="btn btn-primary">
               </a>
            </div>
            <div class="form-group Submit medium_btn">
               <a href="{{url('business/declined-post',base64_encode($post_details->id))}}">
               <input type="Submit" name="Login" value="DECLINED INVITES" class="btn btn-primary">
               </a>
            </div>
         </section>
         <h2 class="color-text">Post Details</h2>
         <section class="uploaded-data common-struct login_FORM login-input">
            <div class="white_bg_sec common_white_bg">
               <div class="labels">
                  <h5>Title</h5>
                  <p style="word-break: break-all;">{{$post_details->post_name ?? 'N/A'}}</p>
               </div>
               <div class="labels">
                  <h5>Description</h5>
                  <p style="word-break: break-all;">{{$post_details->description ?? 'N/A'}}</p>
               </div>
               <div class="labels">
                  <h5>What To Do</h5>
                  <p style="word-break: break-all;">{{$post_details->what_to_include ?? 'N/A'}}</p>
               </div>
               <div class="labels">
                  <h5>What Not To Do</h5>
                  <p style="word-break: break-all;">{{$post_details->what_not_to_include ?? 'N/A'}}</p>
               </div>
               <div class="labels">
                  <h5>Expiration Date</h5>
                  <p>{{$post_details->expiration_date ?? 'N/A'}}</p>
               </div>
               <div class="labels">
                  <h5>Other Requirement</h5>
                  <p>{{$post_details->other_requirement ?? 'N/A'}}</p>
               </div>
               <div class="labels">
                  <h5>Theme Photo</h5>
                  <div class="theme_photos">
                     <ul>
                        @if(!empty($post_details->images))                                  
                        <?php $videos = $All_files = "";
                           $no_doc = 0;
                           $total = 0;
                           $video_ext = ["mp4","avi","3gp","flv","mov","video"]; ?>
                        @forelse($post_details->images as $images)
                        <li>
                           <?php
                              $total++;
                              $get_file = $images->media;
                              $has_video = false;
                              $has_file = false;
                              $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                              $get_file_type = strtolower($get_file_type);
                              if(in_array($get_file_type,$video_ext)){
                              echo '<video src="'.$get_file.'" alt="upload-one" controls="" class="img-responsive video-containt video" target="_blank" title="Click to expand image/video"></video>';
                            }else {
                              echo '<img src="'.$get_file.'" " alt="upload-one" class="img-responsive image" target="_blank" title="Click to expand image/video"/>';
                            }
                              ?>    
                        </li>
                        @empty
                        N/A
                        @endforelse
                        @else
                        N/A
                        @endif
                     </ul>
                  </div>
               </div>
               <div class="labels">
                  <h5>HashTag</h5>
                  <p style="padding-bottom: 20px;">@if($post_details->hashtag) #{{$post_details->hashtag}} @else N/A @endif</p>
               </div>
            </div>
         </section>
                 <div class="modal fade bs-example-modal-lg view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <img src="" alt="woman"  class="target_img">
                              </div>
                              <div class="cross_icon" data-dismiss="modal">
                                <a href="">
                                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade bs-example-modal-lg1 view_img_video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <video src="" alt="upload-one" class="target_vidoe" controls="" style = "width:100%;" title="Click to expand image"></video>
                              </div>
                              <div class="cross_icon" data-dismiss="modal">
                                <a href="">
                                  <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                </a>
                              </div>
                            </div>
                          </div>
                        </div> 
      </div>
   </div>
</div>
@endsection
@section('js')
 <script type="text/javascript" src="{{url('public/business/js/jquery.mousewheel.pack.js')}}"></script>

  <!-- Add fancyBox main JS and CSS files -->
  <script type="text/javascript" src="{{url('public/business/js/jquery.fancybox.pack.js')}}"></script>
      <!-- <script src="js/bootstrap-lightbox.min.js"></script> -->
     
      <script type="text/javascript">
        $('.image_video').click(function(){
          $('.bs-example-modal-lg').modal({
            backdrop: 'static'
          });
        }); 
        $('.image_video1').click(function(){
          $('.bs-example-modal-lg1').modal({
            backdrop: 'static'
          });
        }); 

        $( document ).ready(function() {
          $('.fancybox').fancybox();
  console.log( "document ready!" );
  // $('#myLightbox').lightbox();

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>


      <script type="text/javascript">
        
        $('.image').click(function(){
         let img = $(this).attr('src')
         $(".target_img").attr('src',img)
          $('.bs-example-modal-lg').modal({
            backdrop: 'static'
          });
        }); 
         $('.video').click(function(){
         let video = $(this).attr('src')
         $(".target_vidoe").attr('src',video)
          $('.bs-example-modal-lg1').modal({
            backdrop: 'static'
          });
        }); 
      </script>
<!-- <script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script> -->
@endsection