@extends("business/layout/web")
@section("title","Change Password")
@section("content")
<style>
.error {
  color: red;
}
</style>
<div class="right_col bg_banner" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Change Password</h2>
   <ul class="tabs" style="visibility: hidden;">
      <li class="first">Create</li>
      <li class="second">On Going</li>
      <li class="third">Completed</li>
   </ul>
   <div class="hashtags create_post_input">
     <form method="POST" id="form_validate" novalidate="novalidate" >
          {{csrf_field()}}
         <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
          @include('business.includes.notifications')
         <div class="form-group change_icon">
            <div class="label_addres">
               <label for="">
               Old Password
               </label>

            </div>
            <div class="email" style="position: relative;">
               <i class="fa lock_icon">
               <img src="{{url('public/business/images/lock1.png')}}">
               </i>
              <input type="password" name="old_password" id="old_password"  class="form-control" placeholder = "" > 
                        <span class="text-danger"></span>
                       <label id="old_password-error" class="error" for="old_password">{{$errors->first('old_password')}}</label>
            </div>
         </div>
         <div class="form-group change_icon">
            <div class="label_addres">
               <label for="">
               New Password
               </label>
            </div>
            <div class="email" style="position: relative;">
               <i class="fa lock_icon">
               <img src="{{url('public/business/images/lock1.png')}}">
               </i>
              <input type="password" name="new_password" id="new_password"  class="form-control" placeholder = "" > 
                        <span class="text-danger"></span>
                       <label id="new_password-error" class="error" for="new_password">{{$errors->first('new_password')}}</label>
            </div>
         </div>
         <div class="form-group change_icon">
            <div class="label_addres">
               <label for="">
               Confirm Password
               </label>
            </div>
            <div class="email" style="position: relative;">
               <i class="fa lock_icon">
               <img src="{{url('public/business/images/lock1.png')}}">
               </i>
               <input type="password" name="confirm_password" id="confirm_password"  class="form-control" placeholder = "" > 
                        <span class="text-danger"></span>
                       <label id="confirm_password-error" class="error" for="confirm_password">{{$errors->first('confirm_password')}}</label>
            </div>
         </div>
      
       <div class="space-top top">
        <input type = "submit" name = "submit" value = "UPDATE" class="button"/>
      </div>
      </form>
   </div>
</div>
@endsection
@section('js')
 <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});
</script>
<script>  
   jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
          }, "Space not allowed");    
  $('#form_validate').validate({ 
      rules : {
                    old_password  : {
                      required : true,
                       noSpace:true
                    },
                    new_password :  {
                      required : true,
                      minlength : 6,
                       noSpace:true
                    },
                    confirm_password : {
                      required : true,
                      minlength : 6,
                       noSpace:true,
                      equalTo : "#new_password"
                    },
                },

       messages : {
                      old_password :  {
                        required : "Please enter old password."
                      },
                      new_password :  {
                        required : "Please enter new password.",
                        minlength : "New password should be atleast 6 characters long."  
                      },
                      confirm_password : {
                        required : "Please enter confirm password.",
                        minlength : "New password and confirm password must be same." ,
                        equalTo : "New password and confirm password must be same."
                      }, 
                },

  });
</script>

<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>

   
@endsection