<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" href="{{url('public/business/images/fav.png')}}" type="image/x-icon">
      <title>Reset Password</title>
      <link rel="icon" href="{{url('public/business/images/fav.png')}}" type="image/x-icon">
      <link href="{{url('public/business/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{url('public/business/css/font-awesome.min.css')}}" rel="stylesheet">
      <link href="{{url('public/business/css/daterangepicker.css')}}" rel="stylesheet">
      <link href="{{url('public/business/css/style.css')}}" rel="stylesheet">
   </head>
   <style>
   .error {
    color: red;
   }
   </style>
   <body>
      <div class="main_container main_bg">
         <div class="top_nav">
            <div class="nav_menu header_contain">
               <nav>
                  <div class="institute_logo">
                     <a href="{{url('business/login')}}">
                     <img src="{{url('public/business/images/logo.png')}}">
                     </a>
                  </div>
               </nav>
            </div>
         </div>
         <section class="login_FORM login-input contain_banner about-us">
            <div class="container">
               <div class="col-sm-12">
                  <h2 class="forgot_icon"> 
                    <!--  <a href="{{url('business/login')}}">
                     <img src="{{url('public/business/images/back_icon.png')}}" alt="back_icon">
                     </a> -->
					  @include('business.includes.notifications')
                     Reset Password
                  </h2>
                  <form method="POST" id="form_validate" novalidate="novalidate">
				    {{csrf_field()}}
                     <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
                      <div class="label_addres">
                        <label for="">
                        New Password
                        </label>
                     </div>
                       <div class="form-group">
                        <div class="password">
                           <i class="fa lock_icon">
                           <img src="{{url('public/business/images/lock1.png')}}">
                           </i>
                          <input type="password" name="password" id="password"  class="form-control" placeholder = "" > 
                        <span class="text-danger"></span>
                         </div>
                       <label id="password-error" class="error" for="password">{{$errors->first('password')}}</label>
                        </div>
					     <div class="label_addres">
                        <label for="">
                        Confirm Password
                        </label>
                     </div>
                       <div class="form-group">
                        <div class="password">
                           <i class="fa lock_icon">
                           <img src="{{url('public/business/images/lock1.png')}}">
                           </i>
                          <input type="password" name="confirm_password" class="form-control" placeholder = "" >
                        <span class="text-danger"></span>
                         </div>
                        <label id="confirm_password-error" class="error" for="confirm_password">{{$errors->first('confirm_password')}}</label>
                        </div>
                     <div class="form-group Submit">
                        <input type="SUBMIT" name="SUBMIT" value="SUBMIT" class="btn btn-primary" style="margin-top: 28px; margin-bottom: 160px;">
                     </div>
               </div>
               <div class="form-group trouble_text">
               </div>
               </form>
            </div>
      </div>
      </section>
      
      <section class="footer">
         <div class="container footer_contain">
            <div class="col-md-6 col-xs-4">
               <div class="new-file">
                  <a href="{{url('business/login')}}">
                     <figure>
                        <img src="{{url('public/business/images/logo-footer.png')}}" alt="">
                     </figure>
                  </a>
                  <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
               </div>
            </div>
            <div class="col-md-3 col-xs-4">
               <div class="new-good">
                  <h2>Useful Links</h2>
                  <ul>
                     <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
                  </ul>
               </div>
            </div>
            <div class="col-md-3 col-xs-4">
               <div class="new-good gone social">
                  <h2>Contact Us</h2>
                  <ul>
                     <li>
                        <!-- <p>info@twiva.com</p> -->
                        <a href="mailto:info@twiva.co.ke">
                           <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>
                           info@twiva.co.ke
                        </a>
                     </li>
                     <br>
                     <li>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        &nbsp; 0708088114
                     </li>
                  </ul>
               </div>
               <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Facebook"><a href="https://www.facebook.com/twiva/" target="_blank">
                        <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon"/></a>
                     </li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Twitter">
                        <a href="https://twitter.com/Twiva_influence" target="_blank">
                        <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon"/></a>
                     </li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Instagram">
                        <a href="https://www.instagram.com/twiva_influence/" target="_blank">
                        <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon"/></a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      </div>
      <script src="{{url('public/business/js/jquery.min.js')}}"></script>
      <!-- Bootstrap -->
      <script src="{{url('public/business/js/bootstrap.min.js')}}"></script>
      <script src="{{url('public/business/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
      <script src="{{url('public/business/js/custom.min.js')}}"></script>
      <script src="{{url('public/business/js/custom.js')}}"></script>
	  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});
</script>
<script>

     jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
          }, "Space not allowed");    

     /*$(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });*/
     $('#form_validate').validate({ 
      rules: {
        
          password: {
              required: true,
              minlength:6,
              noSpace:true,
            
          },
          confirm_password: {
              required: true,
              minlength:6,
              equalTo: "#password",
              noSpace:true,
          },
          
      },

        messages:{
         
          password:{  
            required:'Please enter new password.',
            minlength:'New password must be at least 6 characters long.'
           
          },

         

          confirm_password:{  
            required:'Please enter confirm password.',
            equalTo:'New password and confirm password must be same.',
            minlength : "New password and confirm password must be same."
          }, 

         
        },
        });    
   </script>
   </body>
</html>