<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" href="{{url('public/business/images/fav.png')}}" type="image/x-icon">
      <title>Business SignUp</title>
      <link rel="icon" href="{{url('public/business/images/fav.png')}}" type="image/x-icon">
      <link href="{{url('public/business/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{url('public/business/css/font-awesome.min.css')}}" rel="stylesheet">
      <link href="{{url('public/business/css/daterangepicker.css')}}" rel="stylesheet">
      <link href="{{url('public/business/css/style.css')}}" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/css/intlTelInput.css" integrity="sha256-rTKxJIIHupH7lFo30458ner8uoSSRYciA0gttCkw1JE=" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/css/intlTelInput.css" rel="stylesheet" />
   </head>
  
       <style>
         .words {
    color: #686868 !important;
    text-align: right;
    font-size: 12px;
    margin-top: 3px;
  }
        .common-struct .Update-img img {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
}
.error {
  color: red;
}
.common-btn a {
      color: #fff;
    background-color: #00b3a7;
    border-color: #00b3a7;
    height: 55px;
    font-size: 22px;
    font-weight: 500;
    width: 72%;
    text-transform: uppercase;
    padding-top: 13px;
}
label#country_name-error {
    margin-top: 16px;
}
.common-btn a:hover {
  background-color: #02cabc;
    border-color: #02cabc;
}

.selected-flag{
   padding-left: 26px !important;
}
.selected-dial-code{
padding-left: 28px !important;
}
.iti-arrow{
   right:-10px !important;
}
.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-2 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-2 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-2 input[type=tel] {
    padding-left: 90px;
}

.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-3 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-3 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-3 input[type=tel]{
       padding-left: 100px;
}
.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-4 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-4 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-4 input[type=tel]{
   padding-left: 108px;
}
.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-5 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-5 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-5 input[type=tel]{
   padding-left: 115px;
}
.signup_text .form-group.select_input {
  margin-top: 3px;
}
.signup_text .form-group select {
    width: 100%;
    border-bottom: 2px solid rgb(199, 200, 199) !important;
    margin-left: -19px;

    padding-left: 27px !important;

}
.signup_text  select {
      background-position: 99% 61% !important;
}
.Orther_field {
    display: inline-block;
    width: 100%;
}
  </style>
  
   <body>
      <div class="main_container main_bg">
         <div class="top_nav">
            <div class="nav_menu header_contain">
               <nav>
                  <div class="institute_logo">
                     <a href="{{url('business/login')}}">
                     <img src="{{url('public/business/images/logo.png')}}">
                     </a>
                  </div>
               </nav>
            </div>
         </div>
         <section class="login_FORM login-input signup_text contain_banner country_code1 about-us">
            <div class="container">
               <div class="col-sm-12">
                  <h2 class="forgot_icon" style="margin-bottom: 20px;">
                     <a href="{{url('business/login')}}">
                     <img src="{{url('public/business/images/back_icon.png')}}" alt="back_icon">
                     </a>
                     Business SignUp
                  </h2>
                   <form method="post" id="form_validate" enctype="multipart/form-data" novalidate="novalidate" class="common-struct" onsubmit="return validateForm()"> 
                     {{csrf_field()}}
                     <div style="margin-bottom: 34px">
                        <div class="Update-img text-center ">
                           @php($url =  url('public/business/images/upload-one.png'))
                         <img  title="Click here to upload image"
                           onclick="$('#imgInp').click()" src='{{$url}}' id="blah" alt="upload-one" class="img-responsive"/>
                        <input style="display:none;" type="file" id="imgInp" name="profile" data-role="magic-overlay"
                             data-target="#pictureBtn" value="" alt="upload-one" class="img-responsive">
                        </div>

                         <div style="display:none; color: #ff0000 !important;width: 100%;text-align: center;     margin-top: 5px;" class="text-danger" id="invalid_file">Please select jpg, jpeg or png image format only. </div>
                         <span class = "error">{{$errors->first('profile')}}</span>
                     </div>
                     <div class="label_addres">
                        <label for="">
                        Email Address
                        </label>
                     </div>
                     <div class="form-group">
                        <div class="email">
                           <i class="fa">
                           <img src="{{url('public/business/images/inbox.png')}}">
                           </i>
                          <input type="text" id="email" name="email" value = "{{old('email')}}" class="form-control" placeholder = "" >
                           <span class="text-danger"></span>
                        </div>
                          <label id="email-error" class="error" for="email">{{$errors->first('email')}}</label>
                     </div>
                     <div class="label_addres">
                        <label for="">
                        Company Name
                        </label>
                     </div>
                     <div class="form-group">
                        <div class="email">
                           <i class="fa lock_icon">
                           <img src="{{url('public/business/images/building.png')}}">
                           </i>
                            <input type="text" name="company_name" value ="{{old('company_name')}}" class="form-control" required>
                        </div>
                        <label id="company_name-error" class="error" for="company_name">{{$errors->first('company_name')}}</label>
                     </div>
                     <div class="input_feilds">
                        <div class="input_style">
                           <div class="label_addres">
                              <label for="">
                              Password
                              </label>
                           </div>
                           <div class="form-group">
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/lock1.png')}}">
                                 </i>
                                  <input type="password" name="password" id="password"  class="form-control" placeholder = "" > 
                                 <span class="text-danger"></span>
                              </div>
                               <label id="password-error" class="error" for="password">{{$errors->first('password')}}</label>
                           </div>
                        </div>
                        <div class="input_style">
                           <div class="label_addres">
                              <label for="">
                              Confirm Password
                              </label>
                           </div>
                           <div class="form-group">
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/lock1.png')}}">
                                 </i>
                                <input type="password" name="confirm_password" class="form-control" placeholder = "" >
                                 <span class="text-danger"></span>
                              </div>
                              <label id="confirm_password-error" class="error" for="confirm_password">{{$errors->first('confirm_password')}}</label>
                           </div>
                        </div>
                     </div>
                     <div class="input_feilds">
                        <div class="input_style">
                           <div class="label_addres">
                              <label for="">
                              Contact Person Name
                              </label>
                           </div>
                           <div class="form-group">
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/asd.png')}}"></i>
                                 <input type="text" id="contact_person_name" name="contact_person_name" value = "{{old('contact_person_name')}}"  class="form-control" placeholder="">
                                 <span class="text-danger"></span>
                              </div>
                               <label id="contact_person_name-error" class="error" for="contact_person_name">{{$errors->first('contact_person_name')}}</label>
                           </div>
                        </div>
                        <div class="input_style">
                           <div class="label_addres">
                              <label for="">
                              Business Phone Number
                              </label>
                           </div>
                           <div class="form-group" >
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/tel.png')}}"></i>
                               <input type="text" name="contact_phone_number" id="contact_phone_number" onkeypress="return isNumberKey(event)" value = "{{old('contact_phone_number')}}" class="form-control" placeholder = "">
                                 <span class="text-danger"></span>
                                  <label id="contact_phone_number-error" class="error" for="contact_phone_number">{{$errors->first('contact_phone_number')}}</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="input_feilds" style="margin-bottom: 30px;">
                        <div class="input_style">
                           <div class="label_addres">
                              <label for="">
                              Country Name
                              </label>
                           </div>
                           <div class="form-group select_input">
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/glob.png')}}">
                                 </i>
                                 <select class="form-control" name="country_name">
                                    <option value="">Select Country</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="DR Congo">DR Congo</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="South Sudan">South Sudan</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Libya">Libya</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Eswatini">Eswatini</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Cabo Verde">Cabo Verde</option>
                                    <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                    <option value="Seychelles">Seychelles</option>
                                  </select>
                                 <span class="text-danger"></span>
                              </div>
                           </div>
                              <label id="country_name-error" class="error" for="country_name">{{$errors->first('country_name')}}</label>
                        </div>
                        <div class="input_style">
                           <div class="label_addres">
                              <label for="">
                              Alternative Phone Number
                              </label>
                           </div>
                           <div class="form-group">
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/phone.png')}}">
                                 </i>
                                 <input type="text" name="register_phone_number" id="register_phone_number" onkeypress="return isNumberKey(event)" value = "{{old('register_phone_number')}}" class="form-control" placeholder = "">
                                 <span class="text-danger"></span>
                              </div>
                               <label id="register_phone_number-error" class="error" for="register_phone_number">{{$errors->first('register_phone_number')}}</label>
                           </div>
                        </div>
                     </div>
                     <div class="label_addres">
                        <label for="">
                        Description (Write something about your company….)
                        </label>
                     </div>
                     <div class="form-group">
                        <div class="email">
                           <i class="fa lock_icon" style="top: 6px;">
                           <img src="{{url('public/business/images/buil-first.png')}}">
                           </i>
                           <textarea type="text" name="business_detail" maxlength="1000" id="" rows="3" value = "" class="form-control" placeholder="">{{old('business_detail')}}</textarea>
                             <!-- <div class="words">Maximum limit 1000 characters</div> -->
                           <span class="text-danger"></span>
                        </div>

                        <label id="business_detail-error" class="error" for="business_detail">{{$errors->first('business_detail')}}</label>
                     </div>
                     <div class="label_addres">
                        <label for="">
                        Business Address
                        </label>
                     </div>
                     <div class="form-group">
                        <div class="email">
                           <i class="fa lock_icon" style="top: 6px;">
                           <img src="{{url('public/business/images/locatiion.png')}}">
                           </i>
                           <textarea type="text" name="business_address"  maxlength="1000"  id="" rows="3"class="form-control" placeholder="" style = "color:#171717">{{old('business_address')}}</textarea>
                           <!-- <div class="words">Maximum limit 1000 characters</div> -->
                           <span class="text-danger"></span>
                        </div>
                         <label id="business_address-error" class="error" for="business_address">{{$errors->first('business_address')}}</label>
                     </div>
                     <div class="form-group" style="margin-bottom: 45px!important;">
                     <div class="input_feilds" >
                        <div class="input_style" style="width: 100%;">
                           <div class="label_addres">
                              <label for="">
                              How did you discover Twiva?
                              </label>
                           </div>
                           <div class="form-group select_input" style="width: 100%;">
                              <div class="email">
                                 <i class="fa lock_icon">
                                 <img src="{{url('public/business/images/glob.png')}}">
                                 </i>
                                 <select id="discover_twiva" class="form-control" name="discover_twiva">
                                   <!--  <option value="">how did you discover Twiva?</option> -->
                                    <option value="Search engine(Google, etc)">Search engine(Google, etc)</option>
                                    <option value="Social media">Social media</option>
                                    <option value="Referral">Referral</option>
                                    <option value="KEPSA">KEPSA</option>
                                    <option value="Other">Other</option>
                                  </select>
                                 <span class="text-danger"></span>
                              </div>
                           </div>
                              <label id="discover_twiva-error" class="error" for="discover_twiva">{{$errors->first('discover_twiva')}}</label>
                        </div>
                      </div>
                    </div>
                    <div class="Orther_field">
                      <div class="label_addres">
                        <label for="">
                        Other
                        </label>
                     </div>
                     <div class="form-group">
                        <div class="email">
                           <i class="fa lock_icon">
                           <img src="{{url('public/business/images/building.png')}}">
                           </i>
                            <input type="text" name="other" value ="{{old('other')}}" class="form-control">
                        </div>
                        <label id="other-error" class="error" for="other">{{$errors->first('other')}}</label>
                     </div>
                   </div>
                     <div class="form-group Submit SignUp ">
                        <input type="submit" name="submit" value="SIGNUP" class="btn btn-primary">
                     </div>
                  </form>
               </div>
            </div>
         </section>
         <!-- Modal -->
         <div class="modal fade modal_heading" id="exampleModalCenter23" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">
                  <div class="modal-body">
                     <h6 class="modal-title" id="exampleModalLongTitle">Congratulations!</h6>
                     <p>
                        You have successfully joined the Twiva platform! Please click on the confirmation link we've sent you before signing-in!
                     </p>
                     <div class="common-btn">
                        <a href="{{url('business/login')}}" class="btn btn-primary">OKAY</a>
                     
                     </div>
                  </div>
                  <div class="cross_icon">
                     <a href="{{url('business/login')}}">
                     <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <section class="footer">
            <div class="container footer_contain">
               <div class="col-md-6 col-xs-4">
                  <div class="new-file">
                     <a href="{{url('business/login')}}">
                        <figure>
                           <img src="{{url('public/business/images/logo-footer.png')}}" alt="">
                        </figure>
                     </a>
                     <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                  </div>
               </div>
               <div class="col-md-3 col-xs-4">
                  <div class="new-good">
                     <h2>Useful Links</h2>
                     <ul>
                        <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-xs-4">
                  <div class="new-good gone social">
                     <h2>Contact Us</h2>
                     <ul>
                        <li>
                           <!-- <p>info@twiva.com</p> -->
                           <a href="mailto:info@twiva.co.ke">
                              <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>
                              info@twiva.co.ke
                           </a>
                        </li>
                        <br>
                        <li>
                           <i class="fa fa-phone" aria-hidden="true"></i>
                           &nbsp; 0708088114
                        </li>
                     </ul>
                  </div>
                  <div class="new-good gone">
                     <h2>Social Media</h2>
                     <ul>
                        <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Facebook"><a href="https://www.facebook.com/twiva/" target="_blank">
                           <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon"/></a>
                        </li>
                        <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Twitter">
                           <a href="https://twitter.com/Twiva_influence" target="_blank">
                           <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon"/></a>
                        </li>
                        <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Instagram">
                           <a href="https://www.instagram.com/twiva_influence/" target="_blank">
                           <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon"/></a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <script src="{{url('public/business/js/jquery.min.js')}}"></script>
      <!-- Bootstrap -->
      <script src="{{url('public/business/js/bootstrap.min.js')}}"></script>
      <script src="{{url('public/business/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
      <script src="{{url('public/business/js/custom.min.js')}}"></script>
      <script src="{{url('public/business/js/custom.js')}}"></script>

    @if(Session::has('success'))
       
       <script type="text/javascript">
            $('#exampleModalCenter23').modal("show")
       </script>
    @endif
    
   
      
      <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });

    </script>
     <script>
     function validateForm(){
      $("#invalid_file").hide();
      if($("input[name='profile']").val() == ""){
        $("#invalid_file").show().text("Please upload the business logo.");
        return false;
      }
    }    
   </script>
 <script language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }

       //-->
    </script>

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});
</script>
<script>

  


   jQuery.validator.addMethod("valid_email", function(value, element) {
        if(value.indexOf(".") >= 0 ){
          return true;
        }else {
          return false;
        }
    }, "Please enter a valid email address");


    jQuery.validator.addMethod("noSpace", function(value, element) {
           return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
          }, "Space not allowed");



  $('#form_validate').validate({ 
      rules: {
          email: {
              required: true,
              email:true,
              maxlength:200,
              valid_email:true,
              remote:"{{url('business/check-exist-email')}}"
          },
           
          password: {
              required: true,
              minlength:6,
              maxlength:30,
               noSpace:true
          },
          confirm_password: {
              required: true,
              minlength:6,
               noSpace:true,
              equalTo: "#password",
          },
          company_name: {
              required: true,
              maxlength:100,
              minlength:3,
              noSpace : true
          },
           contact_person_name: {
              required: true,
              maxlength:100,
              minlength:3,
             
          },
          contact_phone_number: {
              required: true,
              minlength:8,
              digits : true,
              maxlength:15
             
          },
          register_phone_number: {
              digits : true,
              minlength:8,
              maxlength:15
          },
           country_name: {
              required: true,
             
          },
           business_detail: {
              required: true,
              minlength:3,
              maxlength:1000
          },
           business_address: {
              required: true,
              minlength:3,
              maxlength:1000
          }
      },

        messages:{
          company_name:{  
            required:'Please enter company name.',
            minlength:'Company name must be at least 3 characters long.',
            maxlength:'Company name maximum 100 characters long.'
          },
          contact_person_name:{  
            required:'Please enter contact person name.',
            minlength:'Contact person name must be at least 3 characters long.',
            maxlength:'Contact person name maximum 100 characters long.'
          },
          email:{  
            required:'Please enter email address.',
            email:'Please enter a valid email address.',
            maxlength:'Email address maximum 200 characters long.'
          },
          password:{  
            required:'Please enter password.',
            minlength:'Password must be at least 6 characters long.',
            maxlength:'Password maximum 10 characters.'
          },

          business_detail:{  
            required:'Please enter description.',
            minlength:'Description must be at least 3 characters long.',
            maxlength:'Description maximum 1000 characters long.'
          },


          business_address:{  
            required:'Please enter business address.',
            minlength:'Business address must be at least 3 characters long.',
            maxlength:'Business address maximum 1000 characters long.'
          },


          confirm_password:{  
            required:'Please enter confirm password.',
            equalTo:'Password and confirm password must be same.',
            minlength :"Password and confirm password must be same."
          }, 

          register_phone_number:{  
            required:'Please enter alternative phone number.',
            minlength:'Alternative phone number must be at least 8 digits.',
            maxlength:'Alternative phone number  maximum 15 digits.',
            digits:'Invalid phone number.',
          }, 
          contact_phone_number:{  
            required:'Please enter business phone number.',
            minlength:'Business phone number must be at least 8 digits.',
            maxlength:'Business phone number  maximum 15 digits.',
            digits:'Invalid phone number.',
          }, 
          country_name:{  
             required:'Please select country name.',
            
           
          }          
        },
        submitHandler:function(form){

                  if($("#email-error").text().length > 0){
                    return false;
                  }else {
                    form.submit();
                  }
          }
        /*errorHandler:function(e){
          console.log(e)
        }*/
       
    
  });
</script>
<script type="text/javascript">
  $(".Orther_field").hide()
  $("#discover_twiva").change(function() {
    let val = $("#discover_twiva option:selected" ).val();
    if(val == 'Other') {
      $(".Orther_field").show()
    } else {
      $(".Orther_field").hide()
    }
    
  })
</script>

   </body>
</html>