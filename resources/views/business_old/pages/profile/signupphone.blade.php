<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://twiva.co.ke/public/landing/images/fav.png" type="image/x-icon">
    <title>Business SignUp</title>
    <link rel="icon" href="images/fav.png" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/daterangepicker.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  </head>
  <body>
    <div class="main_container main_bg">

      <div class="top_nav">
        <div class="nav_menu header_contain">
          <nav>
            <div class="institute_logo">
              <a href="login.html">
                <img src="images/logo.png">
              </a>
            </div>
           <!--  <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div> -->
          </nav>
        </div>
      </div>
      <section class="login_FORM login-input contain_banner">
        <div class="container">
          <div class="col-sm-12">
            <h2 class="forgot_icon">
              Business SignUp
              <a href="signup.html">
                <img src="images/back_icon.png" alt="back_icon">
              </a>
            </h2>
            <form method="POST" id="form_validate" novalidate="novalidate"> 
              <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">

              <div class="form-group">
                <div class="email">
                  <i class="fa lock_icon">
                    <img src="images/phone.png">
                  </i>
                  <input type="text" id="email" name="email" value="" class="form-control" placeholder="Registered Phone Number">

                  <span class="text-danger"></span>
                </div>
              </div>

              <div class="form-group Submit SignUp">
                <input type="button" name="Login" value="SIGNUP" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter23">
              </div>

            </form>
          </div>
        </div>
      </section>
      <!-- Modal -->
      <div class="modal fade modal_heading" id="exampleModalCenter23" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <h6 class="modal-title" id="exampleModalLongTitle">Congratulations!</h6>
              <p>
                You have successfully joined the Twiva platform! Please click on the confirmation link we've sent you before signing-in!
              </p>
              <div class="common-btn">
                <a href="influ_list.html">
                <input type="button" name="Login" value="OKAY" class="btn btn-primary">
                  
                </a>
              </div>
            </div>
            <div class="cross_icon">
              <a href="login.html">
                <img src="images/white_cross_icon.png" alt="white_cross_icon">   
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
       <section class="footer">
         <div class="container footer_contain">
         <div class="col-md-6 col-xs-4">
            <div class="new-file">
              <a href="influ_list.html">
                 <figure>
                    <img src="https://twiva.co.ke/public/landing/images/logo-footer.png" alt="">
                 </figure>
                
              </a>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="https://twiva.co.ke/contact-us">Contact Us</a></li>
                  <li><a href="https://twiva.co.ke/policy">Privacy Policy</a></li>
                  <li><a href="https://twiva.co.ke/terms-conditions">Terms and Conditions</a></li>
                  <li><a href="https://twiva.co.ke/faq">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                   
                     <!-- <p>info@twiva.com</p> -->
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
               <br>
                  <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>
            </div>
             <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Facebook"><a href="https://www.facebook.com/twiva/" target="_blank"><img src="images/fb_icon.png" alt="fb_icon"/></a></li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Twitter"><a href="https://twitter.com/Twiva_influence" target="_blank"><img src="images/twitter_icon.png" alt="twitter_icon"/></a></li>
                     <li class="red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Instagram"><a href="https://www.instagram.com/twiva_influence/" target="_blank"><img src="images/insta_icon.png" alt="insta_icon"/></a></li>
                  </ul>
               </div>
         </div>
      </div></section>
    </div>
<script src="js/jquery.min.js"></script>
      <!-- Bootstrap -->
      <script src="js/bootstrap.min.js"></script>
      <script src="bootstrap-daterangepicker/daterangepicker.js"></script>
      <script src="js/custom.min.js"></script>
      <script src="js/custom.js"></script>
  </body>
  </html>
