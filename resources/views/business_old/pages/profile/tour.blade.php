@extends("admin/layout/admin_layout")
@section("title","Add Tour Guide")
@section('content')

<style type="text/css">
   div#checkboxes {
   display: flex;
   }
   .form-group.jack {
   margin-left: 20px;
   }
   .row.homewer .col-md-4 {
   margin-bottom: 16px;
   }
   .intl-tel-input {
    position: relative;
    display: inline-block;
    width: 100%;
}

.text-danger {
    color: red !important; 
}

label#phone1-error,label#name-error, label#region-error, label#date-error, label#educational_background-error, label#vehicle_number-error, label#certifications-error, label#minimum_passengers-error, label#maximum_passengers-error,label#experience-error,label#price_per_km_us-error,label#price_per_km_kenya-error,label#no_of_safari-error,label#vehicle_type-error{
    margin-top: 2px;
    color: red;
    font-size: 14px;
}
.text-new.add img {
   margin-bottom: 4px;
    object-fit: contain;
    border: 2px solid #000;
    background-color: #ccc;
}

.text-new {
    text-align: left!important;
}

}
img.vehicle_added_image {
    height: 100px;
    width: 100px;
    object-fit: cover;
}
select#vehicle_type{
       background-position: 99% 50% !important;

}
.form-control.change-color{
       background-position: 98.7% 51% !important;

}

select.member.change-color{
  color: grey !important;
}
select.member option { color: black; }


.multipleer {
    background-position: 99% 46%!important;
}


</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/css/intlTelInput.css" integrity="sha256-rTKxJIIHupH7lFo30458ner8uoSSRYciA0gttCkw1JE=" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/css/intlTelInput.css" rel="stylesheet" />
<div class="right_col" role="main">
   <div>
      <div class="page-title">
         <div class="title_left">
            <h3>Add Tour Guide </h3>
         </div>
         <div class="clearfix"></div>
      </div>
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
               <ul class="breadcrum">
                  <li><a href="{{url('admin/index')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                  <li><a href="{{url('admin/tour-management')}}">Tour Guide Management</a></li>
                  <li>Add Tour Guide</li>
               </ul>
               <div class="x_content new add">
                  <form method="post" id="validate_form" enctype='multipart/form-data' onsubmit="return validate_form();">
                     {{csrf_field()}}
                     <div class="col-sm-12">
                        <p class="mewew" style="font-size: 20px; margin-top: 12px; font-weight: 600;">Add Personal Info
                        </p>
                     </div>
                     <div class="text-center new">
                        <img onclick="$('#imgInp').click()" style="cursor: pointer;"  id="oldImg" width="100" height="100" src="{{url('public/admin/production/images/user.png')}}" alt="" title="Click to upload profile">
                        <input style="display:none;" type="file" id="imgInp" name="profile" data-role="magic-overlay" data-target="#pictureBtn">
                        <p id="msz" style="margin-top: 3px; font-weight: 600" class="error text-danger"></p>
                     
                     </div>
                     <div class="col-md-12">
                        <label>Name of Freelancer</label>
                        <input type="text" class="form-control" maxlength="30" placeholder="Enter Name of Freelancer" name="name" required/>
                        <label id="name-error" class="error" for="name" style="display: none;">{{$errors->first('name')}}</label>
                     </div>
                     <div class="col-md-6">
                        <label>Region</label>
                        <select class="form-control member change-color" name="region" required>
                           <option value="">Select Region</option>
                           <option value="Nairobi">Nairobi</option>
                           <option value="Kisumu">Kisumu</option>
                           <option value="Mombasa">Mombasa</option>
                        </select>
                        <label id="region-error" class="error" for="region" style="display: none;">{{$errors->first('region')}}</label>
                     </div>
                     <div class="col-md-6">
                        <label>Gender</label>
                        <select class="form-control member change-color" name="gender" required>
                           <option value="">Select Gender</option>
                           <option value="Male">Male</option>
                           <option value="Female">Female</option>
                           <option value="Other">Other</option>
                        </select>
                        <label id="gender-error" class="error" for="gender" style="display: none;">{{$errors->first('gender')}}</label>
                     </div>
                     <div class="col-md-6">
                        <label>Contact Number</label>
                        <input type="text" class="form-control" maxlength="15" placeholder="Enter Contact Number" name="phone_number" id="phone1" />
                        <label id="phone1-error" class="error" for="phone1" maxlength="15" style="display: none; margin-top: 2px; color: red; font-size: 14px;"></label>
                        <input type="hiden" name="country_code" id="country_code">
                     </div>
                     <div class="col-md-6">
                        <label>Date of Birth(DOB)</label>
                        <div class="field">
                           <div class="cal w100">
                              <span>
                                 <div class="bootstrap-iso"> <input type="text" id="date" name="dob" class="form-control datepicker" placeholder="Select Date of Birth(DOB)" autocomplete="off" required></div>
                                 <label id="date-error" class="error" for="date" style="display: none;">{{$errors->first('dob')}}</label>
                              </span>
                           </div>
                        </div>
                     </div>
                     
                     <div class="col-md-12">
                        <label>Countries Lived </label>
                        <div id="output"></div>
                           <select data-placeholder="Select Countries Lived" name="lived_countries[]" multiple class="chosen-select form-control lived_countries">
                              @foreach($countries as $country)
                                 <option value="{{$country->id}}">{{$country->name}}</option>
                              @endforeach()
                           </select>
                           <label id="lived_countries[]-error" class="error" for="lived_countries[]" style="display: none; margin-top: 2px; color: red; font-size: 14px;"></label>
                     </div>
                     <div class="col-md-12">
                        <label>Countries Visited </label>
                        <div id="output"></div>
                           <select data-placeholder="Select Countries Visited" name="visted_countries[]" multiple class="chosen-select form-control Choose visted_countries">
                              @foreach($countries as $country)
                                 <option value="{{$country->id}}">{{$country->name}}</option>
                              @endforeach()
                           </select>
                           <label id="visted_countries[]-error" class="error" for="visted_countries[]" style="display: none; margin-top: 2px; color: red; font-size: 14px;"></label>
                     </div>
                     <div class="col-md-12">
                        <label>Languages</label>
                        <div id="output"></div>
                           <select data-placeholder="Select Languages" name="language[]" multiple class="chosen-select form-control languages">
                              <option value="English">English</option>
                              <option value="Spanish">Spanish</option>
                              <option value="Swahili">Swahili</option>
                              <option value="French">French</option>
                              <option value="Arabic">Arabic</option>
                              <option value="Italian">Italian</option>
                              <option value="Russian">Russian</option>
                              <option value="Japanese">Japanese</option>
                              <option value="Chinese">Chinese</option>
                              <option value="Portuguese">Portuguese</option>
                              <option value="Polish">Polish</option>
                              <option value="German">German</option>
                              <option value="Hindi">Hindi</option>
                           </select>
                           <label id="language[]-error" class="error" for="language[]" style="display: none; margin-top: 2px; color: red; font-size: 14px;"></label>
                     </div>
                     <div class="col-md-12">
                        <label>Specializations</label>
                        <div id="output"></div>
                           <select data-placeholder="Select Specializations" name="specializations[]" multiple class="chosen-select form-control specializations" required>
                              <option value="Wildlife">Wildlife</option>
                              <option value="Culture">Culture </option>
                              <option value="Adventure">Adventure</option>
                              <option value="History & Museums">History & Museums</option>
                              <option value="Sports">Sports </option>
                              <option value="Art">Art </option>
                              <option value="Bird watching">Bird watching </option>
                              <option value="Plants & Vegetation">Plants & Vegetation </option>
                              <option value="Adventure & Active life">Adventure & Active life</option>
                              <option value="Marine Life">Marine Life</option>
                              <option value="Mountain Climbing">Mountain Climbing</option>
                           </select>
                           <label id="specializations[]-error" class="error" for="specializations[]" style="display: none; margin-top: 2px; color: red; font-size: 14px;"></label>
                     </div>
                     <div class="col-md-12">
                        <label>Work Experience</label>
                        <div class="field">
                           <div class="cal w100">
                              <!-- <span>
                                 <div class="bootstrap-iso"> <input type="text" id="date1" name="experience" class="form-control datepicker multipleer" placeholder="Select Work Experience" autocomplete="off" required></div>
                              </span> -->
                              <input type="text" class="form-control" name="experience" maxlength="5" placeholder="Enter Work Experience" required/>
                           </div>
                        </div>
                     </div>

                     <div class="col-md-12">
                        <label>Kenya Professional Safari Guides Association  (KPSGA) Membership</label>
                        <select placeholder="Select Specializations" class="form-control member change-color" name="educational_background" required>
                           <option value="">Select Kenya Professional Safari Guides Association  (KPSGA) Membership</option>
                           <option value="Gold">Gold</option>
                           <option value="Silver">Silver</option>
                           <option value="Bronze">Bronze</option>
                           <option value="Not KPSG">Not KPSGA</option>
                        </select>
                        <label id="educational_background-error" class="error" for="educational_background" style="display: none;">{{$errors->first('educational_background')}}</label>
                     </div>


                     <div class="col-md-12">
                        <label>No. of Safari Done</label>
                        <input type="text" class="form-control" placeholder="Enter No. of Safari Done" maxlength="5" name="no_of_safari" required/>
                        
                     </div>
                     <div class="col-md-12">
                        <label>Certifications </label>
                        <input type="text" class="form-control" placeholder="Enter Certifications" name="certifications" required/>
                     </div>
                     <div class="col-sm-12">
                        <p class="mewew" style="font-size: 20px; margin-top: 12px; font-weight: 600;">Add New Vehicle </p>
                     </div>
                     <div class="col-md-12">
                        <div class="text-center new">
                           <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">
                              <img src="{{url('public/images/plus_ing.png')}}" class="vehicle_added_image"  alt="" width="200" title="Click to upload vehicle image">
                              <input type="hidden" name="vehicle_image" class="vehicle_image" value="" required>
                           </a>
                           <label id="vehicle_image-error" class="error image_error" for="vehicle_image" style="display: none; margin-top: -10px; color: red; font-size: 14px; display: block;"></label>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <label>Vehicle Number </label>
                        <input type="text" class="form-control" placeholder="Enter Vehicle Number" maxlength="15" name="vehicle_number" required/>
                     </div>
                     <div class="col-md-12">
                        <label>Vehicle Type </label>
                        <select class="form-control member change-color multipleer" id="vehicle_type" name="vehicle_type" required>
                           <option value="">Select Vehicle Type</option>
                           <option value="Van">Van</option>
                           <option value="Land Cruiser">Land Cruiser</option>
                           <option value="Overland Truck">Overland Truck</option>
                        </select>
                     </div>
                     <div class="col-md-6">
                        <label>Number of Passengers
                        </label>
                        <input type="text" class="form-control" placeholder="Enter Min. Passengers" maxlength="5" id="minimum_passengers" name="minimum_passengers" required />
                     </div>
                     <div class="col-md-6">
                        <label>&nbsp;
                        </label>
                        <input type="text" class="form-control maximum_passengers" id="maximum_passengers" placeholder="Enter Max. Passengers " name="maximum_passengers" required/>
                        <input type="hidden" name="max_value" id="max_value" value="6">
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="col-md-6">
                              <div class="form-group ">
                                 <label>Front Ac</label>
                                 <div class="new-files " id="checkboxes">
                                    <div class="form-group">
                                       <input type="radio" id="" value="Yes" name="front_ac">
                                       <label for="">Yes</label>
                                    </div>
                                    <div class="form-group jack">
                                       <input type="radio" id="" value="No" checked name="front_ac">
                                       <label for="">No</label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <label>Rear Ac</label>
                              <div class="form-group ">
                                 <div class="new-files " id="checkboxes">
                                    <div class="form-group">
                                       <input type="radio" value="Yes"  name="rear_ac">
                                       <label for="">Yes</label>
                                    </div>
                                    <div class="form-group jack">
                                       <input type="radio" value="No" checked name="rear_ac">
                                       <label for="">No</label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>                    
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Cooler-Cooler box </label>
                           <div class="new-files" id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="Yes" name="cooler_box">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" checked name="cooler_box">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Cooler-Portable Fridge </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id="" value="Yes" name="cooler_fridge">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" id="" value="No" checked name="cooler_fridge">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Pop up Roof</label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id="" value="Yes" name="pop_up_roof">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" id="" value="No" checked name="pop_up_roof">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>1 Binocular</label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id="" value="Yes" name="binocular">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" id="" checked name="binocular">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Water Provided</label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id=""  value="Yes" name="water_provided">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" id="" value="No" checked name="water_provided">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Baby Seat Available </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id="" value="Yes" name="baby_seat">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" id="" value="No" checked name="baby_seat">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Power Outlet </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="yes" id="" name="power_outlet">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" id="" checked name="power_outlet">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Wi-Fi </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="Yes" id="" name="wifi">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" checked id="" name="wifi">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Side Steps – for elderly</label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio"  id="" value="Yes" name="side_steps">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" id="" checked name="side_steps">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Safari Guide Book/Checklist </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="Yes" name="safari_guide_book" id="">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" checked name="safari_guide_book" id="">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Picnic Hamper  </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id="" value="Yes" name="picnic_hamper">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" id="" value="No" checked name="picnic_hamper">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>HF/VHF  Radio </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="Yes" name="radio" id="">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" checked value="No" name="radio">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Vehicle – 4X4 </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="Yes" name="vehicle_4X4">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" checked name="vehicle_4X4">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Disabled Equipment </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" id="" value="Yes" name="disabled_equipment">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" id="" checked value="No" name="disabled_equipment">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label>Sliding Window </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio"  value="Yes" name="sliding_window">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" checked value="No"  name="sliding_window">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                           <label> Open Sided With Canvas Cover  </label>
                           <div class="new-files " id="checkboxes">
                              <div class="form-group">
                                 <input type="radio" value="Yes" name="open_sided" id="">
                                 <label for="">Yes</label>
                              </div>
                              <div class="form-group jack">
                                 <input type="radio" value="No" checked name="open_sided" id="">
                                 <label for="">No</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <label>Rate Per KM</label>
                        <p id="rate">Van</p>
                     </div>
                     <div class="row homewer">
                        <div class="col-md-4">
                           <label style="padding-left: 8px;">Price Per KM </label>
                        </div>
                        <div class="col-md-4">
                           <input type="text" class="form-control price_per_km_us" maxlength="20" placeholder="Enter Price (US) Dollar" name="price_per_km_us" required>
                        </div>
                        <div class="col-md-4">
                           <input type="text" class="form-control price_per_km_kenya" maxlength="20" placeholder="Enter Price (Kenya Shillings)" name="price_per_km_kenya" required>
                        </div>
                        <div class="col-md-12">
                           <button type="submit" class="btn btn-primary edit button_submit" style="margin-top: 22px;">Add</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header jack">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
            </button>
            <h2 class="text-center">Select Vehicle Image</h2>
         </div>
         <div class="modal-body">
            <div class="text-new add">
               @foreach($vehicleImages as $vehicleImage)
                  <img class="set_image" src="{{$vehicleImage->image}}" data-id="{{$vehicleImage->id}}" style="cursor: pointer;">
               @endforeach()
            </div>
         </div>
         <div class="modal-footer">
            <!-- <button type="submit" class="btn btn-success" data-dismiss="modal">Add</button> -->
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/intlTelInput.js"></script>
<script type="text/javascript" src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script type="text/javascript" src="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>


<script type="text/javascript">
   document.getElementById('output').innerHTML = location.search;
   $(".chosen-select").chosen({max_selected_options: 10});

   $(document).ready(function(){
       $("select.specializations").change(function(){
           var selectedCountry = $(this).children("option:selected").val();
           if(selectedCountry) {
            $(this).next().next().html("")
           }
       });

       $("select.languages").change(function(){
           var selectedCountry = $(this).children("option:selected").val();
           if(selectedCountry) {
            $(this).next().next().html("")
           }
       });

       $("select.lived_countries").change(function(){
           var selectedCountry = $(this).children("option:selected").val();
           if(selectedCountry) {
            $(this).next().next().html("")
           }
       });
      $("select.visted_countries").change(function(){
           var selectedCountry = $(this).children("option:selected").val();
           if(selectedCountry) {
            $(this).next().next().html("")
           }
       });
   });

</script>
<script type="text/javascript">
   $("#vehicle_type").change(function(){
      $("#rate").text($(this).val())

      if($(this).val() == "Van") {
         $("#max_value").attr("value",6)
         $(".price_per_km_us").val("")
         $(".price_per_km_kenya").val("")

         $("#minimum_passengers").val("")
         $("#maximum_passengers").val("")
      } else if($(this).val() == "Land Cruiser") {
         $("#max_value").attr("value",7)
         $(".price_per_km_us").val("")
         $(".price_per_km_kenya").val("")

         $("#minimum_passengers").val("")
         $("#maximum_passengers").val("")
      } else {
         $("#max_value").attr("value",15)
         $(".price_per_km_us").val("")
         $(".price_per_km_kenya").val("")

         $("#minimum_passengers").val("")
         $("#maximum_passengers").val("")
      }
   })
</script>


<script>
   $(document).ready(function() {
       $('#datatable-filter').dataTable({
           columnDefs: [{
               targets: 'no-sort',
               orderable: false
           }],
           language: {
               search: "_INPUT_",
               searchPlaceholder: "Search..."
           },
           "bLengthChange": false,
           "bInfo": false,
           "pageLength": 5,
       });
   });
   
</script>
<script>
   $(document).ready(function() {
    let date = new Date();

    let startDate = (new Date().getFullYear() - 16) +"/"+ (date.getMonth() + 1) + "/" + date.getDate();

    // console.log(startDate)
       var date_input = $('input[name="dob"]').datetimepicker({
        format: 'YYYY-MM-DD',
        maxDate: startDate
       });
   })
</script>

<script type="text/javascript">
   $('#checkboxes').on('click', ':checkbox', function(e) {
       $('#checkboxes :checkbox').each(function() {
           if (this != e.target)
               $(this).prop('checked', false);
       });
   });
   
</script>
<script>
   $('.navbar-right .user-profile.dropdown-toggle').click(function() {
       $("body").removeClass("nav-sm").addClass('nav-md');
   });
   
</script>

<script type="text/javascript">
   $('#country_code').hide();
   $("#phone1").intlTelInput({
     initialCountry: "us",
     separateDialCode: true,
     nationalMode: false,
     formatOnDisplay: false,
     autoFormat: false,
     preferredCountries: ["fr", "us", "gb"],
     geoIpLookup: function (callback) {
         $.get('https://ipinfo.io', function () {
         }, "jsonp").always(function (resp) {
             var countryCode = (resp && resp.country) ? resp.country : "";
             callback(countryCode);
         });
     },
     utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"
     
   });
   
   $("#hiden").intlTelInput({
     initialCountry: "us",
     dropdownContainer: 'body',
     utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"
   });
   
      
   var country_code = $("#phone1").intlTelInput();
   $("#phone1").on("countrychange", function (e, countryData) {
     $("#phone1").val('');
     var country_code = countryData['dialCode'];
     $('#country_code').val(country_code);
   });
   
   var number =  $('input[name="phone_number"]').val();
   var classf = $(".selected-flag > div").attr("class");
   var flag = classf.slice(-2);
   

   $('input.hide').parent().hide();
</script>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {  
                var type = input.files[0].type;
                if(type == 'image/jpeg' || type == 'image/png'){
                    var reader = new FileReader();
           
                    reader.onload = function (e) {
                        $('#oldImg').attr('src', e.target.result);
                    }
           
                    reader.readAsDataURL(input.files[0]);
                    $('#msz').hide();
                    $("#oldImg").addClass("profile_image")
                    $('.test').hide();
           
                } else {
                    $('#msz').show();
                    $('#imgInp').val("");
                    $("#oldImg").removeClass("profile_image")
                    $('#oldImg').attr('src',"{{url('public/admin/production/images/user.png')}}");
                    $('#msz').text('Only Jpeg and Png format allowed');
                }
            }
        }
        $("#imgInp").change(function(){
          readURL(this);
        });
    </script>
    <script type="text/javascript">
      var form = $("#validate_form");
      $.validator.addMethod("notOnlyZero", function (value, element, param) {
          return this.optional(element) || parseInt(value) > 0;
      });

      $.validator.addMethod("greaterThan",function (value, element, param) {
             var $otherElement = $(param);
             return parseInt(value, 10) > parseInt($otherElement.val(), 10);
      });

      jQuery.validator.addMethod("noSpace", function(value, element) {
           return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
          }, "Space not allowed");

      form.validate({
         ignore: [],
         rules : {
               name : {
                  required : true,
                  minlength : 2,
                  noSpace: true
               },
               region : {
                  required : true
               },
               gender : {
                  required : true
               },
               vehicle_type : {
                  required : true
               },

               dob : {
                  required : true
               },

               phone_number : {
                  required : true,
                  number: true,
                  minlength : 8,
                  noSpace: true
               },
               'lived_countries[]' : {
                  required : true
               },
               'visted_countries[]' : {
                  required : true
               },
               'language[]' : {
                  required : true
               },
               'specializations[]' : {
                  required : true
               },
               experience : {
                  required : true,
                  number: true,
                  notOnlyZero: true
               },
               certifications : {
                  required : true,
                  noSpace: true
               },
               price_per_km_us : {
                  required : true,
                  number: true,
                  notOnlyZero: true
               },
               price_per_km_kenya : {
                  required : true,
                  number: true,
                  notOnlyZero: true
               },
               no_of_safari : {
                  required : true,
                  number: true,
                  noSpace: true,
                  notOnlyZero: true
               },
               educational_background : {
                  required : true
               },

               vehicle_image : {
                  required : true
               },
               minimum_passengers : {
                  required : true,
                  number: true,
                  notOnlyZero: true,
                  noSpace: true
               },
               maximum_passengers : {
                  required : true,
                  number: true,
                  notOnlyZero: true,
                  noSpace: true,
                  greaterThan: '#minimum_passengers',
                  max: function () {
                    return parseInt($('#max_value').val());
                  }
               },
               vehicle_number : {
                  required : true,
                  noSpace: true
               }
               
          },
          messages : {
              name : {
                  required : "Please enter name of freelancer",
                  minlength : "Name of freelancer should be atleast 2 characters long"
              },
              no_of_safari : {
                  required : "Please enter no. of safari done",
                  number: "Please enter a valid number",
                  notOnlyZero: "No. of safari done must be greater than 0",
              },
              region : {
                  required : "Please select region"
              },
              gender : {
                  required : "Please select gender"
              },
              phone_number : {
                  required : "Please enter contact number",
                  number: "Please enter a valid contact number",
                  minlength :"Contact number should be between 8 to 15 digits"
              },
              'lived_countries[]' : {
                  required : "Please select countries lived"
              },
              'visted_countries[]' : {
                  required : "Please select countries visited"
              },
               dob : {
                  required : "Please select date of birth"
              },
              'language[]' : {
                  required : "Please select languages"
               },
               'specializations[]' : {
                  required : "Please select specializations"
               },
               experience : {
                  required : "Please enter work experience",
                  number: "Please enter a valid number",
                  notOnlyZero: "Experience must be greater than 0",
               },
               educational_background : {
                  required : "Please select educational background"
               },
               certifications : {
                  required : "Please enter certifications"
               },
               vehicle_number : {
                  required : "Please enter vehicle number"
               },
               vehicle_type : {
                  required : "Please enter vehicle type"
               },
               minimum_passengers : {
                  required : "Please enter minimum passengers",
                  notOnlyZero: "Minimum passengers must be greater than 0",
                  number: "Please enter a valid number"
               },

               maximum_passengers : {
                  required : "Please enter maximum passengers",
                  notOnlyZero: "Maximum passengers must be greater than 0",
                  number: "Please enter a valid number",
                  greaterThan: "Maximum passengers must be greater than minimum passengers",
                  // max:"Maximum passengers"
               },
               price_per_km_us : {
                  required : "Please enter price (US dollar)",
                  number: "Please enter a valid number",
                  notOnlyZero: "Price (US dollar) must be greater than 0"
               },
               price_per_km_kenya : {
                  required : "Please enter price (Kenya Shillings)",
                  number: "Please enter a valid number",
                  notOnlyZero: "Price (Kenya Shillings) must be greater than 0"
               },
               vehicle_image : {
                  required : "Please select vehicle image"
               },
          }
      })

      setTimeout(function(){
          $(".alert").hide();    
      },4000);

      
      function validate_form() {
         if(form.valid()){
            $(".button_submit").attr("disabled",true);
            return true;
         }else {
            return false;
         }

      }

</script>

<script type="text/javascript">
   $(".set_image").click(function() {
      let image = $(this).attr('src');
      let id = $(this).attr('data-id');
      
      $(".vehicle_added_image").attr('src',image)
      $(".vehicle_image").val(id)
      $(".image_error").text("")

      $('#myModal').modal('toggle');
   })
   function changeSelectColor(){
      if($("select.member option:selected").val() == ""){
         $("select.member").addClass("change-color")
      }else {
        $("select.member").removeClass("change-color")
      }
   }
   changeSelectColor();
   $("select.member").change(function(){
      changeSelectColor();
   })
</script>

@endsection