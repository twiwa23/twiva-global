@extends("business/layout/web")
@section("title","Update Mpesa Account")
@section("content")
  <style>
.error {
  color: red;
}
  </style>
<div class="right_col bg_banner" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
   <h2 class="color-text">Update Mpesa Account</h2>
   <ul class="tabs" style="visibility: hidden;">
      <li class="first">Create</li>
      <li class="second">On Going</li>
      <li class="third">Completed</li>
   </ul>
 
   
   <div class="hashtags create_post_input country_code">
        @include('business.includes.notifications')
      <form method="POST" id="form_validate" novalidate="novalidate">
         <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
         <div class="label_addres">
            <label for=""> 
            Registered Mpesa Phone Number
            </label>
         </div>
         <div class="form-group change_icon">
            <div class="email" style="position: relative;">
               <i class="fa lock_icon">
               <img src="{{url('public/business/images/phone.png')}}">
               </i>
               <input type="text" name="register_mpesa_phone_number" id="register_mpesa_phone_number" onkeypress="return isNumberKey(event)" value = "{{$data->phone_number}}" class="form-control"  placeholder="">
               <span class="text-danger"></span>
               <label id="register_mpesa_phone_number-error" class="error" for="register_mpesa_phone_number">{{$errors->first('register_mpesa_phone_number')}}</label>
            </div>
         </div>
      
      <div class="space-top top">
         <button type = "submit" name = "submit" class="button">UPDATE</button>
      </div>
      </form>
   </div>
</div>
@endsection
@section('js')
<script language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
       //-->
    </script>

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
  $('#form_validate').validate({ 
      rules: {
          register_mpesa_phone_number: {
              required: true,
              minlength:8,
              maxlength:15
          },
         
      },

        messages:{
         
          register_mpesa_phone_number:{  
            required:'Please enter register mpesa phone number.',
            minlength:'Register mpesa phone number must be at least 8 digits.',
            maxlength:'Register mpesa phone number  maximum 15 digits.',
          }, 
          
                     
        },
       
    
  });
</script>
 <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});
</script>
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
@endsection