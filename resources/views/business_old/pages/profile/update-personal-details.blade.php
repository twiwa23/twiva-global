@extends("business/layout/web")
@section("title","Update Personal Details")
@section("content")
<link rel="stylesheet" href="{{url('public/business/csss/intlTelInput.css')}}" />
<link href="{{url('public/business/csss/intlTelInput.min.css')}}" rel="stylesheet" />
<style>
  .words {
    color: #686868 !important;
    text-align: right;
    font-size: 12px;
    margin-top: 3px;
  }
   .error {
   color: red;
   }
   .selected-flag{
   padding-left: 26px !important;
   }
   .selected-dial-code{
   padding-left: 28px !important;
   }
   .iti-arrow{
   right:-10px !important;
   }
   .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-2 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-2 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-2 input[type=tel] {
    padding-left: 90px;
}

.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-3 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-3 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-3 input[type=tel]{
       padding-left: 100px;
}
.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-4 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-4 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-4 input[type=tel]{
   padding-left: 108px;
}
.intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-5 input, .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-5 input[type=text], .intl-tel-input.separate-dial-code.allow-dropdown.iti-sdc-5 input[type=tel]{
   padding-left: 115px;
}
   .intl-tel-input {
   width: 100%;
   }
   .textarea{
   box-shadow: none;
   }
</style>
<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
   <div class="about-us">
      <h2 class="color-text">Update Personal Details</h2>
      <section class="update_input uploaded-data common-struct login_FORM login-input">
         <form method="POST" id="form_validate" enctype="multipart/form-data" novalidate="novalidate">
            {{csrf_field()}}
            <div class="Update-img text-center" style="margin: 13px 0 15px;">
              @include('business.includes.notifications') 
         
               @php($url =  $business_list->profile ? url($business_list->profile) : url('public/business/images/upload-one.png'))
               <img  title="Click here to change image"
                  onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 120px;height: 120px;object-fit: cover;border-radius: 100%;border-radius: 100%;" class="img-responsive" class="red-tooltip" data-toggle="tooltip" data-placement="right" title="Upload profile image"/>
               <input style="display:none;" type="file" id="imgInp" name="profile"  data-role="magic-overlay" data-target="#pictureBtn" value="{{$business_list->profile}}">
            </div>
            <span style="display:none; margin :20px;color: red" class="text-danger" id="invalid_file">Please select jpg, jpeg or png image format only. </span>
            <!--  <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive" class="red-tooltip" data-toggle="tooltip" data-placement="right" title="Upload profile image"> -->
            <input type="hidden" name="_token" value="kAKrRbdr1373kj5b62W2XZOH4kbD2KzLOQFi2vXx">
            <div class="label_addres">
               <label for="">
               Company Name
               </label>
            </div>
            <div class="form-group">
               <div class="email">
                  <i class="fa lock_icon">
                  <img src="{{url('public/business/images/company_icon.png')}}">
                  </i>
                  <input type="text" name="company_name" id = "name" value = "{{$business_list->name}}" class="form-control" placeholder = "" > 
                  <span class="text-danger"></span>
                  <span class="text-danger"></span>
                  <label id="company_name-error" class="error" for="company_name">{{$errors->first('company_name')}}</label>
               </div>
            </div>
            <div class="label_addres">
               <label for="">
               Email Address
               </label>
            </div>
            <div class="form-group">
               <div class="email">
                  <i class="fa">
                  <img src="{{url('public/business/images/mesaage.png')}}">
                  </i>
                  <input type="text"  value ="{{$business_list->email}}" class="form-control" placeholder = "" readonly >
                  <span class="text-danger"></span>
                  <!-- <label id="email-error" class="error" for="email">{{$errors->first('email')}}</label> -->
               </div>
            </div>
            <div class="label_addres">
               <label for="">
               Phone Number
               </label>
            </div>
            <div class="form-group">
               <div class="email" style="position: relative;">
                  <i class="fa lock_icon">
                  <img src="{{url('public/business/images/phone-icon.png')}}">
                  </i>
                  <input type="text" class="form-control" placeholder="" name="phone_number" value = "{{$business_list->phone_number}}" id="phone_number" onkeypress="return isNumberKey(event)"/>
                     <span class="text-danger"></span>
                      <label id="phone_number-error" class="error" for="phone_number">{{$errors->first('phone_number')}}</label>
               </div>
            </div>
            <div class="label_addres">
            <label for="">
            Contact Person Name
            </label>
            </div>
            <div class="form-group">
               <div class="email">
                  <i class="fa lock_icon">
                  <img src="{{url('public/business/images/name_icon.png')}}">
                  </i>
                  <input type="text" id="contact_person_name" name="contact_person_name" value = "{{$business_list->contact_person}}"  class="form-control" placeholder="">
                  <span class="text-danger"></span>
                  <label id="contact_person_name-error" class="error" for="contact_person_name">{{$errors->first('contact_person_name')}}</label>
               </div>
            </div>
            <div class="form-group">
               <div class="label_addres">
                  <label for="">
                  Description
                  </label>
               </div>
               <div class="form-group" style="margin-top: -10px;">
                  <div class="email">
                     <i class="fa lock_icon" style="top: 14px;">
                     <img src="{{url('public/business/images/buil-first-b.png')}}">
                     </i>
                 
                     <textarea type="text" name="description" maxlength="1000" id="description" rows="4"  class="form-control textarea" placeholder=""style="font-size: 18px; margin-top: 12px; height: 108px; padding-left: 33px; color:#171717">@if(!empty($business_details->business_detail)){{$business_details->business_detail}} @else N/A @endif</textarea>
                     <div class="words">Maximum limit 1000 characters</div>
                    <span class="text-danger"></span>
                    <label id="description-error" class="error" for="description">{{$errors->first('description')}}</label>
                    
                  </div>
                   
               </div>
               <div class="form-group Submit SignUp" style="margin-bottom: 115px !important;">
                  <input type="submit" name="submit" value="UPDATE" class="btn btn-primary">
               </div>
         </form>
      </section>
      </div>
      <div class="clr"></div>
   </div>
</div>
@endsection
@section('js')
<script>
     Jquery.validator.addMethod("no_space",function(val,ele){
if(val != ""){
if(val.trim().length == 0){
return false;
}
}
return true;
},"Space not allowed")
</script>
  <script type="text/javascript">
    $('#mob_phone').hide();
    $('#phone').on('keyup', function() {
    $('#mob_phone').html($(this).val());
  });
  </script>
@if(Session::has('success'))
<script type="text/javascript">
   $('#exampleModalCenter23').modal("show")
</script>
@endif
<script>
   function readURL(input) {
       if (input.files && input.files[0]) {
   
           var type = (input.files[0].type);
   
           if (type == "image/png" || type == "image/jpeg") {
               $('#invalid_file').css({'display': 'none'});
           } else {
               $('#invalid_file').css({'display': 'block'});
               $('#imgInp').val('');
               return false;
           }
   
   
           var reader = new FileReader();
   
           reader.onload = function (e) {
               $('#blah').attr('src', e.target.result);
           }
   
           reader.readAsDataURL(input.files[0]);
       }
   }
   
   $("#imgInp").change(function () {
       readURL(this);
   });
   
</script>
<script type="text/javascript">
   <!--
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
         return false;
   
      return true;
   }

   //-->
</script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});
</script>
<script>

   
   $('#form_validate').validate({ 
     rules: {
     
         
         company_name: {
             required: true,
             maxlength:100,
             minlength:3,
             
         },
          contact_person_name: {
             required: true,
             maxlength:100,
             minlength:3,
             
         },
          phone_number: {
             required: true,
             minlength:8,
             maxlength:15,
             digits:true
            
         },
        
          description: {
             required: true,
             minlength:3,
             maxlength:1000,
             noSpace : true
         },
     },
   
       messages:{
         company_name:{  
           required:'Please enter company name.',
           minlength:'Company name must be at least 3 characters long.',
           maxlength:'Company name maximum 100 characters long.'
         },
         contact_person_name:{  
           required:'Please enter contact person name.',
           minlength:'Contact person name must be at least 3 characters long.',
           maxlength:'Contact person name maximum 100 characters long.'
         },
        /* email:{  
           required:'Please enter email address.',
           email:'Please enter a valid email address.',
           maxlength:'Email address maximum 200 characters.'
         },
       
   */
   
         description:{  
           required:'Please enter description.',
           minlength:'Description address must be at least 3 characters.',
           maxlength:'Description maximum 1000 characters.'
         },
   
   
         phone_number:{  
           required:'Please enter  phone number.',
           minlength:'Phone number must be at least 8 digits.',
           maxlength:'Phone number  maximum 15 digits.',
           digits:'Only numeric value in  phone number .',
         }, 
                     
       },
      
   
   });
</script>
<script type="text/javascript">
   console.log( "document ready!" );
   
   
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
     var generalSidebarHeight = $sticky.innerHeight();
     var stickyTop = $sticky.offset().top;
     var stickOffset = 0;
     var stickyStopperPosition = $stickyrStopper.offset().top;
     var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
     var diff = stopPoint + stickOffset;
   
     $(window).scroll(function(){ // scroll event
       var windowTop = $(window).scrollTop(); // returns number
   
       if (stopPoint < windowTop) {
           $sticky.css({ position: 'absolute', top: diff });
       } else if (stickyTop < windowTop+stickOffset) {
           $sticky.css({ position: 'fixed', top: stickOffset });
       } else {
           $sticky.css({position: 'absolute', top: 'initial'});
       }
     });
   
   }
       
</script>
@endsection