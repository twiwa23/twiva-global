@extends("business/layout/web")
@section("title","Create Gig")
@section("content")
<style>
  .words {
    color: #686868 !important;
    text-align: right;
    font-size: 12px;
    margin-top: 3px;
  }
   .error {
    color: red;
    font-size: 16px;
   }
  .email.calender_icon img {
    position: absolute;
    width: 17px;
    top: 8px;
    right: 0;
    cursor: pointer;
}
.email.calender_icon{
   position: relative;
}
.color-info {
  text-align: right;
  margin: 11px 39px 0;
}
.color-info a {
  color: #5b0c33;
}
.color-info i {
  font-size: 28px;
    vertical-align: middle;
}

.rating {
  display: inline-block;
  position: relative;
  height: 50px;
  line-height: 50px;
  font-size: 50px;
}

.rating label {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  cursor: pointer;
}

.rating label:last-child {
  position: static;
}

.rating label:nth-child(1) {
  z-index: 5;
}

.rating label:nth-child(2) {
  z-index: 4;
}

.rating label:nth-child(3) {
  z-index: 3;
}

.rating label:nth-child(4) {
  z-index: 2;
}

.rating label:nth-child(5) {
  z-index: 1;
}

.rating label input {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
}

.rating label .icon {
  float: left;
  color: transparent;
}

.rating label:last-child .icon {
  color: #fff;
  text-shadow: 0 0 5px rgb(115,14,57);
}

.rating:not(:hover) label input:checked ~ .icon,
.rating:hover label:hover input ~ .icon {
  color: rgb(246, 204, 7);
}

.rating label input:focus:not(:checked) ~ .icon:last-child {
  color: #fff;
}
.error {
    color: red;
    font-size: 16px;
    float: left;
    width: 100%;
}
</style>
<div class="right_col" role="main">
<div class="x_panel">
<div class="dashboard-container About">
<div class="x_content new toofle">
<div class="row">
<div class="col-sm-12">
<div class="about-us">
  
   <h2 class="color-text">Rating & Review</h2>
   
   <div class="hashtags create_post_input">
      <form method="POST" id="form_validate" novalidate="novalidate" autocomplete="off">
          {{csrf_field()}}
          <div class="label_addres">
            <label for="">
            Rating
            </label>
         </div>
        <div class="rating">
          <label>
            <input type="radio" name="stars" value="1" />
            <span class="icon">★</span>
          </label>
          <label>
            <input type="radio" name="stars" value="2" />
            <span class="icon">★</span>
            <span class="icon">★</span>
          </label>
          <label>
            <input type="radio" name="stars" value="3" />
            <span class="icon">★</span>
            <span class="icon">★</span>
            <span class="icon">★</span>   
          </label>
          <label>
            <input type="radio" name="stars" value="4" />
            <span class="icon">★</span>
            <span class="icon">★</span>
            <span class="icon">★</span>
            <span class="icon">★</span>
          </label>
          <label>
            <input type="radio" name="stars" value="5" />
            <span class="icon">★</span>
            <span class="icon">★</span>
            <span class="icon">★</span>
            <span class="icon">★</span>
            <span class="icon">★</span>
          </label>
        </div>
          <label id="stars-error" class="error" for="stars"></label>
         
         <div class="label_addres">
            <label for="">
            Comment
            </label>
         </div>
         <div class="form-group">
            <div class="email uploaded-data">
               <textarea type="text" name="review" maxlength="1000" id="review" rows="2" placeholder="">{{old('review')}}</textarea>
               <span class="text-danger"></span>
            </div>
             <label id="review-error" class="error" for="review">{{$errors->first('review')}}</label>
         </div>
         
      <div class="space-top top">
        <button type = "submit" name = "submit"  class="button">Submit</button>
      </div>
   </form>
   </div>
</div>
@endsection
@section('js')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
    $(':radio').change(function() {
      console.log('New star rating: ' + this.value);
    });

   function calculateDateTime(){
      let result = 1;
       let startDate = $("#gig_start_date").val();
       let startTime = $("input[name='gig_start_time']").val();
       let endDate = $("#gig_end_date").val();
       let endTime = $("#gig_end_time").val();

       if(startDate != "" && startTime != "" && endDate != "" || endTime != ""){
         let start = new Date(startDate + " " + startTime);
         let end = new Date(endDate + " " + endTime);
         if(start > end){
            result = 0;
            alert("Start time should be less than end time.");
         }
       }
       return result;
   }
  
  $('#form_validate').validate({ 
      rules: {
          stars: {
              required: true,
              maxlength:30,
          },
           
          review: {
              required: true
          }
      },

        messages:{
          stars:{  
            required:'Please select rating.'
          },
          review:{  
            required:'Please enter comment.'
          },
                  
        },

        submitHandler : function(form){
         if(calculateDateTime() == 1){
          localStorage.setItem("post_data",JSON.stringify({
            "title" : $("#title").val(),
            "description" : $("textarea[name='description']").val(),
            "contact_person_name" : $("#contact_person_name").val(),
            "contact_person_number" : $("#contact_person_number").val(),
            "gig_start_date" : $("#gig_start_date").val(),
            "gig_start_time" : $("textarea[name='gig_start_time']").val(),
            "gig_end_date" : $("#gig_end_date").val(),
            "gig_end_time" : $("#gig_end_time").val(),
          }))
            form.submit();
         }
        }
  });
  let isPrevPage = localStorage.getItem("pageIs");
  if(isPrevPage){
      if(isPrevPage.indexOf("create-gig-field") != -1){
          console.log("--------- last ",isPrevPage)
      }
  }
  console.log(localStorage.getItem("post_data"))
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
   $(document).ready(function() {
    var date = new Date();
   var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      let datepicker = $('input[name="gig_start_date"],input[name="gig_end_date"]').datetimepicker({
         format: 'YYYY-MM-DD',
         minDate: today,
       });
   })
</script>
<script type="text/javascript">
   $( document ).ready(function() {
   console.log( "document ready!" );
   
   var $sticky = $('.sticky');
   var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
   if (!!$sticky.offset()) { // make sure ".sticky" element exists
   
   var generalSidebarHeight = $sticky.innerHeight();
   var stickyTop = $sticky.offset().top;
   var stickOffset = 0;
   var stickyStopperPosition = $stickyrStopper.offset().top;
   var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
   var diff = stopPoint + stickOffset;
   
   $(window).scroll(function(){ // scroll event
   var windowTop = $(window).scrollTop(); // returns number
   
   if (stopPoint < windowTop) {
     $sticky.css({ position: 'absolute', top: diff });
   } else if (stickyTop < windowTop+stickOffset) {
     $sticky.css({ position: 'fixed', top: stickOffset });
   } else {
     $sticky.css({position: 'absolute', top: 'initial'});
   }
   });
   
   }
   });
</script>
  <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
       //-->
    </SCRIPT>
    <script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});

/* show hide # in hashtag on focus in and focus out */
   $("#hashtag").on("click paste keypress",function(){
    let val = $(this).val();
    if(val[0] != "#"){
       $(this).val("#"+val)
    }
   })

   $("#hashtag").focusout(function(){
      let val = $(this).val();
      if(val[0] == "#"){
        $(this).val(val.substring(1))
      }
   })

</script>
@endsection