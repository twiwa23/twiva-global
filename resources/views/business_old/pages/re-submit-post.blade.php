@extends("business/layout/web")
@section("title","Re Submit Post Price")
@section("content")
<style type="text/css">
  label#price-error {
    color: red;
    font-size: 16px;
}
</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Re Submit Post Price</h2>
                        <section class="uploaded-data common-struct login_FORM login-input resubmit">
                          <div class="white_bg_sec common_white_bg">
                            <form method="post" id="form_validate">
                              {{csrf_field()}}
                                <div class="labels">
                                  <h5>Post Name</h5>
                                  <p>{{$declinedPostDetails->post->post_name ?? 'N/A'}}</p>
                                </div>
                                <div class="labels">
                                  <h5>New Request Price</h5>
                                  <div class="form-group price_input" style="margin-bottom: 7px;">
                                    <div class="email">
                                      <input type="text" maxlength="15" name="price" class="form-control" required>
                                    </div>
                                  </div>
                                </div>
                                <input type="hidden" name="infulancerId" value="{{$declinedPostDetails->user_id}}" class="form-control">
                                <div class="labels">
                                  <h5>Influencer Name</h5>
                                  <p>{{$declinedPostDetails->user->name ?? 'N/A'}}</p>
                                </div>
                                <div class="form-group Submit medium_btn m_top spacing_btn">
                                    <input type="submit" name="Login" value="SUBMIT" class="btn btn-primary">
                                  <!-- <a href="{{url('business/pending-post-list')}}">
                                  </a> -->
                                </div>
                            </form>
                          </div>
                        </section>
                </div>
           
              </div>
            </div>
         @endsection
         @section('js')
         <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
      <script type="text/javascript">
        $( document ).ready(function() {
        console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
      <script type="text/javascript">

     jQuery.validator.addMethod("noSpace", function(value, element) {
        return value.length > 0 ? (value.trim().length == 0 ? false : true) : true
     }, "Space not allowed");

     $.validator.addMethod("notOnlyZero", function (value, element, param) {
          return this.optional(element) || parseInt(value) > 0;
      });

     $('#form_validate').validate({ 
        rules: {
            price: {
                required: true,
                notOnlyZero: true
            },           
        },
     
        messages:{
            
           price:{  
              required:'Please new request price.',
              notOnlyZero:'Price should be greater than 0.'
           },             
        }
     });
  </script>
 @endsection