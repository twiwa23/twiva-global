@extends("business/layout/web")
@section("title","Settings")
@section("content")
<style>
       .common-struct .Update-img img {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
  }
    .alert-success {
   
    max-width: 550px;
   
}
.bnner_image h5{
 word-break: break-all;
}

</style>
         <div class="right_col bg_banner" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Settings</h2>
                        @include('business.includes.notifications') 
                        <section class="uploaded-data common-struct login_FORM login-input setting_bg">
                          <div class="white_bg_sec bnner_image"> 
                            <div class="Update-img text-center">

                              @if(!empty($user->profile))
                              <img src="{{$user->profile}}" class="img-responsive"/>

                              @else
                             <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                              @endif
                            </div>
                            <h5>@if(!empty($user->name)){{$user->name}} @else N/A @endif</h5>
                            <div class="settings_links">
                              <div class="detils_links">
                               <a href="{{url('business/update-personal-details')}}"><img src="{{url('public/business/images/contact-(1).png')}}" alt="contact-(1)">Update Personal Details</a>
                                <div class="border-bottom"></div> 
                              </div>
                              <div class="detils_links">
                               <a href="{{url('business/change-password')}}"><img src="{{url('public/business/images/password.png')}}" alt="password">Change Password</a>
                                <div class="border-bottom"></div> 
                              </div>
                              <div class="detils_links">
                                <a href="{{url('business/update-mpsa-account')}}"><img src="{{url('public/business/images/person.png')}}" alt="person">Update Mpesa Account</a>
                                <!-- <div class="border-bottom"></div>  -->
                              </div>
                            </div>
                            <div class="strip"></div>
                            <div class="strip2"></div>
                            <div class="strip3"></div>
                            <div class="strip4"></div>
                          </div>
                        </section>
                </div>
              </div>
            </div>
        
@endsection
@section('js')
      <script type="text/javascript">
        $( document ).ready(function() {
  console.log( "document ready!" );
  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
</script>
<script type="text/javascript">
$(document).ready(function(){
setTimeout(()=>{
$(".alert").fadeOut("slow");
},6000);
});
</script>
   @endsection