@extends("business/layout/web")
@section("title","Summary")
@section("content")
       <style>
        .common-struct .Update-img img {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
}
</style>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col bg_banner" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="color-text">Summary</h2>
                      <div class="p_allside">
                        <div class="tabs_list" style="margin-top: 12px;">
                          <div class="summary_details">
                            <div class="date_content">
                                <span class="equal_width">Total Influencers: </span>3
                            </div>
                            <div class="date_content">
                                <span class="equal_width">Total Amount:  </span>$360
                            </div>
                            <div class="date_content">
                                <span class="equal_width">Minimum Reach:   </span>65.5K
                            </div>
                          </div>
                        </div>
                        <section class="common-struct login-input">
                          <div class="post_list">
                            <div class="row">
                              <div class="col-md-4">
                                <div class="white_bg_sec selected_bg_color hide_content"> 
                                  <div class="Update-img text-center">
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                  </div>
                                  <h5>Influencer Name</h5>
                                  <div class="border-top">
                                  </div>
                                  <div class="social_flex">
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                      <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                  </div>
                                  <div class="border-bottom">
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>$120
                                  </div>
                                  <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span>$120
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup">ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide">
                                    <a href="#">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="white_bg_sec selected_bg_color hide_content2"> 
                                  <div class="Update-img text-center">
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                  </div>
                                  <h5>Influencer Name</h5>
                                  <div class="border-top">
                                  </div>
                                  <div class="social_flex">
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                      <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                  </div>
                                  <div class="border-bottom">
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>$120
                                  </div>
                                  <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span>$120
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup">ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide2">
                                    <a href="#">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="white_bg_sec selected_bg_color hide_content3"> 
                                  <div class="Update-img text-center">
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                  </div>
                                  <h5>Influencer Name</h5>
                                  <div class="border-top">
                                  </div>
                                  <div class="social_flex">
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                      <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                  </div>
                                  <div class="border-bottom">
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>$120
                                  </div>
                                  <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span>$120
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup">ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide3">
                                    <a href="#">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                        <section class="login_FORM summary_deials infu_btns">
                          <div class="form-group Submit medium_btn m_top">
                            <a href="{{url('business/gig-list')}}">
                              <input type="button" name="Login" value="CONFIRM" class="btn btn-primary"> 
                            </a>
                          </div>
                        </section>
                        <!-- Modal -->
                        <div class="modal fade modal_heading" id="exampleModalCenter23" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-body">
                                <h6 class="modal-title" id="exampleModalLongTitle">Set Price</h6>
                                <section class="set_price_popup login_FORM">
                                  <div class="label_addres" style="text-align: center;">
                                    <label for="">
                                      Set Price
                                    </label>
                                  </div>
                                  <div class="form-group">
                                    <div class="email">
                                      <input type="text" id="email" name="email" value="" class="form-control" placeholder="">

                                      <span class="text-danger"></span>
                                    </div>
                                  </div>
                                  <div class="label_addres" style="text-align: center;">
                                    <label for="">
                                      Complementary Items (Optional)
                                    </label>
                                  </div>
                                  <div class="form-group text_area">
                                    <div class="email">
                                      <!-- <input type="text" id="email" name="email" value="" class="form-control" placeholder="Complementary Items (Optional)"> -->
                                      <textarea type="text" name="" id=""  rows="2" placeholder=""></textarea>

                                      <span class="text-danger"></span>
                                    </div>
                                  </div>
                                </section>
                                <div class="common-btn">
                                 <input type="button" name="Login" value="UPDATE" class="btn btn-primary" data-dismiss="modal">
                               </div>
                             </div>
                             <div class="cross_icon" data-dismiss="modal">
                              <a href="">
                                <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Modal -->
                      </div>
                </div>
   
              </div>
            </div>
      

@endsection
@section('js')

    
      <script src="{{url('public/business/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
      
      <script type="text/javascript">
        $('.cross_hide').click(function(){
          $('.hide_content').hide();
          // $('.hide_content2').hide();
          // $('.hide_conten3').hide();
        });
         $('.cross_hide2').click(function(){
          $('.hide_content2').hide();
        });
         $('.cross_hide3').click(function(){
          $('.hide_content3').hide();
        });
        $('.adjust_popup').click(function(){
          $('#exampleModalCenter23').modal({
            backdrop: 'static'
          });
        }); 

        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});
      </script>
   @endsection