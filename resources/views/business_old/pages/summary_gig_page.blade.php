@extends("business/layout/web")
@section("title","Summary")
@section("content")
       <style>
        .common-struct .Update-img img {
    position: relative;
    border: 4px solid #fff;
    border-radius: 50%;
    box-shadow: 0px 0px 16px 0px rgba(82, 82, 82, 0.35);
    width: 120px;
    height: 120px;
    margin: auto;
    cursor: pointer;
}
.forgot_icon img {
    width: 45px;
    height: 45px;
    position: absolute;
    left: 24px;
    top: 4px;
}

.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid rgb(115 14 57);
  width: 120px;
  height: 120px;
  -webkit-animation: spin 1.5s linear infinite; /* Safari */
  animation: spin 1.5s linear infinite;
  float: none;
  margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

div#paymentPop .btn-success {
    color: #fff;
    background-color: #00b3a7;
    border-color: #00b3a7;
    padding: 11px 23px;
}
div#paymentPop .alert-success {
    color: #fff;
    background-color: #7a0f3d;
    border-color: #7a0f3d;
}
span#res-message {
    font-size: 16px;
}
input#mphone {
    border-top: 0;
    border-right: 0;
    border-left: 0;
    box-shadow: none;
    border-radius: 0;
    border-bottom: 2px solid rgb(199, 200, 199);

}

</style>
<script type="text/javascript">
  if(!localStorage.getItem("users_price") || 
  !localStorage.getItem("selected_users")){
    localStorage.removeItem("users_price")
    localStorage.removeItem("selected_users")
    window.history.back(-1)
  }
</script>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col bg_banner" role="main">
          <div class="x_panel">
            <div class="dashboard-container About">
            <div class="x_content new toofle">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="about-us">
                      <h2 class="forgot_icon color-text">
                          <a href="{{url('business/invite-influencers-gig',Request::segment(3))}}">
                          <img src="{{url('public/business/images/back_icon.png')}}" alt="back_icon">
                        </a>
                       Summary
                      </h2>
                      <div class="p_allside">
                        <div class="tabs_list" style="margin-top: 12px;">
                          <div class="summary_details">
                            <div class="date_content">
                                <span class="equal_width">Total Amount:  </span><span id="total-amount"></span>
                            </div>
                            <div class="date_content">
                                <span class="equal_width">Total Influencers: </span><span id="total-influencer"></span>
                            </div>
                            <div class="date_content">
                                <span class="equal_width">Minimum Reach:   </span><span id="min-influencer-reach"></span>
                            </div>
                          </div>
                        </div>
                        <section class="common-struct login-input">
                          <div class="post_list">                        
                          </div>
                        </section>

                        <!-- 
  <div class="row">
                              <div class="col-md-4">
                                <div class="white_bg_sec selected_bg_color hide_content"> 
                                  <div class="Update-img text-center">
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                  </div>
                                  <h5>Influencer Name</h5>
                                  <div class="border-top">
                                  </div>
                                  <div class="social_flex">
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                      <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                  </div>
                                  <div class="border-bottom">
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>$120
                                  </div>
                                  <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span>$120
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup">ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide">
                                    <a href="#">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="white_bg_sec selected_bg_color hide_content2"> 
                                  <div class="Update-img text-center">
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                  </div>
                                  <h5>Influencer Name</h5>
                                  <div class="border-top">
                                  </div>
                                  <div class="social_flex">
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                      <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                  </div>
                                  <div class="border-bottom">
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>$120
                                  </div>
                                  <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span>$120
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup">ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide2">
                                    <a href="#">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="white_bg_sec selected_bg_color hide_content3"> 
                                  <div class="Update-img text-center">
                                    <img src="{{url('public/business/images/upload-one.png')}}" alt="upload-one" class="img-responsive">
                                  </div>
                                  <h5>Influencer Name</h5>
                                  <div class="border-top">
                                  </div>
                                  <div class="social_flex">
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img">
                                      <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                    <div class="social_img border_none">
                                      <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                      <p>200<br> Followers</p>
                                    </div>
                                  </div>
                                  <div class="border-bottom">
                                  </div>
                                  <div class="date_content">
                                    <span class="equal_width">Desired Post Rate: </span>$120
                                  </div>
                                  <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span>$120
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup">ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide3">
                                    <a href="#">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                         -->
                         <div style="clear: both;"></div>
                        <section class="login_FORM summary_deials infu_btns">
                          <div class="form-group Submit medium_btn m_top">
                            <form method="POST" id="confirm_booking">
                              <input type="hidden" name="gig_prices" />
                              <input type="submit" value="CONFIRM" class="btn btn-primary"> 
                            </form>
                          </div>
                        </section>
                        <!-- modal -->

                        <div class="modal fade modal_heading" id="open-update-price-pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-body">
                                    <h6 class="modal-title" id="exampleModalLongTitle">Set Price</h6>
                                    <section class="set_price_popup login_FORM">
                                      <div class="label_addres" style="text-align: center;">
                                        <label for="">
                                          Set Price
                                        </label>
                                      </div>
                                      <div class="form-group">
                                        <input type="hidden" id="set_uid" />
                                        <div style="width: 80%;margin:auto">
                                          <input type="text" id="set_price"  maxlength="6" class="form-control" required />
                                          <span class="text-danger"></span>
                                        </div>
                                      </div>
                                      <div class="label_addres" style="text-align: center;">
                                        <label for="">
                                          Complementary Items (Optional)
                                        </label>
                                      </div>
                                      <div class="form-group text_area">
                                        <div style="width: 80%;margin:auto;">
                                          <textarea id="set_compliment"></textarea>
                                          <span class="text-danger"></span>
                                        </div>
                                      </div>
                                    </section>
                                    <div class="common-btn">
                                     <input type="submit" value="UPDATE" class="btn btn-primary" id="update_price">
                                    </div>
                                  </div>
                                  <div class="cross_icon" data-dismiss="modal">
                                    <a href="">
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                      <!-- Modal -->
                      </div>
                </div>
          <input type="hidden" id="users_price">
              </div>
            </div>
      

@endsection
@section('js')

    
      <script src="{{url('public/business/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
      
    <!--   <script type="text/javascript">
        $('.cross_hide').click(function(){
          $('.hide_content').hide();
          // $('.hide_content2').hide();
          // $('.hide_conten3').hide();
        });
         $('.cross_hide2').click(function(){
          $('.hide_content2').hide();
        });
         $('.cross_hide3').click(function(){
          $('.hide_content3').hide();
        });
        $('.adjust_popup').click(function(){
          $('#exampleModalCenter23').modal({
            backdrop: 'static'
          });
        }); 

        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});




  /* set all users price after getting from localstorage*/
  $("#users_price").val(localStorage.getItem("users_price"))

/* parsing number according to digit*/

function parseK(input){
  input = parseInt(input)
  if(input >= 1000000){
    return parseFloat((input / 1000000).toFixed(2)) + "M";
  }else if(input >= 1000){
    return parseFloat((input / 1000).toFixed(2)) + "K";
  }else {
    return input;
  }
}


 /* get new selected users */
 function fetchGigs(url,data = {}){
  let setReach = [];
  let initReach = 0;
  $.ajax({
    url : url,
    method : "POST",
    data : data,
    success:function(res){
      var showRes = "";
      if(res.gigs != "" && res.gigs != null){
        let gigs = res.gigs;
        if(gigs != null && gigs != undefined && gigs.length  > 0){
          gigs.forEach(function(data,i){
            let profile = "{{ url('public/business/images/upload-one.png') }}";
            if(data.images != "" && data.images != null && data.images.length > 0){
              for(let a = 0;a < data.images.length;a++){
                if(data.images[a].image != null || data.images[a].image != "" || data.images[a].image != undefined){
                  profile = data.images[a].image;
                  break;
                }
              }
            }

            i++;
            setReach.push({
              id : data.uid,
              social : data.facebook_friends + data.instagram_friends + data.twitter_friends
            });

            initReach = (data.facebook_friends + data.instagram_friends + data.twitter_friends) + initReach
            let setPrice = (getPrice("get",data.uid).price);
            showRes += (`<div class="col-md-4 gig_container" uid='${data.uid}' >
                                  <div class="white_bg_sec selected_bg_color unslected_bg unslected"> 
                                    <div class="Update-img text-center">
                                      <img src="${profile}" alt="upload-one" class="img-responsive">
                                    </div>
                                    <h5>${data.name ? data.name.substr(0,20) : "User"}</h5>
                                    <div class="border-top">
                                    </div>
                                    <div class="social_flex">
                                      <div class="social_img">
                                        <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                        <p>${data.facebook_friends > 0 ? parseK(data.facebook_friends) : 0}<br> Followers</p>
                                      </div>
                                      <div class="social_img">
                                        <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                        <p>${data.instagram_friends > 0 ? parseK(data.instagram_friends) : 0}<br> Followers</p>
                                      </div>
                                      <div class="social_img border_none">
                                        <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                        <p>${data.twitter_friends > 0 ? parseK(data.twitter_friends) : 0}<br> Followers</p>
                                      </div>
                                    </div>
                                    <div class="border-bottom">
                                    </div>
                                    <div class="date_content">
                                      <span class="equal_width">Desired Post Rate: </span>KSh${data.gig_price > 0 ? data.gig_price : 0}
                                    </div>
                                    <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span><span id="${data.uid}" style='font-weight:normal'>KSh${setPrice}</span>
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup" data-uid = ${data.uid}>ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide">
                                    <a href="#" class='remove-influencer' data-id='${data.uid}'>
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                  </div>
                                </div>`)
          })
        }
      }
      $('.post_list').html(showRes)
      localStorage.setItem("influencer_reach",JSON.stringify(setReach))
      $("#min-influencer-reach").text(parseK(initReach))

    }
  })
}
 let getUsers = localStorage.getItem("selected_users");
fetchGigs("{{url('business/gig-summary-page').'/'.Request::segment(3)}}",{users_id : getUsers});


/* set get price */
function setPrice(id,price,type,compliment = null){
  let prices = $("#users_price");
  let getAllPrices = prices.val();
  let setPrices = [];

  if(getAllPrices != "" && getAllPrices != null){
    setPrices = JSON.parse(getAllPrices);
  }

  if(type == "remove"){
    let delIndex = -1;
     setPrices.map((e,i) => {
      if(e.id == id){
        delIndex = i;
      }
      return e;
    });
     setPrices.splice(delIndex,1);
  }else if(type == "add"){
    let setData = {
      id : id,
      price : price
    };
    setPrices.push(setData);
  }else if(type == "update"){
    setPrices.map((e) => {
      if(e.id == id){
        if(compliment){
           e['compliment'] = compliment;
        }

        e.price = price;
      }
      return e;
    })
  }
  prices.val(JSON.stringify(setPrices))
}

function getPrice(type,id = null){
  let prices = $("#users_price").val();
  let getPrices = [];

  if(prices != "" && prices != null){
    getPrices = JSON.parse(prices);
  }

   if(type == "get"){
    let index = -1;
     getPrices.map((e,i) => {
      if(e.id == id){
        index = i;
      }
      return e;
    });
     return getPrices[index];

 }else if(type == "sum"){
    let allData = {
      "sum" : 0,
      "total" : 0
    }

    getPrices.map((e,i) => {
      allData["sum"] = + allData["sum"] +  + e.price 
      allData["total"] = allData["total"] + 1
    });
    return allData;
 }
}

function setInfluencerData(amount,total){
  $("#total-amount").text("KSh"+amount)
  $("#total-influencer").text(total)
}

let initSet = getPrice("sum");
setInfluencerData(initSet.sum,initSet.total);

function getSetReach(id){
  let getTotalReach = localStorage.getItem("influencer_reach");
  let setReach = 0;
  if(getTotalReach && getTotalReach != ""){
    getTotalReach = JSON.parse(getTotalReach);
    let delIndex = -1;
    getTotalReach.map((e,i) => {
      if(e.id == id){
        delIndex = i
      }else {
        setReach = setReach + e.social;
      }
    })
    getTotalReach.splice(delIndex,1);
    localStorage.setItem("influencer_reach",JSON.stringify(getTotalReach))
  }
  $("#min-influencer-reach").text(parseK(setReach))

}

/* remove influencer*/
$(document).on("click",".remove-influencer",function(){
  event.preventDefault();
  let id = $(this).data("id");
  $(this).closest(".gig_container").remove();
  setPrice(id,null,"remove")
  let setDetails = getPrice("sum");
  setInfluencerData(setDetails.sum,setDetails.total)
  getSetReach(id)
});

/* Adjust price pop up*/
  $(document).on("click",'.adjust_popup',function(){
    if($(this).parent().find(".perfomselectionchange").text() == "SELECT"){
      alert("Please select user for adjust price.");
      return false;
    }

    let uid = $(this).data("uid");
    $("#set_uid").val(uid);

    let getDetail = getPrice("get",uid)

    $("#set_price").val(getDetail.price);
    $("#set_compliment").val(getDetail.compliment != null && getDetail.compliment != undefined ? getDetail.compliment : '');

    $('#open-update-price-pop').modal({
      backdrop: 'static'
    });
  });

   /* set updated price for user*/
    $(document).on("click","#update_price",function(){
          let price = $("#set_price").val();
          let compliment = $("#set_compliment").val();
          if(compliment == ""){
            compliment = $("#set_compliment").text();
          }
          let uid = $("#set_uid").val();
          if(price == "" || price.trim().length == 0){
            alert("Please enter price");
            return false;
          }else if(isNaN(price)){
              alert("Please enter number only");
              return false;
          } else if(price <= 0){
            alert("Price should be equal or higher then 1");
            return false;
          }

          setPrice(uid,price,"update",compliment);
          $("#open-update-price-pop").modal("hide")
           let setDetails = getPrice("sum");
          setInfluencerData(setDetails.sum,setDetails.total)

          $("#"+uid).html("KSh"+parseK(price))

        })


    $("#confirm_booking").on("submit",function(){
      
      if($("#users_price").val() == "" || $("#users_price").val() == "[]"){
        alert("Please select atleast one influencer to confirm gig");
        return false;
      }

      $("input[name='gig_prices']").val($("#users_price").val())
      localStorage.removeItem("users_price")
      localStorage.removeItem("user_filter")
      localStorage.removeItem("influencer_reach")
      localStorage.removeItem("selected_users")
      localStorage.removeItem("next_page")
      return true;
    })

      </script> -->

       <script type="text/javascript">
        $('.cross_hide').click(function(){
          $('.hide_content').hide();
          // $('.hide_content2').hide();
          // $('.hide_conten3').hide();
        });
         $('.cross_hide2').click(function(){
          $('.hide_content2').hide();
        });
         $('.cross_hide3').click(function(){
          $('.hide_content3').hide();
        });
        $('.adjust_popup').click(function(){
          $('#exampleModalCenter23').modal({
            backdrop: 'static'
          });
        }); 

        $( document ).ready(function() {
  console.log( "document ready!" );

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.col-md-3.left_col.menu_fixed');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial'});
      }
    });

  }
});

  /* set all users price after getting from localstorage*/
  $("#users_price").val(localStorage.getItem("users_price"))

/* parsing number according to digit*/

function parseK(input){
  input = parseInt(input)
  if(input >= 1000000){
    return parseFloat((input / 1000000).toFixed(2)) + "M";
  }else if(input >= 1000){
    return parseFloat((input / 1000).toFixed(2)) + "K";
  }else {
    return input;
  }
}


 /* get new selected users */
 function fetchGigs(url,data = {}){
  let setReach = [];
  let initReach = 0;
  $.ajax({
    url : url,
    method : "POST",
    data : data,
    success:function(res){
      var showRes = "";
      if(res.gigs != "" && res.gigs != null){
        let gigs = res.gigs;
        if(gigs != null && gigs != undefined && gigs.length  > 0){
          gigs.forEach(function(data,i){
            let profile = "{{ url('public/business/images/upload-one.png') }}";
            if(data.images != "" && data.images != null && data.images.length > 0){
              for(let a = 0;a < data.images.length;a++){
                if(data.images[a].image != null || data.images[a].image != "" || data.images[a].image != undefined){
                  profile = data.images[a].image;
                  break;
                }
              }
            }

            i++;
            setReach.push({
              id : data.uid,
              social : data.facebook_friends + data.instagram_friends + data.twitter_friends
            });

            initReach = (data.facebook_friends + data.instagram_friends + data.twitter_friends) + initReach
            let setPrice = (getPrice("get",data.uid).price);
             let influencer_url = "{{url('business/influencer-details/')}}/"+btoa(data.uid);
            showRes += (`<div class="col-md-4 gig_container" uid='${data.uid}' >
                                  <div class="white_bg_sec selected_bg_color unslected_bg unslected">
                                   <a href="${influencer_url}"> 
                                    <div class="Update-img text-center">
                                      <img src="${profile}" alt="upload-one" class="img-responsive">
                                    </div>
                                    <h5>${data.name ? data.name.substr(0,20) : "User"}</h5>
                                    <div class="border-top">
                                    </div>
                                    <div class="social_flex">
                                      <div class="social_img">
                                        <img src="{{url('public/business/images/fb_icon.png')}}" alt="fb_icon">
                                        <p>${data.facebook_friends > 0 ? parseK(data.facebook_friends) : 0}<br> Followers</p>
                                      </div>
                                      <div class="social_img">
                                        <img src="{{url('public/business/images/insta_icon.png')}}" alt="insta_icon">
                                        <p>${data.instagram_friends > 0 ? parseK(data.instagram_friends) : 0}<br> Followers</p>
                                      </div>
                                      <div class="social_img border_none">
                                        <img src="{{url('public/business/images/twitter_icon.png')}}" alt="twitter_icon">
                                        <p>${data.twitter_friends > 0 ? parseK(data.twitter_friends) : 0}<br> Followers</p>
                                      </div>
                                    </div>
                                    <div class="border-bottom">
                                    </div>
                                    <div class="date_content">
                                      <span class="equal_width">Desired Post Rate: </span>KSh${data.gig_price > 0 ? data.gig_price : 0}
                                    </div>
                                    </a>
                                    <div class="filter_btns d-flex">
                                    <div class="date_content">
                                      <span class="equal_width" style="width: 70px;">Price Set: </span><span id="${data.uid}" style='font-weight:normal'>KSh${setPrice}</span>
                                    </div>
                                    <button type="button" class="btn btn-primary green_btn adjust_popup" data-uid = ${data.uid}>ADJUST PRICE</button>
                                  </div>
                                  <div class="cross_icon cross_hide">
                                    <a href="#" class='remove-influencer' data-id='${data.uid}'>
                                      <img src="{{url('public/business/images/white_cross_icon.png')}}" alt="white_cross_icon">   
                                    </a>
                                  </div>
                                  </div>
                                </div>`)
          })
        }
      }
      $('.post_list').html(showRes)
      localStorage.setItem("influencer_reach",JSON.stringify(setReach))
      $("#min-influencer-reach").text(parseK(initReach))

    }
  })
}
 let getUsers = localStorage.getItem("selected_users");
fetchGigs("{{url('business/gig-summary-page').'/'.Request::segment(3)}}",{users_id : getUsers});


/* set get price */

function setPrice(id,price,type,compliment = null){
  let prices = $("#users_price");
  let getAllPrices = prices.val();
  let setPrices = [];

  if(getAllPrices != "" && getAllPrices != null){
    setPrices = JSON.parse(getAllPrices);
  }

  if(type == "remove"){
    let delIndex = -1;
     setPrices.map((e,i) => {
      if(e.id == id){
        delIndex = i;
      }
      return e;
    });
     setPrices.splice(delIndex,1);
  }else if(type == "add"){
    let setData = {
      id : id,
      price : price
    };
    setPrices.push(setData);
  }else if(type == "update"){
    setPrices.map((e) => {
      if(e.id == id){
        if(compliment){
           e['compliment'] = compliment;
        }
        e.price = price;
      }
      return e;
    })

  }

  prices.val(JSON.stringify(setPrices))

  localStorage.setItem("_curSelUserPrs",JSON.stringify(setPrices))
}

function getPrice(type,id = null){
  let prices = $("#users_price").val();
  let getPrices = [];

  if(prices != "" && prices != null){
    getPrices = JSON.parse(prices);
  }

   if(type == "get"){
    let index = -1;
     getPrices.map((e,i) => {
      if(e.id == id){
        index = i;
      }
      return e;
    });
     return getPrices[index];

 }else if(type == "sum"){
    let allData = {
      "sum" : 0,
      "total" : 0
    }

    getPrices.map((e,i) => {
      allData["sum"] = + allData["sum"] +  + e.price 
      allData["total"] = allData["total"] + 1
    });
    return allData;
 }
}

function setInfluencerData(amount,total){
  $("#total-amount").text("KSh"+amount)
  $("#total-influencer").text(total)
}

let initSet = getPrice("sum");
setInfluencerData(initSet.sum,initSet.total);

function getSetReach(id){
  let getTotalReach = localStorage.getItem("influencer_reach");
  let setReach = 0;
  if(getTotalReach && getTotalReach != ""){
    getTotalReach = JSON.parse(getTotalReach);
    let delIndex = -1;
    getTotalReach.map((e,i) => {
      if(e.id == id){
        delIndex = i
      }else {
        setReach = setReach + e.social;
      }
    })
    getTotalReach.splice(delIndex,1);
    localStorage.setItem("influencer_reach",JSON.stringify(getTotalReach))
  }
  $("#min-influencer-reach").text(parseK(setReach))

}

/* remove influencer*/
$(document).on("click",".remove-influencer",function(){
  event.preventDefault();
  let id = $(this).data("id");
  $(this).closest(".gig_container").remove();
  setPrice(id,null,"remove")
  let setDetails = getPrice("sum");
  setInfluencerData(setDetails.sum,setDetails.total)
  getSetReach(id)
  pushPopUsers(id)
});

function pushPopUsers(id){
  /* if user id exist then remove id else add user id*/
  let val = localStorage.getItem("_curSelUser");
  let ids = [];
  if(val != ""){
    ids = val.split(",").map(e => parseInt(e));
  }

  let index = ids.indexOf(id);
  if(index != -1){
    ids.splice(index,1);
  }

  let putId = ids.join(",");

  localStorage.setItem("_curSelUser",putId)
}

/* Adjust price pop up*/
  $(document).on("click",'.adjust_popup',function(){
    if($(this).parent().find(".perfomselectionchange").text() == "SELECT"){
      alert("Please select user for adjust price.");
      return false;
    }

    let uid = $(this).data("uid");
    $("#set_uid").val(uid);

    let getDetail = getPrice("get",uid)

    $("#set_price").val(getDetail.price);
    $("#set_compliment").val(getDetail.compliment != null && getDetail.compliment != undefined ? getDetail.compliment : '');

    $('#open-update-price-pop').modal({
      backdrop: 'static'
    });
  });

   /* set updated price for user*/
    $(document).on("click","#update_price",function(){
          let price = $("#set_price").val();
          let compliment = $("#set_compliment").val();
          if(compliment == ""){
            compliment = $("#set_compliment").text();
          }
          let uid = $("#set_uid").val();
          if(price == "" || price.trim().length == 0){
            alert("Please enter price");
            return false;
          }else if(isNaN(price)){
              alert("Please enter number only");
              return false;
          } else if(price <= 0){
            alert("Price should be equal or higher then 1");
            return false;
          }

          setPrice(uid,price,"update",compliment);
          $("#open-update-price-pop").modal("hide")
           let setDetails = getPrice("sum");
          setInfluencerData(setDetails.sum,setDetails.total)

          $("#"+uid).html("KSh"+parseK(price))

        })


      $("#confirm_booking").on("submit",function(){
        event.preventDefault();
      if($("#users_price").val() == "" || $("#users_price").val() == "[]"){
        alert("Please select atleast one influencer to confirm post");
        return false;
      }



     

       let data  = {
              "gig_prices" : $("#users_price").val(),
              "_token" : "{{csrf_token()}}",
              "is_save" : true
          }

      $("#paymentPop").modal("show");

      let getCreatedId = localStorage.getItem("create_id");

      if(!getCreatedId){
        console.log("mnpt have ",getCreatedId)
          localStorage.removeItem("users_price")
          localStorage.removeItem("user_filter")
          localStorage.removeItem("influencer_reach")
          localStorage.removeItem("selected_users")
          localStorage.removeItem("next_page")
          localStorage.removeItem("_curSelUserPrs")
          localStorage.removeItem("_curSelUser")

          $("input[name='gig_prices']").val()
          $("#res-message,#mPesa-container,#success-pop").hide();

          $.ajax({
            url : "{{url('business/gig-summary-page').'/'.Request::segment(3)}}",
            type : "POST",
            data : data,
            beforeSend : function(){
              $(document).find("#save-mphone").attr("disabled",true)
            },
            complete : function(){
              $(document).find("#save-mphone").removeAttr("disabled")
            },
            success : function(res,msg,xhr){
              if(xhr.status == 201){
                /*show pop up here*/
                setTimeout(function(){
                  $("#success-pop").show()
                },1000)
               $("#created_id").val(res.created);
                $(document).find("#save-mphone").text("Process Payment")

                 $(document).find("#success-pop .alert")
                .addClass("alert-success")
                .removeClass("alert-danger")
                .text("Please proceed for the payment to create a Gig.")

                localStorage.setItem("create_id",res.created + "_"+res.data.phone_number)

              }else if(xhr.status == 202){
                $("#res-message").show().text("Please set your mPesa phone number to make payment");
                $("#created_id").val(res.created);
                $("#mPesa-container").show()
                $("#success-pop .alert").text("Please proceed for the payment to create a Gig.")
                setTimeout(function(){
                  $("#success-pop").show()
                },1000)
                localStorage.setItem("create_id",res.created + "_")
              }else {
                alert("Unable to proceed your request, please try later.");
                window.location.href = "{{url('business/gig-list')}}"
              }
            },error : function(err){

            }
          })
        } else {
          let createdId = getCreatedId.split("_")[0];
          let phone = getCreatedId.split("_")[1];

          setTimeout(function(){
                $("#success-pop").show()
          },1000)
          if(phone == "" && phone.length == 0){
            $("#res-message").show().text("Please set your mPesa phone number to make payment");
            $("#created_id").val(createdId);
            $("#mPesa-container").show()
            $("#success-pop .alert").text("Please proceed for the payment to create a Gig.")
          }else {
            $("#created_id").val(createdId);
            $(document).find("#save-mphone").text("Process Payment")
          }
        }
      return false;
      // $("input[name='gig_prices']").val($("#users_price").val())
      
      // return true;
    });

  </script>

<!-- Payment Model -->

<!-- #paymentPop .alert-success
#paymentPop .modal-footer #save-mphone
 -->

<div id="paymentPop" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" id="close-pop">&times;</button>
        <h2 class="modal-title text-center" style="font-size: 24px;font-weight: bold;">
          Make Payment
        </h2>
      </div>
      <div class="modal-body">
        <div class="form-group" id="success-pop" style="display: none;">
          <div style="padding:11px;font-weight: 400;" class="alert alert-success text-center">
            Please proceed for the payment to create a Gig.
          </div>
        </div>
        <span id="res-message"></span>
        <input type="hidden" id="created_id" />
        <!-- <div class="form-group" id="mPesa-container" style="display: none;">
          <input type="number" id="mphone" class="form-control" />
          <label class="error"></label>
        </div> -->
             <div class="form-group" id="mPesa-container" style="display: none;">
        <div class="form-group">
            <div class="email">
               <input type="text" id="mphone" name="title" value="" class="form-control" placeholder="" maxlength="15" onkeypress="return onlyNumber(event)">
               <span class="text-danger"></span>
            </div>
             <label class="error"></label>
         </div>
         
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="save-mphone">Submit</button>
      </div>
    </div>
  </div>
</div>

<!--  <script type="text/javascript">
   $("#paymentPop").modal("show")
 </script> -->

<script type="text/javascript">
  localStorage.removeItem("uid")
  function checkPaymenStatus(uid){
    if(uid){
      $.ajax({
          url : "{{url('business/get-business-payment-status')}}/" + uid,
          type : "GET",
          timeout: 5000,
          success : function(res,msg,xhr){
            if(xhr.status == 200){
              window.location.href = "{{url('business/gig-list')}}"
            }else if(xhr.status == 203) {
             alert("Payment Failed");
             window.location.href = "{{url('business/gig-list')}}"
           }
          },error:function(error){
            console.log("error in ajax ",error)
          }
        })
      }
  }

/*  $(document).on("keyup paste keydown","#mphone",function(e){
        if($(this).val().length == 15){
          return false;
        }
      });*/

  $("#save-mphone").click(function(){

    let type = $(this).text();
    let _this = $(this)

    if(type == "Process Payment"){

      let getPayload = async function(){
            return {
                "_token" : "{{ csrf_token() }}",
                post_id : await $("#created_id").val(),
                type : "gig"
            }
       }
      getPayload()
      .then((data)=>{
      $("#mPesa-container").show().html("<div class='loader'></div>")

      $.ajax({
            url : "{{url('business/make-mpesa-payment')}}",
            type : "POST",
            data :data ,
            beforeSend : function(){
              $(document).find("#save-mphone").attr("disabled",true)

            },
            complete : function(){
              $(document).find("#save-mphone").removeAttr("disabled")
            },
            success : function(res,msg,xhr){
              console.log("res",res)
              localStorage.setItem("payment_completed",1)
              if(xhr.status == 200){
                $("#paymentPop h2.modal-title").text("Payment Under Process");
                // $("#mPesa-container").hide()
                $("#success-pop .alert").removeClass("alert-danger")
                  .addClass("alert-success")
                  .html("A confirmation message has been sent to your mPesa phone number, Please confirm to continue.<br/><span class='text-center'>Please do not refresh or press back<span>");

                  $("#close-pop").hide()
                  localStorage.removeItem("created_id");

                $(document).find("#save-mphone").text("Go to List Page").hide();
                let uid = res.uid;
                
                setInterval(function(){
                  checkPaymenStatus(uid);
                },5000);

              }else if(xhr.status == 204) {
                $("#mPesa-container").hide()
                $("#mPesa-container").hide()
                $("#success-pop .alert").removeClass("alert-success").addClass("alert-danger").text("Unexpected error while payment process");
              }else {
                $("#mPesa-container").hide()
                alert("Unable to proceed your request, please try later");
              }
            },
            error : function(){
               $("#mPesa-container").hide()
                alert("Unable to proceed your request, please try later");
            }
        })
      })

    } else if(type == "Go to List Page"){
      window.location.href = "{{url('business/gig-list')}}";
    }else {
      let phone = $("#mphone").val();
      if(phone.length == 0){
        alert("Please enter phone number.");
        return false;
      }else if(isNaN(phone)){
        alert("Please enter number only.");
        return false;
      }else if(phone.length < 8){
        alert("Phone should be atleast 8 digits long.");
        return false;
      }else {
        let data = {
          phone : phone,
          "_token" : "{{ csrf_token() }}"
        }

        $.ajax({
            url : "{{url('business/set-mpesa-number')}}",
            type : "POST",
            data : data,
            beforeSend : function(){
              $(document).find("#save-mphone").attr("disabled",true)

            },
            complete : function(){
              $(document).find("#save-mphone").removeAttr("disabled")
              $("#mPesa-container").hide()
              $(document).find("#res-message").hide()
            },
            success : function(res,msg,xhr){
              if(xhr.status == 200){
                let createdData = localStorage.getItem("create_id");
                if(createdData){
                  let id = createdData.split("_")[0];
                  localStorage.setItem("create_id",id + "_" + $("#mphone").val())
                  $("#created_id").val(id);
                  
                  $("#success-pop .alert").text("Phone number set successfully.")
                  setTimeout(function(){
                    $("#success-pop").show()
                  },1000)

                  $(document).find("#save-mphone").text("Process Payment")

                }
              }else {
                alert("Unable to proceed your request, please try later");
              }
            }
        })
      }  
    }
  })

  $(document).on("click","#close-pop",function(){
    if($(this).attr("is-redirect")){

    }else {
      $("#paymentPop").modal("hide")
    }
  })
      function onlyNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        }
    return true;
}
</script>

   @endsection