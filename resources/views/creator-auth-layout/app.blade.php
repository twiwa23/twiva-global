<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('creative/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('creative/assets/css/style.css') }}">
    <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">

    @yield('styles')
</head>

<body>
    @yield('content')


    <script src="{{ asset('creative/assets/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('creative/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('creative/assets/js/custom.js') }}"></script>
    @yield('scripts')
</body>

</html>
