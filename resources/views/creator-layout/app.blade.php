<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('creative/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('creative/assets/css/style.css') }}">
    <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
    @yield('styles')
</head>

<body>

    <!-- Top Navbar Start -->
    <header class="header-section">

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button">
            <img src="{{ asset('creative/assets/images/icons/toggle-icon.svg') }}" alt="">
        </button>

        <!-- Navbar Logo -->
        <div class="logo">
            <a href="/"><img src="{{ asset('creative/assets/images/logo.svg') }}"></a>
        </div>

        <!-- Navbar Menu -->
        <div class="top_nav">
            <nav class="">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile-img">
                                @if (!empty(auth()->user()->profile))
                                    <img src="{{ auth()->user()->profile }}" alt="">
                                @else
                                    <img src="{{ asset('business/extra/images/profile.jpeg') }}" alt="">
                                @endif
                            </div>
                            {{ auth()->user()->name ?? 'User'}}
                            <img src="{{ asset('creative/assets/images/icons/chevron-down-white.svg') }}" alt="" class="down-arrow">
                        </a>

                        <ul class="dropdown-menu">
                            <li class="header-company-name-wrapper">
                                <p class="company-name">{{ auth()->user()->name ?? 'User' }}</p>
                                <p class="email">{{ auth()->user()->email ?? 'Email' }}</p>
                            </li>
                            <li class="logout">
                                <a href="{{ url('user/userlogout') }}" id="logout-btn">Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>

    </header>
    <!-- Top Navbar End -->
    <div class="backdrop"></div>
    @yield('content')


    <script src="{{ asset('creative/assets/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('creative/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('creative/assets/js/custom.js') }}"></script>

    @yield('scripts')

</body>

</html>
