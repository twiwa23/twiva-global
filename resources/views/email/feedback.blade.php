@extends('creator-auth-layout.app')
@section('title')
{{$title}}
@endsection
@section('content')
<div class="login">

    <div class="container-fluid m-0">
        <div class="back-button">
            <a href="{{url('/user/creators')}}"><button id="desktop-btn"><img
                src="{{ asset('creative/assets/images/icons/chevron-left-white.svg') }}"
                alt="">Back</button></a>
            <a href="{{url('/user/creators')}}"><button id="mobile-btn"><img
                src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Back</button></a>
        </div>
    </div>

    <div class="container" id="step">
        <div class="login-inner">

            <div class="login-left">
                <!-- <img src="../images/banner/login.png"> -->
            </div>

            <div class="login-right">
                <div class="login-section">

                    <div class="logo"><img src="{{ asset('creative/assets/images/logo-red.svg') }}"></div>
                    @php
                    $color = $type == 'success' ? 'green-hightlight' : 'red-hightlight';
                    @endphp
                    <div class="{{ $color }}">{{ $message }}</div>

                    <a href="{{url('/user/login')}}"><button class="red-bttn" id="submitbtn" >Login</button></a>

                </div>
            </div>

        </div>
    </div>

    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="#">Terms & Conditions</a>
    </div>

</div>
@endsection
