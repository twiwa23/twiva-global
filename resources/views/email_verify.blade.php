<div style="margin-bottom: 20px;text-align: center;">
	<img src="{{url('public/admin/production/images/twiva.png')}}" alt="Twiva" height="100" width="100"/>
</div>
<p>Hello <?php echo $user_data->name;?>,</p>
<p>Thanks for signing up to Twiva. We're happy to have you.</p>
<p>Please take a second to make sure we have your correct email address.</p> <br />
<a href="<?php echo $url;?>" class="new-form" style="background-color: #0e7aa7; padding: 12px 15px;color: #fff; text-decoration: none; border-radius: 10px;">Confirm your email address</a>
<br> <br> 
<p>Didn't sign up for Twiva?<span style="color: #0e7aa7;">
<a href = "{{url('/contact-us')}}"> Let us know.</a></span> </p>
<br> <br>
<ul style="margin: 0; padding: 0;">
<li style="display: block; list-style: none; padding-bottom: 7px;">Cheers,</li>
<li style="display: block; list-style: none; padding-bottom: 7px;">Twiva Team</li>
<li style="display: block; list-style: none; padding-bottom: 7px;">0708088114</li>
<li style="display: block; list-style: none; padding-bottom: 7px;"><a style="text-decoration: none;" href="mailto:info@twiva.co.ke">info@twiva.co.ke</a></li>
<li style="display: block; list-style: none; padding-bottom: 7px;"><a style="text-decoration: none;" href="http://twiva.co.ke/">http://twiva.co.ke/</a></li>
</ul>

