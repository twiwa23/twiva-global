<div class="table-responsive">
                        <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>IMAGE</th>
                                    <th>TITLE</th>
                                    <th>EXPIRATION DATE</th>
                                    <th>COMPENSATION</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if(count($posts)>0)
                            <?php //print'<pre>';print_R($posts);exit; ?>
                                @foreach($posts as $post)
                                <tr>
                                    <td>
                                        <div class="user-image">
                                        @if(!empty($post->Post->Images[0]->media))
                                        <img src="{{$post->Post->Images[0]->media}}" alt="Post Image">
                                         @else
                                        <img src="{{url('public/business/images/default.svg')}}" alt="">
                                        @endif
                                        </div>
                                    </td>
                                    <td class="user-name font-weight"> <a href="{{asset('/influencers/accepted/posts/details/'.$post->post_id)}}"> {{$post->Post->post_name}} </a></td>
                                    <td class="post-rate">{{$post->Post->expiration_date ?? 'NA'}}</td>
                                    <td class="post-rate">KSH{{$post->price->price}} </td>
                                    @if($post->status==5)
                                    @if($post->getPayment==2)
                                    <td class="completed">
                                        <p>Payment Recieved </p>
                                    </td>
                                    @else
                                        <td class="ongoing">
                                            <p>Payment Pending</p>
                                        </td>
                                    @endif
                                    @else
                                        @if($post->is_submit==1)
                                        <td class="completed">
                                            <p>Submitted</p>
                                        </td>
                                        @elseif($post->status==4)
                                        <td class="declined">
                                            <p>Rejected</p>
                                        </td>
                                        @elseif($post->status==2)
                                        <td class="declined">
                                            <p>Declined</p>
                                        </td>
                                        @else
                                        <td class="ongoing">
                                            <p>Submit</p>
                                        </td>
                                        @endif
                                    @endif

                                    <td>
                                        <div class="action-btns">
                                            @if($post->status!=5)                                         
                                                @if($post->is_submit==1)
                                                <!-- <button class="table-red-btn" disabled>Submitted</button> -->
                                                <a href="{{asset('influencers/accepted/posts/view-post/'.$post->post_id)}}" class="table-red-btn">View Post</a>
                                                @elseif ($post->status==2)
                                                <a href="{{asset('influencers/accepted/posts/resubmit-post-view/'.$post->post_id)}}" class="table-red-btn">Resubmit</a>
                                                @elseif ($post->status==4)
                                                <a href="{{asset('influencers/accepted/posts/resubmit-post-view/'.$post->post_id)}}" class="table-red-btn">Resubmit</a>
                                                @else
                                                <a href="{{asset('influencers/accepted/posts/start-post/'.$post->post_id)}}" class="table-red-btn">Start Post</a>   
                                                @endif
                                            @endif  
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center p-5 border border-0" colspan="7">
                                        <img src="{{asset('influencers/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                        <h3 style="font-size: 20px;">No Data Found!</h3>
                                    </td>
                                </tr>   
                            @endif 

                            </tbody>

                        </table>
                    </div>

                    <div class="pagination">
                        <div class="col-sm-5">
                            <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                            <div class="">
                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                    <ul class="pagination">
                                       {{$posts->links()}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>