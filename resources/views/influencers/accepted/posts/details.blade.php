@extends('influencers.layout.app')
@section('title')
My Invites Posts
@endsection
@section('content')
<div class="right_col feedpost-details-page" role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="active">Post<img src="{{asset('influencers/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        <li><a href="#">{{$post->post_name}}</a></li>
                    </ul>
                </div>
                <!--********** Breadcrumb End ***********-->

                <!-- <div class="mobile-breadcrumb-wrapper">
                    <ul class="mobile-breadcrumb">
                        <li><a href="#" class="breadcrumb-heading"><img src="./assets/images/icons/chevron-left.svg" alt="">Feedpost Details</a></li>
                    </ul>
                </div> -->



                <div class="post-container custom-style">
                    <div class="post-heading">
                        <h3>{{$post->post_name}}</h3>
                    </div>

                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label for="desc">Description</label>
                                        <textarea name="" id="desc" class="form-control custom-h" readonly>{{$post->post_name}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Compensation</label>
                                        <input type="text" class="form-control" value="{{$post->price_per_post}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Expiration Date</label>
                                        <input type="text" name="date" class="form-control" value="{{$post->expiration_date}}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label for="to-do">What To Do</label>
                                        <textarea name="" id="to-do" class="form-control custom-h" readonly>{{$post->what_to_include}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="not-to-do">What Not To Do</label>
                                        <textarea name="" id="not-to-do" class="form-control custom-h" readonly>{{$post->what_not_to_include}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="other-req">Other Requirements</label>
                                        <textarea name="" id="other-req" class="form-control custom-h" readonly>{{$post->other_requirement}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="post-input-container">
                        <div class="row" style="align-items: flex-end;">

                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Theme</label>
                                        <div class="form-control custom-form" style="height: 99px;">
                                        
                                            @foreach($post->Images as $image)
                                            <div class="image-container">
                                                @if(!empty($image->media))
                                                <img src="{{$image->media}}" alt="">
                                                @else
                                                <img src="{{url('public/business/images/default.svg')}}" alt="">
                                                @endif
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Hashtag</label>
                                        <input type="text" class="form-control" value="#{{$post->hashtag}}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

    <!-- The Modal -->
    <div class="modal fade" id="decline-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Please state your reason for declining the Twitter Trend</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>
        
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <button class="red-btn"  data-toggle="modal" data-target="#lowPayModal" data-dismiss="modal">Low Pay</button>
                        </li>
                        <li class="list-item">
                        <form action="{{asset('influencers/my-invites/posts/reject')}}" method="POST" style="margin-bottom:0;">
                        <input type="hidden" name="post_id" class="post_id">
                        <input type="hidden" name="reason" value="Conflict of Interests">  
                            <button type="submit" class="red-btn">Conflict of Interests</button>
                        </form>
                        </li>
                        <li class="list-item">
                        <form action="{{asset('influencers/my-invites/posts/reject')}}" method="POST" style="margin-bottom:0;">
                        <input type="hidden" name="post_id" class="post_id">
                        <input type="hidden" name="reason" value="Not Interested"> 
                            <button class="red-btn" type="submit">Not Interested</button>
                        </form>
                        </li>
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="white-btn" data-dismiss="modal">Cancel</button>
                </div>
        
            </div>
        </div>
    </div> 

    <!-- The Modal -->
    <div class="modal fade" id="lowPayModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Apply</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{asset('/my-invites/posts/reject')}}" method="POST" style="margin-bottom:0;">
                    <input type="hidden" name="post_id" class="post_id">
                    <input type="hidden" name="reason" value="Low Pay">     
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <div class="form-group">
                                <label for="enterPrice">Enter Price</label>
                                <input type="text" class="form-control" id="enterPrice" name="reject_amount" required>
                            </div>
                        </li>
                        <!-- <li class="list-item">
                            <div class="form-group">
                                <label for="compliment">Enter complementary items</label>
                                <textarea class="form-control" rows="5" id="compliment"></textarea>
                            </div>
                        </li> -->
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer justify-content-center">
                    <button type="submit" class="red-btn w-100">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>

$(document).ready(function(){
    $(".decline").on('click' , function(){
    console.log($(this).data("id"));
    $(".post_id").val($(this).data("id"));
    });
});
</script>
@endsection