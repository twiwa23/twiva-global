@extends('influencers.layout.app')
@section('title')
Resubmit  Post Details
@endsection
@section('content')
            <div class="right_col dashboard-page" role="main" id="send-post">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="active">Accepted/Posts/<img src="{{asset('influencers/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        <li><a href="#">{{$post->post_name}}</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
                
                <div class="post-container custom-style">
                    <div class="post-heading">
                        <h3>{{$post->post_name}}</h3>
                        <div class="btn-section">
                            <button class="dropdown-arrow" data-toggle="collapse" data-target="#collapseForm" aria-expanded="false" aria-controls="collapseExample"><img src="{{asset('influencers/assets/images/icons/drop-arrow.svg')}}"></button>
                        </div>
                    </div>

                    <div class="collapse" id="collapseForm">
                        <div class="post-input-container">
                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label for="desc">Description</label>
                                            <textarea  id="desc" class="form-control custom-h" disabled>{{$post->description}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Compensation</label>
                                            <input type="text" class="form-control" value="{{$post->post_name}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Expiration Date</label>
                                            <input type="text" name="date" class="form-control" value="{{$post->expiration_date}}" disabled>
                                        </div>
                                    </div>
                                </div>
    
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label for="to-do">What To Do</label>
                                            <textarea name="" id="to-do" class="form-control custom-h" disabled>{{$post->what_to_include}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="not-to-do">What Not To Do</label>
                                            <textarea name="" id="not-to-do" class="form-control custom-h" disabled>{{$post->what_not_to_include}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="other-req">Other Requirements</label>
                                            <textarea name="" id="other-req" class="form-control custom-h" disabled>{{$post->other_requirement}}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-input-container">
                            <div class="row" style="align-items: flex-end;">
    
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label>Theme</label>
                                            <div class="form-control custom-form" style="height: 99px;">
                                            @foreach($post->Images as $image)
                                            <div class="image-container">
                                                @if(!empty($image->media))
                                                <img src="{{$image->media}}" alt="">
                                                @else
                                                <img src="{{url('public/business/images/default.svg')}}" alt="">
                                                @endif
                                            </div>
                                            @endforeach
                                                <!-- <div class="image-container">
                                                    <img src="{{asset('influencers/assets/images/profile-images/rectangle-700.png')}}" alt="">
                                                </div> -->
                                                <!-- <div class="image-container">
                                                    <img src="{{asset('influencers/assets/images/profile-images/rectangle-710.png')}}" alt="">
                                                </div>
                                                <label for="files" class="upload-field file-upload">
                                                    <input type="file" id="files" name="files[]" multiple="">
                                                </label> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label>Hashtag</label>
                                            <input type="text" class="form-control" value="#{{$post->hashtag}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="post-details" id="addPost">
                @if(session()->has('message'))
                    <div class="alert alert-danger">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <form action="{{asset('influencers/accepted/posts/send-for-approval')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <input type="hidden" name="post_user_id" value="{{$post->user_id}}">
                <div class="send-approval">
                        <h4>Add Post Image :</h4>
                        <button type="submit" class="red-btn">Resubmit</button>
                    </div>
                    <div class="catalog-form m-0 p-0" >
                        <div class="form-field-full">
                            <div class="browse-items m-0">
                                
                                <div class="browse-ss position-relative">
                                    <label for="image1">
                                        <input type="file" id="image1" name="media[]" onchange="image_1(this)">
                                        <span class="addImg"><img src="{{asset('influencers/assets/images/icons/plus-img.svg')}}" alt=""></span>
                                        <div class="product-additional-image">
                                            <img id="img1" alt="">
                                        </div>
                                    </label>
                                    <span class="image-delete1 img-delete" data-name='image1'>
                                        <img src="{{asset('influencers/assets/images/icons/x.svg')}}" alt="">
                                    </span>
                                </div>
                
                                <div class="browse-ss position-relative">
                                    <label for="image2">
                                        <input type="file" name="media[]" id="image2" onchange="image_2(this)">
                                        <span class="addImg"><img src="{{asset('influencers/assets/images/icons/plus-img.svg')}}" alt=""></span>
                                        <div class="product-additional-image">
                                            <img id="img2" alt="">
                                        </div>
                                    </label>
                                    <span class="image-delete2 img-delete" data-name='image2'>
                                        <img src="{{asset('influencers/assets/images/icons/x.svg')}}" alt="">
                                    </span>
                                </div>
                
                                <div class="browse-ss position-relative">
                                    <label for="image3">
                                        <input type="file" id="image3" name="media[]" onchange="image_3(this)">
                                        <span class="addImg"><img src="{{asset('influencers/assets/images/icons/plus-img.svg')}}" alt=""></span>
                                        <div class="product-additional-image">
                                            <img id="img3" alt="">
                                        </div>
                                    </label>
                                    <span class="image-delete3 img-delete" data-name='image3'>
                                        <img src="{{asset('influencers/assets/images/icons/x.svg')}}" alt="">
                                    </span>
                                </div>
                
                                <!-- <div class="browse-ss position-relative">
                                    <label for="image4">
                                        <input type="file" id="image4"  name="media[]" onchange="image_4(this)">
                                        <span class="addImg"><img src="{{asset('influencers/assets/images/icons/plus-img.svg')}}" alt=""></span>
                                        <div class="product-additional-image">
                                            <img id="img4" alt="">
                                        </div>
                                    </label>
                                    <span class="image-delete4 img-delete" data-name='image4'>
                                        <img src="{{asset('influencers/assets/images/icons/x.svg')}}" alt="">
                                    </span>
                                </div> -->
                            </div>
                        </div>               
                    </div>
                    <h4 class="add-caption">Add Caption:</h4>

                    <div class="caption-lists">
                        <div class="caption-list-item" style="width: 402px">
                            <div>
                                <label for="caption-1" class="caption-label">Caption 1</label>
                            </div>
                            <input type="text" name="text_one" class="w-100 form-control" style="background: #F8F8F8;border-radius: 4px; border:0;" value="{{$getPostDetails->text_one}}" required>
                        </div>

                        <div class="caption-list-item" style="width: 402px">
                            <div>
                                <label for="caption-2" class="caption-label">Caption 2</label>
                            </div>
                            <input type="text" name="text_two" class="w-100 form-control" style="background: #F8F8F8;border-radius: 4px; border:0;" value="{{$getPostDetails->text_two}}" required >
                        </div>

                        <div class="caption-list-item" style="width: 402px">
                            <div>
                                <label for="caption-3"  class="caption-label">Caption 3</label>
                            </div>
                            <input type="text" name="text_three" class="w-100 form-control" style="background: #F8F8F8;border-radius: 4px; border:0;" value="{{$getPostDetails->text_three}}" required >
                        </div>
                    </div>
                </form>
                </div>

            </div>
@endsection
@section('script')
<script>
$(document).on("click",".image-delete1",function(){
    var image = $(this).attr('data-name');
    $('#img1').removeAttr('src');
    $('.image-delete1').removeClass('d-flex');
    $("#image1").val('');
});
$(document).on("click",".image-delete2",function(){
    var image = $(this).attr('data-name');
    $('#img2').removeAttr('src');
    $('.image-delete2').removeClass('d-flex');
    $("#image2").val('');
});
$(document).on("click",".image-delete3",function(){
    var image = $(this).attr('data-name');
    $('#img3').removeAttr('src');
    $('.image-delete3').removeClass('d-flex');
    $("#image3").val('');
});
$(document).on("click",".image-delete4",function(){
    var image = $(this).attr('data-name');
    $('#img4').removeAttr('src');
    $('.image-delete4').removeClass('d-flex');
    $("#image4").val('');
});
</script>
@endsection