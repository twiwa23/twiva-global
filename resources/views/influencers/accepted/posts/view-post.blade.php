@extends('influencers.layout.app')
@section('title')
View Post
@endsection
@section('content')
            <div class="right_col dashboard-page" role="main" id="send-post">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="active">Accepted/Posts/<img src="{{asset('influencers/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        <li><a href="#">{{$post->post_name}}</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
                
                <div class="post-container custom-style">
                    <div class="post-heading">
                        <h3>{{$post->post_name}}</h3>
                        <div class="btn-section">
                            <button class="dropdown-arrow" data-toggle="collapse" data-target="#collapseForm" aria-expanded="false" aria-controls="collapseExample"><img src="{{asset('influencers/assets/images/icons/drop-arrow.svg')}}"></button>
                        </div>
                    </div>

                    <div class="collapse" id="collapseForm">
                        <div class="post-input-container">
                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label for="desc">Description</label>
                                            <textarea  id="desc" class="form-control custom-h" disabled>{{$post->description}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Compensation</label>
                                            <input type="text" class="form-control" value="{{$post->post_name}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Expiration Date</label>
                                            <input type="text" name="date" class="form-control" value="{{$post->expiration_date}}" disabled>
                                        </div>
                                    </div>
                                </div>
    
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label for="to-do">What To Do</label>
                                            <textarea name="" id="to-do" class="form-control custom-h" disabled>{{$post->what_to_include}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="not-to-do">What Not To Do</label>
                                            <textarea name="" id="not-to-do" class="form-control custom-h" disabled>{{$post->what_not_to_include}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="other-req">Other Requirements</label>
                                            <textarea name="" id="other-req" class="form-control custom-h" disabled>{{$post->other_requirement}}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-input-container">
                            <div class="row" style="align-items: flex-end;">
    
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label>Theme</label>
                                            <div class="form-control custom-form" style="height: 99px;">
                                            @foreach($post->Images as $image)
                                            <div class="image-container">
                                                @if(!empty($image->media))
                                                <img src="{{$image->media}}" alt="">
                                                @else
                                                <img src="{{url('public/business/images/default.svg')}}" alt="">
                                                @endif
                                            </div>
                                            @endforeach
                                                <!-- <div class="image-container">
                                                    <img src="{{asset('influencers/assets/images/profile-images/rectangle-700.png')}}" alt="">
                                                </div> -->
                                                <!-- <div class="image-container">
                                                    <img src="{{asset('influencers/assets/images/profile-images/rectangle-710.png')}}" alt="">
                                                </div>
                                                <label for="files" class="upload-field file-upload">
                                                    <input type="file" id="files" name="files[]" multiple="">
                                                </label> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-sm-12 col-lg-6">
                                    <div class="input-section">
                                        <div class="form-group">
                                            <label>Hashtag</label>
                                            <input type="text" class="form-control" value="#{{$post->hashtag}}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="post-details">
                    <div class="send-approval">
                        <h4>Post</h4>
                        @if($post_user->status==3)
                            <div class="blue-box">
                                Approved
                            </div>
                        @else
                            <div class="blue-box">
                                Awaiting Approval
                            </div>
                        @endif
                    </div>
                    

                    <ul class="img-lists">
                        @foreach($postviewmedia as $image)
                        @if($image->media)
                        <div class="image-container">
                            <img src="{{$image->media}}" alt="Post Image">
                        </div>
                        @else
                        <div class="image-container">
                            <img src="{{asset('/public/business/images/default.svg')}}" alt="Post Image">
                        </div>
                        @endif
                        @endforeach
                       
                    </ul>

                    <h4 class="add-caption">Suggested Captions:</h4>

                    <div class="caption-lists">
                        <div class="caption-list-item">
                            <div>
                                <label for="caption-1" class="caption-label">Caption 1</label>
                            </div>
                            <p>{{$postview->text_one}}</p>
                        </div>

                        <div class="caption-list-item">
                            <div>
                                <label for="caption-2" class="caption-label">Caption 2</label>
                            </div>
                            <p>{{$postview->text_two}}</p>
                        </div>

                        <div class="caption-list-item">
                            <div>
                                <label for="caption-3" class="caption-label">Caption 3</label>
                            </div>
                            <p>{{$postview->text_three}}</p>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('script')

@endsection