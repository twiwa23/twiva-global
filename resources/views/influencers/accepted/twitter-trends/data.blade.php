<div class="table-responsive">
                        <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>IMAGE</th>
                                    <th>TITLE</th>
                                    <th>START DATE</th>
                                    <th>END DATE</th>
                                    <th>COMPENSATION</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if(count($tts)>0)
                                @foreach($tts as $tt)
                                <tr>
                                    <td>
                                        <div class="user-image">
                                        @if($tt->Gig->Images[0]->media)
                                        <img src="{{$tt->Gig->Images[0]->media}}" alt="Thumbnail Image">
                                        @else
                                        <img src="{{asset('influencers/assets/images/product-img/Rectangle-263.png')}}" alt="Thumnail Image">
                                        @endif
                                        </div>
                                    </td>
                                    <td class="user-name font-weight"><a href="{{asset('/influencers/accepted/twitter-trends/details/'.$tt->gig_id)}}">{{$tt->Gig->gig_name}}</a></td>
                                    <td class="post-rate">{{$tt->Gig->gig_start_date_time}}</td>
                                    <td class="post-rate">{{$tt->Gig->gig_end_date_time}}</td>
                                    <td class="post-rate">KSH{{$tt->getPrice->price}}</td>
                                    @if($tt->status==4)
                                    <td class="blue-box">
                                        <p>Completed</p>
                                    </td>
                                    @else
                                    <td class="blue-box">
                                        <p>Ongoing</p>
                                    </td>
                                    @endif
                                    <td>
                                        @if($tt->is_checkin==0)
                                        <a href="{{asset('influencers/accepted/twitter-trends/check-in/'.$tt->gig_id)}}" class="table-red-btn">Check In</a>
                                        @elseif($tt->status==3)
                                        <a href="{{asset('influencers/accepted/twitter-trends/check-out/'.$tt->gig_id)}}" class="table-red-btn">Check Out</a>
                                        @endif
                                        <!-- <button class="table-red-btn">Raise Dispute</button> -->
                                        <!-- <button class="msg-btn"><img src="{{asset('influencers/assets/images/icons/msg-circle.svg')}}"></button> -->
                                    </td>
                                </tr>
                                @endforeach  
                            @else
                                <tr>
                                    <td class="text-center p-5 border border-0" colspan="7">
                                        <img src="{{asset('influencers/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                        <h3 style="font-size: 20px;">No Data Found!</h3>
                                    </td>
                                </tr>   
                            @endif                             
                            </tbody>

                        </table>
                    </div>

                    <div class="pagination">
                        <div class="col-sm-5">
                            <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                            <div class="">
                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                    <ul class="pagination">
                                       {{$tts->links()}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>