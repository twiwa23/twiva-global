@extends('influencers.layout.app')
@section('title')
My Invites Posts
@endsection
@section('content')
<div class="right_col dashboard-page" role="main" id="influencer-page">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="breadcrumb-heading">Accepted / Twitter Trends</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
                
                <div class="catalog-page">
                    <div class="select-top select-search">
                        <div class="search-wrap">
                           
                            <div class="search-panel">
                                <input type="text" placeholder="Search by Title Name" id="search">
                            </div>
    
                            <div class="button-catalog">
                                    <div class="date-picker-btn">
                                        <input type="text" name="daterange" value="05/01/2021 - 05/21/2021" class="m-0" style="max-width: 200px;"/>
                                    </div>

                                <!-- <div class="drop-btn">
                                    <div class="select-btn">
                                        <div class="btn__trigger">Status</div>
                                        <div class="btn-options">
                                            <span class="btn-option" data-value="Status">Status</span>
                                            <span class="btn-option" data-value="5-Star">5-Star</span>
                                            <span class="btn-option" data-value="4-Star">4-Star</span>
                                            <span class="btn-option" data-value="3-Star">3-Star</span>
                                            <span class="btn-option" data-value="2-Star">2-Star</span>
                                            <span class="btn-option" data-value="1-Star">1-Star</span>
                                        </div>
                                        <img src="/assets/images/icons/down-arrow.svg" alt="">
                                    </div>
                                </div> -->
                            </div>
                            
                        </div>
                    </div>

                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                    @endif
                    @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                    @endif
                <div id="data">
                
                </div>
                    
                </div>
            </div>
            <input type="hidden" id="from_date">
            <input type="hidden" id="to_date">
@endsection
@section('script')
<script>
$(document).ready(function(){
    $.ajax({
    type : 'get',
    url:"/influencers/accepted/twitter-trends/data",
    success:function(tts)
    {
        $('#data').html(tts);
    }
    });
$(document).on('click', '.page-link', function(event){
  event.preventDefault(); 
  var page = $(this).attr('href').split('page=')[1];
  var name=$(".name").val();
  var from_date = $("#from_date").val();
  var to_date = $("#to_date").val();
  fetch_data(page,name,from_date,to_date);
 });
 function fetch_data(page,name,from_date,to_date)
 {
    $.ajax({
    type : 'get',
    url:"/influencers/accepted/twitter-trends/data?page="+page,
    data:{'name':name , 'from_date':from_date , 'to_date':to_date},
    success:function(tts)
    {
        $('#data').html(tts);
    }
    });
 }

 $('#search').on('keyup',function(){
var name=$(this).val();
var from_date = $("#from_date").val();
var to_date = $("#to_date").val();
$.ajax({
type : 'get',
url:"/influencers/accepted/twitter-trends/data",
data:{'name':name , 'from_date':from_date , 'to_date':to_date }, 
success:function(posts){
    $('#data').html(posts);
}
});
});

$(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'right',
            }, function(start, end, label) {
                var from_date = start.format('YYYY-MM-DD');
                var to_date = end.format('YYYY-MM-DD');
                var name=$("#search").val();
                $("#from_date").val(from_date);
                $("#to_date").val(to_date);
                $.ajax({
                    type : 'get',
                    url:"/influencers/accepted/twitter-trends/data",
                    data:{'name':name , 'from_date':from_date , 'to_date':to_date},
                    success:function(posts){
                        $('#data').html(posts);
                    }
                    }); 
            });
        });
});
</script>
@endsection