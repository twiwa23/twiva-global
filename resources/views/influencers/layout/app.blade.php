<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('/influencers/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link href="{{asset('/influencers/assets/css/daterangepicker.css')}}" rel="stylesheet">  
    <link href="{{asset('/influencers/assets/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('/influencers/assets/css/style.css')}}" rel="stylesheet">  
</head>

<body>

    <!-- Top Navbar Start -->
    <header class="header-section">

        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button">
            <img src="{{asset('/influencers/assets/images/toggle-icon.svg')}}" alt="">
        </button>

        <!-- Navbar Logo -->
        <div class="logo">
            <img src="{{asset('/influencers/assets/images/icons/twiva-logo.svg')}}">
        </div>

        <!-- Navbar Menu -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="">
                    <div class="nav toggle">
                        <a id="menu_toggle"></a>
                    </div>

                    <ul class="nav navbar-nav">
                        <li class="d-none">
                            <a href="#"><img src="{{asset('/influencers/assets/images/icons/message-square.svg')}}" alt=""></a>
                        </li>
                        <li class="">
                            <a href="#">
                                <img src="{{asset('/influencers/assets/images/icons/bell-icon.svg')}}">
                            </a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile-img">
                                    @if(empty(auth()->guard('business')->user()->InfluencerDetailNew->profile_image))
                                        <img src="{{ asset('business/extra/images/profile.jpeg') }}" alt="">
                                    @else
                                        <img src="{{ config('app.dev_storage_url'). auth()->guard('business')->user()->InfluencerDetailNew->profile_image }}" alt="">
                                    @endif
                                </div>
                                {{ auth()->guard('business')->user()->name ?? 'User'}}
                                <img src="/assets/images/icons/chevron-down-white.svg" alt="" class="down-arrow">
                            </a>

                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li class="header-company-name-wrapper">
                                    <p class="company-name">{{ auth()->guard('business')->user()->name ?? 'Business'}}</p>
                                    <p class="email">{{ auth()->guard('business')->user()->email ?? 'Business Email' }}</p>
                                </li>
                                <li>
                                    <a href="{{url('influencers/logout')}}" id="logout-btn">Log Out</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </nav>
            </div>
        </div>
    </header>
    <!-- Top Navbar End -->

    <div class="backdrop"></div>

    <div class="" id="business-dashboard">
        <div class="dashboard_container">

            <!-- Left Column Start -->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>
    
                    <!-- Sidebar menu -->
                    @include('influencers/layout/side-menu')
                    <!-- /Sidebar menu -->

                </div>
            </div>
            <!-- Left Column End -->

            <!-- page content -->
            @yield('content')
            <!-- /page content -->
        </div>
    </div> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('/influencers/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('/influencers/assets/js/custom.js')}}"></script>
    <script src="{{asset('/influencers/assets/js/moment.min.js')}}"></script>
    <script src="{{asset('/influencers/assets/js/daterangepicker.min.js')}}"></script>
    @yield('script')
    <script>
        $(document).on('click', '#nav-click-new', function(e) {
            var user_id = $("#user_id").val();
            var nav_url = $(this).data('url');
            e.preventDefault();
            if(user_id) {
                $.ajax({
                    url: "{{ route('inavChangeToNew') }}",
                    type: "post",
                    data: { 'user_id': user_id, 'nav_url' : nav_url},
                    success: function(data){
                       window.location.href = data.url;
                    },
                    error : function(data) {
                        console.log(data)
                    }
                });
            }
        });
    </script>
</body>

</html>