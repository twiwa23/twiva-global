                    <?php $id = Auth::guard('business')->user()->id; ?>
                    <input type="hidden" id="user_id" value="{{ auth()->guard('business')->user()->id }}">
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h4>Menu</h4>
                            <ul class="nav side-menu">
                                <li  id="nav-click-new"  data-url="influencer-dashboard.php">
                                    <a href="#">
                                        <img src="{{asset('/influencers/assets/images/icons/dashboard-2.svg')}}">Dashboard
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                </li>

                                <li class="sidebar-dropdown">
                                    <a href="javascript:void(0);" class="@if(Request::segment(2) == 'my-invites') current-page @endif"> <img src="{{asset('/influencers/assets/images/icons/invitation-2.svg')}}">My Invites<span class="fa fa-chevron-down rotate-active"></span></a>
                                    
                                    <div class="sidebar-submenu @if(Request::segment(2) == 'my-invites') active @endif">
                                        <ul class="dropdown-lists">
                                            <li class="drop-down-item @if(Request::segment(3) == 'posts') active @endif">
                                                <a href="{{asset('/influencers/my-invites/posts/posts')}}" class="">Posts</a>
                                            </li>
                                            <li class="drop-down-item @if(Request::segment(3) == 'twitter-trends') active @endif">
                                                <a href="{{asset('/influencers/my-invites/twitter-trends/twitter-trends')}}">Twitter Trends</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <!-- <li class="sidebar-dropdown">
                                    <a href="javascript:void(0);" class="@if(Request::segment(2) == 'accepted') current-page @endif">
                                        <img src="{{asset('/influencers/assets/images/icons/file-plus.svg')}}">Accepted<span class="fa fa-chevron-down"></span>
                                    </a>
    
                                    <div class="sidebar-submenu @if(Request::segment(2) == 'accepted') active @endif">
                                        <ul class="dropdown-lists">
                                            <li class="drop-down-item @if(Request::segment(3) == 'posts') active @endif">
                                                <a href="{{asset('/influencers/accepted/posts/posts')}}" class="">Posts</a>
                                            </li>
                                            <li class="drop-down-item @if(Request::segment(3) == 'twitter-trends') active @endif">
                                                <a href="{{asset('/influencers/accepted/twitter-trends/twitter-trends')}}">Twitter Trends</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li> -->

                                <li id="nav-click-new"  data-url="influencer-eshop.php">
                                    <a href="#">
                                        <img src="{{asset('/influencers/assets/images/icons/my-eshop.svg')}}">My eShop
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                </li>

                                <!-- <li class="sidebar-dropdown">
                                    <a href="javascript:void(0);"> <img src="{{asset('/influencers/assets/images/icons/social-selling.svg')}}" /> Social Selling<span class="fa fa-chevron-down"></span></a>
                                    <div class="sidebar-submenu">
                                        <ul class="dropdown-lists">                                      
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-post-story.php">
                                                <a href="#">Add Post</a>
                                            </li >
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-social.php">
                                                <a href="#">Social Selling</a>
                                            </li >
                                        </ul>
                                    </div>
                                </li> -->

                                <li class="sidebar-dropdown">
                                    <a href="javascript:void(0);"><img src="{{asset('/influencers/assets/images/icons/settings.svg')}}">Account Settings<span class="fa fa-chevron-down"></span></a>
                                    
                                    <div class="sidebar-submenu">
                                        <ul class="dropdown-lists">
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-edit-profile.php">
                                                <a href="#">Profile</a>
                                            </li >
                                            <!-- <li class="drop-down-item" id="nav-click-new"  data-url="social-media-connections.php">
                                                <a href="#">Social Media Connection</a>
                                            </li > -->
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-bank-setup.php">
                                                <a href="#">Bank Setup</a>
                                            </li>
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-my-earning.php">
                                                <a href="#">My Earnings</a>
                                            </li>
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-payment-history.php">
                                                <a href="#">Payment History</a>
                                            </li>
                                            <li class="drop-down-item" id="nav-click-new"  data-url="influencer-mobile-verification.php">
                                                <a href="#">Phone Verification</a>
                                            </li>
                                            <li class="drop-down-item" id="nav-click-new" data-url="influencer-change-password.php">
                                                <a href="#">Change Password</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li id="nav-click-new"  data-url="influencer-refer-earn.php" >
                                    <a href="#">
                                        <img src="{{asset('/influencers/assets/images/icons/refer.svg')}}">Refer & Earn
                                        <span class="fa fa-chevron-down"></span>
                                    </a> 
                                </li>

                                <li >
                                    <a href="{{url('influencers/logout')}}">
                                        <img src="{{asset('/influencers/assets/images/icons/logout-sidebar.svg')}}">Logout
                                        <span class="fa fa-chevron-down"></span>
                                    </a> 
                                </li>
                            </ul>

                        </div>
                    </div>