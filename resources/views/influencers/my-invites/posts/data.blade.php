                        <div class="tab-pane active" id="invitations">
                            <div class="table-responsive">
                                <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>IMAGE</th>
                                            <th>TITLE</th>
                                            <!-- <th>STAR RATING</th> -->
                                            <th>EXPIRATION DATE</th>
                                            <th>COMPENSATION</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
        
                                    <tbody>
                                    @if(count($posts)>0)
                                        @foreach($posts as $post)
                                        <tr>
                                            <td>
                                                <div class="user-image">
                                                    @if(!empty($post->Post->Images[0]->media))
                                                    <img src="{{$post->Post->Images[0]->media}}" alt="Post Image">
                                                    @else
                                                    <img src="{{url('public/business/images/default.svg')}}" alt="">
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="user-name font-weight"><a href="{{asset('influencers/my-invites/posts/details/'.$post->post_id)}}">{{$post->Post->post_name}}</a></td>
                                            <!-- <td>
                                                <div class="user-ratings">
                                                    <img src="{{asset('influencers/assets/images/icons/star1.svg')}}" alt="">
                                                    <img src="{{asset('influencers/assets/images/icons/star1.svg')}}" alt="">
                                                    <img src="{{asset('influencers/assets/images/icons/star1.svg')}}" alt="">
                                                    <img src="{{asset('influencers/assets/images/icons/star1.svg')}}" alt="">
                                                    <img src="{{asset('influencers/assets/images/icons/star1.svg')}}" alt="">
                                                </div>
                                            </td> -->
                                            <td class="post-rate">{{$post->Post->expiration_date ?? "NA"}}</td>
                                            <td class="post-rate">KSH{{$post->price->price ?? "0"}}</td>
                                            <td class="completed">
                                                <p>Invited</p>
                                            </td>
                                            <td>
                                                <a href="{{asset('influencers/my-invites/posts/accept/'.$post->post_id)}}" class="table-red-btn">Accept</a>
                                                <button class="table-red-btn decline" data-toggle="modal" data-target="#decline-modal" data-id="{{$post->post_id}}" >Decline</button>
                                            </td>
                                        </tr>
                                        @endforeach  
                                    @else
                                    <tr>
                                        <td class="text-center p-5 border border-0" colspan="7">
                                            <img src="{{asset('influencers/assets/images/icons/empty.svg')}}" style="width: 87px; height: 113px">
                                            <h3 style="font-size: 20px;">There are no jobs to show currently</h3>
                                        </td>
                                    </tr>   
                                    @endif  
                                    </tbody>
        
                                </table>
                            </div>
        
                            <div class="pagination">
                                <div class="col-sm-5">
                                    <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                                    <div class="">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                            <ul class="pagination">
                                                {{$posts->links()}}        
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="bids">
                            <div class="table-responsive">
                                <table id="admin-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>IMAGE</th>
                                            <th>TITLE</th>
                                            <th>STAR RATING</th>
                                            <th>EXPIRATION DATE</th>
                                            <th>COMPENSATION</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
        
                                    <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>
                                                <div class="user-image">
                                                    @if(!empty($post->Post->Images[0]->media))
                                                    <img src="{{$post->Post->Images[0]->media}}" alt="Post Image">
                                                    @else
                                                    <img src="{{asset('influencers/assets/images/product-img/default.svg')}}" alt="">
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="user-name font-weight">{{$post->Post->post_name}}</td>
                                            <td>
                                                <div class="user-ratings">
                                                    <img src="./assets/images/icons/star1.svg" alt="">
                                                    <img src="./assets/images/icons/star1.svg" alt="">
                                                    <img src="./assets/images/icons/star1.svg" alt="">
                                                    <img src="./assets/images/icons/star1.svg" alt="">
                                                    <img src="./assets/images/icons/star1.svg" alt="">
                                                </div>
                                            </td>
                                            <td class="post-rate">{{$post->Post->expiration_date ?? "NA"}}</td>
                                            <td class="post-rate">KSH{{$post->price->price ?? "0"}}</td>
                                            <td class="blue-box">
                                                <p>Quote sent</p>
                                            </td>
                                            <td>
                                                <button class="table-red-btn" data-toggle="modal" data-target="#payment-modal">Bid</button>
                                                <button class="table-red-btn">Not Interested</button>
                                            </td>
                                        </tr>
                                    @endforeach       
                                    </tbody>
        
                                </table>
                            </div>
        
                            <div class="pagination">
                                <div class="col-sm-5">
                                    <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                                    <div class="">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                            <ul class="pagination">
                                            {{$posts->links()}} 
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

    <!-- The Modal -->
    <div class="modal fade" id="payment-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Canon EOS 80D 24.2MP Digital SLR Camera</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>
        
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <h5>Please enter the desired amount</h5>
                            <p>Ksh</p>
                            <input type="number">
                        </li>
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                    <button class="red-btn">Send Quote</button>
                </div>
        
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="decline-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Please state your reason for declining the Twitter Trend</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>
        
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <button class="red-btn"  data-toggle="modal" data-target="#lowPayModal" data-dismiss="modal">Low Pay</button>
                        </li>
                        <li class="list-item">
                        <form action="{{asset('influencers/my-invites/posts/reject')}}" method="POST" style="margin-bottom:0;">
                        <input type="hidden" name="post_id" class="post_id">
                        <input type="hidden" name="reason" value="Conflict of Interests">  
                            <button type="submit" class="red-btn">Conflict of Interests</button>
                        </form>
                        </li>
                        <li class="list-item">
                        <form action="{{asset('influencers/my-invites/posts/reject')}}" method="POST" style="margin-bottom:0;">
                        <input type="hidden" name="post_id" class="post_id">
                        <input type="hidden" name="reason" value="Not Interested"> 
                            <button class="red-btn" type="submit">Not Interested</button>
                        </form>
                        </li>
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="white-btn" data-dismiss="modal">Cancel</button>
                </div>
        
            </div>
        </div>
    </div> 

    <!-- The Modal -->
    <div class="modal fade" id="lowPayModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Apply</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{asset('influencers/my-invites/posts/reject')}}" method="POST" style="margin-bottom:0;">
                    <input type="hidden" name="post_id" class="post_id">
                    <input type="hidden" name="reason" value="Low Pay">     
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <div class="form-group">
                                <label for="enterPrice">Enter Price</label>
                                <input type="text" class="form-control" id="enterPrice" name="reject_amount" required>
                            </div>
                        </li>
                        <!-- <li class="list-item">
                            <div class="form-group">
                                <label for="compliment">Enter complementary items</label>
                                <textarea class="form-control" rows="5" id="compliment"></textarea>
                            </div>
                        </li> -->
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer justify-content-center">
                    <button type="submit" class="red-btn w-100">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
        $(".decline").on('click' , function(){
        console.log($(this).data("id"));
        $(".post_id").val($(this).data("id"));
        });
    });
    </script>