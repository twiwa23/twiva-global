@extends('influencers.layout.app')
@section('title')
My Invites Posts
@endsection
@section('content')
<div class="right_col feedpost-details-page" role="main" id="twitter-trend">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="active">Twitter Trends<img src="{{asset('influencers/assets/images/icons/chevron-right-gray.svg')}}" alt=""></a></li>
                        <li><a href="#">{{$tt->gig_name}}</a></li>
                    </ul>
                </div>
                <!--********** Breadcrumb End ***********-->


                <div class="bid-invite-section">
                    <h1>Do you wish to accept or decline the Twitter trend?</h1>
                    <div class="btn-section">
                        <a href="{{asset('influencers/my-invites/twitter-trends/accept/'.$tt->id)}}" class="red-btn">Accept</a>
                        <button class="white-btn decline" data-toggle="modal" data-target="#decline-modal" data-id="{{$tt->id}}">Decline</button>
                    </div>
                </div>

                <div class="post-container custom-style">
                    <div class="post-heading">
                        <h3>Twitter Trend Details</h3>
                    </div>

                    <div class="post-input-container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" value="{{$tt->gig_name}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input type="text" class="form-control" value="{{$tt->description}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Person Name</label>
                                        <input type="text" class="form-control" value="{{$tt->owner_name}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Person Number</label>
                                        <input type="number" class="form-control" value="{{$tt->phone_number}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>What To Do <span>(These are the things Influencers have to do in their content)</span></label>
                                        <input type="text" class="form-control custom-h" value="{{$tt->what_to_do}}" readonly>
                                    </div>

                                    <div class="row">
                                        <div class="com-sm-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Gig Start Date</label>
                                                <input type="text" name="date" class="form-control" value="{{date('d-m-Y' , strtotime($tt->gig_start_date_time) )}}" readonly>
                                            </div>
                                        </div>

                                        <div class="com-sm-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Gig Start Time</label>
                                                <div class="form-control">
                                                    <div class="quantity">
                                                        <input type="number" min="1" max="12" step="1"  class="time-input" value="{{date('h' , strtotime($tt->gig_start_date_time) )}}" readonly>
                                                        <div class="quantity-nav">
                                                            <!-- <button class="quantity-button quantity-up">&#xf106;</button>
                                                            <button class="quantity-button quantity-down">&#xf107;</button> -->
                                                            <div class="time-text">Hrs</div>
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="quantity">
                                                        <input type="number" min="1" max="60" step="1" value="{{date('i' , strtotime($tt->gig_start_date_time) )}}" class="time-input" readonly>
                                                        <div class="quantity-nav">
                                                            <!-- <button class="quantity-button quantity-up">&#xf106;</button>
                                                            <button class="quantity-button quantity-down">&#xf107;</button> -->
                                                            <div class="time-text">Min</div>
                                                        </div>
                                                    </div>
                                                    
                                                    <ul class="meridian">
                                                        <li class="am" style="text-transform: uppercase;">{{date('a' , strtotime($tt->gig_start_date_time) )}}</li>
                                                        <!-- <li class="pm">PM</li> -->
                                                    </ul>
                                                </div>

                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="com-sm-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Gig End Date</label>
                                                <input type="text" name="date" class="form-control" value="{{date('d-m-Y' , strtotime($tt->gig_end_date_time) )}}" readonly >
                                            </div>
                                        </div>

                                        <div class="com-sm-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Gig End Time</label>
                                                <div class="form-control">
                                                    <div class="quantity">
                                                        <input type="number" min="1" max="12" step="1" value="{{date('h' , strtotime($tt->gig_end_date_time) )}}" class="time-input" readonly>
                                                        <div class="quantity-nav">
                                                            <!-- <button class="quantity-button quantity-up">&#xf106;</button>
                                                            <button class="quantity-button quantity-down">&#xf107;</button> -->
                                                            <div class="time-text">Hrs</div>
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="quantity">
                                                        <input type="number" min="1" max="60" step="1" value="{{date('i' , strtotime($tt->gig_end_date_time) )}}" class="time-input" readonly>
                                                        <div class="quantity-nav">
                                                            <!-- <button class="quantity-button quantity-up">&#xf106;</button>
                                                            <button class="quantity-button quantity-down">&#xf107;</button> -->
                                                            <div class="time-text">Min</div>
                                                        </div>
                                                    </div>
                                                    
                                                    <ul class="meridian">
                                                        <li class="am" style="text-transform: uppercase;">{{date('a' , strtotime($tt->gig_end_date_time) )}}</li>
                                                        <!-- <li class="pm">PM</li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="input-section">
                                    <div class="form-group">
                                        <label>Compensation</label>
                                        <input type="text" class="form-control" value="KSH {{$tt->price_per_gig}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea name="" class="form-control custom-h">{{$tt->location}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Venue</label>
                                        <textarea name="" class="form-control custom-h">{{$tt->venue}}</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Add Theme <span>(Include any photo/video to be used as an inspiration for the Influencer)</span></label>
                                        <div class="form-control custom-form" style="height: 99px;">
                                       
                                            @foreach($tt->Images as $image)
                                            <div class="image-container">
                                                @if(!empty($image->media))
                                                <img src="{{$image->media}}" alt="">
                                                @else                                               
                                                <img src="{{url('public/business/images/default.svg')}}" alt="">
                                                @endif
                                            </div>
                                            @endforeach
                                       
                                            <!-- <div class="image-container">
                                                <img src="{{asset('influencers/assets/images/profile-images/rectangle-710.png')}}" alt="">
                                            </div> -->
                                            <!-- <label for="files" class="upload-field file-upload">
                                                <input type="file" id="files" name="files[]" multiple="">
                                            </label> -->
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Hashtag <span>(Include hashtags that will be attached to the created content e.g. #twiva #yourproductlaunch)</span></label>
                                        <input type="text" class="form-control" value="#{{$tt->hashtag}}" readonly>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                   
                    
                </div>
            </div>

    <!-- The Modal -->
    <div class="modal fade" id="decline-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Please state your reason for declining the Twitter Trend</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>
        
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <button class="red-btn" data-toggle="modal" data-target="#lowPayModal" data-dismiss="modal">Low Pay</button>
                        </li>
                        <li class="list-item">
                        <form action="{{asset('influencers/my-invites/twitter-trends/reject')}}" method="POST" style="margin-bottom:0;">
                            <input type="hidden" name="gig_id" class="gig_id">
                            <input type="hidden" name="reason" value="Conflict Of Interests"> 
                            <button type="submit" class="red-btn">Conflict of Interests</button>
                        </form>
                        </li>
                        <li class="list-item">
                        <form action="{{asset('influencers/my-invites/twitter-trends/reject')}}" method="POST" style="margin-bottom:0;">
                        <input type="hidden" name="gig_id" class="gig_id">
                        <input type="hidden" name="reason" value="Not Interested"> 
                            <button type="submit" class="red-btn">Not Interested</button>
                        </form>
                        </li>
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="white-btn" data-dismiss="modal">Cancel</button>
                </div>
        
            </div>
        </div>
    </div>

        <!-- The Modal -->
        <div class="modal fade" id="lowPayModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Apply</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{asset('influencers/my-invites/twitter-trends/reject')}}" method="POST" style="margin-bottom:0;">
                    <input type="hidden" name="gig_id" class="gig_id">
                    <input type="hidden" name="reason" value="Low Pay">     
                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="inlfuencer-lists">
                        <li class="list-item">
                            <div class="form-group">
                                <label for="enterPrice">Enter Price</label>
                                <input type="text" class="form-control" id="enterPrice" name="reject_amount" required>
                            </div>
                        </li>
                        <!-- <li class="list-item">
                            <div class="form-group">
                                <label for="compliment">Enter complementary items</label>
                                <textarea class="form-control" rows="5" id="compliment"></textarea>
                            </div>
                        </li> -->
                    </ul>
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer justify-content-center">
                    <button type="submit" class="red-btn w-100">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>

$(document).ready(function(){
    $(".decline").on('click' , function(){
    console.log($(this).data("id"));
    $(".gig_id").val($(this).data("id"));
    });
});
</script>
@endsection