@extends('influencers.layout.app')
@section('title')
My Invites Twitter Trends
@endsection
@section('content')
            <!-- page content -->
            <div class="right_col dashboard-page" role="main" id="influencer-page">
                <!--********** Breadcrumb Start ***********-->
                <div class="page-title breadcrumb-wrapper">
                    <ul class="breadcrumb">
                        <li><a href="#" class="breadcrumb-heading">My Invites / Twitter Trends</a></li>
                    </ul>
                </div>
                <!--**********  Breadcrumb End ***********-->
                
                <div class="catalog-page">
                    <div class="select-top select-search">
                        <div class="search-wrap">
                            <div class="top-filter-section">
                                <div class="search-panel">
                                    <input type="text" placeholder="Search by Title Name" id="search">
                                </div>
        
                                <div class="button-catalog">
                                    <div class="date-picker-btn">
                                        <input type="text" name="daterange" value="05/01/2021 - 05/01/2021" class="m-0" style="max-width: 200px;"/>
                                    </div>
    
                                    <!-- <div class="drop-btn" >
                                        <div class="select-btn" style="width: 150px;">
                                            <div class="btn__trigger">Compensation</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="Compensation">Compensation</span>
                                                <span class="btn-option" data-value="1000-5000">1000-5000</span>
                                                <span class="btn-option" data-value="6000-9000">6000-9000</span>
                                                <span class="btn-option" data-value="Above 10000">Above 10000</span>
                                            </div>
                                            <img src="/assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div> -->

                                    <!-- <div class="drop-btn">
                                        <div class="select-btn">
                                            <div class="btn__trigger">All</div>
                                            <div class="btn-options">
                                                <span class="btn-option" data-value="All">All</span>
                                                <span class="btn-option" data-value="5-Star">5-Star</span>
                                                <span class="btn-option" data-value="4-Star">4-Star</span>
                                                <span class="btn-option" data-value="3-Star">3-Star</span>
                                                <span class="btn-option" data-value="2-Star">2-Star</span>
                                                <span class="btn-option" data-value="1-Star">1-Star</span>
                                            </div>
                                            <img src="./assets/images/icons/down-arrow.svg" alt="">
                                        </div>
                                    </div> -->
                                </div>
                            </div>

                            <div class="influencer-list">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="pill" href="#invitations">Invitations</a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#bids">Bid for Twitter Trends</a>
                                    </li> -->
                                </ul>
                            </div>

                        </div>
                    </div>

                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                    <!-- Tab panes -->
                    <div class="tab-content">
                        
                    </div>
                    
                </div>
            </div>
            <!-- /page content -->
    </div>
</div>
<input type="hidden" id="from_date">
<input type="hidden" id="to_date">
@endsection
@section('script')
<script>
    $(document).ready(function(){
            $.ajax({
            type : 'get',
            url:"/influencers/my-invites/twitter-trends/data",
            success:function(tts){
                $('.tab-content').html(tts);
            }
            });
        $(document).on('click', '.page-link', function(event){
        event.preventDefault(); 
        var page = $(this).attr('href').split('page=')[1];
        fetch_data(page);
        });
        function fetch_data(page)
        {
        var name=$(".name").val();
        $.ajax({
        type : 'get',
        url:"/influencers/my-invites/twitter-trends/data?page="+page,
        data:{'name':name},
        success:function(tts)
        {
            $('.tab-content').html(tts);
        }
        });
        }

$('#search').on('keyup',function(){
var name=$(this).val();
$.ajax({
type : 'get',
url:"/influencers/my-invites/twitter-trends/data",
data:{'name':name},
success:function(tts){
    $('.tab-content').html(tts);
}
});
});  
$(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'right',
            }, function(start, end, label) {
                var from_date = start.format('YYYY-MM-DD');
                var to_date = end.format('YYYY-MM-DD');
                var name=$(".name").val();
                $("#from_date").val(from_date);
                $("#to_date").val(to_date);
                $.ajax({
                    type : 'get',
                    url:"/influencers/my-invites/twitter-trends/data",
                    data:{'name':name , 'from_date':from_date , 'to_date':to_date},
                    success:function(posts){
                        $('.tab-content').html(tts);
                    }
                    }); 
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });     
    });
</script>
@endsection