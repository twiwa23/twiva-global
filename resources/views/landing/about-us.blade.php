@extends('layouts.app')
@section('title')
    Twiva About
@endsection
@section('content')
<!--Banner Section-->
<section class="faq-banner about-us-banner">
    <div class="banner-content b-p">
        <div class="faq-text">
            <h1>About Us</h1>
        </div>
        <div class="faq-img">
            <img src="./assets/images/about-img.svg" alt="">
        </div>
    </div>
</section>
</div>


<!--Main Section-->
<main class="about-page">

<div class="main-content-wrapper">
    <ul class="description-list">
        <li class="list-item">
            <!-- <img src="./assets/images/icons/Polygon.svg" alt=""> -->
            <p>Twiva is an influencer marketing and social commerce Startup that gives MSMEs affordable, efficient, and digital access to markets. This is done by leveraging social media influencers to market and sell MSMEs’ products and services on social media platforms. Twiva was created to harness the vast penetration of smartphones, internet connectivity, accessible mobile banking, and the power of influencer marketing to create dignifying digital and digitally enabled jobs.</p>
        </li>
    </ul>

    <div class="purpose">
        <h1>Our Purpose</h1>
        <p>We give MSMEs affordable, effective and digital access to markets to increase sales and scale</p>
    </div>

    <div class="values">
        <h1>Our Values</h1>
        <ul class="value-lists">
           <li class="list-item">
               <img src="./assets/images/icons/empower.svg" alt="">
               <p>Empower People</p>
           </li>

           <li class="list-item">
               <img src="./assets/images/icons/learn.svg" alt="">
               <p>Learn Continuously</p>
           </li>

           <li class="list-item">
               <img src="./assets/images/icons/youth.svg" alt="">
               <p>Youth-centric</p>
           </li>
        </ul>
    </div>
</div>

</main>
@endsection
