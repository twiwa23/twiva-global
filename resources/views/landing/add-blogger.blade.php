@extends('creator-layout.app')
@section('title')
    Add Blog
@endsection
@section('content')
<div class="main-dashboard-container" id="service-page">

    <div class="page-content p-t">
        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper">
            <ul class="breadcrumb">
                <li><a href="{{ url('user/portfolio/'.auth()->user()->id) }}" class="breadcrumb-heading active">Dashboard<img src="{{ asset('creative/assets/images/icons/chevron-right.svg') }}" alt=""></a></li>
                <li><a href="#" class="breadcrumb-heading">Add Blog</a></li>
            </ul>
        </div>
        <!--**********  Breadcrumb End ***********-->

        <div class="mobile-breadcrumb-wrapper">
            <ul class="mobile-breadcrumb">
                <li><a href="#" class="breadcrumb-heading"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Add Blog</a></li>
            </ul>
        </div>
        <form method="post"  enctype="multipart/form-data" onsubmit="return validateForm()">
            {{ csrf_field() }}
        <div class="portfolio-container">
            <div class="portfolio-upload-section">
                <h1>Add Blog</h1>
                @include('layouts.notifications')

                    <div class="input-section" style="width: 80%;">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea id="description" name="description" placeholder="Description" maxlength="2000" class="form-control custom-h"></textarea>
                            <span id="description-error" class="error" for="description">{{$errors->first('description')}}</span>
                        </div>
                    </div>
                <button type="submit" class="red-btn">Add</button>
            </div>
        </div>
    </form>

    </div>

</div>
@endsection
@section('scripts')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>


  $('#form_validate').validate({
      rules: {


          description: {
              required: true,
               minlength:10,
               maxlength:2000
          },

      },

        messages:{

          description:{
            required:'Please enter description.',
             minlength : 'Description must be at least 10 characters.',
             maxlength : 'Description is maximum 2000 characters.'
          },



        },

        /*errorHandler:function(e){
          console.log(e)
        }*/


  });
</script>
@endsection
