@extends('creator-layout.app')
@section('title')
    Add Portfolio
@endsection
@section('content')
<div class="main-dashboard-container" id="service-page">

    <div class="page-content p-t">
        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper">
            <ul class="breadcrumb">
                <li><a href="{{ url('user/portfolio/'.auth()->user()->id) }}" class="breadcrumb-heading active">Dashboard<img src="{{ asset('creative/assets/images/icons/chevron-right.svg') }}" alt=""></a></li>
                <li><a href="#" class="breadcrumb-heading">Add Portfolio</a></li>
            </ul>
        </div>
        <!--**********  Breadcrumb End ***********-->

        <div class="mobile-breadcrumb-wrapper">
            <ul class="mobile-breadcrumb">
                <li><a href="#" class="breadcrumb-heading"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Add Portfolio</a></li>
            </ul>
        </div>
        <form method = "post"  enctype="multipart/form-data" onsubmit="return validateForm()">
            {{csrf_field()}}
        <div class="portfolio-container">
            <div class="portfolio-upload-section">
                <h1>Add Portfolio</h1>
                @include('layouts.notifications')

                <div class="profile-img-upload">
                    <label for="service-img" class="upload-img">
                        <input type="file" class='uploader' id="service-img" name="image_video" class="get_input_files" />
                        <img src="{{ asset('creative/assets/images/icons/default-large.svg') }}" id='image' class="profileImg">
                        <span class="text-danger" id="invalid_file">{{$errors->first('image_video')}}</span>
                    </label>
                </div>
                <button type="submit" class="red-btn">Add</button>
            </div>
        </div>
    </form>

    </div>

</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
function validateForm(){
    $("#invalid_file").hide();
    if($("input[name='image_video']").val() == ""){
    $("#invalid_file").show().text("Please upload image/video");
    return false;

    }
}
</script>
@endsection
