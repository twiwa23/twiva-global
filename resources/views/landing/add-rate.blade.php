@extends('creator-layout.app')
@section('title')
    Add Rate
@endsection
@section('content')
<div class="main-dashboard-container" id="service-page">

    <div class="page-content p-t">
        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper">
            <ul class="breadcrumb">
                <li><a href="{{ url('user/portfolio/'.auth()->user()->id) }}" class="breadcrumb-heading active">Dashboard<img src="{{ asset('creative/assets/images/icons/chevron-right.svg') }}" alt=""></a></li>
                <li><a href="#" class="breadcrumb-heading">Add Rate</a></li>
            </ul>
        </div>
        <!--**********  Breadcrumb End ***********-->

        <div class="mobile-breadcrumb-wrapper">
            <ul class="mobile-breadcrumb">
                <li><a href="#" class="breadcrumb-heading"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Add Rate</a></li>
            </ul>
        </div>

        <div class="service-container add-rate-page">
            <div class="add-service">
                <div class="service-heading">
                    <h1>Add Rate</h1>
                </div>
                @include('layouts.notifications')
                <form method="POST" id="form_validate" >
                    {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group" style="margin-bottom: 24px;">
                                <label>Amount</label>
                                <div class="input-group">
                                    <input type="text" name="amount" id = "amount" value = "{{old('amount')}}" onkeypress="return isNumberKey(event)" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Ksh</span>
                                    </div>
                                </div>
                                <span id="name-amount" class="error" for="amount">{{$errors->first('amount')}}</span>
                            </div>

                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" id = "title" value = "{{old('title')}}" placeholder="Title" class="form-control">
                                <span id="name-title" class="error" for="title">{{$errors->first('title')}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id = "description" name="description" placeholder="Description" maxlength="400" class="form-control custom-h"></textarea>
                                <span id="description-error" class="error" for="description">{{$errors->first('description')}}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="btn-wrapper">
                    <button type="reset" class="white-btn">Reset</button>
                    <button type="submit" class="red-btn">Add</button>
                </div>
                </form>
            </div>
        </div>

    </div>

</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$('#form_validate').validate({
    rules: {

        amount: {
            required: true,
            minlength:1,
            maxlength:5,
            number : true

        },

        title: {
            required: true,
            minlength:5,
            maxlength:30
        },

        description: {
            required: true,
            minlength:10,
            maxlength:400
        },

    },

    messages:{

        description:{
        required:'Please enter description.',
            minlength : 'Description must be at least 10 characters.',
            maxlength : 'Description is maximum 400 characters.'
        },

        title:{
        required:'Please enter title.',
        minlength : 'Title must be at least 5 characters.',
            maxlength : 'Title is maximum 30 characters.'
        },

        amount:{
        required:'Please enter amount.',
            minlength:'Amount must be at least 1 digits.',
            maxlength : 'Amount is maximum 5 digits.'
        },


    },

    /*errorHandler:function(e){
        console.log(e)
    }*/


});
</script>
@endsection
