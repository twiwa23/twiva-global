@extends('creator-layout.app')
@section('title')
    Add Services
@endsection
@section('styles')
<style>
span .error {
    color : red;
}
</style>
@endsection
@section('content')
<div class="main-dashboard-container" id="service-page">

    <div class="page-content p-t">
        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper">
            <ul class="breadcrumb">
                <li><a href="{{ url('user/portfolio/'.auth()->user()->id) }}" class="breadcrumb-heading active">Dashboard<img src="{{ asset('creative/assets/images/icons/chevron-right.svg') }}" alt=""></a></li>
                <li><a href="#" class="breadcrumb-heading">Add Service</a></li>
            </ul>
        </div>
        <!--**********  Breadcrumb End ***********-->

        <div class="mobile-breadcrumb-wrapper">
            <ul class="mobile-breadcrumb">
                <li><a href="#" class="breadcrumb-heading"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Add Service</a></li>
            </ul>
        </div>

        <div class="service-container">
            <div class="add-service">
                <div class="service-heading">
                    <h1>Add Service</h1>
                </div>
                @include('layouts.notifications')
                <form method="POST" id="form_validate" enctype="multipart/form-data" onsubmit="return validateForm()">
                    {{csrf_field()}}
                <div class="profile-img-upload">
                    <label for="service-img" class="upload-img">
                        <input type="file" class='uploader' id="service-img" name="profile" />
                        <img src="{{ asset('creative/assets/images/icons/default.png') }}" id='image' class="profileImg">
                        <div class="camera-img">
                            <img src="{{ asset('creative/assets/images/icons/camera-edit.svg') }}" alt="">
                        </div>
                    </label>
                </div>
                <span id="invalid_file" style="color:red"></span>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text"  name="title" id = "title" value = "{{old('title')}}" class="form-control">
                                <span id="name-title" class="error" for="title">{{$errors->first('title')}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id = "description" name="description" placeholder="Description" maxlength="400" class="form-control custom-h"></textarea>
                                <span id="description-error" class="error" for="description">{{$errors->first('description')}}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="btn-wrapper">
                    <a href="{{ url('user/portfolio/'.auth()->user()->id) }}" class="white-btn">Cancel</a>
                    <button type="submit" class="red-btn">Add</button>
                </div>
                </form>
            </div>
        </div>

    </div>

</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>


  $('#form_validate').validate({
      rules: {

          title: {
              required: true,
               minlength:5,
               maxlength:30
          },

          description: {
              required: true,
               minlength:10,
               maxlength:400
          },

      },

        messages:{

          description:{
            required:'Please enter description.',
             minlength : 'Description must be at least 10 characters.',
             maxlength : 'Description is maximum 400 characters.'
          },

          title:{
            required:'Please enter title.',
            minlength : 'Title must be at least 5 characters.',
             maxlength : 'Title is maximum 30 characters.'
          },

        },

        /*errorHandler:function(e){
          console.log(e)
        }*/


  });
    function validateForm(){
        $("#invalid_file").hide();
        if($("input[name='profile']").val() == ""){
            $("#invalid_file").show().text("Please upload image");
            return false;
        }
    }
</script>
@endsection
