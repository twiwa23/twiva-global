@extends('layouts.app')
@section('title')
    Banned Products
@endsection
@section('content')
<!--Banner Section-->
<section class="faq-banner about-us-banner">
    <div class="banner-content b-p">
        <div class="faq-text">
            <h1>Banned Products</h1>
        </div>
    </div>
</section>
</div>


<!--Main Section-->
<main class="privacy-page">

<div class="main-content-wrapper">

    <h3>List of Banned Products</h3>
    <p>1. Adult Products and Pornographic/ Obscene Materials in any form (Print, audio/video, MMS, images, photographs, etc.)</p>
    <p>2. Any article/material/service which cannot be exhibited, advertised, made available, offered for sale at e-commerce platforms/ technology due to restrictions/conditions for sale of those articles / material/ service unless all those conditions are met pursuant to the Applicable Laws</p>
    <p>3. Any article/material/service which are prohibited by any law at any given time. </p>
    <p>4. Any item/material which may assist in performance of any illegal or unlawful activity </p>
    <p>5. Counterfeit Goods and goods/materials infringing any intellectual property rights </p>
    <p>6. Currency, Negotiable Instruments, etc</p>
    <p>7. Endangered species of animals and plants, whether alive or dead </p>
    <p>8. Firearms, parts thereof and ammunitions, weapons, knives, sharp-edged and other deadly weapons, and parts of, and machinery for manufacturing, arms, but does not include articles designed solely for domestic or agricultural uses such as a lathi or an ordinary walking stick and weapons incapable of being used otherwise than as toys or of being converted into serviceable weapons; </p>
    <p>9. Hazardous materials including but not limited to acid, fireworks, explosives, flammable adhesives, poison, hazardous chemical, oil-based paint and thinners (flammable liquids), industrial solvents, insecticides & pesticides, machinery (containing fuel), Fuel for camp stoves/lanterns/heating elements, infectious substances etc. </p>
    <p>10. Narcotic Drugs and Psychotropic Substances </p>
    <p>11. Prescription Medicines and Drugs </p>
    <p>12. Racially/ethnically/religiously offensive materials </p>
    <p>13. Radioactive Materials </p>
    <p>14. Stocks and Securities </p>
    <p>15. Stolen Properties </p>
    <p>16. Unauthorized Copies of Intellectual Property</p>
    <p>17. Veterinary Drugs for animals </p>
    <p>18. Any other sanctioned or prohibited items as per law. </p>
    <p>19. Any other items deemed unfit for listing by Twiva.</p>

    
</div>

</main>
@endsection
