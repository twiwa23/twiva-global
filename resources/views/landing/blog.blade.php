<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
</head>

<body>
    
<!--Main Header-->
<div class="main-header"  id="blog-page-header">
        <!--Top Navbar-->
        <header class="header-wrapper">
            <div class="nav-p">
                <nav class="navbar navbar-expand-md bg-dark navbar-dark">

                    <!-- Toggler/collapsibe Button -->
                    <button class="navbar-toggler" type="button">
                        <span><img src="{{ asset('assets/images/toggle-icon.svg') }}" alt=""></span>
                    </button>

                    <!-- Nav Logo -->
                    <a class="navbar-brand" href="/">
                        <img src="{{ asset('assets/images/logo.svg') }}" alt="">
                    </a>


                    <!-- Navbar links -->
                    <div class="" id="collapsibleNavbar">
                        <ul class="navbar-nav">
                            <li class="nav-item">

                                <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="/">Brands</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('influencer') ? 'active' : '' }}" href="/influencer">Influencers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('user/creators') ? 'active' : '' }}" href="{{ url('/user/creators') }}">Creatives</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('eshop') ? 'active' : '' }}" href="{{ url('/eshop') }}">Eshop</a>
                            </li>
                        </ul>

                        <div class="navbar-btn-wrap">
                            @if(auth()->guard('business')->user())
                                <a href="{{ url('business/posts') }}"><button class="login-btn">Dashboard</button></a>
                            @else
                            <a href="{{ config('app.business_subdomain_url') }}"><button class="login-btn">Log In</button></a>
                            <a href="{{ config('app.business_subdomain_signup_url') }}"><button class="signup-btn">Sign Up</button></a>
                            @endif
                        </div>
                    </div>

                </nav>
            </div>
        </header>
        <!--Banner Section-->
        <section class="blog-banner">
            <div class="banner-content b-p">
                <div class="banner-text">
                    <h1>Our Latest</h1>
                    <h1 class="highlight">Blog !!</h1>
                </div>
            </div>
        </section>
</div>


<!--Main Section-->
<main class="blog-page-container">

<div class="main-content-wrapper">
    <div class="row">
        @foreach ($blog as $blogs)
            <div class="col-sm-6 col-lg-4">
                <div class="blog-content">
                    <div class="img-wrapper">
                        @if($blogs->image)
                        <img src="{{url($blogs->image)}}" alt="blog"/>
                        @else
                        <img src="{{asset('assets/images/blog-page/blog-img.png')}}" alt="">
                        @endif
                    </div>
                    <h2>@if(!empty($blogs->title)){{$blogs->title}} @else N/A @endif</h2>
                    <p> {!!html_entity_decode(substr($blogs->text,0,100))!!} </p>
                    <a href="{{url('/view-blog/'.base64_encode($blogs->id))}}">Read More</a>
                </div>
            </div>
        @endforeach
    </div>

</div>
</main>

<!--Main Footer-->
<footer class="footer-wrapper">

    <div class="footer-section f-p">
        <div class="footer-lists">
            <h1 class="footer-list-heading"><img src="{{ asset('assets/images/icons/Union.svg') }}">TWIVA</h1>
            <a href="{{ url('about-us') }}" class="footer-list-item">About Us</a>
            {{-- <a href="{{ url('pricing') }}" class="footer-list-item">Pricing</a> --}}
            {{-- <a href="" class="footer-list-item">Banned Item</a> --}}
            <a href="{{ url('contact-us') }}" class="footer-list-item">Contact Us</a>
            <a href="{{ url('blog') }}" class="footer-list-item">Blog</a>
            <a href="{{ url('faq') }}" class="footer-list-item">FAQ</a>
        </div>

        <div class="footer-lists">
            <h1 class="footer-list-heading">Legal</h1>
            <a href="{{ url('policy') }}" class="footer-list-item">Privacy Policy</a>
            <a href="{{ url('terms-conditions') }}" class="footer-list-item">Terms & Conditions</a>
        </div>

        <div class="footer-lists">
            <h1 class="footer-list-heading">Contact Us</h1>
            <a href="tel:+254708088114" class="footer-list-item"><img src="{{ asset('assets/images/icons/tel.svg') }}" alt=""> +254708088114</a>
            <a href="" class="footer-list-item"><img src="{{ asset('assets/images/icons/mail-icon.svg') }}" alt=""> info@twiva.co.ke.</a>
        </div>

        {{-- <div class="footer-lists">
            <h1 class="footer-list-heading"> Pricing</h1>
            <a href="{{ url('pricing') }}" class="footer-list-item">Pricing</a>
        </div> --}}

        {{-- <div class="footer-lists">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Your Message">
                <div class="input-group-append">
                    <button class="btn" type="submit">Talk to us</button>
                </div>
            </div>
        </div> --}}
    </div>

    <div class="copyright-section">
        <div class="copyright-heading">
            <p>Copyright © 2021 Twiva. All Rights Reserved.</p>
        </div>

        <ul class="social-icons">
            <li class="icon-list-item">
                <a href="https://www.instagram.com/twiva_influence/" target="_blank"><img src="{{ asset('assets/images/icons/instagram.svg') }}" alt=""></a>
            </li>

            <li class="icon-list-item">
                <a href="https://twitter.com/Twiva_influence" target="_blank"><img src="{{ asset('assets/images/icons/twitter.svg') }}" alt=""></a>
            </li>

            <li class="icon-list-item">
                <a href="https://www.facebook.com/twiva/" target="_blank"><img src="{{ asset('assets/images/icons/facebook.svg') }}" alt=""></a>
            </li>

            {{-- <li class="icon-list-item">
                <a href=""><img src="{{ asset('assets/images/icons/linkedin.svg') }}" alt=""></a>
            </li> --}}
        </ul>

    </div>
</footer>








<script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>
