@extends('layouts.app')
@section('title')
    Contact Us
@endsection
@section('content')
 <!--Banner Section-->
 <section class="faq-banner">
    <div class="banner-content" style="justify-content: center;">
        <div class="faq-text" style="padding-top: 40px;">
            <h1>Contact Us</h1>
        </div>
    </div>
</section>
</div>


<!--Main Section-->
<main class="contact-page position-relative">

    <div class="main-content-wrapper">
        <form method="post" enctype="multipart/form-data" action="{{url('send-contact-us')}}">
            @include('layouts.notifications')
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
        {{ csrf_field() }}
            <div class="form-group">
                <label for="first-name">First Name*</label>
                <input type="text" class="form-control" placeholder="Enter First Name"
                    id="first-name" required name="first_name" value="{{old('first_name')}}" >
                    <span class="text-danger">{{$errors->first('first_name')}}</span>
            </div>
            <div class="form-group">
                <label for="last-name">Last Name*</label>
                <input type="text" class="form-control" placeholder="Enter Last Name" id="last-name" required name = "last_name" value = "{{old('last_name')}}">
                <span class="text-danger">{{$errors->first('last_name')}}</span>
            </div>
            <div class="form-group">
                <label for="email">Email address*</label>
                <input type="email" class="form-control" placeholder="Enter email" id="email" required name = "email" value = "{{old('email')}}">
                <span class="text-danger">{{$errors->first('email')}}</span>
            </div>
            <div class="form-group">
                <label for="subject">Subject*</label>
                <input type="text" class="form-control" id="subject" required name="subject" value="{{old('subject')}}">
                <span class="text-danger">{{$errors->first('subject')}}</span>
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="5" id="message" name = "message">
                    {{old('message')}}
                </textarea>
                <span class="text-danger">{{$errors->first('message')}}</span>
            </div>

            <div class="submit-btn">
                <button type="submit" name="submit" value="Submit" class="contact-btn">Submit</button>
            </div>

        </form>

    </div>

    <div class="whatsapp-link">
        <a href="https://api.whatsapp.com/send?phone=+254708088114" target="blank">
            <img src="{{asset('/assets/images/whatsapp.gif')}}">
            <p>Talk to Us</p>
        </a>
    </div>

</main>
@endsection
