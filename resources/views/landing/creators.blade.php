@extends('layouts.app')
@section('title')
Twiva Designers
@endsection
@section('content')
<!--Banner Section-->
<section class="primary-banner creative-banner">
    <div class="banner-content b-p">
        <div class="banner-text" style="margin-top: 100px;">
            <h1>Ready to monetize</h1>
            <h1>your <span>creative?</span></h1>
        </div>

        <a href="{{ url('user/signup') }}" class="white-btn" style="font-size: 16px;">Sign Up</a>
        {{-- <div style="">
        <a href="{{url('/user/signup')}}" class="white-btn">Sign Up</a>
        <a href="{{url('/user/login')}}" class="white-btn">Login</a>
        </div> --}}

        <div class="banner-img" style="width: 550px;height:600px;right: 120px;" id="designer-banner-img">
            <img src="{{ asset('assets/images/creatives-page/creative-banner-image.svg') }}" alt="">
        </div>
    </div>
</section>
</div>

<!--Main Section-->
<main class="creatives-page">

    <div class="why-join-heading">
        Why join today?
    </div>

    <div class="performance-wrapper">
        <div class="row">
            <div class="col-lg-4">
                <div class="content-box">
                    <div class="box-image">
                        <img src="{{ asset('assets/images/creatives-page/pay-day.png') }}" alt="">
                    </div>
                    <h2>Anyday can be a Payday!</h2>
                    <p class="line-1" style="width: 85%;"> Get the opportunity to work with industry leading brands who
                        are constantly looking for
                        <span>creative people like you</span> on our platform.
                    </p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="content-box">
                    <div class="box-image">
                        <img src="{{ asset('assets/images/creatives-page/studio.png') }}" alt="">
                    </div>
                    <h2>Access to Twiva Studio</h2>
                    <p class="line-2" style="width: 85%;">Our Studio is equipped with state of the art equipment that
                        will enable you to deliver
                        <span>high quality results.</span> Don't slow yourself down.
                    </p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="content-box">
                    <div class="box-image">
                        <img src="{{ asset('assets/images/creatives-page/training.svg') }}" alt="" style="width: 280px;">
                    </div>
                    <h2>Free training courses</h2>
                    <p class="line-3" style="width: 85%;">We organize free training on various topics to equip you with
                        new industry skills and knowledge,
                        <span>giving you an edge</span> when seeking new work opportunities.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrapper content-p">
        <div class="main-heading">
            <h1>Twiva Creatives</h1>
            <p>sign up to collaborate with Twiva Creatives!</p>
            <a href="{{ url('user/signup') }}" class="red-btn m-0">Sign Up</a>
        </div>
        
        <div id="hmenu"></div>

        <div class="profile-menu">
            <ul class="menu-lists" id="elem">
                <li class="menu-item @if(Request::path() == 'user/creators' && !app("request")->input('category_id') ) active @endif">
                    <a href="{{url('/user/creators')}}" class="menu-link">All</a>
                </li>

                @foreach($category as $value)
                <li class="menu-item  @if(app("request")->input('category_id') == $value->id) active @endif">
                    <a href="{{url('/user/creators').'?category_id='.$value->id.'#hmenu'}}"
                        class="menu-link">{{$value->name}}</a>
                </li>
                @endforeach
                {{--
            <li class="menu-item">
                <a href="" class="menu-link">Photography</a>
            </li>

            <li class="menu-item">
                <a href="" class="menu-link">Videographer</a>
            </li>

            <li class="menu-item">
                <a href="" class="menu-link">Blogger</a>
            </li>

            <li class="menu-item">
                <a href="" class="menu-link">Artist</a>
            </li> --}}

                <!-- <a href="" class="right-arrow">
                    <img src="{{ asset('assets/images/icons/chevron-right.svg') }}" alt="">
                </a> -->
            </ul>
        </div>



        <div class="profile-section">

            <div class="row">
                @forelse ($users as $user)
                <div class="col-md-4">
                    @php
                        $bg_image = $user->background_image ?: 'assets/images/creatives-page/profile-back.jpg';
                    @endphp
                    @if(!empty($user->user_profile))
                    @php $image = asset('storage/uploads/images/business/'.$user->user_profile); @endphp
                    @else
                    @php $image = asset('assets/images/creatives-page/profile-image.jpg'); @endphp
                    @endif
                    <div class="card">

                        <div class="cover-img">
                            <img src="{{ asset($bg_image) }}" alt="">
                        </div>

                        <div class="profile-img">
                            <img src="{{ $image }}">
                        </div>

                        <div class="card-body">
                            <div class="card-text">
                                <h4 class="name">@if(!empty($user->user_name)){{$user->user_name}} @else N/A @endif
                                </h4>
                                <p class="work-profile">@if(!empty($user->category_name)){{$user->category_name}}
                                    @else N/A @endif</p>
                            </div>

                            <div class="social-icons">
                                <a href="">
                                    <img src="{{ asset('assets/images/icons/insta-black.svg') }}" alt="">
                                </a>

                                <a href="">
                                    <img src="{{ asset('assets/images/icons/circle-black.svg') }}" alt="">
                                </a>

                                <a href="">
                                    <img src="{{ asset('assets/images/icons/twitter-black.svg') }}" alt="">
                                </a>

                                <a href="">
                                    <img src="{{ asset('assets/images/icons/youtube-black.svg') }}" alt="">
                                </a>
                            </div>

                            <div class="profile-links">
                                <a href="{{url('/user/portfolio/'.$user->user_id)}}" class="profile-btn">Profile</a>
                                <a href="{{url('/user/hire-me/'.$user->user_id)}}" class="contact-btn">Contact
                                    me</a>
                            </div>
                        </div>

                    </div>
                </div>
                @empty
                <div class="text-center">
                    <h2 style="font-size: 30px;">No Data Found</h2>
                </div>
                @endforelse
            </div>

            <ul class="pagination">
            <a href="{{asset('user/signup')}}" class="red-btn m-0">View More</a>
            <!-- <a href="http://business.twiva.co.ke/influencer/auth/signup.php" class="red-btn m-0">View More</a> -->
                <!-- {{ $users->appends(request()->except('page'))->links() }} -->
                {{-- <li class="page-item">
                    <a class="page-link" href="#"><img src="{{ asset('assets/images/icons/red-left-arrow.svg') }}"
                            alt=""></a>
                </li>
                <li class="page-item"><a class="page-link active" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#"><img src="{{ asset('assets/images/icons/red-right-arrow.svg') }}"
                            alt=""></a>
                </li> --}}
            </ul>

        </div>

    </div>

</main>

@endsection
