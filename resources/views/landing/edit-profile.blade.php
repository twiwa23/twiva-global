@extends('creator-layout.app')
@section('title')
    Edit Profile
@endsection
@section('content')
<div class="main-dashboard-container" id="service-page">

    <div class="page-content p-t">
        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper">
            <ul class="breadcrumb">
                <li><a href="{{ url('user/portfolio/'.auth()->user()->id) }}" class="breadcrumb-heading active">Dashboard<img src="{{ asset('creative/assets/images/icons/chevron-right.svg') }}" alt=""></a></li>
                <li><a href="#" class="breadcrumb-heading">Edit Profile</a></li>
            </ul>
        </div>
        <!--**********  Breadcrumb End ***********-->

        <div class="mobile-breadcrumb-wrapper">
            <ul class="mobile-breadcrumb">
                <li><a href="#" class="breadcrumb-heading"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Add Service</a></li>
            </ul>
        </div>
        @include('layouts.notifications')
        <form method="POST" id="form_validate" enctype=multipart/form-data ">
               {{csrf_field()}}
        <div class="browse-image">
            <div class="cover-img-upload">
                <label for="cover-img" class="upload-img">
                    <input type="file" class='cover-uploader' name="background_image" id="cover-img" />
                    @if(!empty($user->background_image))
                            <img src="{{ $user->background_image }}" id="main-img" class="profileImg">
                        @else
                            <img src="{{ asset('creative/assets/images/icons/blank.png') }}"  id="main-img" class="profileImg">
                        @endif
                    <div class="change-cover">
                        <img src="{{ asset('creative/assets/images/icons/camera-img.svg') }}" alt="">
                        Change cover image
                    </div>
                </label>
            </div>
        </div>

        <div class="service-container edit-profile">
            <div class="add-service">

                <div class="profile-img-upload">
                    <label for="service-img" class="upload-img">
                        <input type="file" class='uploader' id="service-img" name="profile"  />
                        @if(!empty($user->profile))
                            <img src="{{ $user->profile }}" id='image' class="profileImg">
                        @else
                            <img src="{{ asset('creative/assets/images/icons/default.png') }}" id='image' class="profileImg">
                        @endif
                        <div class="camera-img">
                            <img src="{{ asset('creative/assets/images/icons/camera-edit.svg') }}" alt="">
                        </div>
                    </label>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group" style="margin-bottom: 24px;">
                                <label>Title</label>
                                <input type="text"  name="name" id = "name"  value="{{ $user->name ?? ''}}" class="form-control">
                                <span id="name-error" class="error" for="name">{{$errors->first('name')}}</span>
                            </div>

                            <div class="form-group">
                                <label>Select Category</label>
                                <select name="category_name" id="category_name" class="form-control">
                                    <option value=""> Select Category</option>
                                    @foreach($category as $value)
                                    <option value="{{ $value->id }}" {{ $user_category_id == $value->id ? 'selected="selected"' : '' }}>{{ $value->name }}</option>
                                    @endforeach
                                </select>
                                <span id="category_name-error" class="error" for="category_name">{{$errors->first('category_name')}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id = "description" name="description" placeholder="Description" maxlength="1000" class="form-control custom-h">{{ $user->description ?? '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="btn-wrapper">
                    <button class="white-btn">Cancel</button>
                    <button class="red-btn">Update</button>
                </div>
            </div>
        </div>
        </form>


    </div>

</div>

@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
     $("#name").on("focusout",function(){
                let val = $(this).val();

                  //alert(val);
                var data = {
                  '_token': "{{csrf_token()}}",
                  'name': val,
                };


              $.ajax({
                  url:"{{url('user/check-exist-name')}}",
                  type:'POST',
                  data:data,
                  success: function(res){
                    console.log(res)
                    if(res == 1){
                      $("#name-error").text("Username is already exists.").show();
                    }else{
                      $("#name-error").text("").hide();
                    }
                  },
                  error: function(data, textStatus, xhr) {
                    if(data.status == 422){
                      var result = data.responseJSON;
                      alert('Something went worng.');
                      window.location.href = "";
                      return false;
                    }
                  }
                });

        });

    </script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.indexOf(" ") < 0 && value != "";
}, "Space not allowed");

$('#form_validate').validate({
    rules: {
        name: {
            required: true,
            maxlength:50,
            minlength:3,
            noSpace : true
        },

        description: {
            required: true,
            minlength:10,
            maxlength:1000
        },
        category_name: {
            required: true,
        },
    },

    messages:{
        name:{
            required:'Please enter username.',
            minlength:'Username must be at least 3 characters.',
            maxlength:'Username maximum 15 characters.'
        },

        description:{
            required:'Please enter description.',
            minlength:'Description must be at least 10 characters.',
            maxlength:'Description maximum 1000 characters.'
        },

        category_name:{
            required:'Please select category name.',

        },
    },

    submitHandler:function(form){

            if($("#name-error").text().length > 0){
                return false;
            }else {
                form.submit();
            }
    }
});
</script>
<script>

    // Cover Image Upload //
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#main-img')
                    .attr('src', e.target.result)
                    .width("100%")
                    .height("100%");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".cover-uploader").change(function() {
        readURL(this);
    });
</script>

@endsection
