@extends('layouts.app')
@section('title')
Twiva Eshop
@endsection
@section('content')
<!--Banner Section-->
    <section class="primary-banner eshop-banner">
        <div class="banner-content b-p">
            <div class="banner-text" style="margin-top: 80px;">
                <h1>Skip marketing,</h1>
                <h1>start <span>selling!</span></h1>
            </div>

            @if(auth()->guard('business')->user())
            @else
            <a href="{{ config('app.business_subdomain_signup_url') }}" class="white-btn">Get Started</a>
            @endif

            <div class="banner-img" style="right: 115px; bottom: 50px;">
                <img src="./assets/images/eshop-page/esop-banner.svg" alt="">
            </div>
        </div>
    </section>
</div>


<!--Main Section-->
<main class="eshop-page">

    <div class="main-content-wrapper">

        <div class="main-heading">
            <h1>Ready to increase your sales today?</h1>
            <p>List your products and services on 1000 online shops and Let Twiva Influencers sell on your behalf.</p>
        </div>


        <div class="eshop-services content-p">
            <div class="row">

                <div class="col-sm-6 col-lg-6">
                    <div class="services-container">

                        <ul class="service-lists">
                            <li class="list-item">
                                <div class="list-content active">
                                    <h4>Join Twiva</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/red-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <li class="list-item">
                                <div class="list-content">
                                    <h4>List your products or services</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <li class="list-item">
                                <div class="list-content">
                                    <h4>Twiva Influencers will sell on your behalf</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <li class="list-item">
                                <div class="list-content">
                                    <h4>Receive and fulfill orders</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-6">
                    <div class="mobile-image">
                        <img src="./assets/images/eshop-page/phone.png" alt="">
                    </div>
                </div>
            </div>
        </div>


        <div class="start-earning-section increase-sales">
            <div class="left-image">
                <img src="./assets/images/icons/left-image.svg" alt="">
            </div>

            <div class="earning-section-content">
                <h1>Ready to increase your sales Today?</h1>
                <div class="earning-tips">
                    <p><img src="./assets/images/icons/white-dot.svg"><img src="./assets/images/icons/dollar-sign.svg">
                        Guaranteed increased sales</p>
                    <p><img src="./assets/images/icons/white-dot.svg"><img src="./assets/images/icons/settings.svg">
                        Easy to set up</p>
                </div>
                @if(auth()->guard('business')->user())
                @else
                <a href="{{ config('app.business_subdomain_signup_url') }}" class="white-btn">Get Started</a>
                @endif
            </div>

            <div class="right-image">
                <img src="./assets/images/icons/right-image.svg" alt="">
            </div>
        </div>

        <div class="main-heading" style="margin-bottom: 85px;">
            <h1>Ready to increase your sales today?</h1>
            <p>List your products and services on 1000 online shops and Let Twiva Influencers sell on your behalf.</p>
        </div>

        <div class="eshop-services content-p">
            <div class="row">

                <div class="col-sm-6 col-lg-6">
                    <div class="services-container">

                        <ul class="service-lists" style="margin-top: 20px;">
                            <li class="list-item">
                                <div class="list-content active">
                                    <h4>Create your Twiva eShop</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/red-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <li class="list-item">
                                <div class="list-content">
                                    <h4>Select the products and services your audience love</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <li class="list-item">
                                <div class="list-content">
                                    <h4> Twiva automatically list to your social media accounts </h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <li class="list-item">
                                <div class="list-content">
                                    <h4>Sell while you do what you love</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li>

                            <!-- <li class="list-item">
                                <div class="list-content">
                                    <h4>Sell while you do what you love</h4>
                                    {{-- <p>Manage your important documents and notes for each action. </p> --}}
                                    <div class="right-arrow"></div>
                                </div>
                                <div class="list-circle">
                                    <img src="{{ asset('assets/images/icons/grey-circle.svg') }}" alt="">
                                </div>
                            </li> -->

                        </ul>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-6">
                    <div class="mobile-image">
                        <img src="./assets/images/eshop-page/phone-2.png" alt="" id="mobile-img-2">
                    </div>
                </div>
            </div>
        </div>


        <div class="start-earning-section" style="margin-bottom: 88px;">
            <div class="left-image">
                <img src="{{ asset('assets/images/icons/left-image.svg') }}" alt="">
            </div>

            <div class="earning-section-content">
                <h1>Start Earning Today!</h1>
                <div class="earning-tips">
                    <p><img src="./assets/images/icons/white-dot.svg"><img src="./assets/images/icons/dollar-sign.svg">
                        Free to join</p>
                    <p><img src="./assets/images/icons/white-dot.svg"><img src="./assets/images/icons/settings.svg">
                        Easy to set up</p>
                </div>
                @if(auth()->guard('business')->user())
                @else
                <a href="{{ config('app.business_subdomain_signup_url') }}" class="white-btn">Get Started</a>
                @endif
            </div>

            <div class="right-image">
                <img src="{{ asset('assets/images/icons/right-image.svg') }}" alt="">
            </div>
        </div>


    </div>

</main>

@endsection
