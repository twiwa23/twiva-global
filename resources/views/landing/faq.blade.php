@extends('layouts.app')
@section('title')
    Twiva Faqs
@endsection
@section('content')
    <!--Banner Section-->
    <section class="faq-banner">
        <div class="banner-content b-p">
            <div class="faq-text">
                <h1>Frequently Asked</h1>
                <h1>Questions</h1>
            </div>
            <div class="faq-img">
                <img src="./assets/images/icons/question-mark.svg" alt="">
            </div>
        </div>
    </section>
</div>


<!--Main Section-->
<main class="faq-page">

    <div class="main-content-wrapper">
        <nav class="navbar navbar-expand-sm">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="pill" href="#general">General</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#business">Businesses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#influencers">Influencers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#buyers">Buyers</a>
                </li>
            </ul>
        </nav>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane container active" id="general">
                <div class="accordion" id="general-accordion">
                    <div class="card">
                        <div class="card-header active" id="g-headingOne">
                            <h2 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                How does Twiva work?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="g-headingOne" data-parent="#general-accordion">
                            <div class="card-body">
                                Twiva is a mobile application platform that enhances the collaboration between brands and influencers. Twiva aims to be the platform of choice that delivers value to businesses and influencers.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="g-headingTwo">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Get to know Twiva?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="g-headingTwo" data-parent="#general-accordion">
                            <div class="card-body">
                                Twiva offers two major services. Our Post feature is a tool where companies collaborate with specific influencers in promoting their businesses. The Gig feature allows companies to invite influencers to events or venues and harness the marketing power of influencers' presence, live video blogging, and venue-specific advertising.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="g-headingThree">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                What social media channels does Twiva support?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="g-headingThree" data-parent="#general-accordion">
                            <div class="card-body">
                                We are currently supporting collaborations on Instagram, Facebook and Twitter. Reach out to us if you need to use our services on other platforms.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="g-headingFour">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                What payment gateway does Twiva use?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="g-headingFour" data-parent="#general-accordion">
                            <div class="card-body">
                                We are currently supporting transactions through M-Pesa and look to integrate other gateways
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane container fade" id="business">
                <div class="accordion" id="business-accordion">
                    <div class="card">
                        <div class="card-header active" id="b-headingOne">
                            <h2 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                How does Twiva work?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="b-headingOne" data-parent="#business-accordion">
                            <div class="card-body">
                                You create a company profile and then search for influencers you would like to work with. You can identify influencers of a certain age, gender, follower number, and even those that are near your business. You then set the terms of your engagement and compensation level. We take care of streamlining your collaboration and processing the payments and logistics of the engagement. We provide analytics to track the performance of your collaboration.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="b-headingTwo">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                How can I get started?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="b-headingTwo" data-parent="#business-accordion">
                            <div class="card-body">
                                Download the app and sign up to use our services for free. Our easy-to-use mobile application platform will streamline your collaboration with influencers. Reach out to us if you need help getting started.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="b-headingThree">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                What social media channels does Twiva support?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="b-headingThree" data-parent="#business-accordion">
                            <div class="card-body">
                                We are currently supporting collaborations on Instagram, Facebook and Twitter.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="b-headingFour">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Who are the Influencers on Twiva?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="b-headingThree" data-parent="#business-accordion">
                            <div class="card-body">
                                At Twiva, we believe everyone has some level of influence. Our influencers range from micro-influencers (<10,000 followers) to bonafide influencers (>100,000 followers). This allows a broad range of selection to meet your marketing needs.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="b-headingfifth">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFifth" aria-expanded="false" aria-controls="collapseFifth">
                                How can I track my post's performance?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseFifth" class="collapse" aria-labelledby="b-headingfifth" data-parent="#business-accordion">
                            <div class="card-body">
                                Twiva offers a comprehensive analytics tool to track your engagement and collaborations in one place. Our analytics are provided for Posts and Campaigns.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane container fade" id="influencers">
                <div class="accordion" id="influencers-accordion">
                    <div class="card">
                        <div class="card-header active" id="i-headingOne">
                            <h2 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                How does Twiva work?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="i-headingOne" data-parent="#influencers-accordion">
                            <div class="card-body">
                                Once you have the Twiva app on your phone, companies can identify you for sponsored content. For a Post or a Gig, you will receive a direct invite from a company. Review the terms of the invite and decide whether you would like to accept the offer or negotiate for higher compensation. Once you agree, you will leverage our streamlined collaboration tool to complete the ask. Once the task is completed, we release the payment within 24 hrs. You control the terms you accept and when you work.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingTwo">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                How do I join Twiva
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="i-headingTwo" data-parent="#influencers-accordion">
                            <div class="card-body">
                                Download our app from the Apple store. Android version is coming soon.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingThree">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                How do I reach out to brands to collaborate with me?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="i-headingThree" data-parent="#influencers-accordion">
                            <div class="card-body">
                                Once you join our platform, companies will reach out to you for collaboration before you even contact them.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingfour">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Who decides how much I get paid?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="i-headingfour" data-parent="#influencers-accordion">
                            <div class="card-body">
                                Twiva is not a middleman. Companies will usually set out how much they are willing to pay initially. If this amount is not desirable, then you have the option to negotiate directly with the brand through the app.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingfive">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                What sort of rights of usage does the brand get to my content?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="i-headingfive" data-parent="#influencers-accordion">
                            <div class="card-body">
                                The transfer of ownership happens when you are compensated for your services in promoting the company. Any uncompensated piece of your craft is still in your full ownership.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingsix">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                How many collaborations can I get?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="i-headingsix" data-parent="#influencers-accordion">
                            <div class="card-body">
                                There is no limit. You control how much you want to take on.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingseven">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseSeventh" aria-expanded="false" aria-controls="collapseSeventh">
                                How do I get paid?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseSeventh" class="collapse" aria-labelledby="i-headingseven" data-parent="#influencers-accordion">
                            <div class="card-body">
                                Once your engagement with the company you're in collaboration with ends, Twiva releases your payment directly into your M-Pesa mobile account. You receive the money in 24hrs after your collaboration completes.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="i-headingeight">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                How long do I have to keep the content on my social media timeline?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="i-headingeight" data-parent="#influencers-accordion">
                            <div class="card-body">
                                Our Terms of Use outlines that all posts made from Twiva should be on your social media page for at least 60 days.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane container fade" id="buyers">
                <div class="accordion" id="buyers-accordion">
                    <div class="card">
                        <div class="card-header active" id="by-headingOne">
                            <h2 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                How do I request a refund?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="by-headingOne" data-parent="#buyers-accordion">
                            <div class="card-body">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam explicabo quia, incidunt aspernatur dignissimos rem temporibus neque porro! Laboriosam eaque, quae ipsam dolores officia sint. Maxime, molestiae. Culpa, distinctio sapiente?
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="by-headingTwo">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                What is your return policy?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="by-headingTwo" data-parent="#buyers-accordion">
                            <div class="card-body">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam explicabo quia, incidunt aspernatur dignissimos rem temporibus neque porro! Laboriosam eaque, quae ipsam dolores officia sint. Maxime, molestiae. Culpa, distinctio sapiente?
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="by-headingThree">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                How can I track my order?
                                <span class="plus"><img src="./assets/images/icons/plus.svg" alt=""></span>
                                <span class="minus"><img src="./assets/images/icons/minus.svg" alt=""></span>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="by-headingThree" data-parent="#buyers-accordion">
                            <div class="card-body">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam explicabo quia, incidunt aspernatur dignissimos rem temporibus neque porro! Laboriosam eaque, quae ipsam dolores officia sint. Maxime, molestiae. Culpa, distinctio sapiente?
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</main>

@endsection
