@extends('creator-auth-layout.app')
@section('title')
Forgot Password
@endsection
@section('styles')
<style>
    .error {
        color: red;
    }
</style>
@endsection
@section('content')
<div class="login">

    <div class="container-fluid m-0">
        <div class="back-button">
            <a href="{{url('/user/creators')}}"><button id="desktop-btn"><img
                src="{{ asset('creative/assets/images/icons/chevron-left-white.svg') }}"
                alt="">Back</button></a>
            <a href="{{url('/user/creators')}}"><button id="mobile-btn"><img
                src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Back</button></a>
        </div>
    </div>

    <div class="container">
        <div class="login-inner">

            <div class="login-left">
                <!-- <img src="../images/banner/login.png"> -->
            </div>

            <div class="login-right">
                <div class="login-section">
                    <div class="logo"><img src="{{ asset('creative/assets/images/logo-red.svg') }}"></div>

                    <p>Enter your registered email address to get a reset link.</p>
                    @include('layouts.notifications')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" id="form_validate">
                        {{csrf_field()}}
                    <div class="input-field">
                        <label>E-mail Address</label>
                        <input id = "email" name="email" value = "{{old('email')}}"  type="text" placeholder="Email">
                        <h5 id="email-error" class="error" for="email">{{$errors->first('email')}}</h5>
                    </div>
                    <button id="submit" type="submit">Confirm</button>
                    </form>
                    <div class="no-account">Back to <a href="{{ url('user/login') }}">Login</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="#">Terms & Conditions</a>
    </div>

</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
   <script>
   $('#form_validate').validate({
      rules: {

         email: {
              required: true,
              email:true,
              maxlength:200
          },

      },

        messages:{

          email:{
            required:'Please enter email address.',
            maxlength:'Email address maximum 200 characters.'
          },
        },
        });
   </script>

@endsection
