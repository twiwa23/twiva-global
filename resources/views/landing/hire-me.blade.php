@extends('layouts.app')
@section('title')
    Hire Me
@endsection
@section('content')
<div class="main-dashboard-container" id="service-page">

    <div class="page-content p-t">
        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper" style="padding-top: 40px">
            <ul class="breadcrumb">
                <li><a href="{{ url('user/portfolio/'.$user->id) }}" class="breadcrumb-heading active">{{ $user->name }}<img src="{{ asset('assets/images/icons/chevron-right.svg') }}" alt=""></a></li>
                <li><a href="#" class="breadcrumb-heading">Hire me</a></li>
            </ul>
        </div>
        <!--**********  Breadcrumb End ***********-->

        <div class="mobile-breadcrumb-wrapper">
            <ul class="mobile-breadcrumb">
                <li><a href="#" class="breadcrumb-heading"><img src="{{ asset('assets/images/icons/chevron-left.svg') }}" alt="">Hire me</a></li>
            </ul>
        </div>

        <div class="post-container" id="hire-me-page-wrapper">
            <div class="post-heading">
                <h3>Hire Me ( {{ $user->name }} )</h3>
            </div>
            @include('admin.layouts.notifications')
            <form method = "post" action  = "" >
                {{csrf_field()}}
            <div class="post-input-container">
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group">
                                <label>From</label>
                                <input type="text" name = "email" value = "{{old('email')}}"  class="form-control">
                                @if($errors->has('email'))
                                    <div class="error text-danger" >{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="input-section">
                            <div class="form-group">
                                <label>To</label>
                                <input type="text"  name = "to" value = "{{$user->name}}" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="post-input-container">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="input-section">
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" class="form-control" name = "subject" value = "{{old('subject')}}">
                                @if($errors->has('subject'))
                                    <div class="error text-danger" >{{ $errors->first('subject') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="input-section">
                            <div class="form-group">
                                <label>Message</label>
                                <textarea name="text" value = "{{old('text')}}" rows="5" class="form-control c-text" placeholder="Write Something here.." maxlength="1000"></textarea>
                                @if($errors->has('text'))
                                    <div class="error text-danger" >{{ $errors->first('text') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="btn-wrapper">
                <button class="red-btn">Submit</button>
            </div>
            </form>
        </div>

    </div>

</div>
@endsection
@section('scripts')
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('text');
    CKEDITOR.config.autoParagraph = false;
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>



    $('#form_validate').validate({
        rules: {
            email: {
                required: true,
                email:true,
                maxlength:200
            },

            subject: {
                required: true,
                maxlength:100,
                minlength:3,

            },

            text: {
                required: true,
                minlength:10,
                maxlength:2000
            },

        },

        messages:{
            subject:{
                required:'Please ensubjectter subject.',
                minlength:'subject must be at least 3 characters.',
                maxlength:'subject maximum 100 characters.'
            },
            email:{
                required:'Please enter email address.',
                email:'Please enter a valid email address.',
                maxlength:'Email address maximum 200 characters.'
            },


            text:{
                required:'Please enter tex.',
                minlength:'Text must be at least 10 characters.',
                maxlength:'Text maximum 2000 characters.'
            },


        },

          /*errorHandler:function(e){
            console.log(e)
          }*/


    });
</script>
@endsection
