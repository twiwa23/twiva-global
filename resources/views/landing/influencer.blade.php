@extends('layouts.app')
@section('title')
Twiva Influencers
@endsection
@section('content')
<!--Banner Section-->
<section class="primary-banner">
    <div class="banner-content b-p">
        <div class="banner-text">
            <h1>Bring your <span>Influence,</span></h1>
            <h1>We'll bring the</h1>
            <h1>Brands!</h1>
        </div>

        <a href="{{ config('app.business_subdomain_signup_url') }}" class="white-btn">Get Started</a>

        <div class="banner-img" style="right:100px; bottom:50px;">
            <img src="./assets/images/influencer-page/influencer.svg" alt="">
        </div>
    </div>
</section>
</div>


<!--Main Section-->
<main class="influencer-page">

    <div class="main-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-heading">
                        <h1>Why Twiva is the Influencer’s Platform</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-tab-content content-p">
            <nav class="navbar navbar-expand-sm">
                <!-- Tab Menu -->
                <ul class="navbar-nav nav">
                    <li class="nav-item">
                        <a href="#first-item" class="nav-link active">Everything in just One App</a>
                    </li>
                    <li class="nav-item">
                        <a href="#second-item" class="nav-link">Fair and Timely Payment</a>
                    </li>
                    <li class="nav-item">
                        <a href="#third-item" class="nav-link">Earn Money and Build your Audience</a>
                    </li>
                    <li class="nav-item">
                        <a href="#fourth-item" class="nav-link">Discover New Brands to Partner with</a>
                    </li>
                </ul>
            </nav>

            <!-- Tab panes -->
            <div class="tab-section-container container-fluid">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="tab-items" id="first-item">
                            <div class="img-wrapper">
                                <img src="./assets/images/influencer-page/everything-in-just-one-app.svg" alt="">
                            </div>
                            <div class="text-wrapper">
                                <h2><img src="./assets/images/red-dot.svg" alt="">Everything in Just One App</h2>
                                <p> Silence the noise in your DM.Choose the gigs you are interested in, set your rates,
                                    and get paid - all on Twiva app!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="tab-items right-align" id="second-item">
                            <div class="img-wrapper">
                                <img src="./assets/images/influencer-page/fair-and-timely-payment.svg" alt="">
                            </div>
                            <div class="text-wrapper">
                                <h2><img src="./assets/images/red-dot.svg" alt="">Fair and timely Payment</h2>
                                <p> Games with your sweat? Let us sweat on your behalf. You should be paid on time.
                                    Who said an influencer is a collector?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="tab-items" id="third-item">
                            <div class="img-wrapper">
                                <img src="./assets/images/influencer-page/earn-money-&-build-your-audience.png" alt="">
                            </div>
                            <div class="text-wrapper">
                                <h2><img src="./assets/images/red-dot.svg" alt="">Earn Money & Build Your Audience</h2>
                                <p> Get paid doing what you love while building your number of followers on social media.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="tab-items right-align" id="fourth-item">
                            <div class="img-wrapper">
                                <img src="./assets/images/influencer-page/discover-new-brands-to-partner-with.png" alt="">
                            </div>
                            <div class="text-wrapper">
                                <h2><img src="./assets/images/red-dot.svg" alt="">Discover New Brands to Partner With</h2>
                                <p> Just raise your hand when a new brand posts a campaign brief and express your interest in
                                    working with the brand.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="start-earning-section">
            <div class="left-image">
                <img src="./assets/images/icons/left-image.svg" alt="">
            </div>

            <div class="earning-section-content">
                <h1>Start Earning Today!</h1>
                <div class="earning-tips">
                    <p><img src="./assets/images/icons/white-dot.svg"><img src="./assets/images/icons/dollar-sign.svg"> Free to join</p>
                    <p><img src="./assets/images/icons/white-dot.svg"><img src="./assets/images/icons/settings.svg"> Easy to set up</p>
                </div>
                <a href="" class="white-btn">Get Started</a>
            </div>

            <div class="right-image">
                <img src="./assets/images/icons/right-image.svg" alt="">
            </div>
        </div>
        @if($featured)

        <div class="featured-influencers content-p">

            <div class="influencer-heading">
                <h1>Featured Influencers</h1>
            </div>

            <!--Carousel Wrapper-->
            <div id="multi-item-slider">
                <!-- class="carousel slide carousel-multi-item" data-ride="carousel" -->

                <!--Controls-->
                <!-- <div class="controls-top">
                <a class="btn-floating" href="#multi-item-slider" data-slide="prev">
                    <img src="./assets/images/icons/left-arrow.svg" alt="">
                </a>
                <a class="btn-floating" href="#multi-item-slider" data-slide="next">
                    <img src="./assets/images/icons/right-arrow.svg" alt="">
                </a>
                </div>-->
                <!--/.Controls-->

                <!--Indicators-->
                <!-- <ol class="carousel-indicators">
                <li data-target="#multi-item-slider" data-slide-to="0" class="active"></li>
                <li data-target="#multi-item-slider" data-slide-to="1"></li>-->
                <!-- <li data-target="#multi-item-slider" data-slide-to="2"></li> -->
                <!-- </ol>-->
                <!--/.Indicators-->

                <!--Slides-->
                <!--<div class="carousel-inner" role="listbox">-->

                <!--First slide-->
                <!--<div class="carousel-item active">

                    <div class="row">
                        {{-- <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-1.png">
                                </div>

                                <div class="card-body">
                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">Lara Cooper in Reebok</h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div> --}}
                        {{-- @foreach($featured as $featureds) --}}
                        @for ($i=0; $i < count($featured); $i++)

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <div class="card-image">
                                    @if($featured[$i]['image'])
                                    <a href="{{$featured[$i]['link']}}" target="_blank"><img src="{{url($featured[$i]['image'])}}" alt="blog"/></a>
                                    @else
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-2.png">
                                    @endif
                                </div>

                                <div class="card-body">
                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">{{$featured[$i]['link']}}
                                    </h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($i==2)
                        @php
                            break;
                        @endphp
                        @endif
                        @endfor
                        {{-- @endforeach --}}

                        {{-- <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <div class="card-image">
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-3.png">
                                </div>

                                <div class="card-body">

                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/youtube-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">Lara Cooper in Reebok</h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> --}}

                    </div>
                </div>-->
                <!--/.First slide-->


                <!--Second slide-->
                <!--<div class="carousel-item">

                    <div class="row">
                        {{-- <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-1.png">
                                </div>

                                <div class="card-body">
                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">Lara Cooper in Reebok</h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <div class="card-image">
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-2.png">
                                </div>

                                <div class="card-body">
                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">Lara Cooper in Reebok</h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <div class="card-image">
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-3.png">
                                </div>

                                <div class="card-body">

                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/youtube-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">Lara Cooper in Reebok</h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
			</div> --}}

                        @for ($i=0; $i < count($featured); $i++)

                        @if($i>=3)

                        <div class="col-md-4 clearfix d-none d-md-block">
                            <div class="card mb-2">
                                <div class="card-image">
                                    @if($featured[$i]['image'])
                                    <a href="{{$featured[$i]['link']}}" target="_blank"><img src="{{url($featured[$i]['image'])}}" alt="blog"/></a>
                                    @else
                                    <img class="card-img-top" src="./assets/images/influencer-page/inluencer-2.png">
                                    @endif
                                </div>

                                <div class="card-body">
                                    <div class="icons">
                                        <a href="">
                                            <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/fb-icon.svg" alt="">
                                        </a>

                                        <a href="">
                                            <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                        </a>
                                    </div>

                                    <h4 class="card-title">{{$featured[$i]['link']}}</h4>

                                    <div class="card-text">
                                        <div>
                                            <h2>240K</h2>
                                            <p>Audience Reach</p>
                                        </div>
                                        <div>
                                            <h2>9000<span>Ksh</span></h2>
                                            <p>Worth Sale</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endfor
                    </div>
                </div>-->
                <!--Second slide-->


                <!--</div>-->
                <div class="container-fluid">
                    <div id="influencer-carousel" class="carousel slide" data-ride="carousel" data-interval="9000">
                        <div class="carousel-inner row flex-nowrap w-100" role="listbox">
			    @foreach($featured as $key=>$feature)

                            <div class="carousel-item col-12 col-sm-12 col-md-4 @if($key==1)  active  @endif">
                                <div class="panel panel-default">
                                    <div class="panel-thumbnail">
                                        <div class="card">
                                            <div class="card-image">
                                                @if($feature->image)
                                                <a href="{{$feature->link}}" target="_blank"><img src="{{url($feature->image)}}" alt="blog" /></a>
                                                @else
                                                <a href="{{$feature->link}}" target="_blank"><img class="card-img-top" src="./assets/images/influencer-page/inluencer-1.png"></a>
                                                @endif
                                            </div>

                                            <div class="card-body">
                                                <div class="icons">
                                                    @if($feature->type=='i')
                                                    <a href="{{$feature->link}}" target="blank">
                                                        <img src="./assets/images/icons/instagram-icon.svg" alt="">
                                                    </a>
                                                    @endif
                                                    @if($feature->type=='f')
                                                    <a href="{{$feature->link}}" target="blank">
                                                        <img src="./assets/images/icons/fb-icon.svg" alt="">
                                                    </a>
                                                    @endif
                                                    @if($feature->type=='t')
                                                    <a href="{{$feature->link}}" target="blank">
                                                        <img src="./assets/images/icons/twitter-icon.svg" alt="">
                                                    </a>
                                                    @endif
                                                </div>
                                                <!-- <h4 class="card-title">{{$feature->link}}</h4> -->
                                                <div class="card-text">
                                                    <!-- <div>
                                                        <h2>240K</h2>
                                                        <p>Audience Reach</p>
                                                    </div> -->
                                                    <!-- <div>
                                                        <h2>9000<span>Ksh</span></h2>
                                                        <p>Worth Sale</p>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <!--Controls-->
                        <a class="carousel-control-prev btn-floating" href="#influencer-carousel" data-slide="prev">
                            <img src="./assets/images/icons/left-arrow.svg" alt="">
                        </a>
                        <a class="carousel-control-next btn-floating" href="#influencer-carousel" data-slide="next">
                            <img src="./assets/images/icons/right-arrow.svg" alt="">
                        </a>
                        <!--/.Controls-->

                    </div>
                </div>

            </div>
        </div>
        @endif

    </div>

</main>

@endsection
