@extends('layouts.app')
@section('title')
Twiva Brands
@endsection
@section('content')

        <!--Banner Section-->
        <section class="banner b-size">
            <!-- <div class="banner-image">
                <img src="./assets/images/Home-page-hero-image.png" alt="">
            </div> -->

            <div class="banner-image">
                <!-- <img src="./assets/images/hero-image.png" alt=""> -->
                <video playsinline="" muted="" autoplay="" preload="auto" loop="" poster="./assets/images/hero-image.png">
                    <source src="./assets/images/Website_Video_Webm.webm" type="video/webm">
                </video>
            </div>


            <div class="banner-content b-p">
                <h1>Tell your</h1>
                <h1>Brand Story Better</h1>
                <p>Go where your customers are, <span><img src="./assets/images/online.svg" alt=""></span></p>
                @if(auth()->guard('business')->user())
                @else
                <a href="{{ config('app.business_subdomain_signup_url') }}" class="red-btn m-0 b-shadow">Get Started</a>
                @endif
            </div>
        </section>
    </div>


    <!--Main Section-->
    <main class="brands-page">

        <div class="performance-wrapper main-p">
            <div class="row">
                <div class="col-lg-4">
                    <div class="content-box">
                        <div class="box-image">
                            <img src="./assets/images/home-page/find-the-right-influencers.svg" alt="">
                        </div>
                        <h2>Find the right influencers</h2>
                        <p class="line-1">The <span>largest database</span> of influencers to help you activate the right ones for your campaign</p>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="content-box">
                        <div class="box-image">
                            <img src="./assets/images/home-page/execute-campaigns-easily.png" alt="" style="width: 300px;height: 350px;margin-top: -30px;">
                        </div>
                        <h2>Execute campaigns easily </h2>
                        <p class="line-2">Run campaigns on a <span>streamlined platform</span> from start to finish</p>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="content-box">
                        <div class="box-image">
                            <img src="./assets/images/home-page/keep-an-eye-on-performance.svg" alt="">
                        </div>
                        <h2>Keep an eye on performance</h2>
                        <p class="line-3">Measure the performance of your campaigns without <span>vanity metrics</span></p>
                    </div>
                </div>
            </div>
        </div>


        <div class="main-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-heading">
                            <h1>Every step of your influencer marketing campaign</h1>
                            <p>Twiva streamlines your collaboration with influencers from start to finish</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-tab-content content-p">
                <nav class="navbar navbar-expand-sm">
                    <!-- Tab Menu -->
                    <ul class="navbar-nav nav">
                        <li class="nav-item">
                            <a href="#first-item" class="nav-link active">Influencers Activation</a>
                        </li>
                        <li class="nav-item">
                            <a href="#second-item" class="nav-link">Influencer Campaign Management</a>
                        </li>
                        <li class="nav-item">
                            <a href="#third-item" class="nav-link">Social Listening and Reporting</a>
                        </li>
                        <li class="nav-item">
                            <a href="#fourth-item" class="nav-link" >Trending Campaigns on Social Media</a>
                        </li>
                        <li class="nav-item">
                            <a href="#fifth-item" class="nav-link" >Social Commerce</a>
                        </li>
                        <li class="nav-item">
                            <a href="#sixth-item" class="nav-link" >Online Market Days</a>
                        </li>
                        <li class="nav-item">
                            <a href="#seventh-item" class="nav-link" >The social ads that work</a>
                        </li>
                    </ul>
                </nav>

                <!-- Tab panes -->
                <div class="tab-section-container container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items" id="first-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/influencers-activation.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">Influencers Activation</h2>
                                    <p> Post your campaign brief, influencers submit their ideas and select the best match for  
                                        your brand & campaign objectives. Twiva recommends the right influencers that will reach your target audience.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items right-align" id="second-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/influencer-campaign-management.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">Influencer Campaign Management</h2>
                                    <p> Twiva manages your campaign end-to-end and closely monitors performance to make sure you
                                        have a return on your investment.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items" id="third-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/social-listening-and-reporting.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">Social Listening and Reporting</h2>
                                    <p> Cut through the noise of social media and know what people are saying about your products and services.
                                        It might just be the insight you need to increase your sales.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items right-align" id="fourth-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/trending-campaigns-on-social-media.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">Trending Campaigns on Social Media</h2>
                                    <p> Imagine a synchronized team of influencers posting and resharing your brand and campaign
                                        messages on social media resulting in a trend of the day! Let us put your brand on the map.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items" id="fifth-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/social-ecommerce.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">Social eCommerce</h2>
                                    <p> Create an account and list of your products and services that influencers
                                        will sell through their social media platforms.Leave the rest to us.
                                    </p>
                                    {{-- <a href="" class="red-btn">Get Started</a> --}}
                                    @if(auth()->guard('business')->user())
                                    @else
                                    <a href="{{ config('app.business_subdomain_signup_url') }}" class="red-btn">Get Started</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items right-align" id="sixth-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/online-market-days.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">Online Market Days</h2>
                                    <p> Take advantage of our online market days and reach more consumers as influencers
                                        post about your products and services on their social media platforms through stories.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="tab-items" id="seventh-item">
                                <div class="img-wrapper">
                                    <img src="./assets/images/home-page/social-ads.png" alt="">
                                </div>
                                <div class="text-wrapper">
                                    <h2><img src="./assets/images/red-dot.svg" alt="">The social ads that work</h2>
                                    <p> Work with our influencers to place paid social ads using their social media accounts.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>


        <div class="footer-banner">
            <div class="banner-container">
                <div class="banner-img">
                    <img src="./assets/images/home-page/footer-banner-img.svg" alt="">
                </div>

                <div class="banner-text">
                    <h1>Get the app on your phone</h1>
                    <p><img src="./assets/images/icons/white-dot.svg" alt=""> Guaranteed increased sales</p>
                    <p><img src="./assets/images/icons/white-dot.svg" alt=""> Easy to set up</p>
                    <div class="store-btn">
                        <a href="https://play.google.com/store/apps/details?id=com.twiva.ke" class="play-store" target="blank">
                            <img src="./assets/images/icons/play-store.svg" alt="">
                            <span>Play Store</span>
                        </a>
                        {{--<a href="https://apps.apple.com/us/app/twiva/id1492915204?ls=1" class="apple-store" target="blank">
                            <img src="./assets/images/icons/apple-icon.svg" alt="">
                            <span>Apple Store</span>
                        </a>--}}
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection
