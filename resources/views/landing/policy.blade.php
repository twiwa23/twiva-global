@extends('layouts.app')
@section('title')
    Policy
@endsection
@section('content')
<!--Banner Section-->
<section class="faq-banner about-us-banner">
    <div class="banner-content b-p">
        <div class="faq-text">
            <h1>Privacy Policy</h1>
        </div>
    </div>
</section>
</div>


<!--Main Section-->
<main class="privacy-page">

<div class="main-content-wrapper">
    <p>
        Twiva Influence Inc (“Twiva”) is the producer and owner of Twiva (collectively referred to here as the/our “Platform”, “App” or “Site”), which is a web and mobile-based platform that facilitates collaborations between Brands and Influencers.
    </p>

    <P>
        In this Twiva Privacy Policy (“Policy”), “we”, “us”, “its” and “our” refer to Twiva Influence Inc. (“Twiva”), and “you” and “your” refer to you. Before using any of our services, products, features, functionality, and Content made available through https://Twiva.co.ke and any web pages that are a part of https://Twiva.co.ke (the “Site”), please carefully read this Policy relating to your use of our platform.
    </P>

    <p>
        In this Twiva Privacy Policy (“Policy”), “we”, “us”, “its” and “our” refer to Twiva Influence Inc. (“Twiva”), and “you” and “your” refer to you. Before using any of our services, products, features, functionality, and Content made available through https://Twiva.co.ke and any web pages that are a part of https://Twiva.co.ke (the “Site”), please carefully read this Policy relating to your use of our platform.
    </p>

    <p>
        We, Twiva influence Inc, are committed to safeguarding personal privacy. We recognize that you have a right to control how your personal information is collected and used. Trust is one of our pillars and part of our culture. Your providing personal information is an act of trust and we take it seriously. Our aim is to respect and protect all your personal data in accordance with the applicable current data protection regulations. Unless given consent to do otherwise, we will only collect and use personal information as set out below.
    </p>

    <h3>What information do we collect?</h3>
    <p>You need to create an account to access our services. When registering on our platform or using our services, as appropriate, you may be asked to enter: name, gender, date of birth, e-mail address, social media handles, and mailing address, and phone number to help with your experience.</p>

    <p>We also collect information about you when you use our Services, including browsing our website and taking certain actions within the Services. These may include; computer sign-on data, time and date data, statistics on page views, your IP address(es), your GPS location, the type of computing environment you use, and other standard web log information and information gathered from cookies, beacons and other mechanisms.</p>

    <h3>When do we collect information?</h3>
    <p>First, you retain full ownership of, and absolute liability for, your channel and the content you curate. You retain complete creative direction and control over your channels and your contents.</p>

    <p>Any of the information we collect from you may be used in one of the following ways:</p>

    <p>1. To categorize your social followers base and assign personalized and focused influencing campaigns to you.</p>
    <p>Your personal information will be kept on our platform and will not be disclosed to any third party.</p>

    <p>2. To improve our platform to serve you better.</p>
    <p>We continually strive to improve our platform offerings based on the information and feedback we receive from you. We may use this information to perform research and analysis aimed at improving our products, services, and technologies.</p>

    <p>3. To process transactions</p>
    <p>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent. We only use this information to process transactions with you.</p>

    <p>4. To send periodic emails</p>
    <p>The email address you provide may be used to send you information, respond to inquiries, and/or other requests or questions.
    </p>

    <p>5. To provide customer service</p>
    <p>The contact information you provide us is to facilitate our communications with you; Send information, respond to inquiries, and/or other requests or questions.</p>

    <h3>How do we protect your information?</h3>
    <p>We follow generally accepted industry standards to help protect your personal information. We implement a variety of security measures to maintain the safety of your personal information when you become an influencer or Brand with us.</p>

    <p>All supplied sensitive information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into only to be accessible by those authorized with special access rights to such systems and are required to keep the information confidential.
    </p>

    <h3>For how long we retain your personal data?</h3>
    <p>We retain your personal data for a period strictly necessary for the use and the purposes described above.</p>
    <p>Particularly, each data shall be retained for as long as the contract for the provision of the services you have requested is not terminated. At the end of such contract, Twiva shall cancel any data collected from you. All the personal data connected to the use of our services, as the participation to Campaigns, Gigs, Posts, contents created and published by you for such activities, shall be retained if required by the applicable regulations, laws and regulatory reporting purposes. At the end of each prescribed period, we shall cancel any personal data which is no longer necessary to retain.</p>

    <h3>Do we use cookies?</h3>
    <p>Yes, Cookies are small text files that are stored by the browser (for example, Chrome or Safari) on your computer or mobile phone (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information to better serve you next time (for example, the selected language).</p>
    <p>Our site, like many others, uses cookies to help us customise your experience for your future visits. Cookies save you time by eliminating the need to repeatedly enter the same information, help protect your security by checking your login details and may help prevent fraud and they remember your preferences when using the site.</p>

    <h3>Do we disclose any information to outside parties?</h3>
    <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect others or our rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

    <h3>How and when do I consent?</h3>
    <p>Proof of consent is made evident in the following events:</p>

    <h5>• Brands</h5>
    <p>When you create a Twiva Account</p>
    <p>Signing up on our platform</p>

    <h5>• Influencers</h5>
    <p>When you create a Twiva Account</p>

    <h3>• Can I withdraw my email consent?</h3>
    <p>Yes, you can withdraw your email consent for marketing related communication with us at any time. If you receive such an email, you will find an unsubscribe option at the bottom of each email.</p>

    <h3>• Changes to this privacy policy</h3>
    <p>We reserve the right to change the terms of this Policy at any time. If there are material changes to this statement or in how Twiva will use your personal information, we will notify you by prominently posting a notice of such changes on our Platforms, or by sending you an email.</p>
    <p>You may contact us with any queries you may have in respect of this Privacy Policy or your personal information by contacting us through info@twiva.co.ke</p>
</div>

</main>
@endsection
