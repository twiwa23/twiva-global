@php
    $file_name = 'layouts.app';
@endphp
@if(auth()->user())
    @if(Request::segment(3) == auth()->user()->id)
        @php $file_name = 'creator-layout.app'; @endphp
    @endif
@endif
@extends($file_name)
@section('title')
    Portfolio
@endsection

@section('content')
<?php

  $bg_image = $user->background_image ?: asset('assets/images/creators-page/creators-banner.png');  ?>
<section class="creators-banner">
    <img src="{{ $bg_image  }}" alt="">
    @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
    <div class="edit-profile">
        <a href="{{ url('user/edit-profile') }}"><img src="{{ asset('creative/assets/images/icons/edit.svg') }}" alt="">Edit Profile</a>
    </div>
    @endif
</section>

@include('layouts.notifications')


<!--Main Section-->
<main class="creators-profile-page blog-page">

    <div class="row">
        <div class="col-sm-6 col-lg-4">
            <div class="creators-profile-wrapper">
                <div class="card">
                    <div class="profile-img">
                        @if(!empty($user->profile))
                        <img src="{{ $user->profile }}">
                        @else
                            <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}">
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="card-text">
                            <h4 class="name">@if(!empty($user->name)) {{ $user->name }} @else N/A @endif</h4>
                            <p class="work-profile">@if(!empty($category->name)) {{$category->name}} @else N/A @endif</p>
                            {{-- <p class="work-place"><img src="./assets/images/icons/placeholder.svg" alt=""> New Zealand</p> --}}
                        </div>

                        <div class="social-icons">
                            <a href="">
                                <img src="{{ asset('assets/images/icons/insta-black.svg') }}" alt="">
                            </a>

                            <a href="">
                                <img src="{{ asset('assets/images/icons/circle-black.svg') }}" alt="">
                            </a>

                            <a href="">
                                <img src="{{ asset('assets/images/icons/twitter-black.svg') }}" alt="">
                            </a>

                            <a href="">
                                <img src="{{ asset('assets/images/icons/youtube-black.svg') }}" alt="">
                            </a>
                        </div>

                        <p class="description">
                            @if(!empty($user->description)) {{$user->description}} @else N/A @endif
                        </p>

                        <div class="profile-links">
                            <a href="{{url('/user/hire-me/'.$user->id)}}"><button type="button" class="red-btn m-0">Contact me</button></a>
                        </div>

                        <div class="services">
                            <h2>Services</h2>
                            <ul class="service-list">
                                @forelse($service as $services)
                                <li class="list-item">
                                    <div class="img-wrap">
                                        @if(!empty($services->profile))
                                        <img src="{{$services->profile}}" alt="" />
                                        @else
                                        <img src="{{  asset('creative/assets/images/profile-image/profile-img-3.png')  }}" alt="">
                                        @endif
                                    </div>
                                    <h1>@if(!empty($services->title)) <b>{{$services->title}}</b> @else
                                        N/A @endif </h1>
                                    <p>@if(!empty($services->text)) {{$services->text}} @else N/A @endif</p>
                                    {{-- <button type="button" class="delete-btn" data-toggle="modal" data-target="#delete-service"><img src="{{ asset('creative/assets/images/icons/delete-icon.svg') }}" alt=""></button> --}}
                                </li>
                                @empty
                                @endforelse

                                @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
                                <a href="{{ url('user/add-services') }}" class="add-service">
                                    <img src="{{ asset('creative/assets/images/icons/plus-gray.svg') }}" alt="">
                                    Add Service
                                </a>
                                @endif
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-8">
            <div class="creators-details-wrapper">
                @if($category->name == "Blogger")
                <div class="my-profile">
                    <div class="portfolio">
                        <h1 class="title">My Blog</h1>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="all">
                            <div class="row">
                                @foreach($blogger as $bloggers)
                                    <div class="blog-container">
                                        <p>@if(!empty($bloggers->description)){!!html_entity_decode(substr($bloggers->description,0,574))!!}@endif</p>
                                        <a href="{{url('/user/view-blogger-detail/'.$bloggers->id)}}"><button class="red-btn">View More</button></a>
                                        {{-- <div class="remove" data-toggle="modal" data-target="#delete-image">Delete</div> --}}
                                    </div>
                                @endforeach



                                @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
                                <a href="{{ url('/user/add-blogger') }}" class="add-image">
                                    <img src="{{ asset('creative/assets/images/icons/black-plus-icon.svg') }}" alt="">
                                    Add Blog
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>



                </div>
                @else
                <div class="my-profile">
                    <div class="portfolio">
                        <h1 class="title">My Portfolio</h1>
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#all">All</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#images">Images</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#videos">Videos</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="all">
                            <div class="row">
                                @if(!empty($portfolio))
                                @foreach ($portfolio as $p)
                                @if(!empty($p->profile))
                                <div class="image-container">
                                    @if($p->type == 'image')
                                    <img src="{{ $p->profile }}" alt="">
                                    @else
                                    <video id="video" src="{{ $p->profile }}" controls></video>
                                    @endif
                                    {{-- <div class="remove" data-toggle="modal" data-target="#delete-image">Delete</div> --}}
                                </div>
                                @endif
                                @endforeach

                                @endif
                                @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
                                <a href="{{ url('user/add-portfolio') }}" class="add-image">
                                    <img src="{{ asset('creative/assets/images/icons/black-plus-icon.svg') }}" alt="">
                                    Add Images/Videos
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="images">
                            <div class="row">
                                @if(!empty($portfolio_images))
                                @foreach ($portfolio_images as $portfolio)
                                @if(!empty($portfolio->profile))
                                <div class="image-container">
                                    <img src="{{ $portfolio->profile }}" alt="">
                                    {{-- <div class="remove">Delete</div> --}}
                                </div>
                                @endif
                                @endforeach
                                @endif

                                @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
                                <a href="{{ url('user/add-portfolio') }}" class="add-image">
                                    <img src="{{ asset('creative/assets/images/icons/black-plus-icon.svg') }}" alt="">
                                    Add Images/Videos
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="videos">
                            <div class="row">
                                @if(!empty($portfolio_videos))
                                @foreach ($portfolio_videos as $portfolio)
                                <div class="image-container">
                                    <video id="video" src="{{ $portfolio->profile }}" controls></video>
                                    {{-- <div class="remove">Delete</div> --}}
                                </div>
                                @endforeach
                                @endif

                                @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
                                <a href="{{ url('user/add-portfolio') }}" class="add-image">
                                    <img src="{{ asset('creative/assets/images/icons/black-plus-icon.svg') }}" alt="">
                                    Add Images/Videos
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>



                </div>
                @endif


                <div class="rate">
                    <h1 class="title">Rate</h1>

                    <div class="rate-row">
                        @foreach($rate as $rates)
                        <div class="product-rate">
                            <h2 class="product-name">@if(!empty($rates->title)){{$rates->title}} @else N/A @endif</h2>
                            <p class="product-price">@if(!empty($rates->rate)){{$rates->rate}} @else N/A @endif Ksh</p>
                            <?php
                                $text = $rates->text;
                                    $texts = array();
                                if($text && !empty($text)){
                                    $texts = explode("\r\n",$text);
                                }
                            ?>
                            <ul class="product-details">
                                <?php for($i = 0; $i < count($texts); $i++){
                                    if(isset($texts[$i]) && !empty($texts[$i])){
                                ?>
                                <li>
                                    <img src="{{ asset('assets/images/icons/polygon.svg') }}" alt="">
                                    <p>{{$texts[$i]}}</p>
                                </li>
                                <?php } } if(count($texts) == 0){ echo "N/A"; } ?>
                            </ul>
                            <div class="profile-links">
                                <a href="{{url('/user/hire-me/'.$user->id)}}" class="red-btn m-0">Contact me</a>
                            </div>
                        </div>
                        @endforeach

                        @if(auth()->user() && (Request::segment(3) == auth()->user()->id))
                        <a href="{{ url('user/add-rate') }}" class="add-rate-card">
                            <img src="{{ asset('creative/assets/images/icons/black-plus-icon.svg') }}" alt="">
                            Add Rate Card
                        </a>
                        @endif

                    </div>
                </div>
            </div>

        </div>

    </div>

</main>

<div id="imgModal" class="popup">

    <div class="modal-content">
        <span class="close cursor" onclick="closeModal()"><img src="{{ asset('assets/images/icons/x-circle.svg') }}"></span>

        <div class="mySlides">
            <img src="./assets/images/creators-page/rectangle-671.png">
        </div>

        <div class="mySlides">
            <img src="./assets/images/creators-page/rectangle-672.png">
        </div>

        <div class="mySlides">
            <img src="./assets/images/creators-page/rectangle-673.png">
        </div>

        <div class="mySlides">
            <img src="./assets/images/creators-page/rectangle-674.png">
        </div>

        <!-- Next/previous controls -->
        <a class="prev" onclick="plusSlides(-1)">
            <img src="./assets/images/icons/arrow-left-circle.svg" alt="">
        </a>
        <a class="next" onclick="plusSlides(1)">
            <img src="./assets/images/icons/arrow-right-circle.svg" alt="">
        </a>

    </div>
</div>


@endsection
