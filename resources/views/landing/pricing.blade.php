@extends('layouts.app')
@section('title')
    Twiva Pricing
@endsection
@section('content')
<!--Banner Section-->
<section class="pricing-banner">
    <div class="banner-content b-p">
        <div class="pricing-text">
            <h1>Choose a plan that works</h1>
            <h1>for your business</h1>
        </div>
        <div class="pricing-img">
            <img src="./assets/images/pricing-page/plain-card.svg" alt="">
        </div>
    </div>
</section>
</div>


<!--Main Section-->
<main class="pricing-page">

<div class="main-content-wrapper">

    <div class="plans">
        <h4>*10% discount for the annual plans</h4>
        <div class="plan-options">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home">Yearly</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1">Monthly</a>
                </li>
            </ul>
            <div class="select-currency">
                <label for="">Select Currency</label>
                <select name="" id="">
                    <option value="$ USD">$ USD</option>
                    <option value="INR">INR</option>
                </select>
            </div>

        </div>
    </div>

    <div class="choose-plan">
        <h1>Have the free month on the Starter and Pro plans</h1>

        <div class="tab-content">
            <div class="tab-pane active" id="home">

                <div class="row">
                    <div class="col-sm-6 col-lg-3">

                        <div class="plan-container">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/startup-life-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Starter</div>
                            <div class="plan-price">5K <h5>Ksh</h5> <span>per month</span></div>
                            <ul class="plan-desc">
                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>1 <span>Month Free Trial</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>10 <span>listed products and services</span></h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Start with your Free Trial</a>
                        </div>

                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="plan-container active">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/profile-interface-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Pro</div>
                            <div class="plan-price">8K <h5>Ksh</h5> <span>per month</span></div>
                            <ul class="plan-desc">
                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>25 <span>listed products and services</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>5 per month <span>influencer Activation</span></h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Get Started</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="plan-container">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/experts-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Premium</div>
                            <div class="plan-price">25K <h5>Ksh</h5> <span>per month</span></div>
                            <ul class="plan-desc">
                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>100 <span>Listed Products</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>5 per month <span>influencer Activation</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>1 per month <span>Campaign Management</span></h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Get Started</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="plan-container">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/team-goals-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Custom</div>
                            <div class="plan-price"></div>
                            <ul class="plan-desc">
                                <li>
                                    <h5>
                                        <span>We do custom plans for those who require additional assistance or have special business requirements.</span>
                                    </h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Contact Us</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="tab-pane" id="menu">

                <div class="row">
                    <div class="col-sm-6 col-lg-3">

                        <div class="plan-container">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/startup-life-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Starter</div>
                            <div class="plan-price">5K <h5>Ksh</h5> <span>per month</span></div>
                            <ul class="plan-desc">
                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>1 <span>Month Free Trial</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>10 <span>listed products and services</span></h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Start with your Free Trial</a>
                        </div>

                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="plan-container active">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/profile-interface-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Pro</div>
                            <div class="plan-price">8K <h5>Ksh</h5> <span>per month</span></div>
                            <ul class="plan-desc">
                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>25 <span>listed products and services</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>5 per month <span>influencer Activation</span></h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Get Started</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="plan-container">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/experts-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Premium</div>
                            <div class="plan-price">25K <h5>Ksh</h5> <span>per month</span></div>
                            <ul class="plan-desc">
                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>100 <span>Listed Products</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>5 per month <span>influencer Activation</span></h5>
                                </li>

                                <li>
                                    <img src="./assets/images/icons/Polygon.svg" alt="">
                                    <h5>1 per month <span>Campaign Management</span></h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Get Started</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="plan-container">
                            <div class="plan-img">
                                <img src="./assets/images/pricing-page/team-goals-rafiki.svg" alt="">
                            </div>
                            <div class="plan-title">Custom</div>
                            <div class="plan-price"></div>
                            <ul class="plan-desc">
                                <li>
                                    <h5>
                                        <span>We do custom plans for those who require additional assistance or have special business requirements.</span>
                                    </h5>
                                </li>
                            </ul>

                            <a href="" class="plan-btn">Contact Us</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>

</main>

@endsection
