@extends('creator-auth-layout.app')
@section('title')
    Reset Password
@endsection
@section('content')
<div class="login">

    <div class="container-fluid m-0">
        <div class="back-button">
            <a href="{{url('/user/creators')}}"><button id="desktop-btn"><img src="{{ asset('creative/assets/images/icons/chevron-left-white.svg') }}" alt="">Back</button></a>
            <a href="{{url('/user/creators')}}"><button id="mobile-btn"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Back</button></a>
        </div>
    </div>

    <div class="container" id="step">
        <div class="login-inner">

            <div class="login-left">
                <!-- <img src="../images/banner/login.png"> -->
            </div>

            <div class="login-right">
                <div class="login-section">
                    <div class="logo"><img src="{{ asset('creative/assets/images/logo-red.svg') }}"></div>
                    @include('layouts.notifications')
                    <form method="POST" id="form_validate">
                        {{csrf_field()}}
                    <div class="input-field">
                        <label>Password</label>
                        <input type="password" name="password" id="password" placeholder = "Password" >
                        <h5 id="password-error" class="error" for="password">{{$errors->first('password')}}</h5>
                    </div>

                    <div class="input-field">
                        <label>Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control" placeholder = "Confirm Password" >
                        <h5 id="confirm_password-error" class="error" for="confirm_password">{{$errors->first('confirm_password')}}</h5>
                    </div>


                    <button type="submit" class="red-bttn" id="submitbtn" >Submit</button>
                    </form>
                    <div class="no-account">
                        Don’t have an account?<a href="{{url('/user/signup')}}">Sign Up</a>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="#">Terms & Conditions</a>
    </div>

</div>
@endsection
@section('scripts')
    <script>
        $('#form_validate').validate({
      rules: {

          password: {
              required: true,
              minlength:6,
              maxlength:30
          },
          confirm_password: {
              required: true,
              minlength:6,
              equalTo: "#password",
          },

      },

        messages:{

          password:{
            required:'Please enter password.',
            minlength:'Password must be at least 6 characters.',
            maxlength:'Password maximum 30 characters.'
          },



          confirm_password:{
            required:'Please enter confirm password.',
            equalTo:'Password and confirm password should be same.',
            minlength : "Password must be at least 6 characters."
          },


        },
        });
   </script>
@endsection
