@extends('layouts.app')
@section('title')
    Terms & Conditions
@endsection
@section('content')
    <!--Banner Section-->
    <section class="faq-banner about-us-banner">
        <div class="banner-content b-p">
            <div class="faq-text">
                <h1>Terms & Conditions</h1>
            </div>
        </div>
    </section>
</div>


<!--Main Section-->
<main class="privacy-page">

    <div class="main-content-wrapper p-4">
        <!-- <h1>TWIVA PLATFORM TERMS OF SERVICE</h1> -->

        <h3>1. Introduction</h3>
        <p> 1.1. Twiva Media Group Limited hereinafter referred to as (“Twiva”) operates an e-commerce platform run through a website and mobile applications hereinafter referred to as the (“Platform”), for the sale and purchase of consumer products on the internet..
        The platform provides a gateway for businesses to sell and market their brands, products and services through social media and social media influencers. The platform provides a unique sales and marketing platform for its sellers as it links businesses with social media influencers with a large market reach to market their businesses.</p>

        <p>1.2. These general terms and conditions shall apply to buyers, sellers and marketers (influencers) on the Platform and shall govern your use of the platform and related services.</p>

        <p>1.3. By using our platform, you accept these general terms and conditions in full. If you disagree with these general terms and conditions or any part of these general terms and conditions, you must not use the platform.</p>

        <p>1.4. In the course of use of our platform you submit to:</p>
        <p class="pl-4">1.4.1. confirm that you have obtained the necessary authority to agree to these general terms and conditions;<br />
        1.4.2. bind both yourself and the person, company or other legal entity that operates that business or organizational project, to these general terms and conditions; and<br />
        1.4.3. agree that "you" in these general terms and conditions shall reference both the individual user and the relevant person, company or legal entity unless the context requires otherwise.
        </p>

        <h3>2. Registration and account</h3>
        <p>2.1. You may not register with our platform if you are under 18 years of age or the requisite age of majority in your country of residence (by using our platform or agreeing to these general terms and conditions, you warrant and represent to us that you are at least 18 years of age and or the age of majority in your country of residence/country where you are a citizen) by placing orders on the site, the user represent that they are of legal age and that the information they provide is true and accurate..
        </p>
        <p>2.2. You may register for an account with our platform by completing and submitting the registration form on our platform.</p>
        <p>2.3. You represent and warrant that all information provided in the registration form is complete and true.</p>
        <p>2.4. If you register for an account on our platform, you will be asked to provide an email address/ phone number, user ID and password and you agree to:</p>
        <p class="pl-4">2.4.1. keep your password confidential;<br>
        2.4.2. notify us in writing immediately (using our contact details provided at section 23) if you become aware of any disclosure of your password; and <br>
        2.4.3. be responsible for any activity on our platform arising out of any failure to keep your password confidential, and that you may be held liable for any losses arising out of such a failure.</p>


        <p>2.5. Your account shall be used exclusively by you and you shall not transfer your account to any third party.</p>
        <p>2.6. We may suspend or cancel your account, and/or edit your account details, at any time in our sole discretion and without notice or explanation, providing that if we cancel any products or services you have paid for but not received, and you have not breached these general terms and conditions, we will refund you in respect of the same.</p>
        <p>2.7. You may cancel your account on our platform by contacting us as provided at section 23. No subscription fees applicable shall be refundable upon willful cancelation of subscriptions by users unless as approved by Twiva.</p>

        <h3>3. Terms and conditions of sale</h3>
        <p>3.1. You acknowledge and agree that:</p>
        <p class="pl-4">3.1.1. the platform provides an online location for sellers to sell and buyers to purchase products and services;<br>
        3.1.2. we shall accept binding sales, on behalf of sellers, but Twiva is not a party to the transaction between the seller and the buyer; and <br>
        3.1.3. a contract for the sale and purchase of a product or products or a service or services will come into force between the buyer and seller, and accordingly you commit to buying or selling the relevant product or products or service or services, upon the buyer’s confirmation of purchase via the platform.</p>


        <p>3.2. Subject to these general terms and conditions, the seller’s terms of business shall govern the contract for sale and purchase between the buyer and the seller. Notwithstanding this, the following provisions will be incorporated into the contract of sale and purchase between the buyer and the seller:</p>

        <p class="pl-4">3.2.1. the price for a product will be as stated in the relevant product listing; <br>
        3.2.2. the price for the product must include all taxes and comply with applicable laws in force from time to time;<br>
        3.2.3. delivery charges, packaging charges, handling charges, administrative charges, insurance costs, other ancillary costs and charges, will only be payable by the buyer if this is expressly and clearly stated in the product listing;<br>
        3.2.4. products must be of satisfactory quality, fit and safe for any purpose specified in, and conform in all material respects to, the product listing and any other description of the products supplied or made available by the seller to the buyer; and<br>
        3.2.5. the seller warrants that the seller has good title to, and is the sole legal and beneficial owner of, the products, and that the products are not subject to any third-party rights or restrictions including in respect of third-party intellectual property rights and/or any criminal, insolvency or tax investigation or proceedings. Businesses which wish to sell and market products through influencers shall automatically be construed to have appointed the influencers as their agents and or marketing proxies. Twiva shall act as a link to the influencers and businesses shall be informed of the influencers marketing rates and terms of engagement prior to their engagement.
        </p>

        <h3>4. Returns and refunds</h3>
        <p>4.1. Returns of products by buyers and acceptance of returned products by sellers shall be managed by us in accordance with the returns page on the platform, as may be amended from time to time. Acceptance of returns shall be in our discretion, subject to compliance with applicable laws of the territory.</p>

        <p>4.2. Refunds in respect of returned products shall be managed in accordance with the refunds page on the Platform, as may be amended from time to time. Our rules on refunds shall be exercised in our discretion, subject to applicable laws of the territory. We may offer refunds, in our discretion:</p>

        <p class="pl-4">4.2.1. in respect of the product price;<br>
        4.2.2. local and/or international shipping fees (as stated on the refunds page); and<br>
        4.2.3. by way of store credits, wallet refunds, vouchers, mobile money transfer, bank transfers or such other method as we may determine from time to time.
        </p>

        <p>4.3. Returned products shall be accepted and refunds issued by Twiva, for and on behalf of the seller.</p>
        <p>4.4. Changes to our returns page or refunds page shall be effective in respect of all purchases made from the date of publication of the change on our website.</p>

        <h3>5. Payments </h3>
        <p>5.1. You must make payments due under these general terms and conditions in accordance with the Payments Information and Guidelines on the platform. </p>
        <p>5.2. We shall accept the following payment methods (Visa, Mastercard PayPal, Apple Pay and Google Pay, M-Pesa)</p>
        <p>5.3. Missed and late payment penalties shall be born by the purchaser, Twiva shall not be held responsible for missed and late payments.</p>
        <p>5.4 Refunds and returns shall be initiated once a complaint is raised by the user and conditions as to refunds and returns shall be advised by the platform customer care representatives.</p>
        <p>5.5 Payment disputes shall be resolved by Twiva customer care representatives once a complaint is raised by the user through the provided customer care contacts.</p>

        <h3>6. Rules about your content</h3>
        <p>6.1. In these general terms and conditions, "your content" means:</p>
        <p class="pl-4">6.1.1. all works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software and files) that you submit to us or our platform for storage or publication, processing by, or onward transmission; and<br>
        6.1.2. all communications on the platform including product reviews, feedback and comments.
        </p>

        <p>6.2. Your content, and the use of your content by us in accordance with these general terms and conditions, must be accurate, complete and truthful.</p>

        <p>6.3. Your content must be appropriate, civil and tasteful, and accord with generally accepted standards of etiquette and behaviour on the internet, and must not:</p>
        <p class="pl-4">6.3.1. be offensive, obscene, indecent, pornographic, lewd, suggestive or sexually explicit;<br>
        6.3.2. depict violence in an explicit, graphic or gratuitous manner; or<br>
        6.3.3. be blasphemous, in breach of racial or religious hatred or discrimination legislation;<br>
        6.3.4. be deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory;<br>
        6.3.5. cause annoyance, inconvenience or needless anxiety to any person; or<br>
        6.3.6. constitute spam.
        </p>

        <p>6.4. Your content must not be illegal or unlawful, infringe any person's legal rights, or be capable of giving rise to legal action against any person (in each case in any jurisdiction and under any applicable law). Your content must not infringe or breach:</p>
        <p class="pl-4">6.4.1. any copyright, moral right, database right, trademark right, design right, right in passing off or other intellectual property right;<br>
        6.4.2. any right of confidence, right of privacy or right under data protection legislation;<br>
        6.4.3. any contractual obligation owed to any person; or<br>
        6.4.4. any court orders.
        </p>

        <p>6.5. You must not use our platform to link to any website or web page consisting of or containing material that would, were it posted on our platform, breach the provisions of these general terms and conditions.</p>

        <p>6.6. You must not submit to our platform any material that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.</p>

        <p>6.7. The review function on the platform may be used to facilitate buyer reviews on products. You shall not use the review function or any other form of communication to provide inaccurate, inauthentic or fake reviews.</p>

        <p>6.8. You must not interfere with a transaction by: (i) contacting another user to buy or sell an item listed on the platform outside of the platform; or (ii) communicating with a user involved in an active or completed transaction to warn them away from a particular buyer, seller or item; or (iii) contacting another user with the intent to collect any payments.</p>

        <p>6.9. You acknowledge that all users of the platform are solely responsible for interactions with other users and you shall exercise caution and good judgment in your communication with users. You shall not send them personal information including credit card details.</p>

        <p>6.10. We may periodically review your content and we reserve the right to remove any content in our discretion for any reason whatsoever.</p>

        <p>6.11. If you learn of any unlawful material or activity on our platform, or any material or activity that breaches these general terms and conditions, you may inform us by contacting us as provided at section 23.</p>

        <h3>7. Our rights to use your content</h3>
        <p>7.1. You grant to us a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, store, adapt, publish, translate and distribute your content across our marketing channels and any existing or future media.</p>

        <p>7.2. You grant to us the right to sub-license the rights licensed under section 7.1.</p>

        <p>7.3. You grant to us the right to bring an action for infringement of the rights licensed under section 7.1.</p>

        <p>7.4. You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the maximum extent permitted by applicable law.</p>

        <p>7.5. Without prejudice to our other rights under these general terms and conditions, if you breach our rules on content in any way, or if we reasonably suspect that you have breached our rules on content, we may delete, unpublish or edit any or all of your content.</p>


        <h3>8. Use of website and mobile applications</h3>
        <p>8.1. In this section 8, words “platform”, “app”, “application “and "website” shall be used interchangeably to refer to Twiva websites and mobile applications.</p>

        <p>8.2. You may:</p>
        <p class="pl-4">8.2.1. view pages from our website in a web browser.<br>
        8.2.2. download pages from our website for caching in a web browser;<br>
        8.2.3. print pages from our website for your own personal and noncommercial use, providing that such printing is not systematic or excessive;<br>
        8.2.4. stream audio and video files from our website using the media player on our website; and<br>
        8.2.5. use our platform services by means of a web browser, subject to the other provisions of these general terms and conditions.
        </p>

        <p>8.3. Except as expressly permitted by section 8.2 or the other provisions of these general terms and conditions, you must not download any material from our website or save any such material to your computer.</p>

        <p>8.4. You may only use our website for your own personal and business purposes in respect of selling or purchasing products or services on the platform.</p>

        <p>8.5. Except as expressly permitted by these general terms and conditions, you must not edit or otherwise modify any material on our website.</p>

        <p>8.6. Unless you own or control the relevant rights in the material, you must not:</p>
        <p class="pl-4">8.6.1. republish material from our website (including republication on another website);<br>
        8.6.2. sell, rent or sub-license material from our website;<br>
        8.6.3. show any material from our website in public;<br>
        8.6.4. exploit material from our website for a commercial purpose; or<br>
        8.6.5. redistribute material from our website.
        </p>

        <p>8.7. Notwithstanding section 8.6, you may forward links to products on our website and redistribute our newsletter and promotional materials in print and electronic form to any person.</p>

        <p>8.8. We reserve the right to suspend or restrict access to our website, to areas of our website and/or to functionality upon our website. We may, for example, suspend access to the website during server maintenance or when we update the website. You must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on the website.</p>

        <p>8.9. You must not:</p>
        <p class="pl-4">8.9.1. use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability, accessibility, integrity or security of the website;<br>
        8.9.2. use our website in any way that is unethical, unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;<br>
        8.9.3. hack or otherwise tamper with our website;<br>
        8.9.4. probe, scan or test the vulnerability of our website without our permission;<br>
        8.9.5. circumvent any authentication or security systems or processes on or relating to our website;<br>
        8.9.6. use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;<br>
        8.9.7. impose an unreasonably large load on our website resources (including bandwidth, storage capacity and processing capacity);<br>
        8.9.8. decrypt or decipher any communications sent by or to our website without our permission;<br>
        8.9.9. conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;<br>
        8.9.10. access or otherwise interact with our website using any robot, spider or other automated means, except for the purpose of search engine indexing;<br>
        8.9.12. violate the directives set out in the robots.txt file for our website;<br>
        8.9.13. use data collected from our website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing); or<br>
        8.9.14. do anything that interferes with the normal use of our website.
        </p>

        <h3>9. Copyright and trademarks</h3>
        <p>9.1. Subject to the express provisions of these general terms and conditions:</p>
        <p class="pl-4">9.1.1. we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and<br>
        9.1.2. all the copyright and other intellectual property rights in our website and the material on our website are reserved.
        </p>

        <p>9.2. Twiva’s logos and our other registered and unregistered trademarks are trademarks belonging to us; we give no permission for the use of these trademarks, and such use may constitute an infringement of our rights.</p>

        <p>9.3. The third party registered and unregistered trademarks or service marks on our website are the property of their respective owners and we do not endorse and are not affiliated with any of the holders of any such rights and as such we cannot grant any license to exercise such rights.</p>


        <h3>10. Data privacy</h3>
        <p>10.1. Buyers agree to processing of their personal data in accordance with the terms of Twiva’s Privacy and Cookie Notice.</p>

        <p>10.2. Twiva shall process all personal data obtained through the platform and related services in accordance with the terms of our Privacy and Cookie Notice and Privacy Policy.</p>

        <p>10.3. Sellers shall be directly responsible to buyers for any misuse of their personal data and Twiva shall bear no liability to buyers in respect of any misuse by sellers of their personal data.</p>


        <h3>11. Due diligence and audit rights</h3>
        <p>11.1. We intend to operate an anti-money laundering compliance program and reserve the right to perform due diligence checks on all users of the platform.</p>
        <p>11.2. You agree to provide to us all such information, documentation and access to your business premises as we may require:</p>
        <p class="pl-4">11.2.1. in order to verify your adherence to, and performance of, your obligations under this Agreement;<br>
        11.2.2. for the purpose of disclosures pursuant to a valid order by a court or other governmental body; or<br>
        11.2.3. as otherwise required by law or applicable regulation.
        </p>

        <h3>12. Twiva’s role as a platform</h3>
        <p>12.1. You acknowledge that:</p>
        <p class="pl-4">12.1.1. we do not confirm the identity of all platform users, check their credit worthiness or bona fides, or otherwise vet them;<br>
        12.1.2. we do not check, audit or monitor all information contained in listings;<br>
        12.1.3. we are not party to any contract for the sale or purchase of products or services advertised or sold on the platform;<br>
        12.1.4. we are not involved in any transaction between a buyer and a seller in any way, save that we facilitate a platform for buyers and sellers and process payments on behalf of sellers;<br>
        12.1.5. we are not the agents for any buyer or seller, and accordingly we will not be liable to any person in relation to the offer for sale, sale or purchase of any products advertised on our platform; furthermore, we are not responsible for the enforcement of any contractual obligations arising out of a contract for the sale or purchase of any products and we will have no obligation to mediate between the parties to any such contract.
        </p>

        <p>12.2. We do not warrant or represent:</p>
        <p class="pl-4">12.2.1. the completeness or accuracy of the information published on our platform;<br>
        12.2.2. that the material on the platform is up to date;<br>
        12.2.3. that the platform will operate without fault; or<br>
        12.2.4. that the platform or any service on the platform will remain available.
        </p>

        <p>12.3. We reserve the right to discontinue or alter any or all of our platform services, and to stop publishing our platform, at any time in our sole discretion without notice or explanation; and you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any platform services, or if we stop publishing the platform.</p>

        <p>12.4. We do not guarantee any commercial results concerning the use of the platform.</p>

        <p>12.5. To the maximum extent permitted by applicable law and subject to section 13.1 below, we exclude all representations and warranties relating to the subject matter of these general terms and conditions, our platform and the use of our platform.</p>

        <h3>13. Limitations and exclusions of liability</h3>
        <p>13.1. Nothing in these general terms and conditions will:</p>
        <p class="pl-4">13.1.1. limit any liabilities in any way that is not permitted under applicable law; or<br>
        13.1.2. exclude any liabilities or statutory rights that may not be excluded under applicable law.
        </p>

        <p>13.2. The limitations and exclusions of liability set out in this section 13 and elsewhere in these general terms and conditions:</p>
        <p class="pl-4">13.2.1. are subject to section 13.1; and<br>
        13.2.2. govern all liabilities arising under these general terms and conditions or relating to the subject matter of these general terms and conditions, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in these general terms and conditions.
        </p>


        <p>13.3. In respect of the services offered to you free of charge we will not be liable to you for any loss or damage of any nature whatsoever.</p>
        <p>13.4. Our aggregate liability to you in respect of any contract to provide services to you under these general terms and conditions shall not exceed the total amount paid and payable to us under the contract. Each separate transaction on the platform shall constitute a separate contract for the purpose of this section 13.4.</p>
        <p>13.5. Notwithstanding section 13.4 above, we will not be liable to you for any loss or damage of any nature, including in respect of:</p>
        <p class="pl-4">13.5.1. any losses occasioned by any interruption or dysfunction to the website;<br>
        13.5.2. any losses arising out of any event or events beyond our reasonable control;<br>
        13.5.3. any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill;<br>
        13.5.4. any loss or corruption of any data, database or software; or<br>
        13.5.5. any special, indirect or consequential loss or damage.
        </p>

        <p>13.6. We accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the platform or these general terms and conditions (this will not limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).</p>

        <p>13.7. Our platform includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations. We have no control over third party websites and their contents, and we accept no responsibility for them or for any loss or damage that may arise from your use of them.</p>


        <h3>14. Indemnification</h3>
        <p>14.1. You hereby indemnify us, and undertake to keep us indemnified, against:</p>
        <p class="pl-4">14.1.1. any and all losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by us to any third party in settlement of a claim or dispute) incurred or suffered by us and arising directly or indirectly out of your use of our platform or any breach by you of any provision of these general terms and conditions or the codes, policies or guidelines; and<br>
        14.1.2. Any VAT liability or other tax liability that we may incur in relation to any sale, supply or purchase made through our platform, where that liability arises out of your failure to pay, withhold, declare or register to pay any VAT or other tax properly due in any jurisdiction.</p>


        <h3>15. Breaches of these general terms and conditions</h3>
        <p>15.1. If we permit the registration of an account on our platform it will remain open indefinitely, subject to these general terms and conditions.</p>
        <p>15.2. If you breach these general terms and conditions, or if we reasonably suspect that you have breached these general terms and conditions or any Twiva codes, policies or guidelines in any way we may:</p>
        <p class="pl-4">15.2.1. temporarily suspend your access to our platform;<br>
        15.2.2. permanently prohibit you from accessing our platform;<br>
        15.2.3. block computers using your IP address from accessing our platform;<br>
        15.2.4. contact any or all of your internet service providers and request that they block your access to our platform;<br>
        15.2.5. suspend or delete your account on our platform; and/or<br>
        15.2.6. commence legal action against you, whether for breach of contract or otherwise.
        </p>

        <p>15.3. Where we suspend, prohibit or block your access to our platform or a part of our platform you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).</p>


        <h3>16. Entire agreement</h3>
        <p>16.1. These general terms and conditions and the Twiva codes, policies and guidelines shall constitute the entire agreement between you and us in relation to your use of our platform and shall supersede all previous agreements between you and us in relation to your use of our platform.</p>


        <h3>17. Hierarchy</h3>
        <p>17.1. Should these general terms and conditions, the seller terms and conditions, and the Twiva codes, policies and guidelines be in conflict, these terms and conditions, the seller terms and conditions and the Twiva codes, policies and guidelines shall prevail in the order here stated.</p>

        <h3>18. Variation</h3>
        <p>18.1. We may revise these general terms and conditions, the seller terms and conditions, and the Twiva codes, policies and guidelines from time to time.</p>
        <p>18.2. The revised general terms and conditions shall apply from the date of publication on the platform.</p>

        <h3>19. Severability</h3>
        <p>19.1. If a provision of these general terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.</p>
        <p>19.2. If any unlawful and/or unenforceable provision of these general terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.</p>

        <h3>20. Assignment</h3>
        <p>20.1. You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these general terms and conditions.</p>
        <p>20.2. You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these general terms and conditions.</p>

        <h3>21. Third party rights</h3>
        <p>21.1. A contract under these general terms and conditions is for our benefit and your benefit and is not intended to benefit or be enforceable by any third party.</p>

        <p>21.2. The exercise of the parties' rights under a contract under these general terms and conditions is not subject to the consent of any third party.</p>

        <h3>22. Law and jurisdiction</h3>
        <p>22.1. These general terms and conditions shall be governed by and construed in accordance with the laws of the territory.</p>
        <p>22.2. Any disputes relating to these general terms and conditions shall be subject to the exclusive jurisdiction of the courts of the territory.</p>


        <h3>23. Our company details </h3>
        <p>Email: info@twiva.co.ke  <br>
        Phone: +254708088114</p>


    </div>

</main>

@endsection
