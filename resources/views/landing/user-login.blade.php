@extends('creator-auth-layout.app')
@section('title')
    Login
@endsection
@section('content')
<div class="login">

    <div class="container-fluid m-0">
        <div class="back-button">
            <a href="{{url('/user/creators')}}"><button id="desktop-btn"><img src="{{ asset('creative/assets/images/icons/chevron-left-white.svg') }}" alt="">Back</button></a>
            <a href="{{url('/user/creators')}}"><button id="mobile-btn"><img src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Back</button></a>
        </div>
    </div>

    <div class="container" id="step">
        <div class="login-inner">

            <div class="login-left">
                <!-- <img src="../images/banner/login.png"> -->
            </div>

            <div class="login-right">
                <div class="login-section">
                    <div class="logo"><img src="{{ asset('creative/assets/images/logo-red.svg') }}"></div>
                    @include('layouts.notifications')
                    <form method="POST" id="form_validate">
                        {{csrf_field()}}
                    <div class="input-field">
                        <label>E-mail Address</label>
                        <input type="text" id = "email" name="email" value = "{{old('email')}}" placeholder="Email">
                        @if($errors->has('email'))
                            <div class="error">{{ $errors->first('email') }}</div>
                        @endif
                        <h5 id="usercheck" class="empty-field-error"></h5>
                    </div>

                    <div class="input-field">
                        <label>Password</label>
                        <input name="password" id="password" type="password" placeholder="********" value="{{old('password')}}">
                        @if($errors->has('password'))
                            <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                        <h5 id="passcheck" class="empty-field-error"></h5>
                    </div>

                    <div class="forgot-pass"><a href="{{url('/user/forgot-password')}}">Forgot Password?</a></div>

                    <button type="submit" class="red-bttn" id="submitbtn" >Login</button>

                    <div class="no-account">
                        Don’t have an account?<a href="{{url('/user/signup')}}">Sign Up</a>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="#">Terms & Conditions</a>
    </div>

</div>
@endsection
