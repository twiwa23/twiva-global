@extends('creator-auth-layout.app')
@section('title')
Creator SignUp
@endsection
@section('styles')
<style>
    .error {
        color: red;
    }
</style>
@endsection
@section('content')
<div class="login">

    <div class="container-fluid m-0">
        <div class="back-button">
            <a href="{{url('/user/creators')}}"><button id="desktop-btn"><img
                        src="{{ asset('creative/assets/images/icons/chevron-left-white.svg') }}"
                        alt="">Back</button></a>
            <a href="{{url('/user/creators')}}"><button id="mobile-btn"><img
                        src="{{ asset('creative/assets/images/icons/chevron-left.svg') }}" alt="">Back</button></a>
        </div>
    </div>

    <div class="signup-container" id="step">
        <div class="login-inner">

            <div class="login-right">
                <div class="login-section">
                    <div class="logo"><img src="{{ asset('creative/assets/images/logo-red.svg') }}"></div>
                    <form method="POST" id="form_validate" enctype="multipart/form-data" >
                        {{csrf_field()}}
                    @include('layouts.notifications')
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field">
                                <label>E-mail Address</label>
                                <input type="text" id="email" placeholder="Email" name="email" value="{{old('email')}}">
                                <h5 id="email-error" class="error" for="email">{{$errors->first('email')}}</h5>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field">
                                <label>Username</label>
                                <input type="text" name="name" id="name" value="{{old('name')}}" placeholder="Username">
                                <h5 id="name-error" class="error" for="name">{{$errors->first('name')}}</h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field">
                                <label>Password</label>
                                <input id="password" name="password" type="password" placeholder="********">
                                <h5 id="password-error" class="error" for="password">{{$errors->first('password')}}</h5>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field">
                                <label>Confirm Password</label>
                                <input id="confirm-password" name="confirm_password" type="password"
                                    placeholder="********">
                                <h5 id="confirm_password-error" class="error" for="confirm_password">
                                    {{$errors->first('confirm_password')}}</h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="input-group input-field">
                                <label>Contact Number</label>
                                <div class="contact">
                                <div class="input-group-prepend">
                                    <select name="phone_code" id="">
                                        @foreach ($countries as $country)
                                        <option data-countryCode="{{ $country->country_code }}" value="{{  $country->phone_code }}">{{ $country->country_name }} (+{{  $country->phone_code }})</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input name="phone_number" id="phone" onkeypress="return isNumberKey(event)"
                                    value="{{old('phone_number')}}" type="number" placeholder="Contact Number">
                                </div>
                                <h5 id="phone-error" class="error" for="phone">{{$errors->first('phone_number')}}</h5>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field select-input">
                                <label>Select Category</label>
                                <select name="category_name" id="category_name">
                                    <option value=""> Select Category</option>
                                    @foreach($category as $value)
                                    <option value="{{$value->id}}" @if(old('category_name')==$value->id) selected @endif
                                        >{{$value->name}}</option>
                                    @endforeach
                                </select>
                                <h5 id="category_name-error" class="error" for="category_name">
                                    {{$errors->first('category_name')}}</h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field">
                                <label>Description</label>
                                <input type="text" id="description" name="description" placeholder="Description"
                                    maxlength="1000" placeholder="Description" value="{{old('description')}}">
                                <h5 id="description-error" class="error" for="description">
                                    {{$errors->first('description')}}</h5>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field select-input">
                                <label>How did you discover Twiva</label>
                                <select id="discover_twiva" name="discover_twiva">
                                    <option value="">How did you discover Twiva?</option>
                                    <option value="Search engine(Google, etc)">Search engine(Google, etc)</option>
                                    <option value="Social media">Social media</option>
                                    <option value="Referral">Referral</option>
                                    <option value="KEPSA">KEPSA</option>
                                    <option value="Other">Other</option>
                                </select>
                                <h5 id="discover_twiva-error" class="error" for="discover_twiva">
                                    {{$errors->first('discover_twiva')}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row Orther_field">
                        <div class="col-sm-12 col-lg-6">
                            <div class="input-field">
                                <label></label>
                                <input type="text" name="other" value="{{old('other')}}" placeholder="Other">
                                <h5 id="other-error" class="error" for="other">{{$errors->first('other')}}</h5>
                            </div>
                        </div>
                    </div>



                    <div class="btn-wrapper">
                        <button type="submit" class="red-bttn" id="submitbtn">Sign Up</button>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="#">Terms & Conditions</a>
    </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(".Orther_field").hide()
    $("#discover_twiva").change(function() {
      let val = $("#discover_twiva option:selected" ).val();
      if(val == 'Other') {
        $(".Orther_field").show()
      } else {
        $(".Orther_field").hide()
      }

    })

    function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
</script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $("#email").on("keypress", function() {
        $("#email-error").text("").hide();
    })
    $("#name").on("keypress", function() {
        $("#name-error").text("").hide();
    })
    $("#email").on("focusout",function() {
        let val = $(this).val();

            //alert(val);
        var data = {
            '_token': "{{csrf_token()}}",
            'email': val,
        };


        $.ajax({
            url:"{{url('user/check-exist-email')}}",
            type:'POST',
            data:data,
            success: function(res){
            console.log(res)
            if(res == 1){
                $("#email-error").text("Account already exists, please Sign In.").show();
            }else{
                $("#email-error").text("").hide();
            }
            },
            error: function(data, textStatus, xhr) {
            if(data.status == 422){
                var result = data.responseJSON;
                alert('Something went worng.');
                window.location.href = "";
                return false;
            }
            }
        });

    });

    $("#name").on("focusout",function(){
        let val = $(this).val();

            //alert(val);
        var data = {
            '_token': "{{csrf_token()}}",
            'name': val,
        };


        $.ajax({
            url:"{{url('user/check-exist-name')}}",
            type:'POST',
            data:data,
            success: function(res){
            console.log(res)
            if(res == 1){
                $("#name-error").text("Username is already exists.").show();
            }else{
                $("#name-error").text("").hide();
            }
            },
            error: function(data, textStatus, xhr) {
            if(data.status == 422){
                var result = data.responseJSON;
                alert('Something went worng.');
                window.location.href = "";
                // return false;
            }
            }
        });

    });

    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "Space not allowed");

    $('#form_validate').validate({
        rules: {
            email: {
                required: true,
                email:true,
                maxlength:200
            },

            password: {
                required: true,
                minlength:6,
                maxlength:30
            },
            confirm_password: {
                required: true,
                minlength:6,
                equalTo: "#password",
            },
            name: {
                required: true,
                maxlength:50,
                minlength:3,
                noSpace : true
            },
            phone_number: {
                required: true,
                minlength:8,
                maxlength:15,

            },
            description: {
                required: true,
                minlength:10,
                maxlength:1000
            },
            category_name: {
                required: true,
            },

            // discover_twiva: {
            //     required: true,
            // },

            // other: {
            //     required: true,
            // },
        },

        messages:{
            name:{
                required:'Please enter username.',
                minlength:'Username must be at least 3 characters.',
                maxlength:'Username maximum 15 characters.'
            },

            email:{
                required:'Please enter email address.',
                email:'Please enter a valid email address.',
                maxlength:'Email address maximum 200 characters.'
            },
            password:{
                required:'Please enter password.',
                minlength:'Password must be at least 6 characters.',
                maxlength:'Password maximum 10 characters.'
            },

            description:{
                required:'Please enter description.',
                minlength:'Description must be at least 10 characters.',
                maxlength:'Description maximum 1000 characters.'
            },

            confirm_password:{
                required:'Please enter confirm password.',
                equalTo:'Password and confirm password should be same.',
                minlength : "Password must be at least 6 characters."
            },

            phone_number:{
                required:'Please enter phone number.',
                minlength:'Phone number must be at least 8 digits.',
                maxlength:'Phone number maximum 15 digits.',
            },
            category_name:{
                required:'Please select category name.',

            },
            discover_twiva:{
                required:'Please select how did you discover twiva.',

            },
            other:{
                required:'Please enter other.',
            },
        },
        submitHandler:function(form){

            if($("#email-error").text().length > 0){
            return false;
            }else {
            form.submit();
            }

            if($("#name-error").text().length > 0){
            return false;
            }else {
            form.submit();
            }
        }
        /*errorHandler:function(e){
          console.log(e)
        }*/
    });
</script>
@endsection
