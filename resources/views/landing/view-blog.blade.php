@extends('layouts.app')
@section('title')
@if(!empty($blog->title)){{$blog->title}} @else Blog @endif
@endsection
@section('content')
<!--Banner Section-->
<section class="blog-details-banner">
    @if($blog->image)
    <img src="{{url($blog->image)}}" alt="blog"/>
    @else
    <img src="{{ asset('assets/images/blog-page/blog-details.png') }}" alt="" style="">
    @endif
    {{-- <img src="{{ asset('assets/images/blog-page/blog-details.png') }}" alt=""> --}}
</section>
</div>


<!--Main Section-->
<main class="blog-detail-wrapper">

<div class="main-content-wrapper">
    <div class="row">
        <div class="col-sm-6 col-lg-9">
            <div class="blog-details">
                <h1>@if(!empty($blog->title)){{$blog->title}} @else N/A @endif</h1>
                <p>{!!html_entity_decode($blog->text)!!}
                </p>
                </div>
        </div>


        {{-- <div class="col-sm-6 col-lg-3">
            <div class="blog-sidebar">
                <h4>More Posts</h4>
                <ul class="sidebar-lists">
                    <li class="list-item">
                        <h2>BRAND SAFETY IN DIGITAL MARKETING</h2>
                        <div class="img-wrap">
                            <img src="./assets/images/blog-page/blog-img.png" alt="">
                        </div>
                    </li>

                    <li class="list-item">
                        <h2>Basics Of An Effective #Hashtag</h2>
                        <div class="img-wrap">
                            <img src="./assets/images/blog-page/blog-img.png" alt="">
                        </div>
                    </li>

                    <li class="list-item">
                        <h2>BRAND SAFETY IN DIGITAL MARKETING</h2>
                        <div class="img-wrap">
                            <img src="./assets/images/blog-page/blog-img.png" alt="">
                        </div>
                    </li>

                    <li class="list-item">
                        <h2>BRAND SAFETY IN DIGITAL MARKETING</h2>
                        <div class="img-wrap">
                            <img src="./assets/images/blog-page/blog-img.png" alt="">
                        </div>
                    </li>
                </ul>
            </div>


        </div> --}}
    </div>
</div>

</main>

@endsection
