@extends('creator-layout.app')
@section('title')
    Blog Detail
@endsection
@section('content')
<!--Banner Section-->
<?php

  $bg_image = $user->background_image ?: asset('assets/images/creators-page/creators-banner.png');  ?>
<section class="creators-banner">
    <img src="{{ $bg_image  }}" alt="">
    <div class="edit-profile">
        <a href="{{ url('user/edit-profile') }}"><img src="{{ asset('creative/assets/images/icons/edit.svg') }}" alt="">Edit Profile</a>
    </div>
</section>

@include('layouts.notifications')


<!--Main Section-->
<main class="creators-profile-page">

    <div class="row">
        <div class="col-sm-6 col-lg-4">
            <div class="creators-profile-wrapper">
                <div class="card">
                    <div class="profile-img">
                        @if(!empty($user->profile))
                        <img src="{{ $user->profile }}">
                        @else
                            <img src="{{ asset('business/assets/images/profile-images/default-profile.svg') }}">
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="card-text">
                            <h4 class="name">@if(!empty($user->name)) {{ $user->name }} @else N/A @endif</h4>
                            <p class="work-profile">@if(!empty($category->name)) {{$category->name}} @else N/A @endif</p>
                            {{-- <p class="work-place"><img src="./assets/images/icons/placeholder.svg" alt=""> New Zealand</p> --}}
                        </div>

                        <div class="social-icons">
                            <a href="">
                                <img src="{{ asset('assets/images/icons/insta-black.svg') }}" alt="">
                            </a>

                            <a href="">
                                <img src="{{ asset('assets/images/icons/circle-black.svg') }}" alt="">
                            </a>

                            <a href="">
                                <img src="{{ asset('assets/images/icons/twitter-black.svg') }}" alt="">
                            </a>

                            <a href="">
                                <img src="{{ asset('assets/images/icons/youtube-black.svg') }}" alt="">
                            </a>
                        </div>

                        <p class="description">
                            @if(!empty($user->description)) {{$user->description}} @else N/A @endif
                        </p>

                        <div class="profile-links">
                            <a href="{{url('/user/hire-me/'.$user->id)}}"><button type="button" class="red-btn m-0">Contact me</button></a>
                        </div>


                        {{-- <div class="services">
                            <h2>Services</h2>
                            <ul class="service-list">
                                <li class="list-item">
                                    <div class="img-wrap">
                                        <img src="./assets/images/profile-image/profile-img-3.png" alt="">
                                    </div>
                                    <h1>Lorem Ipsum</h1>
                                    <p>3 year experience in helping followers reach out to the best makeup products.</p>
                                    <button type="button" class="delete-btn" data-toggle="modal" data-target="#delete-service"><img src="./assets/images/icons/delete-icon.svg" alt=""></button>
                                </li>

                                <li class="list-item">
                                    <div class="img-wrap">
                                        <img src="./assets/images/profile-image/profile-img-3.png" alt="">
                                    </div>
                                    <h1>Lorem Ipsum</h1>
                                    <p>3 year experience in helping followers reach out to the best makeup products.</p>
                                    <button type="button" class="delete-btn"><img src="./assets/images/icons/delete-icon.svg" alt=""></button>
                                </li>

                                <a href="" class="add-service">
                                    <img src="./assets/images/icons/black-plus-icon.svg" alt="">
                                    Add Service
                                </a>
                            </ul>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-8">
            <div class="creators-details-wrapper">
                <div class="my-profile">
                    <div class="portfolio">
                        <h1 class="title">My Blog</h1>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="all">
                            <div class="row">
                                <div class="blog-details-container">
                                    <p>@if(!empty($blogger->description)){{$blogger->description}} @else N/A @endif</p>
                                </div>

                                {{-- <div class="blog-details-container">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet lectus ut cursus ut dui, ac aliquet aliquam. Arcu egestas quam porttitor gravida et lacus porta quis. Tincidunt aliquam neque ac nulla molestie. Fusce lobortis nisl facilisi a.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet lectus ut cursus ut dui, ac aliquet aliquam. Arcu egestas quam porttitor gravida et lacus porta quis. Tincidunt aliquam neque ac nulla molestie. Fusce lobortis nisl facilisi a.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet lectus ut cursus ut dui, ac aliquet aliquam. Arcu egestas quam porttitor gravida et lacus porta quis. Tincidunt aliquam neque ac nulla molestie. Fusce lobortis nisl facilisi a.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet lectus ut cursus ut dui, ac aliquet aliquam. Arcu egestas quam porttitor gravida et lacus porta quis. Tincidunt aliquam neque ac nulla molestie. Fusce lobortis nisl facilisi a.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet lectus ut cursus ut dui, ac aliquet aliquam. Arcu egestas quam porttitor gravida et lacus porta quis. Tincidunt aliquam neque ac nulla molestie. Fusce lobortis nisl facilisi a.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet lectus ut cursus ut dui, ac aliquet aliquam. Arcu egestas quam porttitor gravida et lacus porta quis. Tincidunt aliquam neque ac nulla molestie. Fusce lobortis nisl facilisi a.</p>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

</main>

@endsection
