
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Twiva</title>
<link rel="stylesheet" href="{{url('public/main-page/css/influencer-landing.css')}}" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
<style>

section.login_FORM {
    margin-top: 98px;
  }

.text-center.sub-title{
    background: -webkit-linear-gradient(#780c4f, #ad0e5a);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    margin: 0;
    font-size: 50px;
    line-height: 46px;
}


.img_video {
    max-height: 160px;
    max-width: 200px;
    height: 174px;
    width: 194px;
    padding-left: 0;
    position: relative;
    right: 7px;
}
.preview_video {
    max-height: 141px;
    max-width: 200px;
    height: 173px;
    width: 189px;
    padding-left: 0;
    position: relative;
    right: 7px;
    background-color: #000;
  }
.alert-danger {
    color:#e61511;
    background-color: #e61511;
    border-color: #e61511;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}
.text-danger {
    color: red;
}
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}
  .error {
  color: red;
}
.upload_pic {
    width: 180px;
    margin-bottom: 42px;
}
.profile_pic:before {
 
    width: 37%;
  }
  .profile_pic:after {
  
    width:38.7%
  }
  img#uploader {
    width: 178px;
    position: relative;
    right: 7px;
}
.upload_pic img {
    height: 152px;
}
</style>
<body>
  <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar res">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
        <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <li><a href="{{url('/blog')}}"> Blog</a></li>
          <?php 
        $auth_id = Auth::id();
        $id = Request::segment(3);
        ?>
      @if(!empty($auth_id))
       <li><a href="{{url('/user/edit-profile')}}">Edit Profile</a></li>
       <li class="last"><a href="{{url('user/userlogout')}}">Logout</a></li>
        @else
       <li class = "last"><a href="{{url('/blog')}}"> Blog</a></li>
       @endif
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
  <section class="login_FORM singup">
    <div class="container">
      <div class="col-sm-12">
        <div class="text-center">
        <p class="text-center sub-title">Add Portfolio </p>
      </div>
         @include('admin.layouts.notifications')
        <form method = "post"  enctype="multipart/form-data" onsubmit="return validateForm()">
                        {{csrf_field()}}
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <div class="profile_pic text-center">
                  <div class="upload_pic">
                    
                    @php($url =  url('public/admin/production/images/placeholder-img.jpg'))
                       <div class="media_preview">
                           <img  title="Click to change image/video" src='{{$url}}' id="uploader" class="choose_images"  />
                       </div>
                        <input style="display:none;" type="file" name="image_video" class="get_input_files" />
                      </div>
                       <span class="text-danger" id="invalid_file">{{$errors->first('image_video')}}</span>
              </div>
              </div>
            </div>
           <div class="col-sm-12">
          <div class="form-group text-center">
                <input type="Submit" name="ADD" value="ADD" class="btn btn-primary width-415">
          </div>
          </div>


        </form>
      </div>
    </div>
  </section>
<section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>

</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>

<!-- <script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script> -->
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>


  
<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

    
   </script>

 <script>
  var file_Types = {
                   "image" : ["jpeg","jpg","png"],
                   "video" : ["mp4","avi","3gp","flv","mov","video"]
                };

    $(document).on("click",".image_video_upload",function(){
      $("#all_error").hide().text("")
      let recursion = $(this).attr("data-recursion");
      recursion++;
      $(this).attr("data-recursion",recursion)
      $(this).parent().append(`<input type="file" name="image_video[${recursion}][]" class="upload_image" multiple accept="video/*,image/*" />`)

      $(this).parent().find("input[name='image_video["+recursion+"][]']").click();

      $(document).on("change","input[name='image_video["+recursion+"][]']",function(event){
      var _this = $(this);
      let _cls = $(this).attr("name");
      _cls = _cls.replace("image_video",'');
      _cls = _cls.replace("[",'');
      _cls = _cls.replace("]",'');
      _cls = _cls.replace("[]",'');
      let files = event.target.files;
      var has_error = 0;
      var valid_images = 0;
      var video_len_error = image_len_error = 0;
      var zero_len = 0;

      for(let i = 0; i < files.length;i++){
        let spl = files[i].name.split(".");
                let get_type = spl[spl.length-1];
                let file_len = files[i].size;
                if(file_len == 0){
                  zero_len++;
                }

                get_type = get_type.toLowerCase();
                if(file_Types.image.indexOf(get_type) == -1 && file_Types.video.indexOf(get_type) == -1){
                   has_error++;
                }else {
                  if(file_Types.image.indexOf(get_type) != -1){
                    if(file_len > 4194304){
                      image_len_error++;
                    }
                  }
                  if(file_Types.video.indexOf(get_type) != -1) {
                    if(file_len > 20971520){
                      video_len_error++;
                    }
                  }

                  let uploaded_images = $(".ext_record").attr("images");
                  uploaded_images = parseInt(uploaded_images)
                  uploaded_images = uploaded_images + 1
                  valid_images++;
                  $(".ext_record").attr("images",uploaded_images)
                }
              }

              if(has_error > 0){
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                _this.val("")
                $("#all_error").show().text("Please upload jpg, jpeg, png, mp4, 3gp, mov, avi file only")
                return false;
              }
              if(parseInt($(".ext_record").attr("images")) > 5){
                _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("Maximum 5 images/videos are allowed")
                  
                return false; 
              }

              if(zero_len > 0){
              _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("File size should be greater than zero")
                  
                return false; 
            }
            if(image_len_error > 0){
              _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("Image size should not greater than 4MB")
                  
                return false; 
            }
            if(video_len_error > 0){
              _this.val("")
                let uploaded_images_up = $(".ext_record").attr("images");
                  uploaded_images_up = parseInt(uploaded_images_up) - valid_images;
                  $(".ext_record").attr("images",uploaded_images_up)
                  $("#all_error").show().text("Video size should not greater than 20MB")
                  
                return false; 
            }


                for(let i = 0; i < files.length;i++){
                  let spl = files[i].name.split(".");
                  let get_type = spl[spl.length-1];
                  get_type = get_type.toLowerCase();

                  let file_name = files[i].name;
                  let trim_name = file_name.substr(0,((file_name.length-1) - get_type.length)).substr(0,30)+"."+get_type;
                  let total_uploads = $(".img_container").find(".cus_img_video").length;
                    if(file_Types.image.indexOf(get_type) != -1){
                        var reader = new FileReader();
                      reader.onload = function(e){
                      $(".media_preview").append(`<div class="img_video col-sm-4 text-center"><div class="preview_image_cover">
                      <i class="glyphicon glyphicon-remove remove-img" data-parent = "${_cls+"_"+i}" title="Remove image"></i>  
                      <img src="${e.target.result}" class="preview_image" title="${file_name}"><br/><div style="word-break: break-all;">${trim_name}</div></div></div>`);
                        }
                      reader.readAsDataURL(files[i]);
                  }else if(file_Types.video.indexOf(get_type) != -1){
                    $(".media_preview").append(`<div class="img_video col-sm-4 text-center"><div class="preview_image_cover">
                      <i class="glyphicon glyphicon-remove remove-img" data-parent = "${_cls+"_"+i}" title="Remove video"></i>  
                      <video controls src="${URL.createObjectURL(files[i])}" class="preview_video" title="${file_name}"></video><br/><div style="word-break: break-all;">${trim_name}</div></div><div>`);
                  }
           }
           setTimeout(function(){
            if($(".media_preview").find(".img_video").length > 1){
                $(".media_chooser").addClass("col-sm-4")
              }else {
                $(".media_chooser").removeClass("col-sm-4")
              }
           },100)
           if(parseInt($(".ext_record").attr("images")) >= 5){
              $(".image_video_upload").hide();
            }
      });
    });

//not accept images/video
    $(document).on("click",".remove-img",function(){
      let num = $(this).attr("data-parent");
      let index = num[0]
      let index1 = num[2]
      //$(".choose_container input[name='images["+index+"]["+index1+"]']").val("")
      $(".not_accept").val($(".not_accept").val()+","+num)
      $(this).closest(".img_video").remove()
      $(".ext_record").attr("images",parseInt($(".ext_record").attr("images")) - 1)
      setTimeout(function(){
        if($(".media_preview").find(".col-sm-4").length < 5){
          $(".image_video_upload").show();
        }
            if($(".media_preview").find(".img_video").length > 1){
                $(".media_chooser").addClass("col-sm-4")
              }else {
                $(".media_chooser").removeClass("col-sm-4")
              }
           },100)
    })


/* upload multiple images/video */
    $(".choose_images").click(function(){
      $(".get_input_files").click();
    })
    var file_Types = {
                   "image" : ["jpeg","jpg","png"],
                   "video" : ["mp4","avi","3gp","flv","mov","video"]
                };

    $(".get_input_files").change(function
      (event){
        $("#invalid_file").hide()
        var _this = $(this);
        let files = event.target.files;
        var has_error = 0;
        var valid_images = 0;
        var video_len_error = image_len_error = 0;
        var zero_len = 0;
        if(files.length > 10){
          _this.val("")
          alert("Maximum 10 images/videos are allowed");
          return false;
        }
      for(let i = 0; i < files.length;i++){
        let spl = files[i].name.split(".");
                let get_type = spl[spl.length-1];
                let file_len = files[i].size;

                get_type = get_type.toLowerCase();
                if(file_Types.image.indexOf(get_type) == -1 && file_Types.video.indexOf(get_type) == -1){
                   has_error++;
                }else {
                  if(file_Types.image.indexOf(get_type) != -1){
                    if(file_len > 4194304){ /* 4 mb for image */
                      image_len_error++;
                    }
                  }
                  if(file_Types.video.indexOf(get_type) != -1) {
                    if(file_len > 20971520){ /*20mb for video */
                      video_len_error++;
                    }
                  }
                }
              }

              if(has_error > 0){
                _this.val("")
                alert("Please upload jpg, jpeg, png, mp4, 3gp, mov, avi file only")
                return false;
              }
            if(image_len_error > 0){
              _this.val("")
               alert("Image size should not greater than 4MB")
                return false; 
            }
            if(video_len_error > 0){
              _this.val("")
                  alert("Video size should not greater than 20MB")
                return false; 
            }

            for(let i = 0; i < files.length;i++){
                  let spl = files[i].name.split(".");
                  let get_type = spl[spl.length-1];
                  get_type = get_type.toLowerCase();

                  let file_name = files[i].name;
                  let trim_name = file_name.substr(0,((file_name.length-1) - get_type.length)).substr(0,30)+"."+get_type;
                    if(file_Types.image.indexOf(get_type) != -1){
                        var reader = new FileReader();
                      reader.onload = function(e){
                      $(".media_preview").append(`<div class="img_video col-sm-3 text-center"><div class="preview_image_cover">
                      <img src="${e.target.result}" class="preview_image"></div></div>`);
                        }
                      reader.readAsDataURL(files[i]);
                  }else if(file_Types.video.indexOf(get_type) != -1){
                    $(".media_preview").append(`<div class="img_video col-sm-3 text-center"><div class="preview_image_cover">
                      <video controls src="${URL.createObjectURL(files[i])}" class="preview_video" ></video></div><div>`);
                  }
           }

           $(".choose_images").hide()
      })
    function validateForm(){
      $("#invalid_file").hide();
      if($("input[name='image_video']").val() == ""){
        $("#invalid_file").show().text("Please upload image/video");
        return false;
    
      }
    }
    </script>

</body>
</html>
