
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Twiva</title>
<link rel="stylesheet" href="{{url('public/landing/css/influencer-landing.css')}}" />
<link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">



</head>
<style type="text/css">
  div#communityCounter {
    display: none;
}
.one-app__section {
    margin: -300px 0 -120px;
    width: 100%;
}
.alert.alert-success.text-center {
    font-size: 20px;
    text-align: center;
    background-color: #931a71;
    color: #fff;
}


.alert.alert-success {
    text-align: center;
    background-color: #931a71;
    color: #fff;
    font-size: 18px;
}

</style>
<body id="style-3" data-lang="en_GB" class="scrollbar">
<div class="header__hamburger" id="headerHamburger">

</div>
<div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
       <!--  <li><a href="{{url('/about-us')}}"> About Us</a></li> -->
        <li><a href="{{url('/user/creators')}}">Creators</a></li>
       <li class="last"><a href="{{url('/blog')}}"> Blog</a></li>
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
<section class="video" id="new" style="background-image:url({{url('public/landing/images/Policy.png')}});
    padding: 130px 0;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
  <div class="container">
    <div class="col-md-12">
      <div class="new-file-desert">
        <!-- <img src="{{url('public/landing/images/Policy.png')}}"> -->
    </div>
</div></div></section>



<section class="content_section contact-us tiwa-works influencer contact-us">
         <div class="container">
          <div class="col-sm-10 col-sm-offset-1">
          <div class="new-faq">
          <div class="heading aos-init aos-animate" data-aos="fade-down">
      <h2>CONTACT US</h2>
  </div>
                     
        <div class="margin-top-90">
            
                  
                   <form method="post" enctype="multipart/form-data" action="{{url('send-contact-us')}}">
                   @include('admin.layouts.notifications')
                     @if(session()->has('success'))
                    <div class="alert alert-success">
                      {{session()->get('success')}}
                    </div>
                    @endif 
                       @if(session()->has('error'))
                    <div class="alert alert-danger">
                      {{session()->get('error')}}
                    </div>
                    @endif
                  {{csrf_field()}}
                <div class="row">
                <div class="col-sm-6">
            <div class="form-group">
                <label>First Name*</label>
                  <input type="text"  name = "first_name" value = "{{old('first_name')}}" class="form-control" required >
                      <span class="text-danger">{{$errors->first('first_name')}}</span>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Last Name*</label>
                <input type="text"  name = "last_name" value = "{{old('last_name')}}" class="form-control" required >
                    <span class="text-danger">{{$errors->first('last_name')}}</span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Email*</label>
                 <input type="text"  name = "email" value = "{{old('email')}}" class="form-control" required >
                     <span class="text-danger">{{$errors->first('email')}}</span>
            </div>
        </div>
         <div class="col-sm-12">
            <div class="form-group">
                <label>Subject*</label>
               <input type="text"  name = "subject" value = "{{old('subject')}}" class="form-control" required >
                       <span class="text-danger">{{$errors->first('subject')}}</span>
            </div>
        </div>
    </div>
     <div class="row">
                <div class="col-sm-12">
            <div class="form-group">
                <label>Message</label>
                <textarea class="form-control" name = "message"  required > 
                  {{old('message')}}</textarea/>
            <span class="text-danger">{{$errors->first('message')}}</span>
               
            </div>
        </div>
    </div>
     <div class="row">
         <div class="col-sm-12 text-center">
            <div class="form-group margin-top-40">
              <!--  <a href="javascript:;" class="btn btn-primary">Submit</a> -->
                <input type="submit" name = "submit" value="Submit" class="btn btn-primary">
            </div>
        </div>
        </div>
            </form>
        </div>
           </div>
         </div>
    </div>
</section>
 <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
              
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                     <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>
            </div>
                <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

     /*$(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });*/
         
   </script>
</script>



</body>
</html>
