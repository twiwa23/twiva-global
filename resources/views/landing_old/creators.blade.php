
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Creators</title>
<link rel="stylesheet" href="{{url('public/main-page/css/influencer-landing.css')}}" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Damion&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">

<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
</head>
<style>
.new-wrappwe img {
    max-width: 100%;
    height: 226px;
    width: 100%;
    object-fit: cover;
}
 img.new-photo {
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border:3px solid #8a0e49;
    object-fit: cover;
}
.pagination>li>a, .pagination>li>span {
    
    padding: 4px 16px;
    margin-left: -1px;
    text-decoration: none;
    font-size: 24px;
    font-size: 26px;
    text-align: center;
    line-height: 42px;
    color: #9ca8ae;
    border:0;
}
.creative-photo-img {
    padding: 0 0;
}
.creative-photo-img .sub-heading {
    margin-bottom: 10px;
    }

.new-creative { 
       padding: 133px 0 7px 0px;

}
.pagination>li:first-child>a, .pagination>li:first-child>span {
    margin-left: 0;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    font-size: 68px!important;
    padding: 0px 23px;


}
.pagination>li:last-child>a, .pagination>li:last-child>span {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    font-size: 68px!important;
    padding: 0px 23px;

}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    z-index: 2;
    color: #ccc;
    background-color: transparent;
    border-color: transparent;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #8a0e49;
    border-color: #8a0e49;
}

h3.text {
    margin-top: 20px;
    margin-bottom: 10px;
    font-size: 24px;
    color: #000;
}
.right_side h5 {
      color: #000;
          /*text-shadow: 3px 2px 8px #0b0b0b;*/
    font-size: 17px;
    }
    .right_side a.new-buttom {
          background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%) !important;
              margin-top: 8px;
    margin-bottom: 5px;
     padding: 11px 33px;
    }
    .first_Point {
    display: flex;
}
.right_side a.new-buttom{
  margin-right: 8px;
}

</style>
<body>
   <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar res">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
        <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <li class="last"><a href="{{url('/blog')}}"> Blog</a></li>
          <?php 
         $auth_id = Auth::id();
         $id = Request::segment(3);
      ?>
      @if($auth_id))
           <li><a href="{{url('/user/edit-profile')}}">Edit Profile</a></li>
       <li class="last"><a href="{{url('user/userlogout')}}">Logout</a></li>
      
       @endif
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
  </header>
  <div class="new-creative">
    <div class="container">
      <div class="col-md-7">

      <h2>Tell your brand <span>Story</span> better!       <img src="{{url('public/main-page/images/dots-small.png')}}" class="dots" alt="">
</h2>
<div class="row">
      <!-- <div class="col-md-9"></div> -->
        <div class="col-md-9" style="margin-top: 15px;">
          <div class="text-left right_side">
              <h5>Ready to monetize your creative?</h5>
                <div class="first_Point">
                <a href="{{url('/user/signup')}}" class="new-buttom second-button ">Sign Up</a>
               <a href="{{url('/user/login')}}" class="new-buttom first-button">Login</a>
                
              </div>
            <!--   <h5>Ready to monetize your creative?</h5>
              <a href="{{url('/user/signup')}}" class="new-buttom">Get Started</a>
               <h5>Already using Twiva? <a href="{{url('/user/login')}}" style="font-size: 16px; color: rgb(115,14,57); font-weight: bold;">Sign In</a></h5> -->
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <figure><img src="{{url('public/main-page/images/phone.png')}}" alt=""></figure>
  </div>
  <!-- <div class="row">
      <div class="col-md-9"></div>
        <div class="col-md-3">
          <div class="text-left right_side">
              <h5>Ready to grow your business?</h5>
              <a href="#download" class="new-buttom">Get Started</a>
               <h5>Already using Twiva? <a href="#" style="font-size: 14px;">Sign In</a></h5>
          </div>
        </div>
      </div> -->
</div>
</div>
   <div class="creative-photo-img">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
      <div class="sub-heading">
      <p>Creators</p>
  </div>
</div>
</div>
  <div class="row">
    <div class="container">
      <div class="col-md-3">
             <select name = "category_name" id = "category_name" class="form-control" required>
            
               <option value=""> Select Category</option>
                <option value="{{url('/user/creators')}}"> All</option>
              @foreach($category as $value)
              <option value="{{url('/user/creators').'?category_id='.$value->id}}" @if(app("request")->input('category_id') == $value->id) selected @endif >{{$value->name}}</option>
              @endforeach
            </select>
          </div>
      </div>
    
  </div>
  <script type="text/javascript">
    document.getElementById("category_name").addEventListener("change",function(){
      window.location.href = this.value;
    })
  </script>


   </div>
 <div class="new-photo-gallerys">
  <div class="container">
   
  <div class="row">
    
     @forelse($users as $user)
  <div class="col-md-4 col-sm-6">
     <a href="{{url('/user/portfolio/'.$user->user_id)}}">
    <div class="gallery-new">
      <div class="new-wrappwe">
        <?php 
         $bg_image = $user->background_image ?: url('public/main-page/images/brown-framed.jpg');  ?>
      <figure>

        
        <img src="{{url($bg_image)}}" alt=""></figure>
      @if(!empty($user->user_profile))
     @php $image =   url('public/storage/uploads/images/business/'.$user->user_profile);@endphp
                            <td><img src="{{$image}}" alt="" class="new-photo"/>
                            @else
                           <img src="{{url('public/main-page/images/creative-photo.png')}}" alt="" class="new-photo">
                           @endif</td> 
      
    </div>
    <div class="text-gallery">
      <h3 class="text">@if(!empty($user->user_name)){{$user->user_name}} @else N/A @endif</h3>
      <h2><img src="{{url('public/main-page/images/camera-creative.png')}}" alt="" class="origin">@if(!empty($user->category_name)){{$user->category_name}} @else N/A @endif</h2>
  </div>
  <div class="text-center">
  <a href="{{url('/user/portfolio/'.$user->user_id)}}" class="new-button creative">PORTFOLIO</a>
</div>
</div>
</a>
 </div>
 @empty
<div class="text-center">
 <h2 style = "font-size: 30px;">No Data Found</h2>
</div>
@endforelse
<div class = "col-md-12">
<div class="text-center">
{{ $users->appends(request()->except('page'))->links() }}
</div>
</div>
</div>

<div class="pagination">
</div>
</div>

  </div>
</div>

<section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
    document.getElementById("category_name").addEventListener("change",function(){
      window.location.href = this.value;
    })
  </script>
<!-- <script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script> -->
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>


<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

    
   </script>
</body>
</html>



  