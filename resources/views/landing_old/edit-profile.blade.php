<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Edit Profile</title>
<link rel="stylesheet" href="{{url('public/main-page/css/influencer-landing.css')}}" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Damion&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
<style>


  .error {
  color: red;
}
section.login_FORM  i.fa {
    font-size: 22px;
    color: #b1bbc1;
    position: absolute;
    top: 15px;
    bottom: unset;
    margin: auto;
    border: 0px solid;
    height: unset;
    line-height: normal;
}

section.login_FORM.singup {
      margin-top: 34px;

}

  /*.alert-danger {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}*/
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}
.New-describe-section {
    text-align: unset;
    }
    .profile_pic.text-center {
    margin-top: -164px;
}
.profile_pic:before{
  display: none;
}

.profile_pic:after{
  display: none;
}
.upload_pic {
    position: relative;
    margin: auto;
    width: 200px;
    height: 200px;
    cursor: pointer;
}
.button-find {
    right: 15%;
    bottom: 14%;
    border: 1px solid #b6bfc5;
    padding: 9px 13px;
    color: #b1b3b5;
    font-size: 15px;
    position: absolute;
    z-index: 99999;
    cursor:pointer; 

}
.button-find button {
padding: 4px 16px;
    color: #a9a8a8;
    font-size: 14px;}
    .button-find i {
    margin-right: 12px;
}
  </style>

<body>
  <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar res">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
           <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <?php 
        $auth_id = Auth::id();
        $id = Request::segment(3);
        ?>
      @if(!empty($auth_id))
       <li><a href="{{url('/user/edit-profile')}}">Edit Profile</a></li>
       <li class="last"><a href="{{url('user/userlogout')}}">Logout</a></li>
       @else
       <li class = "last"><a href="{{url('/blog')}}"> Blog</a></li>
       @endif
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
 @include('admin.layouts.notifications')
 <form method="POST" id="form_validate" enctype=multipart/form-data "> 
        {{csrf_field()}}
        <?php 
         if(!empty($user->background_image)){
          $background_image = $user->background_image;
         }else{
           $background_image = "";

         }
         if(!empty($user->profile)){
          $profile = $user->profile;
         }else{
           $profile = "";

         }
         if(!empty($user->name)){
           $name = $user->name;
         }else{
          $name = "";
         }
         if(!empty($categoryUser->category_id)){
          $category_id = $categoryUser->category_id;
         }else{
           $category_id = "N/A";
         }
         if(!empty($user->description)){
          $description= $user->description;
         }else{
           $description = "";
         }

        $bg_image = $background_image ?: url('public/main-page/images/ brown-framed-background.jpg');  ?>
   <div class="background-image" id="back-image"  style = "
    background-image: url('{{$bg_image}}');
    padding: 98px 0 144px 0;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    margin-top: 77px;
        position: relative;

"
>
<div class="button-find" id="update-bg-image">
    <i class="fa fa-camera"></i>CHANGE COVER IMAGE
  </div>
   <div class="img-goood">
    <h2>@if(!empty($name)) {{$name}} @else N/A @endif</h2>
      <img src="{{url('public/main-page/images/dots-small.png')}}" class="dots" alt="" >
  </div>
</div>
<input type="file" name="background_image" id="bg-image" style="display:none" />

  
<div class="New-describe-section new-photo ">
    
  <section class="login_FORM singup">
    <div class="container">
      <div class="col-sm-12">
        <div class="text-center">
        
      </div>
    
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <div class="profile_pic text-center">
                  <div class="upload_pic">
                  
                    @php($url =  $profile ? url($profile) : url('public/main-page/images/profile-pic.png'))
                         <img  title="Click to change image"
                           onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 190px;height: 190px;object-fit: cover;border-radius: 100%;border: 7px solid #780c4f;border-radius: 100%;" />
                        <input style="display:none;" type="file" id="imgInp" name="profile"  data-role="magic-overlay" data-target="#pictureBtn" value="{{$profile}}">
                </div>
                       
                <span style="display:none; margin :20px;color: red" class="text-danger" id="invalid_file">Please select jpg, jpeg or png image format only. </span>
                       <span class="text-danger" style="display: inherit;">{{$errors->first('profile')}}</span>

                  <div class="text-center"></div>
                </div>
              </div>
            </div>
            <div class = "row">
           <div class="col-sm-6">
          <div class="form-group">
            <div class = "name">
            <input type="text" name="name" id = "name" value = "{{$name}}" class="form-control" placeholder = "Username" > 
            <i class="fa">
              <img src="{{url('public/main-page/images/username.jpg')}}">
            </i>
          </div>     
          <label id="name-error" class="error" for="name">{{$errors->first('name')}}</label>
          </div>
          </div>
          
            <div class="col-sm-6">
           <div class="form-group">
            <div class="category_name">
             <select name = "category_name" id = "category_name" class="form-control" required>
               <option value=""> Select Category</option>
              @foreach($category as $value)
               <option value="{{ $value->id }}" {{ $category_id == $value->id ? 'selected="selected"' : '' }}>{{ $value->name }}</option>    
              @endforeach
            </select>
            <i class="fa">
              <img src="{{url('public/main-page/images/category.jpg')}}">
            </i>
            <span class="text-danger"></span>
          </div>
           <label id="category_name-error" class="error" for="category_name">{{$errors->first('category_name')}}</label>
        </div>
          </div>
        </div>
        <div class = "row">
          <div class="col-sm-12">
          <div class="form-group">
          <div class="description">
            <textarea class = "form-control" id = "description" name="description" placeholder="Description" maxlength="1000" >{{$description}}</textarea>
            <i class="fa">
              <img src="{{url('public/main-page/images/pen.jpg')}}">
            </i>
             <span class="text-danger"></span>
          </div>
          <label id="description-error" class="error" for="description">{{$errors->first('description')}}</label>
          </div>
          </div>
        </div>
           <div class="col-sm-12">
          <div class="form-group text-center">
                <input type="Submit" name="Update" value="Update" class="btn btn-primary width-415">
          </div>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
   <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke"><figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>

</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
    // document.getElementById("category_name").addEventListener("change",function(){
    //   window.location.href = this.value;
    // })
  </script>
<!-- <script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script> -->
yar ab infulancer bhi use karega isko login karega
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

         
   </script>

 <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    </script>
   
 <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
       //-->
    </SCRIPT>
     <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
     $("#name").on("focusout",function(){
                let val = $(this).val();

                  //alert(val);
                var data = {
                  '_token': "{{csrf_token()}}",
                  'name': val,
                };

              
              $.ajax({
                  url:"{{url('user/check-exist-name')}}",
                  type:'POST',
                  data:data,
                  success: function(res){
                    console.log(res)
                    if(res == 1){
                      $("#name-error").text("Username is already exists.").show();
                    }else{
                      $("#name-error").text("").hide();
                    }
                  },
                  error: function(data, textStatus, xhr) {
                    if(data.status == 422){
                      var result = data.responseJSON;
                      alert('Something went worng.');
                      window.location.href = "";
                      return false;
                    } 
                  }
                });

        });

    </script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>




  jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "Space not allowed");

   $('#form_validate').validate({ 
      rules: {
          name: {
              required: true,
              maxlength:50,
              minlength:3,
              noSpace : true
          },
       
          description: {
              required: true,
              minlength:10,
              maxlength:1000
          },
           category_name: {
              required: true,
          },
      },

        messages:{
          name:{  
            required:'Please enter username.',
            minlength:'Username must be at least 3 characters.',
            maxlength:'Username maximum 15 characters.'
          },
       
          description:{  
            required:'Please enter description.',
            minlength:'Description must be at least 10 characters.',
            maxlength:'Description maximum 1000 characters.'
          }, 
 
          category_name:{  
            required:'Please select category name.',
           
          },             
        },

        submitHandler:function(form){

                   if($("#name-error").text().length > 0){
                    return false;
                  }else {
                    form.submit();
                  }
          }
         });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#update-bg-image").click(function(){
        console.log(1)
      $("#bg-image").click();
    })
    $("#bg-image").change(function(event){
        let file = event.target.files[0];

        if (file) {
                var type = file.type;

                if (type == "image/png" || type == "image/jpeg" || type == "image/jpeg") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#back-image').css('background-image', "url('"+ e.target.result+"')");
                }
                reader.readAsDataURL(file);
              }else {
                    $('#back-image').css('background-image', "url('{{$bg_image}}')");

                alert("Please upload jpg, jpeg, png format image only.");
                return false;
              }
          }
    })
  })
</script>
</body>
</html>
