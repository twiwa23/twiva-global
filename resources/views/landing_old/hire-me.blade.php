
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Hire Me</title>
<link rel="stylesheet" href="css/influencer-landing.css" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Damion&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">

<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
<style>

 .error {
  color: red;
}

  .alert-danger {
       color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 545px;
    width: 100%;
    margin: 0 auto;
    margin-bottom: 12px;

}
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}


</style>


<body>

  <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar res">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
           <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <?php 
        $auth_id = Auth::id();
        $id = Request::segment(3);
        ?>
      @if(!empty($auth_id))
       <li><a href="{{url('/user/edit-profile')}}">Edit Profile</a></li>
       <li class="last"><a href="{{url('user/userlogout')}}">Logout</a></li>
       @else
       <li class = "last"><a href="{{url('/blog')}}"> Blog</a></li>
       @endif
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
		<!-- header end -->
	
        
		<section class="sec_banner">
			 @include('admin.layouts.notifications')
			      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
            @endif
			<div class="hire_me_bg">
				
         <!-- <form method = "post" action = "" enctype=multipart/form-data>
          {{csrf_field()}} -->
         
				<form method = "post" action  = "" >
					 {{csrf_field()}}
					<div class="hire_me_heading">
						<h3>Hire Me</h3>
					</div>
					<div class="hire_form_fields">
	                    
						<label for="">From :</label>
						<input type="text"  name = "email" value = "{{old('email')}}" class="form-control"> 
						<label for="" class="to">To :</label>
						<input type="text" name = "to" value = "{{$user->name}}" class="form-control to_input" readonly>
						<label for="" class="subject">Subject :</label>
						<input type="text" name = "subject" value = "{{old('subject')}}" class="form-control sub_input">
						<textarea name="text" value = "{{old('text')}}"  class="form-control" placeholder="Write Something here.."></textarea>
						
					</div>
					<div class="send_info d-flex justify-content-between align-items-center">
						<div>
							<button type="submit" name = "submit"  class="btn btn-success">SEND</button>
							<!-- <input type="Submit" name="submit" value="submit" class="btn btn-primary"> -->
						</div>
						<div>
						</div>
					</div>
				</form>
			</div>
		</section>
		<section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
 <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
       CKEDITOR.replace('text');
        CKEDITOR.config.autoParagraph = false;

      </script>

<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>


<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

    
   </script>
   <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

    
  
  $('#form_validate').validate({ 
      rules: {
          email: {
              required: true,
              email:true,
              maxlength:200
          },
          
          subject: {
              required: true,
              maxlength:100,
              minlength:3,
               
          },
          
          text: {
              required: true,
              minlength:10,
              maxlength:2000
          },
           
      },

        messages:{
          subject:{  
            required:'Please ensubjectter subject.',
            minlength:'subject must be at least 3 characters.',
            maxlength:'subject maximum 100 characters.'
          },
          email:{  
            required:'Please enter email address.',
            email:'Please enter a valid email address.',
            maxlength:'Email address maximum 200 characters.'
          },
         

          text:{  
            required:'Please enter tex.',
            minlength:'Text must be at least 10 characters.',
            maxlength:'Text maximum 2000 characters.'
          },

             
        },
       
        /*errorHandler:function(e){
          console.log(e)
        }*/
       
    
  });
</script>

</body>
</html>
