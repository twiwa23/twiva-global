<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
      <link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
      <title>Twiva</title>
      <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
      <link rel="stylesheet" href="{{url('public/landing/css/influencer-landing.css')}}" />
      <link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
   </head>
   <style type="text/css">
      div#communityCounter {
      display: none;
      }
      .one-app__section {
      margin: -300px 0 -120px;
      width: 100%;
      }
   </style>
   <body id="influencer-landing" data-lang="en_GB">
      <div class="header__hamburger" id="headerHamburger">
      </div>
      <div class="heading toper">
         <div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
            <div class="header__current-language" id="languageSelector">
               <nav class="navbar">
                  <div class="container">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="{{url('/')}}">Brands</a></li>
                           <li class="active"><a href="{{url('/influencer')}}">Influencers</a></li>
                            <li><a href="{{url('/user/creators')}}">Creators</a></li>
                          <li class="last"><a href="{{url('/blog')}}"> Blog</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>
      <section class="video" id="new"style="background-image:url({{url('public/landing/images/influencer.png')}});
         padding: 110px 0;
         background-size: cover;
         background-position: center;
         background-repeat: no-repeat;">
         <div class="container">
            <div class="col-md-12">
               <div class="new-file-desert">
                  <h2>Bring your creativity, we'll bring the brands! </h2>
               </div>
            </div>
         </div>
      </section>
      <!-- how it works section -->
      <section class="tiwa-works influencer again" id="">
         <div class="container">
            <div class="new-desert">
               <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-bottom new-space"  data-aos="fade-left">
                        <h4>Everything in Just One App </h4>
                        <p class="description">Silence the noise in your DM. You accept invites you are interested in, set your rates, and get paid - all on Twiva app! That easy. You can participate in campaigns on: Instagram, Facebook and Twitter!</p>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-box influencer" data-aos="fade-right">
                        <img src="{{url('public/landing/images/t1.png')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                  <div class="new-bottom"  data-aos="fade-left" >
                     <h4>Get Invited to exclusive in-person events</h4>
                     <p class="description">We will hold the door for you into invite-only events. Get free product and compensation to just attend and post about it. Don’t sell your influence short.</p>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="new-box influencer" data-aos="fade-right">
                     <img src="{{url('public/landing/images/red-carpet.png')}}">
                  </div>
               </div>
            </div>
            <div class="new-desert">
               <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-bottom"  data-aos="fade-left">
                        <h4>Automated Payment</h4>
                        <p class="description">Don't worry, we handle payments on your behalf. Once you have completed your tasks, payment is automatically and immediately released, saving valuable time and resulting in stress-free 
                           engagement with brands.
                        </p>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-box influencer" data-aos="fade-right">
                        <img src="{{url('public/landing/images/t3.png')}}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="background-green">
            <div class="row">
               <div class="container">
                  <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                     <div class="new-bottom"  data-aos="fade-left"">
                        <h4>Earn Money & Build Your Audience</h4>
                        <p class="description">The hundreds of brands on our platform looking for influencers will help you diversify your portfolio. Get discovered and partner with industry-leading lifestyle, food, beauty, and fashion brands.</p>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-box influencer" data-aos="fade-right">
                        <img src="{{url('public/landing/images/cash.png')}}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="new-desert">
            <div class="row">
               <div class="container">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-bottom"  data-aos="fade-left">
                        <h4>Discover New Brands to Partner With</h4>
                        <p class="description">The hundreds of brands on our platform looking for influencers will help you diversify your portfolio. Get discovered and partner with industry-leading lifestyle, food, beauty, and fashion brands.</p>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-box influencer" data-aos="fade-right">
                        <img src="{{url('public/landing/images/t5.png')}}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="background-blue">
            <div class="row">
               <div class="container">
                  <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                     <div class="new-bottom"  data-aos="fade-left">
                        <h4>Work Directly with Brands</h4>
                        <p class="description">We connect you directly with marketers at top brands, so you can build the relationship on your terms.</p>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                     <div class="new-box influencer" data-aos="fade-right">
                        <img src="{{url('public/landing/images/gig.png')}}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="new-desert last">
            <div class="new-format">
               <div class="container">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="new-bottom"  data-aos="fade-left">
                           <h4>Get Exclusive Products and Services for FREE</h4>
                           <p class="description">With Twiva you get product or services in exchange for your marketing services. Take advantage of being an influencer and benefit from 
                              exclusive Twiva invites.
                           </p>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="new-box influencer" data-aos="fade-right">
                           <img src="{{url('public/landing/images/men-with-gift.png')}}">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
               </br>
                  <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
      <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
         <i class="fa fa-chevron-up"></i>
      </div>
      <!-- new-home -->
      </main>
      <script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
      <script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
      <script src="{{url('public/landing/js/aos.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
      <script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
      <script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
      <script type="text/javascript">
         $(window).scroll(function() {
           if ($(this).scrollTop() > 100){  
               $('nav.navbar').addClass("sticky");
           }
           else{
               $('nav.navbar').removeClass("sticky");
           }
         });
      </script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
             $(window).scroll(function(){
                 if ($(this).scrollTop() > 50) {
                     $('#backToTop').fadeIn('slow');
                 } else {
                     $('#backToTop').fadeOut('slow');
                 }
             });
             $('#backToTop').click(function(){
                 $("html, body").animate({ scrollTop: 0 }, 500);
                 return false;
             });
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
         // Add smooth scrolling to all links
         $("#nwes").on('click', function(event) {
         
           // Make sure this.hash has a value before overriding default behavior
           if (this.hash !== "") {
             // Prevent default anchor click behavior
             event.preventDefault();
         
             // Store hash
             var hash = this.hash;
         
             // Using jQuery's animate() method to add smooth page scroll
             // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
             $('html, body').animate({
               scrollTop: $(hash).offset().top
             }, 800, function(){
         
               // Add hash (#) to URL when done scrolling (default click behavior)
               window.location.hash = hash;
             });
           } // End if
         });
         });
      </script>
      </script>
   </body>
</html>