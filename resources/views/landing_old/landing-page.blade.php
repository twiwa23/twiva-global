 <!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
      <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
      <title>Twiva</title>
      <link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/owl.carousel.min.css')}}" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Kaushan+Script&display=swap" rel="stylesheet">
      <link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/animate.css')}}" rel="stylesheet">
      <link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160597820-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-160597820-1');
</script>


   </head>
   <style type="text/css">
      .new-home h2 {
             text-transform: inherit;
      }
   .wrapper-line {
    display: flex;
    }
    .Download .new-hight label {
          margin-top: 3px;
    }
    .Download .new-hight label {
    font-size: 21px;
    color: #000;
}
a.store-app {
    padding: 13px 22px 7px 21px;
    margin-right: 22px;
}
    .new-right {
    margin-top: -4px;
    }
    .store-app {

    }
    .video .new-file-center label {
      color: #000 !important;
          padding-top: 1px;
              font-size: 21px;
    }
    .video  a.store-app {
      padding: 13px 22px 7px 21px;
          margin-right: 22px;
    }
    .right_side h5 {
      color: #fff;
          text-shadow: 3px 2px 8px #0b0b0b;
    font-size: 17px;
        margin-bottom: 8px;

    }
    .right_side a.new-buttom {
          background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%) !important;
              margin-top: 8px;
    margin-bottom: 5px;
    }
    video#video{
    background-image: -webkit-linear-gradient( -90deg, rgb(115,14,57) 0%, rgb(35,8,38) 100%);
}
.new-buttom {
    font-size: 15px;
    color: #fff;
   padding: 11px 33px;
    background-color: #e82b3b;
    display: inline-block;
    margin-top: 14px;
    text-transform: uppercase;
        letter-spacing: 1.2px;
}
.first_Point {
    display: flex;
}
.right_side a.new-buttom{
  margin-right: 8px;
}

   </style>

   <body id="influencer-landing" data-lang="en_GB">
      <div class="header__hamburger" id="headerHamburger">
      </div>
      <div class="heading toper">
         <div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
            <div class="header__current-language" id="languageSelector">
               <nav class="navbar">
                  <div class="container">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0);"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                           <li class="active"><a href="{{url('/')}}">Brands</a></li>
                           <li><a href="{{url('/influencer')}}">Influencers</a></li>
                             <li><a href="{{url('/user/creators')}}">Creators</a></li>
                           <li class="last"><a href="{{url('/blog')}}"> Blog</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>
      <section class="video" id="new">
         <div class="easyhtml5video" style="position:relative; max-width:100%;">
         <video playsinline="" muted="" autoplay="" preload="auto" loop="" poster="{{url('public/landing/images/pov-time-lapse-journey-on-the-modern-driverless-dubai-el_002.jpg')}}" style="width:100%; height:100%;" id="video">
            <source src="{{url('public/landing/images/Website_Video_Webm.webm')}}" type="video/webm">
         </video>
         <div class="new-home ">
            <div class="container slidetilUp   wow">
               <h2>
               Tell your Brand <br>Story Better.<sup>TM</sup></h2>
               <h3>We are an influencer and digital creative platform that harnesses, refines, and scales the power of influencer marketing and digital marketing creative to inspire business growth in Kenya.</h3>
               <!-- <a href="#download" class="new-buttom">Join to work with us</a> -->
               <div class="wrapper-line">
               <div  class="text-center">
                  <a href="https://apps.apple.com/us/app/twiva/id1492915204?ls=1"  target="_blank" class="store-app ">
                     <figure class="new-right"><i class="fa fa-apple"></i></figure>
                     <div class="new-file-center"><label>App Store</label></div>
                  </a>
               </div>
                 <div  class="text-center">
                  <a href="https://play.google.com/store/apps/details?id=com.project.twiva&hl=en"  target="_blank" class="store-app " style="margin-left: 14px;">
                     <figure class="new-right"><i class="fa fa-android"></i></figure>
                     <div class="new-file-center"><label>Play Store</label></div>
                  </a>
               </div>
            </div>
            <div class="row">
               <div class="col-md-9"></div>
               <div class="col-md-3">
                  <div class="text-left right_side">
               <h5>Ready to grow your business?</h5>
               <div class="first_Point">
                <a href="{{url('business/signup')}}" class="new-buttom second-button ">Sign Up</a>
               <a href="{{url('business/login')}}" class="new-buttom first-button">Login</a>

              </div>

               <!-- <h5>Already using Twiva? <a href="{{url('business/login')}}" style="font-size: 16px; font-weight: bold; color: rgb(115,14,57);">Sign In</a></h5> -->
            </div>
               </div>
            </div>

            </div>
         </div>
      </section>
      <div class="skew">
         <a href="#nwes"><img src="{{url('public/landing/images/arrow-smoth.png')}}" alt=""></a>
      </div>
      <!-- how it works section -->
      <section class="tiwa-works" id="nwes">
         <div class="container">
         <div class="heading">
            <p>How</p>
            <h2 class="slidetilUp   wow">Twiva Works</h2>
         </div>
         <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
               <div class="new-box slideInRight  wow delay-2">
                  <figure class="new-line"><img src="{{url('public/landing/images/one.png')}}" alt=""></figure>
                  <div class="new-new">
                     <h2>Offline influence</h2>
                     <p>Twiva lets you invite influencers to events or venues - a feature not available on any other platform. Let influencers bring what they are best at to your door.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
               <div class="new-box jack slideInRight  wow delay-1">
                  <figure class="new-line"><img src="{{url('public/landing/images/two.png')}}" alt=""></figure>
                  <div class="new-new">
                     <h2>Analytics</h2>
                     <p>Predict reach, cost, and engagement across influencer content all while measuring real-time influencer post-performance. Then generate reports to help align your marketing strategy with what is working for you.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
               <div class="new-box slideInRight  wow">
                  <figure class="new-line"><img src="{{url('public/landing/images/three.png')}}" alt=""></figure>
                  <div class="new-new">
                     <h2>Streamlined Collaboration</h2>
                     <p class="john">Work with influencers to promote your business easily at scale using our streamlined collaboration tool to make sure every shilling you spend on influencers reaches an audience who matters. </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="screenshot section-v2 one-app__section">
         <div class="container">
         <div class="row">
            <div class="col-md-5 col-xs-6 ">
               <div class="iphone-frames slidetilUp   wow">
                  <div class="new-files">
                     <div class="new-content-display">
                        <div class="blog-carousel owl-carousel owl-theme">
                           <div class="item">
                              <figure><img src="{{url('public/landing/images/a.png')}}" alt=""></figure>
                           </div>
                           <div class="item">
                              <figure><img src="{{url('public/landing/images/b.png')}}" alt=""></figure>
                           </div>
                           <div class="item">
                              <figure>
                                 <img src="{{url('public/landing/images/c.png')}}" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-7 col-xs-6">
               <div class="section-v2__content one-app__content slideInRight  wow" >
                  <div class="section-v2__title">
                     Every step of your influencer marketing campaign:
                  </div>
                  <p>
                     Twiva streamlines your collaboration with influencers from
                  <ul>
                     <li><img src="{{url('public/landing/images/discovery.png')}}" alt="">Discovering</li>
                     <li><img src="{{url('public/landing/images/evaluating.png')}}" alt="">Evaluating</li>
                     <li><img src="{{url('public/landing/images/engaging.png')}}" alt="">Engaging</li>
                     <li><img src="{{url('public/landing/images/activating.png')}}" alt="">Activating</li>
                     <li><img src="{{url('public/landing/images/mesuring.png')}}" alt="">Measuring</li>
                     <li><img src="{{url('public/landing/images/compentiation.png')}}" alt="">Compensating influencers</li>
                  </ul>
                  </p>
                  <!--        <div class="button button--long section-v2__button buttonCheckCampaigns">
                     Download indaHash app! <img class="button__arrow" src="/assets/images/deal_landing/arrow.svg" />
                     </div> -->
               </div>
            </div>
         </div>
      </section>
      <section class="about-us" id="new">
         <div class="container">
         <div class="row">
         <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="text-line slideInRight  wow">
               <div class="heading">
                  <h2>About Twiva</h2>
               </div>
               <h1>Learn More About Twiva</h1>
               <p>Twiva was founded by a group of small business owners who found it difficult to identify and collaborate with local influencers.
                  <br>
                  <br>
                  Twiva allows brands to collaborate with influencers with ease. Through the use of a mobile application, Twiva allows you to invite influencers to your events or businesses to help you increase sales
               </p>
            </div>
         </div>
         <div class="col-md-6 col-xs-6 new" data-aos="fade-left">
            <img src="{{url('public/landing/images/influener-bg.png')}}" alt="">
         </div>
      </section>
      <!-- new-home -->
      <section class="Influencer" id="new">
         <div class="container">
         <div class="new-file-top" data-aos="fade-down" data-aos-delay="300">
            <h2>FEATURED INFLUENCERS</h2>
         </div>
         <div class="heading"  data-aos="fade-down" data-aos-delay="600">
            <h2>meet some of our influencers</h2>
         </div>


         <div class="row">

                <div class="gallery_section">
                            @foreach($featured as $featureds)
               @if($featureds->image)

                                    <a href="{{$featureds->link}}" target="_blank"><img src="{{url($featureds->image)}}" alt="blog"/></a>
                                    @else
                                  <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="">

                                  @endif
            @endforeach
</div>
         </div>
      </section>
      <section class="Download" id="download">
         <div class="container">
         <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="new-hight slideInLeft  wow">
               <h2>Get the app on your phone</h2>
               <div class="wrapper-line">
               <div  class="text-center">
                  <a href="https://apps.apple.com/us/app/twiva/id1492915204?ls=1"  target="_blank" class="store-app ">
                     <figure class="new-right"><i class="fa fa-apple"></i></figure>
                     <div class="new-file-center"><label>App Store</label></div>
                  </a>
               </div>
                 <div  class="text-center">
                  <a href="https://play.google.com/store/apps/details?id=com.project.twiva&hl=en"  target="_blank" class="store-app " style="margin-left: 14px;">
                     <figure class="new-right"><i class="fa fa-android"></i></figure>
                     <div class="new-file-center"><label>Play Store</label></div>
                  </a>
               </div>
            </div>
         </div>
         </div>
         <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="images-hjj slidetilUp   wow">
               <img src="{{url('public/landing/images/img-footer.png')}}" alt="">
            </div>
         </div>
      </section>
      <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>

                     <!-- <p>info@twiva.com</p> -->
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
               </br>
                  <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>

               </ul>
            </div>
             <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
      <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
         <i class="fa fa-chevron-up"></i>
      </div>
      <!-- new-home -->
      </main>
      <script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
      <script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
      <!--       <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
         -->      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
      <script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
      <script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
      <script src="{{url('public/landing/js/owl.carousel.js')}}"></script>
      <script src="{{url('public/landing/js/wow.min.js')}}"></script>
      <script type="text/javascript">
         var wow = new WOW(
           {
             boxClass:     'wow',      // animated element css class (default is wow)
             animateClass: 'animated', // animation css class (default is animated)
             offset:       0,          // distance to the element when triggering the animation (default is 0)
             mobile:       true,       // trigger animations on mobile devices (default is true)
             live:         true,       // act on asynchronously loaded content (default is true)
             callback:     function(box) {
               // the callback is fired every time an animation is started
               // the argument that is passed in is the DOM node being animated
             },
             scrollContainer: null // optional scroll container selector, otherwise use window
           }
         );
         wow.init();
      </script>
      <script type="text/javascript">
         $(window).scroll(function() {
           if ($(this).scrollTop() > 100){
               $('nav.navbar').addClass("sticky");
           }
           else{
               $('nav.navbar').removeClass("sticky");
           }
         });
      </script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
             $(window).scroll(function(){
                 if ($(this).scrollTop() > 50) {
                     $('#backToTop').fadeIn('slow');
                 } else {
                     $('#backToTop').fadeOut('slow');
                 }
             });
             $('#backToTop').click(function(){
                 $("html, body").animate({ scrollTop: 0 }, 500);
                 return false;
             });
         });
      </script>
      <script type="text/javascript">
         $('.blog-carousel').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
              center:true,
            pagination: true,
             animateOut: 'fadeOut',
         animateIn: 'fadeIn',
             autoplaySpeed:900,
              smartSpeed: 1000,
            autoplayHoverPause: false,
            nav: true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                768: {
                    items: 1
                },
            1025: {
                    items: 1
                }
            }
         })

         $('#client-speak').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            pagination: true,
            autoplayTimeout: 3000,
         smartSpeed: 1000,
            autoplayHoverPause: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                }
            }
         })


         (function() {
  // Masonry grid setup
  $(".grid").masonry({
    itemSelector: ".grid__item",
    columnWidth: ".grid__sizer",
    gutter: 15,
    percentPosition: true
  });
  // Image replacement handler
  $(document).on("click", ".js-button", function() {
    var imageSrc = $(this).parents(".grid__item").find("img").attr("src");
    $(".js-download").attr("href", imageSrc);
    $(".js-modal-image").attr("src", imageSrc);
    $(document).on("click", ".js-heart", function() {
      $(this).toggleClass("active");
    });
  });
})();

      </script>
   </body>
</html>
