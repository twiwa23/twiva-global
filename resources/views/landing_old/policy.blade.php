<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Twiva</title>
<link rel="stylesheet" href="{{url('public/landing/css/influencer-landing.css')}}" />
<link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">



</head>
<style type="text/css">
  div#communityCounter {
    display: none;
}
.one-app__section {
    margin: -300px 0 -120px;
    width: 100%;
}

</style>
<body id="style-3" data-lang="en_GB" class="scrollbar">
<div class="header__hamburger" id="headerHamburger">

</div>
<div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
           <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <li class="last"><a href="{{url('/blog')}}"> Blog</a></li>
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
<section class="video" id="new"style="background-image: url({{url('public/landing/images/Policy.png')}});
    padding: 130px 0;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
  <div class="container">
    <div class="col-md-12">
      <div class="new-file-desert">
<!--         <h2>Privacy policy<span>how we protect your data</span></h2>
 -->       
    </div>
</section>

<!-- how it works section -->
<section class="tiwa-works influencer policy" id="">
  <div class="container">
    <div class="small-heading" data-aos="fade-down">
      <h2>Privacy Policy</h2>
      <p>Twiva Influence Inc (“Twiva”) is the producer and owner of Twiva (collectively referred to here as the/our “Platform”, “App” or “Site”), which is a web and mobile-based platform that facilitates collaborations between Brands and Influencers.
        <p>In this Twiva Privacy Policy (“Policy”), “we”, “us”, “its” and “our” refer to Twiva Influence Inc. (“Twiva”), and “you” and “your” refer to you. Before using any of our services, products, features, functionality, and Content made available through https://Twiva.co.ke and any web pages that are a part of https://Twiva.co.ke (the “Site”), please carefully read this Policy relating to your use of our platform.</p>
        <p>In this Twiva Privacy Policy (“Policy”), “we”, “us”, “its” and “our” refer to Twiva Influence Inc. (“Twiva”), and “you” and “your” refer to you. Before using any of our services, products, features, functionality, and Content made available through https://Twiva.co.ke and any web pages that are a part of https://Twiva.co.ke (the “Site”), please carefully read this Policy relating to your use of our platform.</p>
<p>We, Twiva influence Inc, are committed to safeguarding personal privacy. We recognize that you have a right to control how your personal information is collected and used. Trust is one of our pillars and part of our culture. Your providing personal information is an act of trust and we take it seriously.  Our aim is to respect and protect all your personal data in accordance with the applicable current data protection regulations. Unless given consent to do otherwise, we will only collect and use personal information as set out below.</p>
<h3>What information do we collect?</h3>
<p>You need to create an account to access our services. When registering on our platform or using our services, as appropriate, you may be asked to enter: name, gender, date of birth, e-mail address, social media handles, and mailing address, and phone number to help with your experience.
</p>
<p>
We also collect information about you when you use our Services, including browsing our website and taking certain actions within the Services. These may include; computer sign-on data, time and date data, statistics on page views, your IP address(es), your GPS location, the type of computing environment you use, and other standard web log information and information gathered from cookies, beacons and other mechanisms.
<h3>When do we collect information?</h3>
<p>First, you retain full ownership of, and absolute liability for, your channel and the content you curate. You retain complete creative direction and control over your channels and your contents.</p>
<p>Any of the information we collect from you may be used in one of the following ways:</p>
</p>
<ul class="new-text">
  <li>1.  To categorize your social followers base and assign personalized and focused influencing campaigns to you.
    <p>Your personal information will be kept on our platform and will not be disclosed to any third party.</p></li>
     <li>2. To improve our platform to serve you better.
    <p>We continually strive to improve our platform offerings based on the information and feedback we receive from you. We may use this information to perform research and analysis aimed at improving our products, services, and technologies.</p></li>
     <li>3. To process transactions
    <p>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent. We only use this information to process transactions with you.</p></li>
     <li>4. To send periodic emails
    <p>The email address you provide may be used to send you information, respond to inquiries, and/or other requests or questions.</p></li>
      <li>5.   To provide customer service
    <p>The contact information you provide us is to facilitate our communications with you; Send information, respond to inquiries, and/or other requests or questions.</p></li>
</ul>
<h3>How do we protect your information?</h3>
<p>We follow generally accepted industry standards to help protect your personal information. We implement a variety of security measures to maintain the safety of your personal information when you become an influencer or Brand with us.</p>
<p>All supplied sensitive information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into only to be accessible by those authorized with special access rights to such systems and are required to keep the information confidential.</p>
<h3> For how long we retain your personal data?</h3>
<p>We retain your personal data for a period strictly necessary for the use and the purposes described above.</p>
<p>Particularly, each data shall be retained for as long as the contract for the provision of the services you have requested is not terminated. At the end of such contract, Twiva shall cancel any data collected from you. All the personal data connected to the use of our services, as the participation to Campaigns, Gigs, Posts, contents created and published by you for such activities, shall be retained if required by the applicable regulations, laws and regulatory reporting purposes. At the end of each prescribed period, we shall cancel any personal data which is no longer necessary to retain.</p>
<h3> Do we use cookies?</h3>
<p>Yes, Cookies are small text files that are stored by the browser (for example, Chrome or Safari) on your computer or mobile phone (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information to better serve you next time (for example, the selected language).</p>
<p>Our site, like many others, uses cookies to help us customise your experience for your future visits. Cookies save you time by eliminating the need to repeatedly enter the same information, help protect your security by checking your login details and may help prevent fraud and they remember your preferences when using the site.</p>
<h3>Do we disclose any information to outside parties?</h3>
<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect others or our rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
<h3>How and when do I consent?</h3>
<p>Proof of consent is made evident in the following events:</p>
<ul>
  <li class="brands">• Brands</li>
 <p> When you create a Twiva Account</p>
<p> Signing up on our platform</p>
</ul>
<ul style="margin-bottom: 12px;">
  <li class="brands"> •  Influencers</li>
  <p> When you create a Twiva Account</p>
</ul>
<h3>• Can I withdraw my email consent?</h3>
<p>Yes, you can withdraw your email consent for marketing related communication with us at any time. If you receive such an email, you will find an unsubscribe option at the bottom of each email.</p>
<h3>• Changes to this privacy policy</h3>
<p>We reserve the right to change the terms of this Policy at any time. If there are material changes to this statement or in how Twiva will use your personal information, we will notify you by prominently posting a notice of such changes on our Platforms, or by sending you an email.</p>
<p>You may contact us with any queries you may have in respect of this Privacy Policy or your personal information by contacting us through <a href="javascript:void(0)" class="new-blue">info@twiva.co.ke</a></p>



  </div>

</div>
</section>
<section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>



</body>
</html>
