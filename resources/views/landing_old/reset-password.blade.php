<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Reset Password</title>
<link rel="stylesheet" href="{{url('public/main-page/css/influencer-landing.css')}}" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
<style>
section.login_FORM{
  margin-top: 95px;
  }

  .error {
  color: red;
}
section.login_FORM form i.fa {
    font-size: 22px;
    color: #b1bbc1;
    position: absolute;
    top: 15px;
    bottom:unset;
    margin: auto;
    border: 0px solid;
    height: unset;
    line-height: normal;
}
.text-danger {
    color: red;
}
  .alert-danger {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}

section.login_FORM.singup form {
    max-width: 477px;
    width: 100%;
}
  </style>
<body>
  <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
        <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <li class = "last"><a href="{{url('/blog')}}"> Blog</a></li>
         
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
   
  <section class="login_FORM singup">
    <div class="container">
      <div class="col-sm-12">
        <div class="text-center">
        <h2> Reset Password</h2>
        <p class="text-center sub-title">Collaborate with brands through Twiva.</p>
      </div>
      @include('admin.layouts.notifications')
        <form method="POST" id="form_validate" enctype="multipart/form-data"> 
        {{csrf_field()}}
        
             <div class = "row">
          <div class="col-sm-12">
          <div class="form-group">
            <div class = "password">
          <input type="password" name="password" id="password"  class="form-control" placeholder = "Password" > 
            <i class="fa">
              <img src="{{url('public/main-page/images/password.jpg')}}">
            </i>
          </div>
          <label id="password-error" class="error" for="password">{{$errors->first('password')}}</label>
        </div>
          </div>
           </div>
           <div class = "row">
            <div class="col-sm-12">
          <div class="form-group">
            <div class = "password-new">
           <input type="password" name="confirm_password" class="form-control" placeholder = "Confirm Password" >
              <i class="fa">
              <img src="{{url('public/main-page/images/password.jpg')}}">
            </i>
             <span class="text-danger"></span>
          </div>
          <label id="confirm_password-error" class="error" for="confirm_password">{{$errors->first('confirm_password')}}</label>
        </div>
          </div>
        </div>
           <div class="col-sm-12">
          <div class="form-group text-center">
                <input type="Submit" name="Submit" value="Submit" class="btn btn-primary width-415">
          </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>

</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
    document.getElementById("category_name").addEventListener("change",function(){
      window.location.href = this.value;
    })
  </script>
<!-- <script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script> -->
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
 <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

     /*$(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });*/
     $('#form_validate').validate({ 
      rules: {
        
          password: {
              required: true,
              minlength:6,
              maxlength:30
          },
          confirm_password: {
              required: true,
              minlength:6,
              equalTo: "#password",
          },
          
      },

        messages:{
         
          password:{  
            required:'Please enter password.',
            minlength:'Password must be at least 6 characters.',
            maxlength:'Password maximum 30 characters.'
          },

         

          confirm_password:{  
            required:'Please enter confirm password.',
            equalTo:'Password and confirm password should be same.',
            minlength : "Password must be at least 6 characters."
          }, 

         
        },
        });    
   </script>


</body>
</html>
