<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Twiva</title>
<link rel="stylesheet" href="{{url('public/landing/css/influencer-landing.css')}}" />
<link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">



</head>
<style type="text/css">
  div#communityCounter {
    display: none;
}
.one-app__section {
    margin: -300px 0 -120px;
    width: 100%;
}

</style>
<body id="style-3" data-lang="en_GB" class="scrollbar">
<div class="header__hamburger" id="headerHamburger">

</div>
<div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
          <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <li class="last"><a href="{{url('/blog')}}"> Blog</a></li>
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
<section class="video" id="new" style="background-image:url({{url('public/landing/images/influencer.png')}});
    padding: 130px 0;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
  <div class="container">
    <div class="col-md-12">
      <div class="new-file-desert">
<!--         <h2>Twiva Platform Terms Of Service</h2>
 -->      
    </div>
</section>

<!-- how it works section -->
<section class="tiwa-works influencer policy" id="">
  <div class="container">
    <div class="small-heading" data-aos="fade-down">
      <h2>TWIVA PLATFORM TERMS OF SERVICE</h2>
      <h3>OVERVIEW</h3>
      <p>This website is operated by Twiva Influence Inc. Throughout the site, the terms “we”, “us”, “Twiva” and “our” refer to Twiva Influence Inc. Twiva Influence Inc offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.
</p>
<p>By visiting our site and/or signing up with us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of Twiva Platform, including without limitation users who are browsers, customers, and/ or contributors of content.</p>
<p>Please read these Terms of Service carefully before accessing or using our platform. By accessing or using any part of the site and mobile applications, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website and the mobile applications or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service.</p>

<p>Any new features or tools which are added to the current website/platform and mobile applications shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.</p>
<h3>SECTION 1 - WEBSITE TERMS</h3>
<p>By agreeing to these Terms of Service, you represent that you are at least the age of majority in your country of residence.</p>
<p>The Twiva Platform is designed for use by people aged 19 years and over. Users under the age of 19 years may only use the Twiva Platform with the consent of a parent or legal guardian. To use the Twiva Platform, you must be eligible to use the social media platforms (under the relevant platforms prevailing terms and conditions) upon which you intend to publish posts.</p>
<p>You may not use our products or services for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).</p>
<p>You must not transmit any worms or viruses or any code of a destructive nature.</p>
<p>A breach or violation of any of the Terms will result in an immediate termination of your Account.</p>
<h3>SECTION 2 - GENERAL CONDITIONS</h3>
<p>We reserve the right to refuse service to anyone for any reason at any time.
You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.
You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website and all our platforms through which the service is provided, without implied or express written permission by the owner of the content.
The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.
</p>
<h3>SECTION 3 - ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION</h3>
<p>We are not responsible if information made available on this site by brands/companies or influencers is not accurate, complete or current.
This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.
</p>
<h3>SECTION 4 - MODIFICATIONS TO THE SERVICE AND PRICES</h3>
<p>Prices for our services are subject to change at any time by giving you notice to the email address you have registered with us to you.</p>
<p>We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) at any time by giving you notice to the email address you have registered with us to you.</p>
<p>We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.</p>
<h3>SECTION 5 - DISCLAIMER</h3>
<p>Twiva Influence Inc is not a party to any open market campaigns(“Campaign”), invite only services and activities (“Gig” and/or “Posts”) between its Members. Twiva Influence Inc does not employ content creators(“Influencers”), is not an employment agency for Marketers (“Brands”/“Companies”), and is not in the business of providing payment processing services.</p>
<p>You are responsible for determining whether any applicable income tax or other taxes apply to any payments made or received by you in connection with activities on our platform and to collect, report, and remit the correct tax to the appropriate tax authority</p>
<h3>SECTION 6 - BILLING AND ACCOUNT INFORMATION</h3>
<p>You agree to provide current, complete and accurate purchase and account information for all transactions made on our platform. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.</p>
<p>The relevant Brand is solely responsible for reviewing and approving all paid content and posts that form part of their activities and for the payment of the applicable fee or collaboration compensation to Influencers, nor is Twiva liable in any way for the content of any paid content or post. These Terms of Use shall apply to all transactions conducted through the Twiva Platform.</p>
<p>Influencers and Brands/Companies agree that they will not attempt to negotiate terms or payment with each other outside of the Twiva Platform. Without limiting any other rights or remedies available to Twiva, any attempt to circumvent the Twiva Platform may result in removal from the Twiva Platform at Twiva’s sole discretion.</p>
<h3>SECTION 7 - OPTIONAL TOOLS</h3>
<p>We may provide you with access to third-party tools over which we neither monitor nor have any control nor input.</p>
<p>You acknowledge and agree that we provide access to such tools ”as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools.</p>
<p>Any use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).</p>
<p>We may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms of Service.</p>
<h3>SECTION 8 - THIRD-PARTY LINKS</h3>
<p>Certain content, products and services available via our Service may include materials from third-parties.</p>
<p>Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.</p>
<p>We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party's policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.</p>
<h3>SECTION 9 - USER POSTS, CONTENT, COMMENTS, FEEDBACK AND OTHER SUBMISSIONS</h3>
<p>If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative content, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.</p>
<p>We may will monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Service. There is a zero-tolerance policy for posting content that is otherwise objectionable or abusive to other uses. If users deem your content objectionable or abusive, your content and profile on the platform is subject to immediate removal.</p>
<p>You agree and understand that any content you post or provide may be viewed by the general public and will not be treated as private, proprietary or confidential.</p>
<p>All rights, including without limitation, all intellectual property rights in, and to any content, including all photographs, images, videos, audio, works of art, original writing, drawings, derivatives, compositions, creations and inventions developed by Influencer shall be owned exclusively by the Influencer. These rights are transferred to the company/brand when influencer accepts a “Gig” and/or a “Post” or when a company/brand accepts a submission under “Campaign”.</p>
<p>You agree that your content and activities will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your content or activities will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any content. You are solely responsible for any content or activity you make and their accuracy. We take no responsibility and assume no liability for any content posted by you or any third-party.</p>
<p>We are not responsible for any obscene or offensive content that you receive or view from others while using our platform. However, if you do receive or view such content, please contact us by e-mail to <a href="javascript:void(0);" class="new-blue">info@Twiva.co.ke</a> so that we can investigate the matter. Although we are not obligated to do so, we reserve the right to monitor, investigate, and remove obscene or offensive material posted to our platform and/or terminate an account for any reason.</p>
<p>You acknowledge and agree that the brand you are in collaboration with and Twiva has the right at any time to moderate any post after publication to a social media channel and that you will immediately make any reasonable modification or amendment requested by Twiva or the relevant brand to the post, subject to compliance with these Terms of Use. You also acknowledge and agree that if you post an incorrect post, the brand or Twiva may request that you post the correct approved post and that you will immediately comply with such a request.</p>
<p>Influencers must clearly disclose in sponsored posts their relationship with the brand. Twiva requires that you make such disclosures in such a way so that it is clear to the ordinary consumer viewing your post that there is a commercial relationship between Influencer and the brand. This may be achieved using hashtags such as #advertisement or #ad or through other means suitable to your circumstances, Community and Channels. Twiva reserves the right, but is in no way obliged, to review and monitor the disclosure practices of all Influencers in relation to particular posts or your Channels generally and to require greater levels of disclosure (at Twiva’s sole discretion) in particular posts or across your Channels generally or, if you do not agree to the required levels of disclosure, to remove you from the Twiva Platform.</p>
<h3>SECTION 10 – INFLUENCER RESTRAINTS</h3>
<p>As an Influencer, you agree that you will not:</p>
<p>1. Delay posting your Post after the Brand has given its approval to your Post and you must publish your approved Post no later than the specified time after receiving notification of the Brand’s approval.</p>
<p>2. For a period of five (5) hours after a Post is published, post, share, re-tweet or re-gram any other posts or content to that Channel where the effect of publishing such additional posts or content would be to reduce the prominence of the Post;</p>
<p>3. Remove the Post from your Channels for a period of 30 days after the Post is published, expiring at 11:59pm on the thirtieth calendar day after the Post is published;</p>
<p>4. Edit any approved Post before or after it has been published other than in accordance with these Terms of Use;</p>
<p>5. Parody, disparage, give any adverse comment or make fun of the Brand or its products of services generally in any way;</p>
<p>6. Create any contextual or surrounding posts or other material on a Channel that in any way detracts from, dilutes the effect of, or undermines a Post or the Brand or its products or services; and</p>
<p>7. Misrepresent the size of your audience or the numbers of your followers or engagement. Followers must be obtained organically and not through unethical or unsportsmanlike behavior such as (but not limited to), purchasing followers, likes or engagement. If Twiva suspects (in its sole discretion) that Influencers are not complying with the requirement for followers to be authentic and organically grown, Twiva reserves the right to remove Influencers from the Twiva Platform</p>
<h3>SECTION 11 – ACKNOWLEDGEMENT</h3>
<p>The Brand and the Influencer acknowledge that:</p>
<p>1. Neither the Influencer nor Twiva is required to purchase any of the Brand’s products or services;</p>
<p>2. Brand may, in its sole discretion, arrange to send an Influencer a sample product. Influencer may request a Brand to send a sample product, but Brand is under no obligation to do so;</p>
<p>3. Twiva will not be responsible or liable in any way for late delivery or non-arrival or any products sent from a Brand to an Influencer. Address provided by Influencer through the Twiva Platform is not verified by Twiva;</p>
<p>4. If Brand sends a sample product to an Influencer, there is no guarantee that the Influencer will submit a Post about the product or that any post submitted will be positive. All product reviews must reflect the Influencer’s genuinely held beliefs;</p>
<p>5. A product, service or other non-monetary arrangement may be offered or used as partial or full payment by a Brand for an Influencer’s Post;</p>
<p>6. Nothing in these Terms of Use grants to the Brand any ownership rights in the Intellectual Property Rights in the Posts or the Influencer’s Identity;</p>
<p>7. Nothing in these Terms of Use grants to the Influencer any ownership rights in the Intellectual Property Rights of the Brand; and</p>
<p>8. Nothing in these Terms of Use requires the Brand to make use of any of the rights granted to the Brand by the Influencer under this Agreement.</p>
<p>9. Twiva is not legally responsible for inappropriate actions or behaviors on the part of brands and/or influencers, acted online or in-person.</p>
<p>10.  Twiva requires a commission on Brand’s payment to cover transactional costs related to payment processing fees. Twiva also obtains a commission on payment made to influencer through the mobile app.</p>
<h3>SECTION 12 - PERSONAL INFORMATION</h3>
<p>Your submission of personal information through our platform/website/mobile application is governed by our Privacy Policy.
For more on this, view our Privacy Policy.
<h3>SECTION 13 - ERRORS, INACCURACIES AND OMISSIONS</h3>
<p>Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, promotions, offers and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update.</p>
<p>We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.</p>
<h3>SECTION 14 - PROHIBITED USES</h3>
<p>In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site/platform or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.</p>
<h3>SECTION 15 - DISCLAIMER OF WARRANTIES; LIMITATION OF LIABILITY</h3>
<p>uninterrupted, timely, secure or error-free.
We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.
You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, with prior notice to you.
You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through our platform are (except as expressly stated by us) provided 'as is' and 'as available' for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.
In no case shall Twiva, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using our platform/website services, or for any other claim related in any way to your use of the service, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content posted, transmitted, or otherwise made available via our platform, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.</p>
<h3>SECTION 16 - INDEMNIFICATION</h3>
<p>You agree to indemnify, defend and hold harmless Twiva Influence Inc and our subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.</p>
<h3>SECTION 17 - SEVERABILITY</h3>
<p>If any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.</p>
<h3>SECTION 18 - TERMINATION</h3>
<p>The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.</p>
<p>These Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.</p>
<p>If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).</p>
<h3>SECTION 19 - ENTIRE AGREEMENT</h3> 
<p>The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision.
These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service).</p> 
<p>Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.</p>
<h3>SECTION 20 - GOVERNING LAW</h3>
<p>These Terms of Service and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of Nairobi, Kenya.</p>
<h3>SECTION 21 - CHANGES TO TERMS OF SERVICE</h3>
<p>You can review the most current version of the Terms of Service at any time at this page.
We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes acceptance of those changes.
</p>
<h3>SECTION 22 - CONTACT INFORMATION</h3>
<p>Questions about the Terms of Service should be sent to us at <a href="javascript:void(0);" class="new-blue">info@Twiva.co.ke</a></p>
</div>

</div>
</section>
 <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
               
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                   <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>
                           </div>

                   <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>



</body>
</html>

