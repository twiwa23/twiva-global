
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Signup</title>
<link rel="stylesheet" href="{{url('public/main-page/css/influencer-landing.css')}}" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
<style>

  .error {
  color: red;
}
section.login_FORM form i.fa {
    font-size: 22px;
    color: #b1bbc1;
    position: absolute;
    top: 15px;
    bottom:unset;
    margin: auto;
    border: 0px solid;
    height: unset;
    line-height: normal;
}

  .alert-danger {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}
.alert-success {
    color: #fff;
    background-color: #8e1755;
    border-color: #ebccd1;
    max-width: 499px;
    width: 100%;
    margin: 0 auto;
}

  </style>
<body>
 <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
        <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <li class = "last"><a href="{{url('/blog')}}"> Blog</a></li>
         
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
   
  <section class="login_FORM singup">
    <div class="container">
      <div class="col-sm-12">
        <div class="text-center">
        <img src="{{url('public/main-page/images/lets-get.png')}}">
        <p class="text-center sub-title">Collaborate with brands through Twiva.</p>
      </div>
      @include('admin.layouts.notifications')
         <!-- <form method = "post" action = "" enctype=multipart/form-data>
          {{csrf_field()}} -->
 <!--        @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
            @endif -->
        <form method="POST" id="form_validate" enctype="multipart/form-data" onsubmit="return validateForm()"> 
        {{csrf_field()}}
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <div class="profile_pic text-center">
                  <div class="upload_pic">
                   @php($url =  url('public/main-page/images/profile-pic.png'))
                         <img  title="Click to change image"
                           onclick="$('#imgInp').click()" src='{{$url}}' id="blah" style="width: 100px;height: 100px; object-fit: cover; border-radius: 100%;" />
                        <input style="display:none;" type="file" id="imgInp" name="profile" data-role="magic-overlay"
                             data-target="#pictureBtn" value="">
                </div>
                <span style="display:none; margin :20px;color: red" class="text-danger" id="invalid_file">Please select jpg, jpeg or png image format only. </span>
                         <span class = "error">{{$errors->first('profile')}}</span>
               
                  <div class="text-center">Profile Picture</div>
                </div>
              </div>
            </div>
            <div class = "row">
            <div class="col-sm-6">
          <div class="form-group">
            <div class = "email">
          <input type="text" id = "email" name="email" value = "{{old('email')}}" class="form-control" placeholder = "Email" >
             <i class="fa">
              <img src="{{url('public/main-page/images/email.jpg')}}" >
            </i>
           </div>
           <label id="email-error" class="error" for="email">{{$errors->first('email')}}</label>
          </div>
          </div>
           <div class="col-sm-6">
          <div class="form-group">
            <div class = "name">
            <input type="text" name="name" id = "name" value = "{{old('name')}}" class="form-control" placeholder = "Username" > 
            <i class="fa">
              <img src="{{url('public/main-page/images/username.jpg')}}">
            </i>
          </div>     
          <label id="name-error" class="error" for="name">{{$errors->first('name')}}</label>
          </div>
          </div>
          </div>
          <div class = "row">
          <div class="col-sm-6">
          <div class="form-group">
            <div class = "password">
          <input type="password" name="password" id="password"  class="form-control" placeholder = "Password" > 
            <i class="fa">
              <img src="{{url('public/main-page/images/password.jpg')}}">
            </i>
          </div>
          <label id="password-error" class="error" for="password">{{$errors->first('password')}}</label>
        </div>
          </div>

            <div class="col-sm-6">
          <div class="form-group">
            <div class = "password-new">
           <input type="password" name="confirm_password" class="form-control" placeholder = "Confirm Password" >
              <i class="fa">
              <img src="{{url('public/main-page/images/password.jpg')}}">
            </i>
             <span class="text-danger"></span>
          </div>
          <label id="confirm_password-error" class="error" for="confirm_password">{{$errors->first('confirm_password')}}</label>
        </div>
          </div>
        </div>
           <div class = "row">
            <div class="col-sm-6">
          <div class="form-group">
          <div class="phone_number">
          <input type="text" name="phone_number" id="phone" onkeypress="return isNumberKey(event)" value = "{{old('phone_number')}}" class="form-control" placeholder = "Phone Number" >
            <i class="fa">
              <img src="{{url('public/main-page/images/call.jpg')}}">
            </i>
              <span class="text-danger"></span>
          </div>
          <label id="phone-error" class="error" for="phone">{{$errors->first('phone_number')}}</label>
          </div>
           </div>
            <div class="col-sm-6">
           <div class="form-group">
            <div class="category_name">
             <select name = "category_name" id = "category_name" class="form-control" required>
               <option value=""> Select Category</option>
              @foreach($category as $value)
              <option value="{{$value->id}}" @if(old('category_name') == $value->id) selected @endif >{{$value->name}}</option>
              @endforeach
            </select>
            <i class="fa">
              <img src="{{url('public/main-page/images/category.jpg')}}">
            </i>
            <span class="text-danger"></span>
          </div>
           <label id="category_name-error" class="error" for="category_name">{{$errors->first('category_name')}}</label>
        </div>
          </div>
        </div>
        <div class = "row">
          <div class="col-sm-12">
          <div class="form-group">
          <div class="description">
            <textarea class = "form-control" id = "description" name="description" placeholder="Description" maxlength="1000" >{{old('description')}}</textarea>
            <i class="fa">
              <img src="{{url('public/main-page/images/pen.jpg')}}">
            </i>
          </div>
          <label id="description-error" class="error" for="description">{{$errors->first('description')}}</label>
          </div>
          </div>
        </div>
        <div class = "row">
        <div class="col-sm-12">
           <div class="form-group">
            <div class="category_name">
            <select id="discover_twiva" class="form-control" name="discover_twiva">
              <option value="">How did you discover Twiva?</option>
              <option value="Search engine(Google, etc)">Search engine(Google, etc)</option>
              <option value="Social media">Social media</option>
              <option value="Referral">Referral</option>
              <option value="KEPSA">KEPSA</option>
              <option value="Other">Other</option>
            </select>
            <i class="fa">
              <img src="{{url('public/main-page/images/category.jpg')}}">
            </i>
            <span class="text-danger"></span>
          </div>
           <label id="discover_twiva-error" class="error" for="discover_twiva">{{$errors->first('discover_twiva')}}</label>
        </div>
          </div>
        </div>
        <div class = "row Orther_field">
          <div class="col-sm-12">
          <div class="form-group">
          <div class="phone_number">
          <input type="text" name="other" value = "{{old('other')}}" class="form-control" placeholder = "Other" >
            <i class="fa">
              <img src="{{url('public/main-page/images/call.jpg')}}">
            </i>
              <span class="text-danger"></span>
          </div>
          <label id="other-error" class="error" for="other">{{$errors->first('other')}}</label>
          </div>
          </div>
        </div>
           <div class="col-sm-12">
          <div class="form-group text-center">
                <input type="Submit" name="SIGN UP" value="SIGN UP" class="btn btn-primary width-415">
          </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>

<script type="text/javascript">
  $(".Orther_field").hide()
  $("#discover_twiva").change(function() {
    let val = $("#discover_twiva option:selected" ).val();
    if(val == 'Other') {
      $(".Orther_field").show()
    } else {
      $(".Orther_field").hide()
    }
    
  })
</script>
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

     /*$(".alert-success").fadeTo(2000, 5000).slideUp(500, function(){
      $(".alert-success").slideUp(500);
    });*/
         
   </script>
</script>
<script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var type = (input.files[0].type);

                if (type == "image/png" || type == "image/jpeg") {
                    $('#invalid_file').css({'display': 'none'});
                } else {
                    $('#invalid_file').css({'display': 'block'});
                    $('#imgInp').val('');
                    return false;
                }


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });

    </script>
   <script>
     function validateForm(){
      $("#invalid_file").hide();
      if($("input[name='profile']").val() == ""){
        $("#invalid_file").show().text("Please upload profile");
        return false;
      }
    }    
   </script>
 <SCRIPT language=Javascript>
       <!--
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       $("#name").on("keypress",function(event){
        let key = event.keyCode;
        if(key == 32){
          return false;
        }
       })
       //-->
    </SCRIPT>

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

    $("#email").on("focusout",function(){
                let val = $(this).val();

                  //alert(val);
                var data = {
                  '_token': "{{csrf_token()}}",
                  'email': val,
                };

              
              $.ajax({
                  url:"{{url('user/check-exist-email')}}",
                  type:'POST',
                  data:data,
                  success: function(res){
                    console.log(res)
                    if(res == 1){
                      $("#email-error").text("Account already exists, please Sign In.").show();
                    }else{
                      $("#email-error").text("").hide();
                    }
                  },
                  error: function(data, textStatus, xhr) {
                    if(data.status == 422){
                      var result = data.responseJSON;
                      alert('Something went worng.');
                      window.location.href = "";
                      return false;
                    } 
                  }
                });

        });

      $("#name").on("focusout",function(){
                let val = $(this).val();

                  //alert(val);
                var data = {
                  '_token': "{{csrf_token()}}",
                  'name': val,
                };

              
              $.ajax({
                  url:"{{url('user/check-exist-name')}}",
                  type:'POST',
                  data:data,
                  success: function(res){
                    console.log(res)
                    if(res == 1){
                      $("#name-error").text("Username is already exists.").show();
                    }else{
                      $("#name-error").text("").hide();
                    }
                  },
                  error: function(data, textStatus, xhr) {
                    if(data.status == 422){
                      var result = data.responseJSON;
                      alert('Something went worng.');
                      window.location.href = "";
                      return false;
                    } 
                  }
                });

        });

  jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "Space not allowed");

  $('#form_validate').validate({ 
      rules: {
          email: {
              required: true,
              email:true,
              maxlength:200
          },
           
          password: {
              required: true,
              minlength:6,
              maxlength:30
          },
          confirm_password: {
              required: true,
              minlength:6,
              equalTo: "#password",
          },
          name: {
              required: true,
              maxlength:50,
              minlength:3,
              noSpace : true
          },
          phone_number: {
              required: true,
              minlength:8,
              maxlength:15,
             
          },
          description: {
              required: true,
              minlength:10,
              maxlength:1000
          },
           category_name: {
              required: true,
          }, 

          // discover_twiva: {
          //     required: true,
          // },

          // other: {
          //     required: true,
          // },
      },

        messages:{
          name:{  
            required:'Please enter username.',
            minlength:'Username must be at least 3 characters.',
            maxlength:'Username maximum 15 characters.'
          },
         
          email:{  
            required:'Please enter email address.',
            email:'Please enter a valid email address.',
            maxlength:'Email address maximum 200 characters.'
          },
          password:{  
            required:'Please enter password.',
            minlength:'Password must be at least 6 characters.',
            maxlength:'Password maximum 10 characters.'
          },

          description:{  
            required:'Please enter description.',
            minlength:'Description must be at least 10 characters.',
            maxlength:'Description maximum 1000 characters.'
          },

          confirm_password:{  
            required:'Please enter confirm password.',
            equalTo:'Password and confirm password should be same.',
            minlength : "Password must be at least 6 characters."
          }, 

          phone_number:{  
            required:'Please enter phone number.',
            minlength:'Phone number must be at least 8 digits.',
            maxlength:'Phone number maximum 15 digits.',
          }, 
          category_name:{  
            required:'Please select category name.',
           
          }, 
          discover_twiva:{  
            required:'Please select how did you discover twiva.',
           
          },  
          other:{  
            required:'Please enter other.',
          },             
        },
        submitHandler:function(form){

                  if($("#email-error").text().length > 0){
                    return false;
                  }else {
                    form.submit();
                  }

                   if($("#name-error").text().length > 0){
                    return false;
                  }else {
                    form.submit();
                  }
          }
        /*errorHandler:function(e){
          console.log(e)
        }*/
       
    
  });
</script>

</body>
</html>
