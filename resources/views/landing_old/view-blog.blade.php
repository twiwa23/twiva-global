<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
      <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
      <link href="{{url('public/landing/css/aos.css')}}" type="text/css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
      <title>Twiva</title>
      <link rel="stylesheet" href="{{url('public/landing/css/influencer-landing.css')}}" />
      <link href="{{url('public/landing/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
      <link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
   </head>
   <style type="text/css">
      div#communityCounter {
      display: none;
      }
      .one-app__section {
      margin: -300px 0 -120px;
      width: 100%;
      }
      #nwes .col-md-4.col-sm-4.col-xs-4 {
    min-height: 365px;
}
.new-box.slideInRight.wow.delay-2 {
     background-color: #fff;
    padding: 15px;
    margin-bottom: 22px;
    box-shadow: 0 0 13px #ccc;
    min-height: 424px;

}
.new-box span {
    line-height: 20px;
    font-size: initial;
    min-height: 120px;
    word-break: break-all;
    }
 
.contact-us .btn.btn-primary{
      margin-top: 20px;
}
.new-new {
    margin-left: 11px;
}

.new-box h2
{
  margin-bottom: 10px; font-size: initial;
}

.contact-us .btn.btn-primary{
      padding: 9px 40px;
      font-size: 14px;
}

.new-new strong {
    color: #555;
        font-size: initial;
    color: initial;
}

figure.new-line img {
    height: auto;
    width: 100%;
    object-fit: scale-down;
}

.new-new {
    text-align: left;
     margin-top: 40px;
}
.new-box figure {
    text-align: left;
    min-height: 178px;
    height: 470px;
    overflow: hidden;
}
.new-box figure{
      display: flex;

}
.new-box.slideInRight.wow.delay-2{
     min-height: 120px;
    overflow: auto;
}

.new-box .new-new a {
    margin-top: 10px;
    display: inline-block;
    color: initial;
}
.new-box p{
      margin-bottom: 15px;

}
.new-new li {
    margin-bottom: 13px;
}
.new-new ul, .new-new li {
    list-style: inherit;
}
   </style>
   <body id="style-3" data-lang="en_GB" class="scrollbar">
      <div class="header__hamburger" id="headerHamburger">
      </div>
      <div class="heading toper">
         <div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
            <div class="header__current-language" id="languageSelector">
               <nav class="navbar">
                  <div class="container">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="{{url('/')}}">Brands</a></li>
                           <li><a  href="{{url('/influencer')}}">Influencers</a></li>
                            <li><a href="{{url('/user/creators')}}">Creators</a></li>
                           <li class="active last"><a href="{{url('/blog')}}"> Blog</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>
      <section class="video" id="new"style="background-image: url({{url('public/landing/images/influencer.png')}});
         padding: 140px 0;
         background-size: cover;
         background-position: center;
         background-repeat: no-repeat;">
         <div class="container">
      </section>
     <section class="tiwa-works" id="">
         <div class="container">
         <div class="heading">
            
            <h2 class="slidetilUp   wow" style="visibility: visible; animation-name: slidetilUp; text-align: center;">Blog Details</h2>
         </div>
         <div class="row">
       
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="new-box slideInRight  wow delay-2" style="visibility: visible; animation-name: slideInRight;">
                  <figure class="new-line"> @if($blog->image)
                                    <img src="{{url($blog->image)}}" alt="blog"/>
                                    @else
                                  <img src="{{url('public/admin/production/images/user.png')}}" alt="" style="">
                                  @endif
                      </figure>
                  
                     <h2>@if(!empty($blog->title)){{$blog->title}} @else N/A @endif</h2>
                       <div class = "new-new">
                          <span> 
                      {!!html_entity_decode($blog->text)!!}
                     </span>
           </div>
               
               </div>
            </div>
          
       
         </div>
      </div></section>
      <section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
              
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                      <a href="mailto:info@twiva.co.ke"><figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                
               </ul>

            </div>
              <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
      <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
         <i class="fa fa-chevron-up"></i>
      </div>
      <!-- new-home -->
      </main>
      <script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
      <script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
      <script src="{{url('public/landing/js/aos.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
      <script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
      <script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
      <script type="text/javascript">
         $(document).ready(function() {
         function close_accordion_section() {
         $('.accordion .accordion-section-title').removeClass('active');
         $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
         }
         
         $('.accordion-section-title').click(function(e) {
         // Grab current anchor value
         var currentAttrValue = $(this).attr('href');
         
         if($(e.target).is('.active')) {
          close_accordion_section();
         }else {
          close_accordion_section();
         
          // Add active class to section title
          $(this).addClass('active');
          // Open up the hidden content panel
          $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
         }
         
         e.preventDefault();
         });
         });
         
      </script>
      <script type="text/javascript">
         $(window).scroll(function() {
           if ($(this).scrollTop() > 100){  
               $('nav.navbar').addClass("sticky");
           }
           else{
               $('nav.navbar').removeClass("sticky");
           }
         });
      </script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
             $(window).scroll(function(){
                 if ($(this).scrollTop() > 50) {
                     $('#backToTop').fadeIn('slow');
                 } else {
                     $('#backToTop').fadeOut('slow');
                 }
             });
             $('#backToTop').click(function(){
                 $("html, body").animate({ scrollTop: 0 }, 500);
                 return false;
             });
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
         // Add smooth scrolling to all links
         $("#nwes").on('click', function(event) {
         
           // Make sure this.hash has a value before overriding default behavior
           if (this.hash !== "") {
             // Prevent default anchor click behavior
             event.preventDefault();
         
             // Store hash
             var hash = this.hash;
         
             // Using jQuery's animate() method to add smooth page scroll
             // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
             $('html, body').animate({
               scrollTop: $(hash).offset().top
             }, 800, function(){
         
               // Add hash (#) to URL when done scrolling (default click behavior)
               window.location.hash = hash;
             });
           } // End if
         });
         });
      </script>
      </script>
   </body>
</html>