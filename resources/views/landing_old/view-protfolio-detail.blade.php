
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{url('public/main-page/css/aos.css')}}" type="text/css" rel="stylesheet">
              <link rel="shortcut icon" href="{{url('public/main-page/images/fav.png')}}" type="image/x-icon">

<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" rel="stylesheet">
<title>Portfolio</title>
<link rel="stylesheet" href="css/influencer-landing.css" />
<link href="{{url('public/main-page/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/main-page/css/responsive.css')}}" type="text/css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Damion&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">

<link href="{{url('public/landing/css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{url('public/landing/css/responsive.css')}}" type="text/css" rel="stylesheet">
</head>
<style>

.new-file-service.add {
    background-color: #f1f1f1;
    text-align: center;
    display: flex;
    min-height: 414px;
    align-items: center;
    justify-content:center;
}
span.begin {
    margin-top: 23px;
    display: block;
    text-align: center;
}
.pay .col-md-3.col-sm-4 {
    min-height: 758px;
}
.new-file-service.add a {    
    min-height: 526px;
    color:black;
    
}
.text-color ul li{
      min-height: 358px;

}
.new-file-service p{
      min-height: 226px;

}
.alert-success{
  background-color: #ad0e5a;
  color:#fff;
  max-width: 500px;
  margin:0 auto;
  width: 100%;
}
.new-file-service.add figure {
    margin-bottom: 10px;
}
.pluse-add p {
    color: #000;
}
.new-file-service.add.image {
    min-height: 281px;
}
.payments.atrate {
    min-height: 623px;
}
.pluse-add.rate {
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 722px;
  background-color:#f1f1f1;
}.payments.atrate {
    padding-bottom: 0;
}
.pluse-rate figure {
    margin-bottom: 15px;
}

.pluse-add.rate a {
    color: black;
}
 img.new-photo {
    width: 130px;
    height: 130px;
    border-radius: 100%;
    border:3px solid #8a0e49;
}
.over-view view img {
    height: 400px;
}
.over-view video {
    width: 100%;
    height: 400px;
    object-fit: cover;
}
.col-md-4.no-padding-right.col-sm-4 {
    min-height: 418px;
}
.login_HEADER.new .Logn-logo img {
    position: absolute;
    right: 0;
    left: 0;
    top: 5px;
    margin: 0 auto;

}
.New-describe-section figure img {
    margin-top: -108px;
    border-radius: 100%;
    border: 7px solid #780c4f;
    width: 193px;
    height: 193px;
    
    object-fit: cover;
}

.new-file-service  {
    padding: 13px 11px 11px 12px; 
    text-align: left; 
}

.life-style p {
    min-height: 59px;
}
.new-file-service.file figure img {
    height: 100px;
    width: 100px;
    border-radius: 100%;
    margin: 0 auto;
    text-align: center;
    object-fit: cover;    
    border: 4px solid #931a71;
}
header.login_HEADER.new.creatives .new-button {
    width: 136px;
    height: 48px;
    font-size: 18px;
    font-weight: 600;
    margin-left: 16px;
    margin-top: 0px;
}
header.login_HEADER.new.creatives .new-button.first {
    background-color: #e8eff3;
    color: #63727a;
}
.login_HEADER.new.creatives .toggle {
    float: left;
    margin-top: 0;
}
h3.more {
    margin-top: 14px;
    font-size: 19px;
    color: #000;
}
.text-color ul li {
    min-height: auto;
}
.text-color ul {
    list-style-type: none;
    margin-left: 43px;
    min-height: 368px;
}

.gallary .over-view .new img {
    max-width: 100%;
    height: 400px;
    width: 100%;
    object-fit: cover;
}
</style>


<body>

  <div class="heading toper">

<div class="header__language" id="languageContainer" data-lang="" data-state="hidden">
<div class="header__current-language" id="languageSelector">

<nav class="navbar res">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('public/landing/images/logo.png')}}" alt=""></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">Brands</a></li>
        <li><a href="{{url('/influencer')}}">Influencers</a></li>
           <li><a href="{{url('/user/creators')}}">Creators</a></li>
          <?php 
        $auth_id = Auth::id();
        $id = Request::segment(3);
        ?>
      @if(!empty($auth_id))
       <li><a href="{{url('/user/edit-profile')}}">Edit Profile</a></li>
       <li class="last"><a href="{{url('user/userlogout')}}">Logout</a></li>
       @else
       <li class = "last"><a href="{{url('/blog')}}"> Blog</a></li>
       @endif
      </ul>
    </div>
  </div>
</nav>
</div>
</div>
</div>
   <?php 


        $bg_image = $user->background_image ?: url('public/main-page/images/ brown-framed-background.jpg');  ?>
   <div class="background-image" id="back-image" style = "
    background-image: url('{{$bg_image}}');
    padding: 98px 0 144px 0;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    margin-top: 77px;
"
>
     @include('admin.layouts.notifications')
    <div class="img-goood" style = "visibility: hidden;">
    <h2>@if(!empty($user->name)) {{$user->name}} @else N/A @endif</h2>
    <img src="{{url('public/main-page/images/dots-small.png')}}" class="dots" alt="" >
  </div>
</div>
<div class="background-image-new">
  <div class="New-describe-section new-photo ">
    <figure>
      @if(!empty($user->profile))
          <img src="{{$user->profile}}" alt="" />
        @else
        <img src="{{url('public/main-page/images/photo-img.png')}}" alt="">
         @endif
     </figure>
      <h1>@if(!empty($user->name)) {{$user->name}} @else N/A @endif</h1>
      <h2>@if(!empty($category->name)) {{$category->name}} @else N/A @endif</h2>
      <p> @if(!empty($user->description)) {{$user->description}} @else N/A @endif</p>

  </div>
</div>
  <div class="portfolio new">
    <div class="container">
      <div class="sub-heading">
      <p>Portfolio</p>
      <h4>My Photography</h4>
  </div>
  <div class="new-file">
   <!--  <ul>
      <li class="active">All</li>
      <li>Fashion</li>
      <li>Lifestyle</li>
      <li>Portraits</li>
      <li>Nature </li>
      <li>Other</li>

    </ul> -->
<!--     <div class="camera-img">
      <img src="{{url('public/main-page/images/camera-potofolio.png')}}" alt="">
            <img src="{{url('public/main-page/images/camera-grey.png')}}" alt="">

    </div> -->
</div>
</div>
<div class="gallary">
  <div class="container">
    <div class="wrapper">
  
<div class="row">
  <div class="images-gallery">
  @foreach($portfolio as $portfolios)

  <div class="col-md-4 no-padding-right col-sm-4 current overlay-container">
        <div class="over-view">
@if(!empty($portfolios->profile))
 

         <?php
                            $video_ext = ["mp4","avi","3gp","flv","mov","video"];
                            $get_file = $portfolios->profile;
                            $thumbnail_name = $portfolios->thumbnail_name;
                            $has_video = false;
                            $has_file = false;
                            $get_file_type = pathinfo($get_file,PATHINFO_EXTENSION);
                            $get_file_type = strtolower($get_file_type);

                            if(in_array($get_file_type,$video_ext)){
                              echo '<div class="new"><video src="'.$get_file.'"  controls="" "  poster="'.$thumbnail_name.'"></video></div>';
                            }else {
                              echo '<div class="new"><img src="'.$get_file.'" "/></div>';
                            }
                             ?>
                            @else
                             <img src="{{url('public/admin/production/images/user.png')}}" alt="blank_" >
                            @endif
<!-- <img src="{{url('public/main-page/images/gallary2.png')}}" alt=""> -->

</div>
</div>
@endforeach
</div>

</div>
</div>
</div> 
</div>

  

<section class="footer">
         <div class="container">
         <div class="col-md-5 col-xs-4">
            <div class="new-file">
               <figure>
                  <img src="{{url('public/landing/images/logo-footer.png')}}" alt="">
               </figure>
               <p class="home-left">The only influencer marketing platform in Kenya that ties on and offline influence to sales through collaboration with the right influencers.</p>
                
            </div>
         </div>
         <div class="col-md-3 col-xs-4">
            <div class="new-good">
               <h2>Useful Links</h2>
               <ul>
                  <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                  <li><a href="{{url('/policy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/terms-conditions')}}">Terms and Conditions</a></li>
                  <li><a href="{{url('/faq')}}">FAQ</a></li>
               </ul>
            </div>
         </div>
         
         <div class="col-md-4 col-xs-4">
            <div class="new-good gone social">
               <h2>Contact Us</h2>
               <ul>
                  <li>
                    <a href="mailto:info@twiva.co.ke">  <figure><i class="fa fa-envelope" aria-hidden="true"></i></figure>info@twiva.co.ke</a>
                  </li>
                </br>
                 <li>
            <i class="fa fa-phone" aria-hidden="true"></i>
            &nbsp; 0708088114</li>
                 
               </ul>
            </div>
            <div class="new-good gone">
                  <h2>Social Media</h2>
                  <ul>
                     <li><a href = "https://www.facebook.com/twiva/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href = "https://twitter.com/Twiva_influence" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href = "https://www.instagram.com/twiva_influence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
         </div>
         <div class="Copyright">
            <p>  Copyright ©  {{date('Y')}} Twiva. All Rights Reserved.</p>
         </div>
      </section>
  <div class="go-top active" id="backToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i>
<i class="fa fa-chevron-up"></i></div>



<!-- new-home -->





</main>

<script type="text/javascript" src="{{url('public/landing/js/jquery.min.js')}}"></script>
<script src="{{url('public/landing/js/bootstrap.min.js')}}"></script>
 <script src="{{url('public/landing/js/aos.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
<script src="{{url('public/landing/assets/src/js/bounty.js')}}"></script>
<script src="{{url('public/landing/js/influencer-landing.min.js')}}"  type="text/javascript"></script>
<!-- <script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('nav.navbar').addClass("sticky");
    }
    else{
        $('nav.navbar').removeClass("sticky");
    }
});
</script> -->
<script type="text/javascript">
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#backToTop').fadeIn('slow');
        } else {
            $('#backToTop').fadeOut('slow');
        }
    });
    $('#backToTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("#nwes").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</script>


<script>
        setTimeout(function(){
            if($(".alert")){
                $(".alert").remove();
            }
        },5000);

    
   </script>
</body>
</html>
