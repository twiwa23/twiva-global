<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="shortcut icon" href="{{url('public/landing/images/fav.png')}}" type="image/x-icon">
</head>

<body>
    <!--Main Header-->
    <div class="main-header">
        <!--Top Navbar-->
        <header class="header-wrapper">
            <div class="nav-p">
                <nav class="navbar navbar-expand-md bg-dark navbar-dark">

                    <!-- Toggler/collapsibe Button -->
                    <button class="navbar-toggler" type="button">
                        <span><img src="{{ asset('assets/images/toggle-icon.svg') }}" alt=""></span>
                    </button>

                    <!-- Nav Logo -->
                    <a class="navbar-brand" href="/">
                        <img src="{{ asset('assets/images/logo.svg') }}" alt="">
                    </a>


                    <!-- Navbar links -->
                    <div class="" id="collapsibleNavbar">
                        <ul class="navbar-nav">
                            <li class="nav-item">

                                <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="/">Brands</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('influencer') ? 'active' : '' }}" href="/influencer">Influencers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('user/creators') ? 'active' : '' }}" href="{{ url('/user/creators') }}">Designers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('eshop') ? 'active' : '' }}" href="{{ url('/eshop') }}">Eshop</a>
                            </li>

                        </ul>

                        <div class="navbar-btn-wrap">
                            <?php
                                $auth_id = Auth::id();
                                $id = Request::segment(3);
                            ?>

                            @if(auth()->guard('business')->user())
                                <a href="https://business.twiva.co.ke/business/dashboard-2.php"><button class="login-btn">Dashboard</button></a>
                            @else
                                @if(!empty($auth_id))
                                    <a href="{{url('user/portfolio/'.$auth_id)}}"><button class="login-btn">Dashboard</button></a>
                                @else
                                    @if (request()->is('user/creators'))
                                        <a href="{{ url('user/login') }}"><button class="login-btn">Log In</button></a>
                                        <a href="{{ url('user/signup') }}"><button class="signup-btn">Sign Up</button></a>
                                    @else
                                        <a href="{{ config('app.business_subdomain_url') }}"><button class="login-btn">Log In</button></a>
                                        <a href="{{ config('app.business_subdomain_signup_url') }}"><button class="signup-btn">Sign Up</button></a>
                                    @endif
                                @endif

                            @endif
                        </div>
                    </div>

                </nav>
            </div>
        </header>

        @yield('content')


    <!--Main Footer-->
    <footer class="footer-wrapper">

        <div class="footer-section f-p">
            <div class="footer-lists">
                <h1 class="footer-list-heading"><img src="{{ asset('assets/images/icons/Union.svg') }}">TWIVA</h1>
                <a href="{{ url('about-us') }}" class="footer-list-item">About Us</a>
                {{-- <a href="{{ url('pricing') }}" class="footer-list-item">Pricing</a> --}}
                {{-- <a href="" class="footer-list-item">Banned Item</a> --}}
                <a href="{{ url('contact-us') }}" class="footer-list-item">Contact Us</a>
                <!-- <a href="{{ url('blog') }}" class="footer-list-item">Blog</a> -->
                <a href="http://18.189.102.113/twiva-blog/" class="footer-list-item">Blog</a>
                <!-- <a href="{{ url('faq') }}" class="footer-list-item">FAQ</a> -->
            </div>

            <div class="footer-lists">
                <h1 class="footer-list-heading">Legal</h1>
                <a href="{{ url('policy') }}" class="footer-list-item">Privacy Policy</a>
                <a href="{{ url('terms-conditions') }}" class="footer-list-item">Terms & Conditions</a>
                <a href="{{ url('banned') }}" class="footer-list-item">Banned Products</a>
            </div>

            <div class="footer-lists">
                <h1 class="footer-list-heading">Contact Us</h1>
                <a href="tel:+254708088114" class="footer-list-item"><img src="{{ asset('assets/images/icons/tel.svg') }}" alt=""> +254708088114</a>
                <a href="mailto:info@twiva.co.ke" class="footer-list-item"><img src="{{ asset('assets/images/icons/mail-icon.svg') }}" alt=""> info@twiva.co.ke</a>
                <a href="https://goo.gl/maps/Kyy7syGZmyyGCpPD7" target="_blank" class="footer-list-item d-flex" style="max-width: 280px;"><img src="{{ asset('assets/images/icons/address-icon.png') }}" alt=""> Kose Heights, Argwings Kodhek Road, Hurlingham, Nairobi, Kenya Office No. W.3.2B on 3rd Floor</a>
            </div>

            {{-- <div class="footer-lists">
                <h1 class="footer-list-heading"> Pricing</h1>
                <a href="{{ url('pricing') }}" class="footer-list-item">Pricing</a>
            </div> --}}

            {{-- <div class="footer-lists">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Your Message">
                    <div class="input-group-append">
                        <button class="btn" type="submit">Talk to us</button>
                    </div>
                </div>
            </div> --}}
        </div>

        <div class="copyright-section">
            <div class="copyright-heading">
                <p>Copyright © 2021 Twiva. All Rights Reserved.</p>
            </div>

            <ul class="social-icons">
                <li class="icon-list-item">
                    <a href="https://www.instagram.com/twiva_influence/" target="_blank"><img src="{{ asset('assets/images/icons/instagram.svg') }}" alt=""></a>
                </li>

                <li class="icon-list-item">
                    <a href="https://twitter.com/Twiva_influence" target="_blank"><img src="{{ asset('assets/images/icons/twitter.svg') }}" alt=""></a>
                </li>

                <li class="icon-list-item">
                    <a href="https://www.facebook.com/twiva/" target="_blank"><img src="{{ asset('assets/images/icons/facebook.svg') }}" alt=""></a>
                </li>

                <li class="icon-list-item" style="width:70px;">
                    <a href="https://api.whatsapp.com/send?phone=+254708088114" target="_blank">
                        <!-- <img src="{{ asset('assets/images/icons/whatsapp-black.svg') }}" alt=""> -->
                        <i class="fab fa-whatsapp"></i>
                        Talk to Us
                    </a>
                </li>

                {{-- <li class="icon-list-item">
                    <a href=""><img src="{{ asset('assets/images/icons/linkedin.svg') }}" alt=""></a>
                </li> --}}
            </ul>

        </div>
    </footer>








    <script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160597820-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-160597820-1');
    </script>

</body>

</html>
