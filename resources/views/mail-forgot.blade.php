
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
      <meta content="width=device-width" name="viewport">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">

      <title>Mail forgot</title>
   </head>
   <body>
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="background-image:url({{url('public/main-page/images/vetify-bg.png')}}); max-width: 100%;">

         <tr>
         <td width="20" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="    box-shadow: 3px 2px 18px 0px rgba(203, 210, 215, 0.4);
    padding: 10px 0px;">
               <tr>
                  <td width="20" align="left" valign="top">&nbsp;</td>
                  <td align="center" valign="top" style="padding:2px 0;">
                    <img src="{{url('public/main-page/images/login-logo.png')}}" alt="" style ="width:75px;">
                  </td>
                  <td width="20" align="left" valign="top">&nbsp;</td>
               </tr>
            </table>
         </td>
      </tr>
      <tr>
   <td>&nbsp;</td>
</tr>
<tr>
   <td>&nbsp;</td>
</tr>
<tr>
   <td>&nbsp;</td>
</tr>
      <tr>
         <td width="20" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td width="20" align="left" valign="top">&nbsp;</td>
                  <td align="center" valign="top" style="padding:2px 0;">
                    <img src="{{url('public/main-page/images/thankyou.png')}}" alt="">
                  </td>
                  <td width="20" align="left" valign="top">&nbsp;</td>
               </tr>
            </table>
         </td>
      </tr>
      <tr>
         <td align="center" style="    font-family: 'Poppins', sans-serif;
    font-size: 24px; color: #84939b; font-weight: 400;margin-top: 14px;">
            for signing up with Twiva.


         </td>
      </tr>
      <tr>
         <td height="1" align="left" valign="top" bgcolor=""></td>
      </tr>
      <tr>
         <td align="left" valign="top" style=" padding:30px 20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td align="left" valign="top">
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td align="center" valign="top" style="padding:20px 0;">
                            <img src="{{url('public/main-page/images/verify-mail.png')}}">
                           </td>
                        </tr>
                        <tr>
                           <td align="center" valign="top" style="font-family: 'Poppins', sans-serif;
font-size: 36px;color: #3a4145;font-weight: 500;text-align: center;">To procced, please reset your password

</td>
</tr>
<tr>
   <td>&nbsp;</td>
</tr>
<tr>
   <td>&nbsp;</td>
</tr>
<tr>
   <td>&nbsp;</td>
</tr>

 <tr>
     <td align="center" valign="top" style="font-size: 36px;color: #3a4145;font-weight: 500;"><a href = "<?php echo $url;?>  "class="btn btn-primary width-415" style="
       color: #fff;
    background-color: #00b3a7;
    border-color: #00b3a7;
    height: 55px;
    font-size: 22px;
    font-weight: 500;
    width: 100%;
    border: oldlace;
    text-transform: uppercase;
    border-radius: 3px;
    font-family: 'Poppins', sans-serif;
    text-decoration: none;
    padding: 18px 133px;

"
    >Reset Password
  </a>
</td>
</tr>
</table>
</td>
</tr>
                       
                  
            </table>
         </td>
      </tr>
   </body>
</html>