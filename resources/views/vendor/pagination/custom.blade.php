@if ($paginator->hasPages())
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="paginate_button previous disabled" id="example_previous">
                <a href="javascript:void(0);" aria-controls="example" data-dt-idx="0" tabindex="0">
                    <img src="{{ asset('assets/images/icons/button.svg') }}" alt="">
                </a>
            </li>
        @else
            <li class="paginate_button previous" id="example_previous">
                <a href="{{ $paginator->previousPageUrl() }}" aria-controls="example" data-dt-idx="0" tabindex="0">
                    <img src="{{ asset('assets/images/icons/button.svg') }}" alt="">
                </a>
            </li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <!-- <li class="paginate_button active">
                    <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a>
                </li> -->
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="paginate_button active">
                            <a href="javascript:void(0);" aria-controls="example" data-dt-idx="{{ $page }}" tabindex="0">{{ $page }}</a>
                        </li>
                    @else
                        <li class="paginate_button">
                            <a href="{{ $url }}" aria-controls="example" data-dt-idx="{{ $page }}" tabindex="0">{{ $page }}</a>
                        </li>
                        <!-- <li><a href="{{ $url }}">{{ $page }}</a></li> -->
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <li class="paginate_button next" id="example_next">
                <a href="{{ $paginator->nextPageUrl() }}" aria-controls="example" data-dt-idx="2" tabindex="0">
                    <img src="{{ asset('assets/images/icons/right-gray-arr.svg') }}" alt="">
                </a>
            </li>
        @else
            <li class="paginate_button next disabled" id="example_next">
                <a href="javascript:void(0);" aria-controls="example" data-dt-idx="2" tabindex="0">
                    <img src="{{ asset('assets/images/icons/right-gray-arr.svg') }}" alt="">
                </a>
            </li>
        @endif
    </ul>
@endif 
<style>
    .pagination {
        display: flex;
        justify-content: flex-start;
        align-items: center;
    }
</style>