<?php

    use Illuminate\Http\Request;

    /*
     |--------------------------------------------------------------------------
     | API Routes
     |--------------------------------------------------------------------------
     |
     | Here is where you can register API routes for your application. These
     | routes are loaded by the RouteServiceProvider within a group which
     | is assigned the "api" middleware group. Enjoy building your API!
     |
     */

    // Route::middleware('auth:api')->get('/user', function (Request $request) {
    //                                    return $request->user();
    //                                    });



    Route::group(["prefix" => "v1","namespace" => ("API\\v1")],function(){
                Route::post("nav-link","NavController@navSwitch");
                 /* Business api*/
                 Route::post("payment","BusinessController@payment");
                 Route::post("business-register","BusinessController@register");
                 Route::post("business-login","BusinessController@login");
                 //Route::post("logout","BusinessController@logout");
                 Route::post("forgot-password","BusinessController@forgotPassword");

                 /*Influencer api*/
                 Route::get('verify/{userId}/{link}', 'InfluencerController@verify');
                 Route::get('success', 'InfluencerController@success');
                 Route::post("influencer-login","InfluencerController@login");
                 Route::post("normal-signup","InfluencerController@normal_signup");
                 Route::post("normal-login","InfluencerController@normal_login");
                 Route::get("get-interest","InfluencerController@getInterest");
                 Route::get("ios_push","BusinessController@ios_push");
                 Route::get("test","BusinessController@test");
                 Route::post("check-refferal-code","BusinessController@checkRefferalCode");
                 Route::get("paymentProcess","BusinessController@paymentProcess");
                 Route::get("businessPaymentProcess","BusinessController@businessPaymentProcess");



                 //Route::group(["middleware" => 'auth:api'],function(){
                 //new api for chnagesin infulancer block system 10-6-20
                 Route::group(['middleware' =>['auth:api']],function() {
                              Route::get("influencer-get-profile","InfluencerController@getProfile");
                              Route::post("influencer-update-price","InfluencerController@updatePrice");
                              Route::post("share-post","InfluencerController@sharePost");
                              Route::post("influencer-add-rating","InfluencerController@addRating");
                              Route::post("influencer-update-profile","InfluencerController@updateProfile");
                              Route::post("influencer-update-social-detail","InfluencerController@updateSocialDetail");
                              Route::get("infulancer-listing","InfluencerController@infulancerListing");
                              Route::get("infulancer-detail/{infulancer_id}","InfluencerController@infulancerDetails");
                              Route::get("pending-post-list/{post_name?}","InfluencerController@pendingPostList");
                              Route::get("pending-gig-list/{gig_name?}","InfluencerController@pendingGigList");
                              Route::get("accepted-post-list","InfluencerController@acceptedPostList");
                              Route::get("accepted-gig-list","InfluencerController@acceptedGigList");
                              Route::post("update-interests","InfluencerController@updateIntersts");
                              Route::post("post-accept","InfluencerController@AcceptPostRequest");
                              Route::post("gig-accept","InfluencerController@AcceptGigRequest");
                              Route::post("post-reject","InfluencerController@RejectPostRequest");
                              Route::post("gig-reject","InfluencerController@RejectGigRequest");
                              Route::get("infulancer-get-rating-list","InfluencerController@getInfulancerRatingList");

                              Route::post("create-post-detail","InfluencerController@createPostDetail");
                              Route::post("check-in-request","InfluencerController@CheckInRequest");
                              Route::post("stop-check-in","InfluencerController@StopCheckIn");
                              Route::get("complete-post-list/{post_name?}","InfluencerController@completePostList");
                              Route::get("complete-gig-list/{gig_name}","InfluencerController@completeGigList");
                              Route::get("infulancer-account","InfluencerController@infulancerAccount");
                              Route::post("infulancer-post-details","InfluencerController@infulancerPostDetail");
                              Route::post("infulancer-gig-details","InfluencerController@infulancerGigDetail");
                              Route::post("logout","BusinessController@logout");
                              Route::get("get-timeline","BusinessController@getTimeline");
                              Route::post("timeline-like","BusinessController@TimelineLike");
                              Route::get("infulancer-notification-list","InfluencerController@getInfulancerNotificationList");
                              Route::post("business-change-password","BusinessController@changePassword");
                              });
                 Route::group(['middleware' =>['auth:api','checkBlockUser']],function() {
                              /* Business api*/
                              //Route::post("logout","BusinessController@logout");
                              Route::post("create-timeline","BusinessController@createTimeline");


                              Route::post("business-update-profile","BusinessController@updateProfile");
                              Route::get("business-profile","BusinessController@userDetail");
                              Route::post("create-post","BusinessController@createPost");
                              Route::post("create-gig","BusinessController@createGig");
                              Route::post("set-gig-price","BusinessController@setGigPrice");
                              Route::post("set-post-price","BusinessController@setPostPrice");
                              Route::get("accept-post-list/{post_name?}","BusinessController@acceptPostList");
                              Route::get("accept-gig-list/{gig_name?}","BusinessController@acceptGigList");
                              Route::get("reject-post-list/{id}","BusinessController@RejectPostList");
                              Route::get("reject-gig-list/{id}/{user_name?}","BusinessController@RejectGigList");
                              Route::post("resend-post-price","BusinessController@resendPostPrice");
                              Route::post("business-add-rating","BusinessController@addInfulancerRating");
                              Route::post("resend-gig-price","BusinessController@resendGigPrice");
                              Route::get("notification-list","BusinessController@notifiactionList");


                              Route::get("business-get-rating-list","BusinessController@getBusinessRatingList");
                              Route::post("accept-post-detail-list","BusinessController@AcceptPostDetail");
                              Route::post("reject-post-detail-list","BusinessController@RejectPostDetail");
                              Route::post("confirm-check-in-request","BusinessController@confirmCheckInRequest");

                              Route::post("post-reminder","BusinessController@postReminder");
                              Route::post("gig-reminder","BusinessController@gigReminder");
                              /*   Route::post("reject-gig-detail-list","BusinessController@RejectGigDetail");*/
                              Route::get("business-completed-post-list/{post_name}","BusinessController@completedPostList");
                              Route::get("business-completed-gig-list/{gig_name?}","BusinessController@completedGigList");
                              Route::post("update-mpesa-phone-number","BusinessController@updateMpesaPhoneNumber");
                              Route::post("business-post-details","BusinessController@busienssPostDetail");
                              Route::post("business-gig-details","BusinessController@busienssGigDetail");



                              /*Influencer api*/


                              });
                 });

    Route::group(["prefix" => "v2","namespace" => ("API\\v2")],function(){
                 /* Business api*/


                 Route::post("business-register","BusinessController@register");
                 Route::post("business-login","BusinessController@login");
                 //Route::post("logout","BusinessController@logout");
                 Route::post("forgot-password","BusinessController@forgotPassword");
                 //Route::match(['Get','Post'],'mpesaCallbackUrl','BusinessController@mpesaCallbackUrl');
                 /*Influencer api*/
                 Route::get('verify/{userId}/{link}', 'InfluencerController@verify');
                 Route::get('success', 'InfluencerController@success');
                 Route::post("influencer-login","InfluencerController@login");
                 Route::post("normal-signup","InfluencerController@normal_signup");
                 Route::post("normal-login","InfluencerController@normal_login");
                 Route::get("get-interest","InfluencerController@getInterest");
                 Route::get("ios_push","BusinessController@ios_push");
                 Route::get("test","BusinessController@test");
                 Route::post("check-refferal-code","BusinessController@checkRefferalCode");
                 Route::get("paymentProcess","BusinessController@paymentProcess");
                 //Route::group(["middleware" => 'auth:api'],function(){
                 //new api for chnagesin infulancer block system 10-6-20
                 Route::group(['middleware' =>['auth:api']],function() {

                              Route::get("influencer-get-profile","InfluencerController@getProfile");
                              Route::post("influencer-update-price","InfluencerController@updatePrice");
                              Route::post("share-post","InfluencerController@sharePost");
                              Route::post("influencer-add-rating","InfluencerController@addRating");
                              Route::post("influencer-update-profile","InfluencerController@updateProfile");
                              Route::post("influencer-update-social-detail","InfluencerController@updateSocialDetail");
                              Route::get("infulancer-listing","InfluencerController@infulancerListing");
                              Route::get("infulancer-detail/{infulancer_id}","InfluencerController@infulancerDetails");
                              Route::get("pending-post-list/{post_name?}","InfluencerController@pendingPostList");
                              Route::get("pending-gig-list/{gig_name?}","InfluencerController@pendingGigList");
                              Route::get("accepted-post-list","InfluencerController@acceptedPostList");
                              Route::get("accepted-gig-list","InfluencerController@acceptedGigList");
                              Route::post("update-interests","InfluencerController@updateIntersts");
                              Route::post("post-accept","InfluencerController@AcceptPostRequest");
                              Route::post("gig-accept","InfluencerController@AcceptGigRequest");
                              Route::post("post-reject","InfluencerController@RejectPostRequest");
                              Route::post("gig-reject","InfluencerController@RejectGigRequest");
                              Route::get("infulancer-get-rating-list","InfluencerController@getInfulancerRatingList");
                              Route::post("create-post-detail","InfluencerController@createPostDetail");
                              Route::post("check-in-request","InfluencerController@CheckInRequest");
                              Route::post("stop-check-in","InfluencerController@StopCheckIn");
                              Route::get("complete-post-list/{post_name?}","InfluencerController@completePostList");
                              Route::get("complete-gig-list/{gig_name?}","InfluencerController@completeGigList");
                              Route::get("infulancer-account","InfluencerController@infulancerAccount");
                              Route::post("infulancer-post-details","InfluencerController@infulancerPostDetail");
                              Route::post("infulancer-gig-details","InfluencerController@infulancerGigDetail");
                              Route::post("logout","BusinessController@logout");
                              Route::get("get-timeline","BusinessController@getTimeline");
                              Route::post("timeline-like","BusinessController@TimelineLike");
                              Route::get("infulancer-notification-list","InfluencerController@getInfulancerNotificationList");
                              Route::post("business-change-password","BusinessController@changePassword");
                              //ADD NEW API FOR NOTIFICATION  
                              Route::post("influencer-notify","BusinessController@sendBiddingNotification");
                              //change post status by abhinav
                              Route::post("add-bid","BusinessController@changeBidStatus");
                              Route::post("send-influencer-notification","sendInfluencerNotification@sendnotification");
                              });
                 Route::group(['middleware' =>['auth:api','checkBlockUser']],function() {
                              /* Business api*/
                              //Route::post("logout","BusinessController@logout");
                              Route::post("create-timeline","BusinessController@createTimeline");


                              Route::post("business-update-profile","BusinessController@updateProfile");
                              Route::get("business-profile","BusinessController@userDetail");
                              Route::post("create-post","BusinessController@createPost");
                              Route::post("create-gig","BusinessController@createGig");
                              Route::post("set-gig-price","BusinessController@setGigPrice");
                              Route::post("set-post-price","BusinessController@setPostPrice");
                              Route::get("accept-post-list/{post_name?}","BusinessController@acceptPostList");
                              Route::get("accept-gig-list/{gig_name?}","BusinessController@acceptGigList");
                              Route::get("reject-post-list/{id}","BusinessController@RejectPostList");
                              Route::get("reject-gig-list/{id}/{user_name?}","BusinessController@RejectGigList");
                              Route::post("resend-post-price","BusinessController@resendPostPrice");
                              Route::post("business-add-rating","BusinessController@addInfulancerRating");
                              Route::post("resend-gig-price","BusinessController@resendGigPrice");
                              Route::get("notification-list","BusinessController@notifiactionList");


                              Route::get("business-get-rating-list","BusinessController@getBusinessRatingList");
                              Route::post("accept-post-detail-list","BusinessController@AcceptPostDetail");
                              Route::post("reject-post-detail-list","BusinessController@RejectPostDetail");
                              Route::post("confirm-check-in-request","BusinessController@confirmCheckInRequest");

                              Route::post("post-reminder","BusinessController@postReminder");
                              Route::post("gig-reminder","BusinessController@gigReminder");
                              /*   Route::post("reject-gig-detail-list","BusinessController@RejectGigDetail");*/
                              Route::get("business-completed-post-list/{post_name?}","BusinessController@completedPostList");
                              Route::get("business-completed-gig-list/{gig_name?}","BusinessController@completedGigList");
                              Route::post("update-mpesa-phone-number","BusinessController@updateMpesaPhoneNumber");
                              Route::post("business-post-details","BusinessController@busienssPostDetail");
                              Route::post("business-gig-details","BusinessController@busienssGigDetail");
                              Route::post("callback-transaction","BusinessController@getCallBackTransaction");
                              Route::post("payment","BusinessController@payment");
                              //By Vikash Payment API
                              Route::post("transaction/confirmation" , "BusinessController@paymentCallback");
                              Route::post("check-transaction-status" , "BusinessController@checkTransactionStatus");

                              /*Influencer api*/
                              //Invite For All Influencers
                              Route::post('let-influencers-bid' , 'BusinessController@letInfluencerBid');
                              Route::post("influencer-bid","InfluencerController@influencerBid");
                              Route::get("influencers-bid/{post_id}","BusinessController@influencersBid");
                              Route::post('bid-invite' , 'BusinessController@bidInvite');
                              });
                 });


