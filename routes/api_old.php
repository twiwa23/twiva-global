<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(["prefix" => "v1","namespace" => ("API\\v1")],function(){
	/* Business api*/
	Route::post("payment","BusinessController@payment");
	Route::post("business-register","BusinessController@register");
	Route::post("business-login","BusinessController@login");
	//Route::post("logout","BusinessController@logout");
	Route::post("forgot-password","BusinessController@forgotPassword");

	/*Influencer api*/
    Route::get('verify/{userId}/{link}', 'InfluencerController@verify');
    Route::get('success', 'InfluencerController@success');
	Route::post("influencer-login","InfluencerController@login");
	Route::post("normal-signup","InfluencerController@normal_signup");
    Route::post("normal-login","InfluencerController@normal_login");
	Route::get("get-interest","InfluencerController@getInterest");
     Route::get("ios_push","BusinessController@ios_push");
	 Route::get("test","BusinessController@test");
	 Route::post("check-refferal-code","BusinessController@checkRefferalCode");
	//Route::group(["middleware" => 'auth:api'],function(){
	Route::group(['middleware' =>['auth:api','checkBlockUser']],function() {
		/* Business api*/
    Route::post("logout","BusinessController@logout");
    Route::post("create-timeline","BusinessController@createTimeline");
    Route::get("get-timeline","BusinessController@getTimeline");
    Route::post("timeline-like","BusinessController@TimelineLike");
	Route::post("business-change-password","BusinessController@changePassword");
	Route::post("business-update-profile","BusinessController@updateProfile");
	Route::get("business-profile","BusinessController@userDetail");
	Route::post("create-post","BusinessController@createPost");
    Route::post("create-gig","BusinessController@createGig");
    Route::post("set-gig-price","BusinessController@setGigPrice");
    Route::post("set-post-price","BusinessController@setPostPrice");
    Route::get("accept-post-list","BusinessController@acceptPostList");
    Route::get("accept-gig-list","BusinessController@acceptGigList");
    Route::get("reject-post-list/{id}","BusinessController@RejectPostList");
    Route::get("reject-gig-list/{id}","BusinessController@RejectGigList");
    Route::post("resend-post-price","BusinessController@resendPostPrice");
    Route::post("business-add-rating","BusinessController@addInfulancerRating");
    Route::post("resend-gig-price","BusinessController@resendGigPrice");
    Route::get("notification-list","BusinessController@notifiactionList");
    Route::get("infulancer-notification-list","InfluencerController@getInfulancerNotificationList");
    
    Route::get("business-get-rating-list","BusinessController@getBusinessRatingList");
    Route::post("accept-post-detail-list","BusinessController@AcceptPostDetail");
    Route::post("reject-post-detail-list","BusinessController@RejectPostDetail");
    Route::post("confirm-check-in-request","BusinessController@confirmCheckInRequest");

    Route::post("post-reminder","BusinessController@postReminder");
    Route::post("gig-reminder","BusinessController@gigReminder");
 /*   Route::post("reject-gig-detail-list","BusinessController@RejectGigDetail");*/
    Route::get("business-completed-post-list","BusinessController@completedPostList");
    Route::get("business-completed-gig-list","BusinessController@completedGigList");
    Route::post("update-mpesa-phone-number","BusinessController@updateMpesaPhoneNumber");
    Route::post("business-post-details","BusinessController@busienssPostDetail");
    Route::post("business-gig-details","BusinessController@busienssGigDetail");
 
 
	/*Influencer api*/
	Route::get("influencer-get-profile","InfluencerController@getProfile");
	Route::post("influencer-update-price","InfluencerController@updatePrice");
    Route::post("share-post","InfluencerController@sharePost");
	Route::post("influencer-add-rating","InfluencerController@addRating");
    Route::post("influencer-update-profile","InfluencerController@updateProfile");
    Route::post("influencer-update-social-detail","InfluencerController@updateSocialDetail");
    Route::get("infulancer-listing","InfluencerController@infulancerListing");
    Route::get("infulancer-detail/{infulancer_id}","InfluencerController@infulancerDetails");
    Route::get("pending-post-list","InfluencerController@pendingPostList");
    Route::get("pending-gig-list","InfluencerController@pendingGigList");
    Route::get("accepted-post-list","InfluencerController@acceptedPostList");
    Route::get("accepted-gig-list","InfluencerController@acceptedGigList");
    Route::post("update-interests","InfluencerController@updateIntersts");
    Route::post("post-accept","InfluencerController@AcceptPostRequest");
	Route::post("gig-accept","InfluencerController@AcceptGigRequest");
	Route::post("post-reject","InfluencerController@RejectPostRequest");
	Route::post("gig-reject","InfluencerController@RejectGigRequest");
	Route::get("infulancer-get-rating-list","InfluencerController@getInfulancerRatingList");
	Route::post("create-post-detail","InfluencerController@createPostDetail");
	Route::post("check-in-request","InfluencerController@CheckInRequest");
	Route::post("stop-check-in","InfluencerController@StopCheckIn");
	Route::get("complete-post-list","InfluencerController@completePostList");
	Route::get("complete-gig-list","InfluencerController@completeGigList");
    Route::get("infulancer-account","InfluencerController@infulancerAccount");
    Route::post("infulancer-post-details","InfluencerController@infulancerPostDetail");
    Route::post("infulancer-gig-details","InfluencerController@infulancerGigDetail");

	});
});
