<?php
Route::match(['GET','POST'],'make-mpesa-payment','TwitterTrendController@businessPayment');

Route::match(['GET','POST'],'set-mpesa-number','TwitterTrendController@setMpesaPhone');

Route::match(['GET','POST'],'create-gig-filed-2','TwitterTrendController@createGigFiled2');

Route::match(['GET','POST'],'create-gig-field','TwitterTrendController@createGigField');
Route::match(['GET','POST'],'invite-influencers-gig/{gig_id}/{type?}','TwitterTrendController@inviteinfluencersGig');
Route::match(['GET','POST'],'get-influencers-gig/{gig_id}/{type?}','TwitterTrendController@getInfluencersGig');
Route::match(['GET','POST'],'create-gig','TwitterTrendController@createGig');
Route::match(['GET','POST'],'create-gig-desc','TwitterTrendController@createGigDesc');
Route::match(['GET','POST'],'gig-list','TwitterTrendController@gigList');
Route::match(['GET','POST'],'influencers-detail','TwitterTrendController@influencersDetails');
Route::match(['GET','POST'],'gig-details/{id}/{type?}','TwitterTrendController@gigDetails');
Route::match(['GET','POST'],'gig-filter/{id}','TwitterTrendController@gigfilter');
Route::match(['GET','POST'],'gig-summary-page/{id}','TwitterTrendController@gigSummaryPage');
Route::match(['GET','POST'],'accept-gig/{gig_id}','TwitterTrendController@acceptGig');
Route::match(['GET','POST'],'complete-accept-gig/{gig_id}','TwitterTrendController@completeAcceptGig');
 Route::match(['GET','POST'],'gig-reminder/{gig_id}/{influencer_id}','TwitterTrendController@gigReminder');
Route::match(['GET','POST'],'pending-gig-list/{gig_id}','TwitterTrendController@pendingGigList');
Route::match(['GET','POST'],'pending-gig/{gig_id}','TwitterTrendController@pendingGig');
Route::match(['GET','POST'],'declined-gig/{gig_id}','TwitterTrendController@declinedGig');
Route::match(['GET','POST'],'re-submit-gig/{gig_id}','TwitterTrendController@resubmitGig');
Route::match(['GET','POST'],'accept-gig-details/{gig_id}','TwitterTrendController@acceptGigDetails');

Route::match(['GET','POST'],'gig-complete-list','TwitterTrendController@getGigCompleteList');

Route::match(['GET','POST'],'gig-checkin-confirm/{id}','TwitterTrendController@gigCheckInConfirm');
Route::post('confirm-check-in-request/{gig_id}/{influencer_id}' ,'TwitterTrendController@confirmCheckInRequest');