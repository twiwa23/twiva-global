<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */
    Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');

   return " Cache is cleared!";

   });
    Route::get('storage-link',function(){
        \Artisan::call('storage:link');
        echo "linked";
    });
    Route::get("admin_/migrate",function(){
        \Artisan::call("migrate");
        return "migrate successfully";
    });

    Route::get("admin_/migrate-back-roll",function(){
        \Artisan::call("migrate:rollback");
        return "migrate rollback successfully";
    });

    Route::get("admin_/seeder_db",function(){
        \Artisan::call("db:seed");
        return "database seed successfully";
    });
    Route::match(['Get','Post'],'send-contact-us',"Admin\ProfileController@contactUs");

    Route::group(['namespace' => 'Admin','prefix'=>'user', 'as' => 'landing'], function() {
    Route::match(['Get','Post'],'login',"ProfileController@userLogin");
    Route::match(['Get','Post'],'signup',"ProfileController@userSignup");
    Route::get('verify/{link}', 'ProfileController@verify');
    Route::post('check-exist-email', 'ProfileController@checkEmail');
    Route::post('check-exist-name', 'ProfileController@checkName');
    Route::match(['Get','Post'],'forgot-password',"ProfileController@landingForgotPassword");
    Route::match(['Get','Post'],'reset-password/{token?}',"ProfileController@landingResetPassword");
    Route::get('password-reset-success','ProfileController@viewMessageResetPassword')->name('passwordResetInvalid');
    Route::get('reset-password-invalid','ProfileController@resetPasswordInvalid')->name('passwordResetInvalid');
     });

    Route::group(['middleware' =>['web'],'namespace' => 'Admin','prefix'=>'user', 'as' => 'landing'], function() {
    Route::get('userlogout','ProfileController@userlogout');
    Route::match(['Get','Post'],'edit-profile',"ProfileController@updateProfile");
    Route::match(['Get','Post'],'portfolio/{id?}','ProfileController@portfolio');
    Route::get('view-portfolio/{id?}','ProfileController@viewPortfolio');
    Route::get('creators','ProfileController@creators');
    Route::group(['middleware' =>['creator']], function() {
        Route::match(['Get','Post'],'add-services',"ProfileController@addServices");
        Route::match(['Get','Post'],'add-rate',"ProfileController@addRate");

    });
    Route::match(['Get','Post'],'add-blogger',"ProfileController@addBlogger");
    Route::get('view-blogger/{id?}','ProfileController@viewBlogger');
    Route::get('view-blogger-detail/{id?}','ProfileController@viewBloggerDetail');
    Route::match(['Get','Post'],'add-portfolio',"ProfileController@addPortfolio");
    Route::match(['Get','Post'],'hire-me/{id?}',"ProfileController@hireMe");

    });
    Route::group(['namespace' => 'Admin', 'as' => 'landing'], function() {

    Route::get('/','ProfileController@landingPage');
    Route::get('contact-us','ProfileController@contactUs');
    Route::get('faq','ProfileController@faq')->name('faqs');
    Route::get('influencer','ProfileController@influencer');
    Route::get('policy','ProfileController@policy');
    Route::get('terms-conditions','ProfileController@termsConditions');
    Route::get('about-us','ProfileController@aboutUs')->name('about-us');
    //Route::get('blog','ProfileController@blog');
    Route::get('eshop','ProfileController@eshop');
    Route::get('view-blog/{id}','banned@viewBlog');
    Route::get('banned' , function(){
        return view('landing/banned');
    });
    Route::get('pricing', function() {
        return view('landing.pricing');
    })->name('pricing');
    });

    Route::match(['Get','Post'],'mpesaCallbackUrl/{uid}','API\v1\BusinessController@mpesaCallbackUrl');
    Route::match(['Get','Post'],'mpesaCallbackUrlBusiness/{uid}','Business\BusinessController@mpesaCallbackUrlBusiness');
    //By vikash MpesaCallBack URL
    Route::post('mpesaCallbackUrl','API\v2\BusinessController@paymentCallback');

    Route::match(['Get','Post'],'business/get-business-payment-status/{uid}','Business\BusinessController@getBusinessPaymentStatus');

    Route::match(['Get','Post'],'get-password/{token}','API\v1\BusinessController@resetPassword');
    Route::get('verify/{link}', 'API\v1\BusinessController@verify');

    Route::get('success', 'API\v1\InfluencerController@success');
    Route::get('verify/{link}', 'API\v2\BusinessController@verify');

    Route::get('success', 'API\v2\InfluencerController@success');
    Route::group(['namespace' => 'Admin','prefix'=>'admin', 'as' => 'admin'], function() {
    Route::get('landing-page','ProfileController@landingPage');
    Route::match(['Get','Post'],'/','ProfileController@login');
    Route::match(['Get','Post'],'login','ProfileController@login')->name('login');
    Route::match(['Get','Post'],'forgot-password','ProfileController@forgotPassword')->name('forgotPassword');
    Route::match(['Get','Post'],'reset-password/{token?}','ProfileController@resetPassword')->name('resetPassword');
    Route::get('password-reset-success','ProfileController@viewMessageResetPassword')->name('passwordResetInvalid');
    Route::get('reset-password-invalid','ProfileController@resetPasswordInvalid')->name('passwordResetInvalid');

    Route::group([ 'middleware'=>['admin'], 'as' => 'admin'] , function() {

    Route::get('logout','ProfileController@logout');
    Route::get('index','ProfileController@index')->name('index');
    Route::match(['Get','Post'],'change-password','ProfileController@changePassword');
    Route::match(['Get','Post'],'edit-email','ProfileController@viewEmail');
    Route::match(['Get','Post'],'manage-email','ProfileController@updateEmail');

#################################### customer Management ###########################

    Route::get('influencer-Management','AdminController@influencerManagement');
    Route::match(['Get','Post'],'view-influencer/{id}','AdminController@viewInfluencer');
    Route::match(['Get','Post'],'edit-influencer/{id}','AdminController@editInfluencer');
    Route::post('delete-infulancer','AdminController@deleteInfluencer');
    Route::post('delete-business','AdminController@deleteBusiness');
    Route::get('business','AdminController@business');
    Route::get('view-business/{id}','AdminController@viewBusiness');

##################################################################
    Route::get('creators-Management','AdminController@creatorsManagement');
    Route::match(['Get','Post'],'view-creators/{id}','AdminController@viewCreators');
    Route::match(['Get','Post'],'edit-influencer/{id}','AdminController@editInfluencer');
    Route::post('delete-creators','AdminController@deleteCreators');
    ##################################################################
    Route::get('portfolio-management/{id}','AdminController@portfolioManagement');
    Route::match(['Get','Post'],'view-portfolio/{id}','AdminController@viewPortfolio');
    Route::post('delete-portfolio','AdminController@deletePortfolio');
    ##################################################################
    Route::get('services-management/{id}','AdminController@servicesManagement');
    Route::match(['Get','Post'],'view-services/{id}','AdminController@viewServices');
    Route::post('delete-services','AdminController@deleteServices');
    ##################################################################
    Route::get('blogger-management/{id}','AdminController@bloggerManagement');
    Route::match(['Get','Post'],'view-bloggers/{id}','AdminController@viewBlogger');
    Route::post('delete-blogger','AdminController@deleteBlogger');
    ##################################################################
    Route::get('rate-management/{id}','AdminController@rateManagement');
    Route::match(['Get','Post'],'view-rate/{id}','AdminController@viewRate');
    Route::post('delete-rate','AdminController@deleteRate');
//--------------------------------------------------------//
    Route::get('subadmin-management','AdminController@subadminManagement');
    Route::match(['Get','Post'],'add-subadmin','AdminController@addsubAdmin');
    Route::match(['Get','Post'],'view-subadmin/{id}','AdminController@viewsubAdmin');
    Route::match(['Get','Post'],'edit-subadmin/{id}','AdminController@editsubAdmin');
    Route::post('delete-subadmin','AdminController@deletesubAdmin');

    Route::get('refferal-list','AdminController@refferalList');
    Route::get('view-refferal/{id}','AdminController@viewRefferal');

    Route::match(['Get','Post'],'block-bussiness/{id}','AdminController@blockBusiness');
    Route::match(['Get','Post'],'block-influencer/{id}','AdminController@blockInfluencer');
    Route::match(['Get','Post'],'blockInfluencer/{id}','AdminController@editBusiness');
    Route::match(['Get','Post'],'edit-bussiness-contact/{id?}/{user_id?}','AdminController@editBusinessContact');
    Route::get('gigs','AdminController@Gigs');
    Route::match(['Get','Post'],'view-gigs/{id}','AdminController@viewGigs');
    Route::match(['Get','Post'],'edit-business/{id}','AdminController@editBusiness');
    Route::match(['Get','Post'],'plug','AdminController@Plug');
    Route::match(['Get','Post'],'view-plug/{id}','AdminController@viewPlug');
    Route::match(['Get','Post'],'payments','AdminController@payments');
    Route::match(['Get','Post'],'featured','AdminController@featured');
    Route::match(['Get','Post'],'edit-featured/{id}','AdminController@editFeatured');
    Route::match(['Get','Post'],'add-featured','AdminController@addFeatured');
    Route::match(['Get','Post'],'interests','AdminController@interests');
    Route::match(['Get','Post'],'edit-interests/{id}','AdminController@editInterests');
    Route::match(['Get','Post'],'add-interests','AdminController@addInterests');
    Route::get('view-interests/{id}','AdminController@viewInterests');
    Route::post('delete-interests','AdminController@deleteInterests');
    Route::get('view-featured/{id}','AdminController@viewFeatured');
    Route::post('delete-blog','AdminController@deleteBlog');
    Route::match(['Get','Post'],'blog','AdminController@blog');
    Route::match(['Get','Post'],'add-blog','AdminController@addBlog');
    Route::get('view-blog/{id}','AdminController@viewBlog');
    Route::post('delete-user-post','AdminController@deleteUserPost');
    Route::match(['Get','Post'],'user-post','AdminController@userPost');
    Route::get('view-user-post/{id}','AdminController@viewUserPost');
    Route::get('view-twiva-updates-list/{id}','AdminController@viewTimeline');
    Route::match(['Get','Post'],'twiva-updates-list','AdminController@getTimeline');
    Route::match(['Get','Post'],'add-twiva-updates','AdminController@createTimeline');
    Route::get('influencer-payment','AdminController@influencerPayment');
    Route::get('business-payment','AdminController@businessPayment');
    Route::match(['GET','POST'],'view-business-payment/{id}','AdminController@viewBusienssPayment');
    Route::match(['GET','POST'],'view-influencer-payment/{id}','AdminController@viewInfluencerPayment');

     Route::match(['GET','POST'],'add-post-price/{id}','AdminController@addPostPrice');
     Route::match(['GET','POST'],'add-gig-price/{id}','AdminController@addGigPrice');

     Route::get('accepted-gig-list','AdminController@acceptedGigList');
      Route::get('accepted-post-list','AdminController@acceptedPostList');
       Route::get('completed-gig-list','AdminController@completedGigList');
        Route::get('completed-post-list','AdminController@completedPostList');
        Route::match(['Get','Post'],'hashtag','AdminController@hashTag');
    // Route::match(['GET','POST'],'paymentInfluencer','AdminController@influenecerPay');
});
});


Route::match(['GET','POST'],"callback",function(){
    return $_REQUEST;
});



// Business
Route::group(['namespace' => 'Business','prefix'=>'business'], function() {

    Route::get('navChange', 'NewBusinessController@navChange');
    // Route::get('navChange', function() {
    //     return "hi";
    // });
    Route::get('/landing', function() {
        return redirect('/');
    });
    Route::post('navChangeToNew', 'NewBusinessController@navChangeToNew')->name('navChangeToNew')->middleware('business');


    Route::match(['GET','POST'],'login','BusinessProfileController@login');
    Route::match(['Get','Post'],'forgot-password','BusinessProfileController@forgotPassword')->name('forgotPassword');
    Route::match(['Get','Post'],'reset-password/{token?}','BusinessProfileController@resetPassword')->name('resetPassword');
    Route::get('password-reset-success','BusinessProfileController@viewMessageResetPassword')->name('passwordResetInvalid');
    Route::get('reset-password-invalid','BusinessProfileController@resetPasswordInvalid')->name('passwordResetInvalid');
    Route::match(['GET','POST'],'check-exist-email', 'BusinessProfileController@checkEmail');
    Route::match(['GET','POST'],'signup','BusinessProfileController@signUp');
    Route::group([ 'middleware'=>['business'], 'as' => 'business'] , function() {
        Route::get('logout','NewBusinessController@businesslogout');
        // Route::get('logout','BusinessProfileController@businesslogout');
        Route::match(['GET','POST'],'change-password','BusinessProfileController@changePassword');
        Route::match(['GET','POST'],'update-personal-details','BusinessProfileController@updatePersonalDetails');
        // Route::match(['GET','POST'],'update-mpsa-account','BusinessProfileController@updateMpsaDetails');
        Route::match(['GET','POST'],'update-mpsa-account','NewBusinessController@updateMpsaDetails');

        Route::get('hire', function() {
            return view('business.pages.influencers');
            // return auth()->guard('business')->user();
        });
        //---------------------------post start----------------------------//

        Route::match(['GET','POST'],'create-post','BusinessController@createPost');
        Route::match(['GET','POST'],'create-post2','BusinessController@createPost2');

        Route::post('upload-post', 'NewBusinessController@storePost');


        Route::match(['GET','POST'],'create-post-field','BusinessController@createPostField');
        Route::match(['GET','POST'],'create-post-desc','BusinessController@createPostDesc');
        Route::match(['GET','POST'],'invite-influencers-post/{post_id}','BusinessController@inviteInfluencersPost');
        Route::match(['GET','POST'],'influencers-list','BusinessController@influencersList');
        Route::match(['GET','POST'],'fetch-influencers','BusinessController@fetchInfluencers');
        Route::match(['GET','POST'],'influencer-details/{id}','BusinessController@influencersDetails');

        Route::get('posts', 'NewBusinessController@posts');
        Route::match(['GET','POST'],'post-list','BusinessController@postList');
        Route::match(['GET','POST'],'complete-post-list','BusinessController@completedPostList');
        Route::match(['GET','POST'],'post-details/{post_id}','NewBusinessController@postDetails');
        Route::match(['GET','POST'],'get-post-details/{post_id}','NewBusinessController@getPostDetails');
        Route::match(['GET','POST'],'complete-post-details/{post_id}','BusinessController@completePostDetails');
        Route::match(['GET','POST'],'pending-post-list/{post_id}','BusinessController@pendingPostList');
        Route::match(['GET','POST'],'pending-post','BusinessController@pendingPost');
        Route::match(['GET','POST'],'declined-post/{post_id}','BusinessController@declinedPost');
        // Route::match(['GET','POST'],'accept-post-details/{id}','BusinessController@acceptPostDetails');
        Route::match(['GET','POST'],'accept-post-details/{id}','NewBusinessController@acceptPostDetails');
        Route::match(['GET','POST'],'accept-post/{post_id}','NewBusinessController@acceptPost');
        Route::match(['GET','POST'],'edit-post/{post_id}','NewBusinessController@editPost');
        Route::match(['GET','POST'],'complete-accept-post/{post_id}','BusinessController@completeAcceptPost');
        Route::match(['GET','POST'],'upload-data/{post_id}/{influencer_id}','BusinessController@uploadData');
        Route::match(['GET','POST'],'post-reminder/{post_id}/{influencer_id}','BusinessController@postReminder');
        Route::match(['GET','POST'],'post-filter/{id}','BusinessController@postfilter');
        Route::match(['GET','POST'],'re-submit-post/{post_id}','BusinessController@resubmitPost');
        Route::match(['GET','POST'],'post-summary-page/{id}','BusinessController@postSummaryPage');

        include 'twitter_trend.php';
        Route::get('/what-is-twitter-trend' , function(){
            return view('business/pages/gigs/what-is-twitter-trend');
        });
        //-------------gig list------------------------------//
       /* Route::match(['GET','POST'],'make-mpesa-payment','BusinessController@businessPayment');

        Route::match(['GET','POST'],'set-mpesa-number','BusinessController@setMpesaPhone');

        Route::match(['GET','POST'],'create-gig-filed-2','BusinessController@createGigFiled2');

        Route::match(['GET','POST'],'create-gig-field','BusinessController@createGigField');
        Route::match(['GET','POST'],'invite-influencers-gig/{gig_id}','BusinessController@inviteinfluencersGig');
        Route::match(['GET','POST'],'create-gig','BusinessController@createGig');
        Route::match(['GET','POST'],'create-gig-desc','BusinessController@createGigDesc');
        Route::match(['GET','POST'],'gig-list','BusinessController@gigList');
        Route::match(['GET','POST'],'influencers-detail','BusinessController@influencersDetails');
        Route::match(['GET','POST'],'gig-details/{id}/{type?}','BusinessController@gigDetails');
        Route::match(['GET','POST'],'gig-filter/{id}','BusinessController@gigfilter');
        Route::match(['GET','POST'],'gig-summary-page/{id}','BusinessController@gigSummaryPage');
        Route::match(['GET','POST'],'accept-gig/{gig_id}','BusinessController@acceptGig');
        Route::match(['GET','POST'],'complete-accept-gig/{gig_id}','BusinessController@completeAcceptGig');
         Route::match(['GET','POST'],'gig-reminder/{gig_id}/{influencer_id}','BusinessController@gigReminder');
        Route::match(['GET','POST'],'pending-gig-list/{gig_id}','BusinessController@pendingGigList');
        Route::match(['GET','POST'],'pending-gig/{gig_id}','BusinessController@pendingGig');
        Route::match(['GET','POST'],'declined-gig/{gig_id}','BusinessController@declinedGig');
        Route::match(['GET','POST'],'re-submit-gig/{gig_id}','BusinessController@resubmitGig');
        Route::match(['GET','POST'],'accept-gig-details/{gig_id}','BusinessController@acceptGigDetails');

        Route::match(['GET','POST'],'gig-complete-list','BusinessController@getGigCompleteList');

        Route::match(['GET','POST'],'gig-checkin-confirm/{id}','BusinessController@gigCheckInConfirm');
        */
        //---------------------end of gig----------------------------//

        Route::match(['GET','POST'],'notification','BusinessController@notification');
        Route::match(['GET','POST'],'settings','BusinessController@settings');

        Route::get('creaters','NewBusinessController@creators');
        Route::get('portfolio/{id?}','NewBusinessController@portfolio');
        Route::match(['Get','Post'],'hire-me/{id?}',"NewBusinessController@hireMe");
        // Route::match(['Get','Post'],'post-rating/{id?}/{post_id}',"BusinessController@postRatingReview");
        Route::match(['Get','Post'],'post-rating/{id?}/{post_id}',"NewBusinessController@postRatingReview");
        Route::match(['Get','Post'],'gig-rating/{id?}/{gig_id}',"BusinessController@gigRatingReview");

    });
});

// Influencers
Route::group(['namespace' => 'Influencers','prefix'=>'influencers'], function() {
    Route::get('navChange', 'InfluencersController@navChange');
    Route::post('navChange', 'InfluencersController@navChangeToNew')->name('inavChangeToNew');
    Route::get('logout' , 'InfluencersController@influencersLogout');
    //My Invites Posts
    Route::group(['prefix'=>'/my-invites/posts/' , 'middleware'=>['business'], 'as' => 'business'] , function(){
        Route::match(['GET','POST'],'posts' , 'InfluencersController@myInvitesPosts' )->name('myInvitesPosts');
        Route::get('data' , 'InfluencersController@myInvitesPostsData');
        Route::get('details/{id}' , 'InfluencersController@myInvitesPostDetails');
        Route::get('accept/{post_id}' , 'InfluencersController@acceptPostInvites');
        Route::post('reject' , 'InfluencersController@rejectMyInvitesPost');
    });
    //My Invites Twitter Trends
    Route::group(['prefix'=>'/my-invites/twitter-trends/' , 'middleware'=>['business'], 'as' => 'business'] , function(){
        Route::match(['GET' , 'POST'] , 'twitter-trends' , 'InfluencersController@myInvitesTwitterTrends')->name('myInvitesTwitterTrends');
        Route::get('data' , 'InfluencersController@myInvitesTwitterTrendsData');
        Route::get('details/{id}' , 'InfluencersController@myInvitesTwitterTrendDetails');
        Route::get('accept/{gig_id}' , 'InfluencersController@acceptTwitterTrendInvites');
        Route::post('reject' , 'InfluencersController@rejectMyInvitesTwitterTrend');
    });

    //Accepted Posts
    Route::group(['middleware'=>['business'], 'as' => 'business' , 'prefix'=>'/accepted/posts/'] , function(){
        Route::match(['GET' , 'POST'] , 'posts' , 'InfluencersController@acceptedPosts')->name('influencers.accepted.posts');
        Route::get('data' , 'InfluencersController@acceptedPostsData');
        Route::get('details/{id}' , 'InfluencersController@acceptedPostDetails');
        Route::get('start-post/{id}' , 'InfluencersController@startPost');
        Route::post('send-for-approval' , 'InfluencersController@sendForApproval');
        Route::get('resubmit-post-view/{id}' , 'InfluencersController@resubmitPostView');
        Route::get('view-post/{id}' , 'InfluencersController@viewPost');
    });
    //Accepted Twitter Trends
    Route::group(['middleware'=>['business'], 'as' => 'business' , 'prefix'=>'/accepted/twitter-trends/'] , function(){
        Route::match(['GET' , 'POST'] , 'twitter-trends' , 'InfluencersController@acceptedTwitterTrends');
        Route::get('data' , 'InfluencersController@acceptedTwitterTrendsData');
        Route::get('details/{id}' , 'InfluencersController@acceptedTwitterTrendsDetails');
        Route::get('check-in/{id}' , 'InfluencersController@checkIn');
        Route::get('check-out/{id}' , 'InfluencersController@checkOut');
    });



});
Route::get('404' , function(){
    return view('404');
});



