<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Gig extends Model
{
    protected $fillable = [
        'gig_name', 'gig_start_date_time', 'gig_end_date_time','price_per_gig','description','what_to_do','user_id','created_at','updated_at','location','gig_complementry_item','profile','owner_name','phone_number','status'
    ];
  
      public function getprofileAttribute($value)
    {
        $profile = $value;
         $check_path = public_path('storage/uploads/images/business');
        $read_path   = url('public/storage/uploads/images/business');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

   public function Images(){
        return $this->hasMany("App\model\GigMedia");
    }
    public function user(){
        return $this->belongsTo("App\model\User");
    }

     public function price(){
        return $this->hasMany("App\model\GigPrice")->where('user_id',Auth::id());
    }
     public function gigUser(){
        return $this->hasMany("App\model\GigUser","gig_id")
                ->with('user')
                ->select("gig_users.*","gig_prices.price")
                ->join("gig_prices","gig_users.gig_id","=","gig_prices.gig_id");
    }
}

