<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class GigPrice extends Model
{
    protected $fillable = [
        'gig_id', 'user_id','price','status'
    ];
}
