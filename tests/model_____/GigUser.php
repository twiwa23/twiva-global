<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
class GigUser extends Model
{
   protected $fillable = [
        'user_id', 'gig_id', 'status'
    ];
	public function Gig(){
        return $this->belongsTo("App\model\Gig")->with("Images","user");
    }
	 public function Images(){
        return $this->belongsTo("App\model\GigMedia");
    }
	
    public function user(){
        return $this->belongsTo("App\model\User")->with('Images');
    }
}
