<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerInterests extends Model
{
    protected $table = "influencer_interests";

    protected $fillable = ["user_id","image"];

    public function getImageAttribute($value){
    	return  url("public/storage/uploads/images/influencer_images").'/'.$value;
    }
}
