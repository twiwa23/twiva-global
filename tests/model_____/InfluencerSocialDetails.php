<?php 

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InfluencerSocialDetails extends Model
{
    protected $table = "influencer_social_details";

    protected $fillable = [
    	"user_id",
    	"facebook_name",
    	"facebook_friends",
    	"twitter_name",
    	"twitter_friends",
    	"instagram_name",
    	"instagram_friends",
    ];

     
}

?>
