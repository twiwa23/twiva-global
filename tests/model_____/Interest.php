<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $table = "interests";

    protected $fillable = ["user_id","added_by","image","interest_name"];

    protected $hidden = ["user_id","added_by","laravel_through_key"];

    public function getImageAttribute($value){
    	return  url("public/assets/images").'/'.$value;
    }
   
       
}
