<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class InterestUser extends Model
{
    //
     protected $table = "influencer_interests";

    protected $fillable = [
    	"user_id",
    	"interest_id",
    	
    ];
    
}