<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'user_id', 'other_user_id', 'status','message'
    ];
     public function user(){
        return $this->belongsTo("App\model\User")->with("Images");
    }
}
