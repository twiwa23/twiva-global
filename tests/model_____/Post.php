<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Post extends Model
{
    protected $fillable = [
        'post_name', 'price_per_post','description','what_not_to_include','user_id','post_type','what_to_include','status'
    ];
   
    public function Images(){
        return $this->hasMany("App\model\PostImage");
    }

    public function postDetail(){
        return $this->hasMany("App\model\PostDetail")->with('postDetalsMedia');
    }

    public function user(){
    	return $this->belongsTo("App\model\User");
    }
     public function price(){
        return $this->hasMany("App\model\PostPrice")->where('user_id',Auth::id());
    }
    /* public function postUser(){
        return $this->hasMany("App\model\PostUser")
        		->with('user');
    }*/
    public function postUser(){
        return $this->hasMany("App\model\PostUser","post_id")
                ->with(['postDetail'/*=>function($query){
                    return $query->join("post_users",function($join){
                        return $join->on("post_users.post_id","=","post_details.post_id")
                                    ->on("post_details.user_id","=","post_users.user_id");
                                   
                    });
                }*/,'user'])
                ->select("post_users.*","post_prices.price")
                ->join("post_prices","post_users.post_id","=","post_prices.post_id");
    }
}
