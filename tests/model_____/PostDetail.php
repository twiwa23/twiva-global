<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PostDetail extends Model
{
    protected $fillable = [
        'user_id', 'post_id','text'
    ];

    public function media(){
        return $this->hasMany("App\model\PostDetailsMedia",'post_detail_id');
    }
}
