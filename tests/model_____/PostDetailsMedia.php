<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PostDetailsMedia extends Model
{
    protected $fillable = [
        'post_detail_id', 'media','thumbnail'
    ];

    public function getMediaAttribute($value)
    {
        $profile = $value;
        
         $check_path = public_path('storage/uploads/images/business_images/post_detail_image');
        $read_path   = url('public/storage/uploads/images/business_images/post_detail_image');
        //return $check_path.'/'.$profile;
            if(file_exists($check_path.'/'.$profile) && !empty($profile)){
                return $read_path.'/'.$profile;
            }else {
                return "";
            }
    }

}
