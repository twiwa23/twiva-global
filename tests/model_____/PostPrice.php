<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PostPrice extends Model
{
   protected $fillable = [
        'user_id', 'post_id', 'price','status'
    ];
}
