<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PostUser extends Model{

  protected $fillable = [
        'user_id', 'post_id', 'status'
    ];
	
    public function Post(){
        return $this->belongsTo("App\model\Post")->with("Images","user","postDetail");
    }

	public function Images(){
        return $this->belongsTo("App\model\PostImage");
    }

    public function user(){
    	return $this->belongsTo("App\model\User")->with(['Images']);
    }

    public function postDetail(){
        return $this->hasMany("App\model\PostDetail",'post_id',"post_id","extra_usefull_dont_remove")
        ->with("media")
        ->join("post_users",function($join){
            return $join->on("post_users.post_id","=","post_details.post_id")
                        ->on("post_details.user_id","=","post_users.user_id");
                       
        })
        ->whereColumn("post_details.user_id","post_users.user_id");
    }
    
}
