<?php

namespace App\model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\model\BusinessDetail;

class User extends Authenticatable 
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type','profile','country_code','phone_number','contact_person','facebook_id','instagram_id','twitter_id','status','is_deleted','country_code',"device_id","device_type"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password','is_deleted','status','user_type'
    ];

    protected $appends = ['businessDetail'];

    protected $guarded = ['businessDetail'];

    public function getProfileAttribute($value)
    {
        $profile = $value;

        $check_pathB = public_path('storage/uploads/images/business');
        $check_pathI = public_path('storage/uploads/images/influencer');

        $read_pathB   = url('public/storage/uploads/images/business');
        $read_pathI   = url('public/storage/uploads/images/influencer');

        $dummy_profile   = url('public/images/dummy_user.jpg');

        if($this->user_type == 'B'){
            if(file_exists($check_pathB.'/'.$profile) && !empty($profile)){
                return $read_pathB.'/'.$profile;
            }else {
                return $dummy_profile;
            }
        }else {
            if(file_exists($check_pathI.'/'.$profile) && !empty($profile)){
                return $read_pathI.'/'.$profile;
            }else {
                return $dummy_profile;
            }
        }
    }

    public function getBusinessDetailAttribute(){
        if($this->user_type == 'B'){
            return BusinessDetail::whereUserId($this->id)->select("business_detail","owner_name")->first();
        }
    }

    public function Images(){
        return $this->hasMany("App\model\InfluencerImages");
    }

    public function Interests(){
       return $this->hasManyThrough("App\model\Interest","App\model\InfluencerInterests","user_id","id","id","interest_id"); 
    }

    public function SocialDetail(){
        return $this->hasOne("App\model\InfluencerSocialDetails");
    }

    public function price(){
        return $this->hasOne("App\model\InfluencerPrice");
    }

    public function InfluencerDetail(){
        return $this->hasOne("App\model\InfluencerDetail");
    }

    /*Johny 21/11/2019 for post detail after influencer accept the post request*/
    public function postDetail(){
        return $this->hasOne("App\model\PostDetail",'user_id');
    }
    
    
 

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
