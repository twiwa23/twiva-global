-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2020 at 12:58 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twiva`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visible_pwd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgot_password_request` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'No',
  `reset_password_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone_number`, `password`, `visible_pwd`, `remember_token`, `login_token`, `forgot_password_request`, `reset_password_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'adminn@yopmail.com', NULL, '$2y$10$NjlIBcwLbuZMgryeK8JCc.Lx3jvsiKGtMjfdQRNW0NygVsZGrTK7y', '1234567', 'aQMtht1p5ddU46QqkQ1F3lc00Uq8dRTYCyJLGVVmLdDtfnwPvFK6Gx4SGs7B', 'RmWBVAkyrXXCOojzPpQKlyNtMd6ErJ35', 'Yes', '', NULL, '2020-01-20 06:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `admin_payments`
--

CREATE TABLE `admin_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `infulencer_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_payments`
--

INSERT INTO `admin_payments` (`id`, `user_id`, `amount`, `infulencer_id`, `post_id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 20, 1, 5, 'post', 1, '2020-01-18 04:45:18', '2020-01-18 04:45:18'),
(2, 4, 300, 1, 1, 'gig', 1, '2020-01-18 04:46:46', '2020-01-18 04:46:46'),
(3, 4, 20, 1, 23, 'post', 1, '2020-01-20 01:53:26', '2020-01-20 01:53:26'),
(4, 4, 20, 1, 24, 'post', 1, '2020-01-20 01:59:02', '2020-01-20 01:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `text`, `title`, `image`, `created_at`, `updated_at`) VALUES
(2, 'dfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdgdfgdfgdfgdfgdg', 'rftgfdgfdgdfgdfgdfg', '5e21addce25d3_brok.png', '2020-01-17 07:21:40', '2020-01-17 07:21:40'),
(3, 'sdfsfsdfsfsdfsff', 'vsdfdsf', '5e21ae3902243_image-12.jpg', '2020-01-17 07:23:13', '2020-01-17 07:23:13'),
(4, 'cvbvcbcb', 'cbvcvb', '5e21ae69ef542_du.png', '2020-01-17 07:24:01', '2020-01-17 07:24:01');

-- --------------------------------------------------------

--
-- Table structure for table `business_details`
--

CREATE TABLE `business_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `business_detail` text COLLATE utf8mb4_unicode_ci,
  `owner_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_details`
--

INSERT INTO `business_details` (`id`, `user_id`, `business_detail`, `owner_name`, `created_at`, `updated_at`) VALUES
(1, 3, 'This is new Business', 'Owner Aaaa', '2020-01-18 03:01:37', '2020-01-18 03:01:37'),
(2, 4, 'This is a new Business', 'Owmmmm', '2020-01-18 03:17:49', '2020-01-18 03:17:49'),
(3, 6, 'here is a new business let\'s test this with the great way.', 'Malik', '2020-01-20 01:19:48', '2020-01-20 01:19:48'),
(4, 8, 'This is a new user  tested  This is a new user  tested This is a new user  tested This is a new user  tested This is a new user  tested This is a new user  tested This is a new user  tested This is a new user  tested This is a new user  tested', '', '2020-01-20 05:29:13', '2020-01-20 05:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `featureds`
--

CREATE TABLE `featureds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `featureds`
--

INSERT INTO `featureds` (`id`, `image`, `created_at`, `updated_at`) VALUES
(2, '5e21aeade1894_crash.png', NULL, NULL),
(3, '5e21af20c8611_img_0091.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gigs`
--

CREATE TABLE `gigs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `gig_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gig_start_date_time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gig_end_date_time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_per_gig` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gig_complementry_item` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `what_to_do` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashtag` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gigs`
--

INSERT INTO `gigs` (`id`, `user_id`, `gig_name`, `gig_start_date_time`, `gig_end_date_time`, `price_per_gig`, `gig_complementry_item`, `profile`, `owner_name`, `phone_number`, `location`, `description`, `what_to_do`, `hashtag`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 'Newwwww', '2020-01-19 03:45', '2020-01-20 03:54', '300', 'Ertert ertert', '5e22daf98be29_profile.jpeg', 'Owwww', '123456789', 'dfgdgf', 'Dfgdfgdfg', 'Vb ghfghfgh fghf', '#wwfgdfgdfgbhhhh', '1', '2020-01-18 04:46:25', '2020-01-18 04:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `gig_details`
--

CREATE TABLE `gig_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `gig_id` bigint(20) UNSIGNED NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gig_media`
--

CREATE TABLE `gig_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gig_id` bigint(20) UNSIGNED NOT NULL,
  `media` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gig_media_details`
--

CREATE TABLE `gig_media_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gig_detail_id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gig_prices`
--

CREATE TABLE `gig_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `gig_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infulancer_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complementry_item` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_resend` int(50) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gig_prices`
--

INSERT INTO `gig_prices` (`id`, `user_id`, `gig_id`, `infulancer_id`, `price`, `complementry_item`, `status`, `is_resend`, `created_at`, `updated_at`) VALUES
(1, 4, '1', '1', '300', '', '1', 0, '2020-01-18 04:46:43', '2020-01-18 04:46:43');

-- --------------------------------------------------------

--
-- Table structure for table `gig_users`
--

CREATE TABLE `gig_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `gig_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `reason` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reject_amount` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_checkin` int(11) NOT NULL DEFAULT '0',
  `is_resend` int(50) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `influencer_details`
--

CREATE TABLE `influencer_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `dob` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `influencer_images`
--

CREATE TABLE `influencer_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `influencer_images`
--

INSERT INTO `influencer_images` (`id`, `user_id`, `image`, `created_at`, `updated_at`) VALUES
(10, 9, '5e25948bb84f2_1579520262037.jpg', '2020-01-20 06:22:43', '2020-01-20 06:22:43'),
(11, 10, '5e2595d115a4b_1579521482518.jpg', '2020-01-20 06:28:09', '2020-01-20 06:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `influencer_interests`
--

CREATE TABLE `influencer_interests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `interest_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `influencer_interests`
--

INSERT INTO `influencer_interests` (`id`, `user_id`, `interest_id`, `created_at`, `updated_at`) VALUES
(18, 9, 7, NULL, NULL),
(19, 10, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `influencer_price`
--

CREATE TABLE `influencer_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `plug_price` int(11) DEFAULT NULL,
  `gig_price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `influencer_price`
--

INSERT INTO `influencer_price` (`id`, `user_id`, `plug_price`, `gig_price`, `created_at`, `updated_at`) VALUES
(4, 9, NULL, NULL, '2020-01-20 06:22:43', '2020-01-20 06:22:43'),
(5, 10, NULL, NULL, '2020-01-20 06:28:09', '2020-01-20 06:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `influencer_social_details`
--

CREATE TABLE `influencer_social_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `facebook_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_friends` bigint(20) UNSIGNED DEFAULT NULL,
  `twitter_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_friends` bigint(20) UNSIGNED DEFAULT NULL,
  `instagram_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_friends` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `influencer_social_details`
--

INSERT INTO `influencer_social_details` (`id`, `user_id`, `facebook_name`, `facebook_friends`, `twitter_name`, `twitter_friends`, `instagram_name`, `instagram_friends`, `created_at`, `updated_at`) VALUES
(4, 9, 'tina', 1999, 'tina', 199, 'tina', 1234, '2020-01-20 06:22:44', '2020-01-20 06:22:44'),
(5, 10, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-20 06:28:09', '2020-01-20 06:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `infulancer_ratings`
--

CREATE TABLE `infulancer_ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `infulencer_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT 'Created by user',
  `added_by` enum('A','U') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'A => added by admin, Added by user',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interest_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`id`, `user_id`, `added_by`, `image`, `interest_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'A', 'nightlife2.png', 'Nightlife', '2019-09-23 13:00:00', '2019-09-23 13:00:00'),
(2, 1, 'A', 'luxury2.png', 'Luxury', '2019-09-23 13:00:00', '2019-09-23 13:00:00'),
(3, 1, 'A', 'life-style2.png', 'Lifestyle', '2019-09-23 13:00:00', '2019-09-23 13:00:00'),
(4, 1, 'A', 'health2.png', 'Health', '2019-09-23 13:00:00', '2019-09-23 13:00:00'),
(5, 1, 'A', 'food2.png', 'Food', '2019-09-23 13:00:00', '2019-09-23 13:00:00'),
(6, 1, 'A', 'fashion2.png', 'Fashion', '2019-09-23 13:00:00', '2019-09-23 13:00:00'),
(7, 1, 'A', 'fitness.png', 'Fitness', '2019-09-23 13:00:00', '2019-09-23 13:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `interest_users`
--

CREATE TABLE `interest_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `interest_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `intersts_users`
--

CREATE TABLE `intersts_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `interest_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_08_30_173924_create_influencer_images_table', 1),
(10, '2019_08_30_174215_create_influencer_price_table', 1),
(11, '2019_08_30_175629_create_influencer_social_details_table', 1),
(12, '2019_09_23_063055_create_business_details_table', 1),
(13, '2019_09_24_060735_create_interests_table', 1),
(14, '2019_09_24_094700_create_influencer_interests_table', 1),
(15, '2019_09_25_071702_create_influencer_details_table', 1),
(16, '2019_09_27_072817_create_ratings_table', 1),
(17, '2019_09_30_110557_create_gigs_table', 1),
(18, '2019_09_30_110703_create_gig_media_table', 1),
(19, '2019_09_30_135054_create_posts_table', 1),
(20, '2019_09_30_135210_create_post_images_table', 1),
(21, '2019_10_03_055416_create_gig_prices_table', 1),
(22, '2019_10_03_060431_create_post_prices_table', 1),
(23, '2019_10_14_064728_create_intersts_users_table', 1),
(24, '2019_10_16_094703_create_gig_users_table', 1),
(25, '2019_10_16_094723_create_post_users_table', 1),
(26, '2019_10_21_063817_create_interest_users_table', 1),
(27, '2019_11_07_095036_create_notifications_table', 1),
(28, '2019_11_20_064951_create_post_details_table', 1),
(29, '2019_11_20_072634_create_post_detals_media_table', 1),
(30, '2019_11_25_111510_create_gig_details_table', 1),
(31, '2019_11_25_111510_create_gig_media_details_table', 1),
(32, '2019_11_28_054742_create_mpesa_phone_numbers_table', 1),
(33, '2019_11_28_054824_create_payments_table', 1),
(34, '2019_12_09_075755_create_admin_payments_table', 1),
(35, '2019_12_23_071222_create_blogs_table', 1),
(36, '2019_12_23_071253_create_featureds_table', 1),
(37, '2019_12_26_054333_create_infulancer_ratings_table', 1),
(38, '2020_01_08_061739_create_share_data_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_phone_numbers`
--

CREATE TABLE `mpesa_phone_numbers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `phone_number` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mpesa_phone_numbers`
--

INSERT INTO `mpesa_phone_numbers` (`id`, `user_id`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 4, '12245678', '2020-01-18 03:04:33', '2020-01-18 03:04:33');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `other_user_id` int(11) NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `other_user_id`, `message`, `created_at`, `updated_at`) VALUES
(3, 4, 1, 'Tina Sharma accepted your post', '2020-01-20 00:11:15', '2020-01-20 00:11:15'),
(6, 4, 1, ' Review Tina Sharma Post', '2020-01-20 04:56:19', '2020-01-20 04:56:19'),
(7, 4, 1, 'Tina Sharma accepted your post', '2020-01-20 04:57:49', '2020-01-20 04:57:49'),
(8, 4, 1, ' Review Tina Sharma Post', '2020-01-20 05:43:22', '2020-01-20 05:43:22');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('000cd5f337e8dffb9ff1d1c3da741620f36ad801d378fd8864b8c5eaf52429a7e11f457d63c8b173', 4, 1, '*', '[]', 1, '2020-01-13 00:23:46', '2020-01-13 00:23:46', '2021-01-13 05:53:46'),
('00ac6c9cc089c47e128c5bf7d2d9b46a95a80463b74812b4b50472418285fc673544121913a1378d', 5, 1, '*', '[]', 1, '2020-01-18 02:38:42', '2020-01-18 02:38:42', '2021-01-18 08:08:42'),
('00bb82f53e2ba9cba19a0e2910ea34633bf898f27394e9d6f23dbd44295f946391d8c4ee77823377', 20, 1, '*', '[]', 1, '2020-01-12 23:44:51', '2020-01-12 23:44:51', '2021-01-13 05:14:51'),
('00d1b5a96f12cf494f01d9da54f0a9af5b9fd82e7d5266c11f578ad75eeefc1aed7ee49d39fe8a10', 3, 1, '*', '[]', 1, '2020-01-17 06:05:31', '2020-01-17 06:05:31', '2021-01-17 11:35:31'),
('00ef11d9e40696a5668dfb6d58c8d1ebb871d4f504b26f06bd4e9752be31247d9ab991be2780f369', 63, 1, '*', '[]', 1, '2020-01-16 01:56:56', '2020-01-16 01:56:56', '2021-01-16 07:26:56'),
('01b52bf4d6d680893c7c29d399495dc3a6ef7673de1eda81e0ca2cb29b9d060cb8b638a30034cca9', 80, 1, '*', '[]', 1, '2020-01-16 05:30:18', '2020-01-16 05:30:18', '2021-01-16 11:00:18'),
('01bdbfb0d4e99f17a78e75eef5fdd80c7062ca0947e482c9fe9f45aef7102a0fd3dc04dd38e2dd8d', 14, 1, '*', '[]', 1, '2020-01-09 05:09:40', '2020-01-09 05:09:40', '2021-01-09 10:39:40'),
('01db03ffcd5452b5a0561a6a7d3da8add1b7e835e95ad2b4328d456272f227413b29b604fc6333ad', 30, 1, '*', '[]', 0, '2020-01-10 00:33:43', '2020-01-10 00:33:43', '2021-01-10 06:03:43'),
('01e121d66b6a0bec535d77db37f24cfe93d37d56e1605da2f62efbf4f7f4599f0633742ac76a0749', 9, 1, '*', '[]', 1, '2020-01-09 04:13:56', '2020-01-09 04:13:56', '2021-01-09 09:43:56'),
('029f05ed657e6d207f6c5d52c30bca3047c0e6caf43cf0df4db814705088d4ebc52ea6e7160133ca', 32, 1, '*', '[]', 1, '2020-01-09 23:58:21', '2020-01-09 23:58:21', '2021-01-10 05:28:21'),
('0330d0180de7490ab75c62c476e88af8c9c00b8f4bfa34e9d34c6eb0340bbe4f2a8506428882e0c8', 5, 1, '*', '[]', 1, '2020-01-17 04:11:39', '2020-01-17 04:11:39', '2021-01-17 09:41:39'),
('033205c2196315ddac6c2c84213fec3cd7106b78556152e93d76a4c63b8dfad9fe7870478d032020', 1, 1, '*', '[]', 1, '2020-01-18 03:10:21', '2020-01-18 03:10:21', '2021-01-18 08:40:21'),
('0376ae75d91ef3c6cf9533b50d7acd527e01389f12a87b8c825e1c20484d04dd87408fbfa37a5242', 8, 1, '*', '[]', 1, '2020-01-20 05:29:13', '2020-01-20 05:29:13', '2021-01-20 10:59:13'),
('037a62da3374e88179ba4b9663442a45803fc518b43ef39d8755c77d49728ef82b67b6c0bc63fc8e', 3, 1, '*', '[]', 1, '2020-01-15 06:56:16', '2020-01-15 06:56:16', '2021-01-15 12:26:16'),
('038fc3947f6ed58e0180574c0b6e5e8d520496290b9129706025d65f7781bdba73599abd0607594f', 66, 1, '*', '[]', 1, '2020-01-16 06:09:54', '2020-01-16 06:09:54', '2021-01-16 11:39:54'),
('03a7a116d8f580ae7b12708120c1fccdf001d3fdae4beb09e49ffdc39f08116cc995231b6d791b0c', 62, 1, '*', '[]', 1, '2020-01-14 01:29:40', '2020-01-14 01:29:40', '2021-01-14 06:59:40'),
('03c10b229ce5b98a8b8de7288ba73ff5b169657dab7ca02761494717d73e3295fb4a086f32555fbc', 12, 1, '*', '[]', 1, '2020-01-17 05:23:23', '2020-01-17 05:23:23', '2021-01-17 10:53:23'),
('03fcc8aeb39a91dd21c1ff2268985af567aa94ea2dfc42939d25c516e5d5eefbd4410197ae5fe877', 12, 1, '*', '[]', 1, '2020-01-17 04:57:37', '2020-01-17 04:57:37', '2021-01-17 10:27:37'),
('040682398e9531b8468d1485e9481c5f54230e1322b91be69aba0a0702dd6a183fd9b19aa96e4fa8', 62, 1, '*', '[]', 1, '2020-01-14 08:10:48', '2020-01-14 08:10:48', '2021-01-14 13:40:48'),
('040ccdce3fa8e375f860121413680196148904d87273114a8948823c015e073b5d9b66c30a2c6bed', 15, 1, '*', '[]', 1, '2020-01-09 05:06:04', '2020-01-09 05:06:04', '2021-01-09 10:36:04'),
('042caa3298a76049f4d61c36e730e711f66ac2d896447272cf3ee1064e2246739137cd098ffece42', 11, 1, '*', '[]', 1, '2020-01-09 04:21:51', '2020-01-09 04:21:51', '2021-01-09 09:51:51'),
('04e13785d46d56f0441acf3fb70b5a904467461f6f0c104f32bd2fa4fa8791ae43537d547ebc8c72', 50, 1, '*', '[]', 0, '2020-01-13 02:05:13', '2020-01-13 02:05:13', '2021-01-13 07:35:13'),
('04e709616845d427ef4d9af2637196e027e8f2c14e91c822d72039174d2b723feeb191014c740bc1', 1, 1, '*', '[]', 1, '2020-01-18 03:00:17', '2020-01-18 03:00:17', '2021-01-18 08:30:17'),
('053ae68fec03587d9da6f70f6c60b122f73443ecca4408695a7cdca1ec28b36d0861c93921095483', 62, 1, '*', '[]', 1, '2020-01-16 04:31:00', '2020-01-16 04:31:00', '2021-01-16 10:01:00'),
('05f2869d56ad2c4fd3196d533fe60730a058b646e6fae4b95f1a8069a7b4f15ba876094118cf63f7', 62, 1, '*', '[]', 1, '2020-01-16 06:03:15', '2020-01-16 06:03:15', '2021-01-16 11:33:15'),
('060386701e64aeda6ea01e2efb80a41f56376f78a7d03b2635fed00ae55534477e0fd57ac9c6c7e3', 7, 1, '*', '[]', 1, '2020-01-17 23:40:30', '2020-01-17 23:40:30', '2021-01-18 05:10:30'),
('06427ab2e5046b454f81413d7c70486e2873c6eca119a2085c9416f77a591dca3951fe9fb54ac939', 20, 1, '*', '[]', 1, '2020-01-13 00:49:00', '2020-01-13 00:49:00', '2021-01-13 06:19:00'),
('067538781a9a0e9f8060c46a04f6c32018ebe454dd9b36878cebeaa5ce3a4a478140714cf2712a9b', 63, 1, '*', '[]', 1, '2020-01-13 06:28:14', '2020-01-13 06:28:14', '2021-01-13 11:58:14'),
('06cfdc771e802258c40f14c4973175a4f3ba7ca9491ba7339c0e59458417666dcc1f99509a771b2a', 15, 1, '*', '[]', 1, '2020-01-09 05:05:04', '2020-01-09 05:05:04', '2021-01-09 10:35:04'),
('06e608775f6fc50fefdad863784e49b8a8719473801883ac1575a95a8c6793415f52c0ab493c5ecd', 2, 1, '*', '[]', 1, '2020-01-08 02:37:43', '2020-01-08 02:37:43', '2021-01-08 08:07:43'),
('07071ce1e2c42a545b2c3d085e31683aecad7b74dc7af1765a4090089f52d36c3f3f884ce7a6cdef', 4, 1, '*', '[]', 1, '2020-01-15 04:42:52', '2020-01-15 04:42:52', '2021-01-15 10:12:52'),
('07164b9ed30140895bcac2fc0b261978f240e480fc2d5185f8171aed9eb64c7634ad8e1d1a246a4a', 5, 1, '*', '[]', 1, '2020-01-18 02:21:27', '2020-01-18 02:21:27', '2021-01-18 07:51:27'),
('073f094d78ec8f4645eb6ec9c290661858071360096a8bb07d78cafe7d4820a12ede0a21e7611801', 8, 1, '*', '[]', 1, '2020-01-17 04:04:59', '2020-01-17 04:04:59', '2021-01-17 09:34:59'),
('0757d3182a3d9bff043b4ce3c984d4661c406eebb2322fb894677d51b3092a4c963d0aee59379856', 15, 1, '*', '[]', 1, '2020-01-17 05:31:49', '2020-01-17 05:31:49', '2021-01-17 11:01:49'),
('07676799d999072ceeead801205b62c1804c25a96b8c82c9b7ddd34c7b5bf7ad1f53b1314a18edca', 11, 1, '*', '[]', 1, '2020-01-17 05:04:09', '2020-01-17 05:04:09', '2021-01-17 10:34:09'),
('0780a8eb3f31e746ae8c62dd736628c640f75fbcda1eb3c41fdb6ccb2548c924c1ca3fcf8110100d', 20, 1, '*', '[]', 1, '2020-01-15 04:12:48', '2020-01-15 04:12:48', '2021-01-15 09:42:48'),
('07b880b70d5d45f56de473aba0cc068213a1fa16ea0c3ecb9fc6082062e645ffc53061bd8e3e752c', 3, 1, '*', '[]', 1, '2020-01-15 04:49:47', '2020-01-15 04:49:47', '2021-01-15 10:19:47'),
('081d7bcf3ce45bbd038fdf059c97badf999536e6d9ff9a702b22d64e7c5c71035dd31fd1e166c8c2', 62, 1, '*', '[]', 1, '2020-01-15 06:34:33', '2020-01-15 06:34:33', '2021-01-15 12:04:33'),
('089a394637caae5e7700712c73f7a1ceecf630b4ecc63d6a1f4dd1f6ea92ffa81dbe86297237074e', 3, 1, '*', '[]', 1, '2020-01-10 05:53:07', '2020-01-10 05:53:07', '2021-01-10 11:23:07'),
('089be17f02170c5d479e5bbaefb246203a2016d353ca99902e2075dd0b94fd8a993d6eab092a1b7d', 2, 1, '*', '[]', 1, '2020-01-08 07:34:06', '2020-01-08 07:34:06', '2021-01-08 13:04:06'),
('08a2f0c1a83c8a3f507cf8cd034070d98ddb139a32b5fd0afcf1f163b5f18d9101972b864a7a5bfe', 22, 1, '*', '[]', 0, '2020-01-09 07:57:38', '2020-01-09 07:57:38', '2021-01-09 13:27:38'),
('08ead6c41c053a02310e95b19c3023fd2b34f4e681c22a103a521cb9dea78b13a22b1ea6b0a17ce2', 15, 1, '*', '[]', 1, '2020-01-09 05:04:03', '2020-01-09 05:04:03', '2021-01-09 10:34:03'),
('09566e2a6166fe1801ec2867f178a7b017157726e54712df6ceabc0d122a8bc23a6dcd56d72d02e6', 3, 1, '*', '[]', 1, '2020-01-17 05:43:33', '2020-01-17 05:43:33', '2021-01-17 11:13:33'),
('098e292057fe95b249b6583902909dc2addde96e4767ca32e7e79856ace11a19ae63f35e4e607dd0', 3, 1, '*', '[]', 1, '2020-01-10 05:23:03', '2020-01-10 05:23:03', '2021-01-10 10:53:03'),
('098ee300ee8d07dc397bfb3e354c6de5202bd63cb9dd71ca0a5587620a523895b61c06f1e8a3f335', 74, 1, '*', '[]', 1, '2020-01-15 06:53:03', '2020-01-15 06:53:03', '2021-01-15 12:23:03'),
('0a4e3bc5b71d28281278e2ef204d139ba174973c24aa0d4fd89ce3b3fbc661708379aaf2bd2f10d3', 4, 1, '*', '[]', 1, '2020-01-15 06:00:04', '2020-01-15 06:00:04', '2021-01-15 11:30:04'),
('0acee3f073b426a337f6d3943d0c8e1e7fb2ac23501a7159c6387dcff3563c82e36d025e2ba77ad8', 10, 1, '*', '[]', 0, '2020-01-20 06:28:48', '2020-01-20 06:28:48', '2021-01-20 11:58:48'),
('0b15a7f71f6388158c419f1cd11e49bf8e87e07aba272d746846708aea960296a964226eab62803e', 3, 1, '*', '[]', 1, '2020-01-09 04:34:13', '2020-01-09 04:34:13', '2021-01-09 10:04:13'),
('0bc00e71fea6bbcfdd3319f5499817a5e32dc099eadb6e5e6e62bd892cd196df4afcd128075e11e0', 96, 1, '*', '[]', 0, '2020-01-16 11:55:28', '2020-01-16 11:55:28', '2021-01-16 17:25:28'),
('0bee5298e0990b4c638e9f1d627d776e9c032e27a6310344e8db7809a65ff719bd5ed37d26adcc71', 5, 1, '*', '[]', 1, '2020-01-17 04:17:04', '2020-01-17 04:17:04', '2021-01-17 09:47:04'),
('0c4c290571755ecf98e796efede8da6aa86b872b7744dc69bfb34a00f8963fcd097664364757fe47', 66, 1, '*', '[]', 1, '2020-01-15 23:37:20', '2020-01-15 23:37:20', '2021-01-16 05:07:20'),
('0c6e0eb58f6f2c2742e456b9ac11c060439903ef4bd4deb70a29731bc00241a18ed9c3732924d057', 4, 1, '*', '[]', 1, '2020-01-15 07:15:44', '2020-01-15 07:15:44', '2021-01-15 12:45:44'),
('0c92996ea3f68367023f4bbe4b4e52bf7059b9f1404b8416cd24ffd47ba8b1f20e5477975add1b40', 1, 1, '*', '[]', 1, '2020-01-20 01:21:42', '2020-01-20 01:21:42', '2021-01-20 06:51:42'),
('0ccafff5640db0d48cca58b0dd4938b168f9faca5660d2c1ccb110b50af9c419adca7ae8b31173ea', 1, 1, '*', '[]', 1, '2020-01-16 23:16:25', '2020-01-16 23:16:25', '2021-01-17 04:46:25'),
('0ced32ec076cfac015db9ac485c9c59e156f0e51e515b3c793c525c6f1ba970e65a4053ede2d1ea1', 62, 1, '*', '[]', 1, '2020-01-14 06:05:46', '2020-01-14 06:05:46', '2021-01-14 11:35:46'),
('0d07264c8a7ecd26a77b92db7a31fcb91f65766c2449c60a01247218a63a9bf76d58b43dbdae5645', 62, 1, '*', '[]', 1, '2020-01-16 05:27:11', '2020-01-16 05:27:11', '2021-01-16 10:57:11'),
('0d59bf3daefd461c7ca8af7189acc2711c757efa4fceae04400c386191dee116a239a8819e94912f', 14, 1, '*', '[]', 1, '2020-01-09 05:37:10', '2020-01-09 05:37:10', '2021-01-09 11:07:10'),
('0dbf9c1b2b2736d167005260cd7810b2af50da46e9a52764a32ffa1072113b4679e2c2cde6b8a722', 63, 1, '*', '[]', 1, '2020-01-13 23:47:33', '2020-01-13 23:47:33', '2021-01-14 05:17:33'),
('0defba94457e2950a51b8dac2d3f9e8dee0f2e1d0592de6c0da874063fc553ea8cde45b994653526', 62, 1, '*', '[]', 1, '2020-01-16 01:02:22', '2020-01-16 01:02:22', '2021-01-16 06:32:22'),
('0dfc3f239ebcf7fdde8803e9f07db1381cc706a789d347b08db0ce8f3a2a38fe90f3aa7783fe88ab', 62, 1, '*', '[]', 1, '2020-01-16 05:05:25', '2020-01-16 05:05:25', '2021-01-16 10:35:25'),
('0e1687ff554d7111ec423c278bda6c488c4013598c686805aa9f609a34c5102b3efb2bb567a0f72f', 4, 1, '*', '[]', 1, '2020-01-08 08:52:29', '2020-01-08 08:52:29', '2021-01-08 14:22:29'),
('0e9f839061b49d11234764dc35ba365d114bdda1fc33e77ac0170cc807128ba75e772c563d379816', 3, 1, '*', '[]', 1, '2020-01-14 06:25:45', '2020-01-14 06:25:45', '2021-01-14 11:55:45'),
('0fbaae5ada82e0a29ea448f70bb7e86c3a2a390018eba43ba3c8a0bcef2d710e711941fb248bf6c0', 4, 1, '*', '[]', 1, '2020-01-09 01:34:11', '2020-01-09 01:34:11', '2021-01-09 07:04:11'),
('0fc58f472d53dad8671601c907c88842af37228f5f4ff84842691db86ed34cd877f420811b9f5b7a', 62, 1, '*', '[]', 1, '2020-01-14 06:49:34', '2020-01-14 06:49:34', '2021-01-14 12:19:34'),
('0ffa2a77055f82a966f4a8d184806f00720c9136c40fc2f12ec2ebcc5273854acfe0edd68467999b', 64, 1, '*', '[]', 1, '2020-01-13 07:03:20', '2020-01-13 07:03:20', '2021-01-13 12:33:20'),
('10200fa53feb66f77f186c6e193934f3b781380bc831559057e4b059238d088c1ce31932fcb53c5f', 5, 1, '*', '[]', 1, '2020-01-08 08:42:53', '2020-01-08 08:42:53', '2021-01-08 14:12:53'),
('107cb1d49b1accc581b2942eabca5420fb45a9440b31eeb08995271b7172c7efd57674760d93b5b2', 1, 1, '*', '[]', 1, '2020-01-20 00:26:18', '2020-01-20 00:26:18', '2021-01-20 05:56:18'),
('10bb589d0d670de31d3c839c1c922a1dca5db13f92e405ad53caf377b249d4856c3e3f81f1b0a9d4', 9, 1, '*', '[]', 1, '2020-01-17 04:34:05', '2020-01-17 04:34:05', '2021-01-17 10:04:05'),
('10c80e7487a02e9519eae498247ff7cb9f457dd7a630c807ef82b351f8b29b9c4d6c923d9069b371', 4, 1, '*', '[]', 1, '2020-01-15 08:07:09', '2020-01-15 08:07:09', '2021-01-15 13:37:09'),
('10e9e7f91b5b767aa86b00af110018822ddc6038a2e0f39d74489cc9e8c1c08807845f4185f37c5c', 4, 1, '*', '[]', 1, '2020-01-17 06:23:32', '2020-01-17 06:23:32', '2021-01-17 11:53:32'),
('10f51381250d856c058d97021a50eb4be01bbdcb861d3408bbc050754da8a07df639e8471f8631bd', 5, 1, '*', '[]', 1, '2020-01-18 01:28:00', '2020-01-18 01:28:00', '2021-01-18 06:58:00'),
('11059cbec21f27ad6513f0f11ba74ed038e4809de9263205faa40f09eb8665996b6d5a53d2d79287', 4, 1, '*', '[]', 1, '2020-01-16 08:24:42', '2020-01-16 08:24:42', '2021-01-16 13:54:42'),
('1158987c0aedf03c3b3c93538d520ff38e74b8ad922263432e3571a1a6f5eaed7d520294d6dee53f', 32, 1, '*', '[]', 1, '2020-01-09 23:51:36', '2020-01-09 23:51:36', '2021-01-10 05:21:36'),
('11949ca63413546d00d19ce975441bc4e420fd80c52b8253278b85faf7d1e06ac6f1cd203d43ab76', 3, 1, '*', '[]', 1, '2020-01-15 04:19:50', '2020-01-15 04:19:50', '2021-01-15 09:49:50'),
('11a99c7f858676f147b5271f25eb1ebc72a6e9588ded53b80857c4a1f3c11bbe9d75cee70e28b93f', 62, 1, '*', '[]', 1, '2020-01-16 02:34:21', '2020-01-16 02:34:21', '2021-01-16 08:04:21'),
('11e39352ae55349d19fe7e5d12afb3ed3dbffd586bdfabfa347fd2ecd71a6e4b4986d91bfb5b7685', 10, 1, '*', '[]', 1, '2020-01-18 01:51:46', '2020-01-18 01:51:46', '2021-01-18 07:21:46'),
('124f9c878dcbecf9252396f0a442128a554a2346da33a3021ee03c556c04c8d5a067f63f0db4d18c', 74, 1, '*', '[]', 1, '2020-01-15 05:02:52', '2020-01-15 05:02:52', '2021-01-15 10:32:52'),
('12ed3217cd4e28fec79b8ef949ef0fb399c723b6fba764032ef6b70d286004a76225fb8c5e71afda', 1, 1, '*', '[]', 1, '2020-01-08 02:01:48', '2020-01-08 02:01:48', '2021-01-08 07:31:48'),
('132', 1, 1, '*', '[]', 1, '2020-01-08 01:44:22', '2020-01-08 01:44:22', '2021-01-08 07:14:22'),
('13297079c8dc5d6f195c72273e966865f4112233b87aa2718703ca7792171a5680dcf5836d0abb34', 46, 1, '*', '[]', 1, '2020-01-10 03:45:35', '2020-01-10 03:45:35', '2021-01-10 09:15:35'),
('1365bb721b3d553268b676f04ab290e78bce9196f4e3766997b8fbca751df8b6bf29674cb073e142', 4, 1, '*', '[]', 1, '2020-01-15 02:04:09', '2020-01-15 02:04:09', '2021-01-15 07:34:09'),
('139f985d0e3aece82cd22c069d122d8f018ba5a6fe492e0bd59058165735349627f5a27ddf13a81f', 15, 1, '*', '[]', 1, '2020-01-09 05:04:30', '2020-01-09 05:04:30', '2021-01-09 10:34:30'),
('13b430ff1f4462e0791be6e5af5f426c9e732d1edcbe1b560be405c083db4e41514a4a4389189356', 36, 1, '*', '[]', 1, '2020-01-12 23:56:47', '2020-01-12 23:56:47', '2021-01-13 05:26:47'),
('13b63e3c7771959e7088ce1a77dc21be0bc7dab963a5becca5d29fbfd314aa564a8df427019b4210', 2, 1, '*', '[]', 1, '2020-01-18 03:24:38', '2020-01-18 03:24:38', '2021-01-18 08:54:38'),
('13eb3fdfae5584556393356eec0c3cbce90995a4e561c67d04838bc7ad7525bd6094f225c8f57258', 14, 1, '*', '[]', 1, '2020-01-09 05:14:59', '2020-01-09 05:14:59', '2021-01-09 10:44:59'),
('14263086d353239e4aa1b34e1418e1f73e575297674e82cc03e4340af90945639e79cacb2d1fddd1', 4, 1, '*', '[]', 1, '2020-01-15 07:01:26', '2020-01-15 07:01:26', '2021-01-15 12:31:26'),
('1467b80699e9e541c187fab13364d9c44408837072b8a3edcb93e57be5ec4b1f1cafa068080fdf27', 62, 1, '*', '[]', 1, '2020-01-15 08:33:18', '2020-01-15 08:33:18', '2021-01-15 14:03:18'),
('1486182da9ee874fdc13e4774df93566983d63d9e727fa1e6405aa848bb93513ae4220474c6bb321', 1, 1, '*', '[]', 1, '2020-01-09 03:59:37', '2020-01-09 03:59:37', '2021-01-09 09:29:37'),
('14a24c47ad1f1170875a119215a095dc79fe530f972e07102aec575a81d60ec9fce1f0ddec94a1bd', 3, 1, '*', '[]', 1, '2020-01-14 06:22:36', '2020-01-14 06:22:36', '2021-01-14 11:52:36'),
('14ae6c763e516483da25242fe17107c6125afa07c0ee6024e8a6a692f3b0449d8b65bb9fec0fa97c', 4, 1, '*', '[]', 1, '2020-01-17 01:01:22', '2020-01-17 01:01:22', '2021-01-17 06:31:22'),
('1508f68acbd02c6c17d9dc578cd16f60cdd749adea95267b28fbaa5b432b18b224a6435a96350bd9', 9, 1, '*', '[]', 1, '2020-01-17 07:00:09', '2020-01-17 07:00:09', '2021-01-17 12:30:09'),
('150adc59ab1c90002c4270689fc3f6d6a27e94a207c3ea444c963e6067176033b6f3c31c63a0b8c7', 3, 1, '*', '[]', 1, '2020-01-14 06:31:05', '2020-01-14 06:31:05', '2021-01-14 12:01:05'),
('151217ca15eeef19f756df411ce8cc517e891a831aca168c15321396754987f071938d6abc20227d', 62, 1, '*', '[]', 1, '2020-01-13 07:57:04', '2020-01-13 07:57:04', '2021-01-13 13:27:04'),
('155f712842ddc05b378c6ed29ebdde6d5c4969099cd1cfd490e0f174257e02708eca11e5e1d44d24', 62, 1, '*', '[]', 1, '2020-01-16 01:01:42', '2020-01-16 01:01:42', '2021-01-16 06:31:42'),
('15607cdcfd08563528edac0e9b376865c93638fb3465c32f5acb6262c4e9bebb2445d47fcec8ff32', 7, 1, '*', '[]', 1, '2020-01-09 02:17:39', '2020-01-09 02:17:39', '2021-01-09 07:47:39'),
('15657af62aceacbc735d15b44364732c788f42404c22953e6b09d7d364a68bbd7251c3ad656bd22b', 3, 1, '*', '[]', 1, '2020-01-17 04:37:58', '2020-01-17 04:37:58', '2021-01-17 10:07:58'),
('1573da808fa5b35d63035943f96c187c920f22fbd72388b1f6ac60359f6b05fc3afa47046616b5a4', 14, 1, '*', '[]', 1, '2020-01-09 05:06:52', '2020-01-09 05:06:52', '2021-01-09 10:36:52'),
('15d70c15403e7e891d3a77c309d753a048391384419735e4631034e57e8a1f606a6a00337002d1d3', 3, 1, '*', '[]', 1, '2020-01-08 04:36:25', '2020-01-08 04:36:25', '2021-01-08 10:06:25'),
('15fa326fb20e46b07df918aa4b50c36fe1091e7c0aa2b19643b4bfd78fc47be2b67694b64946d829', 4, 1, '*', '[]', 1, '2020-01-15 04:15:02', '2020-01-15 04:15:02', '2021-01-15 09:45:02'),
('162e0f181be4c473776df77285027515046ee13581516554d0417c0e17900be4513ae6c0cf727085', 3, 1, '*', '[]', 1, '2020-01-17 03:58:14', '2020-01-17 03:58:14', '2021-01-17 09:28:14'),
('163f3fadcc731e622a26a66ba3bb9d06d4ccf8bbc83ecddba6adf8a0a9029838a7e909d0dc933a16', 10, 1, '*', '[]', 1, '2020-01-17 00:36:48', '2020-01-17 00:36:48', '2021-01-17 06:06:48'),
('16b243b5b05085d3316fa5f03889a4c1a90b43d5239849c55c7cc12a68aa6db583eec484eeafa583', 4, 1, '*', '[]', 1, '2020-01-15 08:12:39', '2020-01-15 08:12:39', '2021-01-15 13:42:39'),
('171e02c05345c6c339bd1d66a7422d516a35194e545281e079cca55903d0ec235fb70ffdafcdd638', 14, 1, '*', '[]', 1, '2020-01-09 05:15:03', '2020-01-09 05:15:03', '2021-01-09 10:45:03'),
('1726760bd505214407bc3b01bb7fc2c0a08caae03730278e21a184a5e6ce5c4aa210fb559382fa2c', 63, 1, '*', '[]', 1, '2020-01-14 05:02:26', '2020-01-14 05:02:26', '2021-01-14 10:32:26'),
('17347881b328f31e184bf01d3046154ce8f69d639271b8002d1a29cf71dea778cbbb1379fa4af88a', 4, 1, '*', '[]', 1, '2020-01-13 01:08:35', '2020-01-13 01:08:35', '2021-01-13 06:38:35'),
('176f9161848f9e771e21a51cde354975308688a4f45afeb4d3d20c052c8e518c700a1c738f2102d2', 36, 1, '*', '[]', 1, '2020-01-10 01:55:11', '2020-01-10 01:55:11', '2021-01-10 07:25:11'),
('17841687fe36147a6ec6b6ca4be4f158c388a7768023ccc70965d924dede3f1c787c107b968392f1', 1, 1, '*', '[]', 1, '2020-01-09 08:55:04', '2020-01-09 08:55:04', '2021-01-09 14:25:04'),
('17de77e72164544224216eb235f3b91e09c8bbf57f4ac5bea3f343d3a9633bc868ef901eae8e1e78', 35, 1, '*', '[]', 0, '2020-01-10 01:20:02', '2020-01-10 01:20:02', '2021-01-10 06:50:02'),
('182b9ec18d1ff39cdb4fc5c4491e5d0000a4caef3fdc72c780fd2650ac57222a372a6ff314afb00a', 64, 1, '*', '[]', 1, '2020-01-13 06:56:26', '2020-01-13 06:56:26', '2021-01-13 12:26:26'),
('188b531ca2aa0ad74e5bf4ebb58c324370ca817dd1624647a9dd6b317dcbc655267cadda76370648', 4, 1, '*', '[]', 1, '2020-01-15 04:44:18', '2020-01-15 04:44:18', '2021-01-15 10:14:18'),
('18b59e3a79ba866884f4d058e908d92f15490822e2dfe29b032c7a582c60b388941629203b8e752e', 5, 1, '*', '[]', 1, '2020-01-17 04:19:09', '2020-01-17 04:19:09', '2021-01-17 09:49:09'),
('192049009b61be9864f52d779b6bb3f468294108675d625950b2de2f931efd1874def9887b6ea499', 62, 1, '*', '[]', 1, '2020-01-14 23:34:53', '2020-01-14 23:34:53', '2021-01-15 05:04:53'),
('1946f65a75226d7fb073817f1790f0e8c5556f13eb918b97d0a5aeab1cde6bddab42d5a9b0759808', 62, 1, '*', '[]', 1, '2020-01-16 04:07:30', '2020-01-16 04:07:30', '2021-01-16 09:37:30'),
('195c3a232bb50b1b7c3c032e73d8274da886c5657a24ee16adef7203ec8e72ff3c54cdb189a7a649', 74, 1, '*', '[]', 1, '2020-01-16 06:19:12', '2020-01-16 06:19:12', '2021-01-16 11:49:12'),
('19a6852e0216492e7118c68bd99cbd7ae9f062997d104c377377f3eab7a79b064349e896792e68f2', 20, 1, '*', '[]', 0, '2020-01-16 07:09:47', '2020-01-16 07:09:47', '2021-01-16 12:39:47'),
('19fcefc95ead126efbb2933933206a640be2f692885c6357cd4737357133f8d909bfac08a8ec5486', 62, 1, '*', '[]', 1, '2020-01-16 06:18:06', '2020-01-16 06:18:06', '2021-01-16 11:48:06'),
('1a11c3901cee57ba15a1241f648ab5ac09cbe4f9e12bb66e7bd28ab953e54f6107bfd659ea1b1eeb', 60, 1, '*', '[]', 1, '2020-01-13 04:29:17', '2020-01-13 04:29:17', '2021-01-13 09:59:17'),
('1a37b79443e1ac80362e842913d9f1f556da5962c2526944a3357a8264b9b94fb23e26dc1f2b8600', 62, 1, '*', '[]', 1, '2020-01-15 06:50:51', '2020-01-15 06:50:51', '2021-01-15 12:20:51'),
('1a4ecc5a66c54585df89954a10d498ec5aa2be90894f810087deebd699c8f695b63e9aa2affcc3bd', 4, 1, '*', '[]', 1, '2020-01-15 04:09:21', '2020-01-15 04:09:21', '2021-01-15 09:39:21'),
('1a598e0447691f0c88584ad868e35e4918d6a003b7e6da8895e1b3a6333626c2ffce8bd6dbffc3ab', 1, 1, '*', '[]', 1, '2020-01-08 23:46:34', '2020-01-08 23:46:34', '2021-01-09 05:16:34'),
('1a6e45e8a2a90d0bd1eaa665c897d48242db58b9d7a738e48debea39c1c93c7730d2f245693529a2', 36, 1, '*', '[]', 1, '2020-01-10 08:52:39', '2020-01-10 08:52:39', '2021-01-10 14:22:39'),
('1a8e3afb89a01fd27751e98c4c0c545945003c9f704fa91521a35eddb826095bdd8252f1e86ad2bc', 3, 1, '*', '[]', 1, '2020-01-14 23:16:48', '2020-01-14 23:16:48', '2021-01-15 04:46:48'),
('1ab07d5652d21e85d72739f7c3c49d65554ded0e1dde89127ccf2c76a5057064272b6a8595eb72c1', 1, 1, '*', '[]', 1, '2020-01-09 04:27:25', '2020-01-09 04:27:25', '2021-01-09 09:57:25'),
('1b223f453046dfb530e2388e0b41bfd489944443c14e5bf2e919e3e7942cb936452f176ad0d27b97', 63, 1, '*', '[]', 1, '2020-01-16 04:09:27', '2020-01-16 04:09:27', '2021-01-16 09:39:27'),
('1b5b4ca6e10d4414ab5212207ec44805db5e6a2c15b97212b03cc12d1d8c64b26ef80b92e1b88851', 20, 1, '*', '[]', 1, '2020-01-13 00:36:28', '2020-01-13 00:36:28', '2021-01-13 06:06:28'),
('1b61d0d9da56d5193796063195f1168a7d9e1850eda8d38148ef8dc45c7a19bea53e7398356fbd5e', 36, 1, '*', '[]', 1, '2020-01-13 00:35:07', '2020-01-13 00:35:07', '2021-01-13 06:05:07'),
('1be788698c41d081c8b20b334c617e39a032757d7bc7582bddc34f5602a7adc924ac21d98dca0c51', 62, 1, '*', '[]', 1, '2020-01-16 07:41:45', '2020-01-16 07:41:45', '2021-01-16 13:11:45'),
('1bea5a1e6e935902d6eafe14f187427ca8eb93d5dbc7a5a088b048a2c67d8095792a582c471f4433', 2, 1, '*', '[]', 1, '2020-01-08 01:56:29', '2020-01-08 01:56:29', '2021-01-08 07:26:29'),
('1c3beafa31cd02bfd4129a07bada41270db4a367dd99b60995b345dfdb9b312bd9a62f1a33079413', 81, 1, '*', '[]', 1, '2020-01-16 05:34:46', '2020-01-16 05:34:46', '2021-01-16 11:04:46'),
('1c4026b47af2ea33b681347c9f399a28f7a1a90cd2074c23277795d85bc2a72631e2bd5d7313d18b', 62, 1, '*', '[]', 1, '2020-01-13 06:09:04', '2020-01-13 06:09:04', '2021-01-13 11:39:04'),
('1c72c547f15d4d33da6cd5cfe37d81581a4cb5cb57bdabbe06c47f3d2a52bb59f2dfcc178d68bf65', 86, 1, '*', '[]', 0, '2020-01-16 06:18:24', '2020-01-16 06:18:24', '2021-01-16 11:48:24'),
('1cc6e176cb68a8e2016e220284f9b1adf917e084d0c95345158d305f0a277c22194117f285bb556c', 4, 1, '*', '[]', 1, '2020-01-15 07:05:26', '2020-01-15 07:05:26', '2021-01-15 12:35:26'),
('1cd17be89a6afb89292ecf31a4ede029acaf4745d0864eae12d34648b7faa54316ffad47b7be6455', 3, 1, '*', '[]', 1, '2020-01-17 04:33:21', '2020-01-17 04:33:21', '2021-01-17 10:03:21'),
('1cd6808b993faece0f00f0f9d1b32824982ec7dd8d349d74018920a678b8ce822e1040515e29cf8c', 62, 1, '*', '[]', 1, '2020-01-14 08:08:19', '2020-01-14 08:08:19', '2021-01-14 13:38:19'),
('1d35f645ce1a7b382dc50c56b7351bb0f7f186ad18b6a9be4c2828f84279067c6e6bcf86a9e12125', 8, 1, '*', '[]', 1, '2020-01-09 04:13:25', '2020-01-09 04:13:25', '2021-01-09 09:43:25'),
('1e2fe7806a706c4aec7e4a8b489f5ce53127c145afdd453bd3755759ab80ced8886a117274579466', 36, 1, '*', '[]', 1, '2020-01-13 00:14:17', '2020-01-13 00:14:17', '2021-01-13 05:44:17'),
('1e8f9579c73ac39a11562eab6cafa69c907fcd068351e5b9f0b129fa9e619b14e4cf559883a7862e', 81, 1, '*', '[]', 1, '2020-01-16 05:21:24', '2020-01-16 05:21:24', '2021-01-16 10:51:24'),
('1f72c4aca74c478e3333b3e4a23ff25a2f95feeeb91c9187d37fb6fe5882c7422ec387d7fb88c1d0', 15, 1, '*', '[]', 1, '2020-01-09 05:04:36', '2020-01-09 05:04:36', '2021-01-09 10:34:36'),
('1f9070de2cace0d67093b739de771861dc6b5688fe11629bf4e82500a8610babca73d12cd48ff4c1', 4, 1, '*', '[]', 1, '2020-01-15 04:30:39', '2020-01-15 04:30:39', '2021-01-15 10:00:39'),
('1facbb3e674b14eeee01184e797b50b783c182445446ddbd4a5a42d89ebc18aba9b9c52999ec23b9', 4, 1, '*', '[]', 1, '2020-01-16 06:14:52', '2020-01-16 06:14:52', '2021-01-16 11:44:52'),
('1fbee52f48e901dadfdab5b7a173e414c096deb7133d9784f7c773924ee9e3dc220c3cda51a4f9bc', 3, 1, '*', '[]', 1, '2020-01-10 06:18:58', '2020-01-10 06:18:58', '2021-01-10 11:48:58'),
('1fc14e1b3c753b4d366b78d7d7daed502e414951f876405e81d67df2d55c720141adc5707416ccc4', 8, 1, '*', '[]', 1, '2020-01-20 06:00:51', '2020-01-20 06:00:51', '2021-01-20 11:30:51'),
('1fd75ecdecc2c68bce6de059d38c4643874e435dbdceb3c4e03607c25f02f2140292da8031e0d919', 4, 1, '*', '[]', 1, '2020-01-18 03:15:06', '2020-01-18 03:15:06', '2021-01-18 08:45:06'),
('1fd8167401d103040e487eddbbc6dbd3a9a041c9ea5edc869b60c479cbb5dc05e659a085ba171497', 4, 1, '*', '[]', 1, '2020-01-13 00:41:50', '2020-01-13 00:41:50', '2021-01-13 06:11:50'),
('1fdbe157e9a41ecbb7d9dbe98bfe53dfc8b7e76e2ef15fd13a02e43b1c2761f993049359a4aff238', 14, 1, '*', '[]', 1, '2020-01-09 05:37:12', '2020-01-09 05:37:12', '2021-01-09 11:07:12'),
('201a77aa04541f1c967a01b32f323ef9a9be391ee7098812402065f31782e2bdcd1c59db3e75c46e', 7, 1, '*', '[]', 1, '2020-01-18 00:32:35', '2020-01-18 00:32:35', '2021-01-18 06:02:35'),
('20243', 1, 1, '*', '[]', 1, '2020-01-08 01:46:06', '2020-01-08 01:46:06', '2021-01-08 07:16:06'),
('20244', 1, 1, NULL, '[]', 1, '2020-01-08 01:48:08', '2020-01-08 01:48:08', '2021-01-08 07:18:08'),
('20ae0fc23734a0b132f09c2b3e1ef1a7414a7087afea2fc27556f0de37c844a2c915bcd097ab220d', 62, 1, '*', '[]', 1, '2020-01-16 04:21:04', '2020-01-16 04:21:04', '2021-01-16 09:51:04'),
('20db6d905701cdb31b882120319ad305852f24ccc5120798e303b8ee4a2072ea5b7597603f96bb48', 74, 1, '*', '[]', 1, '2020-01-15 05:39:21', '2020-01-15 05:39:21', '2021-01-15 11:09:21'),
('20f1ed63dc280f08cf08e02b01f0ee58f199c41b5ab92ff30051887a9931edda30b5cdc6098f3534', 4, 1, '*', '[]', 1, '2020-01-13 05:21:54', '2020-01-13 05:21:54', '2021-01-13 10:51:54'),
('2147483647', 2, 1, NULL, '[]', 1, '2020-01-08 01:49:29', '2020-01-08 01:49:29', '2021-01-08 07:19:29'),
('2165de87ceb8c3ff92a4e47c407a60ae0f806fd7b12298042fb04e005d8ce8d75fd1b6736cdaa955', 68, 1, '*', '[]', 1, '2020-01-14 01:47:53', '2020-01-14 01:47:53', '2021-01-14 07:17:53'),
('220ef60048f94ed0811b1776de1b30aa7833834f6aea36578c9cd1bce019c3f79037debfbd408989', 10, 1, '*', '[]', 1, '2020-01-18 02:20:40', '2020-01-18 02:20:40', '2021-01-18 07:50:40'),
('22740', 1, 1, '*', '[]', 1, '2020-01-08 01:48:18', '2020-01-08 01:48:18', '2021-01-08 07:18:18'),
('227c77482ae7f74d5b67a673a7a8360e46ce1bd0e1998e42ce7affa4376964ef31a971bc32d47f11', 1, 1, '*', '[]', 1, '2020-01-08 03:09:50', '2020-01-08 03:09:50', '2021-01-08 08:39:50'),
('22806f5cd65ed14778d8cce3768538d14e67cbe7ffcabdba4fa214bb7efc67b753b9d72d851cead4', 1, 1, '*', '[]', 1, '2020-01-08 03:41:55', '2020-01-08 03:41:55', '2021-01-08 09:11:55'),
('237db2be7f1582591b3453b756d8fdbe434fd962e75bccf70daec0a9a4883784be95367d4363ae41', 3, 1, '*', '[]', 1, '2020-01-09 03:55:31', '2020-01-09 03:55:31', '2021-01-09 09:25:31'),
('2408b77fc233f805286c00d59d16c7c0ba9d471097d0ca4fd04ca831d0392e5b1ad7a7becc41bf67', 4, 1, '*', '[]', 1, '2020-01-15 02:36:07', '2020-01-15 02:36:07', '2021-01-15 08:06:07'),
('247f3ce51df2f502d741521e136849e1f61c814ee33e53db4449fc3a6dbbd830581bd82e70055ac4', 62, 1, '*', '[]', 1, '2020-01-14 08:03:19', '2020-01-14 08:03:19', '2021-01-14 13:33:19'),
('24abd60afe9abe648c5bae3c44c8d3029750855a001d44cba7e600280c8ee936fb006eaa231ea121', 37, 1, '*', '[]', 0, '2020-01-12 23:28:25', '2020-01-12 23:28:25', '2021-01-13 04:58:25'),
('254a6e30450b61e737ee42489032e023a4cbad14aafe85b1a9d28d8546368aa62bb61d135d5f2ece', 75, 1, '*', '[]', 1, '2020-01-15 06:08:59', '2020-01-15 06:08:59', '2021-01-15 11:38:59'),
('2561e242fbc1d91cc62778b2c21e3c802619850cd5061419db8a6e5757efd5893589fd6eabf116c2', 36, 1, '*', '[]', 1, '2020-01-10 09:00:16', '2020-01-10 09:00:16', '2021-01-10 14:30:16'),
('25f43d5e71c973260b8b36fbf9906fbb8e949e6eeb9cca1e5221eeb23d37b79c9aeec4224fbf39a0', 62, 1, '*', '[]', 1, '2020-01-14 23:19:56', '2020-01-14 23:19:56', '2021-01-15 04:49:56'),
('261ce5ad81599f323dfbec83825baf1069302e077944cd38292ab35702358c045f71d0e759340858', 62, 1, '*', '[]', 1, '2020-01-13 06:28:05', '2020-01-13 06:28:05', '2021-01-13 11:58:05'),
('26305819f5016a2e06ce9c8be01a6ec9049e20f721e64da3bb54aeca1deecb426cd5f810172f40d3', 4, 1, '*', '[]', 1, '2020-01-17 06:13:03', '2020-01-17 06:13:03', '2021-01-17 11:43:03'),
('263c5f6d0a9455f13a518d2e53b8358d95d14bb827fccc38b84777be13c68e357e65c8cf4d0441f0', 4, 1, '*', '[]', 1, '2020-01-15 08:13:53', '2020-01-15 08:13:53', '2021-01-15 13:43:53'),
('26897117c2f746bfce030feadadc0be5a9e2c50f216383cbcf87cce7e1c19709e948bea70a3200a4', 7, 1, '*', '[]', 1, '2020-01-20 05:31:14', '2020-01-20 05:31:14', '2021-01-20 11:01:14'),
('26a64edbfc772c2fd908191bcab0cf7b68c1bbebf86f412383b0877410ab6b6a2733941c53b3326a', 20, 1, '*', '[]', 1, '2020-01-13 00:00:14', '2020-01-13 00:00:14', '2021-01-13 05:30:14'),
('26b024ef288326247739b536e163bd57dc8b27c38a52c1fd3b1b06b1b0b8ec7e6102d6c1792ed593', 3, 1, '*', '[]', 1, '2020-01-12 23:20:50', '2020-01-12 23:20:50', '2021-01-13 04:50:50'),
('26f2b93bcf45122bcf1121eb467bc8bd92199ecc125ca80586caf9a6ce3a7cbd3c0ee0eab9584899', 8, 1, '*', '[]', 1, '2020-01-20 05:38:11', '2020-01-20 05:38:11', '2021-01-20 11:08:11'),
('271b82881cf90776760f5ceabb8469baf28525e5889d06b8b23c941ef70eb92912adb77ca803a4ed', 9, 1, '*', '[]', 1, '2020-01-17 07:03:45', '2020-01-17 07:03:45', '2021-01-17 12:33:45'),
('271f3d71aca9f70adff28c156e0696fcfce789576495b3bb516da86362a226790a0c7bd9d17c1a76', 2, 1, '*', '[]', 1, '2020-01-20 02:00:15', '2020-01-20 02:00:15', '2021-01-20 07:30:15'),
('27664673a6e3024500689f9cc0b39b7915dafa48c5017b9aad84a665b9c6d7ec7657fdcc1a77ddb5', 73, 1, '*', '[]', 1, '2020-01-15 04:53:07', '2020-01-15 04:53:07', '2021-01-15 10:23:07'),
('2766e1a09eea319b4a1e8e8aa5bbb2fdf0680baaf68d6d904abab86d5a87f0d5ef8b5f641aa80eaf', 4, 1, '*', '[]', 1, '2020-01-17 01:20:03', '2020-01-17 01:20:03', '2021-01-17 06:50:03'),
('276c0a48cee34d385506ef6d768958192237be824944eccbcdb068a44335e5c8fcc17792421bd94a', 3, 1, '*', '[]', 1, '2020-01-14 07:39:58', '2020-01-14 07:39:58', '2021-01-14 13:09:58'),
('27d1a4ebcee274269b12a09f46468bc36c1ee53396e4e1cd9f15f900070131a8004f5a99a93ec0f2', 62, 1, '*', '[]', 1, '2020-01-16 04:25:29', '2020-01-16 04:25:29', '2021-01-16 09:55:29'),
('28137f384ddb6330075642a3ed0588d0598842e2fe81593c8159b6d279739fb8b6f658bf90da74e3', 1, 1, '*', '[]', 1, '2020-01-10 06:25:54', '2020-01-10 06:25:54', '2021-01-10 11:55:54'),
('2826822d19b0c3285d4fc8ed131a39ad05281b524c4de97b69c9328229d63b8adc5880a20a944883', 20, 1, '*', '[]', 1, '2020-01-13 00:12:44', '2020-01-13 00:12:44', '2021-01-13 05:42:44'),
('28a864943416cccd907076335fd590067207f5fd1dab102bdc9f4c7d78466f868c41b7d06425dad6', 63, 1, '*', '[]', 1, '2020-01-14 04:58:37', '2020-01-14 04:58:37', '2021-01-14 10:28:37'),
('28b8b5ca49f205a55bcb20f526a620261ade2baee433790c150b424d9bd9e66323357aecccc50964', 3, 1, '*', '[]', 0, '2020-01-18 03:01:38', '2020-01-18 03:01:38', '2021-01-18 08:31:38'),
('292ea69b6b04639460c642b24f94d1c4025c8bba46a58b68f58c27a9b01bb3543132e1f2f66eb972', 2, 1, '*', '[]', 1, '2020-01-08 03:58:36', '2020-01-08 03:58:36', '2021-01-08 09:28:36'),
('295e544acaebdc70c3f3d6f5ab63524f070a87bc2755dbdbe1d1bdf775785e316b08a97fc6ecd655', 4, 1, '*', '[]', 1, '2020-01-17 00:49:30', '2020-01-17 00:49:30', '2021-01-17 06:19:30'),
('2979375fd94ebb675c79f90e735c8b99c6c9dc1461d48c2df2d0087c9c79ec3ae00f158e3032e558', 3, 1, '*', '[]', 1, '2020-01-15 02:50:06', '2020-01-15 02:50:06', '2021-01-15 08:20:06'),
('298d885d5c48c6e1321f5987671e91b56bee49f5f2548793f2e6268a0315fc5560f62572c3cef2df', 4, 1, '*', '[]', 1, '2020-01-16 09:35:56', '2020-01-16 09:35:56', '2021-01-16 15:05:56'),
('299c7e1df1d1aa9dec445c30680d200cdc8f6dc13c6b7595e48f689087f9c73af2ecb6a7335f5a26', 36, 1, '*', '[]', 1, '2020-01-10 09:09:19', '2020-01-10 09:09:19', '2021-01-10 14:39:19'),
('29adee4af5a4abe983e5324b3372028e9b6a4ca9edaac8cd6063a4f27e3a058500a108d040e598d2', 63, 1, '*', '[]', 1, '2020-01-16 01:32:26', '2020-01-16 01:32:26', '2021-01-16 07:02:26'),
('29c11be47a849fc55a4493cb3177ecb0de74bfc95a2cbe2537e9537ed43eaaefcbaa251b6ef9ec40', 1, 1, '*', '[]', 1, '2020-01-17 00:49:19', '2020-01-17 00:49:19', '2021-01-17 06:19:19'),
('29d89a4f9f375058fa3a1c45fed6014842b654ae62b8d9aa06fbca63c81c3d55bfe101b146051e23', 62, 1, '*', '[]', 1, '2020-01-16 05:06:47', '2020-01-16 05:06:47', '2021-01-16 10:36:47'),
('29e8d5b862e4b558ed36820b46ddbab119c48db0d1f773f02d58e23a5e92e5f8ac0729d77c9190b3', 15, 1, '*', '[]', 1, '2020-01-09 05:02:51', '2020-01-09 05:02:51', '2021-01-09 10:32:51'),
('2a78492debae3d2ce6956a6de46e7180154057f0e9a32256eb5b37dc66b9c7e53fca8756c1c9b099', 33, 1, '*', '[]', 1, '2020-01-10 00:55:43', '2020-01-10 00:55:43', '2021-01-10 06:25:43'),
('2a7dd8c3f7e050dcf5c215828bbcc9a477898767926d7f3b6a1d4af9ee06ddefc120b8b2b20b3cee', 1, 1, '*', '[]', 1, '2020-01-09 03:44:41', '2020-01-09 03:44:41', '2021-01-09 09:14:41'),
('2ac68de65f9a52057ecd9aa3ed8740c3686dd9a9c96b279bac3094ae081347ecc2c15ff04ad1cf6b', 5, 1, '*', '[]', 1, '2020-01-17 07:27:00', '2020-01-17 07:27:00', '2021-01-17 12:57:00'),
('2b4cbbf596d0652fcab9963ec8e410c8a2e5ceef7e68933555fb77c041bed66add71e2c1b3819510', 62, 1, '*', '[]', 1, '2020-01-13 05:59:18', '2020-01-13 05:59:18', '2021-01-13 11:29:18'),
('2bd3cd1160298981cef9dadaf08389b20055734c2d9ca5fcbf06534f24f7fc3b1b3fe927a886b1dd', 2, 1, '*', '[]', 1, '2020-01-20 04:28:54', '2020-01-20 04:28:54', '2021-01-20 09:58:54'),
('2c8334760c9844057cea8ca8e89d171d9f195ce1ee9ddd719fa8f35389c700b1b6fa4ab474928a58', 1, 1, '*', '[]', 1, '2020-01-08 04:01:05', '2020-01-08 04:01:05', '2021-01-08 09:31:05'),
('2cac6dd6762f477e9c9d21804798b3d9059fad04816950683892ab8716ddf9b34c893971e32ab0de', 71, 1, '*', '[]', 0, '2020-01-15 01:46:37', '2020-01-15 01:46:37', '2021-01-15 07:16:37'),
('2ccaf4d857fa161d18dbd06be103c6206d6afb36232c6a2236772a0049fffb786c15e2fd31a05e35', 3, 1, '*', '[]', 1, '2020-01-17 04:20:19', '2020-01-17 04:20:19', '2021-01-17 09:50:19'),
('2ccf9d5ce2a8d50724ae7467012e9139bc960c3b4ad9f54f553231239215569a1579834d35caebac', 92, 1, '*', '[]', 1, '2020-01-16 08:54:02', '2020-01-16 08:54:02', '2021-01-16 14:24:02'),
('2d0a93adf223a3104f91879cad423eca7335ad4ab7ed26b07393415c2bf97e85d5b5de94448ca762', 1, 1, '*', '[]', 1, '2020-01-08 02:38:54', '2020-01-08 02:38:54', '2021-01-08 08:08:54'),
('2d19c399d82b2b72cfa6a9463706f921b3cba51228429cfb8f47faebf1885bee4c2d253d3a32cf85', 63, 1, '*', '[]', 1, '2020-01-14 06:18:29', '2020-01-14 06:18:29', '2021-01-14 11:48:29'),
('2d71a86d5fd9743b94bd085c79558fd5bc6ba20f637dbb0e05b24da64a065bbe6e67fd95742a37bc', 10, 1, '*', '[]', 1, '2020-01-17 05:45:33', '2020-01-17 05:45:33', '2021-01-17 11:15:33'),
('2d8df9fc88d577d9623fc324e8df98a6128d8afdba5ed0e26d2526c9764c5741b1605621d18b7998', 9, 1, '*', '[]', 1, '2020-01-20 06:22:44', '2020-01-20 06:22:44', '2021-01-20 11:52:44'),
('2dcbc33e8a6fd6858f297bf20736cbeea1993e29741ec0a390f89a485a46e3f944ce783c37e127b5', 62, 1, '*', '[]', 1, '2020-01-14 03:39:10', '2020-01-14 03:39:10', '2021-01-14 09:09:10'),
('2dfa27fabda9d00a17bd290494825b52d7f71221fb35cf5008978d2fca0672884beae4f21baf6aa8', 9, 1, '*', '[]', 1, '2020-01-20 06:24:52', '2020-01-20 06:24:52', '2021-01-20 11:54:52'),
('2e32d7f4af6f8e74a24f2b5c109874a2fccad7f405fce1e0dca0383d71280c9945733947b34c6695', 3, 1, '*', '[]', 1, '2020-01-17 02:46:59', '2020-01-17 02:46:59', '2021-01-17 08:16:59'),
('2e7eda65ad8c6c18111d54972cee77bd1f8d0d099b9db4b5b4bc7e8c005593e43f86a00a182a9094', 74, 1, '*', '[]', 1, '2020-01-15 06:56:04', '2020-01-15 06:56:04', '2021-01-15 12:26:04'),
('2e97ec43c247b4b9b6980a67f3709efbc2a41e500ac9c2adf482490fd2e81809f8dc8d405771cf37', 36, 1, '*', '[]', 1, '2020-01-13 00:20:33', '2020-01-13 00:20:33', '2021-01-13 05:50:33'),
('2ec9ea9d7d6f75434baeb9f96b9edf1dccdff4b7bc267f374441f159d36788c7910490a4366b1cf7', 62, 1, '*', '[]', 1, '2020-01-16 05:07:46', '2020-01-16 05:07:46', '2021-01-16 10:37:46'),
('2eecbc09f29c83dd0096887d0e70753fd83347695f9be730fb6c45276c77ec7c74cbfbc811428bed', 1, 1, '*', '[]', 1, '2020-01-20 01:27:17', '2020-01-20 01:27:17', '2021-01-20 06:57:17'),
('2f66393b577fbad96dbac0041a6271b865d6a56eda4c1c95cf4102f9db6bc5101d133b1994ac362a', 62, 1, '*', '[]', 1, '2020-01-14 23:45:00', '2020-01-14 23:45:00', '2021-01-15 05:15:00'),
('2f9b2b13a83a2345ee6765e8e41b657b50368f00852293ed32af8496c391a44d6ff00055917624d2', 58, 1, '*', '[]', 0, '2020-01-10 07:40:33', '2020-01-10 07:40:33', '2021-01-10 13:10:33'),
('2fc582f8ff8f82bc19637547a147f75259b75500d27a6798f49e06e71937f718e958e0ae16318288', 62, 1, '*', '[]', 1, '2020-01-14 06:07:45', '2020-01-14 06:07:45', '2021-01-14 11:37:45'),
('3026cdfb89d2c908ac65b2917bd8bc0bd331bd45122a904de7cac337eeac49cf0f1a71c7360fcb16', 1, 1, '*', '[]', 1, '2020-01-12 23:20:36', '2020-01-12 23:20:36', '2021-01-13 04:50:36'),
('302e97b4bb1b16200205f984d646775241b9bd02822e6213b560b1c86368e96a8317d00674cd7f9e', 74, 1, '*', '[]', 1, '2020-01-15 06:51:32', '2020-01-15 06:51:32', '2021-01-15 12:21:32'),
('30686c8a60d5c59daa65114674fb5ebefffb0b46e995eb23c2adcaef547e5c17b36b28690411f5af', 36, 1, '*', '[]', 1, '2020-01-10 01:46:25', '2020-01-10 01:46:25', '2021-01-10 07:16:25'),
('30ac603b3f13b1cd39d02aac8209ca89b2998e2b07733fdb5c0ed0865c24c9742f558e7c1f3cb673', 84, 1, '*', '[]', 0, '2020-01-16 04:41:15', '2020-01-16 04:41:15', '2021-01-16 10:11:15'),
('30cbdd1a9fc6a02bc2104097ffcb5e3b7a973511568b424714c6a1cc8ac4f41924f0d456691347c6', 14, 1, '*', '[]', 1, '2020-01-09 05:10:27', '2020-01-09 05:10:27', '2021-01-09 10:40:27'),
('30d70371500c7843e96ec9531ac0994af5f914f044b7decfd56bfe683fef36d8a44bd65dbf882c06', 2, 1, '*', '[]', 1, '2020-01-17 05:55:35', '2020-01-17 05:55:35', '2021-01-17 11:25:35'),
('30e00209d37aa20b64a085d9252d358a2057c59169044188630f1be199116ac3e0042dca0b023195', 33, 1, '*', '[]', 1, '2020-01-10 00:54:44', '2020-01-10 00:54:44', '2021-01-10 06:24:44'),
('310a5bcbf8d19d83e420690f1b0458b53d1edec024df5857f69dd83e74fe0692fd8fbe45516f74dc', 62, 1, '*', '[]', 1, '2020-01-15 02:05:28', '2020-01-15 02:05:28', '2021-01-15 07:35:28'),
('31539d0334710d77df8413b2fb575fe95f0d49f52ae8538f3c037ce9102f3fe10da4b796970d027e', 2, 1, '*', '[]', 1, '2020-01-20 05:50:20', '2020-01-20 05:50:20', '2021-01-20 11:20:20'),
('315e66f85d5c7664b291f12ff64b5171c89f01fcbe1c39f2f264a3758374a87be22f10a17f1c44f4', 20, 1, '*', '[]', 1, '2020-01-13 01:29:04', '2020-01-13 01:29:04', '2021-01-13 06:59:04'),
('31968fef98ca009d487ab1d43fa90147be718696ca0930d0f4b61b83f9c4bf3fa9fb69025573688c', 7, 1, '*', '[]', 1, '2020-01-17 06:48:12', '2020-01-17 06:48:12', '2021-01-17 12:18:12'),
('321eecd09d15240d85c1781deb1f48ecfabdad46b52816682746537c99cb10c88e17ab6ebb6a095c', 62, 1, '*', '[]', 1, '2020-01-15 08:35:47', '2020-01-15 08:35:47', '2021-01-15 14:05:47'),
('325678688cf8bbabc4b6e9fcedbc649320d30f133d8eb7a7ffbdbdc72593cebc29f8cba9b28db4e6', 7, 1, '*', '[]', 1, '2020-01-09 03:53:06', '2020-01-09 03:53:06', '2021-01-09 09:23:06'),
('3277d9e3519ebb371dadd23da6d4de8217d6d7dd1e6edbe416f1692913f615fc559372f8b547ad09', 4, 1, '*', '[]', 1, '2020-01-14 03:27:11', '2020-01-14 03:27:11', '2021-01-14 08:57:11'),
('332885d9deb2168c3d421a66174805039f8914f47c6e6f4bea2c0b2736b308dac0d518a21f6a786b', 14, 1, '*', '[]', 1, '2020-01-09 05:06:43', '2020-01-09 05:06:43', '2021-01-09 10:36:43'),
('335d85d89a40e45ea73ebcc6da131782832e591be3ac1640fc73760b9b113c3143bf7ac182601e28', 1, 1, '*', '[]', 1, '2020-01-08 05:55:07', '2020-01-08 05:55:07', '2021-01-08 11:25:07'),
('3364ace4ad9bc517755803237e4021a0ac96c740504dc5569ca985d42987db0a53453414e748170b', 20, 1, '*', '[]', 1, '2020-01-13 00:04:35', '2020-01-13 00:04:35', '2021-01-13 05:34:35'),
('337188f8bb01a7e536cd8b9294a69e154e74cc2dd25b8a01c017927d032ea5b83fbf2a4b87c6b74f', 36, 1, '*', '[]', 1, '2020-01-12 23:42:17', '2020-01-12 23:42:17', '2021-01-13 05:12:17'),
('3421f3cc886fa4a52ef4fa3b3c7aec5766fd0ab7f87add93941be1810129a76e281553b9bffc4eb5', 1, 1, '*', '[]', 1, '2020-01-17 01:03:26', '2020-01-17 01:03:26', '2021-01-17 06:33:26'),
('3478a126e5148105f66d02bbfb3688707140615d80ca29d00a0ad2bcb66b136ca73e8a53dfce0c37', 1, 1, '*', '[]', 0, '2020-01-20 06:06:45', '2020-01-20 06:06:45', '2021-01-20 11:36:45'),
('34ad167ceabb8bdafbeef57dc82c9b5e1c8af18c18029b7f965710a4b67effd10977997ce8bc3cc4', 80, 1, '*', '[]', 0, '2020-01-16 08:50:12', '2020-01-16 08:50:12', '2021-01-16 14:20:12'),
('34bd862de21fc75b74eee7fb2b38a3d233791826386450ba69f33c1bc872aea4b16d0b3e38bb1b2e', 66, 1, '*', '[]', 1, '2020-01-14 04:33:25', '2020-01-14 04:33:25', '2021-01-14 10:03:25'),
('34f46fb787059038578dace141cea5b816c6ec1ef186288daa09ea11b7a696c88d34e8c5caccb530', 11, 1, '*', '[]', 1, '2020-01-17 04:56:16', '2020-01-17 04:56:16', '2021-01-17 10:26:16'),
('35632980a655254698e9425afe5822797b5e3c3899dc2ea8b5b7b6b47321fa41907198fb570ea063', 62, 1, '*', '[]', 1, '2020-01-14 05:53:33', '2020-01-14 05:53:33', '2021-01-14 11:23:33'),
('358aad7f88810508e4609f71d42fb41259b787880eb23e17f3df2546a99e5cfed4c7fc95312e34a9', 10, 1, '*', '[]', 1, '2020-01-18 01:03:26', '2020-01-18 01:03:26', '2021-01-18 06:33:26'),
('3591aec2d0fcf88039fcae6b21c4dc361272cfe5c7aec8a283ffe90c8702392146d418a4feadb10b', 62, 1, '*', '[]', 1, '2020-01-15 06:34:00', '2020-01-15 06:34:00', '2021-01-15 12:04:00'),
('35d2320d0ec91733043b9f11a314b0b4b7ca726e503ab3dbaa8a9a87b70fdc6e720a80bbca5d9846', 50, 1, '*', '[]', 1, '2020-01-10 08:11:42', '2020-01-10 08:11:42', '2021-01-10 13:41:42'),
('3623348fd4fc49e3982e66af691e7366e78ffe952a313964339d75bf30e239cd7395ec917cc428e1', 4, 1, '*', '[]', 1, '2020-01-17 00:50:05', '2020-01-17 00:50:05', '2021-01-17 06:20:05'),
('368b44b95d30dbab3112e253752aba293c0204b6b855265a354cbf9c5c5c72df61ec40f9608bce50', 62, 1, '*', '[]', 1, '2020-01-14 05:40:01', '2020-01-14 05:40:01', '2021-01-14 11:10:01'),
('3784c25e52259e4d889461085bbc6065210c4514d0a8c7b00083faa3c5136f8a369edd169e7e07fe', 62, 1, '*', '[]', 1, '2020-01-14 00:48:37', '2020-01-14 00:48:37', '2021-01-14 06:18:37'),
('3798ba8d80597ca23f1487248f146a8ccaa349e68150fa965fa11df87ba85c0c8dbea162d599c5a7', 38, 1, '*', '[]', 1, '2020-01-10 01:55:35', '2020-01-10 01:55:35', '2021-01-10 07:25:35'),
('38230815fb7777d3a3a3b909d7aa0b3d1ab7c90b07e31c6d79dc930534b87df22e9453fb0371bbf7', 46, 1, '*', '[]', 1, '2020-01-10 03:58:46', '2020-01-10 03:58:46', '2021-01-10 09:28:46'),
('386c9dfd0707b746d7131ceb1da4132f343cae529daf1b983ab01d21727aa27b1f8071b5ece2660e', 58, 1, '*', '[]', 1, '2020-01-10 07:38:43', '2020-01-10 07:38:43', '2021-01-10 13:08:43'),
('38f93f3d40be8e4da4d105d03c531943cbcec72e28ba7313e2cb55c131aa8f2bb78c7938345a169b', 3, 1, '*', '[]', 1, '2020-01-09 04:22:39', '2020-01-09 04:22:39', '2021-01-09 09:52:39'),
('392df67f6dc898beb40691a7a95c8ec6978f7495879c21b589c45210f30135ec00d290f74dd38092', 29, 1, '*', '[]', 0, '2020-01-09 23:14:43', '2020-01-09 23:14:43', '2021-01-10 04:44:43'),
('39628a72ffbf3121d0868e39c7f96b101d2e037eb4a3a9a30ec0d1e3ed280aac880e78f406937be2', 14, 1, '*', '[]', 1, '2020-01-09 04:31:57', '2020-01-09 04:31:57', '2021-01-09 10:01:57'),
('39d8e2ae5f1bfdb6187f8685223aa2ee8e2fceb4e8e3e81d2d6458bd1792bd7b10d735faa30beede', 3, 1, '*', '[]', 1, '2020-01-12 23:26:37', '2020-01-12 23:26:37', '2021-01-13 04:56:37'),
('39fb40eaa75c289b48389d8ac3a16808f8620600142fda284f34158dd5d16eb0c8c9fda60523eb28', 62, 1, '*', '[]', 1, '2020-01-16 04:41:19', '2020-01-16 04:41:19', '2021-01-16 10:11:19'),
('3a56f3c2e2f58c2e5340974c358d3ee7abf6ecfa3e4ecfbd4b6dddb2d384d1eaae89985ea8b41954', 36, 1, '*', '[]', 1, '2020-01-12 23:44:10', '2020-01-12 23:44:10', '2021-01-13 05:14:10'),
('3abe216afd159b982e1916e8412811f2f5779a36a419e2c5b7c1993f09754a5a61d5b1b9bee08c1c', 36, 1, '*', '[]', 1, '2020-01-12 23:30:08', '2020-01-12 23:30:08', '2021-01-13 05:00:08'),
('3b1a39aaf4a19d11ee088ad38cc2461f022932c3dbd56b6cffbeefe60cc3d42680ad827d058f8ed8', 3, 1, '*', '[]', 1, '2020-01-15 04:53:03', '2020-01-15 04:53:03', '2021-01-15 10:23:03'),
('3b200c782fb61cb17b844754ab666513d39db218b130511eda7fd3b16258415ffde2ac396913b58f', 20, 1, '*', '[]', 1, '2020-01-12 23:45:12', '2020-01-12 23:45:12', '2021-01-13 05:15:12'),
('3bc3892801f5a182c335b261aefffd392eb8a22122e2d3d000acfcb1b8397aced2eb3aebed12cbf2', 63, 1, '*', '[]', 0, '2020-01-16 04:11:39', '2020-01-16 04:11:39', '2021-01-16 09:41:39'),
('3bcbd4b1ad5171e5279f58e985f778e93e13b6ce56a8cd97953a9fbed676e66c2d811afb31821a49', 36, 1, '*', '[]', 1, '2020-01-13 00:20:51', '2020-01-13 00:20:51', '2021-01-13 05:50:51'),
('3be72afae68dd28c37c993d50cc25f42373b51f65eb8d046cba7c807c34b28b0c95d47fd0266bc17', 65, 1, '*', '[]', 1, '2020-01-14 00:37:42', '2020-01-14 00:37:42', '2021-01-14 06:07:42'),
('3cecd812da2b0a88ff27194b1c64357c622fdcd90ff201af59fb0b4573def7818e94681309876553', 62, 1, '*', '[]', 1, '2020-01-15 23:47:14', '2020-01-15 23:47:14', '2021-01-16 05:17:14'),
('3d0f5a3a0cfe9ef14585f57afb7e7550d3273ab04a56a70cc197879705ca0c3f348f71dff286238d', 60, 1, '*', '[]', 1, '2020-01-13 02:01:27', '2020-01-13 02:01:27', '2021-01-13 07:31:27'),
('3d1635c0a0f720c988d5fc4ba6278c7c9a5dc778fe86d06a15da85d7361b3ea7b5a121fd0d608383', 4, 1, '*', '[]', 1, '2020-01-15 04:41:00', '2020-01-15 04:41:00', '2021-01-15 10:11:00'),
('3d26830a5d0c92f8c81f0ccfd884a164ee9734a9144253013c927cdfea5579471dbd4ffd7d355b30', 3, 1, '*', '[]', 1, '2020-01-14 07:16:58', '2020-01-14 07:16:58', '2021-01-14 12:46:58'),
('3d425a7bb71f769f9206a8a87ba0e83630f46ec04c52a3afddd3fab6381fad16f52c38d175a95728', 2, 1, '*', '[]', 1, '2020-01-08 04:20:54', '2020-01-08 04:20:54', '2021-01-08 09:50:54'),
('3debda9cb9711f3970a3d4a36c674ef6b4731880ad0339a0c0ea34c870161a8f980e6a403330a1bd', 4, 1, '*', '[]', 1, '2020-01-14 04:59:26', '2020-01-14 04:59:26', '2021-01-14 10:29:26'),
('3e0fd548efb22b55f94bb394220a565ecd0d204bffb04d32d75ba307543df2bf950b76abf5f49fb4', 3, 1, '*', '[]', 1, '2020-01-15 04:48:44', '2020-01-15 04:48:44', '2021-01-15 10:18:44'),
('3ef20d54c815379a636a2970dcfb521a1483f1dcaae62353bd5d15940bf718d38526b776b61f9895', 36, 1, '*', '[]', 1, '2020-01-10 04:12:52', '2020-01-10 04:12:52', '2021-01-10 09:42:52'),
('3f57af27ba7175e77e757fdae7326adee0f1579549d2f1ac2ba5ac8e275dd029baac2c921d13fe8a', 9, 1, '*', '[]', 1, '2020-01-17 05:08:28', '2020-01-17 05:08:28', '2021-01-17 10:38:28'),
('408d24c1ba4f8c97d81b5069ba92a1ef14becb931d4fc33838441cccec5646a7829ad2154d642e57', 4, 1, '*', '[]', 1, '2020-01-13 07:58:04', '2020-01-13 07:58:04', '2021-01-13 13:28:04'),
('40c3197916a201a8c082ee36f7bbb271b14378feb9254c1c3b1258d471a9b18056bc0d2e77d57c8e', 3, 1, '*', '[]', 1, '2020-01-15 03:57:38', '2020-01-15 03:57:38', '2021-01-15 09:27:38'),
('40da411d80f40619422b5c3ccdcfed7b3c5f7ae88c9a48a896d6dfaaf00e1268333a0a3c7d106292', 3, 1, '*', '[]', 1, '2020-01-17 05:34:19', '2020-01-17 05:34:19', '2021-01-17 11:04:19'),
('40de6b6b362e8aeeb62df1cab9f3034f4c41aadc658d02c8de7966a5b4712ba9134589a02d16cae1', 57, 1, '*', '[]', 0, '2020-01-10 07:37:17', '2020-01-10 07:37:17', '2021-01-10 13:07:17'),
('412bceaeecd1ab67424cf031f65663aad21676f964893cd86bc857dbca83cc70773299a12f669e99', 2, 1, '*', '[]', 1, '2020-01-08 01:57:07', '2020-01-08 01:57:07', '2021-01-08 07:27:07'),
('41989c24e3bea51acb3406ea28413a072531dafee4c5438251411b3f5dac1a104dd95630142699e4', 77, 1, '*', '[]', 1, '2020-01-15 08:39:27', '2020-01-15 08:39:27', '2021-01-15 14:09:27'),
('41b604cfa8f1aac89105be082c1b6d58769bd8f7d9b5b5a810a89e576f81a6052ad19296edcb33cc', 14, 1, '*', '[]', 1, '2020-01-09 05:09:22', '2020-01-09 05:09:22', '2021-01-09 10:39:22'),
('42018d90ec3681517a7a6e151b26822c837a3460647fd4397e32e6af0ee1dd7acaf777f5c0e7aa6d', 1, 1, '*', '[]', 1, '2020-01-09 03:58:04', '2020-01-09 03:58:04', '2021-01-09 09:28:04'),
('42a23c7eabb3d38c4fa51ea7338936afb04c6d18696ab07f0b7319041b686ca6af44994ef2c8111e', 4, 1, '*', '[]', 1, '2020-01-17 02:31:42', '2020-01-17 02:31:42', '2021-01-17 08:01:42'),
('42b4887bf2d4eaeba387c61c09f3823c9ba931eaf92be8d09dbcf1e18806f9cd637f329c5d252702', 36, 1, '*', '[]', 1, '2020-01-12 23:17:36', '2020-01-12 23:17:36', '2021-01-13 04:47:36'),
('42be643dca7a1a0e28b34b900117b1cc956843e96731a57c21cca708b9ca2b272c13f930b89916f7', 4, 1, '*', '[]', 1, '2020-01-17 01:18:00', '2020-01-17 01:18:00', '2021-01-17 06:48:00'),
('434778134a9739cf80157b114da2cc80f83ce1488c997f4095c607f3fcbde15d037e9e04ed5a73ed', 7, 1, '*', '[]', 1, '2020-01-09 03:48:11', '2020-01-09 03:48:11', '2021-01-09 09:18:11'),
('439dbb8c2ccec7c98592c7433ad9b9ee82acec76fe75e68db1df21171cf8a8d04407af53deb5fb6b', 94, 1, '*', '[]', 1, '2020-01-16 11:47:23', '2020-01-16 11:47:23', '2021-01-16 17:17:23'),
('43a6a399b7b3520417a77bff0b67860a9ef6974693f5f1c9f5944f4a31e8d0e637c6be54b37d7d44', 30, 1, '*', '[]', 1, '2020-01-09 23:35:43', '2020-01-09 23:35:43', '2021-01-10 05:05:43'),
('4416f1fffda585c0b9d46ec16a506987db41a5f872cedd3753639ddaea32f6981bd8c23acb62191d', 2, 1, '*', '[]', 1, '2020-01-18 03:00:51', '2020-01-18 03:00:51', '2021-01-18 08:30:51'),
('44210f31a9fe3b018ed0d18f08c195c97de171c9c6e0e57361a14bdf9952b99bd6f60de9378eb45b', 62, 1, '*', '[]', 1, '2020-01-16 06:18:56', '2020-01-16 06:18:56', '2021-01-16 11:48:56'),
('44b06e1720a8018ec5fd4e48ad71e6d8333f4a2ed81a9db773c7fda0c3f09472979aefb767a0ec6a', 36, 1, '*', '[]', 1, '2020-01-10 04:25:49', '2020-01-10 04:25:49', '2021-01-10 09:55:49'),
('44bd597577262db702051810278750290f3fe3bf62e991d7c536fdca9d25be7fa0bce3cd3458c323', 1, 1, '*', '[]', 1, '2020-01-20 01:27:27', '2020-01-20 01:27:27', '2021-01-20 06:57:27'),
('453c0f14aaf04f06cf80156465913600533ca4647fbff3bcbf0a6d0f801512a71e65486f02afbc13', 2, 1, '*', '[]', 1, '2020-01-17 02:36:43', '2020-01-17 02:36:43', '2021-01-17 08:06:43'),
('453edb53ec0ce853eb0cd1600fe05f5ecfd3c49ed7ff5fb985f9d8c86fda5bd3975461119e33279c', 81, 1, '*', '[]', 1, '2020-01-16 05:26:30', '2020-01-16 05:26:30', '2021-01-16 10:56:30'),
('454c9f7e758e89207dc83dcaa71b2a7471894b2379698b14f77d0e7084e79f3db1827e6f743992d4', 20, 1, '*', '[]', 1, '2020-01-13 01:22:57', '2020-01-13 01:22:57', '2021-01-13 06:52:57'),
('455037bbf79108a44d610eb1f162563bf07852fdd7add713872fcc1b1e122933f5110f0abebb1c92', 1, 1, '*', '[]', 1, '2020-01-17 05:53:23', '2020-01-17 05:53:23', '2021-01-17 11:23:23');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('45540ae2a326c74d4f8c12a3f42f793645acce9c1d36b959a4e5c9f122486bcaae5acc3d096c9e9b', 81, 1, '*', '[]', 1, '2020-01-16 05:54:49', '2020-01-16 05:54:49', '2021-01-16 11:24:49'),
('4566485abd6abb0c477284c30432ab153b1b81338bf3be49f116d33ec812e6191df89ee84ee5a60a', 62, 1, '*', '[]', 1, '2020-01-15 04:55:58', '2020-01-15 04:55:58', '2021-01-15 10:25:58'),
('4589bf859c2e69a7c257219768d2ca6c7ff3c4f2d60b7ab3bdbe75b343af3346ec3d6745c10f5352', 5, 1, '*', '[]', 1, '2020-01-17 03:56:01', '2020-01-17 03:56:01', '2021-01-17 09:26:01'),
('45a13e3a388cbdf2fa3ac70a580c9953a640ebe42b89259467702148fac05808c6819f092e6730c9', 62, 1, '*', '[]', 1, '2020-01-14 05:40:22', '2020-01-14 05:40:22', '2021-01-14 11:10:22'),
('45d02cce43461b5857839706ffcd4a55ea06382f2c0c5bd65fc77ff624a931ee881928c78ad5968f', 1, 1, '*', '[]', 1, '2020-01-09 01:02:56', '2020-01-09 01:02:56', '2021-01-09 06:32:56'),
('45d5e6ab2c7a26439c851c0b93b95acf3a436bae0dd0020af9fd1cfab06a1266103e178f014cb310', 3, 1, '*', '[]', 1, '2020-01-17 05:40:11', '2020-01-17 05:40:11', '2021-01-17 11:10:11'),
('464511486d1a19e6c74a2b8eb02a584cb3f645ef3d927138bf18dd7e2749a461015d2bd351334099', 36, 1, '*', '[]', 1, '2020-01-13 00:20:06', '2020-01-13 00:20:06', '2021-01-13 05:50:06'),
('4652b6990906c495ee4df9327fb7d4b0b46081475b3907cf8d9411a5007e669e533d6440d8a60ab2', 62, 1, '*', '[]', 1, '2020-01-16 07:53:27', '2020-01-16 07:53:27', '2021-01-16 13:23:27'),
('469393bf549ce492138021fc04865025824ee14580fa9587ad767f9d3a608798cc8359fcaac3907a', 36, 1, '*', '[]', 1, '2020-01-10 01:45:15', '2020-01-10 01:45:15', '2021-01-10 07:15:15'),
('469c7f7f2672e7b7c5938e707151b02b1a697cfd75df993cd8e4c955e515307c74df23483d496706', 1, 1, '*', '[]', 1, '2020-01-08 07:34:53', '2020-01-08 07:34:53', '2021-01-08 13:04:53'),
('46cc79e82e268de8fda4fcc500ffa894feb14ec4d2ec0637d3713203522397a8d8ed4d3215a5e6e0', 62, 1, '*', '[]', 1, '2020-01-14 06:55:12', '2020-01-14 06:55:12', '2021-01-14 12:25:12'),
('47468f975ba05b3423f1e96520909506b30b25ab8d0ab9d77a0d4b1f819217d3035e876bd86cf6e3', 1, 1, '*', '[]', 1, '2020-01-19 23:32:32', '2020-01-19 23:32:32', '2021-01-20 05:02:32'),
('476e86e8d204d206eab6cc2aabcbb994eab9470572274726673a1b8f1d6f0afaca0099f8b80d4158', 41, 1, '*', '[]', 1, '2020-01-10 02:32:17', '2020-01-10 02:32:17', '2021-01-10 08:02:17'),
('482178fde639cca19b7e7eb0a4430bea0ebb8d25044aa4c4cd644bed7e86c86b7c7202a7e54b6694', 3, 1, '*', '[]', 1, '2020-01-15 02:38:01', '2020-01-15 02:38:01', '2021-01-15 08:08:01'),
('4a4dbc386e355362b0d949b70b5aa3a1ad1e681e58cb640495a06724a9ed4443afcb32635d75ff0d', 3, 1, '*', '[]', 1, '2020-01-17 04:33:58', '2020-01-17 04:33:58', '2021-01-17 10:03:58'),
('4a53587a14487934bf9f34b3a988bb44e3e9b73dea14a3fc191529f7b9bc807c81b84f9d1342e75a', 1, 1, '*', '[]', 1, '2020-01-17 01:14:50', '2020-01-17 01:14:50', '2021-01-17 06:44:50'),
('4a9ab081f71ef9766cfbea736abf095b22ad026cb657578902d34850bdeee1f3e5b63eb8389ce6ff', 81, 1, '*', '[]', 1, '2020-01-16 05:25:49', '2020-01-16 05:25:49', '2021-01-16 10:55:49'),
('4affd085475ab1e159ace0f6e954f198d107ec03fc6e5a4ef9d6ded3706a30eeb4de699999ccd203', 74, 1, '*', '[]', 1, '2020-01-16 01:28:36', '2020-01-16 01:28:36', '2021-01-16 06:58:36'),
('4b0ca69a2a73bfdb415b2f543baa412bf181cb3b85fa10fd8f697f0e021c8e8d5abe219113d0ca55', 62, 1, '*', '[]', 1, '2020-01-15 02:08:26', '2020-01-15 02:08:26', '2021-01-15 07:38:26'),
('4b1773851cb68bd16c5c5b67687b39e1e239c093cbd321fa7495128220abf88d43e99e42de334e0a', 94, 1, '*', '[]', 1, '2020-01-16 11:37:19', '2020-01-16 11:37:19', '2021-01-16 17:07:19'),
('4ba9509dfb5956da42f14498293c3aeba04d2e6fce82bf3b0fa00715e1b289d306c81df6fe91d96c', 1, 1, '*', '[]', 1, '2020-01-08 02:12:54', '2020-01-08 02:12:54', '2021-01-08 07:42:54'),
('4c26e58ed95ea5da8116d42e1cf94be7e9a586df201763d09dc0f430cf48f5554577c11e756274b6', 62, 1, '*', '[]', 1, '2020-01-16 05:14:25', '2020-01-16 05:14:25', '2021-01-16 10:44:25'),
('4c436a7ce859bbd2c0a6081e5fa4a906942634f90afd500f936757714939dd74b993f342b84e1433', 1, 1, '*', '[]', 1, '2020-01-20 00:11:09', '2020-01-20 00:11:09', '2021-01-20 05:41:09'),
('4c7b0495ca1443e3602968744ff50ef2a1f8065718b423cb43d082382be299a6cfa8d852b31e7a36', 62, 1, '*', '[]', 1, '2020-01-14 04:01:54', '2020-01-14 04:01:54', '2021-01-14 09:31:54'),
('4c7c3f460aa115b9227815d82eb4fad15a987cbebecc4565f85492a8a291fca34c2e3a7f33a57551', 1, 1, '*', '[]', 1, '2020-01-10 04:09:06', '2020-01-10 04:09:06', '2021-01-10 09:39:06'),
('4ca8b20a60e75d883bdf39d713481a51c85295ff7600f8ba11b566493679d6b585b116a3706b848e', 4, 1, '*', '[]', 1, '2020-01-14 03:11:52', '2020-01-14 03:11:52', '2021-01-14 08:41:52'),
('4ccac9da8eee01339fb92902fa964e5d8fd93ea78fae36dca85c8df892d06371f652ed966db1ad33', 7, 1, '*', '[]', 1, '2020-01-09 03:53:10', '2020-01-09 03:53:10', '2021-01-09 09:23:10'),
('4cd4cc55b920c445e569e8ae57ce912d74ebc3c9a41af94667c12dabd8e9ca5ccb2e3246507159e2', 36, 1, '*', '[]', 1, '2020-01-12 23:38:51', '2020-01-12 23:38:51', '2021-01-13 05:08:51'),
('4d46522f9c2f0e52dbcaf23fa6c4b0063f95dd8d45d5b4b187ae794f8a884ffca3c81b33e1d9021b', 3, 1, '*', '[]', 1, '2020-01-15 03:44:57', '2020-01-15 03:44:57', '2021-01-15 09:14:57'),
('4df2720084459c90ca59745373b3c7a4b03735ceedb1eb105cec9dc4c9d4d930583d90f5fe6deb62', 36, 1, '*', '[]', 1, '2020-01-10 08:53:16', '2020-01-10 08:53:16', '2021-01-10 14:23:16'),
('4e0912ad12cf9e85f0e0931c1a84e5f27d7a5949d2755e00a54e456a7287efd7b35607d824f21ae7', 74, 1, '*', '[]', 1, '2020-01-16 02:04:39', '2020-01-16 02:04:39', '2021-01-16 07:34:39'),
('4e12035323df730197b7ef4423cf1c20175b4ef5a435d5e566f23f507bb66e388a644390183d1784', 66, 1, '*', '[]', 1, '2020-01-16 08:20:08', '2020-01-16 08:20:08', '2021-01-16 13:50:08'),
('4e4c5b28243815e463470c9f2c7ee9f6cb599946dc603ffa718b119e0e5b8e12ad781565f1b2e604', 32, 1, '*', '[]', 0, '2020-01-10 00:48:55', '2020-01-10 00:48:55', '2021-01-10 06:18:55'),
('4e4c5f0a0a346b9626235df8a6f4d18c144bb20cda3671370c977b292f6264f3d16e1574af966469', 7, 1, '*', '[]', 1, '2020-01-09 03:53:08', '2020-01-09 03:53:08', '2021-01-09 09:23:08'),
('4e77be3127b7d618cfc46f5e6a1b36b16179622cc6593887995bd4b9a5fcce455d4fbd39641eb205', 7, 1, '*', '[]', 1, '2020-01-09 02:16:43', '2020-01-09 02:16:43', '2021-01-09 07:46:43'),
('4e901d4161417e37fd69530ea41baa938549f74227fe4afb83f213bcd0fc47cfb6e662af653b0019', 1, 1, '*', '[]', 1, '2020-01-20 01:12:16', '2020-01-20 01:12:16', '2021-01-20 06:42:16'),
('4eedbec6a559164dd00d1f5393d90db60ec3317d31e31df5a6e79e9e491ebb6da7b73ef8fa5d32a8', 2, 1, '*', '[]', 1, '2020-01-08 02:35:15', '2020-01-08 02:35:15', '2021-01-08 08:05:15'),
('4f2103f1527225a1dd5eee30f5e4aa69f355b349f82ccde401ddb7f9f8028d1b34b016a9700641e8', 63, 1, '*', '[]', 1, '2020-01-16 02:49:18', '2020-01-16 02:49:18', '2021-01-16 08:19:18'),
('4f4294f04563adede602f241bbbce06a137ab3fb5dacde2bbbef89766808fb70d807d6a53c0baebe', 62, 1, '*', '[]', 1, '2020-01-15 23:48:39', '2020-01-15 23:48:39', '2021-01-16 05:18:39'),
('4f490ba8ed1b43bac563e91ed0e3065d071cd790172ad43aaa9b053f4f6b3f5fc954b96209360561', 62, 1, '*', '[]', 1, '2020-01-14 06:45:10', '2020-01-14 06:45:10', '2021-01-14 12:15:10'),
('4f610aa5a34040d1c21a0638479427a24ea49a5af5c5a42430915614daed4bce546580e3b7d3e274', 4, 1, '*', '[]', 1, '2020-01-15 04:46:05', '2020-01-15 04:46:05', '2021-01-15 10:16:05'),
('4ff0cb95d5f2d44927cca46f4b44f8d5dc5a1369e39442bbb4de4384de1a497d96221b31f445cadc', 62, 1, '*', '[]', 1, '2020-01-16 02:52:48', '2020-01-16 02:52:48', '2021-01-16 08:22:48'),
('4ff404c5a4499b636d414c0ffefe86627aff78d2c0c67bad94cdddda0f6f6fbc28a6dbc7b7061071', 62, 1, '*', '[]', 1, '2020-01-14 03:19:50', '2020-01-14 03:19:50', '2021-01-14 08:49:50'),
('5', 2, 1, '*', '[]', 1, '2020-01-08 01:32:14', '2020-01-08 01:32:14', '2021-01-08 07:02:14'),
('500f782330d562e253b23ad8af4d5a29c37ea873854986254e7649c964cabd89bef0b143120eaf3f', 5, 1, '*', '[]', 0, '2020-01-20 00:10:17', '2020-01-20 00:10:17', '2021-01-20 05:40:17'),
('501ae6ac0a40aa04bb9303442b375ab4bf3e85a29b0cededc3ecb53482fcc1349679adbbd2bac88f', 62, 1, '*', '[]', 1, '2020-01-14 05:55:58', '2020-01-14 05:55:58', '2021-01-14 11:25:58'),
('50345d893cb6573be7419f8c2b759e042b0cc418c52b45d56c9506cf6e2a5bb16a637367b833f75d', 62, 1, '*', '[]', 1, '2020-01-14 05:46:24', '2020-01-14 05:46:24', '2021-01-14 11:16:24'),
('51333441866ccca16347095fdbda1dc7b4be4a45e56838ad0dfc776aa08c0c172f6113ba6f92cc4b', 62, 1, '*', '[]', 1, '2020-01-15 05:31:52', '2020-01-15 05:31:52', '2021-01-15 11:01:52'),
('5178a85245dd0adf6c310d0b45e4306ea680fbb7cec1208118668602ade155a4a07ba1016092cbd9', 62, 1, '*', '[]', 1, '2020-01-15 04:52:14', '2020-01-15 04:52:14', '2021-01-15 10:22:14'),
('51834c5fe6c997a3e8255f1b576e01aa72f4f2ab1e768464977696a6e4bb3200b3b98009b3b9c50d', 4, 1, '*', '[]', 1, '2020-01-14 03:37:13', '2020-01-14 03:37:13', '2021-01-14 09:07:13'),
('518cd6695d2c862de885426207f41da3890127005c3ee669aa0f45005e798b379eacf1a947d923df', 2, 1, '*', '[]', 1, '2020-01-08 03:24:45', '2020-01-08 03:24:45', '2021-01-08 08:54:45'),
('51b74995713de1625d0ffeb67242cc4b9019b60953bff0684687ab34b15025ba92e52f093b176404', 62, 1, '*', '[]', 1, '2020-01-16 03:58:09', '2020-01-16 03:58:09', '2021-01-16 09:28:09'),
('51ce346e8f4c838e0f9625daf86e60aa9ad307a21a09991f0cb26538c4830c7230831d14e6d73f92', 62, 1, '*', '[]', 1, '2020-01-14 03:05:47', '2020-01-14 03:05:47', '2021-01-14 08:35:47'),
('51cf0f7a462053b7106f1042ae4784a4a13bbeb227e0af071b55189480db936b24ddb47e13c49d3e', 2, 1, '*', '[]', 1, '2020-01-20 01:43:44', '2020-01-20 01:43:44', '2021-01-20 07:13:44'),
('51ec0b9de919bbabcbd44df87184d78cc86d2a39f9fbb18dcdfc34ac0b67d1403a0153b44b12c588', 3, 1, '*', '[]', 1, '2020-01-10 00:23:27', '2020-01-10 00:23:27', '2021-01-10 05:53:27'),
('51f6917067a93c98018304d1658888521c186298788110921bbd8e07499d39ff1f1f9ea9fe4ab28d', 3, 1, '*', '[]', 1, '2020-01-15 04:11:55', '2020-01-15 04:11:55', '2021-01-15 09:41:55'),
('5266530b6d25b752c06750d45ba724dddf24cfb133311c7ce1f0e8144ec93b46ae9dea1055899985', 62, 1, '*', '[]', 1, '2020-01-14 01:11:48', '2020-01-14 01:11:48', '2021-01-14 06:41:48'),
('52bafe524cd1dfa06bdbcee000cd13a9b3abc5226e0bb1f41279f43097934242292ea4ea1b7b9a3e', 74, 1, '*', '[]', 1, '2020-01-16 02:50:55', '2020-01-16 02:50:55', '2021-01-16 08:20:55'),
('52d57e29a5cbf3e838ba5a9193f15a392392a415db7568bcd93fd468bcd3fa71a34981590b37ebe2', 81, 1, '*', '[]', 1, '2020-01-16 05:19:41', '2020-01-16 05:19:41', '2021-01-16 10:49:41'),
('5329e054c96d485d15d3bab25e3cfa5d0151fc5c44b93970c3ef3a52a7f9b44a75a101a102643b72', 1, 1, '*', '[]', 1, '2020-01-08 07:24:49', '2020-01-08 07:24:49', '2021-01-08 12:54:49'),
('53364442c049482291fc734c8fe5de4d8ac299f602c233f61815a70801c340291753ebbf66418faa', 80, 1, '*', '[]', 1, '2020-01-16 06:27:07', '2020-01-16 06:27:07', '2021-01-16 11:57:07'),
('53906277e9a7d591c1c4df686edf08e82f590f03708029be002f6807044733734ee8909e0a3bbcf6', 20, 1, '*', '[]', 1, '2020-01-09 07:21:20', '2020-01-09 07:21:20', '2021-01-09 12:51:20'),
('53db96f3abfed34564743186b9cb9d3e8dadd092176c85d3b0118efe7a25e1df75a6b2b24cd41741', 8, 1, '*', '[]', 1, '2020-01-17 06:51:23', '2020-01-17 06:51:23', '2021-01-17 12:21:23'),
('542c33e24fa60fa368a5e3343c7a1fb71defb39c5240af518f2b06295732f84e36a89c7ff04d3179', 29, 1, '*', '[]', 1, '2020-01-09 23:00:34', '2020-01-09 23:00:34', '2021-01-10 04:30:34'),
('544efa9da2568040758d362b8279b854aa0f21e210946c17a7c45bb1365c2869240a368be6150950', 1, 1, '*', '[]', 1, '2020-01-20 01:34:20', '2020-01-20 01:34:20', '2021-01-20 07:04:20'),
('545873bf91e9a6d120ce5874dad4c0bafb0e3cf40a4bf6a729fbe47490a73c6ee947f68432754fda', 62, 1, '*', '[]', 1, '2020-01-14 03:45:52', '2020-01-14 03:45:52', '2021-01-14 09:15:52'),
('546dab7c0c6e14929ffe4f18c3147b5aceb9bdbc42e6d649d66f5a4991c2fb1eac91471e83bf663e', 36, 1, '*', '[]', 1, '2020-01-10 08:49:42', '2020-01-10 08:49:42', '2021-01-10 14:19:42'),
('5473494dd1a844f5c13261721046919903d165d527c6b32a11e8895d61234724ac7915fcb1330ab7', 10, 1, '*', '[]', 1, '2020-01-18 02:39:10', '2020-01-18 02:39:10', '2021-01-18 08:09:10'),
('54851c16412c7c090a9e7eb66817bde76f9b23f8e5c8de13223f000b89a6d1570585364e4621dc69', 80, 1, '*', '[]', 1, '2020-01-16 05:28:55', '2020-01-16 05:28:55', '2021-01-16 10:58:55'),
('551a46842ae15202f872f193384d1ccee0ec3d211ad65db083de848592977651c5f59958d066ae43', 62, 1, '*', '[]', 1, '2020-01-15 06:44:47', '2020-01-15 06:44:47', '2021-01-15 12:14:47'),
('55415ce8ea3b1a10757ddf251b4fc1b3cb5fc5c0fd212d5e889634e75138d78df3bfe4641e0f9ac2', 12, 1, '*', '[]', 0, '2020-01-17 09:09:05', '2020-01-17 09:09:05', '2021-01-17 14:39:05'),
('556e226b7bf52e7090d4eaa2417f20a833f559ae5baa249c76d192f4a5b79e6e5b422b74942e311b', 2, 1, '*', '[]', 1, '2020-01-08 03:55:38', '2020-01-08 03:55:38', '2021-01-08 09:25:38'),
('55d0b7a97ee6383ae2a9c189f929ef1f13e9fa7daf0e56914c22b14e786ea8245a0f2ece7135b270', 2, 1, '*', '[]', 1, '2020-01-20 00:37:44', '2020-01-20 00:37:44', '2021-01-20 06:07:44'),
('5627d487dbfd45fd1573b4160b58ac8d391871fd3c2236efc22167bb91119b30a3ec69c8051344d5', 62, 1, '*', '[]', 1, '2020-01-14 08:07:02', '2020-01-14 08:07:02', '2021-01-14 13:37:02'),
('567dc5bd6098ffb260f985f8596f343d92cd4e67ae9c1fec06d52e7232fbfa01a5af2a246bcd85bd', 62, 1, '*', '[]', 1, '2020-01-14 06:43:33', '2020-01-14 06:43:33', '2021-01-14 12:13:33'),
('56c9cad5fd4eecc988de136c53369f0bca8a775022db369dad96108b33c70d7ab0811b16fa61e071', 2, 1, '*', '[]', 1, '2020-01-20 00:51:37', '2020-01-20 00:51:37', '2021-01-20 06:21:37'),
('5700982fb09a14e92c6d5eba6520c53373bec4aff5f26487e5b8ff0332b55a9527b03661a3ccfa65', 1, 1, '*', '[]', 1, '2020-01-08 07:53:36', '2020-01-08 07:53:36', '2021-01-08 13:23:36'),
('57383a73e0370c57128466242f4bb6a224d097351e1b8da25c078bb86a22330702d287cf4dce3d6a', 9, 1, '*', '[]', 1, '2020-01-17 00:34:37', '2020-01-17 00:34:37', '2021-01-17 06:04:37'),
('57a442262c90ac16088a0d1b63e6c557633b351c6209bd25835be802abb805158ce12fdcffcaf2bf', 2, 1, '*', '[]', 1, '2020-01-08 02:14:22', '2020-01-08 02:14:22', '2021-01-08 07:44:22'),
('57ba98608363178fbe26fd7c5a773829e5f2975212e71b91812b0db291d0c25de4e8f4c68391f64c', 62, 1, '*', '[]', 1, '2020-01-16 06:14:59', '2020-01-16 06:14:59', '2021-01-16 11:44:59'),
('57c339ef130ffa05068ef315898e97adde14938212978a23596011798118f8156759bb8e3682e77a', 8, 1, '*', '[]', 1, '2020-01-17 04:03:31', '2020-01-17 04:03:31', '2021-01-17 09:33:31'),
('5872db4d57c5d20342f44968d60a6fb48803c19a7c145c4d72d2705fc6e314a2c1ec92e6862f4510', 11, 1, '*', '[]', 1, '2020-01-17 00:37:26', '2020-01-17 00:37:26', '2021-01-17 06:07:26'),
('58fd0bfbb1e611134182a8a4bbc60e4b83f6f18b935ed5e79a3f4453ada100b4c9c340e547af15b0', 16, 1, '*', '[]', 1, '2020-01-09 05:17:46', '2020-01-09 05:17:46', '2021-01-09 10:47:46'),
('5911c4d548a8eee7ade1c3358d6180a6defced63ea0a203b1a0f086d5856af62fc50b33e5d787923', 3, 1, '*', '[]', 1, '2020-01-17 03:56:39', '2020-01-17 03:56:39', '2021-01-17 09:26:39'),
('5932589dddf9deadb74099912c86bc1bdff920ccc1572cec7224ec7ee19b56348faca98cbf4be30e', 4, 1, '*', '[]', 1, '2020-01-15 02:04:37', '2020-01-15 02:04:37', '2021-01-15 07:34:37'),
('5971f9f5c472b7420e6599c32d308d7c08e10c297f934fef5bca36a9ad306c2afca2786dbc6d283d', 62, 1, '*', '[]', 1, '2020-01-14 09:29:03', '2020-01-14 09:29:03', '2021-01-14 14:59:03'),
('597c1232edaa67ea44a045c49a9d54a73a168b3c37ef5e0a82837e5151edc72d6c2b3f335e1a77ff', 79, 1, '*', '[]', 1, '2020-01-15 23:10:46', '2020-01-15 23:10:46', '2021-01-16 04:40:46'),
('5a1f087356d678b8d3180399a9a2d4a46d67c5a91e332b07917013d86a829f47b136a6f937a5f831', 15, 1, '*', '[]', 1, '2020-01-09 05:03:59', '2020-01-09 05:03:59', '2021-01-09 10:33:59'),
('5a4b6464d5eb82f1c94167f48526d54fa50a263f9afec369c583a073663870711530fd8a53ac598f', 62, 1, '*', '[]', 1, '2020-01-14 06:08:22', '2020-01-14 06:08:22', '2021-01-14 11:38:22'),
('5a7153fbf68c39fae798ce46c85e0d690b18eaad3139708835e4b5ef5de4958383a3b50be98fa471', 74, 1, '*', '[]', 1, '2020-01-15 05:37:33', '2020-01-15 05:37:33', '2021-01-15 11:07:33'),
('5a79663fc53e98a3f8f67e4edf51fea696762088bdff83c9bd47de1c98db66a4f4fb8ba1372c602f', 2, 1, '*', '[]', 1, '2020-01-17 01:06:39', '2020-01-17 01:06:39', '2021-01-17 06:36:39'),
('5af52740b8bbc22847530eb549ab1de013d31b9a5bd2cf51dfd415312c1c5c1462c8298a04869b47', 62, 1, '*', '[]', 1, '2020-01-13 02:16:36', '2020-01-13 02:16:36', '2021-01-13 07:46:36'),
('5b0c3b647a09428ae089ef7786df7106783a8eb724cdaeb6b1d3fc93ac3a5355b06e12fc5d6332f4', 4, 1, '*', '[]', 1, '2020-01-17 00:17:06', '2020-01-17 00:17:06', '2021-01-17 05:47:06'),
('5b5cf284cc61f2ccbadded92d5d531e268fe14023735948f63a664844c3f2d13cc3557d33c53f113', 20, 1, '*', '[]', 1, '2020-01-13 02:57:01', '2020-01-13 02:57:01', '2021-01-13 08:27:01'),
('5becbc0fe89b759ee3b582bd19ffdc6dccf3a3ae719eb8ae96c5d8c2631fa0663ee5392ee8d9a606', 77, 1, '*', '[]', 0, '2020-01-15 08:44:38', '2020-01-15 08:44:38', '2021-01-15 14:14:38'),
('5c2bbe8454cd79198cca7ef6e029da6a8289b53072b434ac79a5261579f119d1373e21bd41af2b3b', 62, 1, '*', '[]', 1, '2020-01-13 23:49:52', '2020-01-13 23:49:52', '2021-01-14 05:19:52'),
('5c3cac0c59200f27278f91d1adab274426df2fa4861f2b9342d4f730c8b1bc8d607607d1ba1bd35c', 7, 1, '*', '[]', 1, '2020-01-09 02:53:18', '2020-01-09 02:53:18', '2021-01-09 08:23:18'),
('5c6f6c0969bad158bd00290d53ab2533a0f8c20bb751be465e3a9d4ac461114346fdc778fac93943', 62, 1, '*', '[]', 1, '2020-01-16 05:55:58', '2020-01-16 05:55:58', '2021-01-16 11:25:58'),
('5d605e739209f16fdc726abf9ec3aacd1a561a1609ef1265454c73cac46b9832ee45ec0651d2efff', 3, 1, '*', '[]', 1, '2020-01-10 04:13:02', '2020-01-10 04:13:02', '2021-01-10 09:43:02'),
('5d8a809016a6d20cf8b7b42577ac407698ee42568368b9f0dfb29207b5a707d25ed4b950faa53fa0', 4, 1, '*', '[]', 1, '2020-01-09 04:39:14', '2020-01-09 04:39:14', '2021-01-09 10:09:14'),
('5da7070e7253bfb2ab36603c6690662434214221dac1020a67227f175825e26bba9fdadf0f7afae5', 6, 1, '*', '[]', 1, '2020-01-17 03:53:57', '2020-01-17 03:53:57', '2021-01-17 09:23:57'),
('5ddd9b743509d21a4bcda84f0438335e3d37caa6af98feda726496a7e672d0fba963a5ce2df3eccf', 1, 1, '*', '[]', 1, '2020-01-08 07:50:24', '2020-01-08 07:50:24', '2021-01-08 13:20:24'),
('5e2df1880e626da5e0127f9938cdc65ed22a8e3164d89eac481ecfcfdb9bfa8fd31785c9dd5b55e2', 85, 1, '*', '[]', 1, '2020-01-16 05:10:14', '2020-01-16 05:10:14', '2021-01-16 10:40:14'),
('5e7036c2026c7429ae48ea54b5e9e1b45da86504515625c360166748683968acd541b01360149370', 80, 1, '*', '[]', 1, '2020-01-16 00:13:39', '2020-01-16 00:13:39', '2021-01-16 05:43:39'),
('5f39d9071f0116fc6652283ff8c02159a83c8d19a6b35f7082bdd3fea5ae3cf6315efb0541a45ff0', 33, 1, '*', '[]', 0, '2020-01-10 01:28:13', '2020-01-10 01:28:13', '2021-01-10 06:58:13'),
('5f96178e04eddd2dd4c78dababbb65dfbb94fdd0c64d4688bad1baf66eb7f9c18df7b6270966d1a6', 4, 1, '*', '[]', 1, '2020-01-15 08:13:29', '2020-01-15 08:13:29', '2021-01-15 13:43:29'),
('5fdc20f687e418586c261e636ec7b5a16610b5e71bb660c6a4baacc518e6c09b7e3f9a17b37f0779', 1, 1, '*', '[]', 1, '2020-01-17 01:40:07', '2020-01-17 01:40:07', '2021-01-17 07:10:07'),
('5fe49ba6072686f19e3f2d552f8e585eb67a9f4804908200132ebab918c4faa4ad6f66e1a5212688', 16, 1, '*', '[]', 1, '2020-01-09 05:18:12', '2020-01-09 05:18:12', '2021-01-09 10:48:12'),
('5ff8a3fbd3987d8a191606491d2eff150baa8ae44833460c4b9d7b8d370dd91e763d0c2ef081951c', 5, 1, '*', '[]', 1, '2020-01-17 06:24:50', '2020-01-17 06:24:50', '2021-01-17 11:54:50'),
('6039b8774dcea4f019426723c774c6790cb368fe3ba18d41ae1afc97865959166c3007b2c2f57bac', 62, 1, '*', '[]', 1, '2020-01-13 06:26:14', '2020-01-13 06:26:14', '2021-01-13 11:56:14'),
('605e23a16546ee1d0a3fe77cfb34d01fbdf178db15c7fd3ccd23e1457fe711a41f564bd11147b6f3', 62, 1, '*', '[]', 1, '2020-01-14 01:13:32', '2020-01-14 01:13:32', '2021-01-14 06:43:32'),
('608457b30aa6e0492363534ea2401dbe69dac6918eb6c908d1f47a5bb200f1e5d27ea5719dc0c54e', 85, 1, '*', '[]', 1, '2020-01-16 04:58:14', '2020-01-16 04:58:14', '2021-01-16 10:28:14'),
('60929b8653b941cba49f0ee488b38e1436624e5acb2e3af48218549c168d7be8613c16113f77e251', 2, 1, '*', '[]', 1, '2020-01-08 02:11:26', '2020-01-08 02:11:26', '2021-01-08 07:41:26'),
('60c8065094ff0576647244426957d9c606fe0d576b852075b50810a9ddf298f83c3d3ba202cde421', 28, 1, '*', '[]', 1, '2020-01-09 08:38:27', '2020-01-09 08:38:27', '2021-01-09 14:08:27'),
('60cb5951dfc19c120d5b077235fe35191698dbccfb22b875dc1dc28cfd0e2d38cde52a4c011c65c8', 56, 1, '*', '[]', 0, '2020-01-10 07:35:45', '2020-01-10 07:35:45', '2021-01-10 13:05:45'),
('60df76c95e271e79b0ee7ebaa60e796b2096100ac90a56bb3c1ff980806380d7e0909bdc9025b4d8', 34, 1, '*', '[]', 1, '2020-01-10 00:56:59', '2020-01-10 00:56:59', '2021-01-10 06:26:59'),
('60f7999f552157791ec052c6ed4891ab88cbb9c2a78801b0158752e1c007cb82dfef39f16ab38b66', 62, 1, '*', '[]', 1, '2020-01-13 03:44:54', '2020-01-13 03:44:54', '2021-01-13 09:14:54'),
('612d3db0bb90c7503fedccc7e4a39cd5665bf6e23b999211dd1bc291abc5aead29c00ce670690679', 62, 1, '*', '[]', 1, '2020-01-13 06:19:28', '2020-01-13 06:19:28', '2021-01-13 11:49:28'),
('616f75cb449e395dbc02d5eb9eca4aeb8e522d2b63bc6155ac454af79b435788eda4bdc07c72f5d2', 7, 1, '*', '[]', 1, '2020-01-09 02:21:58', '2020-01-09 02:21:58', '2021-01-09 07:51:58'),
('61a3b93c4838976814ccc324f0d83ed650be094dccb4d7b80e23611a2aea2f84cb09c2bf541d1a39', 40, 1, '*', '[]', 0, '2020-01-10 02:30:39', '2020-01-10 02:30:39', '2021-01-10 08:00:39'),
('61a90f845e631b99420e1e04bbcc387d5822990aca6418851060172312c607a7339561c16e229d30', 62, 1, '*', '[]', 1, '2020-01-16 06:14:26', '2020-01-16 06:14:26', '2021-01-16 11:44:26'),
('6274863ada6bf4e9c6a1b66e2434f70d39d45c0148705aa8b2ff59d3693825339b1363b64d9261b9', 62, 1, '*', '[]', 1, '2020-01-15 04:47:36', '2020-01-15 04:47:36', '2021-01-15 10:17:36'),
('628e048f5de524435bb10037e59ae7746772e93d5be03a5c1d0463c052a1dc7efc5ce13ff54d1f0e', 70, 1, '*', '[]', 0, '2020-01-14 03:02:25', '2020-01-14 03:02:25', '2021-01-14 08:32:25'),
('63250a20633ecd66b587875200afca01e65fc5be2ecb21dc488396066e5742a4188fdf236225c180', 4, 1, '*', '[]', 1, '2020-01-15 06:47:22', '2020-01-15 06:47:22', '2021-01-15 12:17:22'),
('6326227d72f20460d95da316441bd4d8e68a3f39e92398954f58654794b67d7c97e8673566298c74', 5, 1, '*', '[]', 1, '2020-01-19 23:51:16', '2020-01-19 23:51:16', '2021-01-20 05:21:16'),
('6368b0fa03732c657cc7c436bf90c1a7290a100a761af273d12c757454671aa8308662e85921d89b', 74, 1, '*', '[]', 1, '2020-01-16 06:53:52', '2020-01-16 06:53:52', '2021-01-16 12:23:52'),
('63cce6ff69b970e016360beb799368a2e479d38ae037970aa95c30ef56f20ce8e0a5e9f0d1ab78b7', 7, 1, '*', '[]', 1, '2020-01-17 06:45:28', '2020-01-17 06:45:28', '2021-01-17 12:15:28'),
('6434ebab55233a67ac7f141a9e26462d0f4b426e43c27da1b7d3a7021f847e1446d3b1fbfa42ef7a', 3, 1, '*', '[]', 1, '2020-01-16 08:23:34', '2020-01-16 08:23:34', '2021-01-16 13:53:34'),
('64965dae3f984eb21e4b94c420cd4b0ba190a6c62b829bcb016e492e8b2849420a0558f837409bd4', 74, 1, '*', '[]', 1, '2020-01-15 06:33:01', '2020-01-15 06:33:01', '2021-01-15 12:03:01'),
('6498e2325a62adf864295918bbb02a05261fa77b2746d12073096fcb44ec484e61b16c117eec4ccc', 1, 1, '*', '[]', 1, '2020-01-12 23:17:27', '2020-01-12 23:17:27', '2021-01-13 04:47:27'),
('64a9975b910477727461976fd08d035f3f266364872108f8959f3f9c699f5877f801bc2001e7a4f0', 62, 1, '*', '[]', 1, '2020-01-15 06:24:10', '2020-01-15 06:24:10', '2021-01-15 11:54:10'),
('6522f5fbb1eb14365a9d606834bead1393c6b611275802ea66a23f937c44c57735399d851507fad5', 54, 1, '*', '[]', 1, '2020-01-10 05:32:40', '2020-01-10 05:32:40', '2021-01-10 11:02:40'),
('653b7dfe6dc656bcfda3a6d288f929c00faa2c82f3d013c8b26b7366cc422dadbfe3b32aeb148f80', 36, 1, '*', '[]', 1, '2020-01-12 23:37:42', '2020-01-12 23:37:42', '2021-01-13 05:07:42'),
('6576867b7cb0175c03f70172e73280923e4bf0641191ca0d459a4b23a3efd01f6cdc8445eb8634d5', 23, 1, '*', '[]', 1, '2020-01-09 07:59:08', '2020-01-09 07:59:08', '2021-01-09 13:29:08'),
('6586dee1f4e3c62380f61598f004d9aa4278d7c783413d33f10e8283eaf49db509ee5b5332668223', 7, 1, '*', '[]', 1, '2020-01-09 02:55:31', '2020-01-09 02:55:31', '2021-01-09 08:25:31'),
('65d2812bba06d531885800a797c461971265de28f15e0a5362c2810497bceece26cfafe9881043f1', 74, 1, '*', '[]', 1, '2020-01-15 22:57:23', '2020-01-15 22:57:23', '2021-01-16 04:27:23'),
('667633739c6aaa897dac5fbd5f62d97d0ef8d0a96cf2aba458a5343b8d7c053724ed4877236a5f75', 3, 1, '*', '[]', 1, '2020-01-15 04:50:54', '2020-01-15 04:50:54', '2021-01-15 10:20:54'),
('6676e946e006c4047a98dee9199710c7832252f9cedc2984584715eb0c0afbb9529442f0a5204e1f', 23, 1, '*', '[]', 0, '2020-01-09 07:59:23', '2020-01-09 07:59:23', '2021-01-09 13:29:23'),
('66a3bb6b3e5d0f6cab1406442a8473343e5692fdfb2ccb83e7bae47b65647576bf511a71fdfc51e2', 1, 1, '*', '[]', 1, '2020-01-13 02:05:15', '2020-01-13 02:05:15', '2021-01-13 07:35:15'),
('66e25ce14a53896e5d2f3a3cbd8b9a0608487d7bb7b169bbd6dc10477ee7a0c67c6bec963126be43', 62, 1, '*', '[]', 1, '2020-01-13 05:58:12', '2020-01-13 05:58:12', '2021-01-13 11:28:12'),
('674927edd553b018ff446cedafc0ed44fe514f8659449d3a529712414f987c6dde84fd637f78a669', 36, 1, '*', '[]', 1, '2020-01-10 08:57:48', '2020-01-10 08:57:48', '2021-01-10 14:27:48'),
('67aab89260c9034cd85686a2d20368f60a2caeb11cabf60e9abe208863c0d00693daebd0d51495c3', 10, 1, '*', '[]', 1, '2020-01-17 04:42:12', '2020-01-17 04:42:12', '2021-01-17 10:12:12'),
('67debc4c588c5ba38f1dd3e917fa3f8f77e5f43134a3e0b82e53b09a68f797dcd2ec6a3955a86bd2', 62, 1, '*', '[]', 1, '2020-01-15 06:55:06', '2020-01-15 06:55:06', '2021-01-15 12:25:06'),
('67f45be50faeb14a1cbcde9d07353ec1961f67e6c6c785f7f022c1f69137cd5656edcf1b16860c6a', 2, 1, '*', '[]', 1, '2020-01-08 03:40:06', '2020-01-08 03:40:06', '2021-01-08 09:10:06'),
('68aab24fa7bc62ad9b2b275d3f070e0b548b9afb04ed7b538cfdbea3abd8d026830ea18bf7d93c4b', 62, 1, '*', '[]', 1, '2020-01-15 06:41:28', '2020-01-15 06:41:28', '2021-01-15 12:11:28'),
('68b33e7733caa6cc7b4ddb2bbb5d417ad81b658d934df9780857cb65916ba28768b093e2b64a641f', 20, 1, '*', '[]', 1, '2020-01-09 07:20:03', '2020-01-09 07:20:03', '2021-01-09 12:50:03'),
('68bbc3a6775c2c85ac1e4b72d0f209c42323a177d5255320f24d884788fbb06854484778ec4241a5', 62, 1, '*', '[]', 1, '2020-01-13 07:16:42', '2020-01-13 07:16:42', '2021-01-13 12:46:42'),
('6983f7e3dea0d375e14c37ca2fd61d5c5c7af94991fb693c4a1e99243385c055049bbca53518f292', 20, 1, '*', '[]', 1, '2020-01-13 03:00:21', '2020-01-13 03:00:21', '2021-01-13 08:30:21'),
('6985d4d020cc1a359069c9a42453b3883f998ef3b7faa64fa0b00bbcab2edde109000d7072d27ca7', 63, 1, '*', '[]', 1, '2020-01-16 02:56:50', '2020-01-16 02:56:50', '2021-01-16 08:26:50'),
('69d6ec7c22944b2828c6e7ea740c8fbec2f50425a7a3f287bb0d10e55b3b17fdb5fcb36e6ecb885a', 3, 1, '*', '[]', 1, '2020-01-17 04:39:19', '2020-01-17 04:39:19', '2021-01-17 10:09:19'),
('6a2d159e173236a36fbedb2490262b6caccda0ae15a97449f1b329e838bef78c5e67f9ed0d6b4cc0', 1, 1, '*', '[]', 1, '2020-01-19 23:35:23', '2020-01-19 23:35:23', '2021-01-20 05:05:23'),
('6a41f215b0380056c3b6ab9c672bc4a013950e150d6b5c7b3420999ae1658b5cbd0a994fe4f59f97', 36, 1, '*', '[]', 1, '2020-01-12 23:41:41', '2020-01-12 23:41:41', '2021-01-13 05:11:41'),
('6a867d0197bd8df45fbd7562fc879a9107428baa32fbde9ccf1e9875dcb64ec8bafb773c9595f102', 62, 1, '*', '[]', 1, '2020-01-14 06:03:41', '2020-01-14 06:03:41', '2021-01-14 11:33:41'),
('6adc8d7a6f89b52d8101fb0554da4b0100093b93cb4869fec3d478254796740c141736b0b119a29f', 4, 1, '*', '[]', 1, '2020-01-15 07:20:42', '2020-01-15 07:20:42', '2021-01-15 12:50:42'),
('6af54ed7055e454f91baf7eb6de2be2e80c3a100694382926e2da6e508256cee8f4bdb7e220e10d8', 3, 1, '*', '[]', 1, '2020-01-09 03:59:02', '2020-01-09 03:59:02', '2021-01-09 09:29:02'),
('6b45937db94ce851fb1704debe92598149fb6c87a2c8d95d217a89554a5d0ab0047582389c7743c0', 4, 1, '*', '[]', 1, '2020-01-14 00:28:46', '2020-01-14 00:28:46', '2021-01-14 05:58:46'),
('6b5c970db043efa7fad3bae43c2be0d0151dbce448e8eae6a2cb0d9613e44195cc802b3b0e7cd2a7', 3, 1, '*', '[]', 1, '2020-01-08 03:44:17', '2020-01-08 03:44:17', '2021-01-08 09:14:17'),
('6b61dcab93a4d12e32069c66c361a91986258bd51f634ac51fe1f5e8800702c2aace01ec9d586e60', 85, 1, '*', '[]', 1, '2020-01-16 04:42:35', '2020-01-16 04:42:35', '2021-01-16 10:12:35'),
('6bc1e7656cb9d5aa4024046af257cd0b93d5d0ddc7d162e3b61d42aff3a8910553b4970dd9f5b461', 4, 1, '*', '[]', 1, '2020-01-15 04:46:10', '2020-01-15 04:46:10', '2021-01-15 10:16:10'),
('6c14924ce9d02b3ffe985cc97c63d488fdeda7c69220beb5ea5ff028aaacde7c07311730230316a1', 1, 1, '*', '[]', 1, '2020-01-16 23:59:22', '2020-01-16 23:59:22', '2021-01-17 05:29:22'),
('6cbb02514448f4485938bf00901e8ebc5c41c815b4cc55b8026a492957e051d4f0454e9a02311ce7', 62, 1, '*', '[]', 1, '2020-01-16 02:47:28', '2020-01-16 02:47:28', '2021-01-16 08:17:28'),
('6cc52f1c4178afc7541af6ed4c4dcddb4f951df830b12c44b0504a132ca7d0f036bdb8252fe654ce', 4, 1, '*', '[]', 1, '2020-01-15 04:54:05', '2020-01-15 04:54:05', '2021-01-15 10:24:05'),
('6cd052b39d2b1bb216e9747478a6e86b05e993366b764fbac311f5699c58036dbd994ddebe3d4bbf', 7, 1, '*', '[]', 1, '2020-01-09 02:54:34', '2020-01-09 02:54:34', '2021-01-09 08:24:34'),
('6d50389e33aedbdec49b26e0b3ad11d1a19c2883e75a8af46aa33abc3918da5b17e42a11341486b1', 88, 1, '*', '[]', 0, '2020-01-16 06:48:02', '2020-01-16 06:48:02', '2021-01-16 12:18:02'),
('6dd2c231cdb674f4b82b00bf172928ddcdbb5be28ada430b5eeda045fbc1dcdcdd58e2cfb02c14e4', 91, 1, '*', '[]', 0, '2020-01-16 07:09:57', '2020-01-16 07:09:57', '2021-01-16 12:39:57'),
('6e04c854079ee3fab42a07b625cd87f07a0e7f7a85b4ed48d7cbafc9cf83fe00faa9146ca20102d1', 74, 1, '*', '[]', 1, '2020-01-15 06:01:11', '2020-01-15 06:01:11', '2021-01-15 11:31:11'),
('6e2b94c8bb26717afcc833b7ccca0c209f7b9b5779c9a6c8168be845a503353d423107cd3e549f13', 4, 1, '*', '[]', 1, '2020-01-14 03:15:16', '2020-01-14 03:15:16', '2021-01-14 08:45:16'),
('6e3cac7a938670d93417fe19772cfb916cf7cfed41f84959f3219340a899f72316eae5103e60d68a', 80, 1, '*', '[]', 1, '2020-01-16 05:04:06', '2020-01-16 05:04:06', '2021-01-16 10:34:06'),
('6ebafe73efaddaff6053dda7c9f02ec9a8e252d3f3f530eec829676e911742161e604789f69d9ea4', 81, 1, '*', '[]', 1, '2020-01-16 06:08:40', '2020-01-16 06:08:40', '2021-01-16 11:38:40'),
('6f13d2cf299914afc7ccada2a633d7889efb6256e41cb2f014c9c49106afdce795dc7498f238180c', 3, 1, '*', '[]', 1, '2020-01-10 06:25:51', '2020-01-10 06:25:51', '2021-01-10 11:55:51'),
('6f4ed7b6743ada20ca86d764212f43aab518ce9f13fe8841eaa8dac852d01002ba04d00d95bfa686', 62, 1, '*', '[]', 1, '2020-01-14 05:17:28', '2020-01-14 05:17:28', '2021-01-14 10:47:28'),
('6f99bd8077abd64228fd8318d6deea9c651094ed3d9637661d6cefa24d13394286f7aff05de89927', 2, 1, '*', '[]', 1, '2020-01-20 04:46:30', '2020-01-20 04:46:30', '2021-01-20 10:16:30'),
('703e3ef6dad6b82c3bb7dce94b3845eb9ee62d9e53d73b65f348a59dbbc7357f1412fb79152b749c', 6, 1, '*', '[]', 0, '2020-01-20 01:19:49', '2020-01-20 01:19:49', '2021-01-20 06:49:49'),
('708676bddf50dfd1bbf1d00656f8334c86d2e43934a5215857be012d8c9a573b69562b6dc2b7acd5', 27, 1, '*', '[]', 0, '2020-01-09 08:37:46', '2020-01-09 08:37:46', '2021-01-09 14:07:46'),
('7094c3fc5f3788e4c7b40d2eb37cc4eba6f1da3a9824a39f6427c86132f8ee40e2b5ebd4f4e21efb', 1, 1, '*', '[]', 1, '2020-01-17 00:42:03', '2020-01-17 00:42:03', '2021-01-17 06:12:03'),
('70a99012c016f1b1161da76ddc15261e34531837362616551d7bd35d1c1e8c722f1faebcb5756eb1', 2, 1, '*', '[]', 1, '2020-01-17 00:46:52', '2020-01-17 00:46:52', '2021-01-17 06:16:52'),
('70cc0336ff22f4fc7ca32fd11c4fcb3c36daa110993b027e3731ea65acaf1cf7f1e14818438b8bdc', 74, 1, '*', '[]', 1, '2020-01-15 05:42:18', '2020-01-15 05:42:18', '2021-01-15 11:12:18'),
('7140f8b25f7c3ed94ad5d5f405aeefb47735db6ca179fdb07f4dcd730af91658ec2e4be5f63c33fb', 1, 1, '*', '[]', 1, '2020-01-20 01:32:15', '2020-01-20 01:32:15', '2021-01-20 07:02:15'),
('716b947e1e72c5562a5290c3286729dc5fb71ad8001323c4b43161e7bc10a94a06a6052a0eeebbe9', 3, 1, '*', '[]', 1, '2020-01-13 23:19:30', '2020-01-13 23:19:30', '2021-01-14 04:49:30'),
('71b868ac050c3c1cb9b0b0021962d4b76ad91498067be792bf7b6264bf513fea63b3679052660927', 63, 1, '*', '[]', 1, '2020-01-14 05:03:25', '2020-01-14 05:03:25', '2021-01-14 10:33:25'),
('71ec653a0feafdc3c4da1d6933626a13a8ca46e25479f25ed32f600e9508857de78202c26317e099', 62, 1, '*', '[]', 1, '2020-01-13 06:14:37', '2020-01-13 06:14:37', '2021-01-13 11:44:37'),
('72252722de3c7dc8c6cb3986b8586f656cc5e2d246413ff081796936a744c22eb99310808a0c8f2d', 3, 1, '*', '[]', 1, '2020-01-08 08:06:41', '2020-01-08 08:06:41', '2021-01-08 13:36:41'),
('7237d37d4af76b9ea14be79097c7276e700e17813e298d83e5ad4f1979f6ee5a2762eb0835a7111c', 62, 1, '*', '[]', 1, '2020-01-15 06:43:17', '2020-01-15 06:43:17', '2021-01-15 12:13:17'),
('723b804a2b9f73d3d35c0506378d039c271f4ef0448dd23eae57713cbcb5b350a22ca05542ba5d84', 3, 1, '*', '[]', 1, '2020-01-16 23:35:14', '2020-01-16 23:35:14', '2021-01-17 05:05:14'),
('728657e01184f15bf2675787c18c1d6f2668b8e096a9d80e6a4e13ac00477bb72e371e2507a6c7ae', 1, 1, '*', '[]', 1, '2020-01-08 04:02:37', '2020-01-08 04:02:37', '2021-01-08 09:32:37'),
('72b441fc44da6633a78ba1a12574035264898463ea936179941a19aea7af97af12992a9dc31fd779', 36, 1, '*', '[]', 1, '2020-01-10 08:58:11', '2020-01-10 08:58:11', '2021-01-10 14:28:11'),
('72bc12d3d492f13ce4dbc6b20e2f7a389eedb08ada39ffaba6fe9358fa860f0e67d11ffd1c0d367a', 3, 1, '*', '[]', 1, '2020-01-14 08:12:30', '2020-01-14 08:12:30', '2021-01-14 13:42:30'),
('7300c2ce883d964f53257a054cd4ec3eb9970c72fc4fddafcdd2d110b4c676d7210ee86039e70504', 3, 1, '*', '[]', 1, '2020-01-17 04:43:03', '2020-01-17 04:43:03', '2021-01-17 10:13:03'),
('734e25bc816ecb07128cc0ca3e52fa715ac2dfb875115df4cd33062707e126d79defb9f3e5d08224', 94, 1, '*', '[]', 1, '2020-01-16 23:11:48', '2020-01-16 23:11:48', '2021-01-17 04:41:48'),
('737ec77f52c6f0b9009b8b180d6d56de51462edd55c6417e8545bf7d37085137e5f7ca28151d1573', 1, 1, '*', '[]', 1, '2020-01-20 01:23:23', '2020-01-20 01:23:23', '2021-01-20 06:53:23'),
('73ce13f73647c729219261fb898e3d8acd02624596781769b6beeceda891de3ab80e8fe7f8676fcf', 3, 1, '*', '[]', 1, '2020-01-16 23:34:12', '2020-01-16 23:34:12', '2021-01-17 05:04:12'),
('742ea35811d0bbeeef8ecd755da117392d4934f9aa74e0517187dfce04e698e257ef1803e8036bdd', 3, 1, '*', '[]', 1, '2020-01-15 04:12:31', '2020-01-15 04:12:31', '2021-01-15 09:42:31'),
('747dd48292aa7d076fdb8a7ccbe0572fae7724652f4ea8dc25bb50d774764993b6007d195ef36cb6', 62, 1, '*', '[]', 1, '2020-01-14 23:15:24', '2020-01-14 23:15:24', '2021-01-15 04:45:24'),
('74c7ad360983cf746531d8aa6f3b21739b2dee03f176add8de5ece7a7e730ac044f88c8a8d20f0c0', 37, 1, '*', '[]', 1, '2020-01-10 01:56:30', '2020-01-10 01:56:30', '2021-01-10 07:26:30'),
('759c579913c99c1c2e176a6fe87ca948824191358e82dad8ea7f31d78c9d01483e904524b513bd6a', 36, 1, '*', '[]', 1, '2020-01-10 08:58:57', '2020-01-10 08:58:57', '2021-01-10 14:28:57'),
('7637e1bd358e5b59db06637cff6b3d511c498b44490cad2a7edba1228e6697b21f27640fff2af12a', 4, 1, '*', '[]', 1, '2020-01-16 02:22:35', '2020-01-16 02:22:35', '2021-01-16 07:52:35'),
('76b764e362b74983c7b97b56fdaba7bdbb4262a47e55201ded869b768c9f80e9721787c2171db14f', 92, 1, '*', '[]', 1, '2020-01-16 08:47:47', '2020-01-16 08:47:47', '2021-01-16 14:17:47'),
('774096bd68c95abc1d3fd129b0264e4f4c67ab6cdcfb46508bcb6e2dc491288a2dae8bd2bb840052', 62, 1, '*', '[]', 1, '2020-01-14 05:37:16', '2020-01-14 05:37:16', '2021-01-14 11:07:16'),
('779770363e832ce7635c6b1145a2bbf363e200f1a0b8ffcd2036af06ed6e6d70973b5954c5928515', 3, 1, '*', '[]', 1, '2020-01-13 03:46:36', '2020-01-13 03:46:36', '2021-01-13 09:16:36'),
('779c1d701453d86125c3ca70f77296a2a10ed746b70803fd5c994bc2b0eaea46f107e18e27c23224', 62, 1, '*', '[]', 1, '2020-01-13 05:59:22', '2020-01-13 05:59:22', '2021-01-13 11:29:22'),
('77e0195c24768e2f4d81c8af771c4839cbcb874768dbc8f8091e29310065fc15637d9b2a3c829954', 1, 1, '*', '[]', 1, '2020-01-20 00:08:15', '2020-01-20 00:08:15', '2021-01-20 05:38:15'),
('77e072d2964bfb624832e5022128c5bcd75c88fd08be0806e0b3b4d6c76f59640e8bb132c7a10c17', 62, 1, '*', '[]', 1, '2020-01-16 01:44:01', '2020-01-16 01:44:01', '2021-01-16 07:14:01'),
('78760c6c75c133e04e855f637af578013ce8a29c7def3b8d0d1936d4e2b19cd782e3a3b288733ad8', 62, 1, '*', '[]', 1, '2020-01-14 06:10:30', '2020-01-14 06:10:30', '2021-01-14 11:40:30'),
('78adb7f74889a3243658a1fc1f5a037d2975b79be9e033f6b3b03fcede56fa8d0e58fbe2f018e09d', 7, 1, '*', '[]', 1, '2020-01-09 02:54:30', '2020-01-09 02:54:30', '2021-01-09 08:24:30'),
('78c90a3d35268c50114c639dbf78dc31ed2cb5c6e720e10191d79c9256384616cb513b8976ae79f0', 62, 1, '*', '[]', 1, '2020-01-14 06:21:20', '2020-01-14 06:21:20', '2021-01-14 11:51:20'),
('791d5081ebeffd6f2259bb01ee24822e31eead0cf2dc9e427527c3ea040c8acbc3187bca1ef10334', 4, 1, '*', '[]', 1, '2020-01-13 06:58:12', '2020-01-13 06:58:12', '2021-01-13 12:28:12'),
('791dc54d8bc91716020e476d9ebb02c744d74e13f82a720c993772c2a3e1a03ac80780a0db7b7f22', 41, 1, '*', '[]', 1, '2020-01-10 02:32:40', '2020-01-10 02:32:40', '2021-01-10 08:02:40'),
('7a128c49542e5c66c6b9ca17d5a47bdbb191ce17fbf6b13b0f5c7cc1cd5eba28034ea170474094cc', 63, 1, '*', '[]', 1, '2020-01-14 05:01:28', '2020-01-14 05:01:28', '2021-01-14 10:31:28'),
('7a460657d7817542d0b344aee12a25d098f5233c4c48559a92b24881818f0bfe28fd6b1cb900f530', 2, 1, '*', '[]', 1, '2020-01-09 23:31:58', '2020-01-09 23:31:58', '2021-01-10 05:01:58'),
('7a4b934f567182b70a0f0d4daba50c183cd0c269806d7dcb1b7c965c647f4a3966445f069e93aa72', 3, 1, '*', '[]', 1, '2020-01-15 04:04:38', '2020-01-15 04:04:38', '2021-01-15 09:34:38'),
('7a4d111d4ca38b7374d1364dea8c00dd4ed814d2769709795763d1cb5258ba0bc393124a5cc8d4bf', 74, 1, '*', '[]', 1, '2020-01-16 04:20:23', '2020-01-16 04:20:23', '2021-01-16 09:50:23'),
('7a60a198cbc902753474861a23cca7160f058db2113e2203a9e54d013b2a6fb3b606192224dc769d', 4, 1, '*', '[]', 1, '2020-01-09 08:55:01', '2020-01-09 08:55:01', '2021-01-09 14:25:01'),
('7ad9b49446f2635ef4f81c1c3f5b0df5b3013863a77b546a139da29bc5e30edbbe520715a6482af5', 4, 1, '*', '[]', 1, '2020-01-15 04:45:01', '2020-01-15 04:45:01', '2021-01-15 10:15:01'),
('7b02e4b5c75df0728553bbda479730f8794a35c07903204bfe247f2b2563972ff35ee92b078c42d5', 62, 1, '*', '[]', 1, '2020-01-16 04:40:51', '2020-01-16 04:40:51', '2021-01-16 10:10:51'),
('7b6d054431beb52ed19f5d896c3a0b80aa98d71f966f348ea8965c9d7d9b34109214ba445c29016b', 48, 1, '*', '[]', 0, '2020-01-10 04:23:24', '2020-01-10 04:23:24', '2021-01-10 09:53:24'),
('7baca3565e7e6ca121dd1fa0fa6e0722af802874405252ea1d727d04d1c97667958e884c70e834cd', 3, 1, '*', '[]', 1, '2020-01-10 03:38:19', '2020-01-10 03:38:19', '2021-01-10 09:08:19'),
('7c05986d8a1f176319a3c0fe2a4d76e39f9a7271e7bb59b2d8a6e3309b9d0ceca21607e313e490c0', 7, 1, '*', '[]', 1, '2020-01-09 02:53:13', '2020-01-09 02:53:13', '2021-01-09 08:23:13'),
('7c5b4da396c837238d5e2d9a1114b5391961b41ce3a16975116bad805cbe855a30c90d9f86ab0e0b', 62, 1, '*', '[]', 1, '2020-01-15 05:41:04', '2020-01-15 05:41:04', '2021-01-15 11:11:04'),
('7c7ea817dfcf36d0d22696249f7e6c36a70078c86525c92210136da7fc1d8dc53071931f4b9ec205', 2, 1, '*', '[]', 1, '2020-01-19 23:51:46', '2020-01-19 23:51:46', '2021-01-20 05:21:46'),
('7ca2d7e8dff8c30fbe48e840ea4c39b04dafe6f093d726c14ac28f33cd39a5a25ba5708a578b4d79', 24, 1, '*', '[]', 0, '2020-01-09 08:19:00', '2020-01-09 08:19:00', '2021-01-09 13:49:00'),
('7d39682d7ceb6028217c26fe89d3edb164e6ff285905bea1755dc8bf3fb5e2b5a3d6b188f91e4dfc', 2, 1, '*', '[]', 1, '2020-01-20 01:13:55', '2020-01-20 01:13:55', '2021-01-20 06:43:55'),
('7d926ffbe539d3e8a13303ca10924c17ca99a40f6d7150c19ef42fc52953663c17ea2588f645ff11', 3, 1, '*', '[]', 1, '2020-01-17 04:41:22', '2020-01-17 04:41:22', '2021-01-17 10:11:22'),
('7d93d15b7aea3d742a65a14ced459fa3893046d03bd1776e3ae7e0c8b5a5561da91d296425e47350', 5, 1, '*', '[]', 1, '2020-01-17 04:55:23', '2020-01-17 04:55:23', '2021-01-17 10:25:23'),
('7dc6806765d7b25b96f2b89a7a349813790b9aa4ff08cd3e56ca3a614d5fd6d72529e89ecb3e3a3d', 62, 1, '*', '[]', 1, '2020-01-16 07:53:14', '2020-01-16 07:53:14', '2021-01-16 13:23:14'),
('7dd76698f552be2f4f02fd5133e74a11cd2a17ed2e97f4e1604e62ae70c92fa8836219d38bd4ca0d', 1, 1, '*', '[]', 1, '2020-01-08 03:38:25', '2020-01-08 03:38:25', '2021-01-08 09:08:25'),
('7dfcc654ca4739170b76d286a1687a7d15ef7430609f65afa29b289824334fe4f2171cb3935c8f8b', 20, 1, '*', '[]', 1, '2020-01-14 02:12:43', '2020-01-14 02:12:43', '2021-01-14 07:42:43'),
('7e03fe4f7b6178bbb48d9e3f420d631acf9e748de44a43750f4a87a23fd5a7be61a52c06f2b704c8', 3, 1, '*', '[]', 1, '2020-01-10 05:14:54', '2020-01-10 05:14:54', '2021-01-10 10:44:54'),
('7e5e2864356040dfc7fe7f791146f778b669415d5c8d9c9a4e9aa9b5a42f933d897c88398c2f0a73', 29, 1, '*', '[]', 1, '2020-01-09 23:10:00', '2020-01-09 23:10:00', '2021-01-10 04:40:00'),
('7e8d7b0f58a5031b4cab68f2c4557f97345248bb29a10140f06e15e0b350b7831138068a76317e6f', 63, 1, '*', '[]', 1, '2020-01-15 23:33:22', '2020-01-15 23:33:22', '2021-01-16 05:03:22'),
('7f06a7f9a1409e68efcbb5cd3d894524ed1d4b26ede963360868eb4ae9ef4ceed7a08de8b3e6dc68', 62, 1, '*', '[]', 0, '2020-01-16 23:14:02', '2020-01-16 23:14:02', '2021-01-17 04:44:02'),
('7f0855c0b3c5f414326cd140d6e9c2c06f045f0faa95bc42d585a54d05b835fda7e23d9f00f7f190', 62, 1, '*', '[]', 1, '2020-01-14 06:04:19', '2020-01-14 06:04:19', '2021-01-14 11:34:19'),
('7f6923529aadbb4a1b5870e61aefba7609e92517d5c9791befc7320bbc23b86504aee52b493fe999', 4, 1, '*', '[]', 1, '2020-01-13 07:18:15', '2020-01-13 07:18:15', '2021-01-13 12:48:15'),
('7f6d17cd46e2970a5d6364190b5b452ad260315e0569e2e643357ede62a711302aa67fbf9dd9b75a', 66, 1, '*', '[]', 0, '2020-01-16 09:10:57', '2020-01-16 09:10:57', '2021-01-16 14:40:57'),
('7fe3934846e7a957a1434e5abe034b5e17a7869b95a46e783a0bf82248696dbf9722c3b424e2fe3b', 63, 1, '*', '[]', 1, '2020-01-16 02:10:25', '2020-01-16 02:10:25', '2021-01-16 07:40:25'),
('8002246fd19ef54361232f38cee1b847b897aad4e0a9c67d87ae2783d9fe9417f0b0550854de092e', 1, 1, '*', '[]', 1, '2020-01-09 08:49:42', '2020-01-09 08:49:42', '2021-01-09 14:19:42'),
('8017b3b57478ad4ce3a72c7ced192a144a4c2e20a3f3d7cc6036cd5a7d89187d06d6a68d282517f3', 4, 1, '*', '[]', 1, '2020-01-15 00:00:24', '2020-01-15 00:00:24', '2021-01-15 05:30:24'),
('8094c06306b7ed48494d7c23ba08325924fd44625610663bea88a73dc47402cbdf1100971c60d1cc', 4, 1, '*', '[]', 1, '2020-01-13 05:54:56', '2020-01-13 05:54:56', '2021-01-13 11:24:56'),
('80baf54e28246334f1d34b2a9e1e45aa05da420374960c3a459d49a2c455b8e0b6ec7ebb3a0bd27a', 1, 1, '*', '[]', 1, '2020-01-08 23:39:24', '2020-01-08 23:39:24', '2021-01-09 05:09:24'),
('80cede03c2982d53edffc34d50014610e4571d644b303c4063e97f7efb124106ada725f8034494b8', 41, 1, '*', '[]', 1, '2020-01-10 02:30:57', '2020-01-10 02:30:57', '2021-01-10 08:00:57'),
('80dee1c291b1dcb118cb4183a2ae1dc8185cfb88b431d31e0341826864a89b3dccd44800655b81a4', 63, 1, '*', '[]', 1, '2020-01-14 05:00:57', '2020-01-14 05:00:57', '2021-01-14 10:30:57'),
('80dfd5378907cff2a7795979ff6d9c44f7fc6766f869c6d2aabdaa8aef0d3f0f713bbecfc11ba29a', 17, 1, '*', '[]', 1, '2020-01-09 05:19:07', '2020-01-09 05:19:07', '2021-01-09 10:49:07'),
('81094fb3f558fb214d463b3b7dfbc69d2684f0ddd9b8e046bb8656200a4f82227fff3e214f8a8042', 4, 1, '*', '[]', 1, '2020-01-15 08:13:07', '2020-01-15 08:13:07', '2021-01-15 13:43:07'),
('81428b9c716f33216a1a1418ad2e2825a23b9e00f4746f0777503b28630b29e4db6b1e86ee7b8a79', 20, 1, '*', '[]', 1, '2020-01-10 00:58:12', '2020-01-10 00:58:12', '2021-01-10 06:28:12'),
('816c3b641679f35d2c0b6c2cd28803e9b64b5d10af111414eeddda039848e9a5e681fbe6837267db', 4, 1, '*', '[]', 1, '2020-01-15 07:27:51', '2020-01-15 07:27:51', '2021-01-15 12:57:51'),
('817e46982e414bf5fa0697ed109e1189fa4f3cbd3e2d481fd4170622581dcaee3640bb79e41ca0da', 62, 1, '*', '[]', 1, '2020-01-16 05:46:13', '2020-01-16 05:46:13', '2021-01-16 11:16:13'),
('81e5ac9efad82d566961e30a21aa459ae2ce18490e1ceaac3814b66bdc0393f7f6a8204ed12758b0', 4, 1, '*', '[]', 1, '2020-01-15 04:48:07', '2020-01-15 04:48:07', '2021-01-15 10:18:07'),
('8230caf785593d07b2c15b7a252a6b3e7205e8f72de85e9a359648990eafa896098a36884a2c032c', 62, 1, '*', '[]', 1, '2020-01-14 05:39:45', '2020-01-14 05:39:45', '2021-01-14 11:09:45'),
('825782b07500f7e1fa28c2bde2c2edcbd6bd58409698d7c5226b34201921695d7febccdf1cbe4498', 1, 1, '*', '[]', 1, '2020-01-20 01:31:10', '2020-01-20 01:31:10', '2021-01-20 07:01:10'),
('8265b2ba9d862c64e32d8e1703ea1aa0e0af6b3d08618bbee315215f370af30f9d4884a92b47e851', 5, 1, '*', '[]', 1, '2020-01-17 03:55:31', '2020-01-17 03:55:31', '2021-01-17 09:25:31'),
('826a79716085cea5a40c49c5560565b6c69c268f0cbeabfbf12727d6ef4c0688f7ae5539cd0804b1', 1, 1, '*', '[]', 1, '2020-01-13 01:04:46', '2020-01-13 01:04:46', '2021-01-13 06:34:46'),
('827bc0a6362c4332f6ce1e34840a751d76a52084248360d5f64e82a7c963a783a29e48ec25023bdb', 1, 1, '*', '[]', 1, '2020-01-12 23:50:49', '2020-01-12 23:50:49', '2021-01-13 05:20:49'),
('82be5310a9a9153201fbcbc0480301fee4b306357d7b3349187a6fc1748773e0cfa3acf04991d0eb', 2, 1, '*', '[]', 1, '2020-01-08 02:39:32', '2020-01-08 02:39:32', '2021-01-08 08:09:32'),
('82d39a731a51ecc8ff556e3648410f0ff194f6081b6cc9a5ad7c3f3af19b31554b5a14f2ee5ccab3', 4, 1, '*', '[]', 1, '2020-01-17 06:38:48', '2020-01-17 06:38:48', '2021-01-17 12:08:48'),
('8340ef38d029e6fdc3bd105a051c0aa30851696b57a9030d8fe3930a023990d088e7b30d2922f9b4', 63, 1, '*', '[]', 1, '2020-01-15 02:35:31', '2020-01-15 02:35:31', '2021-01-15 08:05:31'),
('83453a80e67c03ee5670daaf681c35465a66644ee8b1b7a8eaba495ef0e7301e58afb5fa6ed6ab1d', 4, 1, '*', '[]', 1, '2020-01-10 00:21:52', '2020-01-10 00:21:52', '2021-01-10 05:51:52'),
('83d09b036a0688f4a20b266c956424a60e7218cc84a64ad4a764f74d79e5a62799e59f2f1b9b0959', 63, 1, '*', '[]', 1, '2020-01-16 01:41:38', '2020-01-16 01:41:38', '2021-01-16 07:11:38'),
('83ec2dc038538cafa6ed3c55108183f5c68f0567e93a1b8a3b6d716a53ebf528ef78b9835755aaed', 62, 1, '*', '[]', 1, '2020-01-15 08:34:29', '2020-01-15 08:34:29', '2021-01-15 14:04:29'),
('84041a5744bfede9c52de4c872bd06df7e3ef2b2ed5ef2c7fdb9c93149af42b53371379969575c87', 4, 1, '*', '[]', 1, '2020-01-17 06:55:57', '2020-01-17 06:55:57', '2021-01-17 12:25:57'),
('84188d662f6dc05b8ca23c32f3bfd55f69ed359e727a930eaaaae507dc90f0f75197c0c6ae108728', 75, 1, '*', '[]', 0, '2020-01-15 06:28:53', '2020-01-15 06:28:53', '2021-01-15 11:58:53'),
('842e3a843b946f6c6ab7784edc5daafde63f7aeddff0ccad7b9cddb5b645cde550b911960ff485bb', 3, 1, '*', '[]', 1, '2020-01-14 01:02:12', '2020-01-14 01:02:12', '2021-01-14 06:32:12'),
('844a2502e82ad6c3c845b9eadf3d249c652e0a32fd613d99ba98b75b98e9ce625cca6ea8fcb33db6', 1, 1, '*', '[]', 1, '2020-01-12 23:48:22', '2020-01-12 23:48:22', '2021-01-13 05:18:22'),
('84593df6c50131260a1c22a3835376cebf2fad9bf898a74a7e3ddfe882698127fa06e176158d1dc3', 2, 1, '*', '[]', 1, '2020-01-18 03:01:22', '2020-01-18 03:01:22', '2021-01-18 08:31:22'),
('8467d8f1b88841f42f7356bbae278a6d036edfe5e4ca96ed930655a49f9b531e98830b73296b652b', 2, 1, '*', '[]', 1, '2020-01-08 08:04:07', '2020-01-08 08:04:07', '2021-01-08 13:34:07'),
('849e52f9a41cbbe6c49949ade2c67d26c14fd94d9b53fc5549b67cc05f0b2fea5723bdd45b98be41', 2, 1, '*', '[]', 1, '2020-01-19 23:49:37', '2020-01-19 23:49:37', '2021-01-20 05:19:37'),
('84a92779654f69de22cd1c36be8f728ce1bd80131e096ed5494717f77b488882031ad7f8bdc64950', 3, 1, '*', '[]', 1, '2020-01-15 03:58:49', '2020-01-15 03:58:49', '2021-01-15 09:28:49'),
('84b2aab3867d0a849fa89939e0924bb6e27506d2d7bfd30f5fccaa9af84c0239126c664f1538dfb0', 75, 1, '*', '[]', 1, '2020-01-15 06:08:30', '2020-01-15 06:08:30', '2021-01-15 11:38:30'),
('84d4d71a9a715955b5212fa7ad8096bd2a1c54cfec9d40a28c80ceba5f67bf5ab3991514cb8bbf46', 3, 1, '*', '[]', 1, '2020-01-17 05:34:07', '2020-01-17 05:34:07', '2021-01-17 11:04:07'),
('850a6674bd550072e65ae80f4d3c444fa20e2c5e2f8939c581d80fb5c53411adb2931fadb880590c', 62, 1, '*', '[]', 1, '2020-01-14 01:28:47', '2020-01-14 01:28:47', '2021-01-14 06:58:47'),
('851709d976642388854cec17290fcf42ee7ce40b45e94cfc212576763ee211f03a4438f1a4c458a0', 3, 1, '*', '[]', 1, '2020-01-14 08:14:55', '2020-01-14 08:14:55', '2021-01-14 13:44:55'),
('853439a7958609a3c068fbbfd6f9d78acc65bafe9bf4e6fe9d45f43f315b16b00b17b1c375df33f9', 5, 1, '*', '[]', 1, '2020-01-17 04:17:29', '2020-01-17 04:17:29', '2021-01-17 09:47:29'),
('855f9c11dc6fb776969b69242b2ae9f97da6b7e3b119c1e2940773dcf182ebfcef743aaa2f9d1c0b', 62, 1, '*', '[]', 1, '2020-01-16 07:51:45', '2020-01-16 07:51:45', '2021-01-16 13:21:45'),
('85782a3a358fb571a2ea64adc6f32ab9238f4e448886b6eaa79f5900ff99ebf88dd32b9b38cbec15', 3, 1, '*', '[]', 1, '2020-01-10 05:24:18', '2020-01-10 05:24:18', '2021-01-10 10:54:18'),
('85a343e2f3d1643f146480be075b992c4274d31f6110575586527925de7397a35ca1c153dcb6366d', 4, 1, '*', '[]', 1, '2020-01-13 06:20:47', '2020-01-13 06:20:47', '2021-01-13 11:50:47'),
('85a73080bba800bc604c2e2efbb7746f3940cf1088c9781d76e0864138b7073989f37c1375ea8f9b', 5, 1, '*', '[]', 1, '2020-01-17 04:18:51', '2020-01-17 04:18:51', '2021-01-17 09:48:51'),
('85d0537215b57bef0fb3af8150c594c0480820da4f6759d780c39c0c2515709c417603199106af15', 15, 1, '*', '[]', 1, '2020-01-09 05:06:23', '2020-01-09 05:06:23', '2021-01-09 10:36:23'),
('85d6803381fa0bf4f3599368b1077c415ef520388718f957569c123fbd19d6931cd9fe50f0d47166', 1, 1, '*', '[]', 1, '2020-01-09 00:51:35', '2020-01-09 00:51:35', '2021-01-09 06:21:35'),
('85d8550d8b7e429f7f28092711e882d42c07e49184551efa6a86ce1ae928b6a8e19476834df289a6', 40, 1, '*', '[]', 1, '2020-01-10 02:23:01', '2020-01-10 02:23:01', '2021-01-10 07:53:01'),
('85faabca5b30b01459375d3e0f52bf66f7ed5f94dd24afff800d54eddeafeb7bf8c16593a4f12631', 63, 1, '*', '[]', 1, '2020-01-15 23:38:00', '2020-01-15 23:38:00', '2021-01-16 05:08:00'),
('86294c17eff644e35997d8cbc052026055e3ba76c506557cbad4720f565de77ca8709efc0cabaa6c', 14, 1, '*', '[]', 1, '2020-01-17 05:29:48', '2020-01-17 05:29:48', '2021-01-17 10:59:48'),
('8631e9beb4d4a31330aba3f6eff1e703449c5fcdeee7814f9782396d34f99289d81be1bc124060ae', 46, 1, '*', '[]', 1, '2020-01-10 03:45:14', '2020-01-10 03:45:14', '2021-01-10 09:15:14'),
('866cde86feea56c5b675b8fd40aee949897bccc9aca2e6d48fa5fe52491845a565010de80aa5d096', 39, 1, '*', '[]', 1, '2020-01-10 02:12:48', '2020-01-10 02:12:48', '2021-01-10 07:42:48'),
('86c5db7b1a1cb231ebc2c7c91f4e903ffbb475f4590778e5bb13427eccbcb06fe412d073bee00a5b', 43, 1, '*', '[]', 0, '2020-01-10 02:40:40', '2020-01-10 02:40:40', '2021-01-10 08:10:40'),
('871a15bebe2f9612344791477582d1a65bbad3498d59acf50506ff1ac956fb4a58ff8e88b1df8dcf', 4, 1, '*', '[]', 1, '2020-01-14 00:48:35', '2020-01-14 00:48:35', '2021-01-14 06:18:35'),
('878189f9efb440745a262d35c54964632eed24ec579615c521bcd74866331bf531b5e26dbf11942d', 7, 1, '*', '[]', 0, '2020-01-20 06:04:51', '2020-01-20 06:04:51', '2021-01-20 11:34:51'),
('87a53bc0010483f9a623a47ed56eed4a628659fb297603ebff9d745fd239de13007679992f73bc6d', 9, 1, '*', '[]', 1, '2020-01-20 06:23:07', '2020-01-20 06:23:07', '2021-01-20 11:53:07'),
('87b3a8257fc5a6e869856e9e662676d0c3a293d8a1b331fe5fc6cde290adef2014d35f66ddfcb137', 3, 1, '*', '[]', 1, '2020-01-13 05:11:20', '2020-01-13 05:11:20', '2021-01-13 10:41:20'),
('887ea3e8e8ca2784ba09d15f93b9e1680146bb91079ee7afbb31d0553d5cd0a30b270c05ced2af6e', 2, 1, '*', '[]', 1, '2020-01-08 04:03:37', '2020-01-08 04:03:37', '2021-01-08 09:33:37');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('88e856667d98b1c278673c39da9332468516e3671ff9c1eebe440c3bc080409c77d0609962137c41', 35, 1, '*', '[]', 1, '2020-01-10 01:18:32', '2020-01-10 01:18:32', '2021-01-10 06:48:32'),
('88ffeed1af3b28770d731d40d24222ac0ab63f754026910c991b3baf4dacb15ad43f3af6ca19ad04', 1, 1, '*', '[]', 1, '2020-01-13 01:27:35', '2020-01-13 01:27:35', '2021-01-13 06:57:35'),
('8938e7e439bc585c33095675dd9340aeebb45168e42b67d2c0452608e1f2397f38881882db8e03ec', 4, 1, '*', '[]', 1, '2020-01-15 23:28:57', '2020-01-15 23:28:57', '2021-01-16 04:58:57'),
('89f3959e0ca6dbd8983877040d95d35f33b11044b23f39b3818e5d1080c20971b0756830041cbda2', 3, 1, '*', '[]', 1, '2020-01-17 04:38:34', '2020-01-17 04:38:34', '2021-01-17 10:08:34'),
('8a03ea8c5703f9f257e61a88c2e4911ae0129598a10f7ee2470c59783e8e3b43cff8241cca4d0761', 9, 1, '*', '[]', 1, '2020-01-20 06:23:33', '2020-01-20 06:23:33', '2021-01-20 11:53:33'),
('8a75b5baa9c18034aeb9cf74b517a76e6d9687727b521ed0a8273efc104051578eaa370e3ae82b48', 1, 1, '*', '[]', 1, '2020-01-20 04:57:37', '2020-01-20 04:57:37', '2021-01-20 10:27:37'),
('8ab6d1468cf69f2a4eeafb7225fd59193cc294f1261b933ab6f246b6f9f5be2e13d286249f0ed5e7', 20, 1, '*', '[]', 1, '2020-01-13 00:10:41', '2020-01-13 00:10:41', '2021-01-13 05:40:41'),
('8b55f4dc35d2118b139418bad2aafaeed30dd37bcdbd5db6c5898b93e4f751d2dc5886d15fae6e33', 1, 1, '*', '[]', 1, '2020-01-08 03:56:52', '2020-01-08 03:56:52', '2021-01-08 09:26:52'),
('8b7718d1f3cebdd554e0e3995c42b7569c877669b286ca9504a67264365caafc2d75b5fbef46f2e0', 1, 1, '*', '[]', 1, '2020-01-08 01:59:22', '2020-01-08 01:59:22', '2021-01-08 07:29:22'),
('8bdb9118e28cc9d028d6c258f49a7c1e6bbdad59982e00231356491b7658e7406d6cab6741213fd9', 3, 1, '*', '[]', 1, '2020-01-15 02:45:15', '2020-01-15 02:45:15', '2021-01-15 08:15:15'),
('8be941cac06d54c26eeab573d6526cbd2a168cfade5173810370db8b3569c8efc126a750d4e8dd64', 18, 1, '*', '[]', 1, '2020-01-09 05:57:54', '2020-01-09 05:57:54', '2021-01-09 11:27:54'),
('8bf7a4bc436974d6f2c157eb6b758f544b6bdd77513816b3647d8c234bef14a0b483b6d8cf9e44df', 62, 1, '*', '[]', 1, '2020-01-16 01:59:04', '2020-01-16 01:59:04', '2021-01-16 07:29:04'),
('8c650e5ef047892f4bfb88cf14253bfba210fdd69c4bc33afdec65b6c8467349dabb64b6f5bf10b3', 44, 1, '*', '[]', 0, '2020-01-10 03:38:02', '2020-01-10 03:38:02', '2021-01-10 09:08:02'),
('8d03e128bf66d183cf209667bfe6c7bc4ff2ce0b0d07b1b70c342ed274d646dc5209c4d45549aae4', 3, 1, '*', '[]', 1, '2020-01-14 00:45:59', '2020-01-14 00:45:59', '2021-01-14 06:15:59'),
('8d06c69afdb778b1093b8accf2bcede648695ac30bd79137d88d503fda04932b3718d1fdee667ba2', 74, 1, '*', '[]', 1, '2020-01-15 06:42:12', '2020-01-15 06:42:12', '2021-01-15 12:12:12'),
('8daedd34b2f9bc79c39065fa565958efe7f897a84253d9a984a39d0fda5350f114b076840faa95ab', 82, 1, '*', '[]', 0, '2020-01-16 04:38:17', '2020-01-16 04:38:17', '2021-01-16 10:08:17'),
('8e1494f3b3f8b080ab7023f00e9736a600e5d34d169702dd22ffcff9a8a8cbed00fb14defb29715f', 62, 1, '*', '[]', 1, '2020-01-13 04:26:03', '2020-01-13 04:26:03', '2021-01-13 09:56:03'),
('8e19147ce18ae8c1921af5b604568e614adb21dca53cf84abc9829bbd4875bb53ec0e79ebdb69d4a', 1, 1, '*', '[]', 1, '2020-01-17 02:34:39', '2020-01-17 02:34:39', '2021-01-17 08:04:39'),
('8e9dcd4af42898514a59989e830dfa9da30489ab2be86a77790fc36f59159b8bc3415c1f9b104adb', 7, 1, '*', '[]', 1, '2020-01-09 05:16:07', '2020-01-09 05:16:07', '2021-01-09 10:46:07'),
('8f82db0404cb5b73b2a44d756ec68ebf8c6296634e29014d19abe1c3b5b6e2e9464e70e16f72fc9a', 36, 1, '*', '[]', 1, '2020-01-10 08:53:52', '2020-01-10 08:53:52', '2021-01-10 14:23:52'),
('8f889502ec544e6674904c27a99f83e45383ad305cd3098e2f8d9635c69b48afcb09d8cc61666811', 62, 1, '*', '[]', 1, '2020-01-15 04:59:15', '2020-01-15 04:59:15', '2021-01-15 10:29:15'),
('8fbac44f0b27395996b2a005c74515a3b9111c93425817d3b085062dfe824cf60a4efc18c27139a9', 62, 1, '*', '[]', 1, '2020-01-14 05:59:39', '2020-01-14 05:59:39', '2021-01-14 11:29:39'),
('9055b6d83ca800496239ad147e40ad59f87203c40376f51678ea9fa40861e9e60b307b88081fae44', 36, 1, '*', '[]', 0, '2020-01-13 01:05:21', '2020-01-13 01:05:21', '2021-01-13 06:35:21'),
('907941784ea6e8b82979cd34071abfd131950a42614e0ccd6a91af3896fe735196425d8cd7af9f64', 28, 1, '*', '[]', 0, '2020-01-09 08:39:03', '2020-01-09 08:39:03', '2021-01-09 14:09:03'),
('921f96f51b7fb905e7b6c80d1d5a2a5ea9cfa4a0bee603e667baad555ae0101526ce0bcc9a7c56bb', 12, 1, '*', '[]', 1, '2020-01-17 01:26:07', '2020-01-17 01:26:07', '2021-01-17 06:56:07'),
('9245dc1ac3f5551ccac4f28b99f24b8fa16b4967a082f7a90fb90c1fb456d9241c56cfa6a83ce4c1', 4, 1, '*', '[]', 1, '2020-01-15 04:06:29', '2020-01-15 04:06:29', '2021-01-15 09:36:29'),
('924b3382c51e75fe43943eac01b3d1aed79555c5948f4a0f4f3438660991c04d936dd6704981784e', 48, 1, '*', '[]', 1, '2020-01-10 03:57:40', '2020-01-10 03:57:40', '2021-01-10 09:27:40'),
('924e04d2e5f0d78528e95044005f587deda97b52f47416cd9e035163252acb38b06d5ef36ff0eb5a', 4, 1, '*', '[]', 1, '2020-01-09 08:45:49', '2020-01-09 08:45:49', '2021-01-09 14:15:49'),
('92aa41fbd567a6b5b54838671171cd2c24473f6f1c41620c1b3b4ade2880119d0ce993b73e850780', 3, 1, '*', '[]', 1, '2020-01-10 04:29:26', '2020-01-10 04:29:26', '2021-01-10 09:59:26'),
('92ae4030b790286018ec3f284be978dc44a7e8b89d137a0be797a2eb3f30f3b237bc5a62ffd47fd9', 62, 1, '*', '[]', 1, '2020-01-15 06:38:24', '2020-01-15 06:38:24', '2021-01-15 12:08:24'),
('92bb95194ee886dac7f178b5f599fa27e83d03da9ccd88837dd677f6d9a356b5fcf8385e990724c0', 4, 1, '*', '[]', 1, '2020-01-13 07:14:12', '2020-01-13 07:14:12', '2021-01-13 12:44:12'),
('9308f0b5ecacee967fa986e1ddbefe499a83241f2538add267405ab2693f7e8aa1ded58932c3ffec', 3, 1, '*', '[]', 1, '2020-01-08 23:39:59', '2020-01-08 23:39:59', '2021-01-09 05:09:59'),
('9353126c6dc564ad085c99223118ad5f2e05aa6ed1bc1f8d57973ec92d0c2c0cc5bf1dd64449d43c', 17, 1, '*', '[]', 1, '2020-01-09 05:19:28', '2020-01-09 05:19:28', '2021-01-09 10:49:28'),
('9356933a2572a20250cdc6dbc269fdcf6d5a9a7f297a7d37c411618f074a53d452d74a2958b1e4d0', 12, 1, '*', '[]', 1, '2020-01-17 00:51:03', '2020-01-17 00:51:03', '2021-01-17 06:21:03'),
('939126bfa6c44cd0948f1be94f1510868d1262c84d957b07a6cbae02c70b2b9e05b2b149fcb0bd59', 62, 1, '*', '[]', 1, '2020-01-16 07:02:27', '2020-01-16 07:02:27', '2021-01-16 12:32:27'),
('93bca25535779151c992acc522001bf8ba21f4a735ec67676827b6ae4161df45477ca5e2a71018a0', 9, 1, '*', '[]', 1, '2020-01-20 06:23:45', '2020-01-20 06:23:45', '2021-01-20 11:53:45'),
('93e0a1802ae27cdf8c3d69589f0571c577d7b2fd634232ee588ca6b33b9e0fdd7f29e4752e93ae88', 62, 1, '*', '[]', 1, '2020-01-14 06:05:05', '2020-01-14 06:05:05', '2021-01-14 11:35:05'),
('94097107d8e90a537d15e1360c56a0f32fd6f3179a225266c2fa0df6613f6eb32e541a61cb4cd11d', 4, 1, '*', '[]', 1, '2020-01-15 04:43:07', '2020-01-15 04:43:07', '2021-01-15 10:13:07'),
('941610217cb1301781b33f21a87aec7002033ab5372dc9d84bfe1a91825a4fd03f90759f59864784', 62, 1, '*', '[]', 1, '2020-01-15 01:47:52', '2020-01-15 01:47:52', '2021-01-15 07:17:52'),
('9428448df1effeaea8af3234421e620de7e5462335a041cf4a60cf820e56bee3924aa93cdb38ec21', 3, 1, '*', '[]', 1, '2020-01-15 04:57:34', '2020-01-15 04:57:34', '2021-01-15 10:27:34'),
('944df949def5e99b76cade4ec54480a8e7cc227ccb179c3e6b37301cc855e49eaf84e020b1c8ea86', 12, 1, '*', '[]', 1, '2020-01-17 01:16:40', '2020-01-17 01:16:40', '2021-01-17 06:46:40'),
('949580c04f285167ef95ac1ae47d48e0258878b0377f340116f27da21e4945c634afa478fadf406a', 74, 1, '*', '[]', 1, '2020-01-16 11:34:15', '2020-01-16 11:34:15', '2021-01-16 17:04:15'),
('94d39129355fcf40e3032dbccca2da2ef509f88a2083c1c448be6113a92f59803362396e1e337234', 62, 1, '*', '[]', 1, '2020-01-14 05:21:07', '2020-01-14 05:21:07', '2021-01-14 10:51:07'),
('9533205dbe0b7b5b36a72405d844b6bb415e5f42a4261cff85db6cbd2eb7066ebeca44734fe789e3', 14, 1, '*', '[]', 1, '2020-01-09 05:06:53', '2020-01-09 05:06:53', '2021-01-09 10:36:53'),
('955af307571510952cb33e09b25cf78b008351cb7d165b90e8ec834ef978feb9c2216ca904a4ea5c', 31, 1, '*', '[]', 1, '2020-01-09 23:42:04', '2020-01-09 23:42:04', '2021-01-10 05:12:04'),
('958496dd743aed216fe4bb78163f75a9d9efc82f4572d98d87249f2d9733c67df87bf9a40dc52ede', 4, 1, '*', '[]', 1, '2020-01-09 23:57:14', '2020-01-09 23:57:14', '2021-01-10 05:27:14'),
('95aa541a8f4ae60837d79dbde53871773d00bb05a3ee36fa1556cd98765d5de2568344724d367770', 66, 1, '*', '[]', 1, '2020-01-14 00:44:50', '2020-01-14 00:44:50', '2021-01-14 06:14:50'),
('95b9c1b0abf9e21ae164e5c25dd4ec530e5c540bf564a7f29014ad5e7314f67536b1c462c555a001', 18, 1, '*', '[]', 0, '2020-01-09 05:58:56', '2020-01-09 05:58:56', '2021-01-09 11:28:56'),
('962fcb0a987b235cae01eb82efb5a98b21fa7012ddc0a29a3091482b7a2bf0975c3a90803622498a', 36, 1, '*', '[]', 1, '2020-01-12 23:54:05', '2020-01-12 23:54:05', '2021-01-13 05:24:05'),
('96c12a598c0633afa382ce7721cc06d227f27136d034140441c45d7feb9f6b97aab10239696d458c', 3, 1, '*', '[]', 1, '2020-01-10 08:17:17', '2020-01-10 08:17:17', '2021-01-10 13:47:17'),
('96c86c3097e483082afbb30f9f96e1cf888811ba7f9e12f7b302863ec28e2ca679b99c923ee7c7c0', 77, 1, '*', '[]', 1, '2020-01-15 08:43:05', '2020-01-15 08:43:05', '2021-01-15 14:13:05'),
('96ec49ee4657455e0742e11196c8b72f252332d36bcd4c34d409636c2755e92d0782e7882165b2ed', 62, 1, '*', '[]', 1, '2020-01-16 04:26:27', '2020-01-16 04:26:27', '2021-01-16 09:56:27'),
('978be05a4726636c72dee37931fb7bf068dc64ae717e8dcea5d4915e8e6960f23d4a9871b2c76535', 62, 1, '*', '[]', 1, '2020-01-15 06:36:26', '2020-01-15 06:36:26', '2021-01-15 12:06:26'),
('97a2afc88ef4338bb9127b84cd0eae2fa01ab26b265e69b8d9a46677dc7ad77ab4b9aa00fe7aef64', 1, 1, '*', '[]', 1, '2020-01-20 01:22:31', '2020-01-20 01:22:31', '2021-01-20 06:52:31'),
('98b35ec6616c4e40e966cdbfc3fadccfc7e85674651b64ad4d85e18f342b0221b890eb94df414b5f', 62, 1, '*', '[]', 1, '2020-01-15 02:48:51', '2020-01-15 02:48:51', '2021-01-15 08:18:51'),
('98ebdba1755925be91a5c0cf0d2ff69ee10d9114d319d0c5f609450352b68b900974470e977e0317', 9, 1, '*', '[]', 1, '2020-01-17 04:51:16', '2020-01-17 04:51:16', '2021-01-17 10:21:16'),
('99', 1, 1, '*', '[]', 1, '2020-01-08 01:30:29', '2020-01-08 01:30:29', '2021-01-08 07:00:29'),
('99149317a75ab95c880d55fd758dcac33ecf199a41406a7afc4d1823a4afb36a52e53df4efe37c80', 3, 1, '*', '[]', 1, '2020-01-09 03:21:41', '2020-01-09 03:21:41', '2021-01-09 08:51:41'),
('9952e464ee2b9b67bde154c4a21707fd9f6be244789891f05115c04340622361e05a95bda9807e5e', 1, 1, '*', '[]', 1, '2020-01-20 02:39:33', '2020-01-20 02:39:33', '2021-01-20 08:09:33'),
('99f507621a5ccaca4983faa68d3e643b8ee2e847480bb2e7c32091886f1e3f1fc3e81de9f1dba7b4', 92, 1, '*', '[]', 0, '2020-01-16 08:59:56', '2020-01-16 08:59:56', '2021-01-16 14:29:56'),
('9a5e3ef1a8239a00df6d3393fed21ab19fedc4002f03831f5723e7cba02286ea91c55ced4b0a0984', 1, 1, '*', '[]', 1, '2020-01-12 23:39:24', '2020-01-12 23:39:24', '2021-01-13 05:09:24'),
('9a7b7f79c68a7079f6b11fc6e10d0ef6b3d6d2daa5020cdfc85adb53cacacf4cd5fdb3d340996bbd', 3, 1, '*', '[]', 1, '2020-01-14 23:40:35', '2020-01-14 23:40:35', '2021-01-15 05:10:35'),
('9b159843e65040cc05bb5fe171de02c3769f8c8566cde34c74016b39cc00a1b77bdbdc0a282bebdf', 62, 1, '*', '[]', 1, '2020-01-16 04:29:25', '2020-01-16 04:29:25', '2021-01-16 09:59:25'),
('9b17068f8f550e9d10227b37de79fae7ba251f4b645c9ea5cff8d72a48af08d1fd95aa0da1150842', 21, 1, '*', '[]', 0, '2020-01-09 07:22:50', '2020-01-09 07:22:50', '2021-01-09 12:52:50'),
('9b61cbf69688df7d075b282efe29e68bc98532c7a9130acc3074bd1e85b6be55f78d2994f52af30c', 34, 1, '*', '[]', 0, '2020-01-10 00:57:16', '2020-01-10 00:57:16', '2021-01-10 06:27:16'),
('9b6d093f1761d91a1cffe51782ba4f66b94d439296bccc34032853a2051648b88d006096dd27ea5d', 1, 1, '*', '[]', 1, '2020-01-20 00:23:20', '2020-01-20 00:23:20', '2021-01-20 05:53:20'),
('9bc03a886412331dba9c606e13f8f9999700b5c690f2c032868a6071b455f68665abc33ee394b3b9', 5, 1, '*', '[]', 1, '2020-01-18 02:51:11', '2020-01-18 02:51:11', '2021-01-18 08:21:11'),
('9c57b9a79eb11cf07a17a20a2c836c6b5603094f1e097a44d87cb1eace80b7d390c5688bb5773039', 3, 1, '*', '[]', 1, '2020-01-15 03:35:43', '2020-01-15 03:35:43', '2021-01-15 09:05:43'),
('9d42d33d4080fc0d4461f4bfb216ee74bf3b241b8f054249b488b038fb2cea623ae0ef027239b20e', 62, 1, '*', '[]', 1, '2020-01-13 07:16:22', '2020-01-13 07:16:22', '2021-01-13 12:46:22'),
('9d4522d6d5c0c6d741254adf09396a0cb66c51db66c03852d3df4cc865b0e228d5debb582c6e373a', 39, 1, '*', '[]', 0, '2020-01-10 02:18:35', '2020-01-10 02:18:35', '2021-01-10 07:48:35'),
('9d587b48fe4b2f9e3d062f297d6027df2c35c06abe1b0fac937cf7aa9e7f13e7b8441d0c20d6fd42', 62, 1, '*', '[]', 1, '2020-01-14 06:26:55', '2020-01-14 06:26:55', '2021-01-14 11:56:55'),
('9d9cff15eb691dae8220388c5ad770b13a084aa8d07b4fe4e71199cbfffd55ade2e64c620b938b56', 9, 1, '*', '[]', 1, '2020-01-20 06:22:47', '2020-01-20 06:22:47', '2021-01-20 11:52:47'),
('9e02c523f0e90b79b3fb947eb91fdd20139ce7d719737aa12522c9b3f032ae2a7241803564b95928', 80, 1, '*', '[]', 1, '2020-01-16 02:31:52', '2020-01-16 02:31:52', '2021-01-16 08:01:52'),
('9e06afa0ae4c50a033ac944f1134dcafd06ff6c5d38c3ac77ade64ab3e9252b6ee4aba0eff25f7e2', 14, 1, '*', '[]', 0, '2020-01-17 05:46:05', '2020-01-17 05:46:05', '2021-01-17 11:16:05'),
('9e1699245cd5d7a9e33778ffdda22a16535fdcfd81c3850e21a3ac7d494277501384a5a5266f6cba', 62, 1, '*', '[]', 1, '2020-01-15 06:52:25', '2020-01-15 06:52:25', '2021-01-15 12:22:25'),
('9e9196f3c2fd2c250486fa3b07584dc84125244e6bba10c6e8a1238adfb125f4a5e2a7418b439ca8', 31, 1, '*', '[]', 1, '2020-01-09 23:47:53', '2020-01-09 23:47:53', '2021-01-10 05:17:53'),
('9e96318df45fba10136a804ff6854b9795b0c8829020922fc3bf555c87d4bb158dc8ca0c3197cdba', 62, 1, '*', '[]', 1, '2020-01-13 06:09:30', '2020-01-13 06:09:30', '2021-01-13 11:39:30'),
('9eba4a69be475299cd6f68b70c9e2876df2fa13732b9de2a058efffee76b60e1acaf9869ac810ad2', 1, 1, '*', '[]', 1, '2020-01-20 02:21:10', '2020-01-20 02:21:10', '2021-01-20 07:51:10'),
('9f98b4176154efd5588bb85c87a418575732d5fc996b1deba7fa8381e39d276d367a3721b676e85b', 95, 1, '*', '[]', 0, '2020-01-16 11:33:20', '2020-01-16 11:33:20', '2021-01-16 17:03:20'),
('9ff708abc33b2d39ed24c19c6ba00f90254287d9120075cb2499082363415970ad8e56eaa09eb610', 4, 1, '*', '[]', 1, '2020-01-16 05:33:13', '2020-01-16 05:33:13', '2021-01-16 11:03:13'),
('a06aa0ac0481a313dc6e17e8153531a73fe2c35789670cffeb7fb429816218c9798c501c59639ea1', 4, 1, '*', '[]', 1, '2020-01-08 08:42:06', '2020-01-08 08:42:06', '2021-01-08 14:12:06'),
('a0849eb7a5842693e63ac4728449d84b8d19cc0b66ea6a5bd1c92fefda45e5411068b845341b155d', 3, 1, '*', '[]', 1, '2020-01-12 23:18:55', '2020-01-12 23:18:55', '2021-01-13 04:48:55'),
('a0da99c7435f8cefa52199f58c1404d9a9db57a909debea146c7016863958c3770333ae5d1bc2953', 94, 1, '*', '[]', 1, '2020-01-16 11:05:05', '2020-01-16 11:05:05', '2021-01-16 16:35:05'),
('a16c4ad28a834c5a14c651565a5026aefe328db994253ae8d3779ee8db38ae8536e4c9896383cfea', 4, 1, '*', '[]', 1, '2020-01-16 09:54:52', '2020-01-16 09:54:52', '2021-01-16 15:24:52'),
('a1a2e036cad3df928a4777c3538d1bfd739cf2c119b016dc39dd892dfa3d1ce29d9210124e33f734', 3, 1, '*', '[]', 1, '2020-01-16 23:32:31', '2020-01-16 23:32:31', '2021-01-17 05:02:31'),
('a21138b70ac8f41a69e95910b1b427b1bb1cb239f8426f66871eee09b50cd89c28c4785b35a0d3cb', 3, 1, '*', '[]', 1, '2020-01-14 03:59:41', '2020-01-14 03:59:41', '2021-01-14 09:29:41'),
('a2605a95fc4456c8b03bc44da7704928cd26012bf95d4d04ac6d2a2f7dedab2d52ebfb0fe4e88a7f', 2, 1, '*', '[]', 1, '2020-01-16 23:29:10', '2020-01-16 23:29:10', '2021-01-17 04:59:10'),
('a27b61e37a020ab04316c40c8421b7429da22c8d0934083e754b2f1536a79675be7b03d99db40895', 5, 1, '*', '[]', 1, '2020-01-18 03:24:13', '2020-01-18 03:24:13', '2021-01-18 08:54:13'),
('a29a8eb5d5eb61df3aff958c6f04811ae804daa9240c83ac8efc4ba40e86ce92b5db41e14d97a718', 3, 1, '*', '[]', 1, '2020-01-15 01:28:44', '2020-01-15 01:28:44', '2021-01-15 06:58:44'),
('a2bb84b6a12d31fc83ece3e95ea04c48cc491fd4273c1f4f6aaa17e008811b2d41d739874d7be7d7', 31, 1, '*', '[]', 1, '2020-01-09 23:49:06', '2020-01-09 23:49:06', '2021-01-10 05:19:06'),
('a2d51fdf1ccbda031e472f7d728c0ad8f9cabed19fe80109e4a3ce1f8fd409424e8949b12a4428d1', 64, 1, '*', '[]', 0, '2020-01-13 07:17:20', '2020-01-13 07:17:20', '2021-01-13 12:47:20'),
('a2de0e89765ccb6c369523066b4b1ab4fe2c8353739f8a112e4142eeaf0f13f615109d0099f5b6be', 1, 1, '*', '[]', 1, '2020-01-13 02:02:58', '2020-01-13 02:02:58', '2021-01-13 07:32:58'),
('a31d176bbb3125a8e5ae191e6f40fe4ae83aa442473252d265df4bba48d1e1f977a6dcb5446454fd', 85, 1, '*', '[]', 1, '2020-01-16 05:12:16', '2020-01-16 05:12:16', '2021-01-16 10:42:16'),
('a40717584ec6b58fe2a7c193e6dd6b9bd466164552464f95e446434715d0777833f8cc8af3a0fbf1', 15, 1, '*', '[]', 1, '2020-01-09 05:06:31', '2020-01-09 05:06:31', '2021-01-09 10:36:31'),
('a415559a926c27c313c959fc851e415042c2f6b1d906e15379695c72e0b20b3e691733761a17f667', 10, 1, '*', '[]', 1, '2020-01-17 08:04:09', '2020-01-17 08:04:09', '2021-01-17 13:34:09'),
('a49f424426feae611eb02d6f9b2312754aa0ca2d238a90bb780121bb955e844036567390be5e040d', 4, 1, '*', '[]', 1, '2020-01-15 06:57:46', '2020-01-15 06:57:46', '2021-01-15 12:27:46'),
('a4c9b087bc76c4014aa2efd648fa910f3ab44b63c3b0adeaf67ca4b2e077ad7ab9ad96520abc70d9', 13, 1, '*', '[]', 1, '2020-01-17 02:18:11', '2020-01-17 02:18:11', '2021-01-17 07:48:11'),
('a544650c29342c4fcadec105d87adcab67a8498f678ef036f3ead3750143978abb8c7765deac87a7', 36, 1, '*', '[]', 1, '2020-01-10 08:58:20', '2020-01-10 08:58:20', '2021-01-10 14:28:20'),
('a5460fd09a3d14533d49f90afc382eb9ecf390436c696fd62d6b67854416c799ac059f5d791db014', 67, 1, '*', '[]', 0, '2020-01-14 06:33:53', '2020-01-14 06:33:53', '2021-01-14 12:03:53'),
('a56eb5300cf75577df1df0ddb5518ee8f3bd57e8bc3cc7db52bf35ec2e3d1d08381803337cd8569b', 37, 1, '*', '[]', 1, '2020-01-10 01:50:20', '2020-01-10 01:50:20', '2021-01-10 07:20:20'),
('a576ae35c4ecfcd834611df48975e300d2b7b92b2cf66daeac58a74b944e26b6103efb31d92be8f6', 63, 1, '*', '[]', 1, '2020-01-14 05:03:13', '2020-01-14 05:03:13', '2021-01-14 10:33:13'),
('a58f86b997953a2f58c54ace4ac8855b9d1c2808335421bb27d0507ac6941d846f1bea64e2e82af5', 62, 1, '*', '[]', 1, '2020-01-16 06:32:34', '2020-01-16 06:32:34', '2021-01-16 12:02:34'),
('a5c2923718545b980547039b7beef0cfa373d54e48b29c790b948e0ee2ad73fc5f56eab720520836', 63, 1, '*', '[]', 1, '2020-01-16 01:38:23', '2020-01-16 01:38:23', '2021-01-16 07:08:23'),
('a628eaa5c642476f70c65fd5fc29e8a110c6b49c86ba1fe1f6e7ba2d8a4c121b787258cc84760b6c', 3, 1, '*', '[]', 1, '2020-01-17 04:25:49', '2020-01-17 04:25:49', '2021-01-17 09:55:49'),
('a6431f2ef75cc31fa4140b2e96bc7a751623b3dd0a9f7eb05fb962dd5802c87667aebed4720d684c', 11, 1, '*', '[]', 1, '2020-01-17 04:49:42', '2020-01-17 04:49:42', '2021-01-17 10:19:42'),
('a81d0731e94422bf63f29289056a5550a3b720d18fdd378ff7d81585824f3b74c05901e14b44142e', 53, 1, '*', '[]', 0, '2020-01-10 04:33:35', '2020-01-10 04:33:35', '2021-01-10 10:03:35'),
('a833df4d8a757b5203eca4e8661c0698ada3fb28fb6421a08dbd6a42cddec679bb1cc76f7c0d087f', 1, 1, '*', '[]', 1, '2020-01-12 23:38:12', '2020-01-12 23:38:12', '2021-01-13 05:08:12'),
('a83623f025c1a4709897bfe354d57a9b586fc5e1b8e0be827e51980a00475e3938ff8e3a4a3dd70d', 36, 1, '*', '[]', 1, '2020-01-10 04:14:31', '2020-01-10 04:14:31', '2021-01-10 09:44:31'),
('a9010f89ab7c65d49d8d5f94ab44dc27dc5759c9a6e0ccd043ddcc8140cf77baff566da050c01e88', 74, 1, '*', '[]', 1, '2020-01-15 06:49:11', '2020-01-15 06:49:11', '2021-01-15 12:19:11'),
('a929040be9cd6c452146cb30f808a2031774614ac64c3f56ea986773430c232922c8ed43d1139498', 74, 1, '*', '[]', 1, '2020-01-15 06:38:42', '2020-01-15 06:38:42', '2021-01-15 12:08:42'),
('a92d9e5a6400e5f6e4f26d9bdb0395ce41c8d56366caf706f769e325e9a1b76f2a5bf1ca0627ce0f', 4, 1, '*', '[]', 1, '2020-01-15 05:10:25', '2020-01-15 05:10:25', '2021-01-15 10:40:25'),
('a951541a284e09735acfd35de83b8a562f49d397fd67c4760ac1072fd73b26c789962dd68ec6779b', 3, 1, '*', '[]', 1, '2020-01-17 04:41:08', '2020-01-17 04:41:08', '2021-01-17 10:11:08'),
('a95e135254fde7f67868c0cb487ec3c8030174d19b9e8bc69f612260d194833547195e5c3589080a', 6, 1, '*', '[]', 1, '2020-01-08 08:57:51', '2020-01-08 08:57:51', '2021-01-08 14:27:51'),
('a9905360431633972997db91320773157adca690154ec66538b8ee0542a6dba4a85aa5af59f79a28', 62, 1, '*', '[]', 1, '2020-01-15 05:02:21', '2020-01-15 05:02:21', '2021-01-15 10:32:21'),
('a9a3b54b597cb63a1589f963d4f8739b66bbedd8802249bc0d96a7791b7861e75dc0a04fc2e33fb7', 93, 1, '*', '[]', 0, '2020-01-16 10:02:22', '2020-01-16 10:02:22', '2021-01-16 15:32:22'),
('a9b1c1a110d58d6e4091041e05193df570332f49a5d3091ff4b8b7b574ff705d5e695c34e4da2083', 3, 1, '*', '[]', 1, '2020-01-15 04:25:09', '2020-01-15 04:25:09', '2021-01-15 09:55:09'),
('aa34f25bbe9039b381a26998629c1da22f0bc4d64eca88fdd652f0bf8dad08a33c325e04c5497752', 36, 1, '*', '[]', 1, '2020-01-12 23:17:23', '2020-01-12 23:17:23', '2021-01-13 04:47:23'),
('aa56ddacf1ec9b5ea6a5e575377426548dd7a7dd6175db4e58091000057257f00c346dbf0a751124', 74, 1, '*', '[]', 1, '2020-01-15 06:36:28', '2020-01-15 06:36:28', '2021-01-15 12:06:28'),
('aa5b82e14a81d59ca931e734f3fef8e3dac209d39e3c40de08c8b8ac33ea1528d598aebdbfe36008', 66, 1, '*', '[]', 1, '2020-01-16 06:37:36', '2020-01-16 06:37:36', '2021-01-16 12:07:36'),
('aa6872fc133949979ba303e58817573136b4878da9f8967bbb1da0e02bc40a9be60c2f044f96a037', 50, 1, '*', '[]', 1, '2020-01-10 08:14:39', '2020-01-10 08:14:39', '2021-01-10 13:44:39'),
('aa6b8351aba47ea25382fbf8839f64ede8fcc068277eedcfb3dc6e18ea1e1c1eec2cd8b59ec51f94', 4, 1, '*', '[]', 1, '2020-01-17 06:47:52', '2020-01-17 06:47:52', '2021-01-17 12:17:52'),
('aa7aece11f305220d22f152ede9a5b391dd2b96981477313712a1f13fc821c32e4800160e6727818', 62, 1, '*', '[]', 1, '2020-01-14 05:26:44', '2020-01-14 05:26:44', '2021-01-14 10:56:44'),
('aaaba4c4ad6ce9ef1d624fb7492d054b3b55008ff3225ae71fe48e3dc49d328f1d921a22cb85a8d6', 62, 1, '*', '[]', 1, '2020-01-16 07:10:59', '2020-01-16 07:10:59', '2021-01-16 12:40:59'),
('ab06aeae0a1205f42878a1cf9ebc31a6b8c724285f4d28d101289d8a9e057b3d8d4aa4e97370b251', 62, 1, '*', '[]', 1, '2020-01-15 04:56:32', '2020-01-15 04:56:32', '2021-01-15 10:26:32'),
('ab18322c43336e161ab86c1e85a481bcc9784c24b0a3156b1cbe3f2cb92f3f98bd6cc0381f6df15b', 3, 1, '*', '[]', 1, '2020-01-15 04:54:23', '2020-01-15 04:54:23', '2021-01-15 10:24:23'),
('ab3dd467af1367845131bba350227192115e074e575a335a518283b865f5670233d8761dd1906ac8', 12, 1, '*', '[]', 1, '2020-01-09 04:22:13', '2020-01-09 04:22:13', '2021-01-09 09:52:13'),
('ab83321c7fdc13ea9cf0c53baed88cd86e961344484df95591ae93734bb8af7050e8d76cf8bb8659', 62, 1, '*', '[]', 1, '2020-01-13 05:48:37', '2020-01-13 05:48:37', '2021-01-13 11:18:37'),
('abc8c399f9832ea6a2f70c7f9467bb4c766d3b51ac208c7076e4f9e1f4492a005d8447c615626710', 62, 1, '*', '[]', 1, '2020-01-16 01:24:57', '2020-01-16 01:24:57', '2021-01-16 06:54:57'),
('ac18fdf6293af650a3c592cae8b0d080df3e40b0dc87e5584e63a439b308d0c47a27edcffee00cf5', 62, 1, '*', '[]', 1, '2020-01-14 08:21:06', '2020-01-14 08:21:06', '2021-01-14 13:51:06'),
('ac5603132a5801ab0957675ee179456e727579c70d30611d90e2729465957f84e80f72697f3a3620', 3, 1, '*', '[]', 1, '2020-01-17 03:31:54', '2020-01-17 03:31:54', '2021-01-17 09:01:54'),
('acbe2fbc90f9156ecbd5fd46e6032c97e873868b4f70f76758d6d0b92776092516175f727afd0150', 63, 1, '*', '[]', 1, '2020-01-14 06:26:00', '2020-01-14 06:26:00', '2021-01-14 11:56:00'),
('ad4c10bedce2940d1c4009385f2c2b5f1e54c7244e300eb57c5c8a109efae3e122c01542c5f0f1d6', 1, 1, '*', '[]', 1, '2020-01-08 07:30:54', '2020-01-08 07:30:54', '2021-01-08 13:00:54'),
('ad99a7dc85ac7188e3a89727f22fd1a07a3447c744e7f2749dba240986abbb24bdb6ee1c207e9145', 78, 1, '*', '[]', 0, '2020-01-15 08:34:01', '2020-01-15 08:34:01', '2021-01-15 14:04:01'),
('adcb3a3dafbfdaa136b1daf20668e4ff302a85cb4aa2ef499675c75d73e4e2e4ca2439542733cebd', 1, 1, '*', '[]', 1, '2020-01-08 07:41:16', '2020-01-08 07:41:16', '2021-01-08 13:11:16'),
('aeb42bf8f77fe6ae3a63c84b9816fb3aa6b48389449316131b090d8154f384c3f320dc7f6990a47c', 1, 1, '*', '[]', 1, '2020-01-08 07:57:04', '2020-01-08 07:57:04', '2021-01-08 13:27:04'),
('aec55ee69a55b7a8d27666040486720a13109177c26c6463d82bc1cfa91bc5df9db2ed3fa0d477c1', 62, 1, '*', '[]', 1, '2020-01-16 05:11:09', '2020-01-16 05:11:09', '2021-01-16 10:41:09'),
('af342a1681e44ac999d97cf89b67ccd540a8707062133d904439937d4f7e1a8539e490de5b26b641', 62, 1, '*', '[]', 1, '2020-01-16 06:12:52', '2020-01-16 06:12:52', '2021-01-16 11:42:52'),
('af4c28195fc452284424108284e36e98d2a91dfa9f5008e2582b3979694caa41e53c35d7bc45a645', 20, 1, '*', '[]', 1, '2020-01-13 00:06:55', '2020-01-13 00:06:55', '2021-01-13 05:36:55'),
('aff58c4a56bfbe492abb2fd791230256b0cf10a1e85ab16b664631e8cc95ff1fafd46b51cd1220ae', 10, 1, '*', '[]', 1, '2020-01-18 02:41:54', '2020-01-18 02:41:54', '2021-01-18 08:11:54'),
('b02a0534591a7267847510deeaeba777c1c29a287c26c0f478878df77a0f3dd0ebee6f660dd3c0b3', 5, 1, '*', '[]', 1, '2020-01-17 08:59:00', '2020-01-17 08:59:00', '2021-01-17 14:29:00'),
('b04aad03a75871f258bb700d5a91a4901e66d6958379b85ab1ac8c0577bfd9f7af8d57a5dab0385b', 1, 1, '*', '[]', 1, '2020-01-08 08:22:45', '2020-01-08 08:22:45', '2021-01-08 13:52:45'),
('b04be70eb423f095a627ac2291adb51736876772ee5efb4e1e86e1093d1dc3cb3eeedaa41d2b4694', 74, 1, '*', '[]', 1, '2020-01-15 23:53:09', '2020-01-15 23:53:09', '2021-01-16 05:23:09'),
('b04d2db6547149c11bd16e96052aa261fdb2c89b59ecf1530684bb9eed15f34f745197d00f13edda', 7, 1, '*', '[]', 1, '2020-01-20 01:30:27', '2020-01-20 01:30:27', '2021-01-20 07:00:27'),
('b05a99a248583aeca3e39e43da589316ae35c898f5178b68e5338777846cb4c2aa5e1da469aef344', 1, 1, '*', '[]', 1, '2020-01-20 01:24:54', '2020-01-20 01:24:54', '2021-01-20 06:54:54'),
('b05b23ea6520d3d1027a0c5389bbf960e469d61882095502ca83f9143f0ebe12637e1bd5f5613f8e', 8, 1, '*', '[]', 1, '2020-01-20 05:34:21', '2020-01-20 05:34:21', '2021-01-20 11:04:21'),
('b0758caa8ea3898143cf3465c9f21e4b4b4e19848ee761b182cfc0bea02b0ba211f444c569e0596f', 1, 1, '*', '[]', 1, '2020-01-20 01:16:33', '2020-01-20 01:16:33', '2021-01-20 06:46:33'),
('b0c8e085911401413bf50aedacaa02511e644b8465d55d0191717d7490ec63c449973f70e34d7557', 81, 1, '*', '[]', 1, '2020-01-16 05:28:54', '2020-01-16 05:28:54', '2021-01-16 10:58:54'),
('b0c9238ab9a6f443ee670cff8f88dacd613d61a2b259b0f8db64ae91eadf68add14101b2f4b818e2', 74, 1, '*', '[]', 1, '2020-01-16 11:50:53', '2020-01-16 11:50:53', '2021-01-16 17:20:53'),
('b1ad8e73270838697093c12812d06d26066d3927e3644f5378e9c0a83f4ea24fb638212198532327', 36, 1, '*', '[]', 1, '2020-01-10 08:57:53', '2020-01-10 08:57:53', '2021-01-10 14:27:53'),
('b1b079c8cb58e36ca7662c4c3a2c088eed3708c41c0f8cbc353061528adef16babe165f96737d0ef', 83, 1, '*', '[]', 1, '2020-01-16 04:40:16', '2020-01-16 04:40:16', '2021-01-16 10:10:16'),
('b1dc31d85207c7163839f3188ac803a7f9f31de56d318413619f73b469f5158a2f37a5b89ac148eb', 62, 1, '*', '[]', 1, '2020-01-13 06:23:23', '2020-01-13 06:23:23', '2021-01-13 11:53:23'),
('b1dce76d7cdc857574f567bc71ae226b6b9e301e9e759db8c74ffbe8e0656deb9d04973376bd49b5', 3, 1, '*', '[]', 1, '2020-01-15 02:09:23', '2020-01-15 02:09:23', '2021-01-15 07:39:23'),
('b1f42bc5b3007f09d231f8232988b37bfdc9edf98f95e5ef3c0aed7f13ba4146564b1e4a3adb5ef3', 62, 1, '*', '[]', 1, '2020-01-16 06:58:42', '2020-01-16 06:58:42', '2021-01-16 12:28:42'),
('b223f486a1dbbb4edf498f3f09f868d735c1ec42224419b26d7f3176ec15dd8a9346d15457d271ef', 83, 1, '*', '[]', 0, '2020-01-16 06:32:54', '2020-01-16 06:32:54', '2021-01-16 12:02:54'),
('b22f9b5c3a6ec9c08661ad7b7d4346a9a0c26db53a7fb9b10170b640b0c133f344274a1d4a2eb06b', 2, 1, '*', '[]', 1, '2020-01-08 04:02:01', '2020-01-08 04:02:01', '2021-01-08 09:32:01'),
('b26d6627593e8e1065051abea9ed746ce05972df4738eefe7ba18f92426b21e4fa5f99e95f4467aa', 62, 1, '*', '[]', 1, '2020-01-13 05:57:40', '2020-01-13 05:57:40', '2021-01-13 11:27:40'),
('b2c54f1215b5735128ea123ae063334dc0fd055e8b56cf83d7209d647638257b82c3992cc44b0d3b', 62, 1, '*', '[]', 1, '2020-01-14 05:21:17', '2020-01-14 05:21:17', '2021-01-14 10:51:17'),
('b2def600bb0f2f06eeaaf9a632237ab10415e73c2d764811f547a92435b829ae8adddb83bf444ee9', 5, 1, '*', '[]', 1, '2020-01-17 03:48:25', '2020-01-17 03:48:25', '2021-01-17 09:18:25'),
('b318816502fd9a5e18608ce2fae2b024e544ff9eaba86d269eda06bedd7b0dcc4f9f733c35d49c14', 62, 1, '*', '[]', 1, '2020-01-14 08:06:34', '2020-01-14 08:06:34', '2021-01-14 13:36:34'),
('b3359d7da61b7bc74027c307dbaa6533dca3746e86e583894c96fa65c90644e9298b03638008fced', 80, 1, '*', '[]', 1, '2020-01-16 02:51:56', '2020-01-16 02:51:56', '2021-01-16 08:21:56'),
('b34be5527faca1865736d23a40e4dcde1b69776617455b98e00999cd5879aba74e377141a7574273', 4, 1, '*', '[]', 1, '2020-01-17 00:17:27', '2020-01-17 00:17:27', '2021-01-17 05:47:27'),
('b42ef40a6057b40bc23866cd08c5f3853008b48f1f9177a8e190bf322df9f733c1646e6e022f99ef', 4, 1, '*', '[]', 1, '2020-01-08 08:43:16', '2020-01-08 08:43:16', '2021-01-08 14:13:16'),
('b477b17c2c56b3a054f92ad99c976f82ebd956d7d72d3b48876d2f052ea3c6e60ff80d963980b370', 62, 1, '*', '[]', 1, '2020-01-16 05:10:33', '2020-01-16 05:10:33', '2021-01-16 10:40:33'),
('b4a77f7e652e0ee15b2e9b91b67c92a1a94784e44f2442433073b0f06cbc07adc5d8ebfa928dda66', 4, 1, '*', '[]', 1, '2020-01-17 07:09:07', '2020-01-17 07:09:07', '2021-01-17 12:39:07'),
('b4a84a68a2f1ff0320ecd25377d49dd77ca7420417f161f0f3d52286edb8237399393a5f109021b0', 3, 1, '*', '[]', 1, '2020-01-10 07:03:30', '2020-01-10 07:03:30', '2021-01-10 12:33:30'),
('b4e1f5acf9f7e8c2b003538943a3f1b8fd153cc9aefe0ebe571a9cdb898b157866dbcf29a90106d4', 14, 1, '*', '[]', 1, '2020-01-09 05:06:50', '2020-01-09 05:06:50', '2021-01-09 10:36:50'),
('b4ef059280a4583459530df8f2caff2bf52580201e32b0b593c06d76654caf6c9f5cb3f1cd91d0d6', 1, 1, '*', '[]', 1, '2020-01-10 07:39:58', '2020-01-10 07:39:58', '2021-01-10 13:09:58'),
('b5083d990849c3889e543fbf660257a545593fe4e243b404531cd580c477d5e5b425b8898b7d6b81', 63, 1, '*', '[]', 1, '2020-01-13 08:02:45', '2020-01-13 08:02:45', '2021-01-13 13:32:45'),
('b513c0abf4778a191d8f1dbec572af0498c3bbc4212fa5762d01dbad11876e6b977c2b7e01e2d134', 3, 1, '*', '[]', 1, '2020-01-10 04:21:20', '2020-01-10 04:21:20', '2021-01-10 09:51:20'),
('b5c124451b615d55d658585b5ff398135f918e8519f1972b8566eba2f02b9bff354889de23b6fb7f', 62, 1, '*', '[]', 1, '2020-01-14 08:02:08', '2020-01-14 08:02:08', '2021-01-14 13:32:08'),
('b5d7dfa0b333782f0e182abe068d3d01104281d7b5dc9f138eb2ee17331dcb5e7bd3076063d0554f', 62, 1, '*', '[]', 1, '2020-01-13 05:13:14', '2020-01-13 05:13:14', '2021-01-13 10:43:14'),
('b5f44fc6edd579ae4c9d649ba5afbb3494e1fa4970c564ee04d1f14322e2d5216eb160dceff95cec', 4, 1, '*', '[]', 1, '2020-01-17 06:19:42', '2020-01-17 06:19:42', '2021-01-17 11:49:42'),
('b5fc2fbfeca858e75c79d6cf83052749a97a8ff9326323d28e333e3130340d4fe72a6505e5a0e7b0', 1, 1, '*', '[]', 1, '2020-01-09 04:27:16', '2020-01-09 04:27:16', '2021-01-09 09:57:16'),
('b638504d725fd3c804688926b1e78194c98b4a9537894a8ffa525fb2b26798bf106ca7974ce1106b', 4, 1, '*', '[]', 1, '2020-01-15 08:35:19', '2020-01-15 08:35:19', '2021-01-15 14:05:19'),
('b65d3b92e01056de08af159c370e640cbc49e516dd7d82f359da1525768cf67b054241579881cb66', 10, 1, '*', '[]', 1, '2020-01-18 02:25:01', '2020-01-18 02:25:01', '2021-01-18 07:55:01'),
('b664f862f0dfa9855164faba661f1ecf010f6f5e9252c6112086ee75495cec919b0ef191847c64eb', 5, 1, '*', '[]', 1, '2020-01-18 01:52:49', '2020-01-18 01:52:49', '2021-01-18 07:22:49'),
('b678ab610e4f372d3e7ee66bd5c88fabe496b1bf82f63672149acbba66ef924d1334c4d58e86b8fd', 26, 1, '*', '[]', 0, '2020-01-09 08:19:38', '2020-01-09 08:19:38', '2021-01-09 13:49:38'),
('b68bfacbe4e31be2f122b1f0f88d8b83f52db3aa9f569f3340733acda21303ff94ac5244eec55f97', 2, 1, '*', '[]', 1, '2020-01-19 23:28:24', '2020-01-19 23:28:24', '2021-01-20 04:58:24'),
('b68d5adc5bd23e6fbd1b251fa4b57b71f259917ea8696587188cb329a7401077be6764fc77a4c9af', 74, 1, '*', '[]', 1, '2020-01-16 04:22:32', '2020-01-16 04:22:32', '2021-01-16 09:52:32'),
('b74d047a72d89e74745d75079326b6905d14f8d3cd4f5529acc7578f8ffce4ab731a3f29d231d0c4', 3, 1, '*', '[]', 1, '2020-01-15 00:24:49', '2020-01-15 00:24:49', '2021-01-15 05:54:49'),
('b887d8d9dbb6e05b0b9c2d12742ef64ad3ffa850a0740cc2a77fe4fff4fbb6fa83173fe8c41b2559', 62, 1, '*', '[]', 1, '2020-01-14 04:53:27', '2020-01-14 04:53:27', '2021-01-14 10:23:27'),
('b8b248337389d031fce4b9881e3bd4599e33bf2443424fb4d9f946c82752a1d2cb714aa31f05120a', 3, 1, '*', '[]', 1, '2020-01-16 23:54:02', '2020-01-16 23:54:02', '2021-01-17 05:24:02'),
('b8e52a78a5fee850bc6a8bab6732be5158ed06fcafaa3e9460c5d5d18ac287ccd6dff77f20b5c101', 62, 1, '*', '[]', 1, '2020-01-14 06:42:48', '2020-01-14 06:42:48', '2021-01-14 12:12:48'),
('b8e97af306ce9c7f9bd779651e4f9451fbffc3ced73ed44768d264c1e2bbb69cb5888b862b0cf414', 62, 1, '*', '[]', 1, '2020-01-14 05:55:27', '2020-01-14 05:55:27', '2021-01-14 11:25:27'),
('ba1c9f2d8aeba9164e3725287f9f40aab5f9562e68a60342733e34c4b6efb7e1c02177e2b77fbb17', 63, 1, '*', '[]', 1, '2020-01-14 04:30:22', '2020-01-14 04:30:22', '2021-01-14 10:00:22'),
('ba406cc8aac7b0d3c19c289d5c70038bf7207a5c337f804acfa09a7aeb1d763b32150975796685e8', 3, 1, '*', '[]', 1, '2020-01-09 03:19:44', '2020-01-09 03:19:44', '2021-01-09 08:49:44'),
('ba4340c4d7d6cac6342eaeb419d91b5948a3fb65e9babff870c3e181d9a83552a76ab164b3aa9886', 62, 1, '*', '[]', 1, '2020-01-14 05:21:35', '2020-01-14 05:21:35', '2021-01-14 10:51:35'),
('ba4b44c3d1e87cf66404cad4b13d6ec6c7c4fda11b67269634e6688cd3b9202af4616f5f2091b9bd', 62, 1, '*', '[]', 1, '2020-01-15 05:40:29', '2020-01-15 05:40:29', '2021-01-15 11:10:29'),
('ba81dfd1b90d308ad64ace3ad7db66e2e61bc1d20fb90af0f86998b2962a7763bc1a542ed432214c', 4, 1, '*', '[]', 1, '2020-01-16 07:10:58', '2020-01-16 07:10:58', '2021-01-16 12:40:58'),
('bbfc2efaea14abc3b3c1d709845ae52d1d9c478653ef8821a375da45d7480cfb9d14dce5b7743eb9', 7, 1, '*', '[]', 1, '2020-01-17 00:33:14', '2020-01-17 00:33:14', '2021-01-17 06:03:14'),
('bbff85937bce3179561f47b4808da33fdc379d00b23583c3a47cf95cceb8a9f94884a5b635cea9e0', 5, 1, '*', '[]', 1, '2020-01-17 03:54:29', '2020-01-17 03:54:29', '2021-01-17 09:24:29'),
('bc25cf6e14b13011eed4bc110e83adea567fabdf0703ad7509a599edfeeb945ec19ac7c52f316559', 62, 1, '*', '[]', 1, '2020-01-16 05:44:49', '2020-01-16 05:44:49', '2021-01-16 11:14:49'),
('bc6788fe619b1cfd627b24d235f7d93e5597394a9e4936b62fb69b48b97cd3ff35c3fb49b6af2f90', 3, 1, '*', '[]', 1, '2020-01-15 03:45:38', '2020-01-15 03:45:38', '2021-01-15 09:15:38'),
('bcb804f5f3f018ad7a35f8b80234a148acc2a35db85687f99b21665d7d53da8d4c5d210b04dae699', 3, 1, '*', '[]', 1, '2020-01-14 08:16:08', '2020-01-14 08:16:08', '2021-01-14 13:46:08'),
('bd422e848cc3b3be948ac36fb7e78de19091890d4334a2ba238521be5c3cf76baffba9833d7100fc', 1, 1, '*', '[]', 1, '2020-01-08 04:38:19', '2020-01-08 04:38:19', '2021-01-08 10:08:19'),
('be14b3e23a6b557b72b586696b3ce3df86f644b7118bf39689d7cf0f3939d27964a55056e033d40b', 4, 1, '*', '[]', 1, '2020-01-15 04:02:19', '2020-01-15 04:02:19', '2021-01-15 09:32:19'),
('be8d5aa9245b69859333a93e98dbad30bd877d507ba7e136aa80f14a292f3403d7d33371e35d3eb2', 32, 1, '*', '[]', 1, '2020-01-09 23:58:46', '2020-01-09 23:58:46', '2021-01-10 05:28:46'),
('bed0bad2d6321dee96b668fcf3f1fc404ecfdc36e98f40d0e64790020ac4599f53cd6b987aebdac5', 21, 1, '*', '[]', 1, '2020-01-09 07:22:27', '2020-01-09 07:22:27', '2021-01-09 12:52:27'),
('bee150cf6eb3e9cb28861f58ea731849b629bd6488932eeeee5ccf86e6e40b303863aa4f924affc1', 62, 1, '*', '[]', 1, '2020-01-16 07:24:08', '2020-01-16 07:24:08', '2021-01-16 12:54:08'),
('bf82ce06da8b6dbb447abb70d9457c71c855fdf1a135358070d0c9fce1379a443e6c58d71dfce90f', 90, 1, '*', '[]', 0, '2020-01-16 07:08:09', '2020-01-16 07:08:09', '2021-01-16 12:38:09'),
('bfa6acc4f2819ec28927455fc515486e2f25e140c01b99657e9e520d15103cd94d19aa242cff01c3', 62, 1, '*', '[]', 1, '2020-01-16 07:25:14', '2020-01-16 07:25:14', '2021-01-16 12:55:14'),
('bfafa1847a17d7bebecfb7774a11be3cf53e0a06589494dd9af663eedd3649f4bddbc7ad3b8fc07f', 54, 1, '*', '[]', 1, '2020-01-10 05:32:11', '2020-01-10 05:32:11', '2021-01-10 11:02:11'),
('c00a8ccf776f37cf07347f6a414706cac691a98bb1098c528926098beeddc7f9122c6f7bb0967848', 36, 1, '*', '[]', 1, '2020-01-13 00:30:55', '2020-01-13 00:30:55', '2021-01-13 06:00:55'),
('c065adc0a2ad585310303619771e61448e53b496e4dc51a08e9ebd46850b51459879f26a1620083c', 80, 1, '*', '[]', 1, '2020-01-16 00:48:02', '2020-01-16 00:48:02', '2021-01-16 06:18:02'),
('c0c3372127fc732ec64fb08ccc8fe6bb080f6ff7f23698c6eace669369f3de2b77f1d4c8a8e35ecb', 4, 1, '*', '[]', 1, '2020-01-15 04:43:44', '2020-01-15 04:43:44', '2021-01-15 10:13:44'),
('c0c608f78d4eb61e4d54ee02b1b5bb5a231ecc59e366f7898cd6fbafb6b77d85864859bf7010d06f', 59, 1, '*', '[]', 0, '2020-01-13 01:37:12', '2020-01-13 01:37:12', '2021-01-13 07:07:12'),
('c116bd3df6d4263675a643b152d07be542edcccbeabde2c4be9d8012675c4cd5fff43eadf2513bf2', 17, 1, '*', '[]', 0, '2020-01-09 05:19:42', '2020-01-09 05:19:42', '2021-01-09 10:49:42'),
('c1b96a814b80e7c232ed9802f1dcaedb3db030dd5f28cca236724d116585174566c2ab0604a446dd', 74, 1, '*', '[]', 1, '2020-01-15 05:28:36', '2020-01-15 05:28:36', '2021-01-15 10:58:36'),
('c1e992efca6100b87fbf7c9deb7ab8199256eb7229fed36024d1d82e782f61aec809ff43542f9106', 62, 1, '*', '[]', 1, '2020-01-15 03:21:59', '2020-01-15 03:21:59', '2021-01-15 08:51:59'),
('c20c9b98e5fe960d58d13edd01730bcca7ac460ec8532a5f4cb7ad00d2c855eaa89a19a8cc3d791d', 3, 1, '*', '[]', 1, '2020-01-14 03:41:13', '2020-01-14 03:41:13', '2021-01-14 09:11:13'),
('c28ca363511b69cdbd2ec8d418a0e52eba902336923bc0f12576e111c55d9f4a815d25e2ee3ff469', 62, 1, '*', '[]', 1, '2020-01-15 06:38:08', '2020-01-15 06:38:08', '2021-01-15 12:08:08'),
('c2c91cb17aa39798391218262e670cdb447adb9fe782714bf3b74e5e73e53b0b88cd34d3cae4c0e3', 73, 1, '*', '[]', 1, '2020-01-15 04:49:25', '2020-01-15 04:49:25', '2021-01-15 10:19:25'),
('c2ca4607683832ec35771122d6e1a1bfecf0c29cb964fff6ebdaf50e5c6e2d841dfda6345ddf2c1b', 8, 1, '*', '[]', 1, '2020-01-17 00:33:39', '2020-01-17 00:33:39', '2021-01-17 06:03:39'),
('c30b2d839905e1579a0e4e3f70161532617a84b25302e65894be24f3de17e07cd1c7a80e5a52350c', 74, 1, '*', '[]', 1, '2020-01-15 23:39:45', '2020-01-15 23:39:45', '2021-01-16 05:09:45'),
('c341a2ba7f66ca789a13b2d840a4b44b064cee2dd6ce229a9a2f2c4b161de42372a1cffe2b013956', 1, 1, '*', '[]', 1, '2020-01-17 00:48:33', '2020-01-17 00:48:33', '2021-01-17 06:18:33'),
('c343ab0fd68a9ceb91ace270f2efad4693c5a9e2431b8fd5356c34e738280cd92c94e1974b4e77e5', 4, 1, '*', '[]', 1, '2020-01-13 00:49:32', '2020-01-13 00:49:32', '2021-01-13 06:19:32'),
('c3932f01dfacdd2cc8dec8516ff3e10e75a439e49850bec6d063736db43b2b0f754e67b4de08315e', 36, 1, '*', '[]', 1, '2020-01-10 01:39:54', '2020-01-10 01:39:54', '2021-01-10 07:09:54'),
('c3b6915896fbec3ae2d62d6e033952659b5a967df2e622a3828d308ee4577f4305df2ee28a6b1d74', 62, 1, '*', '[]', 1, '2020-01-14 06:09:15', '2020-01-14 06:09:15', '2021-01-14 11:39:15'),
('c3d2d6bc631b76b2c1dc5357ed3bd0b56d717699c7b7203480f4f664fb2250bc23f4e126195fcba1', 14, 1, '*', '[]', 1, '2020-01-17 02:21:45', '2020-01-17 02:21:45', '2021-01-17 07:51:45'),
('c3e580266006caa944417a4616f319084ec9ee246d43cee95d0e13e78de5279e6bf8d94e51a3f9ea', 62, 1, '*', '[]', 1, '2020-01-15 05:01:19', '2020-01-15 05:01:19', '2021-01-15 10:31:19'),
('c3ee6c67077a3430a184759856aaaf5fde5c218bdee537d9269bb5344ec05f1f9d5270c22211a1f6', 38, 1, '*', '[]', 0, '2020-01-10 01:55:55', '2020-01-10 01:55:55', '2021-01-10 07:25:55'),
('c3f5e3c60100e7af9335a332d6a7c3f4e4fda075e78a00aa060de3379b804fe591b3f892c0823c33', 3, 1, '*', '[]', 1, '2020-01-15 03:53:30', '2020-01-15 03:53:30', '2021-01-15 09:23:30'),
('c422a7abc99bb8a0b35ae89b6771e8e40f52014c0f0db27b09079fa4d443048b6204b59786056ae7', 13, 1, '*', '[]', 0, '2020-01-17 05:12:42', '2020-01-17 05:12:42', '2021-01-17 10:42:42'),
('c425a82d1548bfaad5e555bdabf01a6244da186aee60a1ae9222098216de2bb2345ba947baafd18a', 1, 1, '*', '[]', 1, '2020-01-08 04:18:58', '2020-01-08 04:18:58', '2021-01-08 09:48:58'),
('c441a224b56a757afb28e374093688bdc101c67f1a563f3934b03ef34bd40f8db0f7833b11b27431', 46, 1, '*', '[]', 1, '2020-01-10 05:01:29', '2020-01-10 05:01:29', '2021-01-10 10:31:29'),
('c4a159be155c4cf8af05a0cf1d56fd28ee3558f7c964c7b94fa250f819e78366b38898a0fe1bfd86', 62, 1, '*', '[]', 1, '2020-01-16 01:49:52', '2020-01-16 01:49:52', '2021-01-16 07:19:52'),
('c4c62905ed4ce24c47a764583a3a789a205b4b4a1d6a071fe881e54d1c9715f52261d6f275a661b5', 74, 1, '*', '[]', 1, '2020-01-15 06:44:13', '2020-01-15 06:44:13', '2021-01-15 12:14:13'),
('c5166ea7c9547c1b7e791af6f0f790ca735a2af4b0dfcb61a4f7d3235c61ad4f42852a73f3253837', 80, 1, '*', '[]', 1, '2020-01-16 05:06:14', '2020-01-16 05:06:14', '2021-01-16 10:36:14'),
('c53308273f90e9cb4ae13a6a7826720cf9de3fbb36690656afa91d8a500d74a8d2ab865066c9b9a6', 81, 1, '*', '[]', 1, '2020-01-16 04:32:41', '2020-01-16 04:32:41', '2021-01-16 10:02:41'),
('c53cd4f466001aa16f5333d0468f7e515ff1d36d448e942f05cdc61f96e0a2f47726684c3d79fa08', 3, 1, '*', '[]', 1, '2020-01-10 06:03:54', '2020-01-10 06:03:54', '2021-01-10 11:33:54'),
('c53d92c9fa2f2d649b1a2933c25cd4adf469457f6270d4def44ba45821c772767201a7accea5eeb5', 62, 1, '*', '[]', 1, '2020-01-14 04:56:44', '2020-01-14 04:56:44', '2021-01-14 10:26:44'),
('c5f89a855dd5b3cad50c35331e31597aea1eb6378cb10f7f4304cd9efb6d22f9f3173ab991981397', 62, 1, '*', '[]', 1, '2020-01-13 06:28:35', '2020-01-13 06:28:35', '2021-01-13 11:58:35'),
('c63308a57be98ac0092ce5694947de4e8b6aa46ce83380dfaaf6af93b24dfbd8a84fb2052cf50ff8', 5, 1, '*', '[]', 1, '2020-01-17 08:08:07', '2020-01-17 08:08:07', '2021-01-17 13:38:07'),
('c651b21aa3523f6b67ae125c23b815c33db001a9c2ba1a7dc605629170443f74ef544741d3b8c87b', 62, 1, '*', '[]', 1, '2020-01-14 08:07:29', '2020-01-14 08:07:29', '2021-01-14 13:37:29'),
('c67a502f158b71e6e4407b26ee08d638642c6da03fe3baf044a55dca8bffec75eafec9c6d24839d2', 4, 1, '*', '[]', 1, '2020-01-17 00:26:28', '2020-01-17 00:26:28', '2021-01-17 05:56:28'),
('c6948a821f94e69b6226e6e08ee292ca135dd503fb20c73d551240e4625ec39343a64c10ccfbd885', 58, 1, '*', '[]', 1, '2020-01-10 07:38:17', '2020-01-10 07:38:17', '2021-01-10 13:08:17'),
('c6d2d848fb9b34c73340650b9eb21c3f2cf4ce0e55ed8fb472c0d88687f998e10f1670d13c37dbf4', 7, 1, '*', '[]', 1, '2020-01-09 05:09:59', '2020-01-09 05:09:59', '2021-01-09 10:39:59'),
('c6d9211d4e37e712e7d833a9eb7b0e4aad30352e1a0e942696ec5a91372bfd95c468142f64bb0f67', 74, 1, '*', '[]', 1, '2020-01-15 06:25:35', '2020-01-15 06:25:35', '2021-01-15 11:55:35'),
('c6f398668021ffbc75f4673906a699a52f12152c9a61e0f76018c1b71073fe0253d7fd4f2175eca4', 2, 1, '*', '[]', 1, '2020-01-20 06:03:55', '2020-01-20 06:03:55', '2021-01-20 11:33:55'),
('c746845a74f55a83eb93dd0d0c323d94939d7117786158a9676c15308ad2b6b47b0ef0588a89588a', 3, 1, '*', '[]', 1, '2020-01-08 05:55:44', '2020-01-08 05:55:44', '2021-01-08 11:25:44'),
('c768fd7549df1cfe415294a940ea40f32ec6d65d0c05571cced2a6b6bdff5cbcade5e8492ba6f49d', 62, 1, '*', '[]', 1, '2020-01-15 00:58:11', '2020-01-15 00:58:11', '2021-01-15 06:28:11'),
('c7986f0d5ad06e384f867e9bf379076904977e48e740be2b4874f09b8b3904efa64059d02b4ead06', 1, 1, '*', '[]', 1, '2020-01-12 23:24:35', '2020-01-12 23:24:35', '2021-01-13 04:54:35'),
('c7c0c3acf4f882e7f85b26d6766aa2bbf9f6dc303ecc84a28a763cb38f4ea89bae3eb0b7300ed8fc', 20, 1, '*', '[]', 1, '2020-01-12 23:34:28', '2020-01-12 23:34:28', '2021-01-13 05:04:28'),
('c7e63836a04de37c164833f2ba006f93fba887401f4ff5e448dc516ef47b1080fb7c812e95b33e32', 3, 1, '*', '[]', 1, '2020-01-09 01:54:21', '2020-01-09 01:54:21', '2021-01-09 07:24:21'),
('c7f6fa1b682957d3ab4f9c71992af1664b4b7478ef7501269e7a365db48c3f7ed0ed85dc37593587', 8, 1, '*', '[]', 0, '2020-01-20 06:04:23', '2020-01-20 06:04:23', '2021-01-20 11:34:23'),
('c8382e7448519c1951bf225c379f288f783453fabef90eca756bcaee6998507371832ae214cee09e', 7, 1, '*', '[]', 1, '2020-01-09 05:10:15', '2020-01-09 05:10:15', '2021-01-09 10:40:15'),
('c83d59bf011e9022fdde1622db7b666d932886a3a7de58ab13ac73d2de26162d79d6082c398f52f2', 61, 1, '*', '[]', 0, '2020-01-13 02:01:39', '2020-01-13 02:01:39', '2021-01-13 07:31:39'),
('c8829d1973ffaa616b773dab78416620606091a57a7208369055ec90309fd28fa27f374e1e71af17', 1, 1, '*', '[]', 1, '2020-01-20 03:35:33', '2020-01-20 03:35:33', '2021-01-20 09:05:33'),
('c8aac280dd99e67a9b580627d9e324733b634ce14c72237677a534e95aaa836c2dc0a95fd3c296b1', 7, 1, '*', '[]', 1, '2020-01-09 02:53:22', '2020-01-09 02:53:22', '2021-01-09 08:23:22'),
('c8bdfc31a855417b0c7689cb9001586aa2e49ca450aa04bb8883b91a52d4c3ff44e9b65013b80db2', 1, 1, '*', '[]', 1, '2020-01-10 08:19:46', '2020-01-10 08:19:46', '2021-01-10 13:49:46'),
('c8e805bb2f25fb17c2fbd1b7f0089a9c0a2fb7951751137f27ade9b17b8bece505f4e1f024443111', 3, 1, '*', '[]', 1, '2020-01-17 04:34:47', '2020-01-17 04:34:47', '2021-01-17 10:04:47'),
('c97ac51339390443de36571fbbee9db9a372599500b5b138dee1a4f7cd76bc1d32c0ea67e01512fa', 4, 1, '*', '[]', 1, '2020-01-17 02:56:06', '2020-01-17 02:56:06', '2021-01-17 08:26:06'),
('ca85089d13fbfe8a0e4aff84378bdc14af26c85caa0e9a5cce03a0d7e5d96aa58aed71ca826101fd', 62, 1, '*', '[]', 1, '2020-01-14 01:30:36', '2020-01-14 01:30:36', '2021-01-14 07:00:36'),
('cab88d2459d2180ccecf48c6bea67ca80837ddb9d981320473c75da5669002aad64b958c271fa14b', 7, 1, '*', '[]', 1, '2020-01-17 04:01:30', '2020-01-17 04:01:30', '2021-01-17 09:31:30'),
('cabc9938c51bb116746f2b6af41f9cfcbdc2695ddfaf5a2ffcd36ca06a4775b31ded0f3d57573455', 3, 1, '*', '[]', 1, '2020-01-17 04:36:22', '2020-01-17 04:36:22', '2021-01-17 10:06:22'),
('caee68f0081e0d6881312941f032dbc1812f42a631d9c85f08ab2b5ada3815004e94f80273a9ec5c', 62, 1, '*', '[]', 1, '2020-01-16 03:54:56', '2020-01-16 03:54:56', '2021-01-16 09:24:56'),
('caf5e8298e22eda1153f8e46b3b7001ad9d296543a2585e578fd8ff31270c232ca57ee6241b0c7e1', 13, 1, '*', '[]', 1, '2020-01-09 04:28:23', '2020-01-09 04:28:23', '2021-01-09 09:58:23'),
('cb0c414a60e222c4341ceae30561876bb4e14badcc1aa90a3b18a726d7795e785d8e148adfb3b148', 5, 1, '*', '[]', 1, '2020-01-17 00:24:00', '2020-01-17 00:24:00', '2021-01-17 05:54:00'),
('cb435b052e25b0d7900b8d8076d33ce570313a05eb870396b4909186ce084d41e785c6067539ba0e', 4, 1, '*', '[]', 1, '2020-01-15 04:35:48', '2020-01-15 04:35:48', '2021-01-15 10:05:48'),
('cb48ddc59988d2140b32ce8e3de06682f7dedaf5e4934b92cb5f5022b02a62ca77aec2a47732da5e', 62, 1, '*', '[]', 1, '2020-01-13 06:17:27', '2020-01-13 06:17:27', '2021-01-13 11:47:27'),
('cbcc962392446d41aa4ad50e97c7ffba3f87becff46908fe4a09eb172dca590f9660d4924854fadd', 3, 1, '*', '[]', 1, '2020-01-13 03:48:53', '2020-01-13 03:48:53', '2021-01-13 09:18:53'),
('cc1a5783afde45a3962a7994514550075a688accf345cb0973d5715b5579f43965c48d3ecbc1470f', 8, 1, '*', '[]', 1, '2020-01-17 04:04:01', '2020-01-17 04:04:01', '2021-01-17 09:34:01'),
('cc2dd8a04652fffb6ef000c5cd8549248af10fd7c37c89664aecf4ee80766e03206f60341f85f397', 89, 1, '*', '[]', 0, '2020-01-16 07:02:10', '2020-01-16 07:02:10', '2021-01-16 12:32:10'),
('cc441befa40220c7e2e12c1fdbe4393db36c4b0961eb9115fe638e79c42e51a733756b01a74146d9', 46, 1, '*', '[]', 1, '2020-01-10 05:01:12', '2020-01-10 05:01:12', '2021-01-10 10:31:12'),
('ccfae5da356e9552b6c172a076e016bf793fe06e540fb12c455c7ec4ac78012bfabcbf48e6e1f28e', 5, 1, '*', '[]', 1, '2020-01-18 02:22:35', '2020-01-18 02:22:35', '2021-01-18 07:52:35'),
('cd7421c0415b41df0da756a5a8a873871a42dad90eaec14876d6e4786e0d3fd67ff24019eb18cd27', 63, 1, '*', '[]', 1, '2020-01-16 00:58:27', '2020-01-16 00:58:27', '2021-01-16 06:28:27'),
('cd9e36aa3f99077e6162ba8a54ad8d6fd91debf1250a1711152a89e726b19e23b375d7ba50dcd4fc', 62, 1, '*', '[]', 1, '2020-01-14 06:34:32', '2020-01-14 06:34:32', '2021-01-14 12:04:32'),
('ce0d4b0bfbf837814a9925c688d93eca6e34633210d1ee16d745e9556ad300cc4e0fc1730d8a37e8', 62, 1, '*', '[]', 1, '2020-01-14 05:12:57', '2020-01-14 05:12:57', '2021-01-14 10:42:57'),
('ce316a40af0d98cdfc41b2016079a2379bbc112eba8ac78b3462ecc78b4ffc7295198cb4835fa6bf', 2, 1, '*', '[]', 0, '2020-01-20 06:25:47', '2020-01-20 06:25:47', '2021-01-20 11:55:47'),
('ce62782c054727727e1ae2fd3e1888f620bf733781e34dd837fd26b330c5fa13745e5ffcc222d578', 62, 1, '*', '[]', 1, '2020-01-15 06:58:03', '2020-01-15 06:58:03', '2021-01-15 12:28:03'),
('cef194c086761f14cb2cc5e8e2e6bc852dda273bbc3b8a748bf4827f4c4119fef9fb73271d517b70', 54, 1, '*', '[]', 0, '2020-01-10 05:34:15', '2020-01-10 05:34:15', '2021-01-10 11:04:15'),
('cf011cfa1e31579c27f36f9470ca2a97a6c50b3f6c8e157f48ec18f764ebf58c4d2bf1349b212906', 62, 1, '*', '[]', 1, '2020-01-15 05:03:19', '2020-01-15 05:03:19', '2021-01-15 10:33:19'),
('cf188cd4c66b35321a4e6c9dea65fa04cbc2d6dc45d4619843b8cdff325883436ce4670c4d524875', 36, 1, '*', '[]', 1, '2020-01-12 23:55:49', '2020-01-12 23:55:49', '2021-01-13 05:25:49'),
('cf4a250da19fa51caff51ee70d63381aab87aeab6a267b53f36e8a1a447e26adc75562e501f5dbbd', 4, 1, NULL, '[]', 1, '2020-01-15 08:12:14', '2020-01-15 08:12:14', '2021-01-15 13:42:14'),
('cf89ba474b402301c7a2e0b6239d1e453934c3e588dd5d9d929d1052b3c18286d4c4ccc510cf95e7', 1, 1, '*', '[]', 1, '2020-01-09 04:32:53', '2020-01-09 04:32:53', '2021-01-09 10:02:53'),
('d011ba44edbaa6c075c42c5904a7ee20e9fd61ea5be8e753a6660e11a75682502e014c485a508091', 4, 1, '*', '[]', 1, '2020-01-17 01:20:25', '2020-01-17 01:20:25', '2021-01-17 06:50:25'),
('d0a5b82f44af75bd70723de36429c60a599a8c9aa7298bb2d2ef6924c075aeda0cb404acf6efae74', 76, 1, '*', '[]', 0, '2020-01-15 06:19:53', '2020-01-15 06:19:53', '2021-01-15 11:49:53'),
('d0e97e1a919a7214636b923c4eacd4e0f1bf752b938ed6513ca93e057c53a81a4c583566a7e42f1e', 4, 1, '*', '[]', 1, '2020-01-13 06:11:34', '2020-01-13 06:11:34', '2021-01-13 11:41:34'),
('d0f9523b793d85442113ee74bd0e969f89609e1d1fff8ae571461f05ceb0be6d460aed75e973da94', 37, 1, '*', '[]', 1, '2020-01-12 23:18:30', '2020-01-12 23:18:30', '2021-01-13 04:48:30'),
('d106d8f8bf6fb5a66a1d59e8a8cc9c847d9fec6beeeb9711d700577d2bddcea3cbf516b68b657129', 13, 1, '*', '[]', 1, '2020-01-09 23:41:39', '2020-01-09 23:41:39', '2021-01-10 05:11:39'),
('d1890fa2c3625166a856b9c24622a5f5fd423e091e607d57d960e6198027c7474ccd242a6d38ed05', 36, 1, '*', '[]', 1, '2020-01-13 00:11:55', '2020-01-13 00:11:55', '2021-01-13 05:41:55'),
('d19c4a96f9253185e4ff11413fbdb13509485dedf1ec2fad5c02bb0148b47a4f105b8a3de834672b', 10, 1, '*', '[]', 1, '2020-01-17 08:46:41', '2020-01-17 08:46:41', '2021-01-17 14:16:41'),
('d1e62864730cdd77cbfec49bafd35a3fd0bd52ad776aeb869d1e69cb90b6f326964a1f89f5870ba7', 1, 1, '*', '[]', 1, '2020-01-20 04:25:01', '2020-01-20 04:25:01', '2021-01-20 09:55:01'),
('d233fc0f7b82d6265ff88bc8e76302135710017956975c860c1aa204bf6ffc0ff9a83d4dffc794d9', 20, 1, '*', '[]', 1, '2020-01-12 23:40:15', '2020-01-12 23:40:15', '2021-01-13 05:10:15'),
('d3230466c68554bdeb980a92588d0b481ae4c0f3be194083f3e322c3dacc35f855a52ef31e203939', 4, 1, '*', '[]', 1, '2020-01-18 03:03:59', '2020-01-18 03:03:59', '2021-01-18 08:33:59'),
('d36bbff049229c938f7d2e5e900ca3404a82555aad40da448ba5768feb2c05d38342577b9c7860a2', 62, 1, '*', '[]', 1, '2020-01-13 07:02:20', '2020-01-13 07:02:20', '2021-01-13 12:32:20'),
('d42c981ead5e1e0f26286e75ff41eccc6c64523987808389d51c102f206a12ccce73395142582cfa', 4, 1, '*', '[]', 1, '2020-01-17 01:00:36', '2020-01-17 01:00:36', '2021-01-17 06:30:36');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('d4b110c1d51f205ef2d7fd85900c1ec01558e57a1d1f9162a4918f61a1d2bde4f5684fa97e90d533', 3, 1, '*', '[]', 1, '2020-01-17 04:27:13', '2020-01-17 04:27:13', '2021-01-17 09:57:13'),
('d4f1061c7082f33c61f46a7f214a0043e36cb3d009f08c4ee3dde0dc9508a294b1da36712f4596aa', 6, 1, '*', '[]', 1, '2020-01-17 06:25:27', '2020-01-17 06:25:27', '2021-01-17 11:55:27'),
('d503b73ed7db90eb3803badf041481507bc85c7b6a1e566a09d045e2c8c85b225b6701dc517e1cf7', 3, 1, '*', '[]', 1, '2020-01-10 06:04:14', '2020-01-10 06:04:14', '2021-01-10 11:34:14'),
('d5673786961b49d372185e95f48e5d013ca478c194e6de703df9b364d423a2d09aa90675d837f1bd', 62, 1, '*', '[]', 1, '2020-01-15 23:43:06', '2020-01-15 23:43:06', '2021-01-16 05:13:06'),
('d5ef1cc9b9b88acd10133cd310a53879f5a1bab832b7357cc5fb5308b5fdf386bf5f0ff9c7fabc53', 50, 1, '*', '[]', 1, '2020-01-10 04:17:54', '2020-01-10 04:17:54', '2021-01-10 09:47:54'),
('d62da54cc93d6227cef14d58c103491f61c9d967b26b183c25ded1a6a9dfa2d342b899b36c25a8f3', 4, 1, '*', '[]', 1, '2020-01-14 01:42:46', '2020-01-14 01:42:46', '2021-01-14 07:12:46'),
('d632cd9ce24a9abe476d7d799863c59db52718e800480b95959026ec1d0bf541874e094bc76082c9', 1, 1, '*', '[]', 1, '2020-01-09 03:28:08', '2020-01-09 03:28:08', '2021-01-09 08:58:08'),
('d6be04f6969e197bb04410089e7d61f530154c1c5d479a999dfe83c5fcc36ca7d0783ba0ef413cdc', 16, 1, '*', '[]', 0, '2020-01-17 05:41:24', '2020-01-17 05:41:24', '2021-01-17 11:11:24'),
('d705c8ec17968966383f7993f409b9be77ec6b7f8cd8cff7043a325b50d6727584224e8906db31a4', 4, 1, '*', '[]', 1, '2020-01-13 00:39:17', '2020-01-13 00:39:17', '2021-01-13 06:09:17'),
('d751f019d830baf9953cb50ee43d80f8f6675c8e91b63faf0ee531881f5874ed6e5adeb5b767dd57', 7, 1, '*', '[]', 1, '2020-01-09 02:53:24', '2020-01-09 02:53:24', '2021-01-09 08:23:24'),
('d771e346e760247e4b679434ba727ad046576c72e75804dd8320393d782896e0a610a24ec52a6f73', 77, 1, '*', '[]', 1, '2020-01-15 08:33:48', '2020-01-15 08:33:48', '2021-01-15 14:03:48'),
('d772f5ac85cb9de6b39a301aa08bec48afa56fed16f1b65c101d6faaf3ff31dea9011c055b82127d', 2, 1, '*', '[]', 1, '2020-01-10 01:09:38', '2020-01-10 01:09:38', '2021-01-10 06:39:38'),
('d77700c1ed288df61b598b9cf58cd1c2cac570ba1d1307664a1c3a5ab02569d203b1c833eef61e2c', 62, 1, '*', '[]', 1, '2020-01-16 03:59:53', '2020-01-16 03:59:53', '2021-01-16 09:29:53'),
('d7b1eae88ce0e1e2e03818be04dc745b72f91a17a9acedd889a9328229af30d9f77fdb7460bf1972', 20, 1, '*', '[]', 1, '2020-01-12 23:53:47', '2020-01-12 23:53:47', '2021-01-13 05:23:47'),
('d7df5b3ea96aedb6c7899608d5092a7ebbd7373b5ccbf4b310292e2a565d9acbf02fba785de40687', 3, 1, '*', '[]', 1, '2020-01-14 06:27:24', '2020-01-14 06:27:24', '2021-01-14 11:57:24'),
('d855445434dc7f3c8b1136a13cc5486c39e47b55caede44045e187da0911cd67ac949dd7cb4562bc', 63, 1, '*', '[]', 1, '2020-01-16 01:27:18', '2020-01-16 01:27:18', '2021-01-16 06:57:18'),
('d875daaf5874b4c3b4a4bbe1c074fdd3b8749569e109cd9578c0486ce4e9827d0919e9e6f0a471a9', 62, 1, '*', '[]', 1, '2020-01-13 06:10:17', '2020-01-13 06:10:17', '2021-01-13 11:40:17'),
('d8835b670211a1868f7483b28603464633117074bc5e4bc65631cc3377880661608578d357e9540f', 7, 1, '*', '[]', 1, '2020-01-20 05:33:07', '2020-01-20 05:33:07', '2021-01-20 11:03:07'),
('d8ebf297811933013e1d824d84d7348701f7f0a6b7c3083facd23ac3795b1945df95a303b0fe71a7', 3, 1, '*', '[]', 1, '2020-01-15 04:13:19', '2020-01-15 04:13:19', '2021-01-15 09:43:19'),
('d8ffe3ebb8a83ecc5fa0e30bd0f878c996fa10759634b94fa7b03d0cfe3cba7755ecfa619ce067a4', 10, 1, '*', '[]', 1, '2020-01-17 07:25:39', '2020-01-17 07:25:39', '2021-01-17 12:55:39'),
('d96223fb84950b4aed2ab63a1732e240605bc163d108ec3d52564d26015adf1d04a5de71ab24e69a', 62, 1, '*', '[]', 1, '2020-01-14 06:04:47', '2020-01-14 06:04:47', '2021-01-14 11:34:47'),
('d9d9071aa9989f86b06401809cd23c63188ede27ec4279c481ef2b0bf8f031d9243b77f698b2a1d0', 1, 1, '*', '[]', 1, '2020-01-20 01:28:37', '2020-01-20 01:28:37', '2021-01-20 06:58:37'),
('d9e7737e9b9e6dd89f80902eb785b9be33553cc68c5e1b788921160a312c8c4204427761526bc5ca', 74, 1, '*', '[]', 1, '2020-01-15 04:58:35', '2020-01-15 04:58:35', '2021-01-15 10:28:35'),
('da18641672912af84959f1d23e2202a1295dcffdc36b7f5643fdab0d576dd9206b56a26ffade9a8e', 11, 1, '*', '[]', 0, '2020-01-17 09:03:22', '2020-01-17 09:03:22', '2021-01-17 14:33:22'),
('da1cfcff0b84effce8a829f12bedab8d20a75fc6f2fabbca1b13f5a168ec691c00c24d1362cc9b50', 81, 1, '*', '[]', 1, '2020-01-16 05:38:28', '2020-01-16 05:38:28', '2021-01-16 11:08:28'),
('da228d250d620b6afc5a0aa2323424845291d0b0117f870b7f242a3bf2ea9e3ab8255cfee7e00c1a', 32, 1, '*', '[]', 1, '2020-01-09 23:51:05', '2020-01-09 23:51:05', '2021-01-10 05:21:05'),
('da36bf9c9843446f405a1acba41b66bad96071ac54f10acaa53d033b358b012dd1a7067f2bbf4a33', 74, 1, '*', '[]', 1, '2020-01-15 05:46:48', '2020-01-15 05:46:48', '2021-01-15 11:16:48'),
('da670e4a1b380b9224b2a5c419a3f3d59ba50b638c65bc5c415091cb5c87fb7534ca7fd6d06919ef', 72, 1, '*', '[]', 0, '2020-01-15 02:46:30', '2020-01-15 02:46:30', '2021-01-15 08:16:30'),
('db14143031d3a530daa2a3283488bb75dc26cb25a5248c435dd75780c914be8e35ae0c9be4fbdb6f', 3, 1, '*', '[]', 1, '2020-01-10 05:16:46', '2020-01-10 05:16:46', '2021-01-10 10:46:46'),
('dbdde03364f300f28d6dbdfeaaef4adce09c0679954cf2952ef2118aace80c318a080d456b117bf2', 62, 1, '*', '[]', 1, '2020-01-16 05:25:32', '2020-01-16 05:25:32', '2021-01-16 10:55:32'),
('dc111e7702dabc0570c1117a97bfd3bf25c855f7936d06f566cc86d4b30878445bc2b6eef76cfcb6', 62, 1, '*', '[]', 1, '2020-01-13 07:19:49', '2020-01-13 07:19:49', '2021-01-13 12:49:49'),
('dc1bc88a6e98d0890d0f86d0abed88399f6904ad6daa03a1495d12803aebe653609079c1e2d2c171', 74, 1, '*', '[]', 1, '2020-01-15 05:30:41', '2020-01-15 05:30:41', '2021-01-15 11:00:41'),
('dc6839c2d8ddc40c0fa47bfa9108f129301c83aa5860d21d0ce80d492770e393b29f3c53ba79668e', 3, 1, '*', '[]', 1, '2020-01-14 23:53:27', '2020-01-14 23:53:27', '2021-01-15 05:23:27'),
('dc7de5b18284bfffcd354e5dcea00ed771e4d097fb90539f430ed7c470678f399745390a63b35b5f', 69, 1, '*', '[]', 0, '2020-01-14 03:00:25', '2020-01-14 03:00:25', '2021-01-14 08:30:25'),
('dcaba9906ed1eef2375a7c0a8c1919273b4ebfdf050aa8a0f0632484ba63daaa7ed210788c302bc1', 3, 1, '*', '[]', 1, '2020-01-16 09:17:16', '2020-01-16 09:17:16', '2021-01-16 14:47:16'),
('dcbd60b96966f8ede0a4468d0219c6871372fca265ee211362e85543a9327f376092f214e27346fa', 3, 1, '*', '[]', 1, '2020-01-15 01:55:00', '2020-01-15 01:55:00', '2021-01-15 07:25:00'),
('dce975c5b00a40c4664821bfb481c9dbe81b9c2cf1fa33737106ce394dab716cd7e37e8ce43fefc8', 62, 1, '*', '[]', 1, '2020-01-15 04:23:05', '2020-01-15 04:23:05', '2021-01-15 09:53:05'),
('dcf5013bf2adb8297900553d9c8ba0697bf9f736fe87d5e2a39b064b87024734bd4dcca5053c7d31', 14, 1, '*', '[]', 1, '2020-01-09 05:15:51', '2020-01-09 05:15:51', '2021-01-09 10:45:51'),
('dd09fe9468b4e61cae58536c356f9cdb67afcbd6c3d2b8dffe8feaf9d8afecfc228b479050856701', 4, 1, '*', '[]', 1, '2020-01-15 23:56:09', '2020-01-15 23:56:09', '2021-01-16 05:26:09'),
('dd46c0202c47f6be6405fb33e9f407e8f4bd55aaf3151a302fc6368b89a797b1c4eb7958961954fd', 41, 1, '*', '[]', 0, '2020-01-10 02:40:05', '2020-01-10 02:40:05', '2021-01-10 08:10:05'),
('dd7769e5dd357d774c45c147f3b87464e1898bb7193e18f16b01633a5b42f07e06171f0856b03537', 36, 1, '*', '[]', 1, '2020-01-13 00:21:03', '2020-01-13 00:21:03', '2021-01-13 05:51:03'),
('dd883aaccfb18ba26c2ad8332838660dce97210b4e63fcf49e64645662fec42e9c1d9ca10326cf29', 55, 1, '*', '[]', 0, '2020-01-10 06:54:15', '2020-01-10 06:54:15', '2021-01-10 12:24:15'),
('de048c4db6ba364756f3b727f91bbe556fe39f67f028d69d8c9a60b642868de9195a0e0d5a943bbb', 93, 1, '*', '[]', 1, '2020-01-16 10:02:09', '2020-01-16 10:02:09', '2021-01-16 15:32:09'),
('deb2dffada11a730b611a4cf50cd7eb18284e4434f8b88ffdd874a562517ae4c9de575976001afaf', 62, 1, '*', '[]', 1, '2020-01-14 06:11:40', '2020-01-14 06:11:40', '2021-01-14 11:41:40'),
('deecc303afddcd73cd6d87ce0da4e8e74f6ba6a0ee4397b04816f4455ba465d31f57b765257df74c', 40, 1, '*', '[]', 1, '2020-01-10 02:30:25', '2020-01-10 02:30:25', '2021-01-10 08:00:25'),
('defe9425718f0cd2f12ce6719fc302e2f1fd50e7308278991f9b0d6e40b20ae8c7d967ea42d49140', 2, 1, '*', '[]', 1, '2020-01-17 00:45:01', '2020-01-17 00:45:01', '2021-01-17 06:15:01'),
('df65df911675f41bc2c574f50ae62c82fd239bc7e49bca72f82da7ed87552076fbf4dd512604fa86', 2, 1, '*', '[]', 1, '2020-01-16 23:18:11', '2020-01-16 23:18:11', '2021-01-17 04:48:11'),
('e00351f9cf00dca59710cfc39aaa76014a8a107dd52771fa25017d217a3b456375c5c7e5ff6d4f9a', 3, 1, '*', '[]', 1, '2020-01-09 08:49:00', '2020-01-09 08:49:00', '2021-01-09 14:19:00'),
('e00d6bf53de458ca2916b455d729dc0a8e6d73163b3e3304c9c99a943ce308d6ed4d4d40dc7dbc9c', 3, 1, '*', '[]', 1, '2020-01-17 04:36:17', '2020-01-17 04:36:17', '2021-01-17 10:06:17'),
('e09dcdf86eec71af84d9abf3fe18584c4157535c0f4797357be6d0cc7d8da90bb1271f9ad4f07f3c', 1, 1, '*', '[]', 1, '2020-01-20 04:46:29', '2020-01-20 04:46:29', '2021-01-20 10:16:29'),
('e0e6b083e25a32e44524477babfb6bbbfa7d57cb19564d9db68eeee78d5cfc97a2ede8c06c5b146e', 62, 1, '*', '[]', 1, '2020-01-13 07:17:23', '2020-01-13 07:17:23', '2021-01-13 12:47:23'),
('e174cdd3a4a2fc1f378e7252ee4f9611bc4787e471365e3eb00b4d43ac7ae9c8048abdc0ad04b352', 3, 1, '*', '[]', 1, '2020-01-17 04:36:10', '2020-01-17 04:36:10', '2021-01-17 10:06:10'),
('e1efd9d20243fd3a6f3cc30ff26f545bfefeab790247bd0cbeccbe0a0c5ca28072d635d942a8ae79', 36, 1, '*', '[]', 1, '2020-01-12 23:44:55', '2020-01-12 23:44:55', '2021-01-13 05:14:55'),
('e1f32f512e0bdd9c0c5b7cce3901d592da4d796300cd3315ef0115e8ff8bbe09b926c2ee947850c1', 46, 1, '*', '[]', 0, '2020-01-10 05:18:32', '2020-01-10 05:18:32', '2021-01-10 10:48:32'),
('e2541596b0e048f3f507e2e5854b64637ec64d78bfb605c41d2bc693e2694dae6a088a181f4fd2fc', 5, 1, '*', '[]', 1, '2020-01-17 04:15:47', '2020-01-17 04:15:47', '2021-01-17 09:45:47'),
('e2c6500c47d86a06c4fcc8dd2508b640f1d01fb629782ad4dc719b9a359e8b4075fbd199ede40dd6', 9, 1, '*', '[]', 0, '2020-01-20 06:26:14', '2020-01-20 06:26:14', '2021-01-20 11:56:14'),
('e2cc17f74b870832bcdd47e7bbecc1f2cf47f2ca30b40457f6cf9b406fead3bba3b108658a3cedb6', 62, 1, '*', '[]', 1, '2020-01-15 23:21:22', '2020-01-15 23:21:22', '2021-01-16 04:51:22'),
('e31b3a4d7439de14eb5eff2584a004c31e294a52bab497a226a2daee5c48e70f2df3e4483c6e6899', 3, 1, '*', '[]', 1, '2020-01-13 23:32:10', '2020-01-13 23:32:10', '2021-01-14 05:02:10'),
('e36f3df1f843d8f4dbba1f89666540ea38fca7f946f89389a42b25b252f8ed94993a21efe797d29f', 81, 1, '*', '[]', 1, '2020-01-16 05:46:00', '2020-01-16 05:46:00', '2021-01-16 11:16:00'),
('e41c27658b55dd536306f9b481eb53fcfbc7aff59806147b03b60589c98a7f4306a92a14e8f2c380', 1, 1, '*', '[]', 1, '2020-01-13 00:20:17', '2020-01-13 00:20:17', '2021-01-13 05:50:17'),
('e439b3b40db1e2df8f7c8250bc465544aa2d6de24535c6a41defca1c1abf2f3578e6d04920b51ff5', 1, 1, '*', '[]', 1, '2020-01-20 01:37:26', '2020-01-20 01:37:26', '2021-01-20 07:07:26'),
('e45876bf273f08b4bf8ba778a9b048a9691881b90f50f3c04df33ec6e2c857a610f60d7b75cdac67', 3, 1, '*', '[]', 1, '2020-01-17 02:46:34', '2020-01-17 02:46:34', '2021-01-17 08:16:34'),
('e45cf57893524623d688835d5f526aa40924d3ae79763e7e15fbee740ae0a12d59f1ed39cc822ab5', 62, 1, '*', '[]', 1, '2020-01-16 05:56:38', '2020-01-16 05:56:38', '2021-01-16 11:26:38'),
('e4d292be7870f0333470a862753ed9400f89a16bbd8dcc9a3c33204b72080f930f36474c995d7caf', 36, 1, '*', '[]', 1, '2020-01-13 00:16:17', '2020-01-13 00:16:17', '2021-01-13 05:46:17'),
('e4db2539c2cdc75f83ef302b17b6245d4a68e8c224dab69f5d9b898b43c64356bf62fb994ef581de', 67, 1, '*', '[]', 1, '2020-01-14 01:47:20', '2020-01-14 01:47:20', '2021-01-14 07:17:20'),
('e518f30cd8afe1298c1c2027eb4a07e97709b6603fabfde876e54820acbe225c9ec31194c2d720c9', 1, 1, '*', '[]', 1, '2020-01-20 00:02:36', '2020-01-20 00:02:36', '2021-01-20 05:32:36'),
('e519e0f1e899a35dc8d798a86fa9036f79f37a9203758679e1e0bd1a157cdd8dd960ea81f3b80dc1', 7, 1, '*', '[]', 1, '2020-01-09 02:54:35', '2020-01-09 02:54:35', '2021-01-09 08:24:35'),
('e52e49d9477188b8c1442896d7ca84f4655d53d90de5f312e252938dd7bf85f44a2e2f4553c773b3', 7, 1, '*', '[]', 1, '2020-01-17 08:33:10', '2020-01-17 08:33:10', '2021-01-17 14:03:10'),
('e53b41e98d8d9c2b26dfe8286aadacb4029a9c889f5e12ee5d5b81f90ea239137e22da6eea9f099c', 10, 1, '*', '[]', 1, '2020-01-09 04:21:23', '2020-01-09 04:21:23', '2021-01-09 09:51:23'),
('e599f1879f61d73c16c9b4d1793ee58d88c625c6ebab3c6ff261cc1e4435a09ee71591373579be4d', 15, 1, '*', '[]', 1, '2020-01-17 02:23:26', '2020-01-17 02:23:26', '2021-01-17 07:53:26'),
('e5eacf43147830a410876e41a3d93e4d0354972dae9312ab945bbaa4afecd438c93da03cc2b1eb07', 12, 1, '*', '[]', 1, '2020-01-17 04:52:50', '2020-01-17 04:52:50', '2021-01-17 10:22:50'),
('e63e736cad29d285437c183fb6e8f65750d9acec9dbb926f256d9c334adfcf7fa67dd1f7984d0ed6', 81, 1, '*', '[]', 1, '2020-01-16 04:25:40', '2020-01-16 04:25:40', '2021-01-16 09:55:40'),
('e644c51d5aa2ad80f8aae1672466d78b2ae04f45d66308ea33b5ed4c97c43475284f57eb80120929', 5, 1, '*', '[]', 1, '2020-01-18 01:51:17', '2020-01-18 01:51:17', '2021-01-18 07:21:17'),
('e64c1bd56f13fea974f79e0f58b0b8a0424dae1317f1a5023dc537114ac74465406deff6dd0a3d9c', 3, 1, '*', '[]', 1, '2020-01-10 04:27:43', '2020-01-10 04:27:43', '2021-01-10 09:57:43'),
('e65c27c9e961618c78507a05502b26abe06e5c4e0b50196911780683365e461d6d69eeca41a3102b', 87, 1, '*', '[]', 0, '2020-01-16 06:47:59', '2020-01-16 06:47:59', '2021-01-16 12:17:59'),
('e65dbdd10ccb2e0258baf10a2c962a6d560eca9bab3ef9402e41313175a6b4175d295e3048086d1e', 3, 1, '*', '[]', 1, '2020-01-15 02:42:23', '2020-01-15 02:42:23', '2021-01-15 08:12:23'),
('e6b6c1deebf693b62aacd2503014ff41dfb735d033d8a9b390f915c43bca8bb1d278b977bc776f8f', 62, 1, '*', '[]', 1, '2020-01-14 08:14:19', '2020-01-14 08:14:19', '2021-01-14 13:44:19'),
('e72c15fbe9958c06591048f6a8e7dfafb4bfee8dba13c6317d0b604991f1eea036af775522f9284c', 8, 1, '*', '[]', 1, '2020-01-20 05:30:39', '2020-01-20 05:30:39', '2021-01-20 11:00:39'),
('e73828db9b5bd7b2e20a3639d2aeee7e63502e682d5fbb9cba4901d032449704288cd721e72fb8eb', 3, 1, '*', '[]', 1, '2020-01-17 04:38:39', '2020-01-17 04:38:39', '2021-01-17 10:08:39'),
('e755c1729284ae269da6894b12a09a947e13b10bdc602b8fca3240458e59f797c24568a3d73a6caa', 4, 1, '*', '[]', 1, '2020-01-14 00:38:28', '2020-01-14 00:38:28', '2021-01-14 06:08:28'),
('e75bd00ec99fe215840b9e6f450c2844ee77d1ee22b00523389600353400be81588f53a5cf7bb1cb', 36, 1, '*', '[]', 1, '2020-01-12 23:56:17', '2020-01-12 23:56:17', '2021-01-13 05:26:17'),
('e7b28ab33bff992f101331c419d285cb9d828aa94ba7a1a5f7c471058b3e48cc54ddbd6f0d298a45', 62, 1, '*', '[]', 1, '2020-01-15 06:53:13', '2020-01-15 06:53:13', '2021-01-15 12:23:13'),
('e7daa65b6172195702141a158f6b638e1d06e9963d549952ec10fde39570b23d626b5635ddb67caf', 2, 1, '*', '[]', 1, '2020-01-20 00:20:42', '2020-01-20 00:20:42', '2021-01-20 05:50:42'),
('e7f5c63feaa23b3f497838034722540d8892f83ceee219d0e443feaf79c82fb23dfc50220dfc0704', 85, 1, '*', '[]', 0, '2020-01-16 05:12:55', '2020-01-16 05:12:55', '2021-01-16 10:42:55'),
('e813e6d34766ecb4acafab5464116037b50cc875de3410433e583fd420390f02a0390f5d5e0b8342', 36, 1, '*', '[]', 1, '2020-01-10 09:07:28', '2020-01-10 09:07:28', '2021-01-10 14:37:28'),
('e847066084c3160760f4e9d0e713b56c7c607d1c1203a3a539ce11f8227a2a1da6ddb38ddef069a7', 25, 1, '*', '[]', 0, '2020-01-09 08:19:14', '2020-01-09 08:19:14', '2021-01-09 13:49:14'),
('e88e68a6d3622eaf84f1e9626cbae28619d4cfd8ab5f3eb57b733e00991504c5cb2a92f6695b2832', 1, 1, '*', '[]', 1, '2020-01-10 08:15:57', '2020-01-10 08:15:57', '2021-01-10 13:45:57'),
('e9df6783c33dd0d043a877a9db921b354d1a43e1863d8ef27e3791dcfeb6e8442a60ac2aab8eb1e0', 80, 1, '*', '[]', 1, '2020-01-16 06:01:42', '2020-01-16 06:01:42', '2021-01-16 11:31:42'),
('e9e7b07e3b772aee550f8d6ee116ed3ea09dff472f78a735395d45e34e4a956a70d574d3edfd2d34', 81, 1, '*', '[]', 1, '2020-01-16 05:24:29', '2020-01-16 05:24:29', '2021-01-16 10:54:29'),
('ea72748b7339615e7fd978772dd41d3bff0acb524cfce35f1aafece64901e7167243ebfd8aabcb55', 62, 1, '*', '[]', 1, '2020-01-14 06:52:00', '2020-01-14 06:52:00', '2021-01-14 12:22:00'),
('ea9c0008f9ffd265db9a87face3ac7ae10070e108a9ce08b4f41bf884090ec615306d91582c5c04e', 63, 1, '*', '[]', 1, '2020-01-14 03:52:17', '2020-01-14 03:52:17', '2021-01-14 09:22:17'),
('eaeefea45ac74d1978285f0578baf4fe5dfa0393d200cab4dc9dd1996544d80d7f58320d8c453aa9', 1, 1, '*', '[]', 1, '2020-01-20 04:54:06', '2020-01-20 04:54:06', '2021-01-20 10:24:06'),
('eb603f53313c7b7e85e793fe5c6b1dbe5e6a46b45dcca5c69d19e6b21c9da51a0e32995f14be8a16', 4, 1, '*', '[]', 1, '2020-01-10 04:27:19', '2020-01-10 04:27:19', '2021-01-10 09:57:19'),
('eba611b06df6e8153d148695c42f0ec5da6fd51ab23b51e0af7abc543d9bac2bc2cac079bcee9ef4', 4, 1, '*', '[]', 1, '2020-01-15 04:57:25', '2020-01-15 04:57:25', '2021-01-15 10:27:25'),
('ec029773c7989b5da4f96cd1fc0b5caf811a31d13c1c6eeabc08a2b64cb392945f3431d20ca5273f', 62, 1, '*', '[]', 1, '2020-01-15 05:02:01', '2020-01-15 05:02:01', '2021-01-15 10:32:01'),
('ec039096bbf406502499ba0bd7591b9bc40c3471fe621785c36d8c283b8950d82ca250b5b2950a77', 62, 1, '*', '[]', 1, '2020-01-15 04:05:10', '2020-01-15 04:05:10', '2021-01-15 09:35:10'),
('ec290331ad249ef0deb01f151baf9a7a0b7ca61efc99f3e03b081727e5d0f830637bfaf3f7ce1906', 80, 1, '*', '[]', 1, '2020-01-16 00:32:57', '2020-01-16 00:32:57', '2021-01-16 06:02:57'),
('ec34b8abf2aa3d437c6b92dd398f4fda1f38b15791b4c667eee3af5eddc5aabc13a8460923457522', 4, 1, '*', '[]', 1, '2020-01-15 05:23:21', '2020-01-15 05:23:21', '2021-01-15 10:53:21'),
('ec605260ff73c80d505c38f912b4d2b486afc61724b459ad00da2b805072339d61b38426d2926890', 36, 1, '*', '[]', 1, '2020-01-10 08:50:15', '2020-01-10 08:50:15', '2021-01-10 14:20:15'),
('ec68bcad6d442eeae450be920ed0e9bf3b55aa4f9894924b8f72fff2840c88f706579aac796110d6', 2, 1, '*', '[]', 1, '2020-01-20 01:26:30', '2020-01-20 01:26:30', '2021-01-20 06:56:30'),
('ecc738cd86fbe2d40ca614ae77c64298dfbb24c15588085fb50d08f253ef109d79d36eca2bcdd4cb', 15, 1, '*', '[]', 1, '2020-01-09 05:06:33', '2020-01-09 05:06:33', '2021-01-09 10:36:33'),
('ecccfc59715bfe94498fa0a02eb953a6b0e4d8b35c403909902b6a394ae6f5db9f564d62a2493c9d', 2, 1, '*', '[]', 1, '2020-01-20 00:56:15', '2020-01-20 00:56:15', '2021-01-20 06:26:15'),
('ece7aedfa5d950e7f0f583762d842604c4bd98a66de2e2809034ed5e800a163ba62fc26634dcdffc', 4, 1, '*', '[]', 1, '2020-01-18 03:06:28', '2020-01-18 03:06:28', '2021-01-18 08:36:28'),
('ed55ccddbc256f4ecdf65f2ec678d67f1b9713c5e88adda04f8dacd6672d757279ecdba5c4eccf6e', 3, 1, '*', '[]', 1, '2020-01-14 06:20:47', '2020-01-14 06:20:47', '2021-01-14 11:50:47'),
('ed9be7e709dfc622fc350966ca28b8525a0063ad2ac9808eed8f5a38e1016262c0607e846be8d735', 1, 1, '*', '[]', 1, '2020-01-20 02:19:15', '2020-01-20 02:19:15', '2021-01-20 07:49:15'),
('ededd02c8eb4ce8d22b1bdb8a66578fb6d87b3f4b7d2a2342df337ca847c698162fc44d0e240f6be', 1, 1, '*', '[]', 1, '2020-01-08 02:42:25', '2020-01-08 02:42:25', '2021-01-08 08:12:25'),
('ee0a0992dd11e6a6d10f40bb15790744a3ea72fb8fc320d5ab140a5faa7de16751b24d19bb8543a2', 7, 1, '*', '[]', 1, '2020-01-09 02:36:09', '2020-01-09 02:36:09', '2021-01-09 08:06:09'),
('ee0aa972c3e636dd3cb377c6755ea4ff821d1927e4fb1c62e09982db2cb4bfbd74fa23139b05ab6c', 64, 1, '*', '[]', 1, '2020-01-13 06:54:27', '2020-01-13 06:54:27', '2021-01-13 12:24:27'),
('ee6b4ce958068a79d7022312be14247401f4008e3b75c251de56c20853a4fab0149a68d07d00957f', 3, 1, '*', '[]', 1, '2020-01-15 02:06:25', '2020-01-15 02:06:25', '2021-01-15 07:36:25'),
('ee9613ca2e6478cc2cca2158312306e7feb60ebe0fe9c2133f938a5c9a6bfeb4c6f2626c2033852b', 3, 1, '*', '[]', 1, '2020-01-08 03:53:44', '2020-01-08 03:53:44', '2021-01-08 09:23:44'),
('eeab2e51e4f2cfe8fae5ee0971d46f67b543c8ed207b434a152705df203012afe6bcf4b91f4bde69', 2, 1, '*', '[]', 1, '2020-01-17 01:18:27', '2020-01-17 01:18:27', '2021-01-17 06:48:27'),
('eec8e0c081eec17fc9c25dc636149a6597dc65b5a17e76d472734aeb684fa129c8df68e75be0aacd', 62, 1, '*', '[]', 1, '2020-01-15 23:55:18', '2020-01-15 23:55:18', '2021-01-16 05:25:18'),
('eef04a8243711203c2596b840dbfc655c54aae31e11dd729cf256d38c1f2ffbc7e34f94cf24de0bd', 1, 1, '*', '[]', 1, '2020-01-08 03:54:23', '2020-01-08 03:54:23', '2021-01-08 09:24:23'),
('f0214c20886f6ff4252957e43e9a3873142d64a59e70ad953a94c126e13d5fb8c88568e4913d823f', 62, 1, '*', '[]', 1, '2020-01-13 06:08:29', '2020-01-13 06:08:29', '2021-01-13 11:38:29'),
('f070053d48a56156ceafc169da6b6bc312c94647f8e0b21ec311faadc45d9aacf36dbacaaff1aba0', 63, 1, '*', '[]', 1, '2020-01-14 04:22:58', '2020-01-14 04:22:58', '2021-01-14 09:52:58'),
('f08335f9c62711205aab01acb2501f0ecf46527a08ff5f0a9adeaea3ea56c83f20e3c51b155ba84a', 6, 1, '*', '[]', 1, '2020-01-17 00:25:24', '2020-01-17 00:25:24', '2021-01-17 05:55:24'),
('f086fe4f20207bbac0ce0d55915ab560513d8c3439261c75c189be226fb14978fda9558068963edd', 4, 1, '*', '[]', 1, '2020-01-13 00:38:41', '2020-01-13 00:38:41', '2021-01-13 06:08:41'),
('f15b273ca6431fc5c270ceaa4c35ca5d855429537b017e44fdf00aae9acf4587230eadd6f8fffccc', 1, 1, '*', '[]', 1, '2020-01-08 02:36:09', '2020-01-08 02:36:09', '2021-01-08 08:06:09'),
('f17a8e11ebf68da53127eb11def1e7706ea120a93aa11d8924b7f7ca07d0e341fd3c9173853f8622', 62, 1, '*', '[]', 1, '2020-01-16 03:05:54', '2020-01-16 03:05:54', '2021-01-16 08:35:54'),
('f1da446a82ccd486950382a6b28baa76df9d2b7874c45bf736b16871038598a74bf75de9ffe0f170', 36, 1, '*', '[]', 1, '2020-01-10 01:47:01', '2020-01-10 01:47:01', '2021-01-10 07:17:01'),
('f23cc4fc00695b6a033d58a69fea32683d5dce9297180b24ea8bbcc05abf8f482754098721969ee4', 74, 1, '*', '[]', 1, '2020-01-16 04:34:56', '2020-01-16 04:34:56', '2021-01-16 10:04:56'),
('f25c5b67ffc73743142edde22fb176d0bce2587329049f9633b389582fbca568690419f4b623ef4b', 36, 1, '*', '[]', 1, '2020-01-10 08:52:48', '2020-01-10 08:52:48', '2021-01-10 14:22:48'),
('f27dc4029722bedd31c3d87c70d9f3802f6c126230205518e758a51807ce2ace803c95e5df391c1c', 12, 1, '*', '[]', 1, '2020-01-17 01:18:31', '2020-01-17 01:18:31', '2021-01-17 06:48:31'),
('f2bd0d1175099664f26147de40b4d213e4d0685e8007d7e8bcf6cf1bc08460928814b415f971bd58', 4, 1, '*', '[]', 1, '2020-01-15 06:52:51', '2020-01-15 06:52:51', '2021-01-15 12:22:51'),
('f2d7b01275781464b1db52bac2e149685cd3d44fc63a9b31214563744d915ebb9caa05117b3f079e', 9, 1, '*', '[]', 1, '2020-01-20 06:24:41', '2020-01-20 06:24:41', '2021-01-20 11:54:41'),
('f2f9d658b2c887f03b494fb97763c2a962b6ec21953d2f31114c7b33fe627c61b662e5784319e456', 19, 1, '*', '[]', 0, '2020-01-09 06:09:07', '2020-01-09 06:09:07', '2021-01-09 11:39:07'),
('f3326ad3afcbfec8395f90e367d7f49578e76650d4b1cec3e2f914345bdb9c13462e923eb68883d0', 62, 1, '*', '[]', 1, '2020-01-16 01:02:54', '2020-01-16 01:02:54', '2021-01-16 06:32:54'),
('f336e65290b666c504b37deb80bb9b43a50fe0f06bf175ab09b580bc6682b12c679b37f9b561832b', 63, 1, '*', '[]', 1, '2020-01-14 06:18:37', '2020-01-14 06:18:37', '2021-01-14 11:48:37'),
('f35f31661f1123f526a617feb39751931c276a87d2db1fb58f902c54e9d4a0946d981d2fdd724c51', 1, 1, '*', '[]', 1, '2020-01-20 06:05:06', '2020-01-20 06:05:06', '2021-01-20 11:35:06'),
('f3d0a0a93bb16fdbf945993a72034e55f216bb461edc3e17a65f3565dae4cf04c3d856367d19e7e4', 9, 1, '*', '[]', 1, '2020-01-17 04:49:18', '2020-01-17 04:49:18', '2021-01-17 10:19:18'),
('f47fbaf850387a53891fa9b02f52663a9ab3baf52a99c8e8398edba11260d9088be7d731bc8a03f9', 3, 1, '*', '[]', 1, '2020-01-15 07:05:20', '2020-01-15 07:05:20', '2021-01-15 12:35:20'),
('f59453468245768887d15a1d8237de404583a334fde1b7761b1f12e114f9ab3fa0dcba04170bd508', 1, 1, '*', '[]', 1, '2020-01-09 04:04:06', '2020-01-09 04:04:06', '2021-01-09 09:34:06'),
('f5994a155f0fd983f7d3176bffaa17e0fd6876450ff3695d8bf2eabc1df301c58db41f477c521c72', 22, 1, '*', '[]', 1, '2020-01-09 07:57:11', '2020-01-09 07:57:11', '2021-01-09 13:27:11'),
('f5a155e70db047f952d6ac6e40afa66f1a1d08e798488bb7fe065d38a479f1d4a32440006aae2286', 3, 1, '*', '[]', 1, '2020-01-09 03:35:00', '2020-01-09 03:35:00', '2021-01-09 09:05:00'),
('f6122d7a0aa0bfc641bbb14aae71531b7ce690997ce2ba85dfbd0f94e2e53da53759b56312e0062b', 3, 1, '*', '[]', 1, '2020-01-14 06:26:36', '2020-01-14 06:26:36', '2021-01-14 11:56:36'),
('f62eb1168390e6901ac50d8a19be54754b25c2c954028066d4d8938fa04bce0e82689b530c1410f1', 20, 1, '*', '[]', 1, '2020-01-13 03:03:03', '2020-01-13 03:03:03', '2021-01-13 08:33:03'),
('f63da3f4c7d97cf94bd4d016db1bde9bd45cc3fbdeedc551b1508df1c33dbc3b908396e2b5bbdd79', 5, 1, '*', '[]', 1, '2020-01-17 04:17:19', '2020-01-17 04:17:19', '2021-01-17 09:47:19'),
('f69daa075adb8c0c8f085dc337da3cd222aa8b6e5134dc9c4093b84be7aaf67aa3501ea34dc2d4c4', 3, 1, '*', '[]', 1, '2020-01-14 04:38:46', '2020-01-14 04:38:46', '2021-01-14 10:08:46'),
('f6a5572596ed28d3b8f4161258668567fae9cda2b4ba5db0b9b8f81b1c8bddc361bd930f3b3a3442', 62, 1, '*', '[]', 1, '2020-01-14 06:12:32', '2020-01-14 06:12:32', '2021-01-14 11:42:32'),
('f71c539eefa00ca5e2e8b8b3f2b0adbb9398f0eaa8da68e316b76c4e8c1eff27d3fa965a385afca6', 62, 1, '*', '[]', 1, '2020-01-16 01:53:10', '2020-01-16 01:53:10', '2021-01-16 07:23:10'),
('f756b3727072e0d9f91cfdfb8f7807edce6f50dcbdab5c27c2cd1a30e4d3921adcaf7580c71ae3a0', 62, 1, '*', '[]', 1, '2020-01-16 06:05:18', '2020-01-16 06:05:18', '2021-01-16 11:35:18'),
('f7e24f0c11344f7b1701b1f9d77ff093ca012e3d22f21ddc129bb77b4333414ee39dece963c073dc', 3, 1, '*', '[]', 1, '2020-01-15 01:48:54', '2020-01-15 01:48:54', '2021-01-15 07:18:54'),
('f7eb5c25986a7760b6e19d5ff5485d7bac9c580e8c87da3c264d1fd3ec2cbb18c750b31d68a82a3d', 46, 1, '*', '[]', 1, '2020-01-10 05:01:06', '2020-01-10 05:01:06', '2021-01-10 10:31:06'),
('f8070ce53277ced42e0e2ac150ecbb97dde595cd734c2ac7c5bf98cc8d4d195c0be32b9bc74fd8f8', 1, 1, '*', '[]', 1, '2020-01-08 02:16:02', '2020-01-08 02:16:02', '2021-01-08 07:46:02'),
('f825de2d1dbbb1733ec139b172ece942440c705eac34a5ad0e075ed2add25cb541a7676e2b7259ec', 62, 1, '*', '[]', 1, '2020-01-14 08:15:37', '2020-01-14 08:15:37', '2021-01-14 13:45:37'),
('f833e4aab911719eaf57ffbb59196ac0d4dd90d660a87cae537ad2f1f5c7dee841e99bcb0d1dd8eb', 3, 1, '*', '[]', 1, '2020-01-10 06:23:57', '2020-01-10 06:23:57', '2021-01-10 11:53:57'),
('f845eed9a63074a088eff9502544c88df0e4d19e92c55a4b0e5de2482b066689b0bf5ef30c5611e6', 4, 1, '*', '[]', 1, '2020-01-15 06:54:20', '2020-01-15 06:54:20', '2021-01-15 12:24:20'),
('f8a4cfe8718bf29c05e6fd523c03a1d8d6b4ec5b782e7eab583fd42874cc0c222f9d7dfc24afe546', 2, 1, '*', '[]', 1, '2020-01-20 05:50:09', '2020-01-20 05:50:09', '2021-01-20 11:20:09'),
('f8c0623cb50843eae0517e6aa30f12e4e0b255165da402da399e725660cadf0631d31ea8d449d873', 50, 1, '*', '[]', 1, '2020-01-10 04:53:41', '2020-01-10 04:53:41', '2021-01-10 10:23:41'),
('f92b12dd26ccc8782b99536058a8f54a8aab8d88828a48757234a966c6a55ed5f9256833c8e4d37f', 2, 1, '*', '[]', 1, '2020-01-20 00:03:17', '2020-01-20 00:03:17', '2021-01-20 05:33:17'),
('f93369d9da287d1b3ea858ebe469161226eeb220815e02dd4c6b5f11db98dacc2f861eb0d35b5e58', 62, 1, '*', '[]', 1, '2020-01-13 07:13:05', '2020-01-13 07:13:05', '2021-01-13 12:43:05'),
('f97e5e2b90c17e3b00ff6236e1ad01e57aca636092b4164f91594f39296e18c8083eab0b56d7fb68', 74, 1, '*', '[]', 1, '2020-01-16 07:16:55', '2020-01-16 07:16:55', '2021-01-16 12:46:55'),
('f99047c792751c8f0196fd3dc03a0cb20901995832f6dabbd679d9ff96e27d7544caeb50f1e62122', 27, 1, '*', '[]', 1, '2020-01-09 08:37:32', '2020-01-09 08:37:32', '2021-01-09 14:07:32'),
('fa04f8a0d79b80fea82317e8a39f88f3b02da7307b3d1908165d2ff0fac63002afcf7c57f93184cd', 4, 1, '*', '[]', 1, '2020-01-17 00:18:14', '2020-01-17 00:18:14', '2021-01-17 05:48:14'),
('fa348e400de4687ca8d01f2092af5045ec890840d24da25415c07aabac66a61e2ccb2411262f748b', 3, 1, '*', '[]', 1, '2020-01-15 01:17:44', '2020-01-15 01:17:44', '2021-01-15 06:47:44'),
('fa7ad9352eea531d47bdf7c4da956e29cdb23af575fb109677d9cd36e4e488582f9d5f1c20e43fa7', 1, 1, '*', '[]', 1, '2020-01-13 00:21:37', '2020-01-13 00:21:37', '2021-01-13 05:51:37'),
('faad8581456e10ccd716ac5c0d5ea3f65a950df83e0c93d64073a38c4388d92b453814ae0163b77a', 63, 1, '*', '[]', 1, '2020-01-16 04:09:52', '2020-01-16 04:09:52', '2021-01-16 09:39:52'),
('fb8d1c0167794b9f68468d0e39a52f3ee6a1591a1112ea1f00ac67dd9e7a7b1ae276a689903bd5aa', 7, 1, '*', '[]', 1, '2020-01-09 02:42:57', '2020-01-09 02:42:57', '2021-01-09 08:12:57'),
('fb939e231ba61755f8dd0098619f224f66118f618b757e6fa02ddfc32d432fee3c4020c0f89700df', 10, 1, '*', '[]', 1, '2020-01-20 06:28:10', '2020-01-20 06:28:10', '2021-01-20 11:58:10'),
('fb991a19d1f1237d1122cf9056a8b5bbd2ff3dd04bb3a25640e48b12e174f44f88a89fcc5ab25738', 62, 1, '*', '[]', 1, '2020-01-14 02:44:05', '2020-01-14 02:44:05', '2021-01-14 08:14:05'),
('fbb3156a1dcdb719b0857b0fdcf264c8d91f1350927e13e6ef3016a72139cbe96542df1e88f2d89d', 31, 1, '*', '[]', 0, '2020-01-09 23:49:46', '2020-01-09 23:49:46', '2021-01-10 05:19:46'),
('fbdc291c137a0e74ab8a894005875bbaaf938904214cff790272911a98520cb87617e5a673dad09e', 4, 1, '*', '[]', 1, '2020-01-17 06:36:12', '2020-01-17 06:36:12', '2021-01-17 12:06:12'),
('fc18eeb31c57571e2bd55460a490a1a8b632ee1f2508b62ac2fa96f4447964bf2691afc8dd81a243', 62, 1, '*', '[]', 1, '2020-01-16 06:15:28', '2020-01-16 06:15:28', '2021-01-16 11:45:28'),
('fc3b61cb7d8a4a37f272a7225b04f0b271dd269d3e57454ddfec35e49524ff392a1a0248bdf0ee59', 62, 1, '*', '[]', 1, '2020-01-15 04:40:01', '2020-01-15 04:40:01', '2021-01-15 10:10:01'),
('fcb4add0df85e6c0a54e3ebce81fa6c48bcc202babad87908e1cbb30b7bab1d2e585dd135d919bae', 20, 1, '*', '[]', 1, '2020-01-15 02:09:11', '2020-01-15 02:09:11', '2021-01-15 07:39:11'),
('fcf67525dd0a2ac06642ef8f082aef88d1ad5a203bce3691ca235bf7e1c1f2a42c1cfbefb28a8c02', 3, 1, '*', '[]', 1, '2020-01-15 01:47:41', '2020-01-15 01:47:41', '2021-01-15 07:17:41'),
('fd35c70bc8febbc7ea8972f698c45d068235611f39f2dc4fe9a6376cc042f2fa524d36cad2971805', 4, 1, '*', '[]', 1, '2020-01-15 04:41:29', '2020-01-15 04:41:29', '2021-01-15 10:11:29'),
('fdb8974c243b83090bd9fcfed455d39897ee56e6add9b4c43d1994b6dd00f579fa24e7ebd6a993ef', 5, 1, '*', '[]', 1, '2020-01-18 02:40:02', '2020-01-18 02:40:02', '2021-01-18 08:10:02'),
('fdb9e7b36f4522dc1270959a681b68a0af1d96d054dbb2ebe679240bcb944fd436793c797e0ef4cc', 7, 1, '*', '[]', 1, '2020-01-09 02:21:51', '2020-01-09 02:21:51', '2021-01-09 07:51:51'),
('fe72b913ea8f75d699125436822f6dba309ee9c5b53ce38b1acc36b957b38106a496408e204d712e', 10, 1, '*', '[]', 1, '2020-01-18 01:48:21', '2020-01-18 01:48:21', '2021-01-18 07:18:21'),
('fe7a88d18e6773752ec53f910e566a2d74077247c5df49f54811ea655aa6a924f221bede8da0bab6', 62, 1, '*', '[]', 1, '2020-01-14 07:46:56', '2020-01-14 07:46:56', '2021-01-14 13:16:56'),
('fedd1964dc74f78b21a6f6ec643ee2c9a746d7e58a1f5ea4a0656fb5f6492ff665fc7d0c3ed612fd', 2, 1, '*', '[]', 1, '2020-01-20 05:44:48', '2020-01-20 05:44:48', '2021-01-20 11:14:48'),
('fee6aa3923f35649118514fe9d0dc0ff7277437861356530489668c954d0d6cee12608a271f0a1e1', 29, 1, '*', '[]', 1, '2020-01-09 23:00:57', '2020-01-09 23:00:57', '2021-01-10 04:30:57'),
('fef97918e8862b10d83d18d3a521c3222933afec6737309b853c626e1fc19aed05750afc2c826774', 7, 1, '*', '[]', 1, '2020-01-09 02:41:37', '2020-01-09 02:41:37', '2021-01-09 08:11:37'),
('ff6ed88c596c1dd5bac9315f79310c898b5ac39cb480f34e6c1646b9db9b70fcdcaf089222414c9d', 62, 1, '*', '[]', 1, '2020-01-15 06:38:04', '2020-01-15 06:38:04', '2021-01-15 12:08:04'),
('ff82209f610f88e8b83c751ed41d6740a2edbf046d9fac47e3f84f0b9fb9f6306012fe24e34488f8', 74, 1, '*', '[]', 1, '2020-01-15 23:00:03', '2020-01-15 23:00:03', '2021-01-16 04:30:03'),
('ff88fdfcea9427bb1d6ee0700a600eeb0e06df393e0c7a4b30110c9f336cf27262c519a73d343ae4', 1, 1, '*', '[]', 1, '2020-01-08 03:26:22', '2020-01-08 03:26:22', '2021-01-08 08:56:22'),
('ff8cf1055694ea381b1b58f1fb00c3dc9d40151147c67f45909b0f617a4d5c8eae62eac14baac884', 10, 1, '*', '[]', 1, '2020-01-17 00:41:08', '2020-01-17 00:41:08', '2021-01-17 06:11:08');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'twiva', 'wSWy0QqFHIcmv26vEVDp6NTrcaYHlTca3Kpf3Ic1', 'http://localhost', 1, 0, 0, '2019-09-27 00:23:59', '2019-09-27 00:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-09-27 00:23:59', '2019-09-27 00:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` int(11) NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `infulencer_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_per_post` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `what_to_include` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `what_not_to_include` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashtag` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `post_name`, `post_type`, `price_per_post`, `description`, `what_to_include`, `what_not_to_include`, `hashtag`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'dgd', NULL, '5635', 'dhhd', 'gdgd', 'dghd', 'dggd', '1', '2020-01-18 03:01:34', '2020-01-18 03:01:34'),
(2, 2, 'you utt', NULL, '6868', 'xbdb', 'vxhx', 'xggx', 'xhxhx', '1', '2020-01-18 03:03:31', '2020-01-18 03:03:31'),
(3, 2, 'bzbz', NULL, '5656', 'sgshs', 'vzbz', 'vdh', 'dvd', '1', '2020-01-18 03:24:50', '2020-01-18 03:24:50'),
(4, 2, 'bzbz', NULL, '5656', 'sgshs', 'vzbz', 'vdh', 'dvd', '1', '2020-01-18 03:25:10', '2020-01-18 03:25:10'),
(5, 4, 'New', NULL, '20', 'Rerte', 'Ertert', 'Hgfh', '#gfgfgrter', '1', '2020-01-18 04:45:02', '2020-01-18 04:45:02'),
(6, 2, 'xbvx', NULL, '849454', 'svsvs', 'czcz', 'zffss', 'xgg', '1', '2020-01-19 23:52:02', '2020-01-19 23:52:02'),
(7, 2, 'mark1', NULL, '123', 'qwerty', 'Not', 'A thing', 'qwerty', '1', '2020-01-20 00:04:46', '2020-01-20 00:04:46'),
(8, 2, 'v, gzz', NULL, '5444', 'csgz', 'vz', 'zfG', 'scgs', '1', '2020-01-20 00:38:00', '2020-01-20 00:38:00'),
(9, 2, 'vgdgd', NULL, '6464', 'bzhz', 'f', 'fzgd', 'dvbs', '1', '2020-01-20 00:41:24', '2020-01-20 00:41:24'),
(10, 2, 'vgdgd', NULL, '6464', 'bzhz', 'f', 'fzgd', 'dvbs', '1', '2020-01-20 00:42:29', '2020-01-20 00:42:29'),
(11, 2, 'vbhd', NULL, '665', 'zgshs', 'ggs', 'sggs', 'bzhs', '1', '2020-01-20 00:44:00', '2020-01-20 00:44:00'),
(12, 2, 'vxgsy', NULL, '45464', 'sgsg', 'vzgz', 'gxy', 'sfsgs', '1', '2020-01-20 00:47:00', '2020-01-20 00:47:00'),
(13, 2, 'txtx', NULL, '2828', 'gxyx', 'ffyhc', 'gxgx', 'txtx', '1', '2020-01-20 00:56:28', '2020-01-20 00:56:28'),
(14, 2, 'vvsh', NULL, '9494', 'ggs', 'gsgz', 'gzgd', 'gsgd', '1', '2020-01-20 00:59:11', '2020-01-20 00:59:11'),
(15, 2, 'gxgx', NULL, '906868', 'sfgs', 'vgx', 'afgs', 'cc', '1', '2020-01-20 01:02:12', '2020-01-20 01:02:12'),
(16, 2, 'gxgx', NULL, '906868', 'sfgs', 'vgx', 'afgs', 'cc', '1', '2020-01-20 01:02:28', '2020-01-20 01:02:28'),
(17, 2, 'gxgx', NULL, '906868', 'sfgs', 'vgx', 'afgs', 'cc', '1', '2020-01-20 01:02:34', '2020-01-20 01:02:34'),
(18, 2, 'vzvxx', NULL, '88757', 'zgzgz', 'bdhd', 'dhdhx', 'zvzfz', '1', '2020-01-20 01:05:36', '2020-01-20 01:05:36'),
(19, 2, 'ugjgfj', NULL, '535', 'ydgd', 'fuuf', 'yffy', 'gxgd', '1', '2020-01-20 01:26:47', '2020-01-20 01:26:47'),
(20, 2, 'gdhx', NULL, '6465', 'hshs', 'gvxd', 'zgzg', 'dgdhd', '1', '2020-01-20 01:37:00', '2020-01-20 01:37:00'),
(21, 2, 'xbbxxh', NULL, '6868', 'hcgx', 'hxch', 'cyhc', 'ggx', '1', '2020-01-20 01:41:55', '2020-01-20 01:41:55'),
(22, 4, 'new', NULL, '12', 'Gtertyrty', 'Try try', 'Hjghjgj', '#sdfsdf', '1', '2020-01-20 01:50:02', '2020-01-20 01:50:02'),
(23, 4, 'Nwwww', NULL, '20', 'Gterte', 'Ertertf', 'Ghfghfgh', '#dfsdf', '1', '2020-01-20 01:53:13', '2020-01-20 01:53:13'),
(24, 4, 'Newe', NULL, '20', 'Hfgh', 'Fghfgh', 'Fghfgh', '#dfgdg', '1', '2020-01-20 01:56:25', '2020-01-20 01:56:25'),
(25, 2, 'jsus', NULL, '4364', 'ahsh', 'jeje', 'hsje', 'shsh', '1', '2020-01-20 01:58:31', '2020-01-20 01:58:31'),
(26, 2, 'vxvxg', NULL, '884', 'sgsgs', 'hdhd', 'xgdhd', 'svsg', '1', '2020-01-20 02:00:28', '2020-01-20 02:00:28'),
(27, 2, 'gxhzz', NULL, '576', 'gzvz', 'czvz', 'zvgz', 'zggz', '1', '2020-01-20 02:18:08', '2020-01-20 02:18:08'),
(28, 2, 'bxbxdysh', NULL, '8698', 'sbbs', 'vzvx', 'zggz', 'zgzsv', '1', '2020-01-20 02:25:29', '2020-01-20 02:25:29'),
(29, 2, 'gsgd', NULL, '6564545', 'sgsg', 'fsgs', 'sfsg', 'sgsgs', '1', '2020-01-20 02:31:04', '2020-01-20 02:31:04'),
(30, 2, 'gxgcft', NULL, '8635', 'g gc', 'ggc', 'cyyc', 'gxgxdy', '1', '2020-01-20 04:15:04', '2020-01-20 04:15:04'),
(31, 2, 'gdtx', NULL, '59565', 'gsgz', 'vxgd', 'zgsh', 'fdg', '1', '2020-01-20 04:46:40', '2020-01-20 04:46:40'),
(32, 2, 'yxhxzbhz', NULL, '4946', 's bs', 'vaga', 'gsg', 'zbzh', '1', '2020-01-20 04:51:20', '2020-01-20 04:51:20'),
(33, 2, 'xbhx', NULL, '455775767', 'vzhz', 'gzgz', 'xgsg', 'svzg', '1', '2020-01-20 04:53:57', '2020-01-20 04:53:57'),
(34, 2, 'gzgs', NULL, '88558', 'txxx', 'fzfz', 'zfzfz', 'gxgx', '1', '2020-01-20 04:57:18', '2020-01-20 04:57:18'),
(35, 2, 'gxgx', NULL, '5758', 'gxxg', 'xggx', 'xgxg', 'xfgx', '1', '2020-01-20 05:02:22', '2020-01-20 05:02:22'),
(36, 2, 'gdgdgd', NULL, '3535', 'gxxg', 'yffy', 'yxxt', 'gxgc', '1', '2020-01-20 05:03:09', '2020-01-20 05:03:09'),
(37, 2, 'tdgxgxxxt', NULL, '5850', 'cgfyc', 'gcgccgcg', 'gtccg', 'xgcgxf', '1', '2020-01-20 05:04:39', '2020-01-20 05:04:39'),
(38, 2, 'zbhzu', NULL, '7679767', 'vzvz', 'c,vz', 'fzvz', 'zgzgz', '1', '2020-01-20 05:19:13', '2020-01-20 05:19:13'),
(39, 2, 'dbhd', NULL, '6868', 'gxhx', ', gz', 'gxgd', 'xhxh', '1', '2020-01-20 05:25:46', '2020-01-20 05:25:46'),
(40, 2, 'zggs', NULL, '7976', 'gzg', 'xggz', 'gsgs', 'gzgz', '1', '2020-01-20 05:28:10', '2020-01-20 05:28:10'),
(41, 2, 'gdgd', NULL, '5234', 'shdh', 'gdgd', 'gdgs', 'dggs', '1', '2020-01-20 05:50:33', '2020-01-20 05:50:33'),
(42, 2, 'gdhd', NULL, '5655', 'sghs', 'hdhs', 'shhs', 'sggs', '1', '2020-01-20 05:53:14', '2020-01-20 05:53:14'),
(43, 2, 'gdhd', NULL, '5655', 'sghs', 'hdhs', 'shhs', 'sggs', '1', '2020-01-20 05:54:58', '2020-01-20 05:54:58'),
(44, 2, 'gdhd', NULL, '5655', 'sghs', 'hdhs', 'shhs', 'sggs', '1', '2020-01-20 05:55:26', '2020-01-20 05:55:26'),
(45, 2, 'gdhd', NULL, '5655', 'sghs', 'hdhs', 'shhs', 'sggs', '1', '2020-01-20 05:55:31', '2020-01-20 05:55:31'),
(46, 2, 'gsys', NULL, '535', 'vdhd', 'gxh', 'dhdh', 'dgyd', '1', '2020-01-20 05:57:33', '2020-01-20 05:57:33'),
(47, 2, 'gsys', NULL, '535', 'vdhd', 'gxh', 'dhdh', 'dgyd', '1', '2020-01-20 06:00:09', '2020-01-20 06:00:09');

-- --------------------------------------------------------

--
-- Table structure for table `post_details`
--

CREATE TABLE `post_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `post_user_id` int(11) DEFAULT NULL,
  `text_one` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_two` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_three` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_details_media`
--

CREATE TABLE `post_details_media` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_detail_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(50) UNSIGNED NOT NULL,
  `media` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_details_media`
--

INSERT INTO `post_details_media` (`id`, `post_detail_id`, `user_id`, `media`, `thumbnail`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '5e25804b05884_heart.png', NULL, '2020-01-20 04:56:19', '2020-01-20 04:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `post_images`
--

CREATE TABLE `post_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `thumbnail` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_images`
--

INSERT INTO `post_images` (`id`, `post_id`, `thumbnail`, `media`, `created_at`, `updated_at`) VALUES
(1, 44, NULL, '5e258e26d1493_1579519525720.jpg', '2020-01-20 05:55:26', '2020-01-20 05:55:26');

-- --------------------------------------------------------

--
-- Table structure for table `post_prices`
--

CREATE TABLE `post_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `infulancer_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complementry_item` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_resend` int(50) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_prices`
--

INSERT INTO `post_prices` (`id`, `user_id`, `post_id`, `price`, `status`, `infulancer_id`, `complementry_item`, `is_resend`, `created_at`, `updated_at`) VALUES
(1, 4, '5', '20', '1', '1', '', 0, '2020-01-18 04:45:15', '2020-01-20 00:11:15'),
(2, 4, '23', '20', '1', '1', '', 0, '2020-01-20 01:53:23', '2020-01-20 01:53:23'),
(3, 4, '24', '20', '1', '1', '', 0, '2020-01-20 01:58:59', '2020-01-20 04:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `post_users`
--

CREATE TABLE `post_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `reason` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reject_post_reason` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_submit` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reject_amount` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_resend` int(50) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `share_data`
--

CREATE TABLE `share_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infulancer_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('I','B') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'I' COMMENT 'I => Influencer B => Business',
  `password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `contact_person` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0 => Inactive 1 => active 2 => block',
  `is_deleted` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 => active 1 => deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `user_type`, `password`, `profile`, `country_code`, `phone_number`, `contact_person`, `facebook_id`, `instagram_id`, `twitter_id`, `device_id`, `device_type`, `reset_password_token`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(2, 'deft', 'deft@yopmail.com', 'B', '$2y$10$4N1Qhs9QCApd5AJOluwdWesgAmgdchs2shTR44qQWBQSwC0U5j6qe', '', '91', NULL, 'None', NULL, NULL, NULL, '6546', 'android', NULL, '1', '0', '2020-01-18 03:00:51', '2020-01-20 06:25:47'),
(3, 'Newwww', 'new3@yopmail.com', 'B', '$2y$10$l4XiIWmgNnCf22pNTT4h..uSmESuC.5lzU.nwlG4ZRtWutmjxtrmG', '5e22c2696ad9c_profile.jpeg', NULL, 1234578935, NULL, NULL, NULL, NULL, '12335556', '1', NULL, '1', '0', '2020-01-18 03:01:37', '2020-01-18 03:01:37'),
(4, 'Newqqqq', 'new1@yopmail.com', 'B', '$2y$10$f8Brj01zzHtRi/BLNCWfr.BhNHLjiFPOeJ31OFXBii9qQBsnbVpgq', '5e22c6355c7fe_profile.jpeg', NULL, 12345678944, NULL, NULL, NULL, NULL, '12335556', '1', NULL, '1', '0', '2020-01-18 03:03:59', '2020-01-18 03:17:49'),
(6, 'Tester Pvt Ltd', 'tester@yopmail.com', 'B', '$2y$10$6TpftX4dhnOltPOX5VKhwuGFcu.m/vO4NzSKx6mw7f1.JUm4tdVAu', '5e254d8cc2df1_profile.jpeg', NULL, 4556885000, NULL, NULL, NULL, NULL, '10bceffe89c17d88b9671c347629f8ccc6ad49f6ad5ba213f517885cc88b7110', '1', NULL, '1', '1', '2020-01-20 01:19:48', '2020-01-20 05:58:04'),
(8, 'Gold mines', 'gold@yopmail.com', 'B', '$2y$10$uyCO7FtndPgHcdvogAFGouKWgzj500FtbQwKOtC5LWAgN3xfSFgfW', '5e258eb5dee77_dahlia-red-blossom-bloom-60597.jpeg', '91', 456123400, 'None', NULL, NULL, NULL, '6546', 'android', NULL, '1', '0', '2020-01-20 05:29:12', '2020-01-20 05:57:49'),
(9, 'Tina Sharma', 'testing.testing139@gmail.com', 'I', NULL, NULL, NULL, NULL, NULL, '1457140931129180', NULL, NULL, 'a51554d579f7cf70', 'Android', NULL, '1', '0', '2020-01-20 06:22:43', '2020-01-20 06:26:42'),
(10, 'Quality Assurance', 'deftsoftmobileqa2@gmail.com', 'I', NULL, NULL, NULL, NULL, NULL, '463470447876042', NULL, NULL, 'a51554d579f7cf70', 'Android', NULL, '2', '0', '2020-01-20 06:28:09', '2020-01-20 06:28:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_payments`
--
ALTER TABLE `admin_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_payments_user_id_index` (`user_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_details`
--
ALTER TABLE `business_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_details_user_id_index` (`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featureds`
--
ALTER TABLE `featureds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gigs`
--
ALTER TABLE `gigs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gigs_user_id_index` (`user_id`);

--
-- Indexes for table `gig_details`
--
ALTER TABLE `gig_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gig_details_user_id_index` (`user_id`),
  ADD KEY `gig_details_gig_id_index` (`gig_id`);

--
-- Indexes for table `gig_media`
--
ALTER TABLE `gig_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gig_media_gig_id_index` (`gig_id`);

--
-- Indexes for table `gig_media_details`
--
ALTER TABLE `gig_media_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gig_media_details_gig_detail_id_index` (`gig_detail_id`);

--
-- Indexes for table `gig_prices`
--
ALTER TABLE `gig_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gig_prices_user_id_index` (`user_id`);

--
-- Indexes for table `gig_users`
--
ALTER TABLE `gig_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gig_users_user_id_index` (`user_id`);

--
-- Indexes for table `influencer_details`
--
ALTER TABLE `influencer_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `influencer_details_user_id_index` (`user_id`);

--
-- Indexes for table `influencer_images`
--
ALTER TABLE `influencer_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `influencer_images_user_id_index` (`user_id`);

--
-- Indexes for table `influencer_interests`
--
ALTER TABLE `influencer_interests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `influencer_interests_user_id_index` (`user_id`);

--
-- Indexes for table `influencer_price`
--
ALTER TABLE `influencer_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `influencer_price_user_id_index` (`user_id`);

--
-- Indexes for table `influencer_social_details`
--
ALTER TABLE `influencer_social_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `influencer_social_details_user_id_index` (`user_id`);

--
-- Indexes for table `infulancer_ratings`
--
ALTER TABLE `infulancer_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `infulancer_ratings_user_id_index` (`user_id`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interest_users`
--
ALTER TABLE `interest_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `interest_users_user_id_index` (`user_id`);

--
-- Indexes for table `intersts_users`
--
ALTER TABLE `intersts_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `intersts_users_user_id_index` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpesa_phone_numbers`
--
ALTER TABLE `mpesa_phone_numbers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mpesa_phone_numbers_user_id_index` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_index` (`user_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_index` (`user_id`);

--
-- Indexes for table `post_details`
--
ALTER TABLE `post_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_details_user_id_index` (`user_id`),
  ADD KEY `post_details_post_id_index` (`post_id`);

--
-- Indexes for table `post_details_media`
--
ALTER TABLE `post_details_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_images`
--
ALTER TABLE `post_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_images_post_id_index` (`post_id`);

--
-- Indexes for table `post_prices`
--
ALTER TABLE `post_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_prices_user_id_index` (`user_id`);

--
-- Indexes for table `post_users`
--
ALTER TABLE `post_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_users_user_id_index` (`user_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_index` (`user_id`);

--
-- Indexes for table `share_data`
--
ALTER TABLE `share_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `share_data_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_payments`
--
ALTER TABLE `admin_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `business_details`
--
ALTER TABLE `business_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `featureds`
--
ALTER TABLE `featureds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gigs`
--
ALTER TABLE `gigs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gig_details`
--
ALTER TABLE `gig_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gig_media`
--
ALTER TABLE `gig_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gig_media_details`
--
ALTER TABLE `gig_media_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gig_prices`
--
ALTER TABLE `gig_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gig_users`
--
ALTER TABLE `gig_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `influencer_details`
--
ALTER TABLE `influencer_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `influencer_images`
--
ALTER TABLE `influencer_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `influencer_interests`
--
ALTER TABLE `influencer_interests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `influencer_price`
--
ALTER TABLE `influencer_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `influencer_social_details`
--
ALTER TABLE `influencer_social_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `infulancer_ratings`
--
ALTER TABLE `infulancer_ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `interest_users`
--
ALTER TABLE `interest_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `intersts_users`
--
ALTER TABLE `intersts_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `mpesa_phone_numbers`
--
ALTER TABLE `mpesa_phone_numbers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `post_details`
--
ALTER TABLE `post_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `post_details_media`
--
ALTER TABLE `post_details_media`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `post_images`
--
ALTER TABLE `post_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `post_prices`
--
ALTER TABLE `post_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `post_users`
--
ALTER TABLE `post_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `share_data`
--
ALTER TABLE `share_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_payments`
--
ALTER TABLE `admin_payments`
  ADD CONSTRAINT `admin_payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `business_details`
--
ALTER TABLE `business_details`
  ADD CONSTRAINT `business_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gigs`
--
ALTER TABLE `gigs`
  ADD CONSTRAINT `gigs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gig_details`
--
ALTER TABLE `gig_details`
  ADD CONSTRAINT `gig_details_gig_id_foreign` FOREIGN KEY (`gig_id`) REFERENCES `gigs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gig_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gig_media`
--
ALTER TABLE `gig_media`
  ADD CONSTRAINT `gig_media_gig_id_foreign` FOREIGN KEY (`gig_id`) REFERENCES `gigs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gig_media_details`
--
ALTER TABLE `gig_media_details`
  ADD CONSTRAINT `gig_media_details_gig_detail_id_foreign` FOREIGN KEY (`gig_detail_id`) REFERENCES `gig_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gig_prices`
--
ALTER TABLE `gig_prices`
  ADD CONSTRAINT `gig_prices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gig_users`
--
ALTER TABLE `gig_users`
  ADD CONSTRAINT `gig_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `influencer_details`
--
ALTER TABLE `influencer_details`
  ADD CONSTRAINT `influencer_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `influencer_images`
--
ALTER TABLE `influencer_images`
  ADD CONSTRAINT `influencer_images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `influencer_interests`
--
ALTER TABLE `influencer_interests`
  ADD CONSTRAINT `influencer_interests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `influencer_price`
--
ALTER TABLE `influencer_price`
  ADD CONSTRAINT `influencer_price_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `influencer_social_details`
--
ALTER TABLE `influencer_social_details`
  ADD CONSTRAINT `influencer_social_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `infulancer_ratings`
--
ALTER TABLE `infulancer_ratings`
  ADD CONSTRAINT `infulancer_ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `interest_users`
--
ALTER TABLE `interest_users`
  ADD CONSTRAINT `interest_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `intersts_users`
--
ALTER TABLE `intersts_users`
  ADD CONSTRAINT `intersts_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mpesa_phone_numbers`
--
ALTER TABLE `mpesa_phone_numbers`
  ADD CONSTRAINT `mpesa_phone_numbers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_details`
--
ALTER TABLE `post_details`
  ADD CONSTRAINT `post_details_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_images`
--
ALTER TABLE `post_images`
  ADD CONSTRAINT `post_images_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_prices`
--
ALTER TABLE `post_prices`
  ADD CONSTRAINT `post_prices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_users`
--
ALTER TABLE `post_users`
  ADD CONSTRAINT `post_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `share_data`
--
ALTER TABLE `share_data`
  ADD CONSTRAINT `share_data_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
